
package co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarAgrupacionSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpActualizarAgrupacionSubsistemaFallo");
    private final static QName _OpCrearAgrupacionSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpCrearAgrupacionSubsistemaResp");
    private final static QName _OpBuscarPorCriteriosAgrupacionSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpBuscarPorCriteriosAgrupacionSubsistemaFallo");
    private final static QName _OpActualizarAgrupacionSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpActualizarAgrupacionSubsistemaResp");
    private final static QName _OpCrearAgrupacionSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpCrearAgrupacionSubsistemaSol");
    private final static QName _OpActualizarAgrupacionSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpActualizarAgrupacionSubsistemaSol");
    private final static QName _OpBuscarPorCriteriosAgrupacionSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpBuscarPorCriteriosAgrupacionSubsistemaSol");
    private final static QName _OpBuscarPorCriteriosAgrupacionSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpBuscarPorCriteriosAgrupacionSubsistemaResp");
    private final static QName _OpCrearAgrupacionSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", "OpCrearAgrupacionSubsistemaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionSubsistemaRespTipo }
     * 
     */
    public OpCrearAgrupacionSubsistemaRespTipo createOpCrearAgrupacionSubsistemaRespTipo() {
        return new OpCrearAgrupacionSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAgrupacionSubsistemaRespTipo }
     * 
     */
    public OpActualizarAgrupacionSubsistemaRespTipo createOpActualizarAgrupacionSubsistemaRespTipo() {
        return new OpActualizarAgrupacionSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo createOpBuscarPorCriteriosAgrupacionSubsistemaRespTipo() {
        return new OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo createOpBuscarPorCriteriosAgrupacionSubsistemaSolTipo() {
        return new OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAgrupacionSubsistemaSolTipo }
     * 
     */
    public OpActualizarAgrupacionSubsistemaSolTipo createOpActualizarAgrupacionSubsistemaSolTipo() {
        return new OpActualizarAgrupacionSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionSubsistemaSolTipo }
     * 
     */
    public OpCrearAgrupacionSubsistemaSolTipo createOpCrearAgrupacionSubsistemaSolTipo() {
        return new OpCrearAgrupacionSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpActualizarAgrupacionSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpActualizarAgrupacionSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAgrupacionSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpCrearAgrupacionSubsistemaResp")
    public JAXBElement<OpCrearAgrupacionSubsistemaRespTipo> createOpCrearAgrupacionSubsistemaResp(OpCrearAgrupacionSubsistemaRespTipo value) {
        return new JAXBElement<OpCrearAgrupacionSubsistemaRespTipo>(_OpCrearAgrupacionSubsistemaResp_QNAME, OpCrearAgrupacionSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpBuscarPorCriteriosAgrupacionSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAgrupacionSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAgrupacionSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAgrupacionSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpActualizarAgrupacionSubsistemaResp")
    public JAXBElement<OpActualizarAgrupacionSubsistemaRespTipo> createOpActualizarAgrupacionSubsistemaResp(OpActualizarAgrupacionSubsistemaRespTipo value) {
        return new JAXBElement<OpActualizarAgrupacionSubsistemaRespTipo>(_OpActualizarAgrupacionSubsistemaResp_QNAME, OpActualizarAgrupacionSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpCrearAgrupacionSubsistemaSol")
    public JAXBElement<OpCrearAgrupacionSubsistemaSolTipo> createOpCrearAgrupacionSubsistemaSol(OpCrearAgrupacionSubsistemaSolTipo value) {
        return new JAXBElement<OpCrearAgrupacionSubsistemaSolTipo>(_OpCrearAgrupacionSubsistemaSol_QNAME, OpCrearAgrupacionSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAgrupacionSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpActualizarAgrupacionSubsistemaSol")
    public JAXBElement<OpActualizarAgrupacionSubsistemaSolTipo> createOpActualizarAgrupacionSubsistemaSol(OpActualizarAgrupacionSubsistemaSolTipo value) {
        return new JAXBElement<OpActualizarAgrupacionSubsistemaSolTipo>(_OpActualizarAgrupacionSubsistemaSol_QNAME, OpActualizarAgrupacionSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpBuscarPorCriteriosAgrupacionSubsistemaSol")
    public JAXBElement<OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo> createOpBuscarPorCriteriosAgrupacionSubsistemaSol(OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo>(_OpBuscarPorCriteriosAgrupacionSubsistemaSol_QNAME, OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpBuscarPorCriteriosAgrupacionSubsistemaResp")
    public JAXBElement<OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo> createOpBuscarPorCriteriosAgrupacionSubsistemaResp(OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo>(_OpBuscarPorCriteriosAgrupacionSubsistemaResp_QNAME, OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1", name = "OpCrearAgrupacionSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpCrearAgrupacionSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAgrupacionSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

}
