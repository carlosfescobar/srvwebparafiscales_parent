
package co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;


/**
 * <p>Clase Java para OpActualizarAgrupacionSubsistemaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarAgrupacionSubsistemaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="agrupacionSubsistema" type="{http://www.ugpp.gov.co/schema/Administradora/AgrupacionSubsistemaTipo/v1}AgrupacionSubsistemaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarAgrupacionSubsistemaSolTipo", propOrder = {
    "contextoTransaccional",
    "agrupacionSubsistema"
})
public class OpActualizarAgrupacionSubsistemaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AgrupacionSubsistemaTipo agrupacionSubsistema;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad agrupacionSubsistema.
     * 
     * @return
     *     possible object is
     *     {@link AgrupacionSubsistemaTipo }
     *     
     */
    public AgrupacionSubsistemaTipo getAgrupacionSubsistema() {
        return agrupacionSubsistema;
    }

    /**
     * Define el valor de la propiedad agrupacionSubsistema.
     * 
     * @param value
     *     allowed object is
     *     {@link AgrupacionSubsistemaTipo }
     *     
     */
    public void setAgrupacionSubsistema(AgrupacionSubsistemaTipo value) {
        this.agrupacionSubsistema = value;
    }

}
