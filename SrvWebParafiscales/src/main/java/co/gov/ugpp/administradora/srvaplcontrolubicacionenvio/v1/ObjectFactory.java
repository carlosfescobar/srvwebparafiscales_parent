
package co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarControlUbicacionEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpActualizarControlUbicacionEnvioResp");
    private final static QName _OpBuscarPorIdControlUbicacionEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpBuscarPorIdControlUbicacionEnvioResp");
    private final static QName _OpCrearControlUbicacionEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpCrearControlUbicacionEnvioFallo");
    private final static QName _OpActualizarControlUbicacionEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpActualizarControlUbicacionEnvioFallo");
    private final static QName _OpActualizarControlUbicacionEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpActualizarControlUbicacionEnvioSol");
    private final static QName _OpCrearControlUbicacionEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpCrearControlUbicacionEnvioSol");
    private final static QName _OpBuscarPorIdControlUbicacionEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpBuscarPorIdControlUbicacionEnvioSol");
    private final static QName _OpBuscarPorIdControlUbicacionEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpBuscarPorIdControlUbicacionEnvioFallo");
    private final static QName _OpCrearControlUbicacionEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", "OpCrearControlUbicacionEnvioResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarControlUbicacionEnvioSolTipo }
     * 
     */
    public OpActualizarControlUbicacionEnvioSolTipo createOpActualizarControlUbicacionEnvioSolTipo() {
        return new OpActualizarControlUbicacionEnvioSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlUbicacionEnvioRespTipo }
     * 
     */
    public OpBuscarPorIdControlUbicacionEnvioRespTipo createOpBuscarPorIdControlUbicacionEnvioRespTipo() {
        return new OpBuscarPorIdControlUbicacionEnvioRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlUbicacionEnvioRespTipo }
     * 
     */
    public OpActualizarControlUbicacionEnvioRespTipo createOpActualizarControlUbicacionEnvioRespTipo() {
        return new OpActualizarControlUbicacionEnvioRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlUbicacionEnvioSolTipo }
     * 
     */
    public OpCrearControlUbicacionEnvioSolTipo createOpCrearControlUbicacionEnvioSolTipo() {
        return new OpCrearControlUbicacionEnvioSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlUbicacionEnvioSolTipo }
     * 
     */
    public OpBuscarPorIdControlUbicacionEnvioSolTipo createOpBuscarPorIdControlUbicacionEnvioSolTipo() {
        return new OpBuscarPorIdControlUbicacionEnvioSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlUbicacionEnvioRespTipo }
     * 
     */
    public OpCrearControlUbicacionEnvioRespTipo createOpCrearControlUbicacionEnvioRespTipo() {
        return new OpCrearControlUbicacionEnvioRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlUbicacionEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpActualizarControlUbicacionEnvioResp")
    public JAXBElement<OpActualizarControlUbicacionEnvioRespTipo> createOpActualizarControlUbicacionEnvioResp(OpActualizarControlUbicacionEnvioRespTipo value) {
        return new JAXBElement<OpActualizarControlUbicacionEnvioRespTipo>(_OpActualizarControlUbicacionEnvioResp_QNAME, OpActualizarControlUbicacionEnvioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlUbicacionEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpBuscarPorIdControlUbicacionEnvioResp")
    public JAXBElement<OpBuscarPorIdControlUbicacionEnvioRespTipo> createOpBuscarPorIdControlUbicacionEnvioResp(OpBuscarPorIdControlUbicacionEnvioRespTipo value) {
        return new JAXBElement<OpBuscarPorIdControlUbicacionEnvioRespTipo>(_OpBuscarPorIdControlUbicacionEnvioResp_QNAME, OpBuscarPorIdControlUbicacionEnvioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpCrearControlUbicacionEnvioFallo")
    public JAXBElement<FalloTipo> createOpCrearControlUbicacionEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearControlUbicacionEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpActualizarControlUbicacionEnvioFallo")
    public JAXBElement<FalloTipo> createOpActualizarControlUbicacionEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarControlUbicacionEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlUbicacionEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpActualizarControlUbicacionEnvioSol")
    public JAXBElement<OpActualizarControlUbicacionEnvioSolTipo> createOpActualizarControlUbicacionEnvioSol(OpActualizarControlUbicacionEnvioSolTipo value) {
        return new JAXBElement<OpActualizarControlUbicacionEnvioSolTipo>(_OpActualizarControlUbicacionEnvioSol_QNAME, OpActualizarControlUbicacionEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlUbicacionEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpCrearControlUbicacionEnvioSol")
    public JAXBElement<OpCrearControlUbicacionEnvioSolTipo> createOpCrearControlUbicacionEnvioSol(OpCrearControlUbicacionEnvioSolTipo value) {
        return new JAXBElement<OpCrearControlUbicacionEnvioSolTipo>(_OpCrearControlUbicacionEnvioSol_QNAME, OpCrearControlUbicacionEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlUbicacionEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpBuscarPorIdControlUbicacionEnvioSol")
    public JAXBElement<OpBuscarPorIdControlUbicacionEnvioSolTipo> createOpBuscarPorIdControlUbicacionEnvioSol(OpBuscarPorIdControlUbicacionEnvioSolTipo value) {
        return new JAXBElement<OpBuscarPorIdControlUbicacionEnvioSolTipo>(_OpBuscarPorIdControlUbicacionEnvioSol_QNAME, OpBuscarPorIdControlUbicacionEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpBuscarPorIdControlUbicacionEnvioFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdControlUbicacionEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdControlUbicacionEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlUbicacionEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1", name = "OpCrearControlUbicacionEnvioResp")
    public JAXBElement<OpCrearControlUbicacionEnvioRespTipo> createOpCrearControlUbicacionEnvioResp(OpCrearControlUbicacionEnvioRespTipo value) {
        return new JAXBElement<OpCrearControlUbicacionEnvioRespTipo>(_OpCrearControlUbicacionEnvioResp_QNAME, OpCrearControlUbicacionEnvioRespTipo.class, null, value);
    }

}
