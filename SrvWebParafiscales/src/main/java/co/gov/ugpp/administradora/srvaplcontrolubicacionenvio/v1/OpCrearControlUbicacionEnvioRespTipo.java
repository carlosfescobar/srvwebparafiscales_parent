
package co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;


/**
 * <p>Clase Java para OpCrearControlUbicacionEnvioRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearControlUbicacionEnvioRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="idControlUbicacionEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearControlUbicacionEnvioRespTipo", propOrder = {
    "contextoRespuesta",
    "idControlUbicacionEnvio"
})
public class OpCrearControlUbicacionEnvioRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(nillable = true)
    protected String idControlUbicacionEnvio;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad idControlUbicacionEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdControlUbicacionEnvio() {
        return idControlUbicacionEnvio;
    }

    /**
     * Define el valor de la propiedad idControlUbicacionEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdControlUbicacionEnvio(String value) {
        this.idControlUbicacionEnvio = value;
    }

}
