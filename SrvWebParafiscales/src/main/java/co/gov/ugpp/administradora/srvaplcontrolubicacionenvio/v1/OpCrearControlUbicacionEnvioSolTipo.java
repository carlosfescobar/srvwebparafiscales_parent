
package co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;


/**
 * <p>Clase Java para OpCrearControlUbicacionEnvioSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearControlUbicacionEnvioSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="controlUbicacionEnvio" type="{http://www.ugpp.gov.co/schema/Transversales/ControlUbicacionEnvioTipo/v1}ControlUbicacionEnvioTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearControlUbicacionEnvioSolTipo", propOrder = {
    "contextoTransaccional",
    "controlUbicacionEnvio"
})
public class OpCrearControlUbicacionEnvioSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ControlUbicacionEnvioTipo controlUbicacionEnvio;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad controlUbicacionEnvio.
     * 
     * @return
     *     possible object is
     *     {@link ControlUbicacionEnvioTipo }
     *     
     */
    public ControlUbicacionEnvioTipo getControlUbicacionEnvio() {
        return controlUbicacionEnvio;
    }

    /**
     * Define el valor de la propiedad controlUbicacionEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlUbicacionEnvioTipo }
     *     
     */
    public void setControlUbicacionEnvio(ControlUbicacionEnvioTipo value) {
        this.controlUbicacionEnvio = value;
    }

}
