
package co.gov.ugpp.administradora.srvapleventoenvio.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvapleventoenvio.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearEventoEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpCrearEventoEnvioResp");
    private final static QName _OpCrearEventoEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpCrearEventoEnvioFallo");
    private final static QName _OpCrearEventoEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpCrearEventoEnvioSol");
    private final static QName _OpBuscarPorIdEventoEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpBuscarPorIdEventoEnvioFallo");
    private final static QName _OpActualizarEventoEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpActualizarEventoEnvioSol");
    private final static QName _OpBuscarPorIdEventoEnvioSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpBuscarPorIdEventoEnvioSol");
    private final static QName _OpActualizarEventoEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpActualizarEventoEnvioResp");
    private final static QName _OpBuscarPorIdEventoEnvioResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpBuscarPorIdEventoEnvioResp");
    private final static QName _OpActualizarEventoEnvioFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", "OpActualizarEventoEnvioFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvapleventoenvio.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearEventoEnvioRespTipo }
     * 
     */
    public OpCrearEventoEnvioRespTipo createOpCrearEventoEnvioRespTipo() {
        return new OpCrearEventoEnvioRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearEventoEnvioSolTipo }
     * 
     */
    public OpCrearEventoEnvioSolTipo createOpCrearEventoEnvioSolTipo() {
        return new OpCrearEventoEnvioSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarEventoEnvioSolTipo }
     * 
     */
    public OpActualizarEventoEnvioSolTipo createOpActualizarEventoEnvioSolTipo() {
        return new OpActualizarEventoEnvioSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdEventoEnvioRespTipo }
     * 
     */
    public OpBuscarPorIdEventoEnvioRespTipo createOpBuscarPorIdEventoEnvioRespTipo() {
        return new OpBuscarPorIdEventoEnvioRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarEventoEnvioRespTipo }
     * 
     */
    public OpActualizarEventoEnvioRespTipo createOpActualizarEventoEnvioRespTipo() {
        return new OpActualizarEventoEnvioRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdEventoEnvioSolTipo }
     * 
     */
    public OpBuscarPorIdEventoEnvioSolTipo createOpBuscarPorIdEventoEnvioSolTipo() {
        return new OpBuscarPorIdEventoEnvioSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearEventoEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpCrearEventoEnvioResp")
    public JAXBElement<OpCrearEventoEnvioRespTipo> createOpCrearEventoEnvioResp(OpCrearEventoEnvioRespTipo value) {
        return new JAXBElement<OpCrearEventoEnvioRespTipo>(_OpCrearEventoEnvioResp_QNAME, OpCrearEventoEnvioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpCrearEventoEnvioFallo")
    public JAXBElement<FalloTipo> createOpCrearEventoEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearEventoEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearEventoEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpCrearEventoEnvioSol")
    public JAXBElement<OpCrearEventoEnvioSolTipo> createOpCrearEventoEnvioSol(OpCrearEventoEnvioSolTipo value) {
        return new JAXBElement<OpCrearEventoEnvioSolTipo>(_OpCrearEventoEnvioSol_QNAME, OpCrearEventoEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpBuscarPorIdEventoEnvioFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdEventoEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdEventoEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarEventoEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpActualizarEventoEnvioSol")
    public JAXBElement<OpActualizarEventoEnvioSolTipo> createOpActualizarEventoEnvioSol(OpActualizarEventoEnvioSolTipo value) {
        return new JAXBElement<OpActualizarEventoEnvioSolTipo>(_OpActualizarEventoEnvioSol_QNAME, OpActualizarEventoEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdEventoEnvioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpBuscarPorIdEventoEnvioSol")
    public JAXBElement<OpBuscarPorIdEventoEnvioSolTipo> createOpBuscarPorIdEventoEnvioSol(OpBuscarPorIdEventoEnvioSolTipo value) {
        return new JAXBElement<OpBuscarPorIdEventoEnvioSolTipo>(_OpBuscarPorIdEventoEnvioSol_QNAME, OpBuscarPorIdEventoEnvioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarEventoEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpActualizarEventoEnvioResp")
    public JAXBElement<OpActualizarEventoEnvioRespTipo> createOpActualizarEventoEnvioResp(OpActualizarEventoEnvioRespTipo value) {
        return new JAXBElement<OpActualizarEventoEnvioRespTipo>(_OpActualizarEventoEnvioResp_QNAME, OpActualizarEventoEnvioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdEventoEnvioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpBuscarPorIdEventoEnvioResp")
    public JAXBElement<OpBuscarPorIdEventoEnvioRespTipo> createOpBuscarPorIdEventoEnvioResp(OpBuscarPorIdEventoEnvioRespTipo value) {
        return new JAXBElement<OpBuscarPorIdEventoEnvioRespTipo>(_OpBuscarPorIdEventoEnvioResp_QNAME, OpBuscarPorIdEventoEnvioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1", name = "OpActualizarEventoEnvioFallo")
    public JAXBElement<FalloTipo> createOpActualizarEventoEnvioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarEventoEnvioFallo_QNAME, FalloTipo.class, null, value);
    }

}
