
package co.gov.ugpp.administradora.srvaplgestionadministradora.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplgestionadministradora.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdGestionAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorIdGestionAdministradoraResp");
    private final static QName _OpActualizarGestionAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpActualizarGestionAdministradoraResp");
    private final static QName _OpCrearGestionAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpCrearGestionAdministradoraResp");
    private final static QName _OpActualizarGestionAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpActualizarGestionAdministradoraSol");
    private final static QName _OpCrearGestionAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpCrearGestionAdministradoraSol");
    private final static QName _OpBuscarPorIdGestionAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorIdGestionAdministradoraSol");
    private final static QName _OpCrearGestionAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpCrearGestionAdministradoraFallo");
    private final static QName _OpBuscarPorIdGestionAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorIdGestionAdministradoraFallo");
    private final static QName _OpBuscarPorCriteriosGestionAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorCriteriosGestionAdministradoraSol");
    private final static QName _OpActualizarGestionAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpActualizarGestionAdministradoraFallo");
    private final static QName _OpBuscarPorCriteriosGestionAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorCriteriosGestionAdministradoraFallo");
    private final static QName _OpBuscarPorCriteriosGestionAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", "OpBuscarPorCriteriosGestionAdministradoraResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplgestionadministradora.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdGestionAdministradoraSolTipo }
     * 
     */
    public OpBuscarPorIdGestionAdministradoraSolTipo createOpBuscarPorIdGestionAdministradoraSolTipo() {
        return new OpBuscarPorIdGestionAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosGestionAdministradoraRespTipo }
     * 
     */
    public OpBuscarPorCriteriosGestionAdministradoraRespTipo createOpBuscarPorCriteriosGestionAdministradoraRespTipo() {
        return new OpBuscarPorCriteriosGestionAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosGestionAdministradoraSolTipo }
     * 
     */
    public OpBuscarPorCriteriosGestionAdministradoraSolTipo createOpBuscarPorCriteriosGestionAdministradoraSolTipo() {
        return new OpBuscarPorCriteriosGestionAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarGestionAdministradoraRespTipo }
     * 
     */
    public OpActualizarGestionAdministradoraRespTipo createOpActualizarGestionAdministradoraRespTipo() {
        return new OpActualizarGestionAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdGestionAdministradoraRespTipo }
     * 
     */
    public OpBuscarPorIdGestionAdministradoraRespTipo createOpBuscarPorIdGestionAdministradoraRespTipo() {
        return new OpBuscarPorIdGestionAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearGestionAdministradoraSolTipo }
     * 
     */
    public OpCrearGestionAdministradoraSolTipo createOpCrearGestionAdministradoraSolTipo() {
        return new OpCrearGestionAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarGestionAdministradoraSolTipo }
     * 
     */
    public OpActualizarGestionAdministradoraSolTipo createOpActualizarGestionAdministradoraSolTipo() {
        return new OpActualizarGestionAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearGestionAdministradoraRespTipo }
     * 
     */
    public OpCrearGestionAdministradoraRespTipo createOpCrearGestionAdministradoraRespTipo() {
        return new OpCrearGestionAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdGestionAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorIdGestionAdministradoraResp")
    public JAXBElement<OpBuscarPorIdGestionAdministradoraRespTipo> createOpBuscarPorIdGestionAdministradoraResp(OpBuscarPorIdGestionAdministradoraRespTipo value) {
        return new JAXBElement<OpBuscarPorIdGestionAdministradoraRespTipo>(_OpBuscarPorIdGestionAdministradoraResp_QNAME, OpBuscarPorIdGestionAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarGestionAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpActualizarGestionAdministradoraResp")
    public JAXBElement<OpActualizarGestionAdministradoraRespTipo> createOpActualizarGestionAdministradoraResp(OpActualizarGestionAdministradoraRespTipo value) {
        return new JAXBElement<OpActualizarGestionAdministradoraRespTipo>(_OpActualizarGestionAdministradoraResp_QNAME, OpActualizarGestionAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearGestionAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpCrearGestionAdministradoraResp")
    public JAXBElement<OpCrearGestionAdministradoraRespTipo> createOpCrearGestionAdministradoraResp(OpCrearGestionAdministradoraRespTipo value) {
        return new JAXBElement<OpCrearGestionAdministradoraRespTipo>(_OpCrearGestionAdministradoraResp_QNAME, OpCrearGestionAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarGestionAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpActualizarGestionAdministradoraSol")
    public JAXBElement<OpActualizarGestionAdministradoraSolTipo> createOpActualizarGestionAdministradoraSol(OpActualizarGestionAdministradoraSolTipo value) {
        return new JAXBElement<OpActualizarGestionAdministradoraSolTipo>(_OpActualizarGestionAdministradoraSol_QNAME, OpActualizarGestionAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearGestionAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpCrearGestionAdministradoraSol")
    public JAXBElement<OpCrearGestionAdministradoraSolTipo> createOpCrearGestionAdministradoraSol(OpCrearGestionAdministradoraSolTipo value) {
        return new JAXBElement<OpCrearGestionAdministradoraSolTipo>(_OpCrearGestionAdministradoraSol_QNAME, OpCrearGestionAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdGestionAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorIdGestionAdministradoraSol")
    public JAXBElement<OpBuscarPorIdGestionAdministradoraSolTipo> createOpBuscarPorIdGestionAdministradoraSol(OpBuscarPorIdGestionAdministradoraSolTipo value) {
        return new JAXBElement<OpBuscarPorIdGestionAdministradoraSolTipo>(_OpBuscarPorIdGestionAdministradoraSol_QNAME, OpBuscarPorIdGestionAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpCrearGestionAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpCrearGestionAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearGestionAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorIdGestionAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdGestionAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdGestionAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosGestionAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorCriteriosGestionAdministradoraSol")
    public JAXBElement<OpBuscarPorCriteriosGestionAdministradoraSolTipo> createOpBuscarPorCriteriosGestionAdministradoraSol(OpBuscarPorCriteriosGestionAdministradoraSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosGestionAdministradoraSolTipo>(_OpBuscarPorCriteriosGestionAdministradoraSol_QNAME, OpBuscarPorCriteriosGestionAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpActualizarGestionAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpActualizarGestionAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarGestionAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorCriteriosGestionAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosGestionAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosGestionAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosGestionAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1", name = "OpBuscarPorCriteriosGestionAdministradoraResp")
    public JAXBElement<OpBuscarPorCriteriosGestionAdministradoraRespTipo> createOpBuscarPorCriteriosGestionAdministradoraResp(OpBuscarPorCriteriosGestionAdministradoraRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosGestionAdministradoraRespTipo>(_OpBuscarPorCriteriosGestionAdministradoraResp_QNAME, OpBuscarPorCriteriosGestionAdministradoraRespTipo.class, null, value);
    }

}
