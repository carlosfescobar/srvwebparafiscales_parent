
package co.gov.ugpp.administradora.srvaplgestionadministradora.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;


/**
 * <p>Clase Java para OpBuscarPorCriteriosGestionAdministradoraRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorCriteriosGestionAdministradoraRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="gestionAdministradora" type="{http://www.ugpp.gov.co/schema/Administradora/GestionAdministradoraTipo/v1}GestionAdministradoraTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorCriteriosGestionAdministradoraRespTipo", propOrder = {
    "contextoRespuesta",
    "gestionAdministradora"
})
public class OpBuscarPorCriteriosGestionAdministradoraRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected List<GestionAdministradoraTipo> gestionAdministradora;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Gets the value of the gestionAdministradora property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the gestionAdministradora property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGestionAdministradora().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GestionAdministradoraTipo }
     * 
     * 
     */
    public List<GestionAdministradoraTipo> getGestionAdministradora() {
        if (gestionAdministradora == null) {
            gestionAdministradora = new ArrayList<GestionAdministradoraTipo>();
        }
        return this.gestionAdministradora;
    }

}
