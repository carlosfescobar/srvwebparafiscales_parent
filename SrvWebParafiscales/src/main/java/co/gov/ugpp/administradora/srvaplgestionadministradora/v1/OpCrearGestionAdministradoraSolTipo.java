
package co.gov.ugpp.administradora.srvaplgestionadministradora.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;


/**
 * <p>Clase Java para OpCrearGestionAdministradoraSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearGestionAdministradoraSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="gestionAdministradora" type="{http://www.ugpp.gov.co/schema/Administradora/GestionAdministradoraTipo/v1}GestionAdministradoraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearGestionAdministradoraSolTipo", propOrder = {
    "contextoTransaccional",
    "gestionAdministradora"
})
public class OpCrearGestionAdministradoraSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected GestionAdministradoraTipo gestionAdministradora;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad gestionAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link GestionAdministradoraTipo }
     *     
     */
    public GestionAdministradoraTipo getGestionAdministradora() {
        return gestionAdministradora;
    }

    /**
     * Define el valor de la propiedad gestionAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link GestionAdministradoraTipo }
     *     
     */
    public void setGestionAdministradora(GestionAdministradoraTipo value) {
        this.gestionAdministradora = value;
    }

}
