
package co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearRespuestaAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpCrearRespuestaAdministradoraFallo");
    private final static QName _OpActualizarRespuestaAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpActualizarRespuestaAdministradoraResp");
    private final static QName _OpBuscarPorIdRespuestaAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpBuscarPorIdRespuestaAdministradoraSol");
    private final static QName _OpActualizarRespuestaAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpActualizarRespuestaAdministradoraFallo");
    private final static QName _OpActualizarRespuestaAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpActualizarRespuestaAdministradoraSol");
    private final static QName _OpCrearRespuestaAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpCrearRespuestaAdministradoraResp");
    private final static QName _OpBuscarPorIdRespuestaAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpBuscarPorIdRespuestaAdministradoraResp");
    private final static QName _OpCrearRespuestaAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpCrearRespuestaAdministradoraSol");
    private final static QName _OpBuscarPorIdRespuestaAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", "OpBuscarPorIdRespuestaAdministradoraFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarRespuestaAdministradoraRespTipo }
     * 
     */
    public OpActualizarRespuestaAdministradoraRespTipo createOpActualizarRespuestaAdministradoraRespTipo() {
        return new OpActualizarRespuestaAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRespuestaAdministradoraSolTipo }
     * 
     */
    public OpBuscarPorIdRespuestaAdministradoraSolTipo createOpBuscarPorIdRespuestaAdministradoraSolTipo() {
        return new OpBuscarPorIdRespuestaAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRespuestaAdministradoraSolTipo }
     * 
     */
    public OpActualizarRespuestaAdministradoraSolTipo createOpActualizarRespuestaAdministradoraSolTipo() {
        return new OpActualizarRespuestaAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRespuestaAdministradoraRespTipo }
     * 
     */
    public OpBuscarPorIdRespuestaAdministradoraRespTipo createOpBuscarPorIdRespuestaAdministradoraRespTipo() {
        return new OpBuscarPorIdRespuestaAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearRespuestaAdministradoraSolTipo }
     * 
     */
    public OpCrearRespuestaAdministradoraSolTipo createOpCrearRespuestaAdministradoraSolTipo() {
        return new OpCrearRespuestaAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearRespuestaAdministradoraRespTipo }
     * 
     */
    public OpCrearRespuestaAdministradoraRespTipo createOpCrearRespuestaAdministradoraRespTipo() {
        return new OpCrearRespuestaAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpCrearRespuestaAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpCrearRespuestaAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearRespuestaAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRespuestaAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpActualizarRespuestaAdministradoraResp")
    public JAXBElement<OpActualizarRespuestaAdministradoraRespTipo> createOpActualizarRespuestaAdministradoraResp(OpActualizarRespuestaAdministradoraRespTipo value) {
        return new JAXBElement<OpActualizarRespuestaAdministradoraRespTipo>(_OpActualizarRespuestaAdministradoraResp_QNAME, OpActualizarRespuestaAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRespuestaAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpBuscarPorIdRespuestaAdministradoraSol")
    public JAXBElement<OpBuscarPorIdRespuestaAdministradoraSolTipo> createOpBuscarPorIdRespuestaAdministradoraSol(OpBuscarPorIdRespuestaAdministradoraSolTipo value) {
        return new JAXBElement<OpBuscarPorIdRespuestaAdministradoraSolTipo>(_OpBuscarPorIdRespuestaAdministradoraSol_QNAME, OpBuscarPorIdRespuestaAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpActualizarRespuestaAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpActualizarRespuestaAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarRespuestaAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRespuestaAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpActualizarRespuestaAdministradoraSol")
    public JAXBElement<OpActualizarRespuestaAdministradoraSolTipo> createOpActualizarRespuestaAdministradoraSol(OpActualizarRespuestaAdministradoraSolTipo value) {
        return new JAXBElement<OpActualizarRespuestaAdministradoraSolTipo>(_OpActualizarRespuestaAdministradoraSol_QNAME, OpActualizarRespuestaAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRespuestaAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpCrearRespuestaAdministradoraResp")
    public JAXBElement<OpCrearRespuestaAdministradoraRespTipo> createOpCrearRespuestaAdministradoraResp(OpCrearRespuestaAdministradoraRespTipo value) {
        return new JAXBElement<OpCrearRespuestaAdministradoraRespTipo>(_OpCrearRespuestaAdministradoraResp_QNAME, OpCrearRespuestaAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRespuestaAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpBuscarPorIdRespuestaAdministradoraResp")
    public JAXBElement<OpBuscarPorIdRespuestaAdministradoraRespTipo> createOpBuscarPorIdRespuestaAdministradoraResp(OpBuscarPorIdRespuestaAdministradoraRespTipo value) {
        return new JAXBElement<OpBuscarPorIdRespuestaAdministradoraRespTipo>(_OpBuscarPorIdRespuestaAdministradoraResp_QNAME, OpBuscarPorIdRespuestaAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRespuestaAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpCrearRespuestaAdministradoraSol")
    public JAXBElement<OpCrearRespuestaAdministradoraSolTipo> createOpCrearRespuestaAdministradoraSol(OpCrearRespuestaAdministradoraSolTipo value) {
        return new JAXBElement<OpCrearRespuestaAdministradoraSolTipo>(_OpCrearRespuestaAdministradoraSol_QNAME, OpCrearRespuestaAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1", name = "OpBuscarPorIdRespuestaAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdRespuestaAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdRespuestaAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

}
