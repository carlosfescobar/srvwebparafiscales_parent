
package co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1.RespuestaAdministradoraTipo;


/**
 * <p>Clase Java para OpActualizarRespuestaAdministradoraSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarRespuestaAdministradoraSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="respuestaAdministradora" type="{http://www.ugpp.gov.co/schema/Administradora/RespuestaAdministradoraTipo/v1}RespuestaAdministradoraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarRespuestaAdministradoraSolTipo", propOrder = {
    "contextoTransaccional",
    "respuestaAdministradora"
})
public class OpActualizarRespuestaAdministradoraSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected RespuestaAdministradoraTipo respuestaAdministradora;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad respuestaAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaAdministradoraTipo }
     *     
     */
    public RespuestaAdministradoraTipo getRespuestaAdministradora() {
        return respuestaAdministradora;
    }

    /**
     * Define el valor de la propiedad respuestaAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaAdministradoraTipo }
     *     
     */
    public void setRespuestaAdministradora(RespuestaAdministradoraTipo value) {
        this.respuestaAdministradora = value;
    }

}
