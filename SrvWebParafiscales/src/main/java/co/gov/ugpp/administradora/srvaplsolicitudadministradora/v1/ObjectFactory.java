
package co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearSolicitudAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpCrearSolicitudAdministradoraFallo");
    private final static QName _OpBuscarPorCriteriosSolicitudAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorCriteriosSolicitudAdministradoraSol");
    private final static QName _OpCrearSolicitudAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpCrearSolicitudAdministradoraResp");
    private final static QName _OpActualizarSolicitudAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpActualizarSolicitudAdministradoraResp");
    private final static QName _OpActualizarSolicitudAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpActualizarSolicitudAdministradoraSol");
    private final static QName _OpBuscarPorCriteriosSolicitudAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorCriteriosSolicitudAdministradoraFallo");
    private final static QName _OpBuscarPorIdSolicitudAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorIdSolicitudAdministradoraSol");
    private final static QName _OpCrearSolicitudAdministradoraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpCrearSolicitudAdministradoraSol");
    private final static QName _OpBuscarPorIdSolicitudAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorIdSolicitudAdministradoraResp");
    private final static QName _OpBuscarPorIdSolicitudAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorIdSolicitudAdministradoraFallo");
    private final static QName _OpActualizarSolicitudAdministradoraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpActualizarSolicitudAdministradoraFallo");
    private final static QName _OpBuscarPorCriteriosSolicitudAdministradoraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", "OpBuscarPorCriteriosSolicitudAdministradoraResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudAdministradoraSolTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudAdministradoraSolTipo createOpBuscarPorCriteriosSolicitudAdministradoraSolTipo() {
        return new OpBuscarPorCriteriosSolicitudAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudAdministradoraRespTipo }
     * 
     */
    public OpActualizarSolicitudAdministradoraRespTipo createOpActualizarSolicitudAdministradoraRespTipo() {
        return new OpActualizarSolicitudAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudAdministradoraRespTipo }
     * 
     */
    public OpCrearSolicitudAdministradoraRespTipo createOpCrearSolicitudAdministradoraRespTipo() {
        return new OpCrearSolicitudAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudAdministradoraSolTipo }
     * 
     */
    public OpBuscarPorIdSolicitudAdministradoraSolTipo createOpBuscarPorIdSolicitudAdministradoraSolTipo() {
        return new OpBuscarPorIdSolicitudAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudAdministradoraSolTipo }
     * 
     */
    public OpActualizarSolicitudAdministradoraSolTipo createOpActualizarSolicitudAdministradoraSolTipo() {
        return new OpActualizarSolicitudAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudAdministradoraRespTipo }
     * 
     */
    public OpBuscarPorIdSolicitudAdministradoraRespTipo createOpBuscarPorIdSolicitudAdministradoraRespTipo() {
        return new OpBuscarPorIdSolicitudAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudAdministradoraSolTipo }
     * 
     */
    public OpCrearSolicitudAdministradoraSolTipo createOpCrearSolicitudAdministradoraSolTipo() {
        return new OpCrearSolicitudAdministradoraSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudAdministradoraRespTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudAdministradoraRespTipo createOpBuscarPorCriteriosSolicitudAdministradoraRespTipo() {
        return new OpBuscarPorCriteriosSolicitudAdministradoraRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpCrearSolicitudAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpCrearSolicitudAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSolicitudAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorCriteriosSolicitudAdministradoraSol")
    public JAXBElement<OpBuscarPorCriteriosSolicitudAdministradoraSolTipo> createOpBuscarPorCriteriosSolicitudAdministradoraSol(OpBuscarPorCriteriosSolicitudAdministradoraSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudAdministradoraSolTipo>(_OpBuscarPorCriteriosSolicitudAdministradoraSol_QNAME, OpBuscarPorCriteriosSolicitudAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpCrearSolicitudAdministradoraResp")
    public JAXBElement<OpCrearSolicitudAdministradoraRespTipo> createOpCrearSolicitudAdministradoraResp(OpCrearSolicitudAdministradoraRespTipo value) {
        return new JAXBElement<OpCrearSolicitudAdministradoraRespTipo>(_OpCrearSolicitudAdministradoraResp_QNAME, OpCrearSolicitudAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpActualizarSolicitudAdministradoraResp")
    public JAXBElement<OpActualizarSolicitudAdministradoraRespTipo> createOpActualizarSolicitudAdministradoraResp(OpActualizarSolicitudAdministradoraRespTipo value) {
        return new JAXBElement<OpActualizarSolicitudAdministradoraRespTipo>(_OpActualizarSolicitudAdministradoraResp_QNAME, OpActualizarSolicitudAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpActualizarSolicitudAdministradoraSol")
    public JAXBElement<OpActualizarSolicitudAdministradoraSolTipo> createOpActualizarSolicitudAdministradoraSol(OpActualizarSolicitudAdministradoraSolTipo value) {
        return new JAXBElement<OpActualizarSolicitudAdministradoraSolTipo>(_OpActualizarSolicitudAdministradoraSol_QNAME, OpActualizarSolicitudAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorCriteriosSolicitudAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosSolicitudAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosSolicitudAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorIdSolicitudAdministradoraSol")
    public JAXBElement<OpBuscarPorIdSolicitudAdministradoraSolTipo> createOpBuscarPorIdSolicitudAdministradoraSol(OpBuscarPorIdSolicitudAdministradoraSolTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudAdministradoraSolTipo>(_OpBuscarPorIdSolicitudAdministradoraSol_QNAME, OpBuscarPorIdSolicitudAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudAdministradoraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpCrearSolicitudAdministradoraSol")
    public JAXBElement<OpCrearSolicitudAdministradoraSolTipo> createOpCrearSolicitudAdministradoraSol(OpCrearSolicitudAdministradoraSolTipo value) {
        return new JAXBElement<OpCrearSolicitudAdministradoraSolTipo>(_OpCrearSolicitudAdministradoraSol_QNAME, OpCrearSolicitudAdministradoraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorIdSolicitudAdministradoraResp")
    public JAXBElement<OpBuscarPorIdSolicitudAdministradoraRespTipo> createOpBuscarPorIdSolicitudAdministradoraResp(OpBuscarPorIdSolicitudAdministradoraRespTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudAdministradoraRespTipo>(_OpBuscarPorIdSolicitudAdministradoraResp_QNAME, OpBuscarPorIdSolicitudAdministradoraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorIdSolicitudAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdSolicitudAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdSolicitudAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpActualizarSolicitudAdministradoraFallo")
    public JAXBElement<FalloTipo> createOpActualizarSolicitudAdministradoraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSolicitudAdministradoraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudAdministradoraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1", name = "OpBuscarPorCriteriosSolicitudAdministradoraResp")
    public JAXBElement<OpBuscarPorCriteriosSolicitudAdministradoraRespTipo> createOpBuscarPorCriteriosSolicitudAdministradoraResp(OpBuscarPorCriteriosSolicitudAdministradoraRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudAdministradoraRespTipo>(_OpBuscarPorCriteriosSolicitudAdministradoraResp_QNAME, OpBuscarPorCriteriosSolicitudAdministradoraRespTipo.class, null, value);
    }

}
