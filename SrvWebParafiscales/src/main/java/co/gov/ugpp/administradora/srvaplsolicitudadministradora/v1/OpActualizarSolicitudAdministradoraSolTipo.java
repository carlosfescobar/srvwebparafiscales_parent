
package co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1.SolicitudAdministradoraTipo;


/**
 * <p>Clase Java para OpActualizarSolicitudAdministradoraSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarSolicitudAdministradoraSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="solicitudAdministradora" type="{http://www.ugpp.gov.co/schema/Administradora/SolicitudAdministradoraTipo/v1}SolicitudAdministradoraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarSolicitudAdministradoraSolTipo", propOrder = {
    "contextoTransaccional",
    "solicitudAdministradora"
})
public class OpActualizarSolicitudAdministradoraSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected SolicitudAdministradoraTipo solicitudAdministradora;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudAdministradoraTipo }
     *     
     */
    public SolicitudAdministradoraTipo getSolicitudAdministradora() {
        return solicitudAdministradora;
    }

    /**
     * Define el valor de la propiedad solicitudAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudAdministradoraTipo }
     *     
     */
    public void setSolicitudAdministradora(SolicitudAdministradoraTipo value) {
        this.solicitudAdministradora = value;
    }

}
