
package co.gov.ugpp.administradora.srvaplsubsistema.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplsubsistema.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorCriteriosSubsistemaSol");
    private final static QName _OpActualizarSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpActualizarSubsistemaSol");
    private final static QName _OpActualizarSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpActualizarSubsistemaFallo");
    private final static QName _OpBuscarPorIdSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorIdSubsistemaFallo");
    private final static QName _OpBuscarPorIdSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorIdSubsistemaSol");
    private final static QName _OpBuscarPorCriteriosSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorCriteriosSubsistemaResp");
    private final static QName _OpActualizarSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpActualizarSubsistemaResp");
    private final static QName _OpBuscarPorCriteriosSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorCriteriosSubsistemaFallo");
    private final static QName _OpCrearSubsistemaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpCrearSubsistemaSol");
    private final static QName _OpBuscarPorIdSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpBuscarPorIdSubsistemaResp");
    private final static QName _OpCrearSubsistemaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpCrearSubsistemaResp");
    private final static QName _OpCrearSubsistemaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", "OpCrearSubsistemaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplsubsistema.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarSubsistemaSolTipo }
     * 
     */
    public OpActualizarSubsistemaSolTipo createOpActualizarSubsistemaSolTipo() {
        return new OpActualizarSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSubsistemaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosSubsistemaSolTipo createOpBuscarPorCriteriosSubsistemaSolTipo() {
        return new OpBuscarPorCriteriosSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSubsistemaRespTipo }
     * 
     */
    public OpBuscarPorIdSubsistemaRespTipo createOpBuscarPorIdSubsistemaRespTipo() {
        return new OpBuscarPorIdSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSubsistemaSolTipo }
     * 
     */
    public OpCrearSubsistemaSolTipo createOpCrearSubsistemaSolTipo() {
        return new OpCrearSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearSubsistemaRespTipo }
     * 
     */
    public OpCrearSubsistemaRespTipo createOpCrearSubsistemaRespTipo() {
        return new OpCrearSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSubsistemaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosSubsistemaRespTipo createOpBuscarPorCriteriosSubsistemaRespTipo() {
        return new OpBuscarPorCriteriosSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSubsistemaSolTipo }
     * 
     */
    public OpBuscarPorIdSubsistemaSolTipo createOpBuscarPorIdSubsistemaSolTipo() {
        return new OpBuscarPorIdSubsistemaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSubsistemaRespTipo }
     * 
     */
    public OpActualizarSubsistemaRespTipo createOpActualizarSubsistemaRespTipo() {
        return new OpActualizarSubsistemaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorCriteriosSubsistemaSol")
    public JAXBElement<OpBuscarPorCriteriosSubsistemaSolTipo> createOpBuscarPorCriteriosSubsistemaSol(OpBuscarPorCriteriosSubsistemaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSubsistemaSolTipo>(_OpBuscarPorCriteriosSubsistemaSol_QNAME, OpBuscarPorCriteriosSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpActualizarSubsistemaSol")
    public JAXBElement<OpActualizarSubsistemaSolTipo> createOpActualizarSubsistemaSol(OpActualizarSubsistemaSolTipo value) {
        return new JAXBElement<OpActualizarSubsistemaSolTipo>(_OpActualizarSubsistemaSol_QNAME, OpActualizarSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpActualizarSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpActualizarSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorIdSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorIdSubsistemaSol")
    public JAXBElement<OpBuscarPorIdSubsistemaSolTipo> createOpBuscarPorIdSubsistemaSol(OpBuscarPorIdSubsistemaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdSubsistemaSolTipo>(_OpBuscarPorIdSubsistemaSol_QNAME, OpBuscarPorIdSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorCriteriosSubsistemaResp")
    public JAXBElement<OpBuscarPorCriteriosSubsistemaRespTipo> createOpBuscarPorCriteriosSubsistemaResp(OpBuscarPorCriteriosSubsistemaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSubsistemaRespTipo>(_OpBuscarPorCriteriosSubsistemaResp_QNAME, OpBuscarPorCriteriosSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpActualizarSubsistemaResp")
    public JAXBElement<OpActualizarSubsistemaRespTipo> createOpActualizarSubsistemaResp(OpActualizarSubsistemaRespTipo value) {
        return new JAXBElement<OpActualizarSubsistemaRespTipo>(_OpActualizarSubsistemaResp_QNAME, OpActualizarSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorCriteriosSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSubsistemaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpCrearSubsistemaSol")
    public JAXBElement<OpCrearSubsistemaSolTipo> createOpCrearSubsistemaSol(OpCrearSubsistemaSolTipo value) {
        return new JAXBElement<OpCrearSubsistemaSolTipo>(_OpCrearSubsistemaSol_QNAME, OpCrearSubsistemaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpBuscarPorIdSubsistemaResp")
    public JAXBElement<OpBuscarPorIdSubsistemaRespTipo> createOpBuscarPorIdSubsistemaResp(OpBuscarPorIdSubsistemaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdSubsistemaRespTipo>(_OpBuscarPorIdSubsistemaResp_QNAME, OpBuscarPorIdSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSubsistemaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpCrearSubsistemaResp")
    public JAXBElement<OpCrearSubsistemaRespTipo> createOpCrearSubsistemaResp(OpCrearSubsistemaRespTipo value) {
        return new JAXBElement<OpCrearSubsistemaRespTipo>(_OpCrearSubsistemaResp_QNAME, OpCrearSubsistemaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1", name = "OpCrearSubsistemaFallo")
    public JAXBElement<FalloTipo> createOpCrearSubsistemaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSubsistemaFallo_QNAME, FalloTipo.class, null, value);
    }

}
