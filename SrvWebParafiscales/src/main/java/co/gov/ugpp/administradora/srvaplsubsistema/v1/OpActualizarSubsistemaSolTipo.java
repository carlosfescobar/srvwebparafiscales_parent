
package co.gov.ugpp.administradora.srvaplsubsistema.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;


/**
 * <p>Clase Java para OpActualizarSubsistemaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarSubsistemaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="subsistema" type="{http://www.ugpp.gov.co/schema/Administradora/SubsistemaTipo/v1}SubsistemaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarSubsistemaSolTipo", propOrder = {
    "contextoTransaccional",
    "subsistema"
})
public class OpActualizarSubsistemaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected SubsistemaTipo subsistema;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad subsistema.
     * 
     * @return
     *     possible object is
     *     {@link SubsistemaTipo }
     *     
     */
    public SubsistemaTipo getSubsistema() {
        return subsistema;
    }

    /**
     * Define el valor de la propiedad subsistema.
     * 
     * @param value
     *     allowed object is
     *     {@link SubsistemaTipo }
     *     
     */
    public void setSubsistema(SubsistemaTipo value) {
        this.subsistema = value;
    }

}
