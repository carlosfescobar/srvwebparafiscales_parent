
package co.gov.ugpp.administradora.srvaplvalidacionestructura.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradora.srvaplvalidacionestructura.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdValidacionEstructuraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpBuscarPorIdValidacionEstructuraFallo");
    private final static QName _OpCrearValidacionEstructuraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpCrearValidacionEstructuraFallo");
    private final static QName _OpBuscarPorIdValidacionEstructuraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpBuscarPorIdValidacionEstructuraResp");
    private final static QName _OpActualizarValidacionEstructuraFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpActualizarValidacionEstructuraFallo");
    private final static QName _OpActualizarValidacionEstructuraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpActualizarValidacionEstructuraResp");
    private final static QName _OpActualizarValidacionEstructuraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpActualizarValidacionEstructuraSol");
    private final static QName _OpBuscarPorIdValidacionEstructuraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpBuscarPorIdValidacionEstructuraSol");
    private final static QName _OpCrearValidacionEstructuraSol_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpCrearValidacionEstructuraSol");
    private final static QName _OpCrearValidacionEstructuraResp_QNAME = new QName("http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", "OpCrearValidacionEstructuraResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradora.srvaplvalidacionestructura.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarValidacionEstructuraRespTipo }
     * 
     */
    public OpActualizarValidacionEstructuraRespTipo createOpActualizarValidacionEstructuraRespTipo() {
        return new OpActualizarValidacionEstructuraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdValidacionEstructuraSolTipo }
     * 
     */
    public OpBuscarPorIdValidacionEstructuraSolTipo createOpBuscarPorIdValidacionEstructuraSolTipo() {
        return new OpBuscarPorIdValidacionEstructuraSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarValidacionEstructuraSolTipo }
     * 
     */
    public OpActualizarValidacionEstructuraSolTipo createOpActualizarValidacionEstructuraSolTipo() {
        return new OpActualizarValidacionEstructuraSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearValidacionEstructuraRespTipo }
     * 
     */
    public OpCrearValidacionEstructuraRespTipo createOpCrearValidacionEstructuraRespTipo() {
        return new OpCrearValidacionEstructuraRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearValidacionEstructuraSolTipo }
     * 
     */
    public OpCrearValidacionEstructuraSolTipo createOpCrearValidacionEstructuraSolTipo() {
        return new OpCrearValidacionEstructuraSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdValidacionEstructuraRespTipo }
     * 
     */
    public OpBuscarPorIdValidacionEstructuraRespTipo createOpBuscarPorIdValidacionEstructuraRespTipo() {
        return new OpBuscarPorIdValidacionEstructuraRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpBuscarPorIdValidacionEstructuraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdValidacionEstructuraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdValidacionEstructuraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpCrearValidacionEstructuraFallo")
    public JAXBElement<FalloTipo> createOpCrearValidacionEstructuraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearValidacionEstructuraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdValidacionEstructuraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpBuscarPorIdValidacionEstructuraResp")
    public JAXBElement<OpBuscarPorIdValidacionEstructuraRespTipo> createOpBuscarPorIdValidacionEstructuraResp(OpBuscarPorIdValidacionEstructuraRespTipo value) {
        return new JAXBElement<OpBuscarPorIdValidacionEstructuraRespTipo>(_OpBuscarPorIdValidacionEstructuraResp_QNAME, OpBuscarPorIdValidacionEstructuraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpActualizarValidacionEstructuraFallo")
    public JAXBElement<FalloTipo> createOpActualizarValidacionEstructuraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarValidacionEstructuraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarValidacionEstructuraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpActualizarValidacionEstructuraResp")
    public JAXBElement<OpActualizarValidacionEstructuraRespTipo> createOpActualizarValidacionEstructuraResp(OpActualizarValidacionEstructuraRespTipo value) {
        return new JAXBElement<OpActualizarValidacionEstructuraRespTipo>(_OpActualizarValidacionEstructuraResp_QNAME, OpActualizarValidacionEstructuraRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarValidacionEstructuraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpActualizarValidacionEstructuraSol")
    public JAXBElement<OpActualizarValidacionEstructuraSolTipo> createOpActualizarValidacionEstructuraSol(OpActualizarValidacionEstructuraSolTipo value) {
        return new JAXBElement<OpActualizarValidacionEstructuraSolTipo>(_OpActualizarValidacionEstructuraSol_QNAME, OpActualizarValidacionEstructuraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdValidacionEstructuraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpBuscarPorIdValidacionEstructuraSol")
    public JAXBElement<OpBuscarPorIdValidacionEstructuraSolTipo> createOpBuscarPorIdValidacionEstructuraSol(OpBuscarPorIdValidacionEstructuraSolTipo value) {
        return new JAXBElement<OpBuscarPorIdValidacionEstructuraSolTipo>(_OpBuscarPorIdValidacionEstructuraSol_QNAME, OpBuscarPorIdValidacionEstructuraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearValidacionEstructuraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpCrearValidacionEstructuraSol")
    public JAXBElement<OpCrearValidacionEstructuraSolTipo> createOpCrearValidacionEstructuraSol(OpCrearValidacionEstructuraSolTipo value) {
        return new JAXBElement<OpCrearValidacionEstructuraSolTipo>(_OpCrearValidacionEstructuraSol_QNAME, OpCrearValidacionEstructuraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearValidacionEstructuraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1", name = "OpCrearValidacionEstructuraResp")
    public JAXBElement<OpCrearValidacionEstructuraRespTipo> createOpCrearValidacionEstructuraResp(OpCrearValidacionEstructuraRespTipo value) {
        return new JAXBElement<OpCrearValidacionEstructuraRespTipo>(_OpCrearValidacionEstructuraResp_QNAME, OpCrearValidacionEstructuraRespTipo.class, null, value);
    }

}
