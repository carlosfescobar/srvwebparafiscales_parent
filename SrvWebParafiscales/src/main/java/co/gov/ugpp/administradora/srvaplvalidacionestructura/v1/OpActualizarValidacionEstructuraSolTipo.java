
package co.gov.ugpp.administradora.srvaplvalidacionestructura.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;


/**
 * <p>Clase Java para OpActualizarValidacionEstructuraSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarValidacionEstructuraSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="validacionEstructura" type="{http://www.ugpp.gov.co/schema/Administradora/ValidacionEstructuraTipo/v1}ValidacionEstructuraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarValidacionEstructuraSolTipo", propOrder = {
    "contextoTransaccional",
    "validacionEstructura"
})
public class OpActualizarValidacionEstructuraSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ValidacionEstructuraTipo validacionEstructura;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad validacionEstructura.
     * 
     * @return
     *     possible object is
     *     {@link ValidacionEstructuraTipo }
     *     
     */
    public ValidacionEstructuraTipo getValidacionEstructura() {
        return validacionEstructura;
    }

    /**
     * Define el valor de la propiedad validacionEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidacionEstructuraTipo }
     *     
     */
    public void setValidacionEstructura(ValidacionEstructuraTipo value) {
        this.validacionEstructura = value;
    }

}
