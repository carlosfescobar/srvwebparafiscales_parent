
package co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosSolicitudEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpBuscarPorCriteriosSolicitudEntidadExternaSol");
    private final static QName _OpRecibirSolicitudEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpRecibirSolicitudEntidadExternaFallo");
    private final static QName _OpActualizarSolicitudEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpActualizarSolicitudEntidadExternaFallo");
    private final static QName _OpRecibirSolicitudEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpRecibirSolicitudEntidadExternaSol");
    private final static QName _OpBuscarPorCriteriosSolicitudEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpBuscarPorCriteriosSolicitudEntidadExternaResp");
    private final static QName _OpActualizarSolicitudEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpActualizarSolicitudEntidadExternaResp");
    private final static QName _OpRecibirSolicitudEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpRecibirSolicitudEntidadExternaResp");
    private final static QName _OpBuscarPorCriteriosSolicitudEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpBuscarPorCriteriosSolicitudEntidadExternaFallo");
    private final static QName _OpActualizarSolicitudEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", "OpActualizarSolicitudEntidadExternaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudEntidadExternaSolTipo }
     * 
     */
    public OpActualizarSolicitudEntidadExternaSolTipo createOpActualizarSolicitudEntidadExternaSolTipo() {
        return new OpActualizarSolicitudEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo createOpBuscarPorCriteriosSolicitudEntidadExternaSolTipo() {
        return new OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link OpRecibirSolicitudEntidadExternaSolTipo }
     * 
     */
    public OpRecibirSolicitudEntidadExternaSolTipo createOpRecibirSolicitudEntidadExternaSolTipo() {
        return new OpRecibirSolicitudEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo createOpBuscarPorCriteriosSolicitudEntidadExternaRespTipo() {
        return new OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link OpRecibirSolicitudEntidadExternaRespTipo }
     * 
     */
    public OpRecibirSolicitudEntidadExternaRespTipo createOpRecibirSolicitudEntidadExternaRespTipo() {
        return new OpRecibirSolicitudEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudEntidadExternaRespTipo }
     * 
     */
    public OpActualizarSolicitudEntidadExternaRespTipo createOpActualizarSolicitudEntidadExternaRespTipo() {
        return new OpActualizarSolicitudEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpBuscarPorCriteriosSolicitudEntidadExternaSol")
    public JAXBElement<OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo> createOpBuscarPorCriteriosSolicitudEntidadExternaSol(OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo>(_OpBuscarPorCriteriosSolicitudEntidadExternaSol_QNAME, OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpRecibirSolicitudEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpRecibirSolicitudEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRecibirSolicitudEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpActualizarSolicitudEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpActualizarSolicitudEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSolicitudEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirSolicitudEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpRecibirSolicitudEntidadExternaSol")
    public JAXBElement<OpRecibirSolicitudEntidadExternaSolTipo> createOpRecibirSolicitudEntidadExternaSol(OpRecibirSolicitudEntidadExternaSolTipo value) {
        return new JAXBElement<OpRecibirSolicitudEntidadExternaSolTipo>(_OpRecibirSolicitudEntidadExternaSol_QNAME, OpRecibirSolicitudEntidadExternaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpBuscarPorCriteriosSolicitudEntidadExternaResp")
    public JAXBElement<OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo> createOpBuscarPorCriteriosSolicitudEntidadExternaResp(OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo>(_OpBuscarPorCriteriosSolicitudEntidadExternaResp_QNAME, OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpActualizarSolicitudEntidadExternaResp")
    public JAXBElement<OpActualizarSolicitudEntidadExternaRespTipo> createOpActualizarSolicitudEntidadExternaResp(OpActualizarSolicitudEntidadExternaRespTipo value) {
        return new JAXBElement<OpActualizarSolicitudEntidadExternaRespTipo>(_OpActualizarSolicitudEntidadExternaResp_QNAME, OpActualizarSolicitudEntidadExternaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirSolicitudEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpRecibirSolicitudEntidadExternaResp")
    public JAXBElement<OpRecibirSolicitudEntidadExternaRespTipo> createOpRecibirSolicitudEntidadExternaResp(OpRecibirSolicitudEntidadExternaRespTipo value) {
        return new JAXBElement<OpRecibirSolicitudEntidadExternaRespTipo>(_OpRecibirSolicitudEntidadExternaResp_QNAME, OpRecibirSolicitudEntidadExternaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpBuscarPorCriteriosSolicitudEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosSolicitudEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosSolicitudEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1", name = "OpActualizarSolicitudEntidadExternaSol")
    public JAXBElement<OpActualizarSolicitudEntidadExternaSolTipo> createOpActualizarSolicitudEntidadExternaSol(OpActualizarSolicitudEntidadExternaSolTipo value) {
        return new JAXBElement<OpActualizarSolicitudEntidadExternaSolTipo>(_OpActualizarSolicitudEntidadExternaSol_QNAME, OpActualizarSolicitudEntidadExternaSolTipo.class, null, value);
    }

}
