
package co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;


/**
 * <p>Clase Java para OpActualizarSolicitudEntidadExternaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarSolicitudEntidadExternaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="solicitudEntidadExterna" type="{http://www.ugpp.gov.co/schema/Administradoras/SolicitudEntidadExternaTipo/v1}SolicitudEntidadExternaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarSolicitudEntidadExternaSolTipo", propOrder = {
    "contextoTransaccional",
    "solicitudEntidadExterna"
})
public class OpActualizarSolicitudEntidadExternaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected SolicitudEntidadExternaTipo solicitudEntidadExterna;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudEntidadExternaTipo }
     *     
     */
    public SolicitudEntidadExternaTipo getSolicitudEntidadExterna() {
        return solicitudEntidadExterna;
    }

    /**
     * Define el valor de la propiedad solicitudEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudEntidadExternaTipo }
     *     
     */
    public void setSolicitudEntidadExterna(SolicitudEntidadExternaTipo value) {
        this.solicitudEntidadExterna = value;
    }

}
