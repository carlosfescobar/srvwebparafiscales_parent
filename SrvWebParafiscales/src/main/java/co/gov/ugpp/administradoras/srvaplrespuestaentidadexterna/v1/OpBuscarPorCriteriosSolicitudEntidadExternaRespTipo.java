
package co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;


/**
 * <p>Clase Java para OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="solicitudEntidadExterna" type="{http://www.ugpp.gov.co/schema/Administradoras/SolicitudEntidadExternaTipo/v1}SolicitudEntidadExternaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo", propOrder = {
    "contextoRespuesta",
    "solicitudEntidadExterna"
})
public class OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected List<SolicitudEntidadExternaTipo> solicitudEntidadExterna;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Gets the value of the solicitudEntidadExterna property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudEntidadExterna property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudEntidadExterna().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudEntidadExternaTipo }
     * 
     * 
     */
    public List<SolicitudEntidadExternaTipo> getSolicitudEntidadExterna() {
        if (solicitudEntidadExterna == null) {
            solicitudEntidadExterna = new ArrayList<SolicitudEntidadExternaTipo>();
        }
        return this.solicitudEntidadExterna;
    }

}
