
package co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;


/**
 * <p>Clase Java para OpRecibirSolicitudEntidadExternaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpRecibirSolicitudEntidadExternaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="solicitudEntidadExterna" type="{http://www.ugpp.gov.co/schema/Administradoras/SolicitudEntidadExternaTipo/v1}SolicitudEntidadExternaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpRecibirSolicitudEntidadExternaSolTipo", propOrder = {
    "contextoTransaccional",
    "solicitudEntidadExterna"
})
public class OpRecibirSolicitudEntidadExternaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    protected List<SolicitudEntidadExternaTipo> solicitudEntidadExterna;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Gets the value of the solicitudEntidadExterna property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudEntidadExterna property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudEntidadExterna().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudEntidadExternaTipo }
     * 
     * 
     */
    public List<SolicitudEntidadExternaTipo> getSolicitudEntidadExterna() {
        if (solicitudEntidadExterna == null) {
            solicitudEntidadExterna = new ArrayList<SolicitudEntidadExternaTipo>();
        }
        return this.solicitudEntidadExterna;
    }

}
