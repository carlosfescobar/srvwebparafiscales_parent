
package co.gov.ugpp.administradoras.srvaplseguimiento.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.administradoras.srvaplseguimiento.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearSeguimientoFallo_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", "OpCrearSeguimientoFallo");
    private final static QName _OpCrearSeguimientoResp_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", "OpCrearSeguimientoResp");
    private final static QName _OpCrearSeguimientoSol_QNAME = new QName("http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", "OpCrearSeguimientoSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.administradoras.srvaplseguimiento.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearSeguimientoRespTipo }
     * 
     */
    public OpCrearSeguimientoRespTipo createOpCrearSeguimientoRespTipo() {
        return new OpCrearSeguimientoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSeguimientoSolTipo }
     * 
     */
    public OpCrearSeguimientoSolTipo createOpCrearSeguimientoSolTipo() {
        return new OpCrearSeguimientoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", name = "OpCrearSeguimientoFallo")
    public JAXBElement<FalloTipo> createOpCrearSeguimientoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSeguimientoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSeguimientoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", name = "OpCrearSeguimientoResp")
    public JAXBElement<OpCrearSeguimientoRespTipo> createOpCrearSeguimientoResp(OpCrearSeguimientoRespTipo value) {
        return new JAXBElement<OpCrearSeguimientoRespTipo>(_OpCrearSeguimientoResp_QNAME, OpCrearSeguimientoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSeguimientoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1", name = "OpCrearSeguimientoSol")
    public JAXBElement<OpCrearSeguimientoSolTipo> createOpCrearSeguimientoSol(OpCrearSeguimientoSolTipo value) {
        return new JAXBElement<OpCrearSeguimientoSolTipo>(_OpCrearSeguimientoSol_QNAME, OpCrearSeguimientoSolTipo.class, null, value);
    }

}
