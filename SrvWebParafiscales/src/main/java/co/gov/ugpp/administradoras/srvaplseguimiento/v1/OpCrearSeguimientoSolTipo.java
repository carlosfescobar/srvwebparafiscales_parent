
package co.gov.ugpp.administradoras.srvaplseguimiento.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;


/**
 * <p>Clase Java para OpCrearSeguimientoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearSeguimientoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="Seguimiento" type="{http://www.ugpp.gov.co/schema/Administradoras/SeguimientoTipo/v1}SeguimientoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearSeguimientoSolTipo", propOrder = {
    "contextoTransaccional",
    "seguimiento"
})
public class OpCrearSeguimientoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(name = "Seguimiento", required = true)
    protected SeguimientoTipo seguimiento;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad seguimiento.
     * 
     * @return
     *     possible object is
     *     {@link SeguimientoTipo }
     *     
     */
    public SeguimientoTipo getSeguimiento() {
        return seguimiento;
    }

    /**
     * Define el valor de la propiedad seguimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link SeguimientoTipo }
     *     
     */
    public void setSeguimiento(SeguimientoTipo value) {
        this.seguimiento = value;
    }

}
