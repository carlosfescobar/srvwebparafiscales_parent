
package co.gov.ugpp.comun.srvaplconceptopersona.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.comun.srvaplconceptopersona.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdPersonaSol_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", "OpBuscarPorIdPersonaSol");
    private final static QName _OpBuscarPorIdPersonaResp_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", "OpBuscarPorIdPersonaResp");
    private final static QName _OpBuscarPorIdPersonaFallo_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", "OpBuscarPorIdPersonaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.comun.srvaplconceptopersona.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaRespTipo }
     * 
     */
    public OpBuscarPorIdPersonaRespTipo createOpBuscarPorIdPersonaRespTipo() {
        return new OpBuscarPorIdPersonaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaSolTipo }
     * 
     */
    public OpBuscarPorIdPersonaSolTipo createOpBuscarPorIdPersonaSolTipo() {
        return new OpBuscarPorIdPersonaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", name = "OpBuscarPorIdPersonaSol")
    public JAXBElement<OpBuscarPorIdPersonaSolTipo> createOpBuscarPorIdPersonaSol(OpBuscarPorIdPersonaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaSolTipo>(_OpBuscarPorIdPersonaSol_QNAME, OpBuscarPorIdPersonaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", name = "OpBuscarPorIdPersonaResp")
    public JAXBElement<OpBuscarPorIdPersonaRespTipo> createOpBuscarPorIdPersonaResp(OpBuscarPorIdPersonaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaRespTipo>(_OpBuscarPorIdPersonaResp_QNAME, OpBuscarPorIdPersonaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1", name = "OpBuscarPorIdPersonaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdPersonaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdPersonaFallo_QNAME, FalloTipo.class, null, value);
    }

}
