
package co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpRegistrarControlTrazabilidadTecnicaFallo_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", "OpRegistrarControlTrazabilidadTecnicaFallo");
    private final static QName _OpRegistrarControlTrazabilidadTecnicaResp_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", "OpRegistrarControlTrazabilidadTecnicaResp");
    private final static QName _OpRegistrarControlTrazabilidadTecnicaSol_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", "OpRegistrarControlTrazabilidadTecnicaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpRegistrarControlTrazabilidadTecnicaSolTipo }
     * 
     */
    public OpRegistrarControlTrazabilidadTecnicaSolTipo createOpRegistrarControlTrazabilidadTecnicaSolTipo() {
        return new OpRegistrarControlTrazabilidadTecnicaSolTipo();
    }

    /**
     * Create an instance of {@link OpRegistrarControlTrazabilidadTecnicaRespTipo }
     * 
     */
    public OpRegistrarControlTrazabilidadTecnicaRespTipo createOpRegistrarControlTrazabilidadTecnicaRespTipo() {
        return new OpRegistrarControlTrazabilidadTecnicaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", name = "OpRegistrarControlTrazabilidadTecnicaFallo")
    public JAXBElement<FalloTipo> createOpRegistrarControlTrazabilidadTecnicaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRegistrarControlTrazabilidadTecnicaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRegistrarControlTrazabilidadTecnicaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", name = "OpRegistrarControlTrazabilidadTecnicaResp")
    public JAXBElement<OpRegistrarControlTrazabilidadTecnicaRespTipo> createOpRegistrarControlTrazabilidadTecnicaResp(OpRegistrarControlTrazabilidadTecnicaRespTipo value) {
        return new JAXBElement<OpRegistrarControlTrazabilidadTecnicaRespTipo>(_OpRegistrarControlTrazabilidadTecnicaResp_QNAME, OpRegistrarControlTrazabilidadTecnicaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRegistrarControlTrazabilidadTecnicaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1", name = "OpRegistrarControlTrazabilidadTecnicaSol")
    public JAXBElement<OpRegistrarControlTrazabilidadTecnicaSolTipo> createOpRegistrarControlTrazabilidadTecnicaSol(OpRegistrarControlTrazabilidadTecnicaSolTipo value) {
        return new JAXBElement<OpRegistrarControlTrazabilidadTecnicaSolTipo>(_OpRegistrarControlTrazabilidadTecnicaSol_QNAME, OpRegistrarControlTrazabilidadTecnicaSolTipo.class, null, value);
    }

}
