
package co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.trazabilidadtecnicaprocesotipo.v1.TrazabilidadTecnicaProcesoTipo;


/**
 * <p>Clase Java para OpRegistrarControlTrazabilidadTecnicaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpRegistrarControlTrazabilidadTecnicaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="trazabilidadTecnicaProceso" type="{http://www.ugpp.gov.co/esb/schema/TrazabilidadTecnicaProcesoTipo/v1}TrazabilidadTecnicaProcesoTipo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpRegistrarControlTrazabilidadTecnicaSolTipo", propOrder = {
    "contextoTransaccional",
    "trazabilidadTecnicaProceso"
})
public class OpRegistrarControlTrazabilidadTecnicaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected List<TrazabilidadTecnicaProcesoTipo> trazabilidadTecnicaProceso;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Gets the value of the trazabilidadTecnicaProceso property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trazabilidadTecnicaProceso property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrazabilidadTecnicaProceso().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TrazabilidadTecnicaProcesoTipo }
     * 
     * 
     */
    public List<TrazabilidadTecnicaProcesoTipo> getTrazabilidadTecnicaProceso() {
        if (trazabilidadTecnicaProceso == null) {
            trazabilidadTecnicaProceso = new ArrayList<TrazabilidadTecnicaProcesoTipo>();
        }
        return this.trazabilidadTecnicaProceso;
    }

}
