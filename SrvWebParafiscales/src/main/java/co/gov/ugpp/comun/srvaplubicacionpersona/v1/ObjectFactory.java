
package co.gov.ugpp.comun.srvaplubicacionpersona.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.comun.srvaplubicacionpersona.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosUbicacionPersonaSol_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", "OpBuscarPorCriteriosUbicacionPersonaSol");
    private final static QName _OpBuscarPorCriteriosUbicacionPersonaResp_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", "OpBuscarPorCriteriosUbicacionPersonaResp");
    private final static QName _OpBuscarPorCriteriosUbicacionPersonaFallo_QNAME = new QName("http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", "OpBuscarPorCriteriosUbicacionPersonaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.comun.srvaplubicacionpersona.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosUbicacionPersonaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosUbicacionPersonaRespTipo createOpBuscarPorCriteriosUbicacionPersonaRespTipo() {
        return new OpBuscarPorCriteriosUbicacionPersonaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosUbicacionPersonaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosUbicacionPersonaSolTipo createOpBuscarPorCriteriosUbicacionPersonaSolTipo() {
        return new OpBuscarPorCriteriosUbicacionPersonaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosUbicacionPersonaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", name = "OpBuscarPorCriteriosUbicacionPersonaSol")
    public JAXBElement<OpBuscarPorCriteriosUbicacionPersonaSolTipo> createOpBuscarPorCriteriosUbicacionPersonaSol(OpBuscarPorCriteriosUbicacionPersonaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosUbicacionPersonaSolTipo>(_OpBuscarPorCriteriosUbicacionPersonaSol_QNAME, OpBuscarPorCriteriosUbicacionPersonaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosUbicacionPersonaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", name = "OpBuscarPorCriteriosUbicacionPersonaResp")
    public JAXBElement<OpBuscarPorCriteriosUbicacionPersonaRespTipo> createOpBuscarPorCriteriosUbicacionPersonaResp(OpBuscarPorCriteriosUbicacionPersonaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosUbicacionPersonaRespTipo>(_OpBuscarPorCriteriosUbicacionPersonaResp_QNAME, OpBuscarPorCriteriosUbicacionPersonaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1", name = "OpBuscarPorCriteriosUbicacionPersonaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosUbicacionPersonaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosUbicacionPersonaFallo_QNAME, FalloTipo.class, null, value);
    }

}
