
package co.gov.ugpp.denuncias.srvaplafectado.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplafectado.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdAfectadoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorIdAfectadoResp");
    private final static QName _OpCrearAfectadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpCrearAfectadoFallo");
    private final static QName _OpCrearAfectadoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpCrearAfectadoSol");
    private final static QName _OpActualizarAfectadoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpActualizarAfectadoResp");
    private final static QName _OpBuscarPorCriteriosAfectadoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorCriteriosAfectadoSol");
    private final static QName _OpBuscarPorCriteriosAfectadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorCriteriosAfectadoFallo");
    private final static QName _OpBuscarPorIdAfectadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorIdAfectadoFallo");
    private final static QName _OpBuscarPorCriteriosAfectadoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorCriteriosAfectadoResp");
    private final static QName _OpActualizarAfectadoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpActualizarAfectadoSol");
    private final static QName _OpBuscarPorIdAfectadoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpBuscarPorIdAfectadoSol");
    private final static QName _OpActualizarAfectadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpActualizarAfectadoFallo");
    private final static QName _OpCrearAfectadoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", "OpCrearAfectadoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplafectado.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAfectadoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAfectadoRespTipo createOpBuscarPorCriteriosAfectadoRespTipo() {
        return new OpBuscarPorCriteriosAfectadoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAfectadoSolTipo }
     * 
     */
    public OpBuscarPorIdAfectadoSolTipo createOpBuscarPorIdAfectadoSolTipo() {
        return new OpBuscarPorIdAfectadoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAfectadoSolTipo }
     * 
     */
    public OpActualizarAfectadoSolTipo createOpActualizarAfectadoSolTipo() {
        return new OpActualizarAfectadoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAfectadoRespTipo }
     * 
     */
    public OpCrearAfectadoRespTipo createOpCrearAfectadoRespTipo() {
        return new OpCrearAfectadoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAfectadoRespTipo }
     * 
     */
    public OpBuscarPorIdAfectadoRespTipo createOpBuscarPorIdAfectadoRespTipo() {
        return new OpBuscarPorIdAfectadoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAfectadoRespTipo }
     * 
     */
    public OpActualizarAfectadoRespTipo createOpActualizarAfectadoRespTipo() {
        return new OpActualizarAfectadoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAfectadoSolTipo }
     * 
     */
    public OpCrearAfectadoSolTipo createOpCrearAfectadoSolTipo() {
        return new OpCrearAfectadoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAfectadoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAfectadoSolTipo createOpBuscarPorCriteriosAfectadoSolTipo() {
        return new OpBuscarPorCriteriosAfectadoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAfectadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorIdAfectadoResp")
    public JAXBElement<OpBuscarPorIdAfectadoRespTipo> createOpBuscarPorIdAfectadoResp(OpBuscarPorIdAfectadoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAfectadoRespTipo>(_OpBuscarPorIdAfectadoResp_QNAME, OpBuscarPorIdAfectadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpCrearAfectadoFallo")
    public JAXBElement<FalloTipo> createOpCrearAfectadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAfectadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAfectadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpCrearAfectadoSol")
    public JAXBElement<OpCrearAfectadoSolTipo> createOpCrearAfectadoSol(OpCrearAfectadoSolTipo value) {
        return new JAXBElement<OpCrearAfectadoSolTipo>(_OpCrearAfectadoSol_QNAME, OpCrearAfectadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAfectadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpActualizarAfectadoResp")
    public JAXBElement<OpActualizarAfectadoRespTipo> createOpActualizarAfectadoResp(OpActualizarAfectadoRespTipo value) {
        return new JAXBElement<OpActualizarAfectadoRespTipo>(_OpActualizarAfectadoResp_QNAME, OpActualizarAfectadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAfectadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorCriteriosAfectadoSol")
    public JAXBElement<OpBuscarPorCriteriosAfectadoSolTipo> createOpBuscarPorCriteriosAfectadoSol(OpBuscarPorCriteriosAfectadoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAfectadoSolTipo>(_OpBuscarPorCriteriosAfectadoSol_QNAME, OpBuscarPorCriteriosAfectadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorCriteriosAfectadoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAfectadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAfectadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorIdAfectadoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAfectadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAfectadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAfectadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorCriteriosAfectadoResp")
    public JAXBElement<OpBuscarPorCriteriosAfectadoRespTipo> createOpBuscarPorCriteriosAfectadoResp(OpBuscarPorCriteriosAfectadoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAfectadoRespTipo>(_OpBuscarPorCriteriosAfectadoResp_QNAME, OpBuscarPorCriteriosAfectadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAfectadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpActualizarAfectadoSol")
    public JAXBElement<OpActualizarAfectadoSolTipo> createOpActualizarAfectadoSol(OpActualizarAfectadoSolTipo value) {
        return new JAXBElement<OpActualizarAfectadoSolTipo>(_OpActualizarAfectadoSol_QNAME, OpActualizarAfectadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAfectadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpBuscarPorIdAfectadoSol")
    public JAXBElement<OpBuscarPorIdAfectadoSolTipo> createOpBuscarPorIdAfectadoSol(OpBuscarPorIdAfectadoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAfectadoSolTipo>(_OpBuscarPorIdAfectadoSol_QNAME, OpBuscarPorIdAfectadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpActualizarAfectadoFallo")
    public JAXBElement<FalloTipo> createOpActualizarAfectadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAfectadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAfectadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1", name = "OpCrearAfectadoResp")
    public JAXBElement<OpCrearAfectadoRespTipo> createOpCrearAfectadoResp(OpCrearAfectadoRespTipo value) {
        return new JAXBElement<OpCrearAfectadoRespTipo>(_OpCrearAfectadoResp_QNAME, OpCrearAfectadoRespTipo.class, null, value);
    }

}
