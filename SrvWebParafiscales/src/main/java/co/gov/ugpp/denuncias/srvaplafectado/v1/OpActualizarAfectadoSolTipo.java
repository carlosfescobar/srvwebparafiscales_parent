
package co.gov.ugpp.denuncias.srvaplafectado.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;


/**
 * <p>Clase Java para OpActualizarAfectadoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarAfectadoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="afectado" type="{http://www.ugpp.gov.co/schema/Denuncias/AfectadoTipo/v1}AfectadoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarAfectadoSolTipo", propOrder = {
    "contextoTransaccional",
    "afectado"
})
public class OpActualizarAfectadoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AfectadoTipo afectado;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad afectado.
     * 
     * @return
     *     possible object is
     *     {@link AfectadoTipo }
     *     
     */
    public AfectadoTipo getAfectado() {
        return afectado;
    }

    /**
     * Define el valor de la propiedad afectado.
     * 
     * @param value
     *     allowed object is
     *     {@link AfectadoTipo }
     *     
     */
    public void setAfectado(AfectadoTipo value) {
        this.afectado = value;
    }

}
