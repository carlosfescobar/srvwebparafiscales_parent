
package co.gov.ugpp.denuncias.srvaplaportante.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplaportante.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdAportanteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpBuscarPorIdAportanteResp");
    private final static QName _OpAnalizarPagosPersuasivoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosPersuasivoResp");
    private final static QName _OpAnalizarPagosAportanteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosAportanteFallo");
    private final static QName _OpBuscarPorIdAportanteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpBuscarPorIdAportanteFallo");
    private final static QName _OpAnalizarPagosAportanteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosAportanteSol");
    private final static QName _OpActualizarAportanteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpActualizarAportanteResp");
    private final static QName _OpActualizarAportanteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpActualizarAportanteSol");
    private final static QName _OpActualizarAportanteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpActualizarAportanteFallo");
    private final static QName _OpAnalizarPagosAportanteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosAportanteResp");
    private final static QName _OpAnalizarPagosPersuasivoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosPersuasivoSol");
    private final static QName _OpAnalizarPagosPersuasivoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpAnalizarPagosPersuasivoFallo");
    private final static QName _OpBuscarPorIdAportanteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", "OpBuscarPorIdAportanteSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplaportante.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpAnalizarPagosAportanteRespTipo }
     * 
     */
    public OpAnalizarPagosAportanteRespTipo createOpAnalizarPagosAportanteRespTipo() {
        return new OpAnalizarPagosAportanteRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAportanteSolTipo }
     * 
     */
    public OpActualizarAportanteSolTipo createOpActualizarAportanteSolTipo() {
        return new OpActualizarAportanteSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAportanteSolTipo }
     * 
     */
    public OpBuscarPorIdAportanteSolTipo createOpBuscarPorIdAportanteSolTipo() {
        return new OpBuscarPorIdAportanteSolTipo();
    }

    /**
     * Create an instance of {@link OpAnalizarPagosPersuasivoSolTipo }
     * 
     */
    public OpAnalizarPagosPersuasivoSolTipo createOpAnalizarPagosPersuasivoSolTipo() {
        return new OpAnalizarPagosPersuasivoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAportanteRespTipo }
     * 
     */
    public OpActualizarAportanteRespTipo createOpActualizarAportanteRespTipo() {
        return new OpActualizarAportanteRespTipo();
    }

    /**
     * Create an instance of {@link OpAnalizarPagosPersuasivoRespTipo }
     * 
     */
    public OpAnalizarPagosPersuasivoRespTipo createOpAnalizarPagosPersuasivoRespTipo() {
        return new OpAnalizarPagosPersuasivoRespTipo();
    }

    /**
     * Create an instance of {@link OpAnalizarPagosAportanteSolTipo }
     * 
     */
    public OpAnalizarPagosAportanteSolTipo createOpAnalizarPagosAportanteSolTipo() {
        return new OpAnalizarPagosAportanteSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAportanteRespTipo }
     * 
     */
    public OpBuscarPorIdAportanteRespTipo createOpBuscarPorIdAportanteRespTipo() {
        return new OpBuscarPorIdAportanteRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAportanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpBuscarPorIdAportanteResp")
    public JAXBElement<OpBuscarPorIdAportanteRespTipo> createOpBuscarPorIdAportanteResp(OpBuscarPorIdAportanteRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAportanteRespTipo>(_OpBuscarPorIdAportanteResp_QNAME, OpBuscarPorIdAportanteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAnalizarPagosPersuasivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosPersuasivoResp")
    public JAXBElement<OpAnalizarPagosPersuasivoRespTipo> createOpAnalizarPagosPersuasivoResp(OpAnalizarPagosPersuasivoRespTipo value) {
        return new JAXBElement<OpAnalizarPagosPersuasivoRespTipo>(_OpAnalizarPagosPersuasivoResp_QNAME, OpAnalizarPagosPersuasivoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosAportanteFallo")
    public JAXBElement<FalloTipo> createOpAnalizarPagosAportanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpAnalizarPagosAportanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpBuscarPorIdAportanteFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAportanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAportanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAnalizarPagosAportanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosAportanteSol")
    public JAXBElement<OpAnalizarPagosAportanteSolTipo> createOpAnalizarPagosAportanteSol(OpAnalizarPagosAportanteSolTipo value) {
        return new JAXBElement<OpAnalizarPagosAportanteSolTipo>(_OpAnalizarPagosAportanteSol_QNAME, OpAnalizarPagosAportanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAportanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpActualizarAportanteResp")
    public JAXBElement<OpActualizarAportanteRespTipo> createOpActualizarAportanteResp(OpActualizarAportanteRespTipo value) {
        return new JAXBElement<OpActualizarAportanteRespTipo>(_OpActualizarAportanteResp_QNAME, OpActualizarAportanteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAportanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpActualizarAportanteSol")
    public JAXBElement<OpActualizarAportanteSolTipo> createOpActualizarAportanteSol(OpActualizarAportanteSolTipo value) {
        return new JAXBElement<OpActualizarAportanteSolTipo>(_OpActualizarAportanteSol_QNAME, OpActualizarAportanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpActualizarAportanteFallo")
    public JAXBElement<FalloTipo> createOpActualizarAportanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAportanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAnalizarPagosAportanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosAportanteResp")
    public JAXBElement<OpAnalizarPagosAportanteRespTipo> createOpAnalizarPagosAportanteResp(OpAnalizarPagosAportanteRespTipo value) {
        return new JAXBElement<OpAnalizarPagosAportanteRespTipo>(_OpAnalizarPagosAportanteResp_QNAME, OpAnalizarPagosAportanteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAnalizarPagosPersuasivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosPersuasivoSol")
    public JAXBElement<OpAnalizarPagosPersuasivoSolTipo> createOpAnalizarPagosPersuasivoSol(OpAnalizarPagosPersuasivoSolTipo value) {
        return new JAXBElement<OpAnalizarPagosPersuasivoSolTipo>(_OpAnalizarPagosPersuasivoSol_QNAME, OpAnalizarPagosPersuasivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpAnalizarPagosPersuasivoFallo")
    public JAXBElement<FalloTipo> createOpAnalizarPagosPersuasivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpAnalizarPagosPersuasivoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAportanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAportante/v1", name = "OpBuscarPorIdAportanteSol")
    public JAXBElement<OpBuscarPorIdAportanteSolTipo> createOpBuscarPorIdAportanteSol(OpBuscarPorIdAportanteSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAportanteSolTipo>(_OpBuscarPorIdAportanteSol_QNAME, OpBuscarPorIdAportanteSolTipo.class, null, value);
    }

}
