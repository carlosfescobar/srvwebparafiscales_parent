
package co.gov.ugpp.denuncias.srvaplaportante.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;


/**
 * <p>Clase Java para OpAnalizarPagosPersuasivoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpAnalizarPagosPersuasivoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo"/>
 *         &lt;element name="cotizante" type="{http://www.ugpp.gov.co/schema/Denuncias/CotizanteTipo/v1}CotizanteTipo" minOccurs="0"/>
 *         &lt;element name="periodo" type="{http://www.ugpp.gov.co/esb/schema/RangoFechaTipo/v1}RangoFechaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpAnalizarPagosPersuasivoSolTipo", propOrder = {
    "contextoTransaccional",
    "idExpediente",
    "aportante",
    "cotizante",
    "periodo"
})
public class OpAnalizarPagosPersuasivoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected Object idExpediente;
    @XmlElement(required = true)
    protected AportanteTipo aportante;
    protected CotizanteTipo cotizante;
    @XmlElement(required = true)
    protected RangoFechaTipo periodo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setIdExpediente(Object value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizante.
     * 
     * @return
     *     possible object is
     *     {@link CotizanteTipo }
     *     
     */
    public CotizanteTipo getCotizante() {
        return cotizante;
    }

    /**
     * Define el valor de la propiedad cotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link CotizanteTipo }
     *     
     */
    public void setCotizante(CotizanteTipo value) {
        this.cotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad periodo.
     * 
     * @return
     *     possible object is
     *     {@link RangoFechaTipo }
     *     
     */
    public RangoFechaTipo getPeriodo() {
        return periodo;
    }

    /**
     * Define el valor de la propiedad periodo.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoFechaTipo }
     *     
     */
    public void setPeriodo(RangoFechaTipo value) {
        this.periodo = value;
    }

}
