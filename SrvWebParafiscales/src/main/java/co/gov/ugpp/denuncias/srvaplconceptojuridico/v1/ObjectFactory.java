
package co.gov.ugpp.denuncias.srvaplconceptojuridico.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplconceptojuridico.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarConceptoJuridicoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpActualizarConceptoJuridicoResp");
    private final static QName _OpActualizarConceptoJuridicoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpActualizarConceptoJuridicoFallo");
    private final static QName _OpCrearConceptoJuridicoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpCrearConceptoJuridicoResp");
    private final static QName _OpCrearConceptoJuridicoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpCrearConceptoJuridicoSol");
    private final static QName _OpBuscarPorIdConceptoJuridicoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpBuscarPorIdConceptoJuridicoSol");
    private final static QName _OpBuscarPorIdConceptoJuridicoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpBuscarPorIdConceptoJuridicoResp");
    private final static QName _OpBuscarPorIdConceptoJuridicoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpBuscarPorIdConceptoJuridicoFallo");
    private final static QName _OpCrearConceptoJuridicoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpCrearConceptoJuridicoFallo");
    private final static QName _OpActualizarConceptoJuridicoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", "OpActualizarConceptoJuridicoSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplconceptojuridico.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarConceptoJuridicoSolTipo }
     * 
     */
    public OpActualizarConceptoJuridicoSolTipo createOpActualizarConceptoJuridicoSolTipo() {
        return new OpActualizarConceptoJuridicoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearConceptoJuridicoSolTipo }
     * 
     */
    public OpCrearConceptoJuridicoSolTipo createOpCrearConceptoJuridicoSolTipo() {
        return new OpCrearConceptoJuridicoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearConceptoJuridicoRespTipo }
     * 
     */
    public OpCrearConceptoJuridicoRespTipo createOpCrearConceptoJuridicoRespTipo() {
        return new OpCrearConceptoJuridicoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarConceptoJuridicoRespTipo }
     * 
     */
    public OpActualizarConceptoJuridicoRespTipo createOpActualizarConceptoJuridicoRespTipo() {
        return new OpActualizarConceptoJuridicoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdConceptoJuridicoRespTipo }
     * 
     */
    public OpBuscarPorIdConceptoJuridicoRespTipo createOpBuscarPorIdConceptoJuridicoRespTipo() {
        return new OpBuscarPorIdConceptoJuridicoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdConceptoJuridicoSolTipo }
     * 
     */
    public OpBuscarPorIdConceptoJuridicoSolTipo createOpBuscarPorIdConceptoJuridicoSolTipo() {
        return new OpBuscarPorIdConceptoJuridicoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarConceptoJuridicoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpActualizarConceptoJuridicoResp")
    public JAXBElement<OpActualizarConceptoJuridicoRespTipo> createOpActualizarConceptoJuridicoResp(OpActualizarConceptoJuridicoRespTipo value) {
        return new JAXBElement<OpActualizarConceptoJuridicoRespTipo>(_OpActualizarConceptoJuridicoResp_QNAME, OpActualizarConceptoJuridicoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpActualizarConceptoJuridicoFallo")
    public JAXBElement<FalloTipo> createOpActualizarConceptoJuridicoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarConceptoJuridicoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearConceptoJuridicoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpCrearConceptoJuridicoResp")
    public JAXBElement<OpCrearConceptoJuridicoRespTipo> createOpCrearConceptoJuridicoResp(OpCrearConceptoJuridicoRespTipo value) {
        return new JAXBElement<OpCrearConceptoJuridicoRespTipo>(_OpCrearConceptoJuridicoResp_QNAME, OpCrearConceptoJuridicoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearConceptoJuridicoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpCrearConceptoJuridicoSol")
    public JAXBElement<OpCrearConceptoJuridicoSolTipo> createOpCrearConceptoJuridicoSol(OpCrearConceptoJuridicoSolTipo value) {
        return new JAXBElement<OpCrearConceptoJuridicoSolTipo>(_OpCrearConceptoJuridicoSol_QNAME, OpCrearConceptoJuridicoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdConceptoJuridicoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpBuscarPorIdConceptoJuridicoSol")
    public JAXBElement<OpBuscarPorIdConceptoJuridicoSolTipo> createOpBuscarPorIdConceptoJuridicoSol(OpBuscarPorIdConceptoJuridicoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdConceptoJuridicoSolTipo>(_OpBuscarPorIdConceptoJuridicoSol_QNAME, OpBuscarPorIdConceptoJuridicoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdConceptoJuridicoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpBuscarPorIdConceptoJuridicoResp")
    public JAXBElement<OpBuscarPorIdConceptoJuridicoRespTipo> createOpBuscarPorIdConceptoJuridicoResp(OpBuscarPorIdConceptoJuridicoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdConceptoJuridicoRespTipo>(_OpBuscarPorIdConceptoJuridicoResp_QNAME, OpBuscarPorIdConceptoJuridicoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpBuscarPorIdConceptoJuridicoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdConceptoJuridicoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdConceptoJuridicoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpCrearConceptoJuridicoFallo")
    public JAXBElement<FalloTipo> createOpCrearConceptoJuridicoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearConceptoJuridicoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarConceptoJuridicoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1", name = "OpActualizarConceptoJuridicoSol")
    public JAXBElement<OpActualizarConceptoJuridicoSolTipo> createOpActualizarConceptoJuridicoSol(OpActualizarConceptoJuridicoSolTipo value) {
        return new JAXBElement<OpActualizarConceptoJuridicoSolTipo>(_OpActualizarConceptoJuridicoSol_QNAME, OpActualizarConceptoJuridicoSolTipo.class, null, value);
    }

}
