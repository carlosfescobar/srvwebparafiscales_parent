
package co.gov.ugpp.denuncias.srvaplconceptojuridico.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;


/**
 * <p>Clase Java para OpActualizarConceptoJuridicoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarConceptoJuridicoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="conceptoJuridico" type="{http://www.ugpp.gov.co/schema/Denuncias/ConceptoJuridicoTipo/v1}ConceptoJuridicoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarConceptoJuridicoSolTipo", propOrder = {
    "contextoTransaccional",
    "conceptoJuridico"
})
public class OpActualizarConceptoJuridicoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ConceptoJuridicoTipo conceptoJuridico;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoJuridico.
     * 
     * @return
     *     possible object is
     *     {@link ConceptoJuridicoTipo }
     *     
     */
    public ConceptoJuridicoTipo getConceptoJuridico() {
        return conceptoJuridico;
    }

    /**
     * Define el valor de la propiedad conceptoJuridico.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptoJuridicoTipo }
     *     
     */
    public void setConceptoJuridico(ConceptoJuridicoTipo value) {
        this.conceptoJuridico = value;
    }

}
