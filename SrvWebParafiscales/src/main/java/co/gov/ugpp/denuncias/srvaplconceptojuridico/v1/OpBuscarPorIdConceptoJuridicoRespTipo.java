
package co.gov.ugpp.denuncias.srvaplconceptojuridico.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;


/**
 * <p>Clase Java para OpBuscarPorIdConceptoJuridicoRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdConceptoJuridicoRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="conceptoJuridico" type="{http://www.ugpp.gov.co/schema/Denuncias/ConceptoJuridicoTipo/v1}ConceptoJuridicoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdConceptoJuridicoRespTipo", propOrder = {
    "contextoRespuesta",
    "conceptoJuridico"
})
public class OpBuscarPorIdConceptoJuridicoRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(required = true)
    protected ConceptoJuridicoTipo conceptoJuridico;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoJuridico.
     * 
     * @return
     *     possible object is
     *     {@link ConceptoJuridicoTipo }
     *     
     */
    public ConceptoJuridicoTipo getConceptoJuridico() {
        return conceptoJuridico;
    }

    /**
     * Define el valor de la propiedad conceptoJuridico.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptoJuridicoTipo }
     *     
     */
    public void setConceptoJuridico(ConceptoJuridicoTipo value) {
        this.conceptoJuridico = value;
    }

}
