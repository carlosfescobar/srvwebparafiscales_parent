
package co.gov.ugpp.denuncias.srvaplcotizante.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplcotizante.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearCotizanteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpCrearCotizanteSol");
    private final static QName _OpCrearCotizanteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpCrearCotizanteFallo");
    private final static QName _OpBuscarPorIdCotizanteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpBuscarPorIdCotizanteFallo");
    private final static QName _OpCrearCotizanteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpCrearCotizanteResp");
    private final static QName _OpBuscarPorIdCotizanteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpBuscarPorIdCotizanteSol");
    private final static QName _OpBuscarPorIdCotizanteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", "OpBuscarPorIdCotizanteResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplcotizante.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearCotizanteRespTipo }
     * 
     */
    public OpCrearCotizanteRespTipo createOpCrearCotizanteRespTipo() {
        return new OpCrearCotizanteRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdCotizanteRespTipo }
     * 
     */
    public OpBuscarPorIdCotizanteRespTipo createOpBuscarPorIdCotizanteRespTipo() {
        return new OpBuscarPorIdCotizanteRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdCotizanteSolTipo }
     * 
     */
    public OpBuscarPorIdCotizanteSolTipo createOpBuscarPorIdCotizanteSolTipo() {
        return new OpBuscarPorIdCotizanteSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearCotizanteSolTipo }
     * 
     */
    public OpCrearCotizanteSolTipo createOpCrearCotizanteSolTipo() {
        return new OpCrearCotizanteSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearCotizanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpCrearCotizanteSol")
    public JAXBElement<OpCrearCotizanteSolTipo> createOpCrearCotizanteSol(OpCrearCotizanteSolTipo value) {
        return new JAXBElement<OpCrearCotizanteSolTipo>(_OpCrearCotizanteSol_QNAME, OpCrearCotizanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpCrearCotizanteFallo")
    public JAXBElement<FalloTipo> createOpCrearCotizanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearCotizanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpBuscarPorIdCotizanteFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdCotizanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdCotizanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearCotizanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpCrearCotizanteResp")
    public JAXBElement<OpCrearCotizanteRespTipo> createOpCrearCotizanteResp(OpCrearCotizanteRespTipo value) {
        return new JAXBElement<OpCrearCotizanteRespTipo>(_OpCrearCotizanteResp_QNAME, OpCrearCotizanteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdCotizanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpBuscarPorIdCotizanteSol")
    public JAXBElement<OpBuscarPorIdCotizanteSolTipo> createOpBuscarPorIdCotizanteSol(OpBuscarPorIdCotizanteSolTipo value) {
        return new JAXBElement<OpBuscarPorIdCotizanteSolTipo>(_OpBuscarPorIdCotizanteSol_QNAME, OpBuscarPorIdCotizanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdCotizanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1", name = "OpBuscarPorIdCotizanteResp")
    public JAXBElement<OpBuscarPorIdCotizanteRespTipo> createOpBuscarPorIdCotizanteResp(OpBuscarPorIdCotizanteRespTipo value) {
        return new JAXBElement<OpBuscarPorIdCotizanteRespTipo>(_OpBuscarPorIdCotizanteResp_QNAME, OpBuscarPorIdCotizanteRespTipo.class, null, value);
    }

}
