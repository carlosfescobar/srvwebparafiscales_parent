
package co.gov.ugpp.denuncias.srvapldenuncia.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvapldenuncia.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearDenunciaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCrearDenunciaResp");
    private final static QName _OpActualizarDenunciaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpActualizarDenunciaFallo");
    private final static QName _OpBuscarPorCriteriosDenunciaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorCriteriosDenunciaResp");
    private final static QName _OpActualizarDenunciaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpActualizarDenunciaSol");
    private final static QName _OpCrearDenunciaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCrearDenunciaFallo");
    private final static QName _OpBuscarPorIdDenunciaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorIdDenunciaSol");
    private final static QName _OpBuscarPorIdDenunciaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorIdDenunciaFallo");
    private final static QName _OpBuscarPorCriteriosDenunciaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorCriteriosDenunciaFallo");
    private final static QName _OpBuscarPorIdDenunciaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorIdDenunciaResp");
    private final static QName _OpCalificarDenunciaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCalificarDenunciaFallo");
    private final static QName _OpBuscarPorCriteriosDenunciaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpBuscarPorCriteriosDenunciaSol");
    private final static QName _OpCrearDenunciaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCrearDenunciaSol");
    private final static QName _OpCalificarDenunciaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCalificarDenunciaResp");
    private final static QName _OpActualizarDenunciaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpActualizarDenunciaResp");
    private final static QName _OpCalificarDenunciaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", "OpCalificarDenunciaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvapldenuncia.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDenunciaRespTipo }
     * 
     */
    public OpBuscarPorIdDenunciaRespTipo createOpBuscarPorIdDenunciaRespTipo() {
        return new OpBuscarPorIdDenunciaRespTipo();
    }

    /**
     * Create an instance of {@link OpCalificarDenunciaSolTipo }
     * 
     */
    public OpCalificarDenunciaSolTipo createOpCalificarDenunciaSolTipo() {
        return new OpCalificarDenunciaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDenunciaRespTipo }
     * 
     */
    public OpActualizarDenunciaRespTipo createOpActualizarDenunciaRespTipo() {
        return new OpActualizarDenunciaRespTipo();
    }

    /**
     * Create an instance of {@link OpCalificarDenunciaRespTipo }
     * 
     */
    public OpCalificarDenunciaRespTipo createOpCalificarDenunciaRespTipo() {
        return new OpCalificarDenunciaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearDenunciaSolTipo }
     * 
     */
    public OpCrearDenunciaSolTipo createOpCrearDenunciaSolTipo() {
        return new OpCrearDenunciaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDenunciaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosDenunciaSolTipo createOpBuscarPorCriteriosDenunciaSolTipo() {
        return new OpBuscarPorCriteriosDenunciaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearDenunciaRespTipo }
     * 
     */
    public OpCrearDenunciaRespTipo createOpCrearDenunciaRespTipo() {
        return new OpCrearDenunciaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDenunciaSolTipo }
     * 
     */
    public OpBuscarPorIdDenunciaSolTipo createOpBuscarPorIdDenunciaSolTipo() {
        return new OpBuscarPorIdDenunciaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDenunciaSolTipo }
     * 
     */
    public OpActualizarDenunciaSolTipo createOpActualizarDenunciaSolTipo() {
        return new OpActualizarDenunciaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDenunciaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosDenunciaRespTipo createOpBuscarPorCriteriosDenunciaRespTipo() {
        return new OpBuscarPorCriteriosDenunciaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDenunciaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCrearDenunciaResp")
    public JAXBElement<OpCrearDenunciaRespTipo> createOpCrearDenunciaResp(OpCrearDenunciaRespTipo value) {
        return new JAXBElement<OpCrearDenunciaRespTipo>(_OpCrearDenunciaResp_QNAME, OpCrearDenunciaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpActualizarDenunciaFallo")
    public JAXBElement<FalloTipo> createOpActualizarDenunciaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarDenunciaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDenunciaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorCriteriosDenunciaResp")
    public JAXBElement<OpBuscarPorCriteriosDenunciaRespTipo> createOpBuscarPorCriteriosDenunciaResp(OpBuscarPorCriteriosDenunciaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDenunciaRespTipo>(_OpBuscarPorCriteriosDenunciaResp_QNAME, OpBuscarPorCriteriosDenunciaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDenunciaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpActualizarDenunciaSol")
    public JAXBElement<OpActualizarDenunciaSolTipo> createOpActualizarDenunciaSol(OpActualizarDenunciaSolTipo value) {
        return new JAXBElement<OpActualizarDenunciaSolTipo>(_OpActualizarDenunciaSol_QNAME, OpActualizarDenunciaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCrearDenunciaFallo")
    public JAXBElement<FalloTipo> createOpCrearDenunciaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearDenunciaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDenunciaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorIdDenunciaSol")
    public JAXBElement<OpBuscarPorIdDenunciaSolTipo> createOpBuscarPorIdDenunciaSol(OpBuscarPorIdDenunciaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdDenunciaSolTipo>(_OpBuscarPorIdDenunciaSol_QNAME, OpBuscarPorIdDenunciaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorIdDenunciaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdDenunciaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdDenunciaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorCriteriosDenunciaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosDenunciaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosDenunciaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDenunciaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorIdDenunciaResp")
    public JAXBElement<OpBuscarPorIdDenunciaRespTipo> createOpBuscarPorIdDenunciaResp(OpBuscarPorIdDenunciaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdDenunciaRespTipo>(_OpBuscarPorIdDenunciaResp_QNAME, OpBuscarPorIdDenunciaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCalificarDenunciaFallo")
    public JAXBElement<FalloTipo> createOpCalificarDenunciaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCalificarDenunciaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDenunciaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpBuscarPorCriteriosDenunciaSol")
    public JAXBElement<OpBuscarPorCriteriosDenunciaSolTipo> createOpBuscarPorCriteriosDenunciaSol(OpBuscarPorCriteriosDenunciaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDenunciaSolTipo>(_OpBuscarPorCriteriosDenunciaSol_QNAME, OpBuscarPorCriteriosDenunciaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDenunciaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCrearDenunciaSol")
    public JAXBElement<OpCrearDenunciaSolTipo> createOpCrearDenunciaSol(OpCrearDenunciaSolTipo value) {
        return new JAXBElement<OpCrearDenunciaSolTipo>(_OpCrearDenunciaSol_QNAME, OpCrearDenunciaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCalificarDenunciaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCalificarDenunciaResp")
    public JAXBElement<OpCalificarDenunciaRespTipo> createOpCalificarDenunciaResp(OpCalificarDenunciaRespTipo value) {
        return new JAXBElement<OpCalificarDenunciaRespTipo>(_OpCalificarDenunciaResp_QNAME, OpCalificarDenunciaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDenunciaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpActualizarDenunciaResp")
    public JAXBElement<OpActualizarDenunciaRespTipo> createOpActualizarDenunciaResp(OpActualizarDenunciaRespTipo value) {
        return new JAXBElement<OpActualizarDenunciaRespTipo>(_OpActualizarDenunciaResp_QNAME, OpActualizarDenunciaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCalificarDenunciaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1", name = "OpCalificarDenunciaSol")
    public JAXBElement<OpCalificarDenunciaSolTipo> createOpCalificarDenunciaSol(OpCalificarDenunciaSolTipo value) {
        return new JAXBElement<OpCalificarDenunciaSolTipo>(_OpCalificarDenunciaSol_QNAME, OpCalificarDenunciaSolTipo.class, null, value);
    }

}
