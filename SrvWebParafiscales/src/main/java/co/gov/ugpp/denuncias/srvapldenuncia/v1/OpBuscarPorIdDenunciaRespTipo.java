
package co.gov.ugpp.denuncias.srvapldenuncia.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;


/**
 * <p>Clase Java para OpBuscarPorIdDenunciaRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdDenunciaRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="denuncia" type="{http://www.ugpp.gov.co/schema/Denuncias/DenunciaTipo/v1}DenunciaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdDenunciaRespTipo", propOrder = {
    "contextoRespuesta",
    "denuncia"
})
public class OpBuscarPorIdDenunciaRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected DenunciaTipo denuncia;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad denuncia.
     * 
     * @return
     *     possible object is
     *     {@link DenunciaTipo }
     *     
     */
    public DenunciaTipo getDenuncia() {
        return denuncia;
    }

    /**
     * Define el valor de la propiedad denuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link DenunciaTipo }
     *     
     */
    public void setDenuncia(DenunciaTipo value) {
        this.denuncia = value;
    }

}
