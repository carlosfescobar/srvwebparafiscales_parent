
package co.gov.ugpp.denuncias.srvapldenuncia.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;


/**
 * <p>Clase Java para OpCrearDenunciaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearDenunciaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="denuncia" type="{http://www.ugpp.gov.co/schema/Denuncias/DenunciaTipo/v1}DenunciaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearDenunciaSolTipo", propOrder = {
    "contextoTransaccional",
    "denuncia"
})
public class OpCrearDenunciaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected DenunciaTipo denuncia;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad denuncia.
     * 
     * @return
     *     possible object is
     *     {@link DenunciaTipo }
     *     
     */
    public DenunciaTipo getDenuncia() {
        return denuncia;
    }

    /**
     * Define el valor de la propiedad denuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link DenunciaTipo }
     *     
     */
    public void setDenuncia(DenunciaTipo value) {
        this.denuncia = value;
    }

}
