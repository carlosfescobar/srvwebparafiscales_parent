
package co.gov.ugpp.denuncias.srvapldenunciante.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvapldenunciante.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarDenuncianteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpActualizarDenuncianteFallo");
    private final static QName _OpBuscarPorIdDenuncianteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpBuscarPorIdDenuncianteSol");
    private final static QName _OpActualizarDenuncianteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpActualizarDenuncianteSol");
    private final static QName _OpActualizarDenuncianteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpActualizarDenuncianteResp");
    private final static QName _OpBuscarPorIdDenuncianteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpBuscarPorIdDenuncianteResp");
    private final static QName _OpBuscarPorIdDenuncianteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpBuscarPorIdDenuncianteFallo");
    private final static QName _OpCrearDenuncianteResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpCrearDenuncianteResp");
    private final static QName _OpCrearDenuncianteSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpCrearDenuncianteSol");
    private final static QName _OpCrearDenuncianteFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", "OpCrearDenuncianteFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvapldenunciante.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearDenuncianteSolTipo }
     * 
     */
    public OpCrearDenuncianteSolTipo createOpCrearDenuncianteSolTipo() {
        return new OpCrearDenuncianteSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearDenuncianteRespTipo }
     * 
     */
    public OpCrearDenuncianteRespTipo createOpCrearDenuncianteRespTipo() {
        return new OpCrearDenuncianteRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDenuncianteRespTipo }
     * 
     */
    public OpBuscarPorIdDenuncianteRespTipo createOpBuscarPorIdDenuncianteRespTipo() {
        return new OpBuscarPorIdDenuncianteRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDenuncianteRespTipo }
     * 
     */
    public OpActualizarDenuncianteRespTipo createOpActualizarDenuncianteRespTipo() {
        return new OpActualizarDenuncianteRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDenuncianteSolTipo }
     * 
     */
    public OpActualizarDenuncianteSolTipo createOpActualizarDenuncianteSolTipo() {
        return new OpActualizarDenuncianteSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDenuncianteSolTipo }
     * 
     */
    public OpBuscarPorIdDenuncianteSolTipo createOpBuscarPorIdDenuncianteSolTipo() {
        return new OpBuscarPorIdDenuncianteSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpActualizarDenuncianteFallo")
    public JAXBElement<FalloTipo> createOpActualizarDenuncianteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarDenuncianteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDenuncianteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpBuscarPorIdDenuncianteSol")
    public JAXBElement<OpBuscarPorIdDenuncianteSolTipo> createOpBuscarPorIdDenuncianteSol(OpBuscarPorIdDenuncianteSolTipo value) {
        return new JAXBElement<OpBuscarPorIdDenuncianteSolTipo>(_OpBuscarPorIdDenuncianteSol_QNAME, OpBuscarPorIdDenuncianteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDenuncianteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpActualizarDenuncianteSol")
    public JAXBElement<OpActualizarDenuncianteSolTipo> createOpActualizarDenuncianteSol(OpActualizarDenuncianteSolTipo value) {
        return new JAXBElement<OpActualizarDenuncianteSolTipo>(_OpActualizarDenuncianteSol_QNAME, OpActualizarDenuncianteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDenuncianteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpActualizarDenuncianteResp")
    public JAXBElement<OpActualizarDenuncianteRespTipo> createOpActualizarDenuncianteResp(OpActualizarDenuncianteRespTipo value) {
        return new JAXBElement<OpActualizarDenuncianteRespTipo>(_OpActualizarDenuncianteResp_QNAME, OpActualizarDenuncianteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDenuncianteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpBuscarPorIdDenuncianteResp")
    public JAXBElement<OpBuscarPorIdDenuncianteRespTipo> createOpBuscarPorIdDenuncianteResp(OpBuscarPorIdDenuncianteRespTipo value) {
        return new JAXBElement<OpBuscarPorIdDenuncianteRespTipo>(_OpBuscarPorIdDenuncianteResp_QNAME, OpBuscarPorIdDenuncianteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpBuscarPorIdDenuncianteFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdDenuncianteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdDenuncianteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDenuncianteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpCrearDenuncianteResp")
    public JAXBElement<OpCrearDenuncianteRespTipo> createOpCrearDenuncianteResp(OpCrearDenuncianteRespTipo value) {
        return new JAXBElement<OpCrearDenuncianteRespTipo>(_OpCrearDenuncianteResp_QNAME, OpCrearDenuncianteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDenuncianteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpCrearDenuncianteSol")
    public JAXBElement<OpCrearDenuncianteSolTipo> createOpCrearDenuncianteSol(OpCrearDenuncianteSolTipo value) {
        return new JAXBElement<OpCrearDenuncianteSolTipo>(_OpCrearDenuncianteSol_QNAME, OpCrearDenuncianteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1", name = "OpCrearDenuncianteFallo")
    public JAXBElement<FalloTipo> createOpCrearDenuncianteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearDenuncianteFallo_QNAME, FalloTipo.class, null, value);
    }

}
