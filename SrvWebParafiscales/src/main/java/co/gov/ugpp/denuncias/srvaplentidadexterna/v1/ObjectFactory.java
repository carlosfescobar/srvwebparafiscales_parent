
package co.gov.ugpp.denuncias.srvaplentidadexterna.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplentidadexterna.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorIdEntidadExternaResp");
    private final static QName _OpBuscarPorCriteriosEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorCriteriosEntidadExternaSol");
    private final static QName _OpBuscarPorCriteriosEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorCriteriosEntidadExternaFallo");
    private final static QName _OpBuscarPorIdEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorIdEntidadExternaFallo");
    private final static QName _OpBuscarPorIdEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorIdEntidadExternaSol");
    private final static QName _OpBuscarPorCriteriosEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", "OpBuscarPorCriteriosEntidadExternaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplentidadexterna.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosEntidadExternaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosEntidadExternaRespTipo createOpBuscarPorCriteriosEntidadExternaRespTipo() {
        return new OpBuscarPorCriteriosEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdEntidadExternaSolTipo }
     * 
     */
    public OpBuscarPorIdEntidadExternaSolTipo createOpBuscarPorIdEntidadExternaSolTipo() {
        return new OpBuscarPorIdEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosEntidadExternaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosEntidadExternaSolTipo createOpBuscarPorCriteriosEntidadExternaSolTipo() {
        return new OpBuscarPorCriteriosEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdEntidadExternaRespTipo }
     * 
     */
    public OpBuscarPorIdEntidadExternaRespTipo createOpBuscarPorIdEntidadExternaRespTipo() {
        return new OpBuscarPorIdEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorIdEntidadExternaResp")
    public JAXBElement<OpBuscarPorIdEntidadExternaRespTipo> createOpBuscarPorIdEntidadExternaResp(OpBuscarPorIdEntidadExternaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdEntidadExternaRespTipo>(_OpBuscarPorIdEntidadExternaResp_QNAME, OpBuscarPorIdEntidadExternaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorCriteriosEntidadExternaSol")
    public JAXBElement<OpBuscarPorCriteriosEntidadExternaSolTipo> createOpBuscarPorCriteriosEntidadExternaSol(OpBuscarPorCriteriosEntidadExternaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosEntidadExternaSolTipo>(_OpBuscarPorCriteriosEntidadExternaSol_QNAME, OpBuscarPorCriteriosEntidadExternaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorCriteriosEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorIdEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorIdEntidadExternaSol")
    public JAXBElement<OpBuscarPorIdEntidadExternaSolTipo> createOpBuscarPorIdEntidadExternaSol(OpBuscarPorIdEntidadExternaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdEntidadExternaSolTipo>(_OpBuscarPorIdEntidadExternaSol_QNAME, OpBuscarPorIdEntidadExternaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1", name = "OpBuscarPorCriteriosEntidadExternaResp")
    public JAXBElement<OpBuscarPorCriteriosEntidadExternaRespTipo> createOpBuscarPorCriteriosEntidadExternaResp(OpBuscarPorCriteriosEntidadExternaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosEntidadExternaRespTipo>(_OpBuscarPorCriteriosEntidadExternaResp_QNAME, OpBuscarPorCriteriosEntidadExternaRespTipo.class, null, value);
    }

}
