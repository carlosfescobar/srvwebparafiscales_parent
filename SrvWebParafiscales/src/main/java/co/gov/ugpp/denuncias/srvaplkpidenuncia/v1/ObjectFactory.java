
package co.gov.ugpp.denuncias.srvaplkpidenuncia.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvaplkpidenuncia.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarDenunciaKPISol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", "OpConsultarDenunciaKPISol");
    private final static QName _OpConsultarDenunciaKPIResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", "OpConsultarDenunciaKPIResp");
    private final static QName _OpConsultarDenunciaKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", "OpConsultarDenunciaKPIFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvaplkpidenuncia.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarDenunciaKPIRespTipo }
     * 
     */
    public OpConsultarDenunciaKPIRespTipo createOpConsultarDenunciaKPIRespTipo() {
        return new OpConsultarDenunciaKPIRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarDenunciaKPISolTipo }
     * 
     */
    public OpConsultarDenunciaKPISolTipo createOpConsultarDenunciaKPISolTipo() {
        return new OpConsultarDenunciaKPISolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarDenunciaKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", name = "OpConsultarDenunciaKPISol")
    public JAXBElement<OpConsultarDenunciaKPISolTipo> createOpConsultarDenunciaKPISol(OpConsultarDenunciaKPISolTipo value) {
        return new JAXBElement<OpConsultarDenunciaKPISolTipo>(_OpConsultarDenunciaKPISol_QNAME, OpConsultarDenunciaKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarDenunciaKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", name = "OpConsultarDenunciaKPIResp")
    public JAXBElement<OpConsultarDenunciaKPIRespTipo> createOpConsultarDenunciaKPIResp(OpConsultarDenunciaKPIRespTipo value) {
        return new JAXBElement<OpConsultarDenunciaKPIRespTipo>(_OpConsultarDenunciaKPIResp_QNAME, OpConsultarDenunciaKPIRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1", name = "OpConsultarDenunciaKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarDenunciaKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarDenunciaKPIFallo_QNAME, FalloTipo.class, null, value);
    }

}
