
package co.gov.ugpp.denuncias.srvapltraslado.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.denuncias.srvapltraslado.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosTrasladoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpBuscarPorCriteriosTrasladoResp");
    private final static QName _OpBuscarPorCriteriosTrasladoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpBuscarPorCriteriosTrasladoFallo");
    private final static QName _OpActualizarTrasladoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpActualizarTrasladoFallo");
    private final static QName _OpActualizarTrasladoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpActualizarTrasladoSol");
    private final static QName _OpCrearTrasladoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpCrearTrasladoSol");
    private final static QName _OpCrearTrasladoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpCrearTrasladoResp");
    private final static QName _OpBuscarPorCriteriosTrasladoSol_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpBuscarPorCriteriosTrasladoSol");
    private final static QName _OpActualizarTrasladoResp_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpActualizarTrasladoResp");
    private final static QName _OpCrearTrasladoFallo_QNAME = new QName("http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", "OpCrearTrasladoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.denuncias.srvapltraslado.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearTrasladoSolTipo }
     * 
     */
    public OpCrearTrasladoSolTipo createOpCrearTrasladoSolTipo() {
        return new OpCrearTrasladoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrasladoSolTipo }
     * 
     */
    public OpActualizarTrasladoSolTipo createOpActualizarTrasladoSolTipo() {
        return new OpActualizarTrasladoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosTrasladoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosTrasladoRespTipo createOpBuscarPorCriteriosTrasladoRespTipo() {
        return new OpBuscarPorCriteriosTrasladoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrasladoRespTipo }
     * 
     */
    public OpActualizarTrasladoRespTipo createOpActualizarTrasladoRespTipo() {
        return new OpActualizarTrasladoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosTrasladoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosTrasladoSolTipo createOpBuscarPorCriteriosTrasladoSolTipo() {
        return new OpBuscarPorCriteriosTrasladoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearTrasladoRespTipo }
     * 
     */
    public OpCrearTrasladoRespTipo createOpCrearTrasladoRespTipo() {
        return new OpCrearTrasladoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosTrasladoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpBuscarPorCriteriosTrasladoResp")
    public JAXBElement<OpBuscarPorCriteriosTrasladoRespTipo> createOpBuscarPorCriteriosTrasladoResp(OpBuscarPorCriteriosTrasladoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosTrasladoRespTipo>(_OpBuscarPorCriteriosTrasladoResp_QNAME, OpBuscarPorCriteriosTrasladoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpBuscarPorCriteriosTrasladoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosTrasladoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosTrasladoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpActualizarTrasladoFallo")
    public JAXBElement<FalloTipo> createOpActualizarTrasladoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarTrasladoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrasladoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpActualizarTrasladoSol")
    public JAXBElement<OpActualizarTrasladoSolTipo> createOpActualizarTrasladoSol(OpActualizarTrasladoSolTipo value) {
        return new JAXBElement<OpActualizarTrasladoSolTipo>(_OpActualizarTrasladoSol_QNAME, OpActualizarTrasladoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrasladoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpCrearTrasladoSol")
    public JAXBElement<OpCrearTrasladoSolTipo> createOpCrearTrasladoSol(OpCrearTrasladoSolTipo value) {
        return new JAXBElement<OpCrearTrasladoSolTipo>(_OpCrearTrasladoSol_QNAME, OpCrearTrasladoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrasladoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpCrearTrasladoResp")
    public JAXBElement<OpCrearTrasladoRespTipo> createOpCrearTrasladoResp(OpCrearTrasladoRespTipo value) {
        return new JAXBElement<OpCrearTrasladoRespTipo>(_OpCrearTrasladoResp_QNAME, OpCrearTrasladoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosTrasladoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpBuscarPorCriteriosTrasladoSol")
    public JAXBElement<OpBuscarPorCriteriosTrasladoSolTipo> createOpBuscarPorCriteriosTrasladoSol(OpBuscarPorCriteriosTrasladoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosTrasladoSolTipo>(_OpBuscarPorCriteriosTrasladoSol_QNAME, OpBuscarPorCriteriosTrasladoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrasladoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpActualizarTrasladoResp")
    public JAXBElement<OpActualizarTrasladoRespTipo> createOpActualizarTrasladoResp(OpActualizarTrasladoRespTipo value) {
        return new JAXBElement<OpActualizarTrasladoRespTipo>(_OpActualizarTrasladoResp_QNAME, OpActualizarTrasladoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1", name = "OpCrearTrasladoFallo")
    public JAXBElement<FalloTipo> createOpCrearTrasladoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearTrasladoFallo_QNAME, FalloTipo.class, null, value);
    }

}
