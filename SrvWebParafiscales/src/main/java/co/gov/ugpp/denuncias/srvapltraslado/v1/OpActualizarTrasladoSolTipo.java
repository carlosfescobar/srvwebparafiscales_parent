
package co.gov.ugpp.denuncias.srvapltraslado.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;


/**
 * <p>Clase Java para OpActualizarTrasladoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarTrasladoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="traslado" type="{http://www.ugpp.gov.co/schema/Denuncias/TrasladoTipo/v1}TrasladoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarTrasladoSolTipo", propOrder = {
    "contextoTransaccional",
    "traslado"
})
public class OpActualizarTrasladoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected TrasladoTipo traslado;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad traslado.
     * 
     * @return
     *     possible object is
     *     {@link TrasladoTipo }
     *     
     */
    public TrasladoTipo getTraslado() {
        return traslado;
    }

    /**
     * Define el valor de la propiedad traslado.
     * 
     * @param value
     *     allowed object is
     *     {@link TrasladoTipo }
     *     
     */
    public void setTraslado(TrasladoTipo value) {
        this.traslado = value;
    }

}
