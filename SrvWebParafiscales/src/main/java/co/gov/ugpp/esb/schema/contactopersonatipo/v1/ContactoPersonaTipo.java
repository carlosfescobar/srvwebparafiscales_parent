
package co.gov.ugpp.esb.schema.contactopersonatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;


/**
 * <p>Clase Java para ContactoPersonaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ContactoPersonaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="telefonos" type="{http://www.ugpp.gov.co/esb/schema/TelefonoTipo/v1}TelefonoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="correosElectronicos" type="{http://www.ugpp.gov.co/schema/Transversales/CorreoElectronicoTipo/v1}CorreoElectronicoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="esAutorizaNotificacionElectronica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ubicaciones" type="{http://www.ugpp.gov.co/esb/schema/UbicacionPersonaTipo/v1}UbicacionPersonaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactoPersonaTipo", propOrder = {
    "telefonos",
    "correosElectronicos",
    "esAutorizaNotificacionElectronica",
    "ubicaciones"
})
public class ContactoPersonaTipo {

    @XmlElement(nillable = true)
    protected List<TelefonoTipo> telefonos;
    @XmlElement(nillable = true)
    protected List<CorreoElectronicoTipo> correosElectronicos;
    @XmlElement(nillable = true)
    protected String esAutorizaNotificacionElectronica;
    @XmlElement(nillable = true)
    protected List<UbicacionPersonaTipo> ubicaciones;

    /**
     * Gets the value of the telefonos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the telefonos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTelefonos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TelefonoTipo }
     * 
     * 
     */
    public List<TelefonoTipo> getTelefonos() {
        if (telefonos == null) {
            telefonos = new ArrayList<TelefonoTipo>();
        }
        return this.telefonos;
    }

    /**
     * Gets the value of the correosElectronicos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the correosElectronicos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorreosElectronicos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CorreoElectronicoTipo }
     * 
     * 
     */
    public List<CorreoElectronicoTipo> getCorreosElectronicos() {
        if (correosElectronicos == null) {
            correosElectronicos = new ArrayList<CorreoElectronicoTipo>();
        }
        return this.correosElectronicos;
    }

    /**
     * Obtiene el valor de la propiedad esAutorizaNotificacionElectronica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAutorizaNotificacionElectronica() {
        return esAutorizaNotificacionElectronica;
    }

    /**
     * Define el valor de la propiedad esAutorizaNotificacionElectronica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAutorizaNotificacionElectronica(String value) {
        this.esAutorizaNotificacionElectronica = value;
    }

    /**
     * Gets the value of the ubicaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ubicaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUbicaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UbicacionPersonaTipo }
     * 
     * 
     */
    public List<UbicacionPersonaTipo> getUbicaciones() {
        if (ubicaciones == null) {
            ubicaciones = new ArrayList<UbicacionPersonaTipo>();
        }
        return this.ubicaciones;
    }

}
