
package co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;


/**
 * <p>Clase Java para ContextoTransaccionalTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ContextoTransaccionalTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTx" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;length value="36"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="fechaInicioTx" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="idInstanciaProceso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="48"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="idDefinicionProceso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="48"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="valNombreDefinicionProceso" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="idInstanciaActividad" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="48"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="valNombreDefinicionActividad" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="0"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="idUsuarioAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valClaveUsuarioAplicacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idEmisor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valTamPagina" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="valNumPagina" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="criteriosOrdenamiento" type="{http://www.ugpp.gov.co/esb/schema/CriterioOrdenamientoTipo}CriterioOrdenamientoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContextoTransaccionalTipo", propOrder = {
    "idTx",
    "fechaInicioTx",
    "idInstanciaProceso",
    "idDefinicionProceso",
    "valNombreDefinicionProceso",
    "idInstanciaActividad",
    "valNombreDefinicionActividad",
    "idUsuarioAplicacion",
    "valClaveUsuarioAplicacion",
    "idUsuario",
    "idEmisor",
    "valTamPagina",
    "valNumPagina",
    "criteriosOrdenamiento"
})
public class ContextoTransaccionalTipo {

    @XmlElement(nillable = true)
    protected String idTx;
    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaInicioTx;
    @XmlElement(nillable = true)
    protected String idInstanciaProceso;
    @XmlElement(nillable = true)
    protected String idDefinicionProceso;
    @XmlElement(nillable = true)
    protected String valNombreDefinicionProceso;
    @XmlElement(nillable = true)
    protected String idInstanciaActividad;
    @XmlElement(nillable = true)
    protected String valNombreDefinicionActividad;
    @XmlElement(required = true, nillable = true)
    protected String idUsuarioAplicacion;
    @XmlElement(required = true, nillable = true)
    protected String valClaveUsuarioAplicacion;
    @XmlElement(required = true, nillable = true)
    protected String idUsuario;
    @XmlElement(required = true, nillable = true)
    protected String idEmisor;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valTamPagina;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valNumPagina;
    @XmlElement(nillable = true)
    protected List<CriterioOrdenamientoTipo> criteriosOrdenamiento;

    /**
     * Obtiene el valor de la propiedad idTx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTx() {
        return idTx;
    }

    /**
     * Define el valor de la propiedad idTx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTx(String value) {
        this.idTx = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaInicioTx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaInicioTx() {
        return fechaInicioTx;
    }

    /**
     * Define el valor de la propiedad fechaInicioTx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaInicioTx(Calendar value) {
        this.fechaInicioTx = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    /**
     * Define el valor de la propiedad idInstanciaProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaProceso(String value) {
        this.idInstanciaProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad idDefinicionProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDefinicionProceso() {
        return idDefinicionProceso;
    }

    /**
     * Define el valor de la propiedad idDefinicionProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDefinicionProceso(String value) {
        this.idDefinicionProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreDefinicionProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreDefinicionProceso() {
        return valNombreDefinicionProceso;
    }

    /**
     * Define el valor de la propiedad valNombreDefinicionProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreDefinicionProceso(String value) {
        this.valNombreDefinicionProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaActividad() {
        return idInstanciaActividad;
    }

    /**
     * Define el valor de la propiedad idInstanciaActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaActividad(String value) {
        this.idInstanciaActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreDefinicionActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreDefinicionActividad() {
        return valNombreDefinicionActividad;
    }

    /**
     * Define el valor de la propiedad valNombreDefinicionActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreDefinicionActividad(String value) {
        this.valNombreDefinicionActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuarioAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuarioAplicacion() {
        return idUsuarioAplicacion;
    }

    /**
     * Define el valor de la propiedad idUsuarioAplicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuarioAplicacion(String value) {
        this.idUsuarioAplicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valClaveUsuarioAplicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValClaveUsuarioAplicacion() {
        return valClaveUsuarioAplicacion;
    }

    /**
     * Define el valor de la propiedad valClaveUsuarioAplicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValClaveUsuarioAplicacion(String value) {
        this.valClaveUsuarioAplicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad idEmisor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEmisor() {
        return idEmisor;
    }

    /**
     * Define el valor de la propiedad idEmisor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEmisor(String value) {
        this.idEmisor = value;
    }

    /**
     * Obtiene el valor de la propiedad valTamPagina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValTamPagina() {
        return valTamPagina;
    }

    /**
     * Define el valor de la propiedad valTamPagina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValTamPagina(Long value) {
        this.valTamPagina = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumPagina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValNumPagina() {
        return valNumPagina;
    }

    /**
     * Define el valor de la propiedad valNumPagina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumPagina(Long value) {
        this.valNumPagina = value;
    }

    /**
     * Gets the value of the criteriosOrdenamiento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the criteriosOrdenamiento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCriteriosOrdenamiento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CriterioOrdenamientoTipo }
     * 
     * 
     */
    public List<CriterioOrdenamientoTipo> getCriteriosOrdenamiento() {
        if (criteriosOrdenamiento == null) {
            criteriosOrdenamiento = new ArrayList<CriterioOrdenamientoTipo>();
        }
        return this.criteriosOrdenamiento;
    }

}
