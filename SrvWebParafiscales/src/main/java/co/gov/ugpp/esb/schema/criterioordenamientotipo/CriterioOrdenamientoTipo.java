
package co.gov.ugpp.esb.schema.criterioordenamientotipo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;


/**
 * <p>Clase Java para CriterioOrdenamientoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CriterioOrdenamientoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valOrden" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="valNombreCampo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CriterioOrdenamientoTipo", propOrder = {
    "valOrden",
    "valNombreCampo"
})
public class CriterioOrdenamientoTipo {

    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valOrden;
    @XmlElement(nillable = true)
    protected String valNombreCampo;

    /**
     * Obtiene el valor de la propiedad valOrden.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValOrden() {
        return valOrden;
    }

    /**
     * Define el valor de la propiedad valOrden.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValOrden(Long value) {
        this.valOrden = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreCampo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreCampo() {
        return valNombreCampo;
    }

    /**
     * Define el valor de la propiedad valNombreCampo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreCampo(String value) {
        this.valNombreCampo = value;
    }

}
