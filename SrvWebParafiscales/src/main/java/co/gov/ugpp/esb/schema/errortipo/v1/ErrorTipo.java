
package co.gov.ugpp.esb.schema.errortipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ErrorTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ErrorTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valDescError" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valDescErrorTecnico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorTipo", propOrder = {
    "codError",
    "valDescError",
    "valDescErrorTecnico"
})
public class ErrorTipo {

    @XmlElement(required = true, nillable = true)
    protected String codError;
    @XmlElement(required = true, nillable = true)
    protected String valDescError;
    @XmlElement(required = true, nillable = true)
    protected String valDescErrorTecnico;

    /**
     * Obtiene el valor de la propiedad codError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodError() {
        return codError;
    }

    /**
     * Define el valor de la propiedad codError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodError(String value) {
        this.codError = value;
    }

    /**
     * Obtiene el valor de la propiedad valDescError.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDescError() {
        return valDescError;
    }

    /**
     * Define el valor de la propiedad valDescError.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDescError(String value) {
        this.valDescError = value;
    }

    /**
     * Obtiene el valor de la propiedad valDescErrorTecnico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDescErrorTecnico() {
        return valDescErrorTecnico;
    }

    /**
     * Define el valor de la propiedad valDescErrorTecnico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDescErrorTecnico(String value) {
        this.valDescErrorTecnico = value;
    }

}
