
package co.gov.ugpp.esb.schema.identificaciontipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;


/**
 * <p>Clase Java para IdentificacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="IdentificacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNumeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="municipioExpedicion" type="{http://www.ugpp.gov.co/esb/schema/MunicipioTipo/v1}MunicipioTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificacionTipo", propOrder = {
    "codTipoIdentificacion",
    "valNumeroIdentificacion",
    "municipioExpedicion"
})
public class IdentificacionTipo {

    @XmlElement(nillable = true)
    protected String codTipoIdentificacion;
    @XmlElement(nillable = true)
    protected String valNumeroIdentificacion;
    @XmlElement(nillable = true)
    protected MunicipioTipo municipioExpedicion;

    /**
     * Obtiene el valor de la propiedad codTipoIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    /**
     * Define el valor de la propiedad codTipoIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoIdentificacion(String value) {
        this.codTipoIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad valNumeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumeroIdentificacion(String value) {
        this.valNumeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad municipioExpedicion.
     * 
     * @return
     *     possible object is
     *     {@link MunicipioTipo }
     *     
     */
    public MunicipioTipo getMunicipioExpedicion() {
        return municipioExpedicion;
    }

    /**
     * Define el valor de la propiedad municipioExpedicion.
     * 
     * @param value
     *     allowed object is
     *     {@link MunicipioTipo }
     *     
     */
    public void setMunicipioExpedicion(MunicipioTipo value) {
        this.municipioExpedicion = value;
    }

}
