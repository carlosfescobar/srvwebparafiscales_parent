
package co.gov.ugpp.esb.schema.parametrotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ParametroTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParametroTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLlave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valValor" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParametroTipo", propOrder = {
    "idLlave",
    "valValor"
})
public class ParametroTipo {

    @XmlElement(required = true, nillable = true)
    protected String idLlave;
    @XmlElement(required = true, nillable = true)
    protected String valValor;

    /**
     * Obtiene el valor de la propiedad idLlave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLlave() {
        return idLlave;
    }

    /**
     * Define el valor de la propiedad idLlave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLlave(String value) {
        this.idLlave = value;
    }

    /**
     * Obtiene el valor de la propiedad valValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValValor() {
        return valValor;
    }

    /**
     * Define el valor de la propiedad valValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValValor(String value) {
        this.valValor = value;
    }

}
