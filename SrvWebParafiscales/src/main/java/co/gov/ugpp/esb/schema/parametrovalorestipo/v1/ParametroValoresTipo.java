
package co.gov.ugpp.esb.schema.parametrovalorestipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;


/**
 * <p>Clase Java para ParametroValoresTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ParametroValoresTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLlave" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valValor" type="{http://www.ugpp.gov.co/esb/schema/ParametroTipo/v1}ParametroTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ParametroValoresTipo", propOrder = {
    "idLlave",
    "valValor"
})
public class ParametroValoresTipo {

    @XmlElement(nillable = true)
    protected String idLlave;
    protected List<ParametroTipo> valValor;

    /**
     * Obtiene el valor de la propiedad idLlave.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLlave() {
        return idLlave;
    }

    /**
     * Define el valor de la propiedad idLlave.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLlave(String value) {
        this.idLlave = value;
    }

    /**
     * Gets the value of the valValor property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valValor property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValValor().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroTipo }
     * 
     * 
     */
    public List<ParametroTipo> getValValor() {
        if (valValor == null) {
            valValor = new ArrayList<ParametroTipo>();
        }
        return this.valValor;
    }

}
