
package co.gov.ugpp.esb.schema.personajuridicatipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;


/**
 * <p>Clase Java para PersonaJuridicaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaJuridicaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codFuente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPersona" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo"/>
 *         &lt;element name="valNombreRazonSocial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPersonaJuridica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPersonaJuridica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSeccionActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descSeccionActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDivisionActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDivisionActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codGrupoActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descGrupoActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codClaseActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descClaseActividadEconomica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contacto" type="{http://www.ugpp.gov.co/esb/schema/ContactoPersonaTipo/v1}ContactoPersonaTipo" minOccurs="0"/>
 *         &lt;element name="idRepresentanteLegal" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="idAutorizado" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="idAbogado" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="valNumTrabajadores" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaJuridicaTipo", propOrder = {
    "codFuente",
    "idPersona",
    "valNombreRazonSocial",
    "codTipoPersonaJuridica",
    "descTipoPersonaJuridica",
    "codSeccionActividadEconomica",
    "descSeccionActividadEconomica",
    "codDivisionActividadEconomica",
    "descDivisionActividadEconomica",
    "codGrupoActividadEconomica",
    "descGrupoActividadEconomica",
    "codClaseActividadEconomica",
    "descClaseActividadEconomica",
    "contacto",
    "idRepresentanteLegal",
    "idAutorizado",
    "idAbogado",
    "valNumTrabajadores"
})
public class PersonaJuridicaTipo {

    @XmlElement(nillable = true)
    protected String codFuente;
    @XmlElement(required = true)
    protected IdentificacionTipo idPersona;
    @XmlElement(nillable = true)
    protected String valNombreRazonSocial;
    @XmlElement(nillable = true)
    protected String codTipoPersonaJuridica;
    @XmlElement(nillable = true)
    protected String descTipoPersonaJuridica;
    @XmlElement(nillable = true)
    protected String codSeccionActividadEconomica;
    @XmlElement(nillable = true)
    protected String descSeccionActividadEconomica;
    @XmlElement(nillable = true)
    protected String codDivisionActividadEconomica;
    @XmlElement(nillable = true)
    protected String descDivisionActividadEconomica;
    @XmlElement(nillable = true)
    protected String codGrupoActividadEconomica;
    @XmlElement(nillable = true)
    protected String descGrupoActividadEconomica;
    @XmlElement(nillable = true)
    protected String codClaseActividadEconomica;
    @XmlElement(nillable = true)
    protected String descClaseActividadEconomica;
    @XmlElement(nillable = true)
    protected ContactoPersonaTipo contacto;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idRepresentanteLegal;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idAutorizado;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idAbogado;
    @XmlElement(nillable = true)
    protected String valNumTrabajadores;

    /**
     * Obtiene el valor de la propiedad codFuente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFuente() {
        return codFuente;
    }

    /**
     * Define el valor de la propiedad codFuente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFuente(String value) {
        this.codFuente = value;
    }

    /**
     * Obtiene el valor de la propiedad idPersona.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdPersona() {
        return idPersona;
    }

    /**
     * Define el valor de la propiedad idPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdPersona(IdentificacionTipo value) {
        this.idPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreRazonSocial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreRazonSocial() {
        return valNombreRazonSocial;
    }

    /**
     * Define el valor de la propiedad valNombreRazonSocial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreRazonSocial(String value) {
        this.valNombreRazonSocial = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPersonaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPersonaJuridica() {
        return codTipoPersonaJuridica;
    }

    /**
     * Define el valor de la propiedad codTipoPersonaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPersonaJuridica(String value) {
        this.codTipoPersonaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPersonaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPersonaJuridica() {
        return descTipoPersonaJuridica;
    }

    /**
     * Define el valor de la propiedad descTipoPersonaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPersonaJuridica(String value) {
        this.descTipoPersonaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad codSeccionActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSeccionActividadEconomica() {
        return codSeccionActividadEconomica;
    }

    /**
     * Define el valor de la propiedad codSeccionActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSeccionActividadEconomica(String value) {
        this.codSeccionActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad descSeccionActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSeccionActividadEconomica() {
        return descSeccionActividadEconomica;
    }

    /**
     * Define el valor de la propiedad descSeccionActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSeccionActividadEconomica(String value) {
        this.descSeccionActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad codDivisionActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodDivisionActividadEconomica() {
        return codDivisionActividadEconomica;
    }

    /**
     * Define el valor de la propiedad codDivisionActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodDivisionActividadEconomica(String value) {
        this.codDivisionActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad descDivisionActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDivisionActividadEconomica() {
        return descDivisionActividadEconomica;
    }

    /**
     * Define el valor de la propiedad descDivisionActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDivisionActividadEconomica(String value) {
        this.descDivisionActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad codGrupoActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodGrupoActividadEconomica() {
        return codGrupoActividadEconomica;
    }

    /**
     * Define el valor de la propiedad codGrupoActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodGrupoActividadEconomica(String value) {
        this.codGrupoActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad descGrupoActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescGrupoActividadEconomica() {
        return descGrupoActividadEconomica;
    }

    /**
     * Define el valor de la propiedad descGrupoActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescGrupoActividadEconomica(String value) {
        this.descGrupoActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad codClaseActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodClaseActividadEconomica() {
        return codClaseActividadEconomica;
    }

    /**
     * Define el valor de la propiedad codClaseActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodClaseActividadEconomica(String value) {
        this.codClaseActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad descClaseActividadEconomica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescClaseActividadEconomica() {
        return descClaseActividadEconomica;
    }

    /**
     * Define el valor de la propiedad descClaseActividadEconomica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescClaseActividadEconomica(String value) {
        this.descClaseActividadEconomica = value;
    }

    /**
     * Obtiene el valor de la propiedad contacto.
     * 
     * @return
     *     possible object is
     *     {@link ContactoPersonaTipo }
     *     
     */
    public ContactoPersonaTipo getContacto() {
        return contacto;
    }

    /**
     * Define el valor de la propiedad contacto.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactoPersonaTipo }
     *     
     */
    public void setContacto(ContactoPersonaTipo value) {
        this.contacto = value;
    }

    /**
     * Obtiene el valor de la propiedad idRepresentanteLegal.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdRepresentanteLegal() {
        return idRepresentanteLegal;
    }

    /**
     * Define el valor de la propiedad idRepresentanteLegal.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdRepresentanteLegal(IdentificacionTipo value) {
        this.idRepresentanteLegal = value;
    }

    /**
     * Obtiene el valor de la propiedad idAutorizado.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdAutorizado() {
        return idAutorizado;
    }

    /**
     * Define el valor de la propiedad idAutorizado.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdAutorizado(IdentificacionTipo value) {
        this.idAutorizado = value;
    }

    /**
     * Obtiene el valor de la propiedad idAbogado.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdAbogado() {
        return idAbogado;
    }

    /**
     * Define el valor de la propiedad idAbogado.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdAbogado(IdentificacionTipo value) {
        this.idAbogado = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumTrabajadores.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNumTrabajadores() {
        return valNumTrabajadores;
    }

    /**
     * Define el valor de la propiedad valNumTrabajadores.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumTrabajadores(String value) {
        this.valNumTrabajadores = value;
    }

}
