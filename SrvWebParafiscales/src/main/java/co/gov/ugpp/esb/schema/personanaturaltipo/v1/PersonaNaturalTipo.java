
package co.gov.ugpp.esb.schema.personanaturaltipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;


/**
 * <p>Clase Java para PersonaNaturalTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaNaturalTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codFuente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPersona" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo"/>
 *         &lt;element name="valNombreCompleto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPrimerNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSegundoNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPrimerApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSegundoApellido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descSexo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoCivil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNivelEducativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descNivelEducativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPersonaNatural" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPersonaNatural" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contacto" type="{http://www.ugpp.gov.co/esb/schema/ContactoPersonaTipo/v1}ContactoPersonaTipo" minOccurs="0"/>
 *         &lt;element name="idAutorizado" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="idAbogado" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaNaturalTipo", propOrder = {
    "codFuente",
    "idPersona",
    "valNombreCompleto",
    "valPrimerNombre",
    "valSegundoNombre",
    "valPrimerApellido",
    "valSegundoApellido",
    "codSexo",
    "descSexo",
    "codEstadoCivil",
    "descEstadoCivil",
    "codNivelEducativo",
    "descNivelEducativo",
    "codTipoPersonaNatural",
    "descTipoPersonaNatural",
    "contacto",
    "idAutorizado",
    "idAbogado"
})
@XmlSeeAlso({
    FuncionarioTipo.class
})
public class PersonaNaturalTipo {

    @XmlElement(nillable = true)
    protected String codFuente;
    @XmlElement(required = true)
    protected IdentificacionTipo idPersona;
    @XmlElement(nillable = true)
    protected String valNombreCompleto;
    @XmlElement(nillable = true)
    protected String valPrimerNombre;
    @XmlElement(nillable = true)
    protected String valSegundoNombre;
    @XmlElement(nillable = true)
    protected String valPrimerApellido;
    @XmlElement(nillable = true)
    protected String valSegundoApellido;
    @XmlElement(nillable = true)
    protected String codSexo;
    @XmlElement(nillable = true)
    protected String descSexo;
    @XmlElement(nillable = true)
    protected String codEstadoCivil;
    @XmlElement(nillable = true)
    protected String descEstadoCivil;
    @XmlElement(nillable = true)
    protected String codNivelEducativo;
    @XmlElement(nillable = true)
    protected String descNivelEducativo;
    @XmlElement(nillable = true)
    protected String codTipoPersonaNatural;
    @XmlElement(nillable = true)
    protected String descTipoPersonaNatural;
    @XmlElement(nillable = true)
    protected ContactoPersonaTipo contacto;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idAutorizado;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idAbogado;

    /**
     * Obtiene el valor de la propiedad codFuente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFuente() {
        return codFuente;
    }

    /**
     * Define el valor de la propiedad codFuente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFuente(String value) {
        this.codFuente = value;
    }

    /**
     * Obtiene el valor de la propiedad idPersona.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdPersona() {
        return idPersona;
    }

    /**
     * Define el valor de la propiedad idPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdPersona(IdentificacionTipo value) {
        this.idPersona = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreCompleto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreCompleto() {
        return valNombreCompleto;
    }

    /**
     * Define el valor de la propiedad valNombreCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreCompleto(String value) {
        this.valNombreCompleto = value;
    }

    /**
     * Obtiene el valor de la propiedad valPrimerNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPrimerNombre() {
        return valPrimerNombre;
    }

    /**
     * Define el valor de la propiedad valPrimerNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPrimerNombre(String value) {
        this.valPrimerNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad valSegundoNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSegundoNombre() {
        return valSegundoNombre;
    }

    /**
     * Define el valor de la propiedad valSegundoNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSegundoNombre(String value) {
        this.valSegundoNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad valPrimerApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPrimerApellido() {
        return valPrimerApellido;
    }

    /**
     * Define el valor de la propiedad valPrimerApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPrimerApellido(String value) {
        this.valPrimerApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad valSegundoApellido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSegundoApellido() {
        return valSegundoApellido;
    }

    /**
     * Define el valor de la propiedad valSegundoApellido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSegundoApellido(String value) {
        this.valSegundoApellido = value;
    }

    /**
     * Obtiene el valor de la propiedad codSexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSexo() {
        return codSexo;
    }

    /**
     * Define el valor de la propiedad codSexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSexo(String value) {
        this.codSexo = value;
    }

    /**
     * Obtiene el valor de la propiedad descSexo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescSexo() {
        return descSexo;
    }

    /**
     * Define el valor de la propiedad descSexo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescSexo(String value) {
        this.descSexo = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoCivil() {
        return codEstadoCivil;
    }

    /**
     * Define el valor de la propiedad codEstadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoCivil(String value) {
        this.codEstadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoCivil.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoCivil() {
        return descEstadoCivil;
    }

    /**
     * Define el valor de la propiedad descEstadoCivil.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoCivil(String value) {
        this.descEstadoCivil = value;
    }

    /**
     * Obtiene el valor de la propiedad codNivelEducativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNivelEducativo() {
        return codNivelEducativo;
    }

    /**
     * Define el valor de la propiedad codNivelEducativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNivelEducativo(String value) {
        this.codNivelEducativo = value;
    }

    /**
     * Obtiene el valor de la propiedad descNivelEducativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescNivelEducativo() {
        return descNivelEducativo;
    }

    /**
     * Define el valor de la propiedad descNivelEducativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescNivelEducativo(String value) {
        this.descNivelEducativo = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPersonaNatural.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPersonaNatural() {
        return codTipoPersonaNatural;
    }

    /**
     * Define el valor de la propiedad codTipoPersonaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPersonaNatural(String value) {
        this.codTipoPersonaNatural = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPersonaNatural.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPersonaNatural() {
        return descTipoPersonaNatural;
    }

    /**
     * Define el valor de la propiedad descTipoPersonaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPersonaNatural(String value) {
        this.descTipoPersonaNatural = value;
    }

    /**
     * Obtiene el valor de la propiedad contacto.
     * 
     * @return
     *     possible object is
     *     {@link ContactoPersonaTipo }
     *     
     */
    public ContactoPersonaTipo getContacto() {
        return contacto;
    }

    /**
     * Define el valor de la propiedad contacto.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactoPersonaTipo }
     *     
     */
    public void setContacto(ContactoPersonaTipo value) {
        this.contacto = value;
    }

    /**
     * Obtiene el valor de la propiedad idAutorizado.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdAutorizado() {
        return idAutorizado;
    }

    /**
     * Define el valor de la propiedad idAutorizado.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdAutorizado(IdentificacionTipo value) {
        this.idAutorizado = value;
    }

    /**
     * Obtiene el valor de la propiedad idAbogado.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdAbogado() {
        return idAbogado;
    }

    /**
     * Define el valor de la propiedad idAbogado.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdAbogado(IdentificacionTipo value) {
        this.idAbogado = value;
    }

}
