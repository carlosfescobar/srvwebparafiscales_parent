
package co.gov.ugpp.esb.schema.personatipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para PersonaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PersonaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="personaJuridica" type="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo"/>
 *         &lt;element name="personaNatural" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonaTipo", propOrder = {
    "personaJuridica",
    "personaNatural"
})
public class PersonaTipo {

    @XmlElement(nillable = true)
    protected PersonaJuridicaTipo personaJuridica;
    @XmlElement(nillable = true)
    protected PersonaNaturalTipo personaNatural;

    /**
     * Obtiene el valor de la propiedad personaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public PersonaJuridicaTipo getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     * Define el valor de la propiedad personaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public void setPersonaJuridica(PersonaJuridicaTipo value) {
        this.personaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad personaNatural.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNatural() {
        return personaNatural;
    }

    /**
     * Define el valor de la propiedad personaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNatural(PersonaNaturalTipo value) {
        this.personaNatural = value;
    }

}
