
package co.gov.ugpp.esb.schema.telefonotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para TelefonoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TelefonoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codTipoTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNumeroTelefono" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelefonoTipo", propOrder = {
    "codTipoTelefono",
    "valNumeroTelefono"
})
public class TelefonoTipo {

    @XmlElement(nillable = true)
    protected String codTipoTelefono;
    @XmlElement(nillable = true)
    protected String valNumeroTelefono;

    /**
     * Obtiene el valor de la propiedad codTipoTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoTelefono() {
        return codTipoTelefono;
    }

    /**
     * Define el valor de la propiedad codTipoTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoTelefono(String value) {
        this.codTipoTelefono = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumeroTelefono.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNumeroTelefono() {
        return valNumeroTelefono;
    }

    /**
     * Define el valor de la propiedad valNumeroTelefono.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumeroTelefono(String value) {
        this.valNumeroTelefono = value;
    }

}
