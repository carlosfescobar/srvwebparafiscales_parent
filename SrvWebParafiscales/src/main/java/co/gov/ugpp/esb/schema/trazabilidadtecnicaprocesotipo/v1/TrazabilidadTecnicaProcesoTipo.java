
package co.gov.ugpp.esb.schema.trazabilidadtecnicaprocesotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;


/**
 * <p>Clase Java para TrazabilidadTecnicaProcesoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TrazabilidadTecnicaProcesoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idInstanciaProceso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codEstadoProceso" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="descEstadoProceso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codEstadoActividad" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="descEstadoActividad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idInstanciaActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombreActividad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idDefinicionActividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecEstadoActividad" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRol" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="valNombreRol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDiasCompletarTarea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecCreacionRegistro" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecActualizacionRegistro" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrazabilidadTecnicaProcesoTipo", propOrder = {
    "idInstanciaProceso",
    "codEstadoProceso",
    "descEstadoProceso",
    "codEstadoActividad",
    "descEstadoActividad",
    "idInstanciaActividad",
    "valNombreActividad",
    "idDefinicionActividad",
    "fecEstadoActividad",
    "idUsuario",
    "codRol",
    "valNombreRol",
    "valDiasCompletarTarea",
    "fecCreacionRegistro",
    "fecActualizacionRegistro"
})
public class TrazabilidadTecnicaProcesoTipo {

    @XmlElement(required = true, nillable = true)
    protected String idInstanciaProceso;
    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long codEstadoProceso;
    @XmlElement(required = true, nillable = true)
    protected String descEstadoProceso;
    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long codEstadoActividad;
    @XmlElement(required = true, nillable = true)
    protected String descEstadoActividad;
    @XmlElement(nillable = true)
    protected String idInstanciaActividad;
    @XmlElement(required = true, nillable = true)
    protected String valNombreActividad;
    @XmlElement(nillable = true)
    protected String idDefinicionActividad;
    @XmlElement(required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEstadoActividad;
    @XmlElement(nillable = true)
    protected String idUsuario;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long codRol;
    @XmlElement(nillable = true)
    protected String valNombreRol;
    @XmlElement(nillable = true)
    protected String valDiasCompletarTarea;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecCreacionRegistro;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecActualizacionRegistro;

    /**
     * Obtiene el valor de la propiedad idInstanciaProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    /**
     * Define el valor de la propiedad idInstanciaProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaProceso(String value) {
        this.idInstanciaProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCodEstadoProceso() {
        return codEstadoProceso;
    }

    /**
     * Define el valor de la propiedad codEstadoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoProceso(Long value) {
        this.codEstadoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoProceso() {
        return descEstadoProceso;
    }

    /**
     * Define el valor de la propiedad descEstadoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoProceso(String value) {
        this.descEstadoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCodEstadoActividad() {
        return codEstadoActividad;
    }

    /**
     * Define el valor de la propiedad codEstadoActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoActividad(Long value) {
        this.codEstadoActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoActividad() {
        return descEstadoActividad;
    }

    /**
     * Define el valor de la propiedad descEstadoActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoActividad(String value) {
        this.descEstadoActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaActividad() {
        return idInstanciaActividad;
    }

    /**
     * Define el valor de la propiedad idInstanciaActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaActividad(String value) {
        this.idInstanciaActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreActividad() {
        return valNombreActividad;
    }

    /**
     * Define el valor de la propiedad valNombreActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreActividad(String value) {
        this.valNombreActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad idDefinicionActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDefinicionActividad() {
        return idDefinicionActividad;
    }

    /**
     * Define el valor de la propiedad idDefinicionActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDefinicionActividad(String value) {
        this.idDefinicionActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEstadoActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEstadoActividad() {
        return fecEstadoActividad;
    }

    /**
     * Define el valor de la propiedad fecEstadoActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEstadoActividad(Calendar value) {
        this.fecEstadoActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad codRol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCodRol() {
        return codRol;
    }

    /**
     * Define el valor de la propiedad codRol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRol(Long value) {
        this.codRol = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreRol.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreRol() {
        return valNombreRol;
    }

    /**
     * Define el valor de la propiedad valNombreRol.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreRol(String value) {
        this.valNombreRol = value;
    }

    /**
     * Obtiene el valor de la propiedad valDiasCompletarTarea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiasCompletarTarea() {
        return valDiasCompletarTarea;
    }

    /**
     * Define el valor de la propiedad valDiasCompletarTarea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiasCompletarTarea(String value) {
        this.valDiasCompletarTarea = value;
    }

    /**
     * Obtiene el valor de la propiedad fecCreacionRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecCreacionRegistro() {
        return fecCreacionRegistro;
    }

    /**
     * Define el valor de la propiedad fecCreacionRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecCreacionRegistro(Calendar value) {
        this.fecCreacionRegistro = value;
    }

    /**
     * Obtiene el valor de la propiedad fecActualizacionRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecActualizacionRegistro() {
        return fecActualizacionRegistro;
    }

    /**
     * Define el valor de la propiedad fecActualizacionRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecActualizacionRegistro(Calendar value) {
        this.fecActualizacionRegistro = value;
    }

}
