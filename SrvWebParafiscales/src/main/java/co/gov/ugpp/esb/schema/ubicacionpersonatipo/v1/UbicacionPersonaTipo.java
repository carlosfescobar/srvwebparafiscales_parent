
package co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;


/**
 * <p>Clase Java para UbicacionPersonaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="UbicacionPersonaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="municipio" type="{http://www.ugpp.gov.co/esb/schema/MunicipioTipo/v1}MunicipioTipo" minOccurs="0"/>
 *         &lt;element name="valDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoDireccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UbicacionPersonaTipo", propOrder = {
    "municipio",
    "valDireccion",
    "codTipoDireccion",
    "descTipoDireccion"
})
public class UbicacionPersonaTipo {

    @XmlElement(nillable = true)
    protected MunicipioTipo municipio;
    @XmlElement(nillable = true)
    protected String valDireccion;
    @XmlElement(nillable = true)
    protected String codTipoDireccion;
    @XmlElement(nillable = true)
    protected String descTipoDireccion;

    /**
     * Obtiene el valor de la propiedad municipio.
     * 
     * @return
     *     possible object is
     *     {@link MunicipioTipo }
     *     
     */
    public MunicipioTipo getMunicipio() {
        return municipio;
    }

    /**
     * Define el valor de la propiedad municipio.
     * 
     * @param value
     *     allowed object is
     *     {@link MunicipioTipo }
     *     
     */
    public void setMunicipio(MunicipioTipo value) {
        this.municipio = value;
    }

    /**
     * Obtiene el valor de la propiedad valDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDireccion() {
        return valDireccion;
    }

    /**
     * Define el valor de la propiedad valDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDireccion(String value) {
        this.valDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDireccion() {
        return codTipoDireccion;
    }

    /**
     * Define el valor de la propiedad codTipoDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDireccion(String value) {
        this.codTipoDireccion = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoDireccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoDireccion() {
        return descTipoDireccion;
    }

    /**
     * Define el valor de la propiedad descTipoDireccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoDireccion(String value) {
        this.descTipoDireccion = value;
    }

}
