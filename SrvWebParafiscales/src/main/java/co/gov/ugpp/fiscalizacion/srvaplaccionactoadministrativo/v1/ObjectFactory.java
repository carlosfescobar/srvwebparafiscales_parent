
package co.gov.ugpp.fiscalizacion.srvaplaccionactoadministrativo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplaccionactoadministrativo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarAccionActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpActualizarAccionActoAdministrativoResp");
    private final static QName _OpBuscarPorIdAccionActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpBuscarPorIdAccionActoAdministrativoFallo");
    private final static QName _OpActualizarAccionActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpActualizarAccionActoAdministrativoFallo");
    private final static QName _OpBuscarPorIdAccionActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpBuscarPorIdAccionActoAdministrativoResp");
    private final static QName _OpCrearAccionActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpCrearAccionActoAdministrativoFallo");
    private final static QName _OpActualizarAccionActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpActualizarAccionActoAdministrativoSol");
    private final static QName _OpCrearAccionActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpCrearAccionActoAdministrativoResp");
    private final static QName _OpCrearAccionActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpCrearAccionActoAdministrativoSol");
    private final static QName _OpBuscarPorIdAccionActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", "OpBuscarPorIdAccionActoAdministrativoSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplaccionactoadministrativo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarAccionActoAdministrativoRespTipo }
     * 
     */
    public OpActualizarAccionActoAdministrativoRespTipo createOpActualizarAccionActoAdministrativoRespTipo() {
        return new OpActualizarAccionActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAccionActoAdministrativoRespTipo }
     * 
     */
    public OpBuscarPorIdAccionActoAdministrativoRespTipo createOpBuscarPorIdAccionActoAdministrativoRespTipo() {
        return new OpBuscarPorIdAccionActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAccionActoAdministrativoSolTipo }
     * 
     */
    public OpActualizarAccionActoAdministrativoSolTipo createOpActualizarAccionActoAdministrativoSolTipo() {
        return new OpActualizarAccionActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAccionActoAdministrativoRespTipo }
     * 
     */
    public OpCrearAccionActoAdministrativoRespTipo createOpCrearAccionActoAdministrativoRespTipo() {
        return new OpCrearAccionActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAccionActoAdministrativoSolTipo }
     * 
     */
    public OpBuscarPorIdAccionActoAdministrativoSolTipo createOpBuscarPorIdAccionActoAdministrativoSolTipo() {
        return new OpBuscarPorIdAccionActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAccionActoAdministrativoSolTipo }
     * 
     */
    public OpCrearAccionActoAdministrativoSolTipo createOpCrearAccionActoAdministrativoSolTipo() {
        return new OpCrearAccionActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAccionActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpActualizarAccionActoAdministrativoResp")
    public JAXBElement<OpActualizarAccionActoAdministrativoRespTipo> createOpActualizarAccionActoAdministrativoResp(OpActualizarAccionActoAdministrativoRespTipo value) {
        return new JAXBElement<OpActualizarAccionActoAdministrativoRespTipo>(_OpActualizarAccionActoAdministrativoResp_QNAME, OpActualizarAccionActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpBuscarPorIdAccionActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAccionActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAccionActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpActualizarAccionActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpActualizarAccionActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAccionActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAccionActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpBuscarPorIdAccionActoAdministrativoResp")
    public JAXBElement<OpBuscarPorIdAccionActoAdministrativoRespTipo> createOpBuscarPorIdAccionActoAdministrativoResp(OpBuscarPorIdAccionActoAdministrativoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAccionActoAdministrativoRespTipo>(_OpBuscarPorIdAccionActoAdministrativoResp_QNAME, OpBuscarPorIdAccionActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpCrearAccionActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpCrearAccionActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAccionActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAccionActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpActualizarAccionActoAdministrativoSol")
    public JAXBElement<OpActualizarAccionActoAdministrativoSolTipo> createOpActualizarAccionActoAdministrativoSol(OpActualizarAccionActoAdministrativoSolTipo value) {
        return new JAXBElement<OpActualizarAccionActoAdministrativoSolTipo>(_OpActualizarAccionActoAdministrativoSol_QNAME, OpActualizarAccionActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAccionActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpCrearAccionActoAdministrativoResp")
    public JAXBElement<OpCrearAccionActoAdministrativoRespTipo> createOpCrearAccionActoAdministrativoResp(OpCrearAccionActoAdministrativoRespTipo value) {
        return new JAXBElement<OpCrearAccionActoAdministrativoRespTipo>(_OpCrearAccionActoAdministrativoResp_QNAME, OpCrearAccionActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAccionActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpCrearAccionActoAdministrativoSol")
    public JAXBElement<OpCrearAccionActoAdministrativoSolTipo> createOpCrearAccionActoAdministrativoSol(OpCrearAccionActoAdministrativoSolTipo value) {
        return new JAXBElement<OpCrearAccionActoAdministrativoSolTipo>(_OpCrearAccionActoAdministrativoSol_QNAME, OpCrearAccionActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAccionActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAccionActoAdministrativo/v1", name = "OpBuscarPorIdAccionActoAdministrativoSol")
    public JAXBElement<OpBuscarPorIdAccionActoAdministrativoSolTipo> createOpBuscarPorIdAccionActoAdministrativoSol(OpBuscarPorIdAccionActoAdministrativoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAccionActoAdministrativoSolTipo>(_OpBuscarPorIdAccionActoAdministrativoSol_QNAME, OpBuscarPorIdAccionActoAdministrativoSolTipo.class, null, value);
    }

}
