
package co.gov.ugpp.fiscalizacion.srvaplaccionactoadministrativo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.accionactoadministrativotipo.v1.AccionActoAdministrativoTipo;


/**
 * <p>Clase Java para OpActualizarAccionActoAdministrativoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarAccionActoAdministrativoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="accionActoAdministrativo" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/AccionActoAdministrativoTipo/v1}AccionActoAdministrativoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarAccionActoAdministrativoSolTipo", propOrder = {
    "contextoTransaccional",
    "accionActoAdministrativo"
})
public class OpActualizarAccionActoAdministrativoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AccionActoAdministrativoTipo accionActoAdministrativo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad accionActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link AccionActoAdministrativoTipo }
     *     
     */
    public AccionActoAdministrativoTipo getAccionActoAdministrativo() {
        return accionActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad accionActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link AccionActoAdministrativoTipo }
     *     
     */
    public void setAccionActoAdministrativo(AccionActoAdministrativoTipo value) {
        this.accionActoAdministrativo = value;
    }

}
