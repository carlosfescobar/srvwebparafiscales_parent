
package co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearAgrupacionFiscalizacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpCrearAgrupacionFiscalizacionActoSol");
    private final static QName _OpCrearAgrupacionFiscalizacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpCrearAgrupacionFiscalizacionActoResp");
    private final static QName _OpCrearAgrupacionFiscalizacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpCrearAgrupacionFiscalizacionActoFallo");
    private final static QName _OpBuscarPorCriteriosAgrupacionFiscalizacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpBuscarPorCriteriosAgrupacionFiscalizacionActoSol");
    private final static QName _OpBuscarPorCriteriosAgrupacionFiscalizacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpBuscarPorCriteriosAgrupacionFiscalizacionActoResp");
    private final static QName _OpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", "OpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionFiscalizacionActoRespTipo }
     * 
     */
    public OpCrearAgrupacionFiscalizacionActoRespTipo createOpCrearAgrupacionFiscalizacionActoRespTipo() {
        return new OpCrearAgrupacionFiscalizacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionFiscalizacionActoSolTipo }
     * 
     */
    public OpCrearAgrupacionFiscalizacionActoSolTipo createOpCrearAgrupacionFiscalizacionActoSolTipo() {
        return new OpCrearAgrupacionFiscalizacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo createOpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo() {
        return new OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo createOpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo() {
        return new OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionFiscalizacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpCrearAgrupacionFiscalizacionActoSol")
    public JAXBElement<OpCrearAgrupacionFiscalizacionActoSolTipo> createOpCrearAgrupacionFiscalizacionActoSol(OpCrearAgrupacionFiscalizacionActoSolTipo value) {
        return new JAXBElement<OpCrearAgrupacionFiscalizacionActoSolTipo>(_OpCrearAgrupacionFiscalizacionActoSol_QNAME, OpCrearAgrupacionFiscalizacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionFiscalizacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpCrearAgrupacionFiscalizacionActoResp")
    public JAXBElement<OpCrearAgrupacionFiscalizacionActoRespTipo> createOpCrearAgrupacionFiscalizacionActoResp(OpCrearAgrupacionFiscalizacionActoRespTipo value) {
        return new JAXBElement<OpCrearAgrupacionFiscalizacionActoRespTipo>(_OpCrearAgrupacionFiscalizacionActoResp_QNAME, OpCrearAgrupacionFiscalizacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpCrearAgrupacionFiscalizacionActoFallo")
    public JAXBElement<FalloTipo> createOpCrearAgrupacionFiscalizacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAgrupacionFiscalizacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpBuscarPorCriteriosAgrupacionFiscalizacionActoSol")
    public JAXBElement<OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo> createOpBuscarPorCriteriosAgrupacionFiscalizacionActoSol(OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo>(_OpBuscarPorCriteriosAgrupacionFiscalizacionActoSol_QNAME, OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpBuscarPorCriteriosAgrupacionFiscalizacionActoResp")
    public JAXBElement<OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo> createOpBuscarPorCriteriosAgrupacionFiscalizacionActoResp(OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo>(_OpBuscarPorCriteriosAgrupacionFiscalizacionActoResp_QNAME, OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1", name = "OpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

}
