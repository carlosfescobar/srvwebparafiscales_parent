
package co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;


/**
 * <p>Clase Java para OpCrearAgrupacionFiscalizacionActoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearAgrupacionFiscalizacionActoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="agrupacionFiscalizacionActo" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/AgrupacionFiscalizacionActoTipo/v1}AgrupacionFiscalizacionActoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearAgrupacionFiscalizacionActoSolTipo", propOrder = {
    "contextoTransaccional",
    "agrupacionFiscalizacionActo"
})
public class OpCrearAgrupacionFiscalizacionActoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad agrupacionFiscalizacionActo.
     * 
     * @return
     *     possible object is
     *     {@link AgrupacionFiscalizacionActoTipo }
     *     
     */
    public AgrupacionFiscalizacionActoTipo getAgrupacionFiscalizacionActo() {
        return agrupacionFiscalizacionActo;
    }

    /**
     * Define el valor de la propiedad agrupacionFiscalizacionActo.
     * 
     * @param value
     *     allowed object is
     *     {@link AgrupacionFiscalizacionActoTipo }
     *     
     */
    public void setAgrupacionFiscalizacionActo(AgrupacionFiscalizacionActoTipo value) {
        this.agrupacionFiscalizacionActo = value;
    }

}
