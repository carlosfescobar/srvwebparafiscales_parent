
package co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosBusquedaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpBuscarPorCriteriosBusquedaResp");
    private final static QName _OpBuscarPorCriteriosBusquedaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpBuscarPorCriteriosBusquedaFallo");
    private final static QName _OpCrearBusquedaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpCrearBusquedaResp");
    private final static QName _OpActualizarBusquedaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpActualizarBusquedaSol");
    private final static QName _OpBuscarPorCriteriosBusquedaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpBuscarPorCriteriosBusquedaSol");
    private final static QName _OpCrearBusquedaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpCrearBusquedaSol");
    private final static QName _OpActualizarBusquedaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpActualizarBusquedaResp");
    private final static QName _OpActualizarBusquedaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpActualizarBusquedaFallo");
    private final static QName _OpCrearBusquedaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", "OpCrearBusquedaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarBusquedaRespTipo }
     * 
     */
    public OpActualizarBusquedaRespTipo createOpActualizarBusquedaRespTipo() {
        return new OpActualizarBusquedaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearBusquedaSolTipo }
     * 
     */
    public OpCrearBusquedaSolTipo createOpCrearBusquedaSolTipo() {
        return new OpCrearBusquedaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosBusquedaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosBusquedaSolTipo createOpBuscarPorCriteriosBusquedaSolTipo() {
        return new OpBuscarPorCriteriosBusquedaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarBusquedaSolTipo }
     * 
     */
    public OpActualizarBusquedaSolTipo createOpActualizarBusquedaSolTipo() {
        return new OpActualizarBusquedaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearBusquedaRespTipo }
     * 
     */
    public OpCrearBusquedaRespTipo createOpCrearBusquedaRespTipo() {
        return new OpCrearBusquedaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosBusquedaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosBusquedaRespTipo createOpBuscarPorCriteriosBusquedaRespTipo() {
        return new OpBuscarPorCriteriosBusquedaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosBusquedaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpBuscarPorCriteriosBusquedaResp")
    public JAXBElement<OpBuscarPorCriteriosBusquedaRespTipo> createOpBuscarPorCriteriosBusquedaResp(OpBuscarPorCriteriosBusquedaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosBusquedaRespTipo>(_OpBuscarPorCriteriosBusquedaResp_QNAME, OpBuscarPorCriteriosBusquedaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpBuscarPorCriteriosBusquedaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosBusquedaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosBusquedaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearBusquedaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpCrearBusquedaResp")
    public JAXBElement<OpCrearBusquedaRespTipo> createOpCrearBusquedaResp(OpCrearBusquedaRespTipo value) {
        return new JAXBElement<OpCrearBusquedaRespTipo>(_OpCrearBusquedaResp_QNAME, OpCrearBusquedaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarBusquedaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpActualizarBusquedaSol")
    public JAXBElement<OpActualizarBusquedaSolTipo> createOpActualizarBusquedaSol(OpActualizarBusquedaSolTipo value) {
        return new JAXBElement<OpActualizarBusquedaSolTipo>(_OpActualizarBusquedaSol_QNAME, OpActualizarBusquedaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosBusquedaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpBuscarPorCriteriosBusquedaSol")
    public JAXBElement<OpBuscarPorCriteriosBusquedaSolTipo> createOpBuscarPorCriteriosBusquedaSol(OpBuscarPorCriteriosBusquedaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosBusquedaSolTipo>(_OpBuscarPorCriteriosBusquedaSol_QNAME, OpBuscarPorCriteriosBusquedaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearBusquedaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpCrearBusquedaSol")
    public JAXBElement<OpCrearBusquedaSolTipo> createOpCrearBusquedaSol(OpCrearBusquedaSolTipo value) {
        return new JAXBElement<OpCrearBusquedaSolTipo>(_OpCrearBusquedaSol_QNAME, OpCrearBusquedaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarBusquedaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpActualizarBusquedaResp")
    public JAXBElement<OpActualizarBusquedaRespTipo> createOpActualizarBusquedaResp(OpActualizarBusquedaRespTipo value) {
        return new JAXBElement<OpActualizarBusquedaRespTipo>(_OpActualizarBusquedaResp_QNAME, OpActualizarBusquedaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpActualizarBusquedaFallo")
    public JAXBElement<FalloTipo> createOpActualizarBusquedaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarBusquedaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1", name = "OpCrearBusquedaFallo")
    public JAXBElement<FalloTipo> createOpCrearBusquedaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearBusquedaFallo_QNAME, FalloTipo.class, null, value);
    }

}
