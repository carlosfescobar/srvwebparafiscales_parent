
package co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionFallo");
    private final static QName _OpActualizarFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionSol");
    private final static QName _OpBuscarPorCriteriosFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorCriteriosFiscalizacionResp");
    private final static QName _OpBuscarPorCriteriosFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorCriteriosFiscalizacionFallo");
    private final static QName _OpBuscarPorIdFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorIdFiscalizacionFallo");
    private final static QName _OpBuscarPorCriteriosFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorCriteriosFiscalizacionSol");
    private final static QName _OpCrearFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionResp");
    private final static QName _OpBuscarPorIdFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorIdFiscalizacionSol");
    private final static QName _OpCrearFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionFallo");
    private final static QName _OpBuscarPorIdFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpBuscarPorIdFiscalizacionResp");
    private final static QName _OpActualizarFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionResp");
    private final static QName _OpCrearFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdFiscalizacionRespTipo }
     * 
     */
    public OpBuscarPorIdFiscalizacionRespTipo createOpBuscarPorIdFiscalizacionRespTipo() {
        return new OpBuscarPorIdFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearFiscalizacionSolTipo }
     * 
     */
    public OpCrearFiscalizacionSolTipo createOpCrearFiscalizacionSolTipo() {
        return new OpCrearFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFiscalizacionRespTipo }
     * 
     */
    public OpActualizarFiscalizacionRespTipo createOpActualizarFiscalizacionRespTipo() {
        return new OpActualizarFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFiscalizacionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosFiscalizacionSolTipo createOpBuscarPorCriteriosFiscalizacionSolTipo() {
        return new OpBuscarPorCriteriosFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdFiscalizacionSolTipo }
     * 
     */
    public OpBuscarPorIdFiscalizacionSolTipo createOpBuscarPorIdFiscalizacionSolTipo() {
        return new OpBuscarPorIdFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearFiscalizacionRespTipo }
     * 
     */
    public OpCrearFiscalizacionRespTipo createOpCrearFiscalizacionRespTipo() {
        return new OpCrearFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFiscalizacionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosFiscalizacionRespTipo createOpBuscarPorCriteriosFiscalizacionRespTipo() {
        return new OpBuscarPorCriteriosFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFiscalizacionSolTipo }
     * 
     */
    public OpActualizarFiscalizacionSolTipo createOpActualizarFiscalizacionSolTipo() {
        return new OpActualizarFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionSol")
    public JAXBElement<OpActualizarFiscalizacionSolTipo> createOpActualizarFiscalizacionSol(OpActualizarFiscalizacionSolTipo value) {
        return new JAXBElement<OpActualizarFiscalizacionSolTipo>(_OpActualizarFiscalizacionSol_QNAME, OpActualizarFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorCriteriosFiscalizacionResp")
    public JAXBElement<OpBuscarPorCriteriosFiscalizacionRespTipo> createOpBuscarPorCriteriosFiscalizacionResp(OpBuscarPorCriteriosFiscalizacionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFiscalizacionRespTipo>(_OpBuscarPorCriteriosFiscalizacionResp_QNAME, OpBuscarPorCriteriosFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorCriteriosFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorIdFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorCriteriosFiscalizacionSol")
    public JAXBElement<OpBuscarPorCriteriosFiscalizacionSolTipo> createOpBuscarPorCriteriosFiscalizacionSol(OpBuscarPorCriteriosFiscalizacionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFiscalizacionSolTipo>(_OpBuscarPorCriteriosFiscalizacionSol_QNAME, OpBuscarPorCriteriosFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionResp")
    public JAXBElement<OpCrearFiscalizacionRespTipo> createOpCrearFiscalizacionResp(OpCrearFiscalizacionRespTipo value) {
        return new JAXBElement<OpCrearFiscalizacionRespTipo>(_OpCrearFiscalizacionResp_QNAME, OpCrearFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorIdFiscalizacionSol")
    public JAXBElement<OpBuscarPorIdFiscalizacionSolTipo> createOpBuscarPorIdFiscalizacionSol(OpBuscarPorIdFiscalizacionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdFiscalizacionSolTipo>(_OpBuscarPorIdFiscalizacionSol_QNAME, OpBuscarPorIdFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpCrearFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpBuscarPorIdFiscalizacionResp")
    public JAXBElement<OpBuscarPorIdFiscalizacionRespTipo> createOpBuscarPorIdFiscalizacionResp(OpBuscarPorIdFiscalizacionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdFiscalizacionRespTipo>(_OpBuscarPorIdFiscalizacionResp_QNAME, OpBuscarPorIdFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionResp")
    public JAXBElement<OpActualizarFiscalizacionRespTipo> createOpActualizarFiscalizacionResp(OpActualizarFiscalizacionRespTipo value) {
        return new JAXBElement<OpActualizarFiscalizacionRespTipo>(_OpActualizarFiscalizacionResp_QNAME, OpActualizarFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionSol")
    public JAXBElement<OpCrearFiscalizacionSolTipo> createOpCrearFiscalizacionSol(OpCrearFiscalizacionSolTipo value) {
        return new JAXBElement<OpCrearFiscalizacionSolTipo>(_OpCrearFiscalizacionSol_QNAME, OpCrearFiscalizacionSolTipo.class, null, value);
    }

}
