
package co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;


/**
 * <p>Clase Java para OpActualizarFiscalizacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarFiscalizacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="fiscalizacion" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/FiscalizacionTipo/v1}FiscalizacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarFiscalizacionSolTipo", propOrder = {
    "contextoTransaccional",
    "fiscalizacion"
})
public class OpActualizarFiscalizacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected FiscalizacionTipo fiscalizacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad fiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link FiscalizacionTipo }
     *     
     */
    public FiscalizacionTipo getFiscalizacion() {
        return fiscalizacion;
    }

    /**
     * Define el valor de la propiedad fiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link FiscalizacionTipo }
     *     
     */
    public void setFiscalizacion(FiscalizacionTipo value) {
        this.fiscalizacion = value;
    }

}
