
package co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;


/**
 * <p>Clase Java para OpBuscarPorIdFiscalizacionRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdFiscalizacionRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="fiscalizacion" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/FiscalizacionTipo/v1}FiscalizacionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdFiscalizacionRespTipo", propOrder = {
    "contextoRespuesta",
    "fiscalizacion"
})
public class OpBuscarPorIdFiscalizacionRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(nillable = true)
    protected List<FiscalizacionTipo> fiscalizacion;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Gets the value of the fiscalizacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fiscalizacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFiscalizacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FiscalizacionTipo }
     * 
     * 
     */
    public List<FiscalizacionTipo> getFiscalizacion() {
        if (fiscalizacion == null) {
            fiscalizacion = new ArrayList<FiscalizacionTipo>();
        }
        return this.fiscalizacion;
    }

}
