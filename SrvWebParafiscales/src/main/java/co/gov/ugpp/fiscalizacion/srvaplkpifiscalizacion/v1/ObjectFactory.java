
package co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarFiscalizacionKPISol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", "OpConsultarFiscalizacionKPISol");
    private final static QName _OpConsultarFiscalizacionKPIResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", "OpConsultarFiscalizacionKPIResp");
    private final static QName _OpConsultarFiscalizacionKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", "OpConsultarFiscalizacionKPIFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarFiscalizacionKPISolTipo }
     * 
     */
    public OpConsultarFiscalizacionKPISolTipo createOpConsultarFiscalizacionKPISolTipo() {
        return new OpConsultarFiscalizacionKPISolTipo();
    }

    /**
     * Create an instance of {@link OpConsultarFiscalizacionKPIRespTipo }
     * 
     */
    public OpConsultarFiscalizacionKPIRespTipo createOpConsultarFiscalizacionKPIRespTipo() {
        return new OpConsultarFiscalizacionKPIRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarFiscalizacionKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", name = "OpConsultarFiscalizacionKPISol")
    public JAXBElement<OpConsultarFiscalizacionKPISolTipo> createOpConsultarFiscalizacionKPISol(OpConsultarFiscalizacionKPISolTipo value) {
        return new JAXBElement<OpConsultarFiscalizacionKPISolTipo>(_OpConsultarFiscalizacionKPISol_QNAME, OpConsultarFiscalizacionKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarFiscalizacionKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", name = "OpConsultarFiscalizacionKPIResp")
    public JAXBElement<OpConsultarFiscalizacionKPIRespTipo> createOpConsultarFiscalizacionKPIResp(OpConsultarFiscalizacionKPIRespTipo value) {
        return new JAXBElement<OpConsultarFiscalizacionKPIRespTipo>(_OpConsultarFiscalizacionKPIResp_QNAME, OpConsultarFiscalizacionKPIRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1", name = "OpConsultarFiscalizacionKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarFiscalizacionKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarFiscalizacionKPIFallo_QNAME, FalloTipo.class, null, value);
    }

}
