
package co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;


/**
 * <p>Clase Java para OpConsultarFiscalizacionKPIRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpConsultarFiscalizacionKPIRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="cantidadFiscalizaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fiscalizaciones" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/FiscalizacionTipo/v1}FiscalizacionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpConsultarFiscalizacionKPIRespTipo", propOrder = {
    "contextoRespuesta",
    "cantidadFiscalizaciones",
    "fiscalizaciones"
})
public class OpConsultarFiscalizacionKPIRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected String cantidadFiscalizaciones;
    protected List<FiscalizacionTipo> fiscalizaciones;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadFiscalizaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadFiscalizaciones() {
        return cantidadFiscalizaciones;
    }

    /**
     * Define el valor de la propiedad cantidadFiscalizaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadFiscalizaciones(String value) {
        this.cantidadFiscalizaciones = value;
    }

    /**
     * Gets the value of the fiscalizaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fiscalizaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFiscalizaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FiscalizacionTipo }
     * 
     * 
     */
    public List<FiscalizacionTipo> getFiscalizaciones() {
        if (fiscalizaciones == null) {
            fiscalizaciones = new ArrayList<FiscalizacionTipo>();
        }
        return this.fiscalizaciones;
    }

}
