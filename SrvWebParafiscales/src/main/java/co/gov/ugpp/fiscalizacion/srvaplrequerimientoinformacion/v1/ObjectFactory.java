
package co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearRequerimientoInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpCrearRequerimientoInformacionResp");
    private final static QName _OpActualizarRequerimientoInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpActualizarRequerimientoInformacionFallo");
    private final static QName _OpActualizarRequerimientoInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpActualizarRequerimientoInformacionResp");
    private final static QName _OpCrearRequerimientoInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpCrearRequerimientoInformacionSol");
    private final static QName _OpBuscarPorIdRequerimientoInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpBuscarPorIdRequerimientoInformacionResp");
    private final static QName _OpBuscarPorIdRequerimientoInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpBuscarPorIdRequerimientoInformacionSol");
    private final static QName _OpCrearRequerimientoInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpCrearRequerimientoInformacionFallo");
    private final static QName _OpActualizarRequerimientoInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpActualizarRequerimientoInformacionSol");
    private final static QName _OpBuscarPorIdRequerimientoInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "OpBuscarPorIdRequerimientoInformacionFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearRequerimientoInformacionRespTipo }
     * 
     */
    public OpCrearRequerimientoInformacionRespTipo createOpCrearRequerimientoInformacionRespTipo() {
        return new OpCrearRequerimientoInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearRequerimientoInformacionSolTipo }
     * 
     */
    public OpCrearRequerimientoInformacionSolTipo createOpCrearRequerimientoInformacionSolTipo() {
        return new OpCrearRequerimientoInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRequerimientoInformacionRespTipo }
     * 
     */
    public OpActualizarRequerimientoInformacionRespTipo createOpActualizarRequerimientoInformacionRespTipo() {
        return new OpActualizarRequerimientoInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRequerimientoInformacionSolTipo }
     * 
     */
    public OpBuscarPorIdRequerimientoInformacionSolTipo createOpBuscarPorIdRequerimientoInformacionSolTipo() {
        return new OpBuscarPorIdRequerimientoInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRequerimientoInformacionRespTipo }
     * 
     */
    public OpBuscarPorIdRequerimientoInformacionRespTipo createOpBuscarPorIdRequerimientoInformacionRespTipo() {
        return new OpBuscarPorIdRequerimientoInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRequerimientoInformacionSolTipo }
     * 
     */
    public OpActualizarRequerimientoInformacionSolTipo createOpActualizarRequerimientoInformacionSolTipo() {
        return new OpActualizarRequerimientoInformacionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRequerimientoInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpCrearRequerimientoInformacionResp")
    public JAXBElement<OpCrearRequerimientoInformacionRespTipo> createOpCrearRequerimientoInformacionResp(OpCrearRequerimientoInformacionRespTipo value) {
        return new JAXBElement<OpCrearRequerimientoInformacionRespTipo>(_OpCrearRequerimientoInformacionResp_QNAME, OpCrearRequerimientoInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpActualizarRequerimientoInformacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarRequerimientoInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarRequerimientoInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRequerimientoInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpActualizarRequerimientoInformacionResp")
    public JAXBElement<OpActualizarRequerimientoInformacionRespTipo> createOpActualizarRequerimientoInformacionResp(OpActualizarRequerimientoInformacionRespTipo value) {
        return new JAXBElement<OpActualizarRequerimientoInformacionRespTipo>(_OpActualizarRequerimientoInformacionResp_QNAME, OpActualizarRequerimientoInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRequerimientoInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpCrearRequerimientoInformacionSol")
    public JAXBElement<OpCrearRequerimientoInformacionSolTipo> createOpCrearRequerimientoInformacionSol(OpCrearRequerimientoInformacionSolTipo value) {
        return new JAXBElement<OpCrearRequerimientoInformacionSolTipo>(_OpCrearRequerimientoInformacionSol_QNAME, OpCrearRequerimientoInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRequerimientoInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpBuscarPorIdRequerimientoInformacionResp")
    public JAXBElement<OpBuscarPorIdRequerimientoInformacionRespTipo> createOpBuscarPorIdRequerimientoInformacionResp(OpBuscarPorIdRequerimientoInformacionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdRequerimientoInformacionRespTipo>(_OpBuscarPorIdRequerimientoInformacionResp_QNAME, OpBuscarPorIdRequerimientoInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRequerimientoInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpBuscarPorIdRequerimientoInformacionSol")
    public JAXBElement<OpBuscarPorIdRequerimientoInformacionSolTipo> createOpBuscarPorIdRequerimientoInformacionSol(OpBuscarPorIdRequerimientoInformacionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdRequerimientoInformacionSolTipo>(_OpBuscarPorIdRequerimientoInformacionSol_QNAME, OpBuscarPorIdRequerimientoInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpCrearRequerimientoInformacionFallo")
    public JAXBElement<FalloTipo> createOpCrearRequerimientoInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearRequerimientoInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRequerimientoInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpActualizarRequerimientoInformacionSol")
    public JAXBElement<OpActualizarRequerimientoInformacionSolTipo> createOpActualizarRequerimientoInformacionSol(OpActualizarRequerimientoInformacionSolTipo value) {
        return new JAXBElement<OpActualizarRequerimientoInformacionSolTipo>(_OpActualizarRequerimientoInformacionSol_QNAME, OpActualizarRequerimientoInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", name = "OpBuscarPorIdRequerimientoInformacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdRequerimientoInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdRequerimientoInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

}
