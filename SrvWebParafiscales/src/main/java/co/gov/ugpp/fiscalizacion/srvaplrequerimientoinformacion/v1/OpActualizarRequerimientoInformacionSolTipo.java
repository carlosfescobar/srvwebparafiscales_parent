
package co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.requerimientoinformaciontipo.v1.RequerimientoInformacionTipo;


/**
 * <p>Clase Java para OpActualizarRequerimientoInformacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarRequerimientoInformacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="requerimientoInformacion" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/RequerimientoInformacionTipo/v1}RequerimientoInformacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarRequerimientoInformacionSolTipo", propOrder = {
    "contextoTransaccional",
    "requerimientoInformacion"
})
public class OpActualizarRequerimientoInformacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected RequerimientoInformacionTipo requerimientoInformacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad requerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link RequerimientoInformacionTipo }
     *     
     */
    public RequerimientoInformacionTipo getRequerimientoInformacion() {
        return requerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad requerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link RequerimientoInformacionTipo }
     *     
     */
    public void setRequerimientoInformacion(RequerimientoInformacionTipo value) {
        this.requerimientoInformacion = value;
    }

}
