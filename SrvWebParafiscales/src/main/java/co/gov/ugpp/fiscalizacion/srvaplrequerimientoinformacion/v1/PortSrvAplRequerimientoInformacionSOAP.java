
package co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplRequerimientoInformacionSOAP", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1.ObjectFactory.class,
    co.gov.ugpp.schema.fiscalizacion.requerimientoinformaciontipo.v1.ObjectFactory.class
})
public interface PortSrvAplRequerimientoInformacionSOAP {


    /**
     * 
     * @param msjOpCrearRequerimientoInformacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1.OpCrearRequerimientoInformacionRespTipo
     * @throws MsjOpCrearRequerimientoInformacionFallo
     */
    @WebMethod(operationName = "OpCrearRequerimientoInformacion", action = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1/OpCrearRequerimientoInformacion")
    @WebResult(name = "OpCrearRequerimientoInformacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpCrearRequerimientoInformacionResp")
    public OpCrearRequerimientoInformacionRespTipo opCrearRequerimientoInformacion(
        @WebParam(name = "OpCrearRequerimientoInformacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpCrearRequerimientoInformacionSol")
        OpCrearRequerimientoInformacionSolTipo msjOpCrearRequerimientoInformacionSol)
        throws MsjOpCrearRequerimientoInformacionFallo
    ;

    /**
     * 
     * @param msjOpActualizarRequerimientoInformacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1.OpActualizarRequerimientoInformacionRespTipo
     * @throws MsjOpActualizarRequerimientoInformacionFallo
     */
    @WebMethod(operationName = "OpActualizarRequerimientoInformacion", action = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1/OpActualizarRequerimientoInformacion")
    @WebResult(name = "OpActualizarRequerimientoInformacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpActualizarRequerimientoInformacionResp")
    public OpActualizarRequerimientoInformacionRespTipo opActualizarRequerimientoInformacion(
        @WebParam(name = "OpActualizarRequerimientoInformacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpActualizarRequerimientoInformacionSol")
        OpActualizarRequerimientoInformacionSolTipo msjOpActualizarRequerimientoInformacionSol)
        throws MsjOpActualizarRequerimientoInformacionFallo
    ;

    /**
     * 
     * @param msjOpBuscarPorIdRequerimientoInformacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1.OpBuscarPorIdRequerimientoInformacionRespTipo
     * @throws MsjOpBuscarPorIdRequerimientoInformacionFallo
     */
    @WebMethod(operationName = "OpBuscarPorIdRequerimientoInformacion", action = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1/OpBuscarPorIdRequerimientoInformacion")
    @WebResult(name = "OpBuscarPorIdRequerimientoInformacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpBuscarPorIdRequerimientoInformacionResp")
    public OpBuscarPorIdRequerimientoInformacionRespTipo opBuscarPorIdRequerimientoInformacion(
        @WebParam(name = "OpBuscarPorIdRequerimientoInformacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", partName = "msjOpBuscarPorIdRequerimientoInformacionSol")
        OpBuscarPorIdRequerimientoInformacionSolTipo msjOpBuscarPorIdRequerimientoInformacionSol)
        throws MsjOpBuscarPorIdRequerimientoInformacionFallo
    ;

}
