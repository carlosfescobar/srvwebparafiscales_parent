
package co.gov.ugpp.fiscalizacion.srvaplrequerimientoinformacion.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplRequerimientoInformacion", targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplRequerimientoInformacion.wsdl")
public class SrvAplRequerimientoInformacion
    extends Service
{

    private final static URL SRVAPLREQUERIMIENTOINFORMACION_WSDL_LOCATION;
    private final static WebServiceException SRVAPLREQUERIMIENTOINFORMACION_EXCEPTION;
    private final static QName SRVAPLREQUERIMIENTOINFORMACION_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "SrvAplRequerimientoInformacion");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplRequerimientoInformacion.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLREQUERIMIENTOINFORMACION_WSDL_LOCATION = url;
        SRVAPLREQUERIMIENTOINFORMACION_EXCEPTION = e;
    }

    public SrvAplRequerimientoInformacion() {
        super(__getWsdlLocation(), SRVAPLREQUERIMIENTOINFORMACION_QNAME);
    }

    public SrvAplRequerimientoInformacion(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLREQUERIMIENTOINFORMACION_QNAME, features);
    }

    public SrvAplRequerimientoInformacion(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLREQUERIMIENTOINFORMACION_QNAME);
    }

    public SrvAplRequerimientoInformacion(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLREQUERIMIENTOINFORMACION_QNAME, features);
    }

    public SrvAplRequerimientoInformacion(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplRequerimientoInformacion(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplRequerimientoInformacionSOAP
     */
    @WebEndpoint(name = "portSrvAplRequerimientoInformacionSOAP")
    public PortSrvAplRequerimientoInformacionSOAP getPortSrvAplRequerimientoInformacionSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "portSrvAplRequerimientoInformacionSOAP"), PortSrvAplRequerimientoInformacionSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplRequerimientoInformacionSOAP
     */
    @WebEndpoint(name = "portSrvAplRequerimientoInformacionSOAP")
    public PortSrvAplRequerimientoInformacionSOAP getPortSrvAplRequerimientoInformacionSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRequerimientoInformacion/v1", "portSrvAplRequerimientoInformacionSOAP"), PortSrvAplRequerimientoInformacionSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLREQUERIMIENTOINFORMACION_EXCEPTION!= null) {
            throw SRVAPLREQUERIMIENTOINFORMACION_EXCEPTION;
        }
        return SRVAPLREQUERIMIENTOINFORMACION_WSDL_LOCATION;
    }

}
