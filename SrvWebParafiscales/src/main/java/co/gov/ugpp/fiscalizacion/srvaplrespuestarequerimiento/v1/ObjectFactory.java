
package co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarRespuestaRequerimientoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpActualizarRespuestaRequerimientoSol");
    private final static QName _OpBuscarPorCriteriosRespuestaRequerimientoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpBuscarPorCriteriosRespuestaRequerimientoSol");
    private final static QName _OpBuscarPorCriteriosRespuestaRequerimientoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpBuscarPorCriteriosRespuestaRequerimientoResp");
    private final static QName _OpBuscarPorCriteriosRespuestaRequerimientoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpBuscarPorCriteriosRespuestaRequerimientoFallo");
    private final static QName _OpCrearRespuestaRequerimientoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpCrearRespuestaRequerimientoResp");
    private final static QName _OpActualizarRespuestaRequerimientoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpActualizarRespuestaRequerimientoResp");
    private final static QName _OpCrearRespuestaRequerimientoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpCrearRespuestaRequerimientoSol");
    private final static QName _OpActualizarRespuestaRequerimientoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpActualizarRespuestaRequerimientoFallo");
    private final static QName _OpCrearRespuestaRequerimientoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", "OpCrearRespuestaRequerimientoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosRespuestaRequerimientoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosRespuestaRequerimientoRespTipo createOpBuscarPorCriteriosRespuestaRequerimientoRespTipo() {
        return new OpBuscarPorCriteriosRespuestaRequerimientoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRespuestaRequerimientoSolTipo }
     * 
     */
    public OpActualizarRespuestaRequerimientoSolTipo createOpActualizarRespuestaRequerimientoSolTipo() {
        return new OpActualizarRespuestaRequerimientoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosRespuestaRequerimientoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosRespuestaRequerimientoSolTipo createOpBuscarPorCriteriosRespuestaRequerimientoSolTipo() {
        return new OpBuscarPorCriteriosRespuestaRequerimientoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearRespuestaRequerimientoRespTipo }
     * 
     */
    public OpCrearRespuestaRequerimientoRespTipo createOpCrearRespuestaRequerimientoRespTipo() {
        return new OpCrearRespuestaRequerimientoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearRespuestaRequerimientoSolTipo }
     * 
     */
    public OpCrearRespuestaRequerimientoSolTipo createOpCrearRespuestaRequerimientoSolTipo() {
        return new OpCrearRespuestaRequerimientoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRespuestaRequerimientoRespTipo }
     * 
     */
    public OpActualizarRespuestaRequerimientoRespTipo createOpActualizarRespuestaRequerimientoRespTipo() {
        return new OpActualizarRespuestaRequerimientoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRespuestaRequerimientoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpActualizarRespuestaRequerimientoSol")
    public JAXBElement<OpActualizarRespuestaRequerimientoSolTipo> createOpActualizarRespuestaRequerimientoSol(OpActualizarRespuestaRequerimientoSolTipo value) {
        return new JAXBElement<OpActualizarRespuestaRequerimientoSolTipo>(_OpActualizarRespuestaRequerimientoSol_QNAME, OpActualizarRespuestaRequerimientoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosRespuestaRequerimientoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpBuscarPorCriteriosRespuestaRequerimientoSol")
    public JAXBElement<OpBuscarPorCriteriosRespuestaRequerimientoSolTipo> createOpBuscarPorCriteriosRespuestaRequerimientoSol(OpBuscarPorCriteriosRespuestaRequerimientoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosRespuestaRequerimientoSolTipo>(_OpBuscarPorCriteriosRespuestaRequerimientoSol_QNAME, OpBuscarPorCriteriosRespuestaRequerimientoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosRespuestaRequerimientoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpBuscarPorCriteriosRespuestaRequerimientoResp")
    public JAXBElement<OpBuscarPorCriteriosRespuestaRequerimientoRespTipo> createOpBuscarPorCriteriosRespuestaRequerimientoResp(OpBuscarPorCriteriosRespuestaRequerimientoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosRespuestaRequerimientoRespTipo>(_OpBuscarPorCriteriosRespuestaRequerimientoResp_QNAME, OpBuscarPorCriteriosRespuestaRequerimientoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpBuscarPorCriteriosRespuestaRequerimientoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosRespuestaRequerimientoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosRespuestaRequerimientoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRespuestaRequerimientoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpCrearRespuestaRequerimientoResp")
    public JAXBElement<OpCrearRespuestaRequerimientoRespTipo> createOpCrearRespuestaRequerimientoResp(OpCrearRespuestaRequerimientoRespTipo value) {
        return new JAXBElement<OpCrearRespuestaRequerimientoRespTipo>(_OpCrearRespuestaRequerimientoResp_QNAME, OpCrearRespuestaRequerimientoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRespuestaRequerimientoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpActualizarRespuestaRequerimientoResp")
    public JAXBElement<OpActualizarRespuestaRequerimientoRespTipo> createOpActualizarRespuestaRequerimientoResp(OpActualizarRespuestaRequerimientoRespTipo value) {
        return new JAXBElement<OpActualizarRespuestaRequerimientoRespTipo>(_OpActualizarRespuestaRequerimientoResp_QNAME, OpActualizarRespuestaRequerimientoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRespuestaRequerimientoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpCrearRespuestaRequerimientoSol")
    public JAXBElement<OpCrearRespuestaRequerimientoSolTipo> createOpCrearRespuestaRequerimientoSol(OpCrearRespuestaRequerimientoSolTipo value) {
        return new JAXBElement<OpCrearRespuestaRequerimientoSolTipo>(_OpCrearRespuestaRequerimientoSol_QNAME, OpCrearRespuestaRequerimientoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpActualizarRespuestaRequerimientoFallo")
    public JAXBElement<FalloTipo> createOpActualizarRespuestaRequerimientoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarRespuestaRequerimientoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1", name = "OpCrearRespuestaRequerimientoFallo")
    public JAXBElement<FalloTipo> createOpCrearRespuestaRequerimientoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearRespuestaRequerimientoFallo_QNAME, FalloTipo.class, null, value);
    }

}
