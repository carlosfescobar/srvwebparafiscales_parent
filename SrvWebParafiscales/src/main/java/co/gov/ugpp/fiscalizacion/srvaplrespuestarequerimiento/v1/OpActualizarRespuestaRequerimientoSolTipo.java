
package co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientotipo.v1.RespuestaRequerimientoTipo;


/**
 * <p>Clase Java para OpActualizarRespuestaRequerimientoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarRespuestaRequerimientoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="respuestaRequerimiento" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/RespuestaRequerimientoTipo/v1}RespuestaRequerimientoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarRespuestaRequerimientoSolTipo", propOrder = {
    "contextoTransaccional",
    "respuestaRequerimiento"
})
public class OpActualizarRespuestaRequerimientoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected RespuestaRequerimientoTipo respuestaRequerimiento;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad respuestaRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaRequerimientoTipo }
     *     
     */
    public RespuestaRequerimientoTipo getRespuestaRequerimiento() {
        return respuestaRequerimiento;
    }

    /**
     * Define el valor de la propiedad respuestaRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaRequerimientoTipo }
     *     
     */
    public void setRespuestaRequerimiento(RespuestaRequerimientoTipo value) {
        this.respuestaRequerimiento = value;
    }

}
