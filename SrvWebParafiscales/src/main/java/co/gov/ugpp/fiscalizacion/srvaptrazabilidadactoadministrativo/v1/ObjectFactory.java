
package co.gov.ugpp.fiscalizacion.srvaptrazabilidadactoadministrativo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizacion.srvaptrazabilidadactoadministrativo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarTrazabilidadActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpActualizarTrazabilidadActoAdministrativoFallo");
    private final static QName _OpCrearTrazabilidadActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpCrearTrazabilidadActoAdministrativoFallo");
    private final static QName _OpCrearTrazabilidadActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpCrearTrazabilidadActoAdministrativoResp");
    private final static QName _OpBuscarPorIdTrazabilidadActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpBuscarPorIdTrazabilidadActoAdministrativoResp");
    private final static QName _OpActualizarTrazabilidadActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpActualizarTrazabilidadActoAdministrativoSol");
    private final static QName _OpCrearTrazabilidadActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpCrearTrazabilidadActoAdministrativoSol");
    private final static QName _OpBuscarPorIdTrazabilidadActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpBuscarPorIdTrazabilidadActoAdministrativoSol");
    private final static QName _OpActualizarTrazabilidadActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpActualizarTrazabilidadActoAdministrativoResp");
    private final static QName _OpBuscarPorIdTrazabilidadActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", "OpBuscarPorIdTrazabilidadActoAdministrativoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizacion.srvaptrazabilidadactoadministrativo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadActoAdministrativoSolTipo }
     * 
     */
    public OpActualizarTrazabilidadActoAdministrativoSolTipo createOpActualizarTrazabilidadActoAdministrativoSolTipo() {
        return new OpActualizarTrazabilidadActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadActoAdministrativoRespTipo }
     * 
     */
    public OpActualizarTrazabilidadActoAdministrativoRespTipo createOpActualizarTrazabilidadActoAdministrativoRespTipo() {
        return new OpActualizarTrazabilidadActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo }
     * 
     */
    public OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo createOpBuscarPorIdTrazabilidadActoAdministrativoSolTipo() {
        return new OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadActoAdministrativoSolTipo }
     * 
     */
    public OpCrearTrazabilidadActoAdministrativoSolTipo createOpCrearTrazabilidadActoAdministrativoSolTipo() {
        return new OpCrearTrazabilidadActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo }
     * 
     */
    public OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo createOpBuscarPorIdTrazabilidadActoAdministrativoRespTipo() {
        return new OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadActoAdministrativoRespTipo }
     * 
     */
    public OpCrearTrazabilidadActoAdministrativoRespTipo createOpCrearTrazabilidadActoAdministrativoRespTipo() {
        return new OpCrearTrazabilidadActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpActualizarTrazabilidadActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpActualizarTrazabilidadActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarTrazabilidadActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpCrearTrazabilidadActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpCrearTrazabilidadActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearTrazabilidadActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpCrearTrazabilidadActoAdministrativoResp")
    public JAXBElement<OpCrearTrazabilidadActoAdministrativoRespTipo> createOpCrearTrazabilidadActoAdministrativoResp(OpCrearTrazabilidadActoAdministrativoRespTipo value) {
        return new JAXBElement<OpCrearTrazabilidadActoAdministrativoRespTipo>(_OpCrearTrazabilidadActoAdministrativoResp_QNAME, OpCrearTrazabilidadActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpBuscarPorIdTrazabilidadActoAdministrativoResp")
    public JAXBElement<OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo> createOpBuscarPorIdTrazabilidadActoAdministrativoResp(OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo>(_OpBuscarPorIdTrazabilidadActoAdministrativoResp_QNAME, OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpActualizarTrazabilidadActoAdministrativoSol")
    public JAXBElement<OpActualizarTrazabilidadActoAdministrativoSolTipo> createOpActualizarTrazabilidadActoAdministrativoSol(OpActualizarTrazabilidadActoAdministrativoSolTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadActoAdministrativoSolTipo>(_OpActualizarTrazabilidadActoAdministrativoSol_QNAME, OpActualizarTrazabilidadActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpCrearTrazabilidadActoAdministrativoSol")
    public JAXBElement<OpCrearTrazabilidadActoAdministrativoSolTipo> createOpCrearTrazabilidadActoAdministrativoSol(OpCrearTrazabilidadActoAdministrativoSolTipo value) {
        return new JAXBElement<OpCrearTrazabilidadActoAdministrativoSolTipo>(_OpCrearTrazabilidadActoAdministrativoSol_QNAME, OpCrearTrazabilidadActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpBuscarPorIdTrazabilidadActoAdministrativoSol")
    public JAXBElement<OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo> createOpBuscarPorIdTrazabilidadActoAdministrativoSol(OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo>(_OpBuscarPorIdTrazabilidadActoAdministrativoSol_QNAME, OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpActualizarTrazabilidadActoAdministrativoResp")
    public JAXBElement<OpActualizarTrazabilidadActoAdministrativoRespTipo> createOpActualizarTrazabilidadActoAdministrativoResp(OpActualizarTrazabilidadActoAdministrativoRespTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadActoAdministrativoRespTipo>(_OpActualizarTrazabilidadActoAdministrativoResp_QNAME, OpActualizarTrazabilidadActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvApTrazabilidadActoAdministrativo/v1", name = "OpBuscarPorIdTrazabilidadActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdTrazabilidadActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdTrazabilidadActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

}
