
package co.gov.ugpp.fiscalizacion.srvaptrazabilidadactoadministrativo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizacion.trazabilidadactoadministrativotipo.v1.TrazabilidadActoAdministrativoTipo;


/**
 * <p>Clase Java para OpCrearTrazabilidadActoAdministrativoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearTrazabilidadActoAdministrativoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="trazabilidadActoAdministrativo" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/TrazabilidadActoAdministrativoTipo/v1}TrazabilidadActoAdministrativoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearTrazabilidadActoAdministrativoSolTipo", propOrder = {
    "contextoTransaccional",
    "trazabilidadActoAdministrativo"
})
public class OpCrearTrazabilidadActoAdministrativoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad trazabilidadActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link TrazabilidadActoAdministrativoTipo }
     *     
     */
    public TrazabilidadActoAdministrativoTipo getTrazabilidadActoAdministrativo() {
        return trazabilidadActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad trazabilidadActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link TrazabilidadActoAdministrativoTipo }
     *     
     */
    public void setTrazabilidadActoAdministrativo(TrazabilidadActoAdministrativoTipo value) {
        this.trazabilidadActoAdministrativo = value;
    }

}
