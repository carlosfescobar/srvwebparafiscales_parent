
package co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionFallo");
    private final static QName _OpLanzarProgramaFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpLanzarProgramaFiscalizacionResp");
    private final static QName _OpActualizarFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionSol");
    private final static QName _OpCrearFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionFallo");
    private final static QName _OpCrearFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionResp");
    private final static QName _OpLanzarProgramaFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpLanzarProgramaFiscalizacionSol");
    private final static QName _OpLanzarProgramaFiscalizacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpLanzarProgramaFiscalizacionFallo");
    private final static QName _OpCrearFiscalizacionSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpCrearFiscalizacionSol");
    private final static QName _OpActualizarFiscalizacionResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", "OpActualizarFiscalizacionResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpLanzarProgramaFiscalizacionSolTipo }
     * 
     */
    public OpLanzarProgramaFiscalizacionSolTipo createOpLanzarProgramaFiscalizacionSolTipo() {
        return new OpLanzarProgramaFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearFiscalizacionSolTipo }
     * 
     */
    public OpCrearFiscalizacionSolTipo createOpCrearFiscalizacionSolTipo() {
        return new OpCrearFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFiscalizacionRespTipo }
     * 
     */
    public OpActualizarFiscalizacionRespTipo createOpActualizarFiscalizacionRespTipo() {
        return new OpActualizarFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearFiscalizacionRespTipo }
     * 
     */
    public OpCrearFiscalizacionRespTipo createOpCrearFiscalizacionRespTipo() {
        return new OpCrearFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpLanzarProgramaFiscalizacionRespTipo }
     * 
     */
    public OpLanzarProgramaFiscalizacionRespTipo createOpLanzarProgramaFiscalizacionRespTipo() {
        return new OpLanzarProgramaFiscalizacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFiscalizacionSolTipo }
     * 
     */
    public OpActualizarFiscalizacionSolTipo createOpActualizarFiscalizacionSolTipo() {
        return new OpActualizarFiscalizacionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLanzarProgramaFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpLanzarProgramaFiscalizacionResp")
    public JAXBElement<OpLanzarProgramaFiscalizacionRespTipo> createOpLanzarProgramaFiscalizacionResp(OpLanzarProgramaFiscalizacionRespTipo value) {
        return new JAXBElement<OpLanzarProgramaFiscalizacionRespTipo>(_OpLanzarProgramaFiscalizacionResp_QNAME, OpLanzarProgramaFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionSol")
    public JAXBElement<OpActualizarFiscalizacionSolTipo> createOpActualizarFiscalizacionSol(OpActualizarFiscalizacionSolTipo value) {
        return new JAXBElement<OpActualizarFiscalizacionSolTipo>(_OpActualizarFiscalizacionSol_QNAME, OpActualizarFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpCrearFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionResp")
    public JAXBElement<OpCrearFiscalizacionRespTipo> createOpCrearFiscalizacionResp(OpCrearFiscalizacionRespTipo value) {
        return new JAXBElement<OpCrearFiscalizacionRespTipo>(_OpCrearFiscalizacionResp_QNAME, OpCrearFiscalizacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLanzarProgramaFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpLanzarProgramaFiscalizacionSol")
    public JAXBElement<OpLanzarProgramaFiscalizacionSolTipo> createOpLanzarProgramaFiscalizacionSol(OpLanzarProgramaFiscalizacionSolTipo value) {
        return new JAXBElement<OpLanzarProgramaFiscalizacionSolTipo>(_OpLanzarProgramaFiscalizacionSol_QNAME, OpLanzarProgramaFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpLanzarProgramaFiscalizacionFallo")
    public JAXBElement<FalloTipo> createOpLanzarProgramaFiscalizacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpLanzarProgramaFiscalizacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFiscalizacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpCrearFiscalizacionSol")
    public JAXBElement<OpCrearFiscalizacionSolTipo> createOpCrearFiscalizacionSol(OpCrearFiscalizacionSolTipo value) {
        return new JAXBElement<OpCrearFiscalizacionSolTipo>(_OpCrearFiscalizacionSol_QNAME, OpCrearFiscalizacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFiscalizacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", name = "OpActualizarFiscalizacionResp")
    public JAXBElement<OpActualizarFiscalizacionRespTipo> createOpActualizarFiscalizacionResp(OpActualizarFiscalizacionRespTipo value) {
        return new JAXBElement<OpActualizarFiscalizacionRespTipo>(_OpActualizarFiscalizacionResp_QNAME, OpActualizarFiscalizacionRespTipo.class, null, value);
    }

}
