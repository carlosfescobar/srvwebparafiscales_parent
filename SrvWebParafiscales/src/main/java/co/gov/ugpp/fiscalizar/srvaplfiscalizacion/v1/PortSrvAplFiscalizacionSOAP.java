
package co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplFiscalizacionSOAP", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contactopersonatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.departamentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.identificaciontipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.municipiotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.personajuridicatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.personanaturaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.telefonotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.ObjectFactory.class,
    co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1.ObjectFactory.class,
    co.gov.ugpp.schema.denuncias.aportantetipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.fiscalizar.fiscalizaciontipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.fiscalizar.llamadatipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.fiscalizar.visitatipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.notificaciones.validacionactoadministrativotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.hallazgodetalletipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.hallazgotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.liquidadortipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.validaciondocumentotipo.v1.ObjectFactory.class
})
public interface PortSrvAplFiscalizacionSOAP {


    /**
     * 
     * @param msjOpCrearFiscalizacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1.OpCrearFiscalizacionRespTipo
     * @throws MsjOpCrearFiscalizacionFallo
     */
    @WebMethod(operationName = "OpCrearFiscalizacion", action = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1/OpCrearFiscalizacion")
    @WebResult(name = "OpCrearFiscalizacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpCrearFiscalizacionResp")
    public OpCrearFiscalizacionRespTipo opCrearFiscalizacion(
        @WebParam(name = "OpCrearFiscalizacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpCrearFiscalizacionSol")
        OpCrearFiscalizacionSolTipo msjOpCrearFiscalizacionSol)
        throws MsjOpCrearFiscalizacionFallo
    ;

    /**
     * 
     * @param msjOpActualizarFiscalizacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1.OpActualizarFiscalizacionRespTipo
     * @throws MsjOpActualizarFiscalizacionFallo
     */
    @WebMethod(operationName = "OpActualizarFiscalizacion", action = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1/OpActualizarFiscalizacion")
    @WebResult(name = "OpActualizarFiscalizacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpActualizarFiscalizacionResp")
    public OpActualizarFiscalizacionRespTipo opActualizarFiscalizacion(
        @WebParam(name = "OpActualizarFiscalizacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpActualizarFiscalizacionSol")
        OpActualizarFiscalizacionSolTipo msjOpActualizarFiscalizacionSol)
        throws MsjOpActualizarFiscalizacionFallo
    ;

    /**
     * 
     * @param msjOpLanzarProgramaFiscalizacionSol
     * @return
     *     returns co.gov.ugpp.fiscalizar.srvaplfiscalizacion.v1.OpLanzarProgramaFiscalizacionRespTipo
     * @throws MsjOpLanzarProgramaFiscalizacionFallo
     */
    @WebMethod(operationName = "OpLanzarProgramaFiscalizacion", action = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1/OpLanzarProgramaFiscalizacion")
    @WebResult(name = "OpLanzarProgramaFiscalizacionResp", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpLanzarProgramaFiscalizacionResp")
    public OpLanzarProgramaFiscalizacionRespTipo opLanzarProgramaFiscalizacion(
        @WebParam(name = "OpLanzarProgramaFiscalizacionSol", targetNamespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplFiscalizacion/v1", partName = "msjOpLanzarProgramaFiscalizacionSol")
        OpLanzarProgramaFiscalizacionSolTipo msjOpLanzarProgramaFiscalizacionSol)
        throws MsjOpLanzarProgramaFiscalizacionFallo
    ;

}
