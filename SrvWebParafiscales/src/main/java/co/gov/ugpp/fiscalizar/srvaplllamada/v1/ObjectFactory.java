
package co.gov.ugpp.fiscalizar.srvaplllamada.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizar.srvaplllamada.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearLlamadaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", "OpCrearLlamadaSol");
    private final static QName _OpCrearLlamadaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", "OpCrearLlamadaResp");
    private final static QName _OpCrearLlamadaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", "OpCrearLlamadaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizar.srvaplllamada.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearLlamadaRespTipo }
     * 
     */
    public OpCrearLlamadaRespTipo createOpCrearLlamadaRespTipo() {
        return new OpCrearLlamadaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearLlamadaSolTipo }
     * 
     */
    public OpCrearLlamadaSolTipo createOpCrearLlamadaSolTipo() {
        return new OpCrearLlamadaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLlamadaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", name = "OpCrearLlamadaSol")
    public JAXBElement<OpCrearLlamadaSolTipo> createOpCrearLlamadaSol(OpCrearLlamadaSolTipo value) {
        return new JAXBElement<OpCrearLlamadaSolTipo>(_OpCrearLlamadaSol_QNAME, OpCrearLlamadaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLlamadaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", name = "OpCrearLlamadaResp")
    public JAXBElement<OpCrearLlamadaRespTipo> createOpCrearLlamadaResp(OpCrearLlamadaRespTipo value) {
        return new JAXBElement<OpCrearLlamadaRespTipo>(_OpCrearLlamadaResp_QNAME, OpCrearLlamadaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplLlamada/v1", name = "OpCrearLlamadaFallo")
    public JAXBElement<FalloTipo> createOpCrearLlamadaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearLlamadaFallo_QNAME, FalloTipo.class, null, value);
    }

}
