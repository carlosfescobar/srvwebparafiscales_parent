
package co.gov.ugpp.fiscalizar.srvaplllamada.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.fiscalizar.llamadatipo.v1.LlamadaTipo;


/**
 * <p>Clase Java para OpCrearLlamadaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearLlamadaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="llamada" type="{http://www.ugpp.gov.co/schema/Fiscalizar/LlamadaTipo/v1}LlamadaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearLlamadaSolTipo", propOrder = {
    "contextoTransaccional",
    "llamada"
})
public class OpCrearLlamadaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected LlamadaTipo llamada;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad llamada.
     * 
     * @return
     *     possible object is
     *     {@link LlamadaTipo }
     *     
     */
    public LlamadaTipo getLlamada() {
        return llamada;
    }

    /**
     * Define el valor de la propiedad llamada.
     * 
     * @param value
     *     allowed object is
     *     {@link LlamadaTipo }
     *     
     */
    public void setLlamada(LlamadaTipo value) {
        this.llamada = value;
    }

}
