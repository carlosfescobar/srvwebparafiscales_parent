
package co.gov.ugpp.fiscalizar.srvaplvisita.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.fiscalizar.srvaplvisita.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearVisitaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpCrearVisitaResp");
    private final static QName _OpCrearVisitaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpCrearVisitaSol");
    private final static QName _OpActualizarVisitaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpActualizarVisitaFallo");
    private final static QName _OpActualizarVisitaResp_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpActualizarVisitaResp");
    private final static QName _OpCrearVisitaFallo_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpCrearVisitaFallo");
    private final static QName _OpActualizarVisitaSol_QNAME = new QName("http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", "OpActualizarVisitaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.fiscalizar.srvaplvisita.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarVisitaSolTipo }
     * 
     */
    public OpActualizarVisitaSolTipo createOpActualizarVisitaSolTipo() {
        return new OpActualizarVisitaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearVisitaSolTipo }
     * 
     */
    public OpCrearVisitaSolTipo createOpCrearVisitaSolTipo() {
        return new OpCrearVisitaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearVisitaRespTipo }
     * 
     */
    public OpCrearVisitaRespTipo createOpCrearVisitaRespTipo() {
        return new OpCrearVisitaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarVisitaRespTipo }
     * 
     */
    public OpActualizarVisitaRespTipo createOpActualizarVisitaRespTipo() {
        return new OpActualizarVisitaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearVisitaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpCrearVisitaResp")
    public JAXBElement<OpCrearVisitaRespTipo> createOpCrearVisitaResp(OpCrearVisitaRespTipo value) {
        return new JAXBElement<OpCrearVisitaRespTipo>(_OpCrearVisitaResp_QNAME, OpCrearVisitaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearVisitaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpCrearVisitaSol")
    public JAXBElement<OpCrearVisitaSolTipo> createOpCrearVisitaSol(OpCrearVisitaSolTipo value) {
        return new JAXBElement<OpCrearVisitaSolTipo>(_OpCrearVisitaSol_QNAME, OpCrearVisitaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpActualizarVisitaFallo")
    public JAXBElement<FalloTipo> createOpActualizarVisitaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarVisitaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarVisitaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpActualizarVisitaResp")
    public JAXBElement<OpActualizarVisitaRespTipo> createOpActualizarVisitaResp(OpActualizarVisitaRespTipo value) {
        return new JAXBElement<OpActualizarVisitaRespTipo>(_OpActualizarVisitaResp_QNAME, OpActualizarVisitaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpCrearVisitaFallo")
    public JAXBElement<FalloTipo> createOpCrearVisitaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearVisitaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarVisitaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Fiscalizar/SrvAplVisita/v1", name = "OpActualizarVisitaSol")
    public JAXBElement<OpActualizarVisitaSolTipo> createOpActualizarVisitaSol(OpActualizarVisitaSolTipo value) {
        return new JAXBElement<OpActualizarVisitaSolTipo>(_OpActualizarVisitaSol_QNAME, OpActualizarVisitaSolTipo.class, null, value);
    }

}
