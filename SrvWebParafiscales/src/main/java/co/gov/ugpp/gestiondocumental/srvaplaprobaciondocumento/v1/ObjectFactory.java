
package co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarAprobacionDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpActualizarAprobacionDocumentoResp");
    private final static QName _OpActualizarAprobacionDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpActualizarAprobacionDocumentoFallo");
    private final static QName _OpCrearAprobacionDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpCrearAprobacionDocumentoResp");
    private final static QName _OpCrearAprobacionDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpCrearAprobacionDocumentoSol");
    private final static QName _OpActualizarAprobacionDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpActualizarAprobacionDocumentoSol");
    private final static QName _OpBuscarPorIdAprobacionDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpBuscarPorIdAprobacionDocumentoSol");
    private final static QName _OpBuscarPorIdAprobacionDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpBuscarPorIdAprobacionDocumentoResp");
    private final static QName _OpCrearAprobacionDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpCrearAprobacionDocumentoFallo");
    private final static QName _OpBuscarPorIdAprobacionDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", "OpBuscarPorIdAprobacionDocumentoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarAprobacionDocumentoRespTipo }
     * 
     */
    public OpActualizarAprobacionDocumentoRespTipo createOpActualizarAprobacionDocumentoRespTipo() {
        return new OpActualizarAprobacionDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAprobacionDocumentoRespTipo }
     * 
     */
    public OpBuscarPorIdAprobacionDocumentoRespTipo createOpBuscarPorIdAprobacionDocumentoRespTipo() {
        return new OpBuscarPorIdAprobacionDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAprobacionDocumentoSolTipo }
     * 
     */
    public OpBuscarPorIdAprobacionDocumentoSolTipo createOpBuscarPorIdAprobacionDocumentoSolTipo() {
        return new OpBuscarPorIdAprobacionDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAprobacionDocumentoSolTipo }
     * 
     */
    public OpActualizarAprobacionDocumentoSolTipo createOpActualizarAprobacionDocumentoSolTipo() {
        return new OpActualizarAprobacionDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAprobacionDocumentoSolTipo }
     * 
     */
    public OpCrearAprobacionDocumentoSolTipo createOpCrearAprobacionDocumentoSolTipo() {
        return new OpCrearAprobacionDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAprobacionDocumentoRespTipo }
     * 
     */
    public OpCrearAprobacionDocumentoRespTipo createOpCrearAprobacionDocumentoRespTipo() {
        return new OpCrearAprobacionDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAprobacionDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpActualizarAprobacionDocumentoResp")
    public JAXBElement<OpActualizarAprobacionDocumentoRespTipo> createOpActualizarAprobacionDocumentoResp(OpActualizarAprobacionDocumentoRespTipo value) {
        return new JAXBElement<OpActualizarAprobacionDocumentoRespTipo>(_OpActualizarAprobacionDocumentoResp_QNAME, OpActualizarAprobacionDocumentoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpActualizarAprobacionDocumentoFallo")
    public JAXBElement<FalloTipo> createOpActualizarAprobacionDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAprobacionDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAprobacionDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpCrearAprobacionDocumentoResp")
    public JAXBElement<OpCrearAprobacionDocumentoRespTipo> createOpCrearAprobacionDocumentoResp(OpCrearAprobacionDocumentoRespTipo value) {
        return new JAXBElement<OpCrearAprobacionDocumentoRespTipo>(_OpCrearAprobacionDocumentoResp_QNAME, OpCrearAprobacionDocumentoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAprobacionDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpCrearAprobacionDocumentoSol")
    public JAXBElement<OpCrearAprobacionDocumentoSolTipo> createOpCrearAprobacionDocumentoSol(OpCrearAprobacionDocumentoSolTipo value) {
        return new JAXBElement<OpCrearAprobacionDocumentoSolTipo>(_OpCrearAprobacionDocumentoSol_QNAME, OpCrearAprobacionDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAprobacionDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpActualizarAprobacionDocumentoSol")
    public JAXBElement<OpActualizarAprobacionDocumentoSolTipo> createOpActualizarAprobacionDocumentoSol(OpActualizarAprobacionDocumentoSolTipo value) {
        return new JAXBElement<OpActualizarAprobacionDocumentoSolTipo>(_OpActualizarAprobacionDocumentoSol_QNAME, OpActualizarAprobacionDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAprobacionDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpBuscarPorIdAprobacionDocumentoSol")
    public JAXBElement<OpBuscarPorIdAprobacionDocumentoSolTipo> createOpBuscarPorIdAprobacionDocumentoSol(OpBuscarPorIdAprobacionDocumentoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAprobacionDocumentoSolTipo>(_OpBuscarPorIdAprobacionDocumentoSol_QNAME, OpBuscarPorIdAprobacionDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAprobacionDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpBuscarPorIdAprobacionDocumentoResp")
    public JAXBElement<OpBuscarPorIdAprobacionDocumentoRespTipo> createOpBuscarPorIdAprobacionDocumentoResp(OpBuscarPorIdAprobacionDocumentoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAprobacionDocumentoRespTipo>(_OpBuscarPorIdAprobacionDocumentoResp_QNAME, OpBuscarPorIdAprobacionDocumentoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpCrearAprobacionDocumentoFallo")
    public JAXBElement<FalloTipo> createOpCrearAprobacionDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAprobacionDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1", name = "OpBuscarPorIdAprobacionDocumentoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAprobacionDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAprobacionDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

}
