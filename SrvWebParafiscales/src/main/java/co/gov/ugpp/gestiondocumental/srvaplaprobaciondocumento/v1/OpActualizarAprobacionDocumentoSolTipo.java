
package co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;


/**
 * <p>Clase Java para OpActualizarAprobacionDocumentoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarAprobacionDocumentoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="aprobacionDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarAprobacionDocumentoSolTipo", propOrder = {
    "contextoTransaccional",
    "aprobacionDocumento"
})
public class OpActualizarAprobacionDocumentoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AprobacionDocumentoTipo aprobacionDocumento;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad aprobacionDocumento.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAprobacionDocumento() {
        return aprobacionDocumento;
    }

    /**
     * Define el valor de la propiedad aprobacionDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAprobacionDocumento(AprobacionDocumentoTipo value) {
        this.aprobacionDocumento = value;
    }

}
