
package co.gov.ugpp.gestiondocumental.srvaplarchivo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplarchivo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPordIdArchivoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpBuscarPordIdArchivoFallo");
    private final static QName _OpActualizarArchivoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpActualizarArchivoSol");
    private final static QName _OpActualizarArchivoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpActualizarArchivoResp");
    private final static QName _OpEliminarPorIdArchivoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpEliminarPorIdArchivoFallo");
    private final static QName _OpActualizarArchivoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpActualizarArchivoFallo");
    private final static QName _OpCrearArchivoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpCrearArchivoSol");
    private final static QName _OpEliminarPorIdArchivoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpEliminarPorIdArchivoSol");
    private final static QName _OpBuscarPorIdArchivoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpBuscarPorIdArchivoSol");
    private final static QName _OpCrearArchivoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpCrearArchivoFallo");
    private final static QName _OpEliminarPorIdArchivoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpEliminarPorIdArchivoResp");
    private final static QName _OpCrearArchivoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpCrearArchivoResp");
    private final static QName _OpBuscarPorIdArchivoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", "OpBuscarPorIdArchivoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplarchivo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearArchivoSolTipo }
     * 
     */
    public OpCrearArchivoSolTipo createOpCrearArchivoSolTipo() {
        return new OpCrearArchivoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarArchivoRespTipo }
     * 
     */
    public OpActualizarArchivoRespTipo createOpActualizarArchivoRespTipo() {
        return new OpActualizarArchivoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarArchivoSolTipo }
     * 
     */
    public OpActualizarArchivoSolTipo createOpActualizarArchivoSolTipo() {
        return new OpActualizarArchivoSolTipo();
    }

    /**
     * Create an instance of {@link OpEliminarPorIdArchivoRespTipo }
     * 
     */
    public OpEliminarPorIdArchivoRespTipo createOpEliminarPorIdArchivoRespTipo() {
        return new OpEliminarPorIdArchivoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdArchivoRespTipo }
     * 
     */
    public OpBuscarPorIdArchivoRespTipo createOpBuscarPorIdArchivoRespTipo() {
        return new OpBuscarPorIdArchivoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearArchivoRespTipo }
     * 
     */
    public OpCrearArchivoRespTipo createOpCrearArchivoRespTipo() {
        return new OpCrearArchivoRespTipo();
    }

    /**
     * Create an instance of {@link OpEliminarPorIdArchivoSolTipo }
     * 
     */
    public OpEliminarPorIdArchivoSolTipo createOpEliminarPorIdArchivoSolTipo() {
        return new OpEliminarPorIdArchivoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdArchivoSolTipo }
     * 
     */
    public OpBuscarPorIdArchivoSolTipo createOpBuscarPorIdArchivoSolTipo() {
        return new OpBuscarPorIdArchivoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpBuscarPordIdArchivoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPordIdArchivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPordIdArchivoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarArchivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpActualizarArchivoSol")
    public JAXBElement<OpActualizarArchivoSolTipo> createOpActualizarArchivoSol(OpActualizarArchivoSolTipo value) {
        return new JAXBElement<OpActualizarArchivoSolTipo>(_OpActualizarArchivoSol_QNAME, OpActualizarArchivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarArchivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpActualizarArchivoResp")
    public JAXBElement<OpActualizarArchivoRespTipo> createOpActualizarArchivoResp(OpActualizarArchivoRespTipo value) {
        return new JAXBElement<OpActualizarArchivoRespTipo>(_OpActualizarArchivoResp_QNAME, OpActualizarArchivoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpEliminarPorIdArchivoFallo")
    public JAXBElement<FalloTipo> createOpEliminarPorIdArchivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpEliminarPorIdArchivoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpActualizarArchivoFallo")
    public JAXBElement<FalloTipo> createOpActualizarArchivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarArchivoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearArchivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpCrearArchivoSol")
    public JAXBElement<OpCrearArchivoSolTipo> createOpCrearArchivoSol(OpCrearArchivoSolTipo value) {
        return new JAXBElement<OpCrearArchivoSolTipo>(_OpCrearArchivoSol_QNAME, OpCrearArchivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpEliminarPorIdArchivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpEliminarPorIdArchivoSol")
    public JAXBElement<OpEliminarPorIdArchivoSolTipo> createOpEliminarPorIdArchivoSol(OpEliminarPorIdArchivoSolTipo value) {
        return new JAXBElement<OpEliminarPorIdArchivoSolTipo>(_OpEliminarPorIdArchivoSol_QNAME, OpEliminarPorIdArchivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdArchivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpBuscarPorIdArchivoSol")
    public JAXBElement<OpBuscarPorIdArchivoSolTipo> createOpBuscarPorIdArchivoSol(OpBuscarPorIdArchivoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdArchivoSolTipo>(_OpBuscarPorIdArchivoSol_QNAME, OpBuscarPorIdArchivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpCrearArchivoFallo")
    public JAXBElement<FalloTipo> createOpCrearArchivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearArchivoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpEliminarPorIdArchivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpEliminarPorIdArchivoResp")
    public JAXBElement<OpEliminarPorIdArchivoRespTipo> createOpEliminarPorIdArchivoResp(OpEliminarPorIdArchivoRespTipo value) {
        return new JAXBElement<OpEliminarPorIdArchivoRespTipo>(_OpEliminarPorIdArchivoResp_QNAME, OpEliminarPorIdArchivoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearArchivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpCrearArchivoResp")
    public JAXBElement<OpCrearArchivoRespTipo> createOpCrearArchivoResp(OpCrearArchivoRespTipo value) {
        return new JAXBElement<OpCrearArchivoRespTipo>(_OpCrearArchivoResp_QNAME, OpCrearArchivoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdArchivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1", name = "OpBuscarPorIdArchivoResp")
    public JAXBElement<OpBuscarPorIdArchivoRespTipo> createOpBuscarPorIdArchivoResp(OpBuscarPorIdArchivoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdArchivoRespTipo>(_OpBuscarPorIdArchivoResp_QNAME, OpBuscarPorIdArchivoRespTipo.class, null, value);
    }

}
