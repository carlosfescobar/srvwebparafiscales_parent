
package co.gov.ugpp.gestiondocumental.srvapldocumento.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvapldocumento.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", "OpCrearDocumentoResp");
    private final static QName _OpCrearDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", "OpCrearDocumentoSol");
    private final static QName _OpCrearDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", "OpCrearDocumentoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvapldocumento.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearDocumentoRespTipo }
     * 
     */
    public OpCrearDocumentoRespTipo createOpCrearDocumentoRespTipo() {
        return new OpCrearDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearDocumentoSolTipo }
     * 
     */
    public OpCrearDocumentoSolTipo createOpCrearDocumentoSolTipo() {
        return new OpCrearDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", name = "OpCrearDocumentoResp")
    public JAXBElement<OpCrearDocumentoRespTipo> createOpCrearDocumentoResp(OpCrearDocumentoRespTipo value) {
        return new JAXBElement<OpCrearDocumentoRespTipo>(_OpCrearDocumentoResp_QNAME, OpCrearDocumentoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", name = "OpCrearDocumentoSol")
    public JAXBElement<OpCrearDocumentoSolTipo> createOpCrearDocumentoSol(OpCrearDocumentoSolTipo value) {
        return new JAXBElement<OpCrearDocumentoSolTipo>(_OpCrearDocumentoSol_QNAME, OpCrearDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplDocumento/v1", name = "OpCrearDocumentoFallo")
    public JAXBElement<FalloTipo> createOpCrearDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

}
