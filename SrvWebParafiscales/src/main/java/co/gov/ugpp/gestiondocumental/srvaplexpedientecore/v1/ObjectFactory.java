
package co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdExpedienteCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpBuscarPorIdExpedienteCOREFallo");
    private final static QName _OpCrearExpedienteCOREResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpCrearExpedienteCOREResp");
    private final static QName _OpCrearExpedienteCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpCrearExpedienteCOREFallo");
    private final static QName _OpCrearExpedienteCORESol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpCrearExpedienteCORESol");
    private final static QName _OpBuscarPorIdExpedienteCOREResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpBuscarPorIdExpedienteCOREResp");
    private final static QName _OpBuscarPorIdExpedienteCORESol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", "OpBuscarPorIdExpedienteCORESol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdExpedienteCORESolTipo }
     * 
     */
    public OpBuscarPorIdExpedienteCORESolTipo createOpBuscarPorIdExpedienteCORESolTipo() {
        return new OpBuscarPorIdExpedienteCORESolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdExpedienteCORERespTipo }
     * 
     */
    public OpBuscarPorIdExpedienteCORERespTipo createOpBuscarPorIdExpedienteCORERespTipo() {
        return new OpBuscarPorIdExpedienteCORERespTipo();
    }

    /**
     * Create an instance of {@link OpCrearExpedienteCORESolTipo }
     * 
     */
    public OpCrearExpedienteCORESolTipo createOpCrearExpedienteCORESolTipo() {
        return new OpCrearExpedienteCORESolTipo();
    }

    /**
     * Create an instance of {@link OpCrearExpedienteCORERespTipo }
     * 
     */
    public OpCrearExpedienteCORERespTipo createOpCrearExpedienteCORERespTipo() {
        return new OpCrearExpedienteCORERespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpBuscarPorIdExpedienteCOREFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdExpedienteCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdExpedienteCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearExpedienteCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpCrearExpedienteCOREResp")
    public JAXBElement<OpCrearExpedienteCORERespTipo> createOpCrearExpedienteCOREResp(OpCrearExpedienteCORERespTipo value) {
        return new JAXBElement<OpCrearExpedienteCORERespTipo>(_OpCrearExpedienteCOREResp_QNAME, OpCrearExpedienteCORERespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpCrearExpedienteCOREFallo")
    public JAXBElement<FalloTipo> createOpCrearExpedienteCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearExpedienteCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearExpedienteCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpCrearExpedienteCORESol")
    public JAXBElement<OpCrearExpedienteCORESolTipo> createOpCrearExpedienteCORESol(OpCrearExpedienteCORESolTipo value) {
        return new JAXBElement<OpCrearExpedienteCORESolTipo>(_OpCrearExpedienteCORESol_QNAME, OpCrearExpedienteCORESolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdExpedienteCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpBuscarPorIdExpedienteCOREResp")
    public JAXBElement<OpBuscarPorIdExpedienteCORERespTipo> createOpBuscarPorIdExpedienteCOREResp(OpBuscarPorIdExpedienteCORERespTipo value) {
        return new JAXBElement<OpBuscarPorIdExpedienteCORERespTipo>(_OpBuscarPorIdExpedienteCOREResp_QNAME, OpBuscarPorIdExpedienteCORERespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdExpedienteCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1", name = "OpBuscarPorIdExpedienteCORESol")
    public JAXBElement<OpBuscarPorIdExpedienteCORESolTipo> createOpBuscarPorIdExpedienteCORESol(OpBuscarPorIdExpedienteCORESolTipo value) {
        return new JAXBElement<OpBuscarPorIdExpedienteCORESolTipo>(_OpBuscarPorIdExpedienteCORESol_QNAME, OpBuscarPorIdExpedienteCORESolTipo.class, null, value);
    }

}
