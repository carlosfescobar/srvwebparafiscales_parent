
package co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosFormatoEstructuraFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", "OpBuscarPorCriteriosFormatoEstructuraFallo");
    private final static QName _OpBuscarPorCriteriosFormatoEstructuraSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", "OpBuscarPorCriteriosFormatoEstructuraSol");
    private final static QName _OpBuscarPorCriteriosFormatoEstructuraResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", "OpBuscarPorCriteriosFormatoEstructuraResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFormatoEstructuraRespTipo }
     * 
     */
    public OpBuscarPorCriteriosFormatoEstructuraRespTipo createOpBuscarPorCriteriosFormatoEstructuraRespTipo() {
        return new OpBuscarPorCriteriosFormatoEstructuraRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFormatoEstructuraSolTipo }
     * 
     */
    public OpBuscarPorCriteriosFormatoEstructuraSolTipo createOpBuscarPorCriteriosFormatoEstructuraSolTipo() {
        return new OpBuscarPorCriteriosFormatoEstructuraSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", name = "OpBuscarPorCriteriosFormatoEstructuraFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosFormatoEstructuraFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosFormatoEstructuraFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFormatoEstructuraSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", name = "OpBuscarPorCriteriosFormatoEstructuraSol")
    public JAXBElement<OpBuscarPorCriteriosFormatoEstructuraSolTipo> createOpBuscarPorCriteriosFormatoEstructuraSol(OpBuscarPorCriteriosFormatoEstructuraSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFormatoEstructuraSolTipo>(_OpBuscarPorCriteriosFormatoEstructuraSol_QNAME, OpBuscarPorCriteriosFormatoEstructuraSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFormatoEstructuraRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1", name = "OpBuscarPorCriteriosFormatoEstructuraResp")
    public JAXBElement<OpBuscarPorCriteriosFormatoEstructuraRespTipo> createOpBuscarPorCriteriosFormatoEstructuraResp(OpBuscarPorCriteriosFormatoEstructuraRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFormatoEstructuraRespTipo>(_OpBuscarPorCriteriosFormatoEstructuraResp_QNAME, OpBuscarPorCriteriosFormatoEstructuraRespTipo.class, null, value);
    }

}
