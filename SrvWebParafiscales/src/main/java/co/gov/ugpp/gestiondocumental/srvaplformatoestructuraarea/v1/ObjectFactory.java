
package co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearFormatoEstructuraAreaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpCrearFormatoEstructuraAreaFallo");
    private final static QName _OpBuscarPorCriteriosFormatoEstructuraAreaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpBuscarPorCriteriosFormatoEstructuraAreaSol");
    private final static QName _OpBuscarPorCriteriosFormatoEstructuraAreaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpBuscarPorCriteriosFormatoEstructuraAreaResp");
    private final static QName _OpBuscarPorCriteriosFormatoEstructuraAreaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpBuscarPorCriteriosFormatoEstructuraAreaFallo");
    private final static QName _OpActualizarFormatoEstructuraAreaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpActualizarFormatoEstructuraAreaResp");
    private final static QName _OpCrearFormatoEstructuraAreaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpCrearFormatoEstructuraAreaResp");
    private final static QName _OpActualizarFormatoEstructuraAreaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpActualizarFormatoEstructuraAreaFallo");
    private final static QName _OpActualizarFormatoEstructuraAreaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpActualizarFormatoEstructuraAreaSol");
    private final static QName _OpCrearFormatoEstructuraAreaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", "OpCrearFormatoEstructuraAreaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearFormatoEstructuraAreaSolTipo }
     * 
     */
    public OpCrearFormatoEstructuraAreaSolTipo createOpCrearFormatoEstructuraAreaSolTipo() {
        return new OpCrearFormatoEstructuraAreaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFormatoEstructuraAreaSolTipo }
     * 
     */
    public OpActualizarFormatoEstructuraAreaSolTipo createOpActualizarFormatoEstructuraAreaSolTipo() {
        return new OpActualizarFormatoEstructuraAreaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarFormatoEstructuraAreaRespTipo }
     * 
     */
    public OpActualizarFormatoEstructuraAreaRespTipo createOpActualizarFormatoEstructuraAreaRespTipo() {
        return new OpActualizarFormatoEstructuraAreaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearFormatoEstructuraAreaRespTipo }
     * 
     */
    public OpCrearFormatoEstructuraAreaRespTipo createOpCrearFormatoEstructuraAreaRespTipo() {
        return new OpCrearFormatoEstructuraAreaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo createOpBuscarPorCriteriosFormatoEstructuraAreaSolTipo() {
        return new OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo createOpBuscarPorCriteriosFormatoEstructuraAreaRespTipo() {
        return new OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpCrearFormatoEstructuraAreaFallo")
    public JAXBElement<FalloTipo> createOpCrearFormatoEstructuraAreaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFormatoEstructuraAreaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpBuscarPorCriteriosFormatoEstructuraAreaSol")
    public JAXBElement<OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo> createOpBuscarPorCriteriosFormatoEstructuraAreaSol(OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo>(_OpBuscarPorCriteriosFormatoEstructuraAreaSol_QNAME, OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpBuscarPorCriteriosFormatoEstructuraAreaResp")
    public JAXBElement<OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo> createOpBuscarPorCriteriosFormatoEstructuraAreaResp(OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo>(_OpBuscarPorCriteriosFormatoEstructuraAreaResp_QNAME, OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpBuscarPorCriteriosFormatoEstructuraAreaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosFormatoEstructuraAreaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosFormatoEstructuraAreaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFormatoEstructuraAreaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpActualizarFormatoEstructuraAreaResp")
    public JAXBElement<OpActualizarFormatoEstructuraAreaRespTipo> createOpActualizarFormatoEstructuraAreaResp(OpActualizarFormatoEstructuraAreaRespTipo value) {
        return new JAXBElement<OpActualizarFormatoEstructuraAreaRespTipo>(_OpActualizarFormatoEstructuraAreaResp_QNAME, OpActualizarFormatoEstructuraAreaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoEstructuraAreaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpCrearFormatoEstructuraAreaResp")
    public JAXBElement<OpCrearFormatoEstructuraAreaRespTipo> createOpCrearFormatoEstructuraAreaResp(OpCrearFormatoEstructuraAreaRespTipo value) {
        return new JAXBElement<OpCrearFormatoEstructuraAreaRespTipo>(_OpCrearFormatoEstructuraAreaResp_QNAME, OpCrearFormatoEstructuraAreaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpActualizarFormatoEstructuraAreaFallo")
    public JAXBElement<FalloTipo> createOpActualizarFormatoEstructuraAreaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarFormatoEstructuraAreaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarFormatoEstructuraAreaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpActualizarFormatoEstructuraAreaSol")
    public JAXBElement<OpActualizarFormatoEstructuraAreaSolTipo> createOpActualizarFormatoEstructuraAreaSol(OpActualizarFormatoEstructuraAreaSolTipo value) {
        return new JAXBElement<OpActualizarFormatoEstructuraAreaSolTipo>(_OpActualizarFormatoEstructuraAreaSol_QNAME, OpActualizarFormatoEstructuraAreaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoEstructuraAreaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1", name = "OpCrearFormatoEstructuraAreaSol")
    public JAXBElement<OpCrearFormatoEstructuraAreaSolTipo> createOpCrearFormatoEstructuraAreaSol(OpCrearFormatoEstructuraAreaSolTipo value) {
        return new JAXBElement<OpCrearFormatoEstructuraAreaSolTipo>(_OpCrearFormatoEstructuraAreaSol_QNAME, OpCrearFormatoEstructuraAreaSolTipo.class, null, value);
    }

}
