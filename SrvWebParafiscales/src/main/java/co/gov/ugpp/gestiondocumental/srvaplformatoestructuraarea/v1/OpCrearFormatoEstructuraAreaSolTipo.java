
package co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1.FormatoEstructuraAreaTipo;


/**
 * <p>Clase Java para OpCrearFormatoEstructuraAreaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearFormatoEstructuraAreaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="formatoEstructuraArea" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoEstructuraAreaTipo/v1}FormatoEstructuraAreaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearFormatoEstructuraAreaSolTipo", propOrder = {
    "contextoTransaccional",
    "formatoEstructuraArea"
})
public class OpCrearFormatoEstructuraAreaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected FormatoEstructuraAreaTipo formatoEstructuraArea;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad formatoEstructuraArea.
     * 
     * @return
     *     possible object is
     *     {@link FormatoEstructuraAreaTipo }
     *     
     */
    public FormatoEstructuraAreaTipo getFormatoEstructuraArea() {
        return formatoEstructuraArea;
    }

    /**
     * Define el valor de la propiedad formatoEstructuraArea.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoEstructuraAreaTipo }
     *     
     */
    public void setFormatoEstructuraArea(FormatoEstructuraAreaTipo value) {
        this.formatoEstructuraArea = value;
    }

}
