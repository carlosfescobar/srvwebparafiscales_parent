
package co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosFuncionarioFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", "OpBuscarPorCriteriosFuncionarioFallo");
    private final static QName _OpBuscarPorCriteriosFuncionarioSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", "OpBuscarPorCriteriosFuncionarioSol");
    private final static QName _OpBuscarPorCriteriosFuncionarioResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", "OpBuscarPorCriteriosFuncionarioResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFuncionarioSolTipo }
     * 
     */
    public OpBuscarPorCriteriosFuncionarioSolTipo createOpBuscarPorCriteriosFuncionarioSolTipo() {
        return new OpBuscarPorCriteriosFuncionarioSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosFuncionarioRespTipo }
     * 
     */
    public OpBuscarPorCriteriosFuncionarioRespTipo createOpBuscarPorCriteriosFuncionarioRespTipo() {
        return new OpBuscarPorCriteriosFuncionarioRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", name = "OpBuscarPorCriteriosFuncionarioFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosFuncionarioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosFuncionarioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFuncionarioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", name = "OpBuscarPorCriteriosFuncionarioSol")
    public JAXBElement<OpBuscarPorCriteriosFuncionarioSolTipo> createOpBuscarPorCriteriosFuncionarioSol(OpBuscarPorCriteriosFuncionarioSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFuncionarioSolTipo>(_OpBuscarPorCriteriosFuncionarioSol_QNAME, OpBuscarPorCriteriosFuncionarioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosFuncionarioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1", name = "OpBuscarPorCriteriosFuncionarioResp")
    public JAXBElement<OpBuscarPorCriteriosFuncionarioRespTipo> createOpBuscarPorCriteriosFuncionarioResp(OpBuscarPorCriteriosFuncionarioRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosFuncionarioRespTipo>(_OpBuscarPorCriteriosFuncionarioResp_QNAME, OpBuscarPorCriteriosFuncionarioRespTipo.class, null, value);
    }

}
