
package co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosTrazaRadicadoSalidaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpBuscarPorCriteriosTrazaRadicadoSalidaSol");
    private final static QName _OpBuscarPorCriteriosTrazaRadicadoSalidaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpBuscarPorCriteriosTrazaRadicadoSalidaResp");
    private final static QName _OpActualizarTrazabilidadRadicadoSalidaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpActualizarTrazabilidadRadicadoSalidaResp");
    private final static QName _OpActualizarTrazabilidadRadicadoSalidaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpActualizarTrazabilidadRadicadoSalidaFallo");
    private final static QName _OpBuscarPorCriteriosTrazaRadicadoSalidaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpBuscarPorCriteriosTrazaRadicadoSalidaFallo");
    private final static QName _OpCrearTrazabilidadRadicadoSalidaFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpCrearTrazabilidadRadicadoSalidaFallo");
    private final static QName _OpCrearTrazabilidadRadicadoSalidaResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpCrearTrazabilidadRadicadoSalidaResp");
    private final static QName _OpActualizarTrazabilidadRadicadoSalidaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpActualizarTrazabilidadRadicadoSalidaSol");
    private final static QName _OpCrearTrazabilidadRadicadoSalidaSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", "OpCrearTrazabilidadRadicadoSalidaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadRadicadoSalidaSolTipo }
     * 
     */
    public OpCrearTrazabilidadRadicadoSalidaSolTipo createOpCrearTrazabilidadRadicadoSalidaSolTipo() {
        return new OpCrearTrazabilidadRadicadoSalidaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadRadicadoSalidaSolTipo }
     * 
     */
    public OpActualizarTrazabilidadRadicadoSalidaSolTipo createOpActualizarTrazabilidadRadicadoSalidaSolTipo() {
        return new OpActualizarTrazabilidadRadicadoSalidaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadRadicadoSalidaRespTipo }
     * 
     */
    public OpCrearTrazabilidadRadicadoSalidaRespTipo createOpCrearTrazabilidadRadicadoSalidaRespTipo() {
        return new OpCrearTrazabilidadRadicadoSalidaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo createOpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo() {
        return new OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadRadicadoSalidaRespTipo }
     * 
     */
    public OpActualizarTrazabilidadRadicadoSalidaRespTipo createOpActualizarTrazabilidadRadicadoSalidaRespTipo() {
        return new OpActualizarTrazabilidadRadicadoSalidaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo createOpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo() {
        return new OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpBuscarPorCriteriosTrazaRadicadoSalidaSol")
    public JAXBElement<OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo> createOpBuscarPorCriteriosTrazaRadicadoSalidaSol(OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo>(_OpBuscarPorCriteriosTrazaRadicadoSalidaSol_QNAME, OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpBuscarPorCriteriosTrazaRadicadoSalidaResp")
    public JAXBElement<OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo> createOpBuscarPorCriteriosTrazaRadicadoSalidaResp(OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo>(_OpBuscarPorCriteriosTrazaRadicadoSalidaResp_QNAME, OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadRadicadoSalidaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpActualizarTrazabilidadRadicadoSalidaResp")
    public JAXBElement<OpActualizarTrazabilidadRadicadoSalidaRespTipo> createOpActualizarTrazabilidadRadicadoSalidaResp(OpActualizarTrazabilidadRadicadoSalidaRespTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadRadicadoSalidaRespTipo>(_OpActualizarTrazabilidadRadicadoSalidaResp_QNAME, OpActualizarTrazabilidadRadicadoSalidaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpActualizarTrazabilidadRadicadoSalidaFallo")
    public JAXBElement<FalloTipo> createOpActualizarTrazabilidadRadicadoSalidaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarTrazabilidadRadicadoSalidaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpBuscarPorCriteriosTrazaRadicadoSalidaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosTrazaRadicadoSalidaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosTrazaRadicadoSalidaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpCrearTrazabilidadRadicadoSalidaFallo")
    public JAXBElement<FalloTipo> createOpCrearTrazabilidadRadicadoSalidaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearTrazabilidadRadicadoSalidaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadRadicadoSalidaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpCrearTrazabilidadRadicadoSalidaResp")
    public JAXBElement<OpCrearTrazabilidadRadicadoSalidaRespTipo> createOpCrearTrazabilidadRadicadoSalidaResp(OpCrearTrazabilidadRadicadoSalidaRespTipo value) {
        return new JAXBElement<OpCrearTrazabilidadRadicadoSalidaRespTipo>(_OpCrearTrazabilidadRadicadoSalidaResp_QNAME, OpCrearTrazabilidadRadicadoSalidaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadRadicadoSalidaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpActualizarTrazabilidadRadicadoSalidaSol")
    public JAXBElement<OpActualizarTrazabilidadRadicadoSalidaSolTipo> createOpActualizarTrazabilidadRadicadoSalidaSol(OpActualizarTrazabilidadRadicadoSalidaSolTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadRadicadoSalidaSolTipo>(_OpActualizarTrazabilidadRadicadoSalidaSol_QNAME, OpActualizarTrazabilidadRadicadoSalidaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadRadicadoSalidaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1", name = "OpCrearTrazabilidadRadicadoSalidaSol")
    public JAXBElement<OpCrearTrazabilidadRadicadoSalidaSolTipo> createOpCrearTrazabilidadRadicadoSalidaSol(OpCrearTrazabilidadRadicadoSalidaSolTipo value) {
        return new JAXBElement<OpCrearTrazabilidadRadicadoSalidaSolTipo>(_OpCrearTrazabilidadRadicadoSalidaSol_QNAME, OpCrearTrazabilidadRadicadoSalidaSolTipo.class, null, value);
    }

}
