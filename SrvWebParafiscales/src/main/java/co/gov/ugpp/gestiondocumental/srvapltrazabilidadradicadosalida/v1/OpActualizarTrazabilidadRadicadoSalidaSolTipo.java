
package co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;


/**
 * <p>Clase Java para OpActualizarTrazabilidadRadicadoSalidaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarTrazabilidadRadicadoSalidaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="acuseEntregaDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AcuseEntregaDocumentoTipo/v1}AcuseEntregaDocumentoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarTrazabilidadRadicadoSalidaSolTipo", propOrder = {
    "contextoTransaccional",
    "acuseEntregaDocumento"
})
public class OpActualizarTrazabilidadRadicadoSalidaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AcuseEntregaDocumentoTipo acuseEntregaDocumento;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad acuseEntregaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link AcuseEntregaDocumentoTipo }
     *     
     */
    public AcuseEntregaDocumentoTipo getAcuseEntregaDocumento() {
        return acuseEntregaDocumento;
    }

    /**
     * Define el valor de la propiedad acuseEntregaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link AcuseEntregaDocumentoTipo }
     *     
     */
    public void setAcuseEntregaDocumento(AcuseEntregaDocumentoTipo value) {
        this.acuseEntregaDocumento = value;
    }

}
