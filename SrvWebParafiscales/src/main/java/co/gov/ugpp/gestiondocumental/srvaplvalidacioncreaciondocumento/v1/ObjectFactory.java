
package co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearValidacionCreacionDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpCrearValidacionCreacionDocumentoResp");
    private final static QName _OpCrearValidacionCreacionDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpCrearValidacionCreacionDocumentoFallo");
    private final static QName _OpBuscarPorCriteriosValidacionCreacionDocumentoFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpBuscarPorCriteriosValidacionCreacionDocumentoFallo");
    private final static QName _OpBuscarPorCriteriosValidacionCreacionDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpBuscarPorCriteriosValidacionCreacionDocumentoSol");
    private final static QName _OpCrearValidacionCreacionDocumentoSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpCrearValidacionCreacionDocumentoSol");
    private final static QName _OpBuscarPorCriteriosValidacionCreacionDocumentoResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", "OpBuscarPorCriteriosValidacionCreacionDocumentoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo createOpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo() {
        return new OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearValidacionCreacionDocumentoRespTipo }
     * 
     */
    public OpCrearValidacionCreacionDocumentoRespTipo createOpCrearValidacionCreacionDocumentoRespTipo() {
        return new OpCrearValidacionCreacionDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearValidacionCreacionDocumentoSolTipo }
     * 
     */
    public OpCrearValidacionCreacionDocumentoSolTipo createOpCrearValidacionCreacionDocumentoSolTipo() {
        return new OpCrearValidacionCreacionDocumentoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo createOpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo() {
        return new OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearValidacionCreacionDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpCrearValidacionCreacionDocumentoResp")
    public JAXBElement<OpCrearValidacionCreacionDocumentoRespTipo> createOpCrearValidacionCreacionDocumentoResp(OpCrearValidacionCreacionDocumentoRespTipo value) {
        return new JAXBElement<OpCrearValidacionCreacionDocumentoRespTipo>(_OpCrearValidacionCreacionDocumentoResp_QNAME, OpCrearValidacionCreacionDocumentoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpCrearValidacionCreacionDocumentoFallo")
    public JAXBElement<FalloTipo> createOpCrearValidacionCreacionDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearValidacionCreacionDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpBuscarPorCriteriosValidacionCreacionDocumentoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosValidacionCreacionDocumentoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosValidacionCreacionDocumentoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpBuscarPorCriteriosValidacionCreacionDocumentoSol")
    public JAXBElement<OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo> createOpBuscarPorCriteriosValidacionCreacionDocumentoSol(OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo>(_OpBuscarPorCriteriosValidacionCreacionDocumentoSol_QNAME, OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearValidacionCreacionDocumentoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpCrearValidacionCreacionDocumentoSol")
    public JAXBElement<OpCrearValidacionCreacionDocumentoSolTipo> createOpCrearValidacionCreacionDocumentoSol(OpCrearValidacionCreacionDocumentoSolTipo value) {
        return new JAXBElement<OpCrearValidacionCreacionDocumentoSolTipo>(_OpCrearValidacionCreacionDocumentoSol_QNAME, OpCrearValidacionCreacionDocumentoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1", name = "OpBuscarPorCriteriosValidacionCreacionDocumentoResp")
    public JAXBElement<OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo> createOpBuscarPorCriteriosValidacionCreacionDocumentoResp(OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo>(_OpBuscarPorCriteriosValidacionCreacionDocumentoResp_QNAME, OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo.class, null, value);
    }

}
