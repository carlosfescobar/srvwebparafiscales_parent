
package co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;


/**
 * <p>Clase Java para OpCrearValidacionCreacionDocumentoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearValidacionCreacionDocumentoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="validacionCreacionDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ValidacionCreacionDocumentoTipo/v1}ValidacionCreacionDocumentoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearValidacionCreacionDocumentoSolTipo", propOrder = {
    "contextoTransaccional",
    "validacionCreacionDocumento"
})
public class OpCrearValidacionCreacionDocumentoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ValidacionCreacionDocumentoTipo validacionCreacionDocumento;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad validacionCreacionDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ValidacionCreacionDocumentoTipo }
     *     
     */
    public ValidacionCreacionDocumentoTipo getValidacionCreacionDocumento() {
        return validacionCreacionDocumento;
    }

    /**
     * Define el valor de la propiedad validacionCreacionDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidacionCreacionDocumentoTipo }
     *     
     */
    public void setValidacionCreacionDocumento(ValidacionCreacionDocumentoTipo value) {
        this.validacionCreacionDocumento = value;
    }

}
