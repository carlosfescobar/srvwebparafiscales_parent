
package co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * <p>Clase Java para msjOpEnviarCorreoInternoUGPPFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpEnviarCorreoInternoUGPPFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1}OpEnviarCorreoInternoUGPPFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpEnviarCorreoInternoUGPPFallo", propOrder = {
    "opEnviarCorreoInternoUGPPFallo"
})
public class MsjOpEnviarCorreoInternoUGPPFallo {

    @XmlElement(name = "OpEnviarCorreoInternoUGPPFallo", namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", nillable = true)
    protected FalloTipo opEnviarCorreoInternoUGPPFallo;

    /**
     * Obtiene el valor de la propiedad opEnviarCorreoInternoUGPPFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpEnviarCorreoInternoUGPPFallo() {
        return opEnviarCorreoInternoUGPPFallo;
    }

    /**
     * Define el valor de la propiedad opEnviarCorreoInternoUGPPFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpEnviarCorreoInternoUGPPFallo(FalloTipo value) {
        this.opEnviarCorreoInternoUGPPFallo = value;
    }

}
