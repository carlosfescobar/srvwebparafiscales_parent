
package co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Input");
    private final static QName _OpEnviarCorreoInternoUGPPResp_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "OpEnviarCorreoInternoUGPPResp");
    private final static QName _OpEnviarCorreoInternoUGPPFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "OpEnviarCorreoInternoUGPPFallo");
    private final static QName _OpEnviarCorreoInternoUGPPSol_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "OpEnviarCorreoInternoUGPPSol");
    private final static QName _PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Output");
    private final static QName _MsjOpEnviarCorreoInternoUGPPFallo_QNAME = new QName("http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", "msjOpEnviarCorreoInternoUGPPFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpEnviarCorreoInternoUGPPRespTipo }
     * 
     */
    public OpEnviarCorreoInternoUGPPRespTipo createOpEnviarCorreoInternoUGPPRespTipo() {
        return new OpEnviarCorreoInternoUGPPRespTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput }
     * 
     */
    public PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput createPortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput() {
        return new PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput();
    }

    /**
     * Create an instance of {@link OpEnviarCorreoInternoUGPPSolTipo }
     * 
     */
    public OpEnviarCorreoInternoUGPPSolTipo createOpEnviarCorreoInternoUGPPSolTipo() {
        return new OpEnviarCorreoInternoUGPPSolTipo();
    }

    /**
     * Create an instance of {@link MsjOpEnviarCorreoInternoUGPPFallo }
     * 
     */
    public MsjOpEnviarCorreoInternoUGPPFallo createMsjOpEnviarCorreoInternoUGPPFallo() {
        return new MsjOpEnviarCorreoInternoUGPPFallo();
    }

    /**
     * Create an instance of {@link PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput }
     * 
     */
    public PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput createPortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput() {
        return new PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Input")
    public JAXBElement<PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput> createPortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput(PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput value) {
        return new JAXBElement<PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput>(_PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput_QNAME, PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpEnviarCorreoInternoUGPPRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "OpEnviarCorreoInternoUGPPResp")
    public JAXBElement<OpEnviarCorreoInternoUGPPRespTipo> createOpEnviarCorreoInternoUGPPResp(OpEnviarCorreoInternoUGPPRespTipo value) {
        return new JAXBElement<OpEnviarCorreoInternoUGPPRespTipo>(_OpEnviarCorreoInternoUGPPResp_QNAME, OpEnviarCorreoInternoUGPPRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "OpEnviarCorreoInternoUGPPFallo")
    public JAXBElement<FalloTipo> createOpEnviarCorreoInternoUGPPFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpEnviarCorreoInternoUGPPFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpEnviarCorreoInternoUGPPSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "OpEnviarCorreoInternoUGPPSol")
    public JAXBElement<OpEnviarCorreoInternoUGPPSolTipo> createOpEnviarCorreoInternoUGPPSol(OpEnviarCorreoInternoUGPPSolTipo value) {
        return new JAXBElement<OpEnviarCorreoInternoUGPPSolTipo>(_OpEnviarCorreoInternoUGPPSol_QNAME, OpEnviarCorreoInternoUGPPSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Output")
    public JAXBElement<PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput> createPortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput(PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput value) {
        return new JAXBElement<PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput>(_PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput_QNAME, PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpEnviarCorreoInternoUGPPFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", name = "msjOpEnviarCorreoInternoUGPPFallo")
    public JAXBElement<MsjOpEnviarCorreoInternoUGPPFallo> createMsjOpEnviarCorreoInternoUGPPFallo(MsjOpEnviarCorreoInternoUGPPFallo value) {
        return new JAXBElement<MsjOpEnviarCorreoInternoUGPPFallo>(_MsjOpEnviarCorreoInternoUGPPFallo_QNAME, MsjOpEnviarCorreoInternoUGPPFallo.class, null, value);
    }

}
