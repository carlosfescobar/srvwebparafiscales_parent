
package co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;


/**
 * <p>Clase Java para OpEnviarCorreoInternoUGPPSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpEnviarCorreoInternoUGPPSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="idPlantilla" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="parametro" type="{http://www.ugpp.gov.co/esb/schema/ParametroTipo/v1}ParametroTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valEmailDestinatario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="descAsuntoCorreo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idNumeroRadicado" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idNumeroExpediente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpEnviarCorreoInternoUGPPSolTipo", propOrder = {
    "contextoTransaccional",
    "idPlantilla",
    "parametro",
    "valEmailDestinatario",
    "descAsuntoCorreo",
    "idNumeroRadicado",
    "idNumeroExpediente"
})
public class OpEnviarCorreoInternoUGPPSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected String idPlantilla;
    protected List<ParametroTipo> parametro;
    @XmlElement(required = true)
    protected String valEmailDestinatario;
    @XmlElement(required = true)
    protected String descAsuntoCorreo;
    @XmlElement(required = true)
    protected String idNumeroRadicado;
    @XmlElement(required = true)
    protected String idNumeroExpediente;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad idPlantilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPlantilla() {
        return idPlantilla;
    }

    /**
     * Define el valor de la propiedad idPlantilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPlantilla(String value) {
        this.idPlantilla = value;
    }

    /**
     * Gets the value of the parametro property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parametro property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParametro().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroTipo }
     * 
     * 
     */
    public List<ParametroTipo> getParametro() {
        if (parametro == null) {
            parametro = new ArrayList<ParametroTipo>();
        }
        return this.parametro;
    }

    /**
     * Obtiene el valor de la propiedad valEmailDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValEmailDestinatario() {
        return valEmailDestinatario;
    }

    /**
     * Define el valor de la propiedad valEmailDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValEmailDestinatario(String value) {
        this.valEmailDestinatario = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsuntoCorreo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsuntoCorreo() {
        return descAsuntoCorreo;
    }

    /**
     * Define el valor de la propiedad descAsuntoCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsuntoCorreo(String value) {
        this.descAsuntoCorreo = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumeroRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumeroRadicado() {
        return idNumeroRadicado;
    }

    /**
     * Define el valor de la propiedad idNumeroRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumeroRadicado(String value) {
        this.idNumeroRadicado = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumeroExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumeroExpediente() {
        return idNumeroExpediente;
    }

    /**
     * Define el valor de la propiedad idNumeroExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumeroExpediente(String value) {
        this.idNumeroExpediente = value;
    }

}
