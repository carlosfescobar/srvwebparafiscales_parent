
package co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1}OpEnviarCorreoInternoUGPPSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Input", propOrder = {
    "opEnviarCorreoInternoUGPPSol"
})
public class PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPInput {

    @XmlElement(name = "OpEnviarCorreoInternoUGPPSol", namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", nillable = true)
    protected OpEnviarCorreoInternoUGPPSolTipo opEnviarCorreoInternoUGPPSol;

    /**
     * Obtiene el valor de la propiedad opEnviarCorreoInternoUGPPSol.
     * 
     * @return
     *     possible object is
     *     {@link OpEnviarCorreoInternoUGPPSolTipo }
     *     
     */
    public OpEnviarCorreoInternoUGPPSolTipo getOpEnviarCorreoInternoUGPPSol() {
        return opEnviarCorreoInternoUGPPSol;
    }

    /**
     * Define el valor de la propiedad opEnviarCorreoInternoUGPPSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpEnviarCorreoInternoUGPPSolTipo }
     *     
     */
    public void setOpEnviarCorreoInternoUGPPSol(OpEnviarCorreoInternoUGPPSolTipo value) {
        this.opEnviarCorreoInternoUGPPSol = value;
    }

}
