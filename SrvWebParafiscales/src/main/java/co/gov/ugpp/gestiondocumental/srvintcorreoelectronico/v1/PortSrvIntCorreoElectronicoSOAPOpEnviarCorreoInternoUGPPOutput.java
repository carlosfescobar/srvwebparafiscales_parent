
package co.gov.ugpp.gestiondocumental.srvintcorreoelectronico.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1}OpEnviarCorreoInternoUGPPResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntCorreoElectronicoSOAP_OpEnviarCorreoInternoUGPP_Output", propOrder = {
    "opEnviarCorreoInternoUGPPResp"
})
public class PortSrvIntCorreoElectronicoSOAPOpEnviarCorreoInternoUGPPOutput {

    @XmlElement(name = "OpEnviarCorreoInternoUGPPResp", namespace = "http://www.ugpp.gov.co/GestionDocumental/SrvIntCorreoElectronico/v1", nillable = true)
    protected OpEnviarCorreoInternoUGPPRespTipo opEnviarCorreoInternoUGPPResp;

    /**
     * Obtiene el valor de la propiedad opEnviarCorreoInternoUGPPResp.
     * 
     * @return
     *     possible object is
     *     {@link OpEnviarCorreoInternoUGPPRespTipo }
     *     
     */
    public OpEnviarCorreoInternoUGPPRespTipo getOpEnviarCorreoInternoUGPPResp() {
        return opEnviarCorreoInternoUGPPResp;
    }

    /**
     * Define el valor de la propiedad opEnviarCorreoInternoUGPPResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpEnviarCorreoInternoUGPPRespTipo }
     *     
     */
    public void setOpEnviarCorreoInternoUGPPResp(OpEnviarCorreoInternoUGPPRespTipo value) {
        this.opEnviarCorreoInternoUGPPResp = value;
    }

}
