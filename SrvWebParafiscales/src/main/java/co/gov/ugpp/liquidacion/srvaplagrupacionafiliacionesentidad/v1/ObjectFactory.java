
package co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarAgrupacionAfiliacionesEntidadResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpActualizarAgrupacionAfiliacionesEntidadResp");
    private final static QName _OpBuscarPorIdAgrupacionAfiliacionesEntidadSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpBuscarPorIdAgrupacionAfiliacionesEntidadSol");
    private final static QName _OpCrearAgrupacionAfiliacionesEntidadFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpCrearAgrupacionAfiliacionesEntidadFallo");
    private final static QName _OpActualizarAgrupacionAfiliacionesEntidadSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpActualizarAgrupacionAfiliacionesEntidadSol");
    private final static QName _OpBuscarPorIdAgrupacionAfiliacionesEntidadResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpBuscarPorIdAgrupacionAfiliacionesEntidadResp");
    private final static QName _OpCrearAgrupacionAfiliacionesEntidadSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpCrearAgrupacionAfiliacionesEntidadSol");
    private final static QName _OpActualizarAgrupacionAfiliacionesEntidadFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpActualizarAgrupacionAfiliacionesEntidadFallo");
    private final static QName _OpBuscarPorIdAgrupacionAfiliacionesEntidadFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpBuscarPorIdAgrupacionAfiliacionesEntidadFallo");
    private final static QName _OpCrearAgrupacionAfiliacionesEntidadResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", "OpCrearAgrupacionAfiliacionesEntidadResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarAgrupacionAfiliacionesEntidadRespTipo }
     * 
     */
    public OpActualizarAgrupacionAfiliacionesEntidadRespTipo createOpActualizarAgrupacionAfiliacionesEntidadRespTipo() {
        return new OpActualizarAgrupacionAfiliacionesEntidadRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo }
     * 
     */
    public OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo createOpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo() {
        return new OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAgrupacionAfiliacionesEntidadSolTipo }
     * 
     */
    public OpActualizarAgrupacionAfiliacionesEntidadSolTipo createOpActualizarAgrupacionAfiliacionesEntidadSolTipo() {
        return new OpActualizarAgrupacionAfiliacionesEntidadSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo }
     * 
     */
    public OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo createOpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo() {
        return new OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionAfiliacionesEntidadSolTipo }
     * 
     */
    public OpCrearAgrupacionAfiliacionesEntidadSolTipo createOpCrearAgrupacionAfiliacionesEntidadSolTipo() {
        return new OpCrearAgrupacionAfiliacionesEntidadSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAgrupacionAfiliacionesEntidadRespTipo }
     * 
     */
    public OpCrearAgrupacionAfiliacionesEntidadRespTipo createOpCrearAgrupacionAfiliacionesEntidadRespTipo() {
        return new OpCrearAgrupacionAfiliacionesEntidadRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAgrupacionAfiliacionesEntidadRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpActualizarAgrupacionAfiliacionesEntidadResp")
    public JAXBElement<OpActualizarAgrupacionAfiliacionesEntidadRespTipo> createOpActualizarAgrupacionAfiliacionesEntidadResp(OpActualizarAgrupacionAfiliacionesEntidadRespTipo value) {
        return new JAXBElement<OpActualizarAgrupacionAfiliacionesEntidadRespTipo>(_OpActualizarAgrupacionAfiliacionesEntidadResp_QNAME, OpActualizarAgrupacionAfiliacionesEntidadRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpBuscarPorIdAgrupacionAfiliacionesEntidadSol")
    public JAXBElement<OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo> createOpBuscarPorIdAgrupacionAfiliacionesEntidadSol(OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo>(_OpBuscarPorIdAgrupacionAfiliacionesEntidadSol_QNAME, OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpCrearAgrupacionAfiliacionesEntidadFallo")
    public JAXBElement<FalloTipo> createOpCrearAgrupacionAfiliacionesEntidadFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAgrupacionAfiliacionesEntidadFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAgrupacionAfiliacionesEntidadSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpActualizarAgrupacionAfiliacionesEntidadSol")
    public JAXBElement<OpActualizarAgrupacionAfiliacionesEntidadSolTipo> createOpActualizarAgrupacionAfiliacionesEntidadSol(OpActualizarAgrupacionAfiliacionesEntidadSolTipo value) {
        return new JAXBElement<OpActualizarAgrupacionAfiliacionesEntidadSolTipo>(_OpActualizarAgrupacionAfiliacionesEntidadSol_QNAME, OpActualizarAgrupacionAfiliacionesEntidadSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpBuscarPorIdAgrupacionAfiliacionesEntidadResp")
    public JAXBElement<OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo> createOpBuscarPorIdAgrupacionAfiliacionesEntidadResp(OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo>(_OpBuscarPorIdAgrupacionAfiliacionesEntidadResp_QNAME, OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionAfiliacionesEntidadSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpCrearAgrupacionAfiliacionesEntidadSol")
    public JAXBElement<OpCrearAgrupacionAfiliacionesEntidadSolTipo> createOpCrearAgrupacionAfiliacionesEntidadSol(OpCrearAgrupacionAfiliacionesEntidadSolTipo value) {
        return new JAXBElement<OpCrearAgrupacionAfiliacionesEntidadSolTipo>(_OpCrearAgrupacionAfiliacionesEntidadSol_QNAME, OpCrearAgrupacionAfiliacionesEntidadSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpActualizarAgrupacionAfiliacionesEntidadFallo")
    public JAXBElement<FalloTipo> createOpActualizarAgrupacionAfiliacionesEntidadFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAgrupacionAfiliacionesEntidadFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpBuscarPorIdAgrupacionAfiliacionesEntidadFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAgrupacionAfiliacionesEntidadFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAgrupacionAfiliacionesEntidadFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAgrupacionAfiliacionesEntidadRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1", name = "OpCrearAgrupacionAfiliacionesEntidadResp")
    public JAXBElement<OpCrearAgrupacionAfiliacionesEntidadRespTipo> createOpCrearAgrupacionAfiliacionesEntidadResp(OpCrearAgrupacionAfiliacionesEntidadRespTipo value) {
        return new JAXBElement<OpCrearAgrupacionAfiliacionesEntidadRespTipo>(_OpCrearAgrupacionAfiliacionesEntidadResp_QNAME, OpCrearAgrupacionAfiliacionesEntidadRespTipo.class, null, value);
    }

}
