
package co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarLiquidacionKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "OpConsultarLiquidacionKPIFallo");
    private final static QName _OpConsultarLiquidacionKPISol_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "OpConsultarLiquidacionKPISol");
    private final static QName _OpConsultarLiquidacionKPIResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "OpConsultarLiquidacionKPIResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarLiquidacionKPISolTipo }
     * 
     */
    public OpConsultarLiquidacionKPISolTipo createOpConsultarLiquidacionKPISolTipo() {
        return new OpConsultarLiquidacionKPISolTipo();
    }

    /**
     * Create an instance of {@link OpConsultarLiquidacionKPIRespTipo }
     * 
     */
    public OpConsultarLiquidacionKPIRespTipo createOpConsultarLiquidacionKPIRespTipo() {
        return new OpConsultarLiquidacionKPIRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", name = "OpConsultarLiquidacionKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarLiquidacionKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarLiquidacionKPIFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarLiquidacionKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", name = "OpConsultarLiquidacionKPISol")
    public JAXBElement<OpConsultarLiquidacionKPISolTipo> createOpConsultarLiquidacionKPISol(OpConsultarLiquidacionKPISolTipo value) {
        return new JAXBElement<OpConsultarLiquidacionKPISolTipo>(_OpConsultarLiquidacionKPISol_QNAME, OpConsultarLiquidacionKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarLiquidacionKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", name = "OpConsultarLiquidacionKPIResp")
    public JAXBElement<OpConsultarLiquidacionKPIRespTipo> createOpConsultarLiquidacionKPIResp(OpConsultarLiquidacionKPIRespTipo value) {
        return new JAXBElement<OpConsultarLiquidacionKPIRespTipo>(_OpConsultarLiquidacionKPIResp_QNAME, OpConsultarLiquidacionKPIRespTipo.class, null, value);
    }

}
