
package co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;


/**
 * <p>Clase Java para OpConsultarLiquidacionKPIRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpConsultarLiquidacionKPIRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="cantidadLiquidaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="liquidaciones" type="{http://www.ugpp.gov.co/schema/Liquidacion/LiquidacionTipo/v1}LiquidacionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpConsultarLiquidacionKPIRespTipo", propOrder = {
    "contextoRespuesta",
    "cantidadLiquidaciones",
    "liquidaciones"
})
public class OpConsultarLiquidacionKPIRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected String cantidadLiquidaciones;
    protected List<LiquidacionTipo> liquidaciones;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadLiquidaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadLiquidaciones() {
        return cantidadLiquidaciones;
    }

    /**
     * Define el valor de la propiedad cantidadLiquidaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadLiquidaciones(String value) {
        this.cantidadLiquidaciones = value;
    }

    /**
     * Gets the value of the liquidaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the liquidaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLiquidaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LiquidacionTipo }
     * 
     * 
     */
    public List<LiquidacionTipo> getLiquidaciones() {
        if (liquidaciones == null) {
            liquidaciones = new ArrayList<LiquidacionTipo>();
        }
        return this.liquidaciones;
    }

}
