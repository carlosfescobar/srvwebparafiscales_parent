
package co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplKPILiquidacion", targetNamespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplKPILiquidacion?wsdl")
public class SrvAplKPILiquidacion
    extends Service
{

    private final static URL SRVAPLKPILIQUIDACION_WSDL_LOCATION;
    private final static WebServiceException SRVAPLKPILIQUIDACION_EXCEPTION;
    private final static QName SRVAPLKPILIQUIDACION_QNAME = new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "SrvAplKPILiquidacion");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplKPILiquidacion?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLKPILIQUIDACION_WSDL_LOCATION = url;
        SRVAPLKPILIQUIDACION_EXCEPTION = e;
    }

    public SrvAplKPILiquidacion() {
        super(__getWsdlLocation(), SRVAPLKPILIQUIDACION_QNAME);
    }

    public SrvAplKPILiquidacion(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLKPILIQUIDACION_QNAME, features);
    }

    public SrvAplKPILiquidacion(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLKPILIQUIDACION_QNAME);
    }

    public SrvAplKPILiquidacion(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLKPILIQUIDACION_QNAME, features);
    }

    public SrvAplKPILiquidacion(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplKPILiquidacion(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplKPILiquidacionSOAP
     */
    @WebEndpoint(name = "portSrvAplKPILiquidacionSOAP")
    public PortSrvAplKPILiquidacionSOAP getPortSrvAplKPILiquidacionSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "portSrvAplKPILiquidacionSOAP"), PortSrvAplKPILiquidacionSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplKPILiquidacionSOAP
     */
    @WebEndpoint(name = "portSrvAplKPILiquidacionSOAP")
    public PortSrvAplKPILiquidacionSOAP getPortSrvAplKPILiquidacionSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1", "portSrvAplKPILiquidacionSOAP"), PortSrvAplKPILiquidacionSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLKPILIQUIDACION_EXCEPTION!= null) {
            throw SRVAPLKPILIQUIDACION_EXCEPTION;
        }
        return SRVAPLKPILIQUIDACION_WSDL_LOCATION;
    }

}
