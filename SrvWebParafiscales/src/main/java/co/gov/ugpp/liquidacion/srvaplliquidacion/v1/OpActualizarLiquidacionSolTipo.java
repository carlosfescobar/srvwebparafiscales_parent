
package co.gov.ugpp.liquidacion.srvaplliquidacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;


/**
 * <p>Clase Java para OpActualizarLiquidacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarLiquidacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="liquidacion" type="{http://www.ugpp.gov.co/schema/Liquidacion/LiquidacionTipo/v1}LiquidacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarLiquidacionSolTipo", propOrder = {
    "contextoTransaccional",
    "liquidacion"
})
public class OpActualizarLiquidacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected LiquidacionTipo liquidacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidacion.
     * 
     * @return
     *     possible object is
     *     {@link LiquidacionTipo }
     *     
     */
    public LiquidacionTipo getLiquidacion() {
        return liquidacion;
    }

    /**
     * Define el valor de la propiedad liquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link LiquidacionTipo }
     *     
     */
    public void setLiquidacion(LiquidacionTipo value) {
        this.liquidacion = value;
    }

}
