
package co.gov.ugpp.liquidaciones.srvaplliquidacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidaciones.srvaplliquidacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarLiquidacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpActualizarLiquidacionFallo");
    private final static QName _OpCrearLiquidacionSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpCrearLiquidacionSol");
    private final static QName _OpCrearLiquidacionResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpCrearLiquidacionResp");
    private final static QName _OpCrearLiquidacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpCrearLiquidacionFallo");
    private final static QName _OpActualizarLiquidacionResp_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpActualizarLiquidacionResp");
    private final static QName _OpActualizarLiquidacionSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", "OpActualizarLiquidacionSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidaciones.srvaplliquidacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarLiquidacionRespTipo }
     * 
     */
    public OpActualizarLiquidacionRespTipo createOpActualizarLiquidacionRespTipo() {
        return new OpActualizarLiquidacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarLiquidacionSolTipo }
     * 
     */
    public OpActualizarLiquidacionSolTipo createOpActualizarLiquidacionSolTipo() {
        return new OpActualizarLiquidacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearLiquidacionRespTipo }
     * 
     */
    public OpCrearLiquidacionRespTipo createOpCrearLiquidacionRespTipo() {
        return new OpCrearLiquidacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearLiquidacionSolTipo }
     * 
     */
    public OpCrearLiquidacionSolTipo createOpCrearLiquidacionSolTipo() {
        return new OpCrearLiquidacionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpActualizarLiquidacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarLiquidacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarLiquidacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLiquidacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpCrearLiquidacionSol")
    public JAXBElement<OpCrearLiquidacionSolTipo> createOpCrearLiquidacionSol(OpCrearLiquidacionSolTipo value) {
        return new JAXBElement<OpCrearLiquidacionSolTipo>(_OpCrearLiquidacionSol_QNAME, OpCrearLiquidacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLiquidacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpCrearLiquidacionResp")
    public JAXBElement<OpCrearLiquidacionRespTipo> createOpCrearLiquidacionResp(OpCrearLiquidacionRespTipo value) {
        return new JAXBElement<OpCrearLiquidacionRespTipo>(_OpCrearLiquidacionResp_QNAME, OpCrearLiquidacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpCrearLiquidacionFallo")
    public JAXBElement<FalloTipo> createOpCrearLiquidacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearLiquidacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarLiquidacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpActualizarLiquidacionResp")
    public JAXBElement<OpActualizarLiquidacionRespTipo> createOpActualizarLiquidacionResp(OpActualizarLiquidacionRespTipo value) {
        return new JAXBElement<OpActualizarLiquidacionRespTipo>(_OpActualizarLiquidacionResp_QNAME, OpActualizarLiquidacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarLiquidacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidaciones/SrvAplLiquidacion/v1", name = "OpActualizarLiquidacionSol")
    public JAXBElement<OpActualizarLiquidacionSolTipo> createOpActualizarLiquidacionSol(OpActualizarLiquidacionSolTipo value) {
        return new JAXBElement<OpActualizarLiquidacionSolTipo>(_OpActualizarLiquidacionSol_QNAME, OpActualizarLiquidacionSolTipo.class, null, value);
    }

}
