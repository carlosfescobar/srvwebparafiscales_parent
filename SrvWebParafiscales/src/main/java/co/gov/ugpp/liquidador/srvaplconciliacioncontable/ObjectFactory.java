
package co.gov.ugpp.liquidador.srvaplconciliacioncontable;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidador.srvaplconciliacioncontable package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearRes_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", "OpCrearRes");
    private final static QName _OpCrearFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", "OpCrearFallo");
    private final static QName _OpCrearSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", "OpCrearSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidador.srvaplconciliacioncontable
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearSolTipo }
     * 
     */
    public OpCrearSolTipo createOpCrearSolTipo() {
        return new OpCrearSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearResTipo }
     * 
     */
    public OpCrearResTipo createOpCrearResTipo() {
        return new OpCrearResTipo();
    }

    /**
     * Create an instance of {@link ConciliacionContableTipo }
     * 
     */
    public ConciliacionContableTipo createConciliacionContableTipo() {
        return new ConciliacionContableTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearResTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", name = "OpCrearRes")
    public JAXBElement<OpCrearResTipo> createOpCrearRes(OpCrearResTipo value) {
        return new JAXBElement<OpCrearResTipo>(_OpCrearRes_QNAME, OpCrearResTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", name = "OpCrearFallo")
    public JAXBElement<FalloTipo> createOpCrearFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable", name = "OpCrearSol")
    public JAXBElement<OpCrearSolTipo> createOpCrearSol(OpCrearSolTipo value) {
        return new JAXBElement<OpCrearSolTipo>(_OpCrearSol_QNAME, OpCrearSolTipo.class, null, value);
    }

}
