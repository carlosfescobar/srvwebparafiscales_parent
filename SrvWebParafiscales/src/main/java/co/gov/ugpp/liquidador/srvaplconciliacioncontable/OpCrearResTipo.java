
package co.gov.ugpp.liquidador.srvaplconciliacioncontable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;


/**
 * <p>Clase Java para OpCrearResTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearResTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="conciliacionContable" type="{http://www.ugpp.gov.co/Liquidador/SrvAplConciliacionContable}ConciliacionContableTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearResTipo", propOrder = {
    "contextoRespuesta",
    "conciliacionContable"
})
public class OpCrearResTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(required = true)
    protected ConciliacionContableTipo conciliacionContable;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad conciliacionContable.
     * 
     * @return
     *     possible object is
     *     {@link ConciliacionContableTipo }
     *     
     */
    public ConciliacionContableTipo getConciliacionContable() {
        return conciliacionContable;
    }

    /**
     * Define el valor de la propiedad conciliacionContable.
     * 
     * @param value
     *     allowed object is
     *     {@link ConciliacionContableTipo }
     *     
     */
    public void setConciliacionContable(ConciliacionContableTipo value) {
        this.conciliacionContable = value;
    }

}
