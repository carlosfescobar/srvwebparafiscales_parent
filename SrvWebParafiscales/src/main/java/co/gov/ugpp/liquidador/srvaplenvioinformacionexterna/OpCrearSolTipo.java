
package co.gov.ugpp.liquidador.srvaplenvioinformacionexterna;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;


/**
 * <p>Clase Java para OpCrearSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="envio" type="{http://www.ugpp.gov.co/schema/SolicitarInformacion/EnvioTipo/v1}EnvioTipo" minOccurs="0"/>
 *         &lt;element name="idsInformacionExternaTemporal" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearSolTipo", propOrder = {
    "envio",
    "idsInformacionExternaTemporal",
    "contextoTransaccional"
})
public class OpCrearSolTipo {

    protected EnvioTipo envio;
    @XmlElement(required = true)
    protected List<String> idsInformacionExternaTemporal;
    protected ContextoTransaccionalTipo contextoTransaccional;

    /**
     * Obtiene el valor de la propiedad envio.
     * 
     * @return
     *     possible object is
     *     {@link EnvioTipo }
     *     
     */
    public EnvioTipo getEnvio() {
        return envio;
    }

    /**
     * Define el valor de la propiedad envio.
     * 
     * @param value
     *     allowed object is
     *     {@link EnvioTipo }
     *     
     */
    public void setEnvio(EnvioTipo value) {
        this.envio = value;
    }

    /**
     * Gets the value of the idsInformacionExternaTemporal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsInformacionExternaTemporal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsInformacionExternaTemporal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsInformacionExternaTemporal() {
        if (idsInformacionExternaTemporal == null) {
            idsInformacionExternaTemporal = new ArrayList<String>();
        }
        return this.idsInformacionExternaTemporal;
    }

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

}
