
package co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplHojaCalculoLiquidacion", targetNamespace = "http://www.ugpp.gov.co/Liquidador/SrvAplHojaCalculoLiquidacion", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvHojaCalculoLiquidacion?wsdl")
public class SrvAplHojaCalculoLiquidacion
    extends Service
{

    private final static URL SRVAPLHOJACALCULOLIQUIDACION_WSDL_LOCATION;
    private final static WebServiceException SRVAPLHOJACALCULOLIQUIDACION_EXCEPTION;
    private final static QName SRVAPLHOJACALCULOLIQUIDACION_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplHojaCalculoLiquidacion", "SrvAplHojaCalculoLiquidacion");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvHojaCalculoLiquidacion?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLHOJACALCULOLIQUIDACION_WSDL_LOCATION = url;
        SRVAPLHOJACALCULOLIQUIDACION_EXCEPTION = e;
    }

    public SrvAplHojaCalculoLiquidacion() {
        super(__getWsdlLocation(), SRVAPLHOJACALCULOLIQUIDACION_QNAME);
    }

    public SrvAplHojaCalculoLiquidacion(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLHOJACALCULOLIQUIDACION_QNAME, features);
    }

    public SrvAplHojaCalculoLiquidacion(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLHOJACALCULOLIQUIDACION_QNAME);
    }

    public SrvAplHojaCalculoLiquidacion(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLHOJACALCULOLIQUIDACION_QNAME, features);
    }

    public SrvAplHojaCalculoLiquidacion(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplHojaCalculoLiquidacion(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplHojaCalculoLiquidacion
     */
    @WebEndpoint(name = "portSrvAplHojaCalculoLiquidacion")
    public PortSrvAplHojaCalculoLiquidacion getPortSrvAplHojaCalculoLiquidacion() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Liquidador/SrvAplHojaCalculoLiquidacion", "portSrvAplHojaCalculoLiquidacion"), PortSrvAplHojaCalculoLiquidacion.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplHojaCalculoLiquidacion
     */
    @WebEndpoint(name = "portSrvAplHojaCalculoLiquidacion")
    public PortSrvAplHojaCalculoLiquidacion getPortSrvAplHojaCalculoLiquidacion(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Liquidador/SrvAplHojaCalculoLiquidacion", "portSrvAplHojaCalculoLiquidacion"), PortSrvAplHojaCalculoLiquidacion.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLHOJACALCULOLIQUIDACION_EXCEPTION!= null) {
            throw SRVAPLHOJACALCULOLIQUIDACION_EXCEPTION;
        }
        return SRVAPLHOJACALCULOLIQUIDACION_WSDL_LOCATION;
    }

}
