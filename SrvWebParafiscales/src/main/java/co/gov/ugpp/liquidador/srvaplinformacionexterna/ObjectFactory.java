
package co.gov.ugpp.liquidador.srvaplinformacionexterna;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidador.srvaplinformacionexterna package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpValidarArchivosTemporalSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpValidarArchivosTemporalSol");
    private final static QName _OpColocarArchivosTemporalResponse_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpColocarArchivosTemporalResponse");
    private final static QName _OpValidarArchivosTemporalFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpValidarArchivosTemporalFallo");
    private final static QName _OpColocarArchivosTemporalSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpColocarArchivosTemporalSol");
    private final static QName _OpColocarArchivosTemporalFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpColocarArchivosTemporalFallo");
    private final static QName _OpValidarArchivosTemporalResponse_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", "OpValidarArchivosTemporalResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidador.srvaplinformacionexterna
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpValidarArchivosTemporalResponse }
     * 
     */
    public OpValidarArchivosTemporalResponse createOpValidarArchivosTemporalResponse() {
        return new OpValidarArchivosTemporalResponse();
    }

    /**
     * Create an instance of {@link OpColocarArchivosTemporalSolTipo }
     * 
     */
    public OpColocarArchivosTemporalSolTipo createOpColocarArchivosTemporalSolTipo() {
        return new OpColocarArchivosTemporalSolTipo();
    }

    /**
     * Create an instance of {@link OpColocarArchivosTemporalResponse }
     * 
     */
    public OpColocarArchivosTemporalResponse createOpColocarArchivosTemporalResponse() {
        return new OpColocarArchivosTemporalResponse();
    }

    /**
     * Create an instance of {@link OpValidarArchivosTemporalSolTipo }
     * 
     */
    public OpValidarArchivosTemporalSolTipo createOpValidarArchivosTemporalSolTipo() {
        return new OpValidarArchivosTemporalSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarArchivosTemporalSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpValidarArchivosTemporalSol")
    public JAXBElement<OpValidarArchivosTemporalSolTipo> createOpValidarArchivosTemporalSol(OpValidarArchivosTemporalSolTipo value) {
        return new JAXBElement<OpValidarArchivosTemporalSolTipo>(_OpValidarArchivosTemporalSol_QNAME, OpValidarArchivosTemporalSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpColocarArchivosTemporalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpColocarArchivosTemporalResponse")
    public JAXBElement<OpColocarArchivosTemporalResponse> createOpColocarArchivosTemporalResponse(OpColocarArchivosTemporalResponse value) {
        return new JAXBElement<OpColocarArchivosTemporalResponse>(_OpColocarArchivosTemporalResponse_QNAME, OpColocarArchivosTemporalResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpValidarArchivosTemporalFallo")
    public JAXBElement<FalloTipo> createOpValidarArchivosTemporalFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpValidarArchivosTemporalFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpColocarArchivosTemporalSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpColocarArchivosTemporalSol")
    public JAXBElement<OpColocarArchivosTemporalSolTipo> createOpColocarArchivosTemporalSol(OpColocarArchivosTemporalSolTipo value) {
        return new JAXBElement<OpColocarArchivosTemporalSolTipo>(_OpColocarArchivosTemporalSol_QNAME, OpColocarArchivosTemporalSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpColocarArchivosTemporalFallo")
    public JAXBElement<FalloTipo> createOpColocarArchivosTemporalFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpColocarArchivosTemporalFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarArchivosTemporalResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplInformacionExterna", name = "OpValidarArchivosTemporalResponse")
    public JAXBElement<OpValidarArchivosTemporalResponse> createOpValidarArchivosTemporalResponse(OpValidarArchivosTemporalResponse value) {
        return new JAXBElement<OpValidarArchivosTemporalResponse>(_OpValidarArchivosTemporalResponse_QNAME, OpValidarArchivosTemporalResponse.class, null, value);
    }

}
