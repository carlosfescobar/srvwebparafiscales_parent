
package co.gov.ugpp.liquidador.srvaplinformacionexterna;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;


/**
 * <p>Clase Java para OpColocarArchivosTemporalSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpColocarArchivosTemporalSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo" minOccurs="0"/>
 *         &lt;element name="identificacion" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="formato" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="archivo" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpColocarArchivosTemporalSolTipo", propOrder = {
    "contextoTransaccional",
    "identificacion",
    "formato",
    "archivo"
})
public class OpColocarArchivosTemporalSolTipo {

    protected ContextoTransaccionalTipo contextoTransaccional;
    protected IdentificacionTipo identificacion;
    protected FormatoTipo formato;
    protected ArchivoTipo archivo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad identificacion.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdentificacion() {
        return identificacion;
    }

    /**
     * Define el valor de la propiedad identificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdentificacion(IdentificacionTipo value) {
        this.identificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad formato.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormato() {
        return formato;
    }

    /**
     * Define el valor de la propiedad formato.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormato(FormatoTipo value) {
        this.formato = value;
    }

    /**
     * Obtiene el valor de la propiedad archivo.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getArchivo() {
        return archivo;
    }

    /**
     * Define el valor de la propiedad archivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setArchivo(ArchivoTipo value) {
        this.archivo = value;
    }

}
