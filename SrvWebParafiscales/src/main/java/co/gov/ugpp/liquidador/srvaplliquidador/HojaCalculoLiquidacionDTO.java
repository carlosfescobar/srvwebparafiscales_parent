
package co.gov.ugpp.liquidador.srvaplliquidador;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;


/**
 * <p>Clase Java para hojaCalculoLiquidacionDTO complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="hojaCalculoLiquidacionDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codesstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desestado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esincumplimiento" type="{http://www.w3.org/2001/XMLSchema}unsignedShort" minOccurs="0"/>
 *         &lt;element name="fechacreacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="idexpediente" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hojaCalculoLiquidacionDTO", propOrder = {
    "codesstado",
    "desestado",
    "esincumplimiento",
    "fechacreacion",
    "id",
    "idexpediente"
})
public class HojaCalculoLiquidacionDTO {

    protected String codesstado;
    protected String desestado;
    @XmlSchemaType(name = "unsignedShort")
    protected Integer esincumplimiento;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechacreacion;
    protected BigDecimal id;
    @XmlElement(type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long idexpediente;

    /**
     * Obtiene el valor de la propiedad codesstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodesstado() {
        return codesstado;
    }

    /**
     * Define el valor de la propiedad codesstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodesstado(String value) {
        this.codesstado = value;
    }

    /**
     * Obtiene el valor de la propiedad desestado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesestado() {
        return desestado;
    }

    /**
     * Define el valor de la propiedad desestado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesestado(String value) {
        this.desestado = value;
    }

    /**
     * Obtiene el valor de la propiedad esincumplimiento.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getEsincumplimiento() {
        return esincumplimiento;
    }

    /**
     * Define el valor de la propiedad esincumplimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setEsincumplimiento(Integer value) {
        this.esincumplimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad fechacreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechacreacion() {
        return fechacreacion;
    }

    /**
     * Define el valor de la propiedad fechacreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechacreacion(Calendar value) {
        this.fechacreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setId(BigDecimal value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad idexpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIdexpediente() {
        return idexpediente;
    }

    /**
     * Define el valor de la propiedad idexpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdexpediente(Long value) {
        this.idexpediente = value;
    }

}
