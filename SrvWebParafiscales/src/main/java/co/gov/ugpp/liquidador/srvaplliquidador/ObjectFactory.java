
package co.gov.ugpp.liquidador.srvaplliquidador;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidador.srvaplliquidador package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpLiquidarFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", "OpLiquidarFallo");
    private final static QName _OpLiquidarRes_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", "OpLiquidarRes");
    private final static QName _OpLiquidarSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", "OpLiquidarSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidador.srvaplliquidador
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpLiquidarResTipo }
     * 
     */
    public OpLiquidarResTipo createOpLiquidarResTipo() {
        return new OpLiquidarResTipo();
    }

    /**
     * Create an instance of {@link OpLiquidarSolTipo }
     * 
     */
    public OpLiquidarSolTipo createOpLiquidarSolTipo() {
        return new OpLiquidarSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", name = "OpLiquidarFallo")
    public JAXBElement<FalloTipo> createOpLiquidarFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpLiquidarFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLiquidarResTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", name = "OpLiquidarRes")
    public JAXBElement<OpLiquidarResTipo> createOpLiquidarRes(OpLiquidarResTipo value) {
        return new JAXBElement<OpLiquidarResTipo>(_OpLiquidarRes_QNAME, OpLiquidarResTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLiquidarSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplLiquidador", name = "OpLiquidarSol")
    public JAXBElement<OpLiquidarSolTipo> createOpLiquidarSol(OpLiquidarSolTipo value) {
        return new JAXBElement<OpLiquidarSolTipo>(_OpLiquidarSol_QNAME, OpLiquidarSolTipo.class, null, value);
    }

}
