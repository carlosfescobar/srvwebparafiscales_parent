
package co.gov.ugpp.liquidador.srvaplpila;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.liquidador.srvaplpila package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCargarPilaDepuradaFallo_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplPila", "OpCargarPilaDepuradaFallo");
    private final static QName _OpCargarPilaDepuradaSol_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplPila", "OpCargarPilaDepuradaSol");
    private final static QName _OpCargarPilaDepuradaRes_QNAME = new QName("http://www.ugpp.gov.co/Liquidador/SrvAplPila", "OpCargarPilaDepuradaRes");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.liquidador.srvaplpila
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCargarPilaDepuradaSolTipo }
     * 
     */
    public OpCargarPilaDepuradaSolTipo createOpCargarPilaDepuradaSolTipo() {
        return new OpCargarPilaDepuradaSolTipo();
    }

    /**
     * Create an instance of {@link OpCargarPilaDepuradaResTipo }
     * 
     */
    public OpCargarPilaDepuradaResTipo createOpCargarPilaDepuradaResTipo() {
        return new OpCargarPilaDepuradaResTipo();
    }

    /**
     * Create an instance of {@link PilaTipo }
     * 
     */
    public PilaTipo createPilaTipo() {
        return new PilaTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplPila", name = "OpCargarPilaDepuradaFallo")
    public JAXBElement<FalloTipo> createOpCargarPilaDepuradaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCargarPilaDepuradaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCargarPilaDepuradaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplPila", name = "OpCargarPilaDepuradaSol")
    public JAXBElement<OpCargarPilaDepuradaSolTipo> createOpCargarPilaDepuradaSol(OpCargarPilaDepuradaSolTipo value) {
        return new JAXBElement<OpCargarPilaDepuradaSolTipo>(_OpCargarPilaDepuradaSol_QNAME, OpCargarPilaDepuradaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCargarPilaDepuradaResTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Liquidador/SrvAplPila", name = "OpCargarPilaDepuradaRes")
    public JAXBElement<OpCargarPilaDepuradaResTipo> createOpCargarPilaDepuradaRes(OpCargarPilaDepuradaResTipo value) {
        return new JAXBElement<OpCargarPilaDepuradaResTipo>(_OpCargarPilaDepuradaRes_QNAME, OpCargarPilaDepuradaResTipo.class, null, value);
    }

}
