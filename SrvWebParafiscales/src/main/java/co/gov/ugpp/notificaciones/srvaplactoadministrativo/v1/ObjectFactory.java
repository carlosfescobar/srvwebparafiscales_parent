
package co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpActualizarActoAdministrativoResp");
    private final static QName _OpCrearActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpCrearActoAdministrativoResp");
    private final static QName _OpActualizarActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpActualizarActoAdministrativoSol");
    private final static QName _OpCrearActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpCrearActoAdministrativoFallo");
    private final static QName _OpBuscarPorCriteriosActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorCriteriosActoAdministrativoFallo");
    private final static QName _OpCrearActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpCrearActoAdministrativoSol");
    private final static QName _OpBuscarPorIdActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorIdActoAdministrativoFallo");
    private final static QName _OpBuscarPorCriteriosActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorCriteriosActoAdministrativoSol");
    private final static QName _OpBuscarPorCriteriosActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorCriteriosActoAdministrativoResp");
    private final static QName _OpActualizarActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpActualizarActoAdministrativoFallo");
    private final static QName _OpBuscarPorIdActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorIdActoAdministrativoSol");
    private final static QName _OpBuscarPorIdActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "OpBuscarPorIdActoAdministrativoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarActoAdministrativoSolTipo }
     * 
     */
    public OpActualizarActoAdministrativoSolTipo createOpActualizarActoAdministrativoSolTipo() {
        return new OpActualizarActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearActoAdministrativoRespTipo }
     * 
     */
    public OpCrearActoAdministrativoRespTipo createOpCrearActoAdministrativoRespTipo() {
        return new OpCrearActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarActoAdministrativoRespTipo }
     * 
     */
    public OpActualizarActoAdministrativoRespTipo createOpActualizarActoAdministrativoRespTipo() {
        return new OpActualizarActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearActoAdministrativoSolTipo }
     * 
     */
    public OpCrearActoAdministrativoSolTipo createOpCrearActoAdministrativoSolTipo() {
        return new OpCrearActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosActoAdministrativoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosActoAdministrativoSolTipo createOpBuscarPorCriteriosActoAdministrativoSolTipo() {
        return new OpBuscarPorCriteriosActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdActoAdministrativoRespTipo }
     * 
     */
    public OpBuscarPorIdActoAdministrativoRespTipo createOpBuscarPorIdActoAdministrativoRespTipo() {
        return new OpBuscarPorIdActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdActoAdministrativoSolTipo }
     * 
     */
    public OpBuscarPorIdActoAdministrativoSolTipo createOpBuscarPorIdActoAdministrativoSolTipo() {
        return new OpBuscarPorIdActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosActoAdministrativoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosActoAdministrativoRespTipo createOpBuscarPorCriteriosActoAdministrativoRespTipo() {
        return new OpBuscarPorCriteriosActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpActualizarActoAdministrativoResp")
    public JAXBElement<OpActualizarActoAdministrativoRespTipo> createOpActualizarActoAdministrativoResp(OpActualizarActoAdministrativoRespTipo value) {
        return new JAXBElement<OpActualizarActoAdministrativoRespTipo>(_OpActualizarActoAdministrativoResp_QNAME, OpActualizarActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpCrearActoAdministrativoResp")
    public JAXBElement<OpCrearActoAdministrativoRespTipo> createOpCrearActoAdministrativoResp(OpCrearActoAdministrativoRespTipo value) {
        return new JAXBElement<OpCrearActoAdministrativoRespTipo>(_OpCrearActoAdministrativoResp_QNAME, OpCrearActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpActualizarActoAdministrativoSol")
    public JAXBElement<OpActualizarActoAdministrativoSolTipo> createOpActualizarActoAdministrativoSol(OpActualizarActoAdministrativoSolTipo value) {
        return new JAXBElement<OpActualizarActoAdministrativoSolTipo>(_OpActualizarActoAdministrativoSol_QNAME, OpActualizarActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpCrearActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpCrearActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorCriteriosActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpCrearActoAdministrativoSol")
    public JAXBElement<OpCrearActoAdministrativoSolTipo> createOpCrearActoAdministrativoSol(OpCrearActoAdministrativoSolTipo value) {
        return new JAXBElement<OpCrearActoAdministrativoSolTipo>(_OpCrearActoAdministrativoSol_QNAME, OpCrearActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorIdActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorCriteriosActoAdministrativoSol")
    public JAXBElement<OpBuscarPorCriteriosActoAdministrativoSolTipo> createOpBuscarPorCriteriosActoAdministrativoSol(OpBuscarPorCriteriosActoAdministrativoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosActoAdministrativoSolTipo>(_OpBuscarPorCriteriosActoAdministrativoSol_QNAME, OpBuscarPorCriteriosActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorCriteriosActoAdministrativoResp")
    public JAXBElement<OpBuscarPorCriteriosActoAdministrativoRespTipo> createOpBuscarPorCriteriosActoAdministrativoResp(OpBuscarPorCriteriosActoAdministrativoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosActoAdministrativoRespTipo>(_OpBuscarPorCriteriosActoAdministrativoResp_QNAME, OpBuscarPorCriteriosActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpActualizarActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpActualizarActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorIdActoAdministrativoSol")
    public JAXBElement<OpBuscarPorIdActoAdministrativoSolTipo> createOpBuscarPorIdActoAdministrativoSol(OpBuscarPorIdActoAdministrativoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdActoAdministrativoSolTipo>(_OpBuscarPorIdActoAdministrativoSol_QNAME, OpBuscarPorIdActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", name = "OpBuscarPorIdActoAdministrativoResp")
    public JAXBElement<OpBuscarPorIdActoAdministrativoRespTipo> createOpBuscarPorIdActoAdministrativoResp(OpBuscarPorIdActoAdministrativoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdActoAdministrativoRespTipo>(_OpBuscarPorIdActoAdministrativoResp_QNAME, OpBuscarPorIdActoAdministrativoRespTipo.class, null, value);
    }

}
