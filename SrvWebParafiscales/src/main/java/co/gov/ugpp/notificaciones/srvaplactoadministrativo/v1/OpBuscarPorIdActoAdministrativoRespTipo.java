
package co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;


/**
 * <p>Clase Java para OpBuscarPorIdActoAdministrativoRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdActoAdministrativoRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="actoAdministrativo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdActoAdministrativoRespTipo", propOrder = {
    "contextoRespuesta",
    "actoAdministrativo"
})
public class OpBuscarPorIdActoAdministrativoRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected ActoAdministrativoTipo actoAdministrativo;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad actoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getActoAdministrativo() {
        return actoAdministrativo;
    }

    /**
     * Define el valor de la propiedad actoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setActoAdministrativo(ActoAdministrativoTipo value) {
        this.actoAdministrativo = value;
    }

}
