
package co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplActoAdministrativo", targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvActoAdministrativo?wsdl")
public class SrvAplActoAdministrativo
    extends Service
{

    private final static URL SRVAPLACTOADMINISTRATIVO_WSDL_LOCATION;
    private final static WebServiceException SRVAPLACTOADMINISTRATIVO_EXCEPTION;
    private final static QName SRVAPLACTOADMINISTRATIVO_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "SrvAplActoAdministrativo");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvActoAdministrativo?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLACTOADMINISTRATIVO_WSDL_LOCATION = url;
        SRVAPLACTOADMINISTRATIVO_EXCEPTION = e;
    }

    public SrvAplActoAdministrativo() {
        super(__getWsdlLocation(), SRVAPLACTOADMINISTRATIVO_QNAME);
    }

    public SrvAplActoAdministrativo(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLACTOADMINISTRATIVO_QNAME, features);
    }

    public SrvAplActoAdministrativo(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLACTOADMINISTRATIVO_QNAME);
    }

    public SrvAplActoAdministrativo(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLACTOADMINISTRATIVO_QNAME, features);
    }

    public SrvAplActoAdministrativo(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplActoAdministrativo(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplActoAdministrativoSOAP
     */
    @WebEndpoint(name = "portSrvAplActoAdministrativoSOAP")
    public PortSrvAplActoAdministrativoSOAP getPortSrvAplActoAdministrativoSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "portSrvAplActoAdministrativoSOAP"), PortSrvAplActoAdministrativoSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplActoAdministrativoSOAP
     */
    @WebEndpoint(name = "portSrvAplActoAdministrativoSOAP")
    public PortSrvAplActoAdministrativoSOAP getPortSrvAplActoAdministrativoSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1", "portSrvAplActoAdministrativoSOAP"), PortSrvAplActoAdministrativoSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLACTOADMINISTRATIVO_EXCEPTION!= null) {
            throw SRVAPLACTOADMINISTRATIVO_EXCEPTION;
        }
        return SRVAPLACTOADMINISTRATIVO_WSDL_LOCATION;
    }

}
