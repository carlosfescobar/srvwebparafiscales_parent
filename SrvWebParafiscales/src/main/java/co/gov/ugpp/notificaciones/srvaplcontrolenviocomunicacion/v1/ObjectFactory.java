
package co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdControlEnvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorIdControlEnvioComunicacionResp");
    private final static QName _OpBuscarPorCriteriosControlEnvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorCriteriosControlEnvioComunicacionResp");
    private final static QName _OpActualizarControlEnvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpActualizarControlEnvioComunicacionFallo");
    private final static QName _OpBuscarPorIdControlEnvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorIdControlEnvioComunicacionSol");
    private final static QName _OpBuscarPorIdControlEnvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorIdControlEnvioComunicacionFallo");
    private final static QName _OpActualizarControlEnvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpActualizarControlEnvioComunicacionSol");
    private final static QName _OpCrearControlEnvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpCrearControlEnvioComunicacionFallo");
    private final static QName _OpBuscarPorCriteriosControlEnvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorCriteriosControlEnvioComunicacionSol");
    private final static QName _OpCrearControlEnvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpCrearControlEnvioComunicacionSol");
    private final static QName _OpActualizarControlEnvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpActualizarControlEnvioComunicacionResp");
    private final static QName _OpCrearControlEnvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpCrearControlEnvioComunicacionResp");
    private final static QName _OpBuscarPorCriteriosControlEnvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", "OpBuscarPorCriteriosControlEnvioComunicacionFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlEnvioComunicacionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosControlEnvioComunicacionRespTipo createOpBuscarPorCriteriosControlEnvioComunicacionRespTipo() {
        return new OpBuscarPorCriteriosControlEnvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlEnvioComunicacionRespTipo }
     * 
     */
    public OpBuscarPorIdControlEnvioComunicacionRespTipo createOpBuscarPorIdControlEnvioComunicacionRespTipo() {
        return new OpBuscarPorIdControlEnvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlEnvioComunicacionSolTipo }
     * 
     */
    public OpBuscarPorIdControlEnvioComunicacionSolTipo createOpBuscarPorIdControlEnvioComunicacionSolTipo() {
        return new OpBuscarPorIdControlEnvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlEnvioComunicacionSolTipo }
     * 
     */
    public OpActualizarControlEnvioComunicacionSolTipo createOpActualizarControlEnvioComunicacionSolTipo() {
        return new OpActualizarControlEnvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlEnvioComunicacionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosControlEnvioComunicacionSolTipo createOpBuscarPorCriteriosControlEnvioComunicacionSolTipo() {
        return new OpBuscarPorCriteriosControlEnvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlEnvioComunicacionRespTipo }
     * 
     */
    public OpActualizarControlEnvioComunicacionRespTipo createOpActualizarControlEnvioComunicacionRespTipo() {
        return new OpActualizarControlEnvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlEnvioComunicacionSolTipo }
     * 
     */
    public OpCrearControlEnvioComunicacionSolTipo createOpCrearControlEnvioComunicacionSolTipo() {
        return new OpCrearControlEnvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlEnvioComunicacionRespTipo }
     * 
     */
    public OpCrearControlEnvioComunicacionRespTipo createOpCrearControlEnvioComunicacionRespTipo() {
        return new OpCrearControlEnvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlEnvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorIdControlEnvioComunicacionResp")
    public JAXBElement<OpBuscarPorIdControlEnvioComunicacionRespTipo> createOpBuscarPorIdControlEnvioComunicacionResp(OpBuscarPorIdControlEnvioComunicacionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdControlEnvioComunicacionRespTipo>(_OpBuscarPorIdControlEnvioComunicacionResp_QNAME, OpBuscarPorIdControlEnvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlEnvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorCriteriosControlEnvioComunicacionResp")
    public JAXBElement<OpBuscarPorCriteriosControlEnvioComunicacionRespTipo> createOpBuscarPorCriteriosControlEnvioComunicacionResp(OpBuscarPorCriteriosControlEnvioComunicacionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlEnvioComunicacionRespTipo>(_OpBuscarPorCriteriosControlEnvioComunicacionResp_QNAME, OpBuscarPorCriteriosControlEnvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpActualizarControlEnvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarControlEnvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarControlEnvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlEnvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorIdControlEnvioComunicacionSol")
    public JAXBElement<OpBuscarPorIdControlEnvioComunicacionSolTipo> createOpBuscarPorIdControlEnvioComunicacionSol(OpBuscarPorIdControlEnvioComunicacionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdControlEnvioComunicacionSolTipo>(_OpBuscarPorIdControlEnvioComunicacionSol_QNAME, OpBuscarPorIdControlEnvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorIdControlEnvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdControlEnvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdControlEnvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlEnvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpActualizarControlEnvioComunicacionSol")
    public JAXBElement<OpActualizarControlEnvioComunicacionSolTipo> createOpActualizarControlEnvioComunicacionSol(OpActualizarControlEnvioComunicacionSolTipo value) {
        return new JAXBElement<OpActualizarControlEnvioComunicacionSolTipo>(_OpActualizarControlEnvioComunicacionSol_QNAME, OpActualizarControlEnvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpCrearControlEnvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpCrearControlEnvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearControlEnvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlEnvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorCriteriosControlEnvioComunicacionSol")
    public JAXBElement<OpBuscarPorCriteriosControlEnvioComunicacionSolTipo> createOpBuscarPorCriteriosControlEnvioComunicacionSol(OpBuscarPorCriteriosControlEnvioComunicacionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlEnvioComunicacionSolTipo>(_OpBuscarPorCriteriosControlEnvioComunicacionSol_QNAME, OpBuscarPorCriteriosControlEnvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlEnvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpCrearControlEnvioComunicacionSol")
    public JAXBElement<OpCrearControlEnvioComunicacionSolTipo> createOpCrearControlEnvioComunicacionSol(OpCrearControlEnvioComunicacionSolTipo value) {
        return new JAXBElement<OpCrearControlEnvioComunicacionSolTipo>(_OpCrearControlEnvioComunicacionSol_QNAME, OpCrearControlEnvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlEnvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpActualizarControlEnvioComunicacionResp")
    public JAXBElement<OpActualizarControlEnvioComunicacionRespTipo> createOpActualizarControlEnvioComunicacionResp(OpActualizarControlEnvioComunicacionRespTipo value) {
        return new JAXBElement<OpActualizarControlEnvioComunicacionRespTipo>(_OpActualizarControlEnvioComunicacionResp_QNAME, OpActualizarControlEnvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlEnvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpCrearControlEnvioComunicacionResp")
    public JAXBElement<OpCrearControlEnvioComunicacionRespTipo> createOpCrearControlEnvioComunicacionResp(OpCrearControlEnvioComunicacionRespTipo value) {
        return new JAXBElement<OpCrearControlEnvioComunicacionRespTipo>(_OpCrearControlEnvioComunicacionResp_QNAME, OpCrearControlEnvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1", name = "OpBuscarPorCriteriosControlEnvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosControlEnvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosControlEnvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

}
