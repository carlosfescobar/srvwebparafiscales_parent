
package co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1.ControlEnvioComunicacionTipo;


/**
 * <p>Clase Java para OpCrearControlEnvioComunicacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearControlEnvioComunicacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="controlEnvioComunicacion" type="{http://www.ugpp.gov.co/schema/Notificaciones/ControlEnvioComunicacionTipo/v1}ControlEnvioComunicacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearControlEnvioComunicacionSolTipo", propOrder = {
    "contextoTransaccional",
    "controlEnvioComunicacion"
})
public class OpCrearControlEnvioComunicacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ControlEnvioComunicacionTipo controlEnvioComunicacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad controlEnvioComunicacion.
     * 
     * @return
     *     possible object is
     *     {@link ControlEnvioComunicacionTipo }
     *     
     */
    public ControlEnvioComunicacionTipo getControlEnvioComunicacion() {
        return controlEnvioComunicacion;
    }

    /**
     * Define el valor de la propiedad controlEnvioComunicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlEnvioComunicacionTipo }
     *     
     */
    public void setControlEnvioComunicacion(ControlEnvioComunicacionTipo value) {
        this.controlEnvioComunicacion = value;
    }

}
