
package co.gov.ugpp.notificaciones.srvaplinteresado.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplinteresado.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdInteresadoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpBuscarPorIdInteresadoSol");
    private final static QName _OpActualizarInteresadoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpActualizarInteresadoResp");
    private final static QName _OpBuscarPorIdInteresadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpBuscarPorIdInteresadoFallo");
    private final static QName _OpBuscarPorIdInteresadoResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpBuscarPorIdInteresadoResp");
    private final static QName _OpActualizarInteresadoSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpActualizarInteresadoSol");
    private final static QName _OpActualizarInteresadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", "OpActualizarInteresadoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplinteresado.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarInteresadoRespTipo }
     * 
     */
    public OpActualizarInteresadoRespTipo createOpActualizarInteresadoRespTipo() {
        return new OpActualizarInteresadoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInteresadoSolTipo }
     * 
     */
    public OpBuscarPorIdInteresadoSolTipo createOpBuscarPorIdInteresadoSolTipo() {
        return new OpBuscarPorIdInteresadoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInteresadoRespTipo }
     * 
     */
    public OpBuscarPorIdInteresadoRespTipo createOpBuscarPorIdInteresadoRespTipo() {
        return new OpBuscarPorIdInteresadoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarInteresadoSolTipo }
     * 
     */
    public OpActualizarInteresadoSolTipo createOpActualizarInteresadoSolTipo() {
        return new OpActualizarInteresadoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInteresadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpBuscarPorIdInteresadoSol")
    public JAXBElement<OpBuscarPorIdInteresadoSolTipo> createOpBuscarPorIdInteresadoSol(OpBuscarPorIdInteresadoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdInteresadoSolTipo>(_OpBuscarPorIdInteresadoSol_QNAME, OpBuscarPorIdInteresadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarInteresadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpActualizarInteresadoResp")
    public JAXBElement<OpActualizarInteresadoRespTipo> createOpActualizarInteresadoResp(OpActualizarInteresadoRespTipo value) {
        return new JAXBElement<OpActualizarInteresadoRespTipo>(_OpActualizarInteresadoResp_QNAME, OpActualizarInteresadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpBuscarPorIdInteresadoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdInteresadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdInteresadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInteresadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpBuscarPorIdInteresadoResp")
    public JAXBElement<OpBuscarPorIdInteresadoRespTipo> createOpBuscarPorIdInteresadoResp(OpBuscarPorIdInteresadoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdInteresadoRespTipo>(_OpBuscarPorIdInteresadoResp_QNAME, OpBuscarPorIdInteresadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarInteresadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpActualizarInteresadoSol")
    public JAXBElement<OpActualizarInteresadoSolTipo> createOpActualizarInteresadoSol(OpActualizarInteresadoSolTipo value) {
        return new JAXBElement<OpActualizarInteresadoSolTipo>(_OpActualizarInteresadoSol_QNAME, OpActualizarInteresadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1", name = "OpActualizarInteresadoFallo")
    public JAXBElement<FalloTipo> createOpActualizarInteresadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarInteresadoFallo_QNAME, FalloTipo.class, null, value);
    }

}
