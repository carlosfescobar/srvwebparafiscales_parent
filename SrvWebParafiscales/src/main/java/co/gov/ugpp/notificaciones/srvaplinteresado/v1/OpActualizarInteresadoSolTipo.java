
package co.gov.ugpp.notificaciones.srvaplinteresado.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;


/**
 * <p>Clase Java para OpActualizarInteresadoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarInteresadoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="interesado" type="{http://www.ugpp.gov.co/schema/Notificaciones/InteresadoTipo/v1}InteresadoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarInteresadoSolTipo", propOrder = {
    "contextoTransaccional",
    "interesado"
})
public class OpActualizarInteresadoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected InteresadoTipo interesado;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad interesado.
     * 
     * @return
     *     possible object is
     *     {@link InteresadoTipo }
     *     
     */
    public InteresadoTipo getInteresado() {
        return interesado;
    }

    /**
     * Define el valor de la propiedad interesado.
     * 
     * @param value
     *     allowed object is
     *     {@link InteresadoTipo }
     *     
     */
    public void setInteresado(InteresadoTipo value) {
        this.interesado = value;
    }

}
