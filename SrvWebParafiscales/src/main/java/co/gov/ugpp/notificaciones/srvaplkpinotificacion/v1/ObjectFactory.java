
package co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarNotificacionKPIPorEstadosResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPIPorEstadosResp");
    private final static QName _OpConsultarNotificacionKPISol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPISol");
    private final static QName _OpConsultarNotificacionKPIPorEstadosSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPIPorEstadosSol");
    private final static QName _OpConsultarNotificacionKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPIFallo");
    private final static QName _OpConsultarNotificacionKPIPorEstadosFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPIPorEstadosFallo");
    private final static QName _OpConsultarNotificacionKPIResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", "OpConsultarNotificacionKPIResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarNotificacionKPIPorEstadosRespTipo }
     * 
     */
    public OpConsultarNotificacionKPIPorEstadosRespTipo createOpConsultarNotificacionKPIPorEstadosRespTipo() {
        return new OpConsultarNotificacionKPIPorEstadosRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarNotificacionKPIRespTipo }
     * 
     */
    public OpConsultarNotificacionKPIRespTipo createOpConsultarNotificacionKPIRespTipo() {
        return new OpConsultarNotificacionKPIRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarNotificacionKPIPorEstadosSolTipo }
     * 
     */
    public OpConsultarNotificacionKPIPorEstadosSolTipo createOpConsultarNotificacionKPIPorEstadosSolTipo() {
        return new OpConsultarNotificacionKPIPorEstadosSolTipo();
    }

    /**
     * Create an instance of {@link OpConsultarNotificacionKPISolTipo }
     * 
     */
    public OpConsultarNotificacionKPISolTipo createOpConsultarNotificacionKPISolTipo() {
        return new OpConsultarNotificacionKPISolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarNotificacionKPIPorEstadosRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPIPorEstadosResp")
    public JAXBElement<OpConsultarNotificacionKPIPorEstadosRespTipo> createOpConsultarNotificacionKPIPorEstadosResp(OpConsultarNotificacionKPIPorEstadosRespTipo value) {
        return new JAXBElement<OpConsultarNotificacionKPIPorEstadosRespTipo>(_OpConsultarNotificacionKPIPorEstadosResp_QNAME, OpConsultarNotificacionKPIPorEstadosRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarNotificacionKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPISol")
    public JAXBElement<OpConsultarNotificacionKPISolTipo> createOpConsultarNotificacionKPISol(OpConsultarNotificacionKPISolTipo value) {
        return new JAXBElement<OpConsultarNotificacionKPISolTipo>(_OpConsultarNotificacionKPISol_QNAME, OpConsultarNotificacionKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarNotificacionKPIPorEstadosSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPIPorEstadosSol")
    public JAXBElement<OpConsultarNotificacionKPIPorEstadosSolTipo> createOpConsultarNotificacionKPIPorEstadosSol(OpConsultarNotificacionKPIPorEstadosSolTipo value) {
        return new JAXBElement<OpConsultarNotificacionKPIPorEstadosSolTipo>(_OpConsultarNotificacionKPIPorEstadosSol_QNAME, OpConsultarNotificacionKPIPorEstadosSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarNotificacionKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarNotificacionKPIFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPIPorEstadosFallo")
    public JAXBElement<FalloTipo> createOpConsultarNotificacionKPIPorEstadosFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarNotificacionKPIPorEstadosFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarNotificacionKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1", name = "OpConsultarNotificacionKPIResp")
    public JAXBElement<OpConsultarNotificacionKPIRespTipo> createOpConsultarNotificacionKPIResp(OpConsultarNotificacionKPIRespTipo value) {
        return new JAXBElement<OpConsultarNotificacionKPIRespTipo>(_OpConsultarNotificacionKPIResp_QNAME, OpConsultarNotificacionKPIRespTipo.class, null, value);
    }

}
