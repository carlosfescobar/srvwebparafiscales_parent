
package co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearMecanismoSubsidiarioResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpCrearMecanismoSubsidiarioResp");
    private final static QName _OpBuscarPorCriteriosMecanismoSubsidiarioFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpBuscarPorCriteriosMecanismoSubsidiarioFallo");
    private final static QName _OpCrearMecanismoSubsidiarioSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpCrearMecanismoSubsidiarioSol");
    private final static QName _OpActualizarMecanismoSubsidiarioFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpActualizarMecanismoSubsidiarioFallo");
    private final static QName _OpBuscarPorCriteriosMecanismoSubsidiarioSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpBuscarPorCriteriosMecanismoSubsidiarioSol");
    private final static QName _OpCrearMecanismoSubsidiarioFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpCrearMecanismoSubsidiarioFallo");
    private final static QName _OpActualizarMecanismoSubsidiarioSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpActualizarMecanismoSubsidiarioSol");
    private final static QName _OpActualizarMecanismoSubsidiarioResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpActualizarMecanismoSubsidiarioResp");
    private final static QName _OpBuscarPorCriteriosMecanismoSubsidiarioResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", "OpBuscarPorCriteriosMecanismoSubsidiarioResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo }
     * 
     */
    public OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo createOpBuscarPorCriteriosMecanismoSubsidiarioSolTipo() {
        return new OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearMecanismoSubsidiarioSolTipo }
     * 
     */
    public OpCrearMecanismoSubsidiarioSolTipo createOpCrearMecanismoSubsidiarioSolTipo() {
        return new OpCrearMecanismoSubsidiarioSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearMecanismoSubsidiarioRespTipo }
     * 
     */
    public OpCrearMecanismoSubsidiarioRespTipo createOpCrearMecanismoSubsidiarioRespTipo() {
        return new OpCrearMecanismoSubsidiarioRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo }
     * 
     */
    public OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo createOpBuscarPorCriteriosMecanismoSubsidiarioRespTipo() {
        return new OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarMecanismoSubsidiarioRespTipo }
     * 
     */
    public OpActualizarMecanismoSubsidiarioRespTipo createOpActualizarMecanismoSubsidiarioRespTipo() {
        return new OpActualizarMecanismoSubsidiarioRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarMecanismoSubsidiarioSolTipo }
     * 
     */
    public OpActualizarMecanismoSubsidiarioSolTipo createOpActualizarMecanismoSubsidiarioSolTipo() {
        return new OpActualizarMecanismoSubsidiarioSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearMecanismoSubsidiarioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpCrearMecanismoSubsidiarioResp")
    public JAXBElement<OpCrearMecanismoSubsidiarioRespTipo> createOpCrearMecanismoSubsidiarioResp(OpCrearMecanismoSubsidiarioRespTipo value) {
        return new JAXBElement<OpCrearMecanismoSubsidiarioRespTipo>(_OpCrearMecanismoSubsidiarioResp_QNAME, OpCrearMecanismoSubsidiarioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpBuscarPorCriteriosMecanismoSubsidiarioFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosMecanismoSubsidiarioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosMecanismoSubsidiarioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearMecanismoSubsidiarioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpCrearMecanismoSubsidiarioSol")
    public JAXBElement<OpCrearMecanismoSubsidiarioSolTipo> createOpCrearMecanismoSubsidiarioSol(OpCrearMecanismoSubsidiarioSolTipo value) {
        return new JAXBElement<OpCrearMecanismoSubsidiarioSolTipo>(_OpCrearMecanismoSubsidiarioSol_QNAME, OpCrearMecanismoSubsidiarioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpActualizarMecanismoSubsidiarioFallo")
    public JAXBElement<FalloTipo> createOpActualizarMecanismoSubsidiarioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarMecanismoSubsidiarioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpBuscarPorCriteriosMecanismoSubsidiarioSol")
    public JAXBElement<OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo> createOpBuscarPorCriteriosMecanismoSubsidiarioSol(OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo>(_OpBuscarPorCriteriosMecanismoSubsidiarioSol_QNAME, OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpCrearMecanismoSubsidiarioFallo")
    public JAXBElement<FalloTipo> createOpCrearMecanismoSubsidiarioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearMecanismoSubsidiarioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarMecanismoSubsidiarioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpActualizarMecanismoSubsidiarioSol")
    public JAXBElement<OpActualizarMecanismoSubsidiarioSolTipo> createOpActualizarMecanismoSubsidiarioSol(OpActualizarMecanismoSubsidiarioSolTipo value) {
        return new JAXBElement<OpActualizarMecanismoSubsidiarioSolTipo>(_OpActualizarMecanismoSubsidiarioSol_QNAME, OpActualizarMecanismoSubsidiarioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarMecanismoSubsidiarioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpActualizarMecanismoSubsidiarioResp")
    public JAXBElement<OpActualizarMecanismoSubsidiarioRespTipo> createOpActualizarMecanismoSubsidiarioResp(OpActualizarMecanismoSubsidiarioRespTipo value) {
        return new JAXBElement<OpActualizarMecanismoSubsidiarioRespTipo>(_OpActualizarMecanismoSubsidiarioResp_QNAME, OpActualizarMecanismoSubsidiarioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1", name = "OpBuscarPorCriteriosMecanismoSubsidiarioResp")
    public JAXBElement<OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo> createOpBuscarPorCriteriosMecanismoSubsidiarioResp(OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo>(_OpBuscarPorCriteriosMecanismoSubsidiarioResp_QNAME, OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo.class, null, value);
    }

}
