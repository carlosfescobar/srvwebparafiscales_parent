
package co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.notificaciones.mecanismosubsidiariotipo.v1.MecanismoSubsidiarioTipo;


/**
 * <p>Clase Java para OpCrearMecanismoSubsidiarioSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearMecanismoSubsidiarioSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="mecanismoSubsidiario" type="{http://www.ugpp.gov.co/schema/Notificaciones/MecanismoSubsidiarioTipo/v1}MecanismoSubsidiarioTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearMecanismoSubsidiarioSolTipo", propOrder = {
    "contextoTransaccional",
    "mecanismoSubsidiario"
})
public class OpCrearMecanismoSubsidiarioSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected MecanismoSubsidiarioTipo mecanismoSubsidiario;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad mecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link MecanismoSubsidiarioTipo }
     *     
     */
    public MecanismoSubsidiarioTipo getMecanismoSubsidiario() {
        return mecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad mecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link MecanismoSubsidiarioTipo }
     *     
     */
    public void setMecanismoSubsidiario(MecanismoSubsidiarioTipo value) {
        this.mecanismoSubsidiario = value;
    }

}
