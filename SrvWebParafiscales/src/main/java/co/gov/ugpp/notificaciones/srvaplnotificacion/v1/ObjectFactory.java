
package co.gov.ugpp.notificaciones.srvaplnotificacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvaplnotificacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosNotificacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpBuscarPorCriteriosNotificacionResp");
    private final static QName _OpCrearNotificacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpCrearNotificacionFallo");
    private final static QName _OpBuscarPorCriteriosNotificacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpBuscarPorCriteriosNotificacionFallo");
    private final static QName _OpActualizarNotificacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpActualizarNotificacionResp");
    private final static QName _OpCrearNotificacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpCrearNotificacionResp");
    private final static QName _OpActualizarNotificacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpActualizarNotificacionSol");
    private final static QName _OpActualizarNotificacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpActualizarNotificacionFallo");
    private final static QName _OpCrearNotificacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpCrearNotificacionSol");
    private final static QName _OpBuscarPorCriteriosNotificacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", "OpBuscarPorCriteriosNotificacionSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvaplnotificacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearNotificacionSolTipo }
     * 
     */
    public OpCrearNotificacionSolTipo createOpCrearNotificacionSolTipo() {
        return new OpCrearNotificacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosNotificacionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosNotificacionSolTipo createOpBuscarPorCriteriosNotificacionSolTipo() {
        return new OpBuscarPorCriteriosNotificacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearNotificacionRespTipo }
     * 
     */
    public OpCrearNotificacionRespTipo createOpCrearNotificacionRespTipo() {
        return new OpCrearNotificacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarNotificacionSolTipo }
     * 
     */
    public OpActualizarNotificacionSolTipo createOpActualizarNotificacionSolTipo() {
        return new OpActualizarNotificacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarNotificacionRespTipo }
     * 
     */
    public OpActualizarNotificacionRespTipo createOpActualizarNotificacionRespTipo() {
        return new OpActualizarNotificacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosNotificacionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosNotificacionRespTipo createOpBuscarPorCriteriosNotificacionRespTipo() {
        return new OpBuscarPorCriteriosNotificacionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosNotificacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpBuscarPorCriteriosNotificacionResp")
    public JAXBElement<OpBuscarPorCriteriosNotificacionRespTipo> createOpBuscarPorCriteriosNotificacionResp(OpBuscarPorCriteriosNotificacionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosNotificacionRespTipo>(_OpBuscarPorCriteriosNotificacionResp_QNAME, OpBuscarPorCriteriosNotificacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpCrearNotificacionFallo")
    public JAXBElement<FalloTipo> createOpCrearNotificacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearNotificacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpBuscarPorCriteriosNotificacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosNotificacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosNotificacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarNotificacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpActualizarNotificacionResp")
    public JAXBElement<OpActualizarNotificacionRespTipo> createOpActualizarNotificacionResp(OpActualizarNotificacionRespTipo value) {
        return new JAXBElement<OpActualizarNotificacionRespTipo>(_OpActualizarNotificacionResp_QNAME, OpActualizarNotificacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearNotificacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpCrearNotificacionResp")
    public JAXBElement<OpCrearNotificacionRespTipo> createOpCrearNotificacionResp(OpCrearNotificacionRespTipo value) {
        return new JAXBElement<OpCrearNotificacionRespTipo>(_OpCrearNotificacionResp_QNAME, OpCrearNotificacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarNotificacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpActualizarNotificacionSol")
    public JAXBElement<OpActualizarNotificacionSolTipo> createOpActualizarNotificacionSol(OpActualizarNotificacionSolTipo value) {
        return new JAXBElement<OpActualizarNotificacionSolTipo>(_OpActualizarNotificacionSol_QNAME, OpActualizarNotificacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpActualizarNotificacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarNotificacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarNotificacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearNotificacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpCrearNotificacionSol")
    public JAXBElement<OpCrearNotificacionSolTipo> createOpCrearNotificacionSol(OpCrearNotificacionSolTipo value) {
        return new JAXBElement<OpCrearNotificacionSolTipo>(_OpCrearNotificacionSol_QNAME, OpCrearNotificacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosNotificacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1", name = "OpBuscarPorCriteriosNotificacionSol")
    public JAXBElement<OpBuscarPorCriteriosNotificacionSolTipo> createOpBuscarPorCriteriosNotificacionSol(OpBuscarPorCriteriosNotificacionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosNotificacionSolTipo>(_OpBuscarPorCriteriosNotificacionSol_QNAME, OpBuscarPorCriteriosNotificacionSolTipo.class, null, value);
    }

}
