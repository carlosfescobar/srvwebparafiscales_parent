
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * <p>Clase Java para msjOpIniciarProcesoMecanismoSubsidiarioFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpIniciarProcesoMecanismoSubsidiarioFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoMecanismoSubsidiarioFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpIniciarProcesoMecanismoSubsidiarioFallo", propOrder = {
    "opIniciarProcesoMecanismoSubsidiarioFallo"
})
public class MsjOpIniciarProcesoMecanismoSubsidiarioFallo {

    @XmlElement(name = "OpIniciarProcesoMecanismoSubsidiarioFallo", nillable = true)
    protected FalloTipo opIniciarProcesoMecanismoSubsidiarioFallo;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpIniciarProcesoMecanismoSubsidiarioFallo() {
        return opIniciarProcesoMecanismoSubsidiarioFallo;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpIniciarProcesoMecanismoSubsidiarioFallo(FalloTipo value) {
        this.opIniciarProcesoMecanismoSubsidiarioFallo = value;
    }

}
