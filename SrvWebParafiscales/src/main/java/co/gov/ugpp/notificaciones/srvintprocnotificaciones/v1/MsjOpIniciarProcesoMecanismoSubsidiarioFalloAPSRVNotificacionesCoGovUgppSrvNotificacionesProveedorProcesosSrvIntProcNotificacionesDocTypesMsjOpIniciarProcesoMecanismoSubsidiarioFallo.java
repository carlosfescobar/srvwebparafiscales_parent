
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "msjOpIniciarProcesoMecanismoSubsidiarioFallo", targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1")
public class MsjOpIniciarProcesoMecanismoSubsidiarioFalloAPSRVNotificacionesCoGovUgppSrvNotificacionesProveedorProcesosSrvIntProcNotificacionesDocTypesMsjOpIniciarProcesoMecanismoSubsidiarioFallo
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private MsjOpIniciarProcesoMecanismoSubsidiarioFallo faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public MsjOpIniciarProcesoMecanismoSubsidiarioFalloAPSRVNotificacionesCoGovUgppSrvNotificacionesProveedorProcesosSrvIntProcNotificacionesDocTypesMsjOpIniciarProcesoMecanismoSubsidiarioFallo(String message, MsjOpIniciarProcesoMecanismoSubsidiarioFallo faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public MsjOpIniciarProcesoMecanismoSubsidiarioFalloAPSRVNotificacionesCoGovUgppSrvNotificacionesProveedorProcesosSrvIntProcNotificacionesDocTypesMsjOpIniciarProcesoMecanismoSubsidiarioFallo(String message, MsjOpIniciarProcesoMecanismoSubsidiarioFallo faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1.MsjOpIniciarProcesoMecanismoSubsidiarioFallo
     */
    public MsjOpIniciarProcesoMecanismoSubsidiarioFallo getFaultInfo() {
        return faultInfo;
    }

}
