
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * <p>Clase Java para msjOpIniciarProcesoNotificacionFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpIniciarProcesoNotificacionFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoNotificacionFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpIniciarProcesoNotificacionFallo", propOrder = {
    "opIniciarProcesoNotificacionFallo"
})
public class MsjOpIniciarProcesoNotificacionFallo {

    @XmlElement(name = "OpIniciarProcesoNotificacionFallo", nillable = true)
    protected FalloTipo opIniciarProcesoNotificacionFallo;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoNotificacionFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpIniciarProcesoNotificacionFallo() {
        return opIniciarProcesoNotificacionFallo;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoNotificacionFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpIniciarProcesoNotificacionFallo(FalloTipo value) {
        this.opIniciarProcesoNotificacionFallo = value;
    }

}
