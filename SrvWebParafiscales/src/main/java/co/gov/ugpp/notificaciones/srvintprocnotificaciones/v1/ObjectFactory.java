
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Input");
    private final static QName _OpIniciarProcesoNotificacionSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoNotificacionSol");
    private final static QName _OpIniciarProcesoNotificacionResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoNotificacionResp");
    private final static QName _OpIniciarProcesoMecanismoSubsidiarioResp_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoMecanismoSubsidiarioResp");
    private final static QName _PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Output");
    private final static QName _MsjOpIniciarProcesoNotificacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "msjOpIniciarProcesoNotificacionFallo");
    private final static QName _MsjOpIniciarProcesoMecanismoSubsidiarioFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "msjOpIniciarProcesoMecanismoSubsidiarioFallo");
    private final static QName _PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Input");
    private final static QName _PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Output");
    private final static QName _OpIniciarProcesoNotificacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoNotificacionFallo");
    private final static QName _OpIniciarProcesoMecanismoSubsidiarioSol_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoMecanismoSubsidiarioSol");
    private final static QName _OpIniciarProcesoMecanismoSubsidiarioFallo_QNAME = new QName("http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", "OpIniciarProcesoMecanismoSubsidiarioFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpIniciarProcesoNotificacionSolTipo }
     * 
     */
    public OpIniciarProcesoNotificacionSolTipo createOpIniciarProcesoNotificacionSolTipo() {
        return new OpIniciarProcesoNotificacionSolTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput }
     * 
     */
    public PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput() {
        return new PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput();
    }

    /**
     * Create an instance of {@link MsjOpIniciarProcesoNotificacionFallo }
     * 
     */
    public MsjOpIniciarProcesoNotificacionFallo createMsjOpIniciarProcesoNotificacionFallo() {
        return new MsjOpIniciarProcesoNotificacionFallo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput }
     * 
     */
    public PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput() {
        return new PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput();
    }

    /**
     * Create an instance of {@link OpIniciarProcesoMecanismoSubsidiarioRespTipo }
     * 
     */
    public OpIniciarProcesoMecanismoSubsidiarioRespTipo createOpIniciarProcesoMecanismoSubsidiarioRespTipo() {
        return new OpIniciarProcesoMecanismoSubsidiarioRespTipo();
    }

    /**
     * Create an instance of {@link OpIniciarProcesoNotificacionRespTipo }
     * 
     */
    public OpIniciarProcesoNotificacionRespTipo createOpIniciarProcesoNotificacionRespTipo() {
        return new OpIniciarProcesoNotificacionRespTipo();
    }

    /**
     * Create an instance of {@link MsjOpIniciarProcesoMecanismoSubsidiarioFallo }
     * 
     */
    public MsjOpIniciarProcesoMecanismoSubsidiarioFallo createMsjOpIniciarProcesoMecanismoSubsidiarioFallo() {
        return new MsjOpIniciarProcesoMecanismoSubsidiarioFallo();
    }

    /**
     * Create an instance of {@link OpIniciarProcesoMecanismoSubsidiarioSolTipo }
     * 
     */
    public OpIniciarProcesoMecanismoSubsidiarioSolTipo createOpIniciarProcesoMecanismoSubsidiarioSolTipo() {
        return new OpIniciarProcesoMecanismoSubsidiarioSolTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput }
     * 
     */
    public PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput() {
        return new PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput();
    }

    /**
     * Create an instance of {@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput }
     * 
     */
    public PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput() {
        return new PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Input")
    public JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput> createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput(PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput value) {
        return new JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput>(_PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput_QNAME, PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpIniciarProcesoNotificacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoNotificacionSol")
    public JAXBElement<OpIniciarProcesoNotificacionSolTipo> createOpIniciarProcesoNotificacionSol(OpIniciarProcesoNotificacionSolTipo value) {
        return new JAXBElement<OpIniciarProcesoNotificacionSolTipo>(_OpIniciarProcesoNotificacionSol_QNAME, OpIniciarProcesoNotificacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpIniciarProcesoNotificacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoNotificacionResp")
    public JAXBElement<OpIniciarProcesoNotificacionRespTipo> createOpIniciarProcesoNotificacionResp(OpIniciarProcesoNotificacionRespTipo value) {
        return new JAXBElement<OpIniciarProcesoNotificacionRespTipo>(_OpIniciarProcesoNotificacionResp_QNAME, OpIniciarProcesoNotificacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpIniciarProcesoMecanismoSubsidiarioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoMecanismoSubsidiarioResp")
    public JAXBElement<OpIniciarProcesoMecanismoSubsidiarioRespTipo> createOpIniciarProcesoMecanismoSubsidiarioResp(OpIniciarProcesoMecanismoSubsidiarioRespTipo value) {
        return new JAXBElement<OpIniciarProcesoMecanismoSubsidiarioRespTipo>(_OpIniciarProcesoMecanismoSubsidiarioResp_QNAME, OpIniciarProcesoMecanismoSubsidiarioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Output")
    public JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput> createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput(PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput value) {
        return new JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput>(_PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput_QNAME, PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpIniciarProcesoNotificacionFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "msjOpIniciarProcesoNotificacionFallo")
    public JAXBElement<MsjOpIniciarProcesoNotificacionFallo> createMsjOpIniciarProcesoNotificacionFallo(MsjOpIniciarProcesoNotificacionFallo value) {
        return new JAXBElement<MsjOpIniciarProcesoNotificacionFallo>(_MsjOpIniciarProcesoNotificacionFallo_QNAME, MsjOpIniciarProcesoNotificacionFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpIniciarProcesoMecanismoSubsidiarioFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "msjOpIniciarProcesoMecanismoSubsidiarioFallo")
    public JAXBElement<MsjOpIniciarProcesoMecanismoSubsidiarioFallo> createMsjOpIniciarProcesoMecanismoSubsidiarioFallo(MsjOpIniciarProcesoMecanismoSubsidiarioFallo value) {
        return new JAXBElement<MsjOpIniciarProcesoMecanismoSubsidiarioFallo>(_MsjOpIniciarProcesoMecanismoSubsidiarioFallo_QNAME, MsjOpIniciarProcesoMecanismoSubsidiarioFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Input")
    public JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput> createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput(PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput value) {
        return new JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput>(_PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput_QNAME, PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Output")
    public JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput> createPortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput(PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput value) {
        return new JAXBElement<PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput>(_PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput_QNAME, PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoNotificacionFallo")
    public JAXBElement<FalloTipo> createOpIniciarProcesoNotificacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpIniciarProcesoNotificacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpIniciarProcesoMecanismoSubsidiarioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoMecanismoSubsidiarioSol")
    public JAXBElement<OpIniciarProcesoMecanismoSubsidiarioSolTipo> createOpIniciarProcesoMecanismoSubsidiarioSol(OpIniciarProcesoMecanismoSubsidiarioSolTipo value) {
        return new JAXBElement<OpIniciarProcesoMecanismoSubsidiarioSolTipo>(_OpIniciarProcesoMecanismoSubsidiarioSol_QNAME, OpIniciarProcesoMecanismoSubsidiarioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1", name = "OpIniciarProcesoMecanismoSubsidiarioFallo")
    public JAXBElement<FalloTipo> createOpIniciarProcesoMecanismoSubsidiarioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpIniciarProcesoMecanismoSubsidiarioFallo_QNAME, FalloTipo.class, null, value);
    }

}
