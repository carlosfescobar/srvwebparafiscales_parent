
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;


/**
 * <p>Clase Java para OpIniciarProcesoNotificacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpIniciarProcesoNotificacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="notificacion" type="{http://www.ugpp.gov.co/schema/Notificaciones/NotificacionTipo/v1}NotificacionTipo"/>
 *         &lt;element name="funcionario" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpIniciarProcesoNotificacionSolTipo", propOrder = {
    "contextoTransaccional",
    "notificacion",
    "funcionario"
})
public class OpIniciarProcesoNotificacionSolTipo {

    @XmlElement(namespace = "", required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(namespace = "", required = true)
    protected NotificacionTipo notificacion;
    @XmlElement(namespace = "", required = true, nillable = true)
    protected FuncionarioTipo funcionario;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad notificacion.
     * 
     * @return
     *     possible object is
     *     {@link NotificacionTipo }
     *     
     */
    public NotificacionTipo getNotificacion() {
        return notificacion;
    }

    /**
     * Define el valor de la propiedad notificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificacionTipo }
     *     
     */
    public void setNotificacion(NotificacionTipo value) {
        this.notificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionario.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionario() {
        return funcionario;
    }

    /**
     * Define el valor de la propiedad funcionario.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionario(FuncionarioTipo value) {
        this.funcionario = value;
    }

}
