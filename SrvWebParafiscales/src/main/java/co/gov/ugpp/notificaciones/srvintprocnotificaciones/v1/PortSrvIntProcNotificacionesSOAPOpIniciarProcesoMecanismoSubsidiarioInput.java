
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoMecanismoSubsidiarioSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Input", propOrder = {
    "opIniciarProcesoMecanismoSubsidiarioSol"
})
public class PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioInput {

    @XmlElement(name = "OpIniciarProcesoMecanismoSubsidiarioSol", nillable = true)
    protected OpIniciarProcesoMecanismoSubsidiarioSolTipo opIniciarProcesoMecanismoSubsidiarioSol;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioSol.
     * 
     * @return
     *     possible object is
     *     {@link OpIniciarProcesoMecanismoSubsidiarioSolTipo }
     *     
     */
    public OpIniciarProcesoMecanismoSubsidiarioSolTipo getOpIniciarProcesoMecanismoSubsidiarioSol() {
        return opIniciarProcesoMecanismoSubsidiarioSol;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpIniciarProcesoMecanismoSubsidiarioSolTipo }
     *     
     */
    public void setOpIniciarProcesoMecanismoSubsidiarioSol(OpIniciarProcesoMecanismoSubsidiarioSolTipo value) {
        this.opIniciarProcesoMecanismoSubsidiarioSol = value;
    }

}
