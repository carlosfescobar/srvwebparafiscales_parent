
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoMecanismoSubsidiarioResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoMecanismoSubsidiario_Output", propOrder = {
    "opIniciarProcesoMecanismoSubsidiarioResp"
})
public class PortSrvIntProcNotificacionesSOAPOpIniciarProcesoMecanismoSubsidiarioOutput {

    @XmlElement(name = "OpIniciarProcesoMecanismoSubsidiarioResp", nillable = true)
    protected OpIniciarProcesoMecanismoSubsidiarioRespTipo opIniciarProcesoMecanismoSubsidiarioResp;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioResp.
     * 
     * @return
     *     possible object is
     *     {@link OpIniciarProcesoMecanismoSubsidiarioRespTipo }
     *     
     */
    public OpIniciarProcesoMecanismoSubsidiarioRespTipo getOpIniciarProcesoMecanismoSubsidiarioResp() {
        return opIniciarProcesoMecanismoSubsidiarioResp;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoMecanismoSubsidiarioResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpIniciarProcesoMecanismoSubsidiarioRespTipo }
     *     
     */
    public void setOpIniciarProcesoMecanismoSubsidiarioResp(OpIniciarProcesoMecanismoSubsidiarioRespTipo value) {
        this.opIniciarProcesoMecanismoSubsidiarioResp = value;
    }

}
