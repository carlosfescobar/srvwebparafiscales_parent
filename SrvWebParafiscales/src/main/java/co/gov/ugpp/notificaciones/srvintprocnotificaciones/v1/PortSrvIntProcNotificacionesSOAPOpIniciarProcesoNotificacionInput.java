
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoNotificacionSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Input", propOrder = {
    "opIniciarProcesoNotificacionSol"
})
public class PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionInput {

    @XmlElement(name = "OpIniciarProcesoNotificacionSol", nillable = true)
    protected OpIniciarProcesoNotificacionSolTipo opIniciarProcesoNotificacionSol;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoNotificacionSol.
     * 
     * @return
     *     possible object is
     *     {@link OpIniciarProcesoNotificacionSolTipo }
     *     
     */
    public OpIniciarProcesoNotificacionSolTipo getOpIniciarProcesoNotificacionSol() {
        return opIniciarProcesoNotificacionSol;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoNotificacionSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpIniciarProcesoNotificacionSolTipo }
     *     
     */
    public void setOpIniciarProcesoNotificacionSol(OpIniciarProcesoNotificacionSolTipo value) {
        this.opIniciarProcesoNotificacionSol = value;
    }

}
