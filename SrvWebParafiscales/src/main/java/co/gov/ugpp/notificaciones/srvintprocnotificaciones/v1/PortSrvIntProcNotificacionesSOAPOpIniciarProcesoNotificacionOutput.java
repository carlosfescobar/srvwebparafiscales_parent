
package co.gov.ugpp.notificaciones.srvintprocnotificaciones.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Notificaciones/SrvIntProcNotificaciones/v1}OpIniciarProcesoNotificacionResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcNotificacionesSOAP_OpIniciarProcesoNotificacion_Output", propOrder = {
    "opIniciarProcesoNotificacionResp"
})
public class PortSrvIntProcNotificacionesSOAPOpIniciarProcesoNotificacionOutput {

    @XmlElement(name = "OpIniciarProcesoNotificacionResp", nillable = true)
    protected OpIniciarProcesoNotificacionRespTipo opIniciarProcesoNotificacionResp;

    /**
     * Obtiene el valor de la propiedad opIniciarProcesoNotificacionResp.
     * 
     * @return
     *     possible object is
     *     {@link OpIniciarProcesoNotificacionRespTipo }
     *     
     */
    public OpIniciarProcesoNotificacionRespTipo getOpIniciarProcesoNotificacionResp() {
        return opIniciarProcesoNotificacionResp;
    }

    /**
     * Define el valor de la propiedad opIniciarProcesoNotificacionResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpIniciarProcesoNotificacionRespTipo }
     *     
     */
    public void setOpIniciarProcesoNotificacionResp(OpIniciarProcesoNotificacionRespTipo value) {
        this.opIniciarProcesoNotificacionResp = value;
    }

}
