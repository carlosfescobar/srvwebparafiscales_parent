package co.gov.ugpp.parafiscales;

import java.io.Serializable;
import javax.ejb.EJB;

/**
 *
 * @author jmunocab
 */
public abstract class AbstractFacade implements Serializable {    
    @EJB
    protected MapperFacade mapper;
}
