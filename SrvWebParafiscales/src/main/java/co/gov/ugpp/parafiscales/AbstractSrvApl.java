package co.gov.ugpp.parafiscales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.DateUtil;
import javax.ejb.EJB;

/**
 *
 * @author rpadilla
 */
public abstract class AbstractSrvApl {

    @EJB
    protected LdapAuthentication ldapAuthentication;

    public void initContextoRespuesta(final ContextoTransaccionalTipo ct, final ContextoRespuestaTipo cr) throws AppException {

        if (ct == null) {
            throw new AppException("El contextoTransaccional no puede ser nulo");
        }
        
        cr.setIdTx(ct.getIdTx());
        cr.setCodEstadoTx(ErrorEnum.EXITO.getCode());
        cr.setFechaTx(DateUtil.currentCalendar());
        cr.setIdInstanciaActividad(ct.getIdInstanciaActividad());
        cr.setIdInstanciaProceso(ct.getIdInstanciaProceso());
        cr.setValNumPagina(ct.getValNumPagina());        
    }

}
