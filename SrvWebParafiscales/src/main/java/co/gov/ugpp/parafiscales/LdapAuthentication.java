package co.gov.ugpp.parafiscales;

//import co.gov.ugpp.parafiscales.procesos.enums.Constantes;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.exception.TechnicalException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Properties;
import javax.ejb.Stateless;
import javax.naming.AuthenticationException;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jsaenzar
 */
@Stateless
public class LdapAuthentication {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(LdapAuthentication.class);

    private static final String file = "config.properties";

    private static final Properties props = loadProperties();

    /**
     * Autenticación al LDAP
     *
     * @param user
     * @param password
     * @autor janguloh - everis
     * @throws AppException
     */
    public void validateLdapAuthentication(String user, String password) throws AppException {

        final Properties properties = obtainProperties();

        DirContext ldapContext = createLdapConnection(properties, user, password);
        closeLdapConnection(ldapContext);

    }

    public DirContext getLdapConnection(Properties properties, String user, String password) throws AppException {

        DirContext ldapContext = createLdapConnection(properties, user, password);

        return ldapContext;
    }

    private void validateUser() {

    }

    /**
     * Crear la conexión a LDAP
     *
     * @param properties
     * @param user
     * @param password
     * @autor janguloh - everis
     * @return
     * @throws AppException
     */
    private DirContext createLdapConnection(final Properties properties, String user, final String password) throws AppException {

        try {

            String prefijo = properties.getProperty("ldap.contexto.prefijo");
            if (StringUtils.isNotBlank(user) && StringUtils.isNotBlank(prefijo)) {
                if (!user.contains("ugpp")) {
                    user = user.concat(prefijo);
                }
            }

            final Hashtable<String, String> ldapEnv = new Hashtable<String, String>();
            ldapEnv.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            ldapEnv.put(Context.PROVIDER_URL, properties.getProperty("ldap.contexto.url"));
            ldapEnv.put(Context.SECURITY_AUTHENTICATION, properties.getProperty("ldap.contexto.tipo.autenticacion"));
            ldapEnv.put(Context.SECURITY_PRINCIPAL, user);
            ldapEnv.put(Context.SECURITY_CREDENTIALS, password);
            ldapEnv.put("com.sun.jndi.ldap.read.timeout", properties.getProperty("timeout-millis"));

            final DirContext ldapContext = new InitialDirContext(ldapEnv);

            return ldapContext;

        } catch (AuthenticationException ex) {
            throw new AppException("Error en la autenticación de LDAP para el user: " + user, ex);
        } catch (NamingException ex) {
            throw new AppException("Error en la autenticación de LDAP para el user: " + user, ex);
        }
    }

    /**
     * Cargar archivo de propiedades "ldap.properties"
     *
     * @param file
     * @autor janguloh - everis
     * @return
     * @throws AppException
     */
    private static Properties loadProperties() {
        final Properties props = new Properties();
        InputStream input = null;
        try {
            input = LdapAuthentication.class.getClassLoader().getResourceAsStream(file);
            props.load(input);
        } catch (IOException ex) {
            throw new TechnicalException("Archivo de configuración de LDAP no encontrado", ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    LOG.warn("No se pudo cerrar el archivo ldap.properties", ex);
                }
            }
        }
        return props;
    }

    public static final Properties obtainProperties() {
        return props;
    }

    /**
     * Cerrar la conexión a LDAP
     *
     * @autor janguloh - everis
     * @param ldapContext
     */
    public void closeLdapConnection(DirContext ldapContext) {
        if (ldapContext != null) {
            try {
                ldapContext.close();
            } catch (NamingException e) {
                LOG.warn("Error cerrando la conexión de LDAP", e);
            }
        }
    }

}
