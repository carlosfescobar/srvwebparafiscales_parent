package co.gov.ugpp.parafiscales;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 *
 * @author rpadilla
 */
public interface MapperFacade extends Serializable {

    <S extends Object, D extends Object> D map(S srcObj, Class<D> destObj);
    
    <S extends Object, D extends Object> D map(S srcObj, D destObj);

    <S extends Object, D extends Object>
            List<D> map(List<S> source, Class<S> srcClass, Class<D> destClass);

    <S extends Object, D extends Object>
            Set<D> map(Set<S> source, Class<S> srcClass, Class<D> destClass);

}
