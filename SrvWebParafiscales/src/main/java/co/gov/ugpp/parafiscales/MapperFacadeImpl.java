package co.gov.ugpp.parafiscales;

import co.gov.ugpp.parafiscales.exception.TechnicalException;
import co.gov.ugpp.parafiscales.util.map.ConverterFactory;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.parafiscales.util.map.converter.AccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ActoAdministrativoToActoAdministrativoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ActoAdministrativoToAgrupacionAnexoActoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AfectadoTipo2AfectadoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AgrupaAfiliacionEntExtToAgrupaAfiliacionEntExtTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AgrupaEntExtPersonaToPersonaNaturalTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AgrupacionAdministradoraToAgrupacionSubsistemaAdminTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AgrupacionSubsistemaAdminToAgrupacionSubsistemaAdminTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AgrupacionSubsistemaSolAdminToAgrupacionSubsistemaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AnalisisSancionToAnalisisSancionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ApoderadoTipo2ApoderadoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AportanteTipo2AportanteConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AprobacionDocumentoTipo2AprobacionDocumentoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AprobacionMasivaActoToAprobacionMasivaActoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.AprobacionValidacionDocumentoToValidacionCreacionDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.Archivo2ArchivoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.BigDecimal2LongConverter;
import co.gov.ugpp.parafiscales.util.map.converter.BigInteger2IntegerConverter;
import co.gov.ugpp.parafiscales.util.map.converter.BigInteger2LongConverter;
import co.gov.ugpp.parafiscales.util.map.converter.Busqueda2BusquedaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.CasoMigradoToCasoMigradoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ConciliacionContableToConciliacionContableTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ConsDecRecursoToConsDecRecursoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ConsFecRecursoToConsFecRecursoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ConstanciaEjecutoriaToConstanciaEjecutoriaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ContactoPersonaTipo2ContactoPersonaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ControlAprobacionActoToControlAprobacionActoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ControlEnvioComunicacionToControlEnvioComunicacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ControlLiquidadorToControlLiquidadorTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ControlUbicacionToControlUbicacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.CorreoElectronicoTipo2CorreoElectronicoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.CotizanteTipo2CotizanteConverter;
import co.gov.ugpp.parafiscales.util.map.converter.CotizanteTipo2SolicitudCotizanteConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DenunciaToDenunciaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DenuncianteTipo2DenuncianteConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DepartamentoTipo2DepartamentoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DocumentacionEsperada2DocumentacionEsperadaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DocumentoAnexoActoToStringConverter;
import co.gov.ugpp.parafiscales.util.map.converter.DocumentoTipo2DocumentoEcmConverter;
import co.gov.ugpp.parafiscales.util.map.converter.EntidadExternaTipo2EntidadExternaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.EntidadNegocioArchivoTemporalToStringConverter;
import co.gov.ugpp.parafiscales.util.map.converter.EntidadNegocioToEntidadNegocioTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.EnvioActoAdministrativo2EnvioActoAdministrativoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.EventoEnvioToEventoEnvioTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ExpedienteTipo2ExpedienteConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionAccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionBusquedaToBusquedaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionHasActoToActoAdministrativoTipo;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionHasActoToAgrupacionFiscalizacionActoTipo;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionTipo2FiscalizacionConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FiscalizacionToIncumplimientoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoAreaToFormatoAreaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoEstructuraTipo2FormatoArchivoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoEstructuraToFormatoEstructuraTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoEstructuraToFormatoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoToFormatoEstructuraTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.FormatoToFormatoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.GestionAdministradoraAccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.GestionAdministradoraControlUbicacionToControlUbicacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.GestionAdministradoraDocumentoToDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.GestionAdministradoraTipo2GestionAdministradoraConverter;
import co.gov.ugpp.parafiscales.util.map.converter.GestionSolAdministradoraToGestionAdministradoraTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.HallazgoDetConContableToHallazgoDetConContableTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.HallazgoDetNominaToHallazgoDetNominaTipoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.HallazgoToHallazgoConciliacionContableTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.HallazgoToHallazgoNominaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.HojaCalculoToHojaCalculoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.IdentificacionTipo2IdentificacionConverter;
import co.gov.ugpp.parafiscales.util.map.converter.InformacionPruebaToDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.InformacionPruebaToInformacionPruebaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.InformacionRequeridaToInformacionRequeridaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.InvestigadoTipo2InvestigadoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.LiquidacionAccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.LiquidacionParcialToLiquidacionParcialTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.LiquidacionTipo2LiquidacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.MetadataDocAgrupadorToStringConverter;
import co.gov.ugpp.parafiscales.util.map.converter.MetadataDocumento2MetadataDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.MigracionToMigracionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.MunicipioTipo2MunicipioConverter;
import co.gov.ugpp.parafiscales.util.map.converter.NoAportanteTipo2PersonaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.NominaToNominaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.NotificacionTipo2NotificacionConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PagoTipo2SolicitudPagoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PersonaJuridicaTipo2PersonaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PersonaNaturalTipo2PersonaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PersonaTipo2PersonaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PersonaToIdentificacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PlanillaTipo2PlanillaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PlanillaTipo2SolicitudPlanillaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PruebaRequeridaToPruebaRequeridaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.PruebaTipo2PruebaConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RadicadoTrazabilidadToRadicadoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RecursoReconsideracionToRecursoReconsideracionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RecursoToAnexoReposicionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RecursoToAnexoRequisitoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RecursoToCausalFalloTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ReenvioComunicacionToReenvioComunicacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RespuestaAdminToRespuestaAdminTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RespuestaAdministradoraToDecisionCasoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RespuestaRequerimientoInformacionTipoToRespuestaRequerimientoInformacionConverter;
import co.gov.ugpp.parafiscales.util.map.converter.RespuestaRequerimientoInformacionToListadoChequeoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SancionHasAccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SancionToSancionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SeguimientoTipo2SeguimientoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SerieDocumental2SerieDocumentalTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudAdministradoraAccionToAccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudAdministradoraToCorreccionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudAdministradoraToSolicitudAdministradoraTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudArchivoAnexoToArchivoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudDocAnexoToStringConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudEntidadExternaToSolicitudEntidadExternaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudFuenteInformacionToStringConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudSeguimientoToSeguimientoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudToSolicitudInformacionTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudToSolicitudProgramaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SolicitudToSolicitudTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.String2BooleanConverter;
import co.gov.ugpp.parafiscales.util.map.converter.String2CorreoElectronicoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.String2LongConverter;
import co.gov.ugpp.parafiscales.util.map.converter.StringToValorDominioConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SubsistemaAdministradoraToSubsistemaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.SubsistemaToSubsistemaTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TelefonoTipo2TelefonoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TrasladoToTrasladoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TrazabilidadActoAdministrativoTipo2TrazabilidadActoAdministrativoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TrazabilidadEnvioActoToEnvioActoAdministrativoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TrazabilidadRadicacionActoTipo2TrazabilidadRadicacionActoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.TrazabilidadRadicadoSalida2AcuseEntregaDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.UVTToUVTTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.UbicacionTipo2UbicacionConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ValidacionCreacionDocumentoToValidacionCreacionDocumentoTipoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ValidacionDocumentoTipo2ValidacionDocumentoConverter;
import co.gov.ugpp.parafiscales.util.map.converter.ValidacionEstructuraToValidacionEstructuraTipoConverter;
import java.util.List;
import java.util.Set;
import javax.ejb.Singleton;
import javax.ejb.Startup;

/**
 *
 * @author rpadilla
 */
@Startup
@Singleton
public class MapperFacadeImpl implements MapperFacade {

    private final MapperFactory mapperFactory;

    public MapperFacadeImpl() {

        mapperFactory = new MapperFactory();

        ConverterFactory converterFactory = mapperFactory.getConverterFactory();

        // Converters programados
        converterFactory.registerConverter(new BigDecimal2LongConverter(mapperFactory));
        converterFactory.registerConverter(new BigInteger2IntegerConverter(mapperFactory));
        converterFactory.registerConverter(new BigInteger2LongConverter(mapperFactory));
        converterFactory.registerConverter(new String2LongConverter(mapperFactory));
        converterFactory.registerConverter(new String2BooleanConverter(mapperFactory));
        converterFactory.registerConverter(new StringToValorDominioConverter(mapperFactory));
        // Business
        converterFactory.registerConverter(new IdentificacionTipo2IdentificacionConverter(mapperFactory));
        converterFactory.registerConverter(new PersonaNaturalTipo2PersonaConverter(mapperFactory));
        converterFactory.registerConverter(new PersonaJuridicaTipo2PersonaConverter(mapperFactory));
        converterFactory.registerConverter(new CotizanteTipo2CotizanteConverter(mapperFactory));
        converterFactory.registerConverter(new UbicacionTipo2UbicacionConverter(mapperFactory));
        converterFactory.registerConverter(new TelefonoTipo2TelefonoConverter(mapperFactory));
        converterFactory.registerConverter(new String2CorreoElectronicoConverter(mapperFactory));
        converterFactory.registerConverter(new MunicipioTipo2MunicipioConverter(mapperFactory));
        converterFactory.registerConverter(new DepartamentoTipo2DepartamentoConverter(mapperFactory));
        converterFactory.registerConverter(new ContactoPersonaTipo2ContactoPersonaConverter(mapperFactory));
        converterFactory.registerConverter(new ExpedienteTipo2ExpedienteConverter(mapperFactory));
        converterFactory.registerConverter(new DocumentoTipo2DocumentoEcmConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudToSolicitudProgramaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SeguimientoTipo2SeguimientoConverter(mapperFactory));
        converterFactory.registerConverter(new CotizanteTipo2SolicitudCotizanteConverter(mapperFactory));
        converterFactory.registerConverter(new PlanillaTipo2SolicitudPlanillaConverter(mapperFactory));
        converterFactory.registerConverter(new PagoTipo2SolicitudPagoConverter(mapperFactory));
        converterFactory.registerConverter(new EntidadExternaTipo2EntidadExternaConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudToSolicitudInformacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AportanteTipo2AportanteConverter(mapperFactory));
        converterFactory.registerConverter(new PersonaToIdentificacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new InvestigadoTipo2InvestigadoConverter(mapperFactory));
        converterFactory.registerConverter(new SancionToSancionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ActoAdministrativoToActoAdministrativoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new InformacionPruebaToInformacionPruebaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ValidacionDocumentoTipo2ValidacionDocumentoConverter(mapperFactory));
        converterFactory.registerConverter(new NotificacionTipo2NotificacionConverter(mapperFactory));
        converterFactory.registerConverter(new RecursoReconsideracionToRecursoReconsideracionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudEntidadExternaToSolicitudEntidadExternaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new MetadataDocumento2MetadataDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SerieDocumental2SerieDocumentalTipoConverter(mapperFactory));
        converterFactory.registerConverter(new MetadataDocAgrupadorToStringConverter(mapperFactory));
        converterFactory.registerConverter(new Archivo2ArchivoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudDocAnexoToStringConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoEstructuraTipo2FormatoArchivoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudArchivoAnexoToArchivoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudFuenteInformacionToStringConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudToSolicitudTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoEstructuraToFormatoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoEstructuraToFormatoEstructuraTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoToFormatoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudSeguimientoToSeguimientoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoToFormatoEstructuraTipoConverter(mapperFactory));
        converterFactory.registerConverter(new HallazgoToHallazgoConciliacionContableTipoConverter(mapperFactory));
        converterFactory.registerConverter(new DocumentacionEsperada2DocumentacionEsperadaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ValidacionCreacionDocumentoToValidacionCreacionDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new TrasladoToTrasladoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ApoderadoTipo2ApoderadoConverter(mapperFactory));
        converterFactory.registerConverter(new PlanillaTipo2PlanillaConverter(mapperFactory));
        converterFactory.registerConverter(new DenuncianteTipo2DenuncianteConverter(mapperFactory));
        converterFactory.registerConverter(new DenunciaToDenunciaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionTipo2FiscalizacionConverter(mapperFactory));
        converterFactory.registerConverter(new Busqueda2BusquedaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AprobacionDocumentoTipo2AprobacionDocumentoConverter(mapperFactory));
        converterFactory.registerConverter(new AprobacionValidacionDocumentoToValidacionCreacionDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionBusquedaToBusquedaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionToIncumplimientoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionAccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new EnvioActoAdministrativo2EnvioActoAdministrativoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new TrazabilidadActoAdministrativoTipo2TrazabilidadActoAdministrativoConverter(mapperFactory));
        converterFactory.registerConverter(new HallazgoDetNominaToHallazgoDetNominaTipoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new HallazgoToHallazgoNominaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new HallazgoDetConContableToHallazgoDetConContableTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ControlLiquidadorToControlLiquidadorTipoConverter(mapperFactory));
        converterFactory.registerConverter(new LiquidacionTipo2LiquidacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new LiquidacionAccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AgrupaAfiliacionEntExtToAgrupaAfiliacionEntExtTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AgrupaEntExtPersonaToPersonaNaturalTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionHasActoToAgrupacionFiscalizacionActoTipo(mapperFactory));
        converterFactory.registerConverter(new FiscalizacionHasActoToActoAdministrativoTipo(mapperFactory));
        converterFactory.registerConverter(new NoAportanteTipo2PersonaConverter(mapperFactory));
        converterFactory.registerConverter(new PruebaTipo2PruebaConverter(mapperFactory));
        converterFactory.registerConverter(new RecursoToAnexoReposicionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new RecursoToAnexoRequisitoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new RecursoToCausalFalloTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ConciliacionContableToConciliacionContableTipoConverter(mapperFactory));
        converterFactory.registerConverter(new NominaToNominaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new HojaCalculoToHojaCalculoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new RespuestaRequerimientoInformacionTipoToRespuestaRequerimientoInformacionConverter(mapperFactory));
        converterFactory.registerConverter(new RespuestaRequerimientoInformacionToListadoChequeoConverter(mapperFactory));
        converterFactory.registerConverter(new PersonaTipo2PersonaConverter(mapperFactory));
        converterFactory.registerConverter(new InformacionPruebaToDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ConstanciaEjecutoriaToConstanciaEjecutoriaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new UVTToUVTTipoConverter(mapperFactory));
        converterFactory.registerConverter(new LiquidacionParcialToLiquidacionParcialTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SancionHasAccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AnalisisSancionToAnalisisSancionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AfectadoTipo2AfectadoConverter(mapperFactory));
        converterFactory.registerConverter(new InformacionRequeridaToInformacionRequeridaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new PruebaRequeridaToPruebaRequeridaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SubsistemaToSubsistemaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SubsistemaAdministradoraToSubsistemaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AgrupacionSubsistemaAdminToAgrupacionSubsistemaAdminTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AgrupacionAdministradoraToAgrupacionSubsistemaAdminTipoConverter(mapperFactory));
        converterFactory.registerConverter(new TrazabilidadRadicacionActoTipo2TrazabilidadRadicacionActoConverter(mapperFactory));
        converterFactory.registerConverter(new TrazabilidadEnvioActoToEnvioActoAdministrativoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new RadicadoTrazabilidadToRadicadoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new EntidadNegocioArchivoTemporalToStringConverter(mapperFactory));
        converterFactory.registerConverter(new EntidadNegocioToEntidadNegocioTipoConverter(mapperFactory));
        converterFactory.registerConverter(new DocumentoAnexoActoToStringConverter(mapperFactory));
        converterFactory.registerConverter(new ActoAdministrativoToAgrupacionAnexoActoConverter(mapperFactory));
        converterFactory.registerConverter(new GestionAdministradoraAccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new GestionAdministradoraControlUbicacionToControlUbicacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new GestionAdministradoraDocumentoToDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new GestionAdministradoraTipo2GestionAdministradoraConverter(mapperFactory));
        converterFactory.registerConverter(new RespuestaAdminToRespuestaAdminTipoConverter(mapperFactory));
        converterFactory.registerConverter(new FormatoAreaToFormatoAreaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new EventoEnvioToEventoEnvioTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ControlUbicacionToControlUbicacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ValidacionEstructuraToValidacionEstructuraTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudAdministradoraToSolicitudAdministradoraTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudAdministradoraAccionToAccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new SolicitudAdministradoraToCorreccionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new GestionSolAdministradoraToGestionAdministradoraTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AgrupacionSubsistemaSolAdminToAgrupacionSubsistemaTipoConverter(mapperFactory));
        converterFactory.registerConverter(new RespuestaAdministradoraToDecisionCasoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new CasoMigradoToCasoMigradoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new MigracionToMigracionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ConsDecRecursoToConsDecRecursoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ConsFecRecursoToConsFecRecursoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ControlEnvioComunicacionToControlEnvioComunicacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new TrazabilidadRadicadoSalida2AcuseEntregaDocumentoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new CorreoElectronicoTipo2CorreoElectronicoConverter(mapperFactory));
        converterFactory.registerConverter(new ReenvioComunicacionToReenvioComunicacionTipoConverter(mapperFactory));
        converterFactory.registerConverter(new ControlAprobacionActoToControlAprobacionActoTipoConverter(mapperFactory));
        converterFactory.registerConverter(new AprobacionMasivaActoToAprobacionMasivaActoTipoConverter(mapperFactory));
    }

    @Override
    public <S extends Object, D extends Object> D map(S sourceObject, Class<D> destClass) {
        return mapperFactory.getMapperFacade().map(sourceObject, destClass);
    }

    @Override
    public <S extends Object, D extends Object> D map(S srcObject, D destObj) {
        return mapperFactory.getMapperFacade().map(srcObject, destObj);
    }

    @Override
    public <S, D> List<D> map(List<S> srcList, Class<S> srcClass, Class<D> destClass) {
        if (srcList == null) {
            return null;
        }
        final List<D> resp;
        try {
            resp = (List<D>) srcList.getClass().newInstance();
        } catch (final IllegalAccessException exc) {
            throw new TechnicalException("Error al realizar el mapeo entre: " + srcClass.getSimpleName() + " y " + destClass.getSimpleName(), exc);
        } catch (final InstantiationException exc) {
            throw new TechnicalException("Error al realizar el mapeo entre: " + srcClass.getSimpleName() + " y " + destClass.getSimpleName(), exc);
        }
        for (final S src : srcList) {
            resp.add(mapperFactory.getMapperFacade().map(src, destClass));
        }
        return resp;
    }

    @Override
    public <S, D> Set<D> map(Set<S> source, Class<S> srcClass, Class<D> destClass) {
        if (source == null) {
            return null;
        }
        final Set<D> resp;
        try {
            resp = (Set<D>) source.getClass().newInstance();
        } catch (final IllegalAccessException exc) {
            throw new TechnicalException("Error al realizar el mapeo entre: " + srcClass.getSimpleName() + " y " + destClass.getSimpleName(), exc);
        } catch (final InstantiationException exc) {
            throw new TechnicalException("Error al realizar el mapeo entre: " + srcClass.getSimpleName() + " y " + destClass.getSimpleName(), exc);
        }
        for (final S src : source) {
            resp.add(mapperFactory.getMapperFacade().map(src, destClass));
        }
        return resp;
    }

    public MapperFactory getMapperFactory() {
        return mapperFactory;
    }

}
