package co.gov.ugpp.parafiscales.consumer;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceNominaPILAFallo;
import java.io.Serializable;

/**
 *
 * @author jmunocab
 */
public interface ProcLiquidadorFacade extends Serializable{
    
    void enviarHallazgosCruceNominaPila(final HallazgoNominaTipo hallazgoNominaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException, TypesMsjOpRecibirHallazgosCruceNominaPILAFallo;
    
    void enviarHallazgosCruceConciliacionContable(final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException, TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo;
}
