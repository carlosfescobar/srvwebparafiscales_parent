package co.gov.ugpp.parafiscales.consumer;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.retry.RetryFacade;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.OpRecibirHallazgosCruceConciliacionContableNominaSolTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.OpRecibirHallazgosCruceNominaPILASolTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAP;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceNominaPILAFallo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.VSSrvIntProcLiquidador;
import com.jcabi.aspects.RetryOnFailure;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.xml.ws.BindingProvider;

/**
 * Cliente que consume el servicio de integración SrvIntProcLiquidador
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ProcLiquidadorFacadeImpl implements ProcLiquidadorFacade {

    @EJB
    private RetryFacade retryFacade;

    /**
     * Método que consume el servicio de integración que recibe los hallazgos del curce de nómina y pila
     *
     * @param hallazgoNominaTipo el hallazgo encontrado con el listado de cotizantes asociados
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @param codProceso el procesos que se está reintentando ejecutar
     * @throws co.gov.ugpp.parafiscales.exception.AppException
     * @throws co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceNominaPILAFallo
     */
    @RetryOnFailure(attempts = 3, delay = 5, verbose = false)
    @Override
    public void enviarHallazgosCruceNominaPila(final HallazgoNominaTipo hallazgoNominaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException, TypesMsjOpRecibirHallazgosCruceNominaPILAFallo {

        retryFacade.createRetry(contextoTransaccionalTipo, codProceso);

        final VSSrvIntProcLiquidador vssipl = new VSSrvIntProcLiquidador();

        PortSrvIntProcLiquidadorSOAP port = vssipl.getVSSrvIntProcLiquidadorsoap12Http();

        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, contextoTransaccionalTipo.getIdUsuarioAplicacion());
        bindingProvider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

        PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput input
                = new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput();

        OpRecibirHallazgosCruceNominaPILASolTipo request = new OpRecibirHallazgosCruceNominaPILASolTipo();
        request.setContextoTransaccional(contextoTransaccionalTipo);
        request.setHallazgo(hallazgoNominaTipo);

        input.setOpRecibirHallazgosCruceNominaPILASol(request);

        PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput output
                = port.opRecibirHallazgosCruceNominaPILA(input);
        
        output.getOpRecibirHallazgosCruceNominaPILAResp().getContextoRespuesta();
    }

    @RetryOnFailure(attempts = 3, delay = 5, verbose = false)
    @Override
    public void enviarHallazgosCruceConciliacionContable(final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException, TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo {

        retryFacade.createRetry(contextoTransaccionalTipo, codProceso);

        final VSSrvIntProcLiquidador vssipl = new VSSrvIntProcLiquidador();

        PortSrvIntProcLiquidadorSOAP port = vssipl.getVSSrvIntProcLiquidadorsoap12Http();

        BindingProvider bindingProvider = (BindingProvider) port;
        bindingProvider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, contextoTransaccionalTipo.getIdUsuarioAplicacion());
        bindingProvider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

        PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput input
                = new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput();

        OpRecibirHallazgosCruceConciliacionContableNominaSolTipo request = new OpRecibirHallazgosCruceConciliacionContableNominaSolTipo();
        request.setContextoTransaccional(contextoTransaccionalTipo);
        request.setHallazgo(hallazgoConciliacionContableTipo);

        input.setOpRecibirHallazgosCruceConciliacionContableNominaSol(request);

        PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput output
                = port.opRecibirHallazgosCruceConciliacionContableNomina(input);
        
        output.getOpRecibirHallazgosCruceConciliacionContableNominaResp().getContextoRespuesta();
    }
}
