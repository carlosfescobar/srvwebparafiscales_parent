package co.gov.ugpp.parafiscales.exception;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import java.util.List;
import javax.ejb.ApplicationException;

/**
 *
 * @author rpadilla
 */
@ApplicationException(rollback = true)
public class AppException extends Exception {

    private List<ErrorTipo> errorTipoList;

    private final ErrorEnum errorEnum;
    
    /**
     * Constructs a new <code>AppException</code> exception with <code>null</code> as its detail message.
     *
     * @param message
     */
    public AppException(final String message) {
        this(message, ErrorEnum.ERROR_NO_ESPERADO);
    }

    public AppException(final String message, final ErrorEnum errorEnum) {
        super(message);
        this.errorEnum = errorEnum;
    }    

    /**
     * Constructs a new <code>AppException</code> exception with <code>null</code> as its detail message.
     *
     * @param message
     * @param throwable excepcion causal
     */
    public AppException(final String message, final Throwable throwable) {
        this(message, throwable, ErrorEnum.ERROR_NO_ESPERADO);
    }

    public AppException(final String message, final Throwable throwable, final ErrorEnum errorEnum) {
        super(message, throwable);
        this.errorEnum = errorEnum;
    }
    
    /**
     * Constructs a new <code>AppException</code> exception with <code>null</code> as its detail message.
     *
     * @param errorTipoList Lista de errores
     */
    public AppException(final List<ErrorTipo> errorTipoList) {
        this(errorTipoList, ErrorEnum.ERROR_NO_ESPERADO);
    }
    
    public AppException(final List<ErrorTipo> errorTipoList, final ErrorEnum errorEnum) {
        this.setErrorTipoList(errorTipoList);
        this.errorEnum = ErrorEnum.ERROR_NO_ESPERADO;
    }

    /**
     * Constructs a new <code>AppException</code> exception with <code>null</code> as its detail message.
     *
     * @param errorTipoList Lista de errores
     * @param throwable excepcion causal
     */
    public AppException(final List<ErrorTipo> errorTipoList, final Throwable throwable) {
        super(throwable);
        this.setErrorTipoList(errorTipoList);
        this.errorEnum = ErrorEnum.ERROR_NO_ESPERADO;
    }

    @Override
    public String getMessage() {
        if (this.getErrorTipoList() == null || this.getErrorTipoList().isEmpty()) {
            return super.getMessage();
        }
        final StringBuilder msg = new StringBuilder();
        final int n = errorTipoList.size();
        final int m = n - 1;
        for (int i = 0; i < m; i++) {
            msg.append(errorTipoList.get(i).getValDescErrorTecnico()).append("\n\n");
        }
        msg.append(errorTipoList.get(m).getValDescErrorTecnico());
        return msg.toString();
    }

    public ErrorEnum getErrorEnum() {
        return errorEnum;
    }

    private void setErrorTipoList(final List<ErrorTipo> errorTipoList) {
        if (errorTipoList == null || errorTipoList.isEmpty()) {
            throw new IllegalArgumentException("La lista de errores no puede ser vacia");
        }
        this.errorTipoList = errorTipoList;
    }
    
    public List<ErrorTipo> getErrorTipoList() {
        return errorTipoList;
    }

    public void addErrorTipo(ErrorTipo errorTipo) {
        this.getErrorTipoList().add(errorTipo);
    }

}
