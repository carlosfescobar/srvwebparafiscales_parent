package co.gov.ugpp.parafiscales.persistence;

import java.util.List;

/**
 *
 * @author rpadilla
 * @param <T>
 */
public class PagerData<T> {

    private final List<T> data;
    private final long numPages;
    
    public PagerData(final List<T> data, final long numPages) {
        this.data = data;
        this.numPages = numPages;
    }

    public long getNumPages() {
        return numPages;
    }

    public List<T> getData() {
        return data;
    }

}
