package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.enums.TipoEntradaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.entity.IEntity;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ParametroBD;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import javax.sql.DataSource;
import javax.annotation.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 * @paramValue <T> Clase del Entity
 * @paramValue <PK> Tipo del Primary Key
 */
public abstract class AbstractDao<T extends IEntity, PK extends Serializable> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractDao.class);

    protected final Class<T> entityClass;
    protected final String entityClassName;
    protected final String tableName;

    @PersistenceContext
    private EntityManager em;
    @Resource(mappedName = "jdbc/ugppWsAppDatasource")
    private DataSource dataSource;

    public AbstractDao(final Class<T> entityClass) {
        this.entityClass = entityClass;
        this.entityClassName = this.entityClass.getSimpleName();
        final Table annotation = this.entityClass.getAnnotation(Table.class);
        this.tableName = annotation == null ? this.entityClassName : annotation.name();
    }

    public EntityManager getEntityManager() {
        return em;
    }

    public PK create(final T entity) throws AppException {
        try {
            this.getEntityManager().persist(entity);
            return (PK) entity.getId();
        } catch (PersistenceException pe) {
            LOG.error("ERROR CREANDO ENTIDAD: " + entityClassName + ": " + pe.getMessage());
            throw new AppException("Excepcion de Persistencia al crear una entidad de tipo: " + entityClassName, pe);
        }
    }

    public PK createFlushing(final T entity) throws AppException {
        try {
            this.getEntityManager().persist(entity);
            this.getEntityManager().flush();
            return (PK) entity.getId();
        } catch (PersistenceException pe) {
            LOG.error("ERROR CREANDO ENTIDAD: " + entityClassName + ": " + pe.getMessage());
            throw new AppException("Excepcion de Persistencia al crear una entidad de tipo: " + entityClassName, pe);
        }
    }

    public void editFlushing(final T entity) throws AppException {
        try {
            this.getEntityManager().merge(entity);
            this.getEntityManager().flush();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia al editar una entidad de tipo: " + entityClassName, pe);
        }
    }

    public void edit(final T entity) throws AppException {
        try {
            this.getEntityManager().merge(entity);
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia al editar una entidad de tipo: " + entityClassName, pe);
        }
    }

    public void remove(final T entity) throws AppException {
        try {
            this.getEntityManager().remove(this.getEntityManager().merge(entity));
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia al borrar una entidad de tipo: " + entityClassName, pe);
        }
    }

    public T find(final PK id) throws AppException {
        try {
            return this.getEntityManager().find(entityClass, id);
        } catch (NoResultException e) {
            return null;
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia al buscar por ID una entidad de tipo: " + entityClassName, pe);
        }
    }

    public List<T> findAll() throws AppException {
        final CriteriaQuery cq = this.getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        try {
            return this.getEntityManager().createQuery(cq).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia al buscar todas las entidades de tipo: " + entityClassName, pe);
        }
    }

    public List<T> findByIdList(final List<PK> idList) throws AppException {
        if (idList == null || idList.isEmpty()) {
            return null;
        }
        final TypedQuery<T> query = this.getEntityManager().createQuery(
                "SELECT e FROM " + entityClassName + " e WHERE e.id IN :idList", entityClass);
        try {
            return query.setParameter("idList", idList).getResultList();
        } catch (PersistenceException ex) {
            throw new AppException("Excepcion de Persistencia al buscar por listado de ID entidades de tipo: "
                    + entityClassName, ex);
        }
    }

    /**
     * Permite consultar un listado de entidades por criterios dinamicos.
     *
     * @param criterios criterios de filtrado para la busqueda.
     * @param ordenamiento representa los criterios de ordenamiento por campos
     * @param numItems cantidad maxima de resultado a obtener.
     * @param numPagina representa el numero de pagina que se va a consultar.
     *
     * @return listado de entidades que cumplen con los criterios dados y
     * cantidad de paginas sin limite.
     * @throws co.gov.ugpp.parafiscales.exception.AppException
     */
    public PagerData<T> findPorCriterios(final List<ParametroTipo> criterios,
            final List<CriterioOrdenamientoTipo> ordenamiento,
            final Long numItems, final Long numPagina) throws AppException {

        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery criteriaQuery = builder.createQuery();
        final Root root = criteriaQuery.from(entityClass);
        criteriaQuery.distinct(true);
        criteriaQuery.select(root);

        if (criterios != null) {
            final List<Predicate> predicateList = new ArrayList<Predicate>();
            for (final ParametroTipo paramValueetro : criterios) {
                if (StringUtils.isNotBlank(paramValueetro.getIdLlave())) {
                    final String llave = paramValueetro.getIdLlave();
                    final Path attr = this.obtainAttr(root, llave);
                    final Class attrType = attr.getJavaType();
                    final Object attrVal;
                    if (attrType == Calendar.class) {
                        attrVal = DateUtil.parseStrDateTimeToCalendar(paramValueetro.getValValor());
                    } else {
                        attrVal = paramValueetro.getValValor();
                    }
                    final Predicate predicate = builder.equal(attr, attrVal);
                    predicateList.add(predicate);
                }
            }
            if (!predicateList.isEmpty()) {
                final Predicate[] predicates = new Predicate[predicateList.size()];
                predicateList.toArray(predicates);
                criteriaQuery.where(predicates);
            }
        }

        if (ordenamiento != null) {
            final List<Order> orderList = new ArrayList<Order>();
            for (final CriterioOrdenamientoTipo orden : ordenamiento) {
                if (StringUtils.isNotBlank(orden.getValNombreCampo())) {
                    final String llave = orden.getValNombreCampo();
                    final Path attr = this.obtainAttr(root, llave);
                    Order order;
                    if (orden.getValOrden() != null
                            && Constants.CRITERIO_ORDENAMIENTO_DESCENDENTE.equals(orden.getValOrden())) {
                        order = builder.desc(attr);
                    } else {
                        order = builder.asc(attr);
                    }
                    orderList.add(order);
                }
            }
            if (!orderList.isEmpty()) {
                criteriaQuery.orderBy(orderList);
            }
        }

        final TypedQuery<T> queryData = this.getEntityManager().createQuery(criteriaQuery);

        if (numItems != null && numItems > 0) {
            final int numIts = numItems.intValue();
            queryData.setMaxResults(numIts);
            if (numPagina != null && numPagina > 0) {
                final int numPag = numPagina.intValue();
                queryData.setFirstResult(numIts * numPag - numIts);
            }
        }

        final List<T> data;

        try {
            data = queryData.getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Error al buscar por criterios entidades de tipo: " + entityClassName, pe);
        }

        long cantidadPaginas = 1;

        if (data.size() > 0 && numItems != null) {
            // Solo se ejecuta la consulta del conteo de registros si el tamaño de la lista traida es igual al limit.
            if (data.size() == numItems) {
                criteriaQuery.select(builder.count(root));
                final TypedQuery<Long> queryCount = this.getEntityManager().createQuery(criteriaQuery);
                final long count;
                try {
                    count = queryCount.getSingleResult();
                } catch (PersistenceException pe) {
                    throw new AppException("Error al consultar la cantidad de registros de busqueda por criterios de"
                            + " una entidad tipo: " + entityClassName, pe);
                }
                final double div = (double) count / (double) numItems;
                final double mod = (double) count % (double) numItems;
                cantidadPaginas = (long) (mod == 0 ? div : div + 1);
            }
        }
        return new PagerData<T>(data, cantidadPaginas);
    }

    private Path obtainAttr(final Root root, final String llave) {
        Path attr;
        if (llave.contains(".")) {
            final String[] llaveArr = llave.split("\\.");
            attr = root.get(llaveArr[0]);
            for (int i = 1, n = llaveArr.length; i < n; ++i) {
                attr = attr.get(llaveArr[i]);
            }
        } else {
            attr = root.get(llave);
        }
        return attr;
    }

    /**
     * Método que se encarga de ejecutar procedimiento almacenado a la base de
     * datos.
     *
     * @param parametros el listado de parámetros para ejecutar el procedimiento
     * almacenado
     * @param spName nombre del procedimiento almacenado a ejecutar
     * @return el listado de objetos que devuelve el procedimiento almacenado
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> ejecutarStoredProcedure(final String spName,
            final ParametroBD[] parametros) throws AppException {

        Connection conn = null;
        CallableStatement cstmt = null;
        ResultSet resultSet = null;
        List<Object[]> salidaProcedimiento = new ArrayList<Object[]>();
        List<Integer> posicionesOUT = new ArrayList<Integer>();
        List<Object> objetosSalida = new ArrayList<Object>();

        try {
            conn = dataSource.getConnection();
            cstmt = conn.prepareCall(spName);
            int i = 0;
            while (i < parametros.length) {
                ParametroBD paramParametroBd = parametros[i];
                int k = ++i;
                if (paramParametroBd != null) {
                    Object paramValue = paramParametroBd.getValorEntrada();
                    if (paramParametroBd.getTipoEntrada().equals(TipoEntradaEnum.IN)) {

                        if (paramValue instanceof String) {
                            cstmt.setString(k, (String) paramValue);
                        } else if (paramValue instanceof Clob) {
                            cstmt.setClob(k, (Clob) paramValue);
                        } else if (paramValue instanceof Long) {
                            cstmt.setLong(k, (Long) paramValue);
                        } else if (paramValue instanceof Integer) {
                            cstmt.setInt(k, (Integer) paramValue);
                        } else if (paramValue instanceof BigDecimal) {
                            cstmt.setBigDecimal(k, (BigDecimal) paramValue);
                        } else if (paramValue instanceof Boolean) {
                            cstmt.setBoolean(k, (Boolean) paramValue);
                        } else if (paramValue instanceof java.sql.Date) {
                            cstmt.setDate(k, (java.sql.Date) paramValue);
                        } else if (paramValue instanceof Timestamp) {
                            cstmt.setTimestamp(k, (Timestamp) paramValue);
                        } else if (paramValue instanceof java.util.Date) {
                            java.sql.Date fecha = new java.sql.Date(((java.util.Date) paramValue)
                                    .getTime());
                            cstmt.setDate(k, fecha);
                        } else if (paramValue instanceof Double) {
                            cstmt.setDouble(k, (Double) paramValue);
                        } else if (paramValue instanceof Float) {
                            cstmt.setFloat(k, (Float) paramValue);
                        } else if (paramValue instanceof Short) {
                            cstmt.setShort(k, (Short) paramValue);
                        }
                    } else if (paramParametroBd.getTipoEntrada().equals(TipoEntradaEnum.OUT)) {
                        if (paramValue instanceof String) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.VARCHAR);
                        } else if (paramValue instanceof Long) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.NUMERIC, 10);
                        } else if (paramValue instanceof Integer) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.NUMERIC, 5);
                        } else if (paramValue instanceof BigDecimal) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.NUMERIC, 15);
                        } else if (paramValue instanceof Boolean) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.BOOLEAN);
                        } else if (paramValue instanceof Timestamp) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.TIMESTAMP);
                        } else if (paramValue instanceof Date) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.TIMESTAMP);
                        } else if (paramValue instanceof Double) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.NUMERIC, 13);
                        } else if (paramValue instanceof Float) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.FLOAT);
                        } else if (paramValue instanceof Short) {
                            posicionesOUT.add(k);
                            cstmt.registerOutParameter(k, Types.NUMERIC, 3);
                        }
                    }
                } else {
                    cstmt.setObject(k, null, java.sql.Types.NULL);
                }
            }
            cstmt.executeQuery();
            for (Integer posicionOut : posicionesOUT) {
                Object objetoSalida = cstmt.getObject(posicionOut);
                objetosSalida.add(objetoSalida);
            }

            salidaProcedimiento.add(objetosSalida.toArray());
            return salidaProcedimiento;

        } catch (SQLException e) {
            throw new AppException("Error ejecutando procedimento almacenado", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                throw new AppException("Error ejecutando procedimento almacenado", e);
            }
        }
    }

    /**
     * Método que ejecuta un insert en batch
     *
     * @param sqlString el string que contiene la sentencia SQL del insert
     * @param listaParametros el listado de parámetros que acompañan el insert
     * @throws SQLException si ocurre un error en base de datos
     */
    public void ejecutarBatchQuery(String sqlString, List<Object[]> listaParametros) throws SQLException {

        if (StringUtils.isNotBlank(sqlString)) {
            try {
                dataSource.getConnection().setAutoCommit(false);
                PreparedStatement statement = dataSource.getConnection().prepareStatement(sqlString);
                for (Object[] parametro : listaParametros) {
                    int posicionParametro = 1;
                    for (Object parametros : parametro) {
                        if (parametros instanceof java.util.Calendar) {

                            statement.setTimestamp(posicionParametro++, new Timestamp(((java.util.Date) parametros).getTime()));
                        } else if (parametros instanceof java.util.Date) {
                            statement.setTimestamp(posicionParametro++, new Timestamp(((java.util.Date) parametros).getTime()));
                        } else if (parametros instanceof Integer) {
                            statement.setInt(posicionParametro++, (Integer) parametros);
                        } else if (parametros instanceof Long) {
                            statement.setLong(posicionParametro++, (Long) parametros);
                        } else if (parametros instanceof Double) {
                            statement.setDouble(posicionParametro++, (Double) parametros);
                        } else if (parametros instanceof Float) {
                            statement.setFloat(posicionParametro++, (Float) parametros);
                        } else if (parametros instanceof BigDecimal) {
                            statement.setBigDecimal(posicionParametro++, (BigDecimal) parametros);
                        } else if (parametros instanceof Character) {
                            statement.setString(posicionParametro++, String.valueOf(parametros));
                        } else if (parametros instanceof Boolean) {
                            statement.setBoolean(posicionParametro++, Boolean.valueOf(parametros.toString()));
                        } else {
                            statement.setString(posicionParametro++, parametros == null ? null : (String) parametros);
                        }
                    }
                    statement.addBatch();
                }
                LOG.info("STATEMENT: " + sqlString);
                statement.executeBatch();
                dataSource.getConnection().commit();
            } catch (SQLException ex) {
                dataSource.getConnection().rollback();
                LOG.error("ERROR EJECUTANDO SENTENCIA EN BATCH: " + ex.getMessage());
                throw ex;
            }
        }
    }

    /**
     * Método que permite buscar resultados basados en JPQLQueries tipados con
     * parámetros
     *
     * @param jpqlQuery Query en JPQL
     * @param clase Class del tipo de retorno esperado en el Query o null
     * @param params Hashtable de parámetros
     * @return una lista tipada de resultados, si el tipo provisto en el método
     * es null retorna
     * <p>
     * List<Object></p>
     */
    public List<T> findByJPQLQuery(String jpqlQuery, Class<T> clase, Hashtable<String, Object> params) {
        TypedQuery<T> q = getEntityManager().createQuery(jpqlQuery, entityClass);

        Enumeration<String> enu = params.keys();

        while (enu.hasMoreElements()) {
            String key = enu.nextElement();
            q.setParameter(key, params.get(key));
        }

        return q.getResultList();
    }

    /**
     * * Método que permite buscar resultados basados en JPQLQueries con
     * parámetros
     *
     * @param jpqlQuery Query en JPQL
     * @param params Hashtable de parámetros
     * @return una lista de resultados
     * <p>
     * List<Object></p>
     */
    public List findByJPQLQuery(String jpqlQuery, Hashtable<String, Object> params) {
        javax.persistence.Query q = getEntityManager().createQuery(jpqlQuery);

        Enumeration<String> enu = params.keys();

        while (enu.hasMoreElements()) {
            String key = enu.nextElement();
            q.setParameter(key, params.get(key));
        }

        return q.getResultList();
    }

    /**
     * Método que permite buscar resultados basados en JPQLQueries tipados
     *
     * @param jpqlQuery Query en JPQL
     * @param clase Class del tipo de retorno esperado en el Query o null
     * @return una lista tipada de resultados, si el tipo provisto en el método
     * es null retorna
     * <p>
     * List<Object></p>
     */
    public List<T> findByJPQLQuery(String jpqlQuery, Class<T> clase) {
        TypedQuery<T> q = getEntityManager().createQuery(jpqlQuery, entityClass);

        return q.getResultList();
    }

    /**
     * Método que permite buscar resultados basados en JPQLQueries
     *
     * @param jpqlQuery Query en JPQL
     * @return una lista de resultados
     * <p>
     * List<Object></p>
     */
    public List findByJPQLQuery(String jpqlQuery) {
        javax.persistence.Query q = getEntityManager().createQuery(jpqlQuery);

        return q.getResultList();
    }
}
