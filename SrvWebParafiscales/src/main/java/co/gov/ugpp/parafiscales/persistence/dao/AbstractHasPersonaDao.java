package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AbstractEntity;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import org.apache.commons.lang3.StringUtils;

/**
 * DAO base para las entidades que son (tienen) una persona, tales como:
 * Cotizante, NoAportante, etc.
 *
 * @author rpadilla
 * @param <T> Clase del Entity
 * @param <PK> Tipo del primary key
 */
public abstract class AbstractHasPersonaDao<T extends AbstractEntity, PK extends Serializable>
        extends AbstractDao<T, PK> {

    public AbstractHasPersonaDao(Class<T> entityClass) {
        super(entityClass);
    }

    public T findByIdentificacion(final Identificacion identificacion) throws AppException {
        final TypedQuery<T> query = this.getEntityManager().createQuery(
                "SELECT e FROM " + this.entityClassName + " e"
                + " WHERE e.persona.valNumeroIdentificacion = :numero"
                + " AND e.persona.codTipoIdentificacion.id = :tipo", entityClass);
        query.setParameter("numero", identificacion.getValNumeroIdentificacion());
        query.setParameter("tipo", identificacion.getCodTipoIdentificacion());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }
    
    public T findByIdentificacion(final Identificacion identificacion, final ClasificacionPersonaEnum clasificacionPersonaEnum) throws AppException {
        final TypedQuery<T> query = this.getEntityManager().createQuery(
                "SELECT e FROM " + this.entityClassName + " e"
                + " LEFT JOIN clasificacionesPersona cf "
                + " WHERE e.persona.valNumeroIdentificacion = :numero"
                + " AND e.persona.codTipoIdentificacion.id = :tipo"
                + " AND cf.codClasificacionPersona.id = :clasificacionPersona", entityClass);
        query.setParameter("numero", identificacion.getValNumeroIdentificacion());
        query.setParameter("tipo", identificacion.getCodTipoIdentificacion());
        query.setParameter("clasificacionPersona", clasificacionPersonaEnum.getCode());
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }
    
    
       /**
     * Método que busca entre la entidad y la tabla persona a través de un LEFT
     * join
     *
     * @param identificacion la identificacion
     * @return la sentencia SQL a ejecutar
     */
    private StringBuilder buildBaseQueryFindByIdentificacionLeftJoin(final Identificacion identificacion) {
        final StringBuilder query = new StringBuilder("SELECT * FROM PERSONA per")
                .append(" LEFT JOIN ").append(tableName).append(" e ON per.id = e.id")
                .append(" WHERE per.val_numero_identificacion = '").append(identificacion.getValNumeroIdentificacion()).append("'")
                .append(" AND per.cod_tipo_identificacion = '").append(identificacion.getCodTipoIdentificacion()).append("'");
        return query;
    }
    

    private StringBuilder buildBaseQueryFindByIdentificacionList(final List<Identificacion> identificacionList) {
        final List<String> identificaciones = new ArrayList<String>(identificacionList.size());
        for (int i = 0, n = identificacionList.size(); i < n; i++) {
            final Identificacion id = identificacionList.get(i);
            identificaciones.add(new StringBuilder("('").append(id.getValNumeroIdentificacion())
                    .append("', '").append(id.getCodTipoIdentificacion()).append("')").toString());
        }
        final String in = StringUtils.join(identificaciones, ", ");
        final StringBuilder query = new StringBuilder("SELECT * FROM ").append(tableName).append(" e")
                .append(" JOIN Persona per ON per.id = e.id")
                .append(" WHERE (per.val_numero_identificacion, per.cod_tipo_identificacion) IN (").append(in).append(")");
        return query;
    }

    /**
     * Método que busca entre la entidad y la tabla persona a través de un LEFT
     * join
     *
     * @param identificacionList el listado de identificaciones
     * @return la sentencia SQL a ejecutar
     */
    private StringBuilder buildBaseQueryFindByIdentificacionListLeftJoin(final List<Identificacion> identificacionList) {
        final List<String> identificaciones = new ArrayList<String>(identificacionList.size());
        for (int i = 0, n = identificacionList.size(); i < n; i++) {
            final Identificacion id = identificacionList.get(i);
            identificaciones.add(new StringBuilder("('").append(id.getValNumeroIdentificacion())
                    .append("', '").append(id.getCodTipoIdentificacion()).append("')").toString());
        }
        final String in = StringUtils.join(identificaciones, ", ");
        final StringBuilder query = new StringBuilder("SELECT * FROM PERSONA per")
                .append(" LEFT JOIN ").append(tableName).append(" e ON per.id = e.id")
                .append(" WHERE (per.val_numero_identificacion, per.cod_tipo_identificacion) IN (").append(in).append(")");
        return query;
    }

    private StringBuilder buildBaseQueryFindByIdentificacionList(final List<Identificacion> identificacionList, final ClasificacionPersonaEnum clasificacionPersonaEnum) {
        final List<String> identificaciones = new ArrayList<String>(identificacionList.size());
        for (int i = 0, n = identificacionList.size(); i < n; i++) {
            final Identificacion id = identificacionList.get(i);
            identificaciones.add(new StringBuilder("('").append(id.getValNumeroIdentificacion())
                    .append("', '").append(id.getCodTipoIdentificacion()).append("')").toString());
        }
        final String in = StringUtils.join(identificaciones, ", ");
        final StringBuilder query = new StringBuilder("SELECT * FROM ").append(tableName).append(" e")
                .append(" JOIN Persona per ON per.id = e.id")
                .append(" LEFT JOIN Clasificacion_Persona cla ON per.id = cla.id_persona")
                .append(" AND cla.cod_clasificacion_persona = NVL(").append(clasificacionPersonaEnum == null ? null : clasificacionPersonaEnum.getCode())
                .append(", cla.cod_clasificacion_persona)")
                .append(" WHERE (per.val_numero_identificacion, per.cod_tipo_identificacion) IN (").append(in).append(")");
        return query;
    }

    public List<T> findByIdentificacionList(final List<Identificacion> identificacionList, final ClasificacionPersonaEnum clasificacionPersonaEnum) throws AppException {
        if (identificacionList == null || identificacionList.isEmpty()) {
            return null;
        }
        final String query = this.buildBaseQueryFindByIdentificacionList(identificacionList, clasificacionPersonaEnum).toString();
        try {
            return this.getEntityManager().createNativeQuery(query, entityClass).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }

    public List<T> findByIdentificacionList(final List<Identificacion> identificacionList) throws AppException {
        if (identificacionList == null || identificacionList.isEmpty()) {
            return null;
        }
        final String query = this.buildBaseQueryFindByIdentificacionList(identificacionList).toString();
        try {
            return this.getEntityManager().createNativeQuery(query, entityClass).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }

    public List<T> findByIdentificacionListLeftJoin(final List<Identificacion> identificacionList) throws AppException {
        if (identificacionList == null || identificacionList.isEmpty()) {
            return null;
        }
        final String query = this.buildBaseQueryFindByIdentificacionListLeftJoin(identificacionList).toString();
        try {
            return this.getEntityManager().createNativeQuery(query, entityClass).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }

    public T findByIdentificacionLeftJoin(final Identificacion identificacion) throws AppException {
        if (identificacion == null) {
            return null;
        }
        final String query = this.buildBaseQueryFindByIdentificacionLeftJoin(identificacion).toString();
        try {
            return (T) this.getEntityManager().createNativeQuery(query, entityClass).getSingleResult();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }

    public List<T> findByIdentificacionList(final List<Identificacion> identificacionList,
            final String naturalezaPersona) throws AppException {
        if (identificacionList == null || identificacionList.isEmpty()) {
            return null;
        }
        final StringBuilder query = this.buildBaseQueryFindByIdentificacionList(identificacionList);
        query.append("AND per.cod_naturaleza_persona = ").append(naturalezaPersona);
        final String strQuery = query.toString();
        try {
            return this.getEntityManager().createNativeQuery(strQuery, entityClass).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }

    private StringBuilder buildBaseQueryFindByIdentificacionPersonas(final List<Identificacion> identificacionList, final ClasificacionPersonaEnum clasificacionPersonaEnum) {
        final List<String> identificaciones = new ArrayList<String>(identificacionList.size());

        for (int i = 0, n = identificacionList.size(); i < n; i++) {
            final Identificacion id = identificacionList.get(i);
            identificaciones.add(new StringBuilder("('").append(id.getValNumeroIdentificacion())
                    .append("', '").append(id.getCodTipoIdentificacion()).append("')").toString());
        }
        final String in = StringUtils.join(identificaciones, ", ");
        final StringBuilder query = new StringBuilder("SELECT  DISTINCT per.* FROM  Persona per")
                .append(" INNER JOIN Clasificacion_Persona cla ON per.id = cla.id_persona")
                .append(" AND cla.cod_clasificacion_persona = ").append(clasificacionPersonaEnum.getCode())
                .append(" WHERE (per.val_numero_identificacion, per.cod_tipo_identificacion) IN (").append(in).append(")");
        return query;
    }

    public List<T> findByIdentificacionPersonasList(final List<Identificacion> identificacionList,
            final ClasificacionPersonaEnum clasificacionPersonaEnum) throws AppException {
        if (identificacionList == null || identificacionList.isEmpty()) {
            return null;
        }
        final StringBuilder query = this.buildBaseQueryFindByIdentificacionPersonas(identificacionList, clasificacionPersonaEnum);
        final String strQuery = query.toString();
        try {
            return this.getEntityManager().createNativeQuery(strQuery, entityClass).getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Excepcion de Persistencia", pe);
        }
    }
}
