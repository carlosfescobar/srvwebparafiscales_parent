package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AccionDao extends AbstractDao<Accion, Long> {

    public AccionDao() {
        super(Accion.class);
    }
    
}
