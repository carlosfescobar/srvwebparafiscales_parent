package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ActoAdministrativoDao extends AbstractDao<ActoAdministrativo, String> {

    public ActoAdministrativoDao() {
        super(ActoAdministrativo.class);
    }

}
