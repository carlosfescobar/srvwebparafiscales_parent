package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Afectado;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AfectadoDao extends AbstractHasPersonaDao<Afectado, Long> {
    
    public AfectadoDao() {
        super(Afectado.class);
    }

}
