package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class AgrupacionAdministradoraDao extends AbstractDao<AgrupacionAdministradora, Long> {

    public AgrupacionAdministradoraDao() {
        super(AgrupacionAdministradora.class);
    }

    public AgrupacionAdministradora findByAgrupacionAdministradoraAdminAndEntidadexterna(final EntidadExterna entidadExterna, final AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin) throws AppException {
        final TypedQuery<AgrupacionAdministradora> query = getEntityManager().createNamedQuery("agrupacionAdministradora.findByAgrupacionSubsistemaAdminAndEntidadexterna", AgrupacionAdministradora.class);
        query.setParameter("idAgrupacionSubsistemaAdmin", agrupacionSubsistemaAdmin.getId());
        query.setParameter("idEntidadExterna", entidadExterna.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
