package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AgrupacionAfiliacionEntidadExternaDao extends AbstractDao<AgrupacionAfiliacionesPorEntidad, Long> {

    public AgrupacionAfiliacionEntidadExternaDao() {
        super(AgrupacionAfiliacionesPorEntidad.class);
    }

}
