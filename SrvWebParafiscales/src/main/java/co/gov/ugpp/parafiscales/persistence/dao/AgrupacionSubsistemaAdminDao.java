package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import javax.ejb.Stateless;

/**
 *
 * @author Everis
 */
@Stateless
public class AgrupacionSubsistemaAdminDao extends AbstractDao<AgrupacionSubsistemaAdmin, Long> {

    public AgrupacionSubsistemaAdminDao() {
        super(AgrupacionSubsistemaAdmin.class);
    }
}
