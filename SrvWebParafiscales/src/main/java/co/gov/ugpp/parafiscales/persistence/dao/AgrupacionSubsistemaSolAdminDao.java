package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaSolAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradora;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Everis
 */
@Stateless
public class AgrupacionSubsistemaSolAdminDao extends AbstractDao<AgrupacionSubsistemaSolAdmin, Long> {

    public AgrupacionSubsistemaSolAdminDao() {
        super(AgrupacionSubsistemaSolAdmin.class);
    }
    
      public AgrupacionSubsistemaSolAdmin findByAgrupacionSubsistemaAndSolicitudAdmin(final AgrupacionSubsistemaAdmin agrupacionSubsistema, final SolicitudAdministradora solicitudAdministradora) throws AppException {
        final TypedQuery<AgrupacionSubsistemaSolAdmin> query = getEntityManager().createNamedQuery("agrupacionSubsitemaSolAdmin.findByAgrupacionSubsistemaAndSolicitudAdmin", AgrupacionSubsistemaSolAdmin.class);
        query.setParameter("idSolicitudAdministradora", solicitudAdministradora.getId());
        query.setParameter("idAdministradora", agrupacionSubsistema.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
