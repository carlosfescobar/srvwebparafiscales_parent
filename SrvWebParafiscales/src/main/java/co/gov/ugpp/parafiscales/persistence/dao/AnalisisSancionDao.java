package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AnalisisSancion;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AnalisisSancionDao extends AbstractDao<AnalisisSancion, Long> {

    public AnalisisSancionDao() {
        super(AnalisisSancion.class);
    }
    
}
