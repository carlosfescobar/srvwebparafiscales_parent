package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativoDocumentoAnexo;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AnexoActoAdministrativoDao extends AbstractHasPersonaDao<ActoAdministrativoDocumentoAnexo, Long> {

    public AnexoActoAdministrativoDao() {
        super(ActoAdministrativoDocumentoAnexo.class);
    }

    public ActoAdministrativoDocumentoAnexo findByActoAndDocumento(final String idActoAdministrativo, final String idDocumentoAnexo) throws AppException {
        Query query = getEntityManager().createNamedQuery("actoAdministrativoDocumentoAnexo.findByActoAndDocumento");
        query.setParameter("idActoAdministrativo", idActoAdministrativo);
        query.setParameter("idDocumentoAnexo", idDocumentoAnexo);
        try {
            return (ActoAdministrativoDocumentoAnexo) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
