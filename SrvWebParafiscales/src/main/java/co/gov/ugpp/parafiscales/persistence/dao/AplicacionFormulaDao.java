package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AplicacionFormula;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

/**
 *
 * @author franzjr
 */
@Stateless
public class AplicacionFormulaDao extends AbstractDao<AplicacionFormula, Long> {

    public AplicacionFormulaDao() {
        super(AplicacionFormula.class);
    }

    public List<AplicacionFormula> findByActivaVigente(final String tipoUso, Date fecha) throws AppException {
        final TypedQuery<AplicacionFormula> query = getEntityManager().createNamedQuery("AplicacionFormula.findByActivaVigente", AplicacionFormula.class);
        query.setParameter("tipoUso", tipoUso);
        query.setParameter("fecha", fecha, TemporalType.TIMESTAMP);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
