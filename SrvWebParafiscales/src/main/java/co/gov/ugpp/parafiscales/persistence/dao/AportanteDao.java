package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AportanteDao extends AbstractHasPersonaDao<Aportante, Long> {

    public AportanteDao() {
        super(Aportante.class);
    }
    
}
