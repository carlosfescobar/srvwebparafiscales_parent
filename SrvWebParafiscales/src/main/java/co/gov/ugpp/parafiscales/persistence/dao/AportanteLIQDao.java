package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AportanteLIQ;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 * 
 * @author franzjr
 */
@Stateless
public class AportanteLIQDao extends AbstractDao<AportanteLIQ, Long> {

    public AportanteLIQDao() {
        super(AportanteLIQ.class);
    }
    
    public AportanteLIQ findByIdentificacion(final String numeroIdentificacion, String sigla) throws AppException {
        final TypedQuery<AportanteLIQ> query = getEntityManager().createNamedQuery("Aportante.findByIdentificacion", AportanteLIQ.class);
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", sigla);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia findByIdentificacion", e);
        }
    }

    
    public Long findByIdentificacionAndTipo(final String numeroIdentificacion, Long tipoIdentificacion) throws AppException {
        
        final TypedQuery<Long> query = getEntityManager().createNamedQuery("Aportante.findByIdentificacionAndTipo", Long.class);
        
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", tipoIdentificacion);

        try {
            return Long.parseLong(query.getSingleResult().toString());
        } catch (NoResultException nre) {
            System.out.println("Excepción de resultado. " + nre);
            return 0L;
        } catch (PersistenceException e) {
            System.out.println("Excepción de Persistencia findByIdentificacionAndTipo" + e);
            return 0L;
        }
    }
    
    
    
}
