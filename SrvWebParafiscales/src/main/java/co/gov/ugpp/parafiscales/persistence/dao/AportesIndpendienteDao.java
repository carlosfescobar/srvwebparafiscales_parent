package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AportesIndependiente;
import javax.ejb.Stateless;

/**
 *
 * @author franzjr
 */
@Stateless
public class AportesIndpendienteDao extends AbstractDao<AportesIndependiente, Long> {

    public AportesIndpendienteDao() {
        super(AportesIndependiente.class);
    }


}
