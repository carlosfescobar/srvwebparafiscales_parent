package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AprobacionDocumentoDao extends AbstractDao<AprobacionDocumento, Long> {

    public AprobacionDocumentoDao() {
        super(AprobacionDocumento.class);
    }

    public AprobacionDocumento findByArchivoAndValidacionCreacionDocumento(final Long idArchivo, final Long idValidacionCreacionDocumento) throws AppException {
        Query query = getEntityManager().createNamedQuery("aprobacionDocumento.findByArchivoAndValidacion");
        query.setParameter("idArchivo", idArchivo);
        query.setParameter("idValidacionDocumento", idValidacionCreacionDocumento);

        try {
            return (AprobacionDocumento) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
