package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AprobacionMasivaActo;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class AprobacionMasivaActoDao extends AbstractDao<AprobacionMasivaActo, Long> {

    public AprobacionMasivaActoDao() {
        super(AprobacionMasivaActo.class);
    }
}
