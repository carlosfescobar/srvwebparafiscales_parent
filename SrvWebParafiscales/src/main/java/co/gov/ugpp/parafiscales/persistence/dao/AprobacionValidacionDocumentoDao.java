package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.AprobacionValidacionDocumento;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class AprobacionValidacionDocumentoDao extends AbstractDao<AprobacionValidacionDocumento, Long> {

    public AprobacionValidacionDocumentoDao() {
        super(AprobacionValidacionDocumento.class);
    }

}
