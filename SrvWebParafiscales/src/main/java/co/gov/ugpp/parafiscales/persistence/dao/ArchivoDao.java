package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ArchivoDao extends AbstractDao<Archivo, Long> {

    public ArchivoDao() {
        super(Archivo.class);
    }
}
