package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.exception.TechnicalException;
import co.gov.ugpp.parafiscales.persistence.entity.Auditoria;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author franzjr
 * @author rpadilla
 */
@Stateless
public class AuditoriaDaoImpl extends AbstractDao<Auditoria, Long> {

    public AuditoriaDaoImpl() {
        super(Auditoria.class);
    }

    public Auditoria findByIdTrx(final String idTx) throws AppException {

        final TypedQuery<Auditoria> query = this.getEntityManager().createNamedQuery("Auditoria.findByIdTrx", Auditoria.class);
        query.setParameter("idTrx", idTx);
        query.setMaxResults(1);

        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            throw new AppException("No se pudo obtener el registro de auditoria de la transaccion", ex);
        } catch (PersistenceException ex) {
            throw new AppException("Error de persistencia al consultar el registro de auditoria", ex);
        }
    }

}
