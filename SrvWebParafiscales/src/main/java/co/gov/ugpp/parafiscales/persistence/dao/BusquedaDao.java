package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Busqueda;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class BusquedaDao extends AbstractDao<Busqueda, Long> {

    public BusquedaDao() {
        super(Busqueda.class);
    }
    
}
