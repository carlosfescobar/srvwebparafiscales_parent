package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ClaseCiiu;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ClaseCiiuDao extends AbstractDao<ClaseCiiu, Long> {

    public ClaseCiiuDao() {
        super(ClaseCiiu.class);
    }
}
