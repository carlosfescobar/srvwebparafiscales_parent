package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContable;
import java.math.BigInteger;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author franzjr
 */
@Stateless
public class ConceptoContableDao extends AbstractDao<ConceptoContable, Long> {

    public ConceptoContableDao() {
        super(ConceptoContable.class);
    }

    public List<ConceptoContable> findByCAI(final String ncc, Long idN, BigInteger ano) throws AppException {
        final TypedQuery<ConceptoContable> query = getEntityManager().createNamedQuery("ConceptoContable.findByCAI", ConceptoContable.class);
        query.setParameter("ncc", ncc);
        query.setParameter("idN", idN);
        query.setParameter("ano", ano);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
