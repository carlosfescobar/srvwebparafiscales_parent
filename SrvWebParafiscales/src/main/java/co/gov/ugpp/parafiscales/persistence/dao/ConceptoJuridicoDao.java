package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridico;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ConceptoJuridicoDao extends AbstractDao<ConceptoJuridico, Long> {

    public ConceptoJuridicoDao() {
        super(ConceptoJuridico.class);
    }

    public List<ConceptoJuridico> findByExpediente(String id) {
        
        Query query = getEntityManager().createNamedQuery("ConceptoJuridico.findByExpediente");
        query.setParameter("expedienteNumero", id);
        
        return (List<ConceptoJuridico>) query.getResultList();
    }

}
