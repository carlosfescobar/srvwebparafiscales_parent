package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJurHasDescObservac;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ConceptoJuridicoObsDao extends AbstractDao<ConceptoJurHasDescObservac, Long> {

    public ConceptoJuridicoObsDao() {
        super(ConceptoJurHasDescObservac.class);
    }

}
