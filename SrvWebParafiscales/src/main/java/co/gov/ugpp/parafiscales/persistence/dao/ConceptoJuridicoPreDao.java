package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridHasDescPregun;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ConceptoJuridicoPreDao extends AbstractDao<ConceptoJuridHasDescPregun, Long> {

    public ConceptoJuridicoPreDao() {
        super(ConceptoJuridHasDescPregun.class);
    }
}
