package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable;
import javax.ejb.Stateless;

/**
 *
 * @author franzjr
 */
@Stateless
public class ConciliacionContableDao extends AbstractDao<ConciliacionContable, Long> {

    public ConciliacionContableDao() {
        super(ConciliacionContable.class);
    }


}
