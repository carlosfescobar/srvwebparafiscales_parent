package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContableDetalle;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author franzjr
 */
@Stateless
public class ConciliacionContableDetalleDao extends AbstractDao<ConciliacionContableDetalle, Long> {

    public ConciliacionContableDetalleDao() {
        super(ConciliacionContableDetalle.class);
    }

    public void ejecutarUpdate(String query, Long id) throws AppException {
        try {
            Query act = getEntityManager().createQuery(query).setParameter("id", id);
            act.executeUpdate();
        } catch (Exception e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
