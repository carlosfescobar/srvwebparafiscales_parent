package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaEjecutoria;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ConstanciaEjecutoriaDao extends AbstractDao<ConstanciaEjecutoria, Long> {

    public ConstanciaEjecutoriaDao() {
        super(ConstanciaEjecutoria.class);
    }
}
