package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ContactoDao extends AbstractDao<ContactoPersona, Long> {

    public ContactoDao() {
        super(ContactoPersona.class);
    }
    
}
