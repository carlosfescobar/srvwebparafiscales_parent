package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Contratante;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author franzjr
 */
@Stateless
public class ContratanteDao extends AbstractDao<Contratante, Long> {

    public ContratanteDao() {
        super(Contratante.class);
    }

    public Contratante findByIdentificacion(final String numeroIdentificacion, String sigla) throws AppException {
        final TypedQuery<Contratante> query = getEntityManager().createNamedQuery("Contratante.findByIdentificacion", Contratante.class);
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", sigla);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
