package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.entity.ControlAprobacionActo;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author yrinconh
 */
@Stateless
public class ControlAprobacionActoDao extends AbstractDao<ControlAprobacionActo, Long> {

    public ControlAprobacionActoDao() {
        super(ControlAprobacionActo.class);
    }

    /**
     * Permite consultar un listado de ControlAprobacionActo por criterios
     * dinamicos.
     *
     * @param criterios criterios de filtrado para la busqueda.
     * @param ordenamiento representa los criterios de ordenamiento por campos
     * @param numItems cantidad maxima de resultado a obtener.
     * @param numPagina representa el numero de pagina que se va a consultar.
     *
     * @return listado de ControlAprobacionActo que cumplen con los criterios
     * dados y cantidad de paginas sin limite.
     * @throws co.gov.ugpp.parafiscales.exception.AppException
     */
    public PagerData<ControlAprobacionActo> findByCriterios(final List<ParametroTipo> criterios,
            final List<CriterioOrdenamientoTipo> ordenamiento,
            final Long numItems, final Long numPagina) throws AppException {

        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery criteriaQuery = builder.createQuery();
        final Root root = criteriaQuery.from(entityClass);
        criteriaQuery.distinct(true);
        criteriaQuery.select(root);

        if (criterios != null) {
            final List<Predicate> predicateList = new ArrayList<Predicate>();
            for (final ParametroTipo paramValueetro : criterios) {
                if (StringUtils.isNotBlank(paramValueetro.getIdLlave())) {
                    final String llave = paramValueetro.getIdLlave();
                    final Path attr = this.obtainAttr(root, llave);
                    final Class attrType = attr.getJavaType();
                    final Object attrVal;
                    final Predicate predicate;
                    if (attrType == Calendar.class) {
                        Calendar fechaInicio = DateUtil.parseStrDateTimeToCalendar(paramValueetro.getValValor());
                        Calendar fechaFin = DateUtil.parseStrDateTimeToCalendar(paramValueetro.getValValor());
                        fechaInicio.set(Calendar.HOUR_OF_DAY, Constants.HORA_INICIO_PERIODO);
                        fechaInicio.set(Calendar.MINUTE, Constants.MINUTO_INICIO_PERIODO);
                        fechaInicio.set(Calendar.SECOND, Constants.SEGUNDO_INICIO_PERIODO);
                        fechaFin.set(Calendar.HOUR_OF_DAY, Constants.HORA_FIN_PERIODO);
                        fechaFin.set(Calendar.MINUTE, Constants.MINUTO_FIN_PERIODO);
                        fechaFin.set(Calendar.SECOND, Constants.SEGUNDO_FIN_PERIODO);
                        predicate = builder.between(attr, fechaInicio.getTime(), fechaFin.getTime());
                    } else {
                        attrVal = paramValueetro.getValValor();
                        predicate = builder.equal(attr, attrVal);
                    }
                    predicateList.add(predicate);
                }
            }
            if (!predicateList.isEmpty()) {
                final Predicate[] predicates = new Predicate[predicateList.size()];
                predicateList.toArray(predicates);
                criteriaQuery.where(predicates);
            }
        }

        if (ordenamiento != null) {
            final List<Order> orderList = new ArrayList<Order>();
            for (final CriterioOrdenamientoTipo orden : ordenamiento) {
                if (StringUtils.isNotBlank(orden.getValNombreCampo())) {
                    final String llave = orden.getValNombreCampo();
                    final Path attr = this.obtainAttr(root, llave);
                    Order order;
                    if (orden.getValOrden() != null
                            && Constants.CRITERIO_ORDENAMIENTO_DESCENDENTE.equals(orden.getValOrden())) {
                        order = builder.desc(attr);
                    } else {
                        order = builder.asc(attr);
                    }
                    orderList.add(order);
                }
            }
            if (!orderList.isEmpty()) {
                criteriaQuery.orderBy(orderList);
            }
        }

        final TypedQuery<ControlAprobacionActo> queryData = this.getEntityManager().createQuery(criteriaQuery);

        if (numItems != null && numItems > 0) {
            final int numIts = numItems.intValue();
            queryData.setMaxResults(numIts);
            if (numPagina != null && numPagina > 0) {
                final int numPag = numPagina.intValue();
                queryData.setFirstResult(numIts * numPag - numIts);
            }
        }

        final List<ControlAprobacionActo> data;

        try {
            data = queryData.getResultList();
        } catch (PersistenceException pe) {
            throw new AppException("Error al buscar por criterios entidades de tipo: " + entityClassName, pe);
        }

        long cantidadPaginas = 1;

        if (data.size() > 0 && numItems != null) {
            // Solo se ejecuta la consulta del conteo de registros si el tamaño de la lista traida es igual al limit.
            if (data.size() == numItems) {
                criteriaQuery.select(builder.count(root));
                final TypedQuery<Long> queryCount = this.getEntityManager().createQuery(criteriaQuery);
                final long count;
                try {
                    count = queryCount.getSingleResult();
                } catch (PersistenceException pe) {
                    throw new AppException("Error al consultar la cantidad de registros de busqueda por criterios de"
                            + " una entidad tipo: " + entityClassName, pe);
                }
                final double div = (double) count / (double) numItems;
                final double mod = (double) count % (double) numItems;
                cantidadPaginas = (long) (mod == 0 ? div : div + 1);
            }
        }
        return new PagerData<ControlAprobacionActo>(data, cantidadPaginas);
    }

    private Path obtainAttr(final Root root, final String llave) {
        Path attr;
        if (llave.contains(".")) {
            final String[] llaveArr = llave.split("\\.");
            attr = root.get(llaveArr[0]);
            for (int i = 1, n = llaveArr.length; i < n; ++i) {
                attr = attr.get(llaveArr[i]);
            }
        } else {
            attr = root.get(llave);
        }
        return attr;
    }
}
