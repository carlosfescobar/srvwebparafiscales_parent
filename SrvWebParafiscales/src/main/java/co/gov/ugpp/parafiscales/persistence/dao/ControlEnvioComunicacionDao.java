package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ControlEnvioComunicacion;
import javax.ejb.Stateless;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class ControlEnvioComunicacionDao extends AbstractDao<ControlEnvioComunicacion, Long> {

    public ControlEnvioComunicacionDao() {
        super(ControlEnvioComunicacion.class);
    }
    
}
