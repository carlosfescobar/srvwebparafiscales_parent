package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidadorArchivoTemporal;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ControlLiquidadorArchivoTemporalDao extends AbstractDao<ControlLiquidadorArchivoTemporal, Long> {

    public ControlLiquidadorArchivoTemporalDao() {
        super(ControlLiquidadorArchivoTemporal.class);
    }

}
