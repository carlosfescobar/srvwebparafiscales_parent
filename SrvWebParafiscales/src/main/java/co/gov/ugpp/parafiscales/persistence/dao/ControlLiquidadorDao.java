package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ControlLiquidadorDao extends AbstractHasPersonaDao<ControlLiquidador, Long> {

    public ControlLiquidadorDao() {
        super(ControlLiquidador.class);
    }
}
