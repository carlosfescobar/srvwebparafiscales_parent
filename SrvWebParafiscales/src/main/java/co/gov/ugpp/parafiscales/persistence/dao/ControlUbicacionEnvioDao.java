package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ControlUbicacionEnvio;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ControlUbicacionEnvioDao extends AbstractDao<ControlUbicacionEnvio, Long> {

    public ControlUbicacionEnvioDao() {
        super(ControlUbicacionEnvio.class);
    }
}
