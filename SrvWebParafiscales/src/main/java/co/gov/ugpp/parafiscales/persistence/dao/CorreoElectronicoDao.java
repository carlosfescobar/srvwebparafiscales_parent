package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class CorreoElectronicoDao extends AbstractDao<CorreoElectronico, Long> {

    public CorreoElectronicoDao() {
        super(CorreoElectronico.class);
    }
    
    
    public Long findCorreoElectronico(final String valCorreoElectronico,final Long idContacto, final ValorDominio codFuente) throws AppException {

        Query query = getEntityManager().createNamedQuery("correoElectronico.findCorreoElectronico");
        query.setParameter("valCorreoElectronico", valCorreoElectronico);
        query.setParameter("idContacto", idContacto);
        query.setParameter("codFuente", codFuente.getId());

        
        try {
            return (Long) query.getSingleResult();
        } catch (NoResultException nre) {
            return 0L;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
 
}
