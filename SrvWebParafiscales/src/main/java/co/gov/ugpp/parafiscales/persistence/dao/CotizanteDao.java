package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Cotizante;
import javax.ejb.Stateless;

/**
 *
 * @author rpadilla
 */
@Stateless
public class CotizanteDao extends AbstractHasPersonaDao<Cotizante, Long> {

    public CotizanteDao() {
        super(Cotizante.class);
    }

}
