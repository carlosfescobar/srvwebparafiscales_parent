package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.AportanteLIQ;
import co.gov.ugpp.parafiscales.persistence.entity.CotizanteLIQ;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 * 
 * @author franzjr
 */
@Stateless
public class CotizanteLIQDao extends AbstractDao<CotizanteLIQ, Long> {

    public CotizanteLIQDao() {
        super(CotizanteLIQ.class);
    }
    
     public CotizanteLIQ findByIdentificacion(final String numeroIdentificacion, String sigla) throws AppException {
        final TypedQuery<CotizanteLIQ> query = getEntityManager().createNamedQuery("Cotizante.findByIdentificacion", CotizanteLIQ.class);
        query.setParameter("numeroIdentificacion", numeroIdentificacion);
        query.setParameter("tipoIdentificacion", sigla);

        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
