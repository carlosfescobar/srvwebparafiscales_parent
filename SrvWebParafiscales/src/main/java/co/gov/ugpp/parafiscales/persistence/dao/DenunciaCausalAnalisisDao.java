package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalAnalisis;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class DenunciaCausalAnalisisDao extends AbstractDao<DenunciaCausalAnalisis, Long> {

    public DenunciaCausalAnalisisDao() {
        super(DenunciaCausalAnalisis.class);
    }
}
