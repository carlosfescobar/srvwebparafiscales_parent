package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalInicial;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class DenunciaCausalInicialDao extends AbstractDao<DenunciaCausalInicial, Long> {

    public DenunciaCausalInicialDao() {
        super(DenunciaCausalInicial.class);
    }
}
