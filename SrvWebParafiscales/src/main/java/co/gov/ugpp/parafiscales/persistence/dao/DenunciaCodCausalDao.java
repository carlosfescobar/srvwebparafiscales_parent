package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DenunciHasCodCausalDenunci;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DenunciaCodCausalDao extends AbstractDao<DenunciHasCodCausalDenunci, Long> {

    public DenunciaCodCausalDao() {
        super(DenunciHasCodCausalDenunci.class);
    }

}
