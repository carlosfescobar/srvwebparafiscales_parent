package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DenunciaHasCodSubsistema;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DenunciaCodSubDao extends AbstractDao<DenunciaHasCodSubsistema, Long> {

    public DenunciaCodSubDao() {
        super(DenunciaHasCodSubsistema.class);
    }

}
