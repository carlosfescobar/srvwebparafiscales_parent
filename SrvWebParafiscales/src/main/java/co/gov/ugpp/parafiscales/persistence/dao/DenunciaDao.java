package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DenunciaDao extends AbstractDao<Denuncia, Long> {

    public DenunciaDao() {
        super(Denuncia.class);
    }

    public List<Denuncia> findByExpediente(String id) throws AppException{

        Query query = getEntityManager().createNamedQuery("Denuncia.findByExpediente");
        query.setParameter("expedienteNumero", id);
     
        try {
            return (List<Denuncia>) query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } 
        catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
        
    }

    public Denuncia findByExpedienteDenuncia(final String id) throws AppException {

        Query query = getEntityManager().createNamedQuery("Denuncia.findByExpediente");
        query.setParameter("expedienteNumero", id);

        try {
            return (Denuncia) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
