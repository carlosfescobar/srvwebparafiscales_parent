package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaAccion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author rpadilla
 */
@Stateless
public class DenunciaHasAccionDao extends AbstractDao<DenunciaAccion, Long> {

    public DenunciaHasAccionDao() {
        super(DenunciaAccion.class);
    }

    public DenunciaAccion findByRequerimientoInformacionAndCodCausalDecolucion(final Denuncia idDenuncia, final Accion idAccion) throws AppException {
        final TypedQuery<DenunciaAccion> query = getEntityManager().createNamedQuery("DenunciaHasAccion.findByDenunciaAndAccion", DenunciaAccion.class);
        query.setParameter("idDenuncia", idDenuncia.getId());
        query.setParameter("idAccion", idAccion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
