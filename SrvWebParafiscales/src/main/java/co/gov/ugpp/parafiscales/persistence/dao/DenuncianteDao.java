package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Denunciante;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DenuncianteDao extends AbstractHasPersonaDao<Denunciante, Long> {
    
    public DenuncianteDao() {
        super(Denunciante.class);
    }

}
