package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Departamento;
import javax.ejb.Stateless;

/**
 *
 * @author ingenian
 */
@Stateless
public class DepartamentoDao extends AbstractDao<Departamento, Long> {

    public DepartamentoDao() {
        super(Departamento.class);
    }


}
