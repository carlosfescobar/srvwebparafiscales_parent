package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DivisionCiiu;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DivisionCiiuDao extends AbstractDao<DivisionCiiu, Long> {

    public DivisionCiiuDao() {
        super(DivisionCiiu.class);
    }
}
