package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosCodnot;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DocAnexosCodNotificacionDao extends AbstractDao<DocumentosAnexosCodnot, Long> {

    public DocAnexosCodNotificacionDao() {
        super(DocumentosAnexosCodnot.class);
    }

}
