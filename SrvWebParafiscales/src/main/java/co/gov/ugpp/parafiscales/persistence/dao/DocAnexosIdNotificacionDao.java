package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosIdnot;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DocAnexosIdNotificacionDao extends AbstractDao<DocumentosAnexosIdnot, Long> {

    public DocAnexosIdNotificacionDao() {
        super(DocumentosAnexosIdnot.class);
    }
    
}
