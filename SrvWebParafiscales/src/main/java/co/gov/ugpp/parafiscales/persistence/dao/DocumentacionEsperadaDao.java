package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentacionEsperada;
import javax.ejb.Stateless;


@Stateless
public class DocumentacionEsperadaDao extends AbstractDao<DocumentacionEsperada, Long> {

    public DocumentacionEsperadaDao() {
        super(DocumentacionEsperada.class);
    }

}
