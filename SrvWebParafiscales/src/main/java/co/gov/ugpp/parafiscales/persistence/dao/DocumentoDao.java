package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DocumentoDao extends AbstractDao<DocumentoEcm, String> {

    public DocumentoDao() {
        super(DocumentoEcm.class);
    }

}
