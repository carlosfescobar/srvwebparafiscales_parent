package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Dominio;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class DominioDao extends AbstractDao<Dominio, String> {

    public DominioDao() {
        super(Dominio.class);
    }
}
