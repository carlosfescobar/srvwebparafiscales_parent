package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Efectividad;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class EfectividadDao extends AbstractDao<Efectividad, Long> {

    public EfectividadDao() {
        super(Efectividad.class);
    }

}
