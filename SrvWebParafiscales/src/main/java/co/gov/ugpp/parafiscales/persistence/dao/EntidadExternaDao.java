package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class EntidadExternaDao extends AbstractHasPersonaDao<EntidadExterna, Long> {

    public EntidadExternaDao() {
        super(EntidadExterna.class);
    }

}
