package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExternaTipoConsulta;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmunocab
 */
@Stateless
public class EntidadExternaTipoConsultaDao extends AbstractDao<EntidadExternaTipoConsulta, Long> {

    public EntidadExternaTipoConsultaDao() {
        super(EntidadExternaTipoConsulta.class);
    }

    public List<EntidadExternaTipoConsulta> findSolicitudByEntidadExternaId(final String tipoConsulta) throws AppException {

        Query query = getEntityManager().createNamedQuery("entExternaTipoConsul.findByTipoConsulta");
        query.setParameter("tipoConsulta", tipoConsulta);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
