package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class EntidadNegocioDao extends AbstractDao<EntidadNegocio, Long> {

    public EntidadNegocioDao() {
        super(EntidadNegocio.class);
    }

    public EntidadNegocio findByCodProcesoAndIdEntidad(final String codProceso, final String idEntidadNegocio) throws AppException {
        Query query = getEntityManager().createNamedQuery("entidadNegocio.findByIdEntidadNegocio");
        query.setParameter("codProceso", codProceso);
        query.setParameter("idEntidadNegocio", idEntidadNegocio);
        try {
            return (EntidadNegocio) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
