package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class EnvioInformacionExternaDao extends AbstractDao<EnvioInformacionExterna, String> {

    public EnvioInformacionExternaDao() {
        super(EnvioInformacionExterna.class);
    }

}
