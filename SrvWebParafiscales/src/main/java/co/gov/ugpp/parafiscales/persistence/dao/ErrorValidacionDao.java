package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ErrorValidacion;
import javax.ejb.Stateless;

/**
 *
 * @author jgonzalezt
 */
@Stateless
public class ErrorValidacionDao extends AbstractDao<ErrorValidacion, Long> {

    public ErrorValidacionDao() {
        super(ErrorValidacion.class);
    }
}
