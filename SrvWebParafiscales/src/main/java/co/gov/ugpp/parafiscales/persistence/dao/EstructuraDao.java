package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.servicios.util.files.Estructura;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author franzjr
 */
@Stateless
public class EstructuraDao extends AbstractDao<Estructura, Long> {

    public EstructuraDao() {
        super(Estructura.class);
    }

    public Estructura findByFormato(Long idFormato, Long idVersion) {

        Query query = getEntityManager().createNamedQuery("estructura.findByFormatoPK");
        query.setParameter("idFormato", idFormato);
        query.setParameter("idVersion", idVersion);

        System.err.println(query.getResultList());

        if (query.getResultList().isEmpty()) {
            return null;
        } else {
            return (Estructura) query.getSingleResult();

        }

    }
}
