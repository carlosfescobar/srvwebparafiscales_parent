package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.EventoEnvio;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class EventoEnvioDao extends AbstractDao<EventoEnvio, Long> {

    public EventoEnvioDao() {
        super(EventoEnvio.class);
    }
}
