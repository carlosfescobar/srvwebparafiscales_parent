package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ExpedienteDao extends AbstractDao<Expediente, String> {

    public ExpedienteDao() {
        super(Expediente.class);
    }
}
