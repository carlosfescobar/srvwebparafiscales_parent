package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.util.Constants;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ExpedienteDocumentoDao extends AbstractDao<ExpedienteDocumentoEcm, Long> {

    public ExpedienteDocumentoDao() {
        super(ExpedienteDocumentoEcm.class);
    }

    public ExpedienteDocumentoEcm findDocumentoExpedienteByDocumento(final String idDocumento) throws AppException {
        final TypedQuery<ExpedienteDocumentoEcm> query = this.getEntityManager()
                .createNamedQuery("expedienteDocumento.findByDocumento", ExpedienteDocumentoEcm.class);
        query.setParameter("idDocumento", idDocumento);
        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException ex) {
            throw new AppException("Excepcion de persistencia", ex);
        }
    }

    public ExpedienteDocumentoEcm findDocumentoExpedienteByExpedienteAndDocumento(final String idExpediente,
            final String idDocumento) throws AppException {

        final TypedQuery<ExpedienteDocumentoEcm> query = this.getEntityManager()
                .createNamedQuery("expedienteDocumento.findByExpedienteAndDocumento", ExpedienteDocumentoEcm.class);

        query.setParameter("idExpediente", idExpediente);
        query.setParameter("idDocumento", idDocumento);

        try {
            return query.getSingleResult();
        } catch (NoResultException ex) {
            return null;
        } catch (PersistenceException ex) {
            throw new AppException("Excepcion de persistencia", ex);
        }
    }

    public void actualizarCodOrigen(final OrigenDocumentoEnum newOrigen, final OrigenDocumentoEnum oldOrigen,
            final ExpedienteDocumentoEcm expedienteDocumentoNoIncluido) throws AppException {

        Query query = getEntityManager().createNamedQuery(Constants.ACTUALIZAR_COD_ORIGEN_MASIVO);
        query.setParameter(Constants.PAR_UNO, newOrigen.getCode());
        query.setParameter(Constants.PAR_DOS, oldOrigen.getCode());
        query.setParameter(Constants.PAR_TRES, expedienteDocumentoNoIncluido.getIdExpediente());
        query.setParameter(Constants.PAR_CUATRO, expedienteDocumentoNoIncluido.getId());
        
        try {
            query.executeUpdate();
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
