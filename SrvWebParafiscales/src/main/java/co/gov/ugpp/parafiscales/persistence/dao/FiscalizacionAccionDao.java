package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionAccion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class FiscalizacionAccionDao extends AbstractDao<FiscalizacionAccion, Long> {

    public FiscalizacionAccionDao() {
        super(FiscalizacionAccion.class);
    }

    public FiscalizacionAccion findByFiscalizacionAndAccion(final Fiscalizacion fiscalizacion, final Accion accion) throws AppException {
        final TypedQuery<FiscalizacionAccion> query = getEntityManager().createNamedQuery("fiscalizacionhasaccion.findByFiscalizacionAndAccion", FiscalizacionAccion.class);
        query.setParameter("idAccion", accion.getId());
        query.setParameter("idFiscalizacion", fiscalizacion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
