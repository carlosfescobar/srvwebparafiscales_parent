package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Busqueda;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionBusqueda;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class FiscalizacionBusquedaDao extends AbstractDao<FiscalizacionBusqueda, Long> {

    public FiscalizacionBusquedaDao() {
        super(FiscalizacionBusqueda.class);
    }

    public FiscalizacionBusqueda findByFiscalizacionAndBusqueda(final Fiscalizacion fiscalizacion, final Busqueda busqueda) throws AppException {
        final TypedQuery<FiscalizacionBusqueda> query = getEntityManager().createNamedQuery("fiscalizacionhasbusqueda.findByFiscalizacionAndBusqueda", FiscalizacionBusqueda.class);
        query.setParameter("idbusqueda", busqueda.getId());
        query.setParameter("idFiscalizacion", fiscalizacion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
