package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import java.util.Calendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class FiscalizacionDao extends AbstractDao<Fiscalizacion, Long> {

    public FiscalizacionDao() {
        super(Fiscalizacion.class);
    }

    /**
     * Método que busca una fiscalización por expediente
     *
     * @param idExpediente el id del expediente relacionado a la fiscalización
     * @return la fiscalización asociada al expediente
     * @throws AppException
     */
    public Fiscalizacion findFiscalizacionByExpediente(final String idExpediente) throws AppException {

        Query query = getEntityManager().createNamedQuery("fiscalizacion.findByExpediente");
        query.setParameter("idExpediente", idExpediente);

        try {
            return (Fiscalizacion) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que busca una fiscalización asociada a un aportante
     *
     * @param identificacionAportante la identificación del aportante que
     * contiene el tipo y número de documento
     * @return la fiscalización asociada al aportante
     * @throws AppException
     */
    public Fiscalizacion findFiscalizacionByAportante(final Identificacion identificacionAportante) throws AppException {

        Query query = getEntityManager().createNamedQuery("fiscalizacion.findByAportante");
        query.setParameter("numeroIdentificacion", identificacionAportante.getValNumeroIdentificacion());
        query.setParameter("tipoIdentificacion", identificacionAportante.getCodTipoIdentificacion());

        try {
            return (Fiscalizacion) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que busca una fiscalización asociada a un aportante en un periodo determinado
     *
     * @param identificacionAportante la identificación del aportante que
     * contiene el tipo y número de documento
     * @param fechaInicio fecha inicio del periodo de la fiscalizacion
     * @param fechaFin fecha fin del periodo de la fiscalizacion
     * @return la fiscalización asociada al aportante dentro del periodo determinado
     * @throws AppException
     */
    public List<Fiscalizacion> findFiscalizacionByAportanteAndPeriodo(final Identificacion identificacionAportante, 
            final Calendar fechaInicio, final Calendar fechaFin) throws AppException {

        Query query = getEntityManager().createNamedQuery("fiscalizacion.findByAportanteAndPeriodo");
        query.setParameter("numeroIdentificacion", identificacionAportante.getValNumeroIdentificacion());
        query.setParameter("tipoIdentificacion", identificacionAportante.getCodTipoIdentificacion());
        query.setParameter("fechaInicio", fechaInicio);
        query.setParameter("fechaFin", fechaFin);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
