package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author rpadilla
 */
@Stateless
public class FiscalizacionHasActoAdmiDao extends AbstractDao<FiscalizacionActoAdministrativo, Long> {

    public FiscalizacionHasActoAdmiDao() {
        super(FiscalizacionActoAdministrativo.class);
    }

    public FiscalizacionActoAdministrativo findByfindByFiscalizacionAndActoAdminitrativo(final Fiscalizacion fiscalizacion, final ActoAdministrativo actoAdministrativo) throws AppException {
        final TypedQuery<FiscalizacionActoAdministrativo> query = getEntityManager().createNamedQuery("FiscalizacionHasActoAdmi.findByFiscalizacionAndActoAdminitrativo", FiscalizacionActoAdministrativo.class);
        query.setParameter("idFiscalizacion", fiscalizacion.getId());
        query.setParameter("actoAdministrativo", actoAdministrativo.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
