package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionIncumplimiento;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author jmuncab
 */
@Stateless
public class FiscalizacionIncumplimientoDao extends AbstractDao<FiscalizacionIncumplimiento, Long> {

    public FiscalizacionIncumplimientoDao() {
        super(FiscalizacionIncumplimiento.class);
    }

    public FiscalizacionIncumplimiento findByFiscalizacionAndCodIncumplimiento(final Fiscalizacion fiscalizacion, final ValorDominio codIncumplimiento) throws AppException {
        final TypedQuery<FiscalizacionIncumplimiento> query = getEntityManager().createNamedQuery("fiscalizahasincumplimto.findByFiscalizacionAndCodIncumplimiento", FiscalizacionIncumplimiento.class);
        query.setParameter("idcodIncumplimiento", codIncumplimiento.getId());
        query.setParameter("idFiscalizacion", fiscalizacion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
