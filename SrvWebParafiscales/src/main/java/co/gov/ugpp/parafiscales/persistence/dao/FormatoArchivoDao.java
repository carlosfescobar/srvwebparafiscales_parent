package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.FormatoArchivo;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoArchivoPK;
import javax.ejb.Stateless;

/**
 *
 * @author jsaenzar
 */
@Stateless
public class FormatoArchivoDao extends AbstractDao<FormatoArchivo, FormatoArchivoPK> {

    public FormatoArchivoDao() {
        super(FormatoArchivo.class);
    }


}