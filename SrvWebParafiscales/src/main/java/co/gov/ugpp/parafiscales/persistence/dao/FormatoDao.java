package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class FormatoDao extends AbstractDao<Formato, FormatoPK> {

    public FormatoDao() {
        super(Formato.class);
    }

    public Formato findFormatoByFormatoAndVersion(Long idFormato, Long idVersion) throws AppException {

        Query query = getEntityManager().createNamedQuery("formato.findByFormatoAndVersion");
        query.setParameter("idFormato", idFormato);
        query.setParameter("idVersion", idVersion);

        
        System.out.println("::ANDRES:: - idFormato: " + idFormato);
        System.out.println("::ANDRES:: - idVersion: " + idVersion);
        
        
        
        try {
            return (Formato) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
