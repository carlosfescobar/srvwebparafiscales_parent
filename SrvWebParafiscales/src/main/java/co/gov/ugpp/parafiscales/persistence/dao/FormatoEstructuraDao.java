package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoEstructura;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jdmunoz
 */
@Stateless
public class FormatoEstructuraDao extends AbstractDao<FormatoEstructura, Long> {

    public FormatoEstructuraDao() {
        super(FormatoEstructura.class);
    }

    public FormatoEstructura findByFormato(final FormatoPK pk) throws AppException {
        Query query = getEntityManager().createNamedQuery("formatoEstructura.findByFormatoPK");
        query.setParameter("idFormato", pk.getIdFormato());
        query.setParameter("idVersion", pk.getIdVersion());

        try {
            return (FormatoEstructura) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
