package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Formula;
import co.gov.ugpp.parafiscales.servicios.util.liquidador.ColumnaDb;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.Stateless;
import javax.persistence.Column;
import javax.persistence.metamodel.EntityType;
import org.eclipse.persistence.exceptions.QueryException;

/**
 * 
 * @author franzjr
 */
@Stateless
public class FormulaDao extends AbstractDao<Formula, Long> {

    private List<ColumnaDb> operaciones = new ArrayList<ColumnaDb>();


    public FormulaDao() {
        super(Formula.class);
        ColumnaDb op = new ColumnaDb();
        op.setTabla("Aritmeticas");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Adición (+)");
        op.setColumna("+");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Sustracción (-)");
        op.setColumna("-");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Multiplicacion (*)");
        op.setColumna("*");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Cociente (/)");
        op.setColumna("/");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Logaritmo Base 10 (Log(variable))");
        op.setColumna("Log()");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Promedio (Promedio(variable))");
        op.setColumna("Promedio()");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Potenciación (Potenciación(Base,Exp))");
        op.setColumna("Potenciación( , )");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Porcentaje (Porcentaje(Base,Porcentaje))");
        op.setColumna("Porcentaje( , )");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Logicas");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Caso Si (Decisión) Entonces (Resultado) Sino (Resultado);");
        op.setColumna("Caso Si () Entonces () Sino ();");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Si (Decisión) Entonces (Resultado)");
        op.setColumna("Si () Entonces ()");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("Y");
        op.setColumna("Y");
        operaciones.add(op);
        op = new ColumnaDb();
        op.setTabla("O");
        op.setColumna("O");
        operaciones.add(op);
    }

    public List<ColumnaDb> findColumnsOfDatabase() {
        Hashtable<String, Object> res = new Hashtable<String, Object>();
        List<ColumnaDb> columnNames = new ArrayList<ColumnaDb>();
        for (EntityType entity : getEntityManager().getMetamodel().getEntities()) {
            columnNames.add(new ColumnaDb(null, entity.getName()));
            Field[] campos2 = entity.getJavaType().getDeclaredFields();
            for (Field atrr : campos2) {
                Column column = atrr.getAnnotation(Column.class);
                if (column != null) {
                    columnNames.add(new ColumnaDb(atrr.getName(), entity.getName()));
                }
            }
        }
        Collections.sort(columnNames);
        return columnNames;
    }

    public List<Object> ejecutarFormula(Formula fo) throws Exception {
        Pattern pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
        Matcher matcher = pattern.matcher(fo.getFormula());
        String formula = fo.getFormula();
        String tablas = "";
        int numTab = 0;
        while (matcher.find()) {
            if (!tablas.contains(matcher.group(1))) {
                tablas = tablas + matcher.group(1) + " AS tab" + numTab + ", ";
                formula = formula.replace(matcher.group(1) + matcher.group(2), "tab" + numTab + matcher.group(2));
                numTab++;
            }
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        formula = formula.replace("Log(", "FUNC('Ln',");
        if (formula.contains("Promedio")) {
            pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
            matcher = pattern.matcher(formula);
            tablas = tablas + " Group by ";
            while (matcher.find()) {
                if (!tablas.contains(matcher.group(1) + matcher.group(2) + matcher.group(3))) {
                    tablas = tablas + matcher.group(1) + matcher.group(2) + matcher.group(3) + ", ";
                }
            }
            tablas = tablas.substring(0, tablas.length() - 2);
            formula = formula.replace("Promedio", "avg");

        }
        formula = formula.replace("Potenciación(", "FUNC('Power',");
        pattern = Pattern.compile("(Porcentaje[(])([ ]*?[A-Za-z0-9_.]*[ ]*?)(,)([ ]*?[A-Za-z0-9_.]*[ ]*?)([)])");
        matcher = pattern.matcher(formula);

        while (matcher.find()) {
            formula = formula.replace(matcher.group(1) + matcher.group(2) + matcher.group(3) + matcher.group(4) + matcher.group(5), "(" + matcher.group(2).trim() + " * (" + matcher.group(4).trim() + "/100))");
        }

        formula = formula.replace("Caso Si ", "CASE WHEN ");
        formula = formula.replace(" Si ", " WHEN ");
        formula = formula.replace(" Entonces ", " THEN ");
        formula = formula.replace(" Sino ", " ELSE ");
        formula = formula.replace(" Y ", " AND ");
        formula = formula.replace(" O ", " OR ");
        formula = formula.replace(";", " END");
        String[] tabla = tablas.split(",");
        List<Integer> orden = new ArrayList<Integer>();
        tablas = "";
        for (int i = 0; i < tabla.length; i++) {
            orden.add(formula.lastIndexOf(tabla[i].substring(tabla[i].length() - 4, tabla[i].length())));
        }

        for (int i = 0; i < tabla.length; i++) {
            for (int j = 1; j < tabla.length; j++) {
                if (orden.get(j) > orden.get(i)) {
                    Integer aux = orden.get(i);
                    String auxtab = tabla[i];
                    orden.set(i, orden.get(j));
                    tabla[i] = tabla[j];
                    orden.set(j, aux);
                    tabla[j] = auxtab;
                }
            }
        }

        for (int i = 0; i < tabla.length; i++) {
            tablas = tablas + tabla[i] + ", ";
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        String jpqlQuery = "SELECT " + formula + " from " + tablas;
        List<Object> res = null;
        try {
            res = findByJPQLQuery(jpqlQuery);
        } catch (QueryException q) {
            String[] newOrder = tablas.split(",");
            tablas = newOrder[1]+ ", ";
            for (int i = 0; i < newOrder.length; i++) {
                if (i != 1) {
                    tablas = tablas + tabla[i] + ", ";
                }
            }
            tablas = tablas.substring(0, tablas.length() - 2);
            jpqlQuery = "SELECT " + formula + " from " + tablas;
            res = findByJPQLQuery(jpqlQuery);
        }
        return res;
    }

    public List<Object> ejecutarFormula(Formula fo, String where) throws Exception {
        Pattern pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
        Matcher matcher = pattern.matcher(fo.getFormula());
        String formula = fo.getFormula();
        String tablas = "";
        int numTab = 0;
        while (matcher.find()) {
            if (!tablas.contains(matcher.group(1))) {
                tablas = tablas + matcher.group(1) + " tab" + numTab + ", ";
                formula = formula.replace(matcher.group(1), "tab" + numTab);
                numTab++;
            }
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        formula = formula.replace("Log(", "FUNC('Ln',");
        if (formula.contains("Promedio")) {
            pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
            matcher = pattern.matcher(formula);
            tablas = tablas + " Group by ";
            while (matcher.find()) {
                if (!tablas.contains(matcher.group(1) + matcher.group(2) + matcher.group(3))) {
                    tablas = tablas + matcher.group(1) + matcher.group(2) + matcher.group(3) + ", ";
                }
            }
            tablas = tablas.substring(0, tablas.length() - 2);
            formula = formula.replace("Promedio", "avg");

        }
        formula = formula.replace("Potenciación(", "FUNC('Power',");
        pattern = Pattern.compile("(Porcentaje[(])([ ]*?[A-Za-z0-9_.]*[ ]*?)(,)([ ]*?[A-Za-z0-9_.]*[ ]*?)([)])");
        matcher = pattern.matcher(formula);

        while (matcher.find()) {
            formula = formula.replace(matcher.group(1) + matcher.group(2) + matcher.group(3) + matcher.group(4) + matcher.group(5), "(" + matcher.group(2).trim() + " * (" + matcher.group(4).trim() + "/100))");
        }

        formula = formula.replace("Caso Si ", "CASE WHEN ");
        formula = formula.replace(" Si ", " WHEN ");
        formula = formula.replace(" Entonces ", " THEN ");
        formula = formula.replace(" Sino ", " ELSE ");
        formula = formula.replace(" Y ", " AND ");
        formula = formula.replace(" O ", " OR ");
        formula = formula.replace(";", " END");
        String[] tabla = tablas.split(",");
        List<Integer> orden = new ArrayList<Integer>();
        tablas = "";
        for (int i = 0; i < tabla.length; i++) {
            orden.add(formula.lastIndexOf(tabla[i].substring(tabla[i].length() - 4, tabla[i].length())));
        }

        for (int i = 0; i < tabla.length; i++) {
            for (int j = 1; j < tabla.length; j++) {
                if (orden.get(j) > orden.get(i)) {
                    Integer aux = orden.get(i);
                    String auxtab = tabla[i];
                    orden.set(i, orden.get(j));
                    tabla[i] = tabla[j];
                    orden.set(j, aux);
                    tabla[j] = auxtab;
                }
            }
        }

        for (int i = 0; i < tabla.length; i++) {
            tablas = tablas + tabla[i] + ", ";
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        String jpqlQuery = "SELECT " + formula + " from " + tablas;

        if (where != "") {
            jpqlQuery += " where " + where;
        }

        return findByJPQLQuery(jpqlQuery);
    }

    public List<Object> ejecutarFormula(Formula fo, String tabla, Long id) throws Exception {
        Pattern pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
        Matcher matcher = pattern.matcher(fo.getFormula());
        String formula = fo.getFormula();
        String tablaId = "";
        String tablas = "";
        int numTab = 0;
        while (matcher.find()) {
            if (!tablas.contains(matcher.group(1))) {
                tablas = tablas + matcher.group(1) + " tab" + numTab + ", ";
                if (matcher.group(1).equals(tabla)) {
                    tablaId = " WHERE  tab" + numTab + ".id = :id";
                }
                formula = formula.replace(matcher.group(1), "tab" + numTab);
                numTab++;
            }
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        formula = formula.replace("Log(", "FUNC('Ln',");
        if (formula.contains("Promedio")) {
            pattern = Pattern.compile("([A-Za-z][A-Za-z0-9_]*)(\\.)([A-Za-z0-9_]*)");
            matcher = pattern.matcher(formula);
            tablas = tablas + " Group by ";
            while (matcher.find()) {
                if (!tablas.contains(matcher.group(1) + matcher.group(2) + matcher.group(3))) {
                    tablas = tablas + matcher.group(1) + matcher.group(2) + matcher.group(3) + ", ";
                }
            }
            tablas = tablas.substring(0, tablas.length() - 2);
            formula = formula.replace("Promedio", "avg");

        }
        formula = formula.replace("Potenciación(", "FUNC('Power',");
        pattern = Pattern.compile("(Porcentaje[(])([ ]*?[A-Za-z0-9_.]*[ ]*?)(,)([ ]*?[A-Za-z0-9_.]*[ ]*?)([)])");
        matcher = pattern.matcher(formula);

        while (matcher.find()) {
            formula = formula.replace(matcher.group(1) + matcher.group(2) + matcher.group(3) + matcher.group(4) + matcher.group(5), "(" + matcher.group(2).trim() + " * (" + matcher.group(4).trim() + "/100))");
        }

        formula = formula.replace("Caso Si ", "CASE WHEN ");
        formula = formula.replace(" Si ", " WHEN ");
        formula = formula.replace(" Entonces ", " THEN ");
        formula = formula.replace(" Sino ", " ELSE ");
        formula = formula.replace(" Y ", " AND ");
        formula = formula.replace(" O ", " OR ");
        formula = formula.replace(";", " END");
        String[] tablaq = tablas.split(",");
        List<Integer> orden = new ArrayList<Integer>();
        tablas = "";
        for (int i = 0; i < tablaq.length; i++) {
            orden.add(formula.lastIndexOf(tablaq[i].substring(tablaq[i].length() - 4, tablaq[i].length())));
        }

        for (int i = 0; i < tablaq.length; i++) {
            for (int j = 1; j < tablaq.length; j++) {
                if (orden.get(j) > orden.get(i)) {
                    Integer aux = orden.get(i);
                    String auxtab = tablaq[i];
                    orden.set(i, orden.get(j));
                    tablaq[i] = tablaq[j];
                    orden.set(j, aux);
                    tablaq[j] = auxtab;
                }
            }
        }

        for (int i = 0; i < tablaq.length; i++) {
            tablas = tablas + tablaq[i] + ", ";
        }
        tablas = tablas.substring(0, tablas.length() - 2);

        String jpqlQuery = "SELECT " + formula + " from " + tablas + tablaId;
        if (tablaId.equals("")) {
            return findByJPQLQuery(jpqlQuery);
        } else {
            Hashtable<String, Object> params = new Hashtable<String, Object>();
            params.clear();
            params.put("id", id);
            return findByJPQLQuery(jpqlQuery, params);
        }
    }

    private List findColumnsDatabaseOracle() {
        javax.persistence.Query q = getEntityManager().createNativeQuery("select col.table_name,col.column_name,col.owner from dba_tab_cols col inner join dba_tables tab on col.table_name=tab.table_name where tab.tablespace_name not in ('SYSAUX','SYSTEM','EXAMPLE') order by table_name,column_id");
        return q.getResultList();
    }

    public List<ColumnaDb> getOperaciones() {
        return operaciones;
    }

    public void setOperaciones(List<ColumnaDb> operaciones) {
        this.operaciones = operaciones;
    }
}