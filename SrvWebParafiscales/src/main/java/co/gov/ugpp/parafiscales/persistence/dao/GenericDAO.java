package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * Clase representa objeto de acceso a datos generico, el cual contiene las
 * operaciones CRUD a realizar. En esta se inyecta la unidad de persistencia.
 *
 * @author franzjr
 */
public abstract class GenericDAO<T> {

    private Class<T> entityClass;

    @PersistenceContext()
    private EntityManager em;

    protected EntityManager getEntityManager() {
        return em;
    }

    public GenericDAO(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public T create(T entity, String string) {
        getEntityManager().persist(entity);
        return entity;
    }

    public void create(T entity) {
        getEntityManager().persist(entity);
    }

    public void edit(T entity) {
        getEntityManager().merge(entity);
    }

    public void remove(T entity) {
        getEntityManager().remove(getEntityManager().merge(entity));
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }

    /**
     * @author cpalacios
     *
     * Permite consultar un listado de entidades por criterios dinamicos.
     *
     * @param parametros Los parametros a partir de los cuales se hara la
     * busqueda.
     * @param criterios Representa los criterios de ordenamiento por campos
     * @param tamPagina Representa la cantidad de registros a obtener.
     * @param numPagina Representa el numero de pagina que se va a consultar.
     *
     * @return El listado de entidades que cumplen con las condiciones dadas.
     */
    public List<T> findPorCriterios(Collection<ParametroTipo> parametros, Collection<CriterioOrdenamientoTipo> criterios,
            int tamPagina, int numPagina) {

        Object valueByType;
        StringBuilder where = new StringBuilder(" WHERE ");
        StringBuilder orderBy = new StringBuilder(" ORDER BY ");
        int cont = 0;
        for (ParametroTipo parametro : parametros) {
            if (parametro.getIdLlave() != null && !"".equals(parametro.getIdLlave())) {
                
                valueByType  = getValueByType(entityClass, parametro.getIdLlave(), parametro.getValValor());
                
                if (valueByType instanceof Calendar) {
                    where.append("  model.").append(parametro.getIdLlave())
                        .append(" LIKE :value").append(cont++).append(" AND");
                } else {
                    where.append("  model.").append(parametro.getIdLlave())
                        .append(" = :value").append(cont++).append(" AND");
                }
                
            }
        }

        if (where.length() > 10) {
            where.delete(where.lastIndexOf("AND"), where.length());//Eliminar el AND sobrante
        } else {
            where.delete(0, where.length());
        }

        System.out.println("Condicion where: " + where);

        for (CriterioOrdenamientoTipo criterio : criterios) {
            if (criterio.getValNombreCampo() != null && !"".equals(criterio.getValNombreCampo())) {
                orderBy.append("  model.").append(criterio.getValNombreCampo()).append(", ");
            }
        }

        if (orderBy.length() > 10) {
            orderBy.delete(orderBy.lastIndexOf(","), orderBy.length());//Elimina la ultima coma
        } else {
            orderBy.delete(0, orderBy.length());
        }

        StringBuilder queryString = new StringBuilder();
        queryString.append("SELECT model from ").append(entityClass.getSimpleName())
                .append(" model ").append(where).append(" ").append(orderBy);

        Query query = getEntityManager().createQuery(queryString.toString());

        cont = 0;

        for (ParametroTipo parametro : parametros) {
            if (parametro.getValValor() != null && !"".equals(parametro.getValValor())) {

                valueByType  = getValueByType(entityClass, parametro.getIdLlave(), parametro.getValValor());
                
                if (valueByType instanceof Calendar) {
                    query.setParameter("value" + (cont++), "%" + parametro.getValValor() + "%");
                } else {
                    query.setParameter("value" + (cont++), getValueByType(entityClass, parametro.getIdLlave(), parametro.getValValor()));
                }
                
            }
        }

        if (tamPagina != 0 && numPagina != 0) {
            query.setFirstResult((tamPagina * numPagina) - tamPagina);
            query.setMaxResults(tamPagina);

        }

        System.out.println("Query: " + queryString);

        return query.getResultList();

    }

    /**
     * @author cpalacios
     *
     * Convierte el valor al tipo de dato real que se haya detectado. Si ningún
     * tipo de dato disitnto de String se ha detectado entonces se retorna el
     * valor con el tipo de dato String.
     *
     * @param claseAConsultar El nombre de la clase a la cual pertence el campo
     * a evaluar.
     * @param nombreCampo El nombre del campo del cual se quiere determinar el
     * tipo de dato.
     * @param valor El valor a convertir
     * @return El valor convertido al tipo de dato detectado.
     */
    private Object getValueByType(Class claseAConsultar, String nombreCampo, String valor) {

        System.out.println("Va obtener el valor del parametro de la clase: " + claseAConsultar + " con el tipo de dato correcto.");

        Object realValue = valor;
        Field[] fields = claseAConsultar.getDeclaredFields();

        for (Field field : fields) {
            if (field.getName().equals(nombreCampo)) {
                System.out.println("EL tipo de dato en string es: " + field.getType().getName());

                if ("java.util.Calendar".equals(field.getType().getName())) {
                    System.out.println("Es un calendar");

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    realValue = Calendar.getInstance();
                    try {
                        ((Calendar) realValue).setTime(dateFormat.parse(valor));
                    } catch (ParseException e) {
                        System.out.println("La fecha dada no puede convertirse correctamente");
                    }
                } else if ("java.util.Date".equals(field.getType().getName())) {
                    System.out.println("Es un Date");

                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    try {
                        realValue = dateFormat.parse(valor);
                    } catch (ParseException e) {
                        System.out.println("La fecha dada no puede convertirse correctamente");
                    }
                } else if ("java.lang.Integer".equals(field.getType().getName())) {
                    System.out.println("Es un Integer");

                    realValue = new Integer(valor);
                } else if ("java.lang.Long".equals(field.getType().getName())) {
                    System.out.println("Es un Long");

                    realValue = new Long(valor);
                } else if ("java.lang.Float".equals(field.getType().getName())) {
                    System.out.println("Es un Float");

                    realValue = new Float(valor);
                } else if ("java.lang.Double".equals(field.getType().getName())) {
                    System.out.println("Es un Double");

                    realValue = new Double(valor);
                } else if ("java.lang.Boolean".equals(field.getType().getName())) {
                    System.out.println("Es un Boolean");

                    realValue = new Boolean(valor);
                } else {
                    System.out.println("El tipo de dato NO FUE RECONOCIDO para el campo: " + nombreCampo);
                }

                break;
            }
        }

        System.out.println("El tipo de class es: " + realValue.getClass());
        System.out.println("EL valor del campo es: " + realValue);

        return realValue;
    }

}
