package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraAccion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class GestionAdministradoraAccionDao extends AbstractDao<GestionAdministradoraAccion, Long> {

    public GestionAdministradoraAccionDao() {
        super(GestionAdministradoraAccion.class);
    }

    public GestionAdministradoraAccion findByGestionAdministradoraAndAccion(final GestionAdministradora gestionAdministradora, final Accion accion) throws AppException {
        final TypedQuery<GestionAdministradoraAccion> query = getEntityManager().createNamedQuery("gestionAdministradoraAccion.findByGestionAdministradoraAndAccion", GestionAdministradoraAccion.class);
        query.setParameter("idGestionAdministradora", gestionAdministradora.getId());
        query.setParameter("idAccion", accion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
