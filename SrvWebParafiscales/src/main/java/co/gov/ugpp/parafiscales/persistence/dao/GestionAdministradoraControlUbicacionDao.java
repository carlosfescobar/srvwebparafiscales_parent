package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ControlUbicacionEnvio;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraControlUbicacion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class GestionAdministradoraControlUbicacionDao extends AbstractDao<GestionAdministradoraControlUbicacion, Long> {

    public GestionAdministradoraControlUbicacionDao() {
        super(GestionAdministradoraControlUbicacion.class);
    }

    public GestionAdministradoraControlUbicacion findByGestionAdministradoraAndControlUbicacion(final GestionAdministradora gestionAdministradora, final ControlUbicacionEnvio controlUbicacionEnvio) throws AppException {
        final TypedQuery<GestionAdministradoraControlUbicacion> query = getEntityManager().createNamedQuery("gestionAdministradoraControlUbicacion.findByGestionAdministradoraAndControlUbicacion", GestionAdministradoraControlUbicacion.class);
        query.setParameter("idGestionAdministradora", gestionAdministradora.getId());
        query.setParameter("idControlUbicacionEnvio", controlUbicacionEnvio.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    public GestionAdministradoraControlUbicacion findByEstadoUbicacion(final GestionAdministradora gestionAdministradora, final String estado) throws AppException {
        final TypedQuery<GestionAdministradoraControlUbicacion> query = getEntityManager().createNamedQuery("gestionAdministradoraControlUbicacion.findByEstadoUbicacion", GestionAdministradoraControlUbicacion.class);
        query.setParameter("idGestionAdministradora", gestionAdministradora.getId());
        query.setParameter("estado", estado);
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
