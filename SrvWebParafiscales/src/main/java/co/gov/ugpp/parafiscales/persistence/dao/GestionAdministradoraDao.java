package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import javax.ejb.Stateless;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class GestionAdministradoraDao extends AbstractDao<GestionAdministradora, Long> {

    public GestionAdministradoraDao() {
        super(GestionAdministradora.class);
    }
}
