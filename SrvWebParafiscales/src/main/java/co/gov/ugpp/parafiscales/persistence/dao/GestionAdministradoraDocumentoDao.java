package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraDocumento;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class GestionAdministradoraDocumentoDao extends AbstractDao<GestionAdministradoraDocumento, Long> {

    public GestionAdministradoraDocumentoDao() {
        super(GestionAdministradoraDocumento.class);
    }

    public GestionAdministradoraDocumento findByGestionAdministradoraAndDocumento(final GestionAdministradora gestionAdministradora, final DocumentoEcm documentoEcm) throws AppException {
        final TypedQuery<GestionAdministradoraDocumento> query = getEntityManager().createNamedQuery("gestionAdministradoraDocumento.findByGestionAdministradoraAndDocumento", GestionAdministradoraDocumento.class);
        query.setParameter("idGestionAdministradora", gestionAdministradora.getId());
        query.setParameter("idDocumentoEcm", documentoEcm.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
