package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionSolAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradora;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author Everis
 */
@Stateless
public class GestionSolAdministradoraDao extends AbstractDao<GestionSolAdministradora, Long> {

    public GestionSolAdministradoraDao() {
        super(GestionSolAdministradora.class);
    }
    
     public GestionSolAdministradora findByGestionAdministradoraAndSolicitudAdmin(final SolicitudAdministradora solicitudAdministradora, final GestionAdministradora gestionAdministradora) throws AppException {
        final TypedQuery<GestionSolAdministradora> query = getEntityManager().createNamedQuery("gestionSolAdministradora.findByGestionAdministradoraAndSolicitudAdmin", GestionSolAdministradora.class);
        query.setParameter("idSolicitudAdministradora", solicitudAdministradora.getId());
        query.setParameter("idGestionAdministradora", gestionAdministradora.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}