package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.GrupoCiiu;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class GrupoCiiuDao extends AbstractDao<GrupoCiiu, Long> {

    public GrupoCiiuDao() {
        super(GrupoCiiu.class);
    }
}
