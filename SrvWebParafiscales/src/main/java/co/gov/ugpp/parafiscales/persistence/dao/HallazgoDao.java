package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class HallazgoDao extends AbstractDao<Hallazgo, Long> {

    public HallazgoDao() {
        super(Hallazgo.class);
    }

    /**
     * Método que se encarga de ejecutar el query que cruza los cotizantes de
     * pila con nómina
     *
     * @param identificacionTipo la identificación del aportante
     * @param expedienteTipo el expediente asociado a la pila y la nómina
     * @param parametroTipoList el listado de parámetros adicionales a la
     * consulta
     * @param sqlQuery el nombre del query a ejecutarse
     * @return el listado de cotizantes a los cuales hay que registrarles
     * hallazgos
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> buscarCotizantesConHallazgosPilaNomina(final IdentificacionTipo identificacionTipo,
            final ExpedienteTipo expedienteTipo, final List<ParametroTipo> parametroTipoList, final String sqlQuery) throws AppException {

        String vPeriodoInicio = null;
        String vPeriodoFinal = null;

        for (ParametroTipo parametroTipo : parametroTipoList) {
            if (parametroTipo.getIdLlave().equals(Constants.PERIODO_INICIAL)) {
                vPeriodoInicio = parametroTipo.getValValor();
            } else if (parametroTipo.getIdLlave().equals(Constants.PERIODO_FINAL)) {
                vPeriodoFinal = parametroTipo.getValValor();
            }
        }

        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(1, expedienteTipo.getIdNumExpediente());
        query.setParameter(2, identificacionTipo.getValNumeroIdentificacion());
        query.setParameter(3, vPeriodoInicio);
        query.setParameter(4, vPeriodoFinal);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que se encarga de ejecutar el query que cruza los caotizantes de
     * vacaciones
     *
     * @param identificacionTipo la identificación del aportante
     * @param expedienteTipo el expediente asociado a la pila y la nómina
     * @param parametroTipoList el listado de parámetros adicionales a la
     * consulta
     * @param sqlQuery el nombre del query a ejecutarse
     * @return el listado de cotizantes a los cuales hay que registrarles
     * hallazgos
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> buscarCotizantesConHallazgosVacaciones(final IdentificacionTipo identificacionTipo,
            final ExpedienteTipo expedienteTipo, final List<ParametroTipo> parametroTipoList, final String sqlQuery) throws AppException {

        String vPeriodoInicio = null;
        String vPeriodoFinal = null;

        for (ParametroTipo parametroTipo : parametroTipoList) {
            if (parametroTipo.getIdLlave().equals(Constants.PERIODO_INICIAL)) {
                vPeriodoInicio = parametroTipo.getValValor();
            } else if (parametroTipo.getIdLlave().equals(Constants.PERIODO_FINAL)) {
                vPeriodoFinal = parametroTipo.getValValor();
            }
        }

        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(1, expedienteTipo.getIdNumExpediente());
        query.setParameter(2, identificacionTipo.getValNumeroIdentificacion());
        query.setParameter(3, Constants.CONCEPTO_CONTABLE_VACACIONES);
        query.setParameter(4, vPeriodoInicio);
        query.setParameter(5, vPeriodoFinal);
        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que se encarga de buscar la información para crear un nuevo
     * hallazgo
     *
     * @param idExpediente el expediente que relaciona la pila y la nómina
     * @param sqlQuery Nombre Query que se ejecuta.
     * @return la información de pila, nomina y conciliación asociada al
     * expediente
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> buscarInformacionHallazgo(final String idExpediente, final String sqlQuery) throws AppException {

        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(1, idExpediente);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que se encarga de sumar el valor de las cuentas agrupadas
     * encontradas en la tabla concepto contable
     *
     * @param identificacionTipo identificacion con la que se busca el aportante
     * en la base de datos.
     * @param expedienteTipo Objeto asociado a la conciliacionContable
     * @param parametroTipoList Listado de parametros para realizar la consulta
     * @param sqlQuery Nombre Query que se ejecuta.
     * @return Valor de la cuenta y codigo de la Cuenta.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> obtenerSumaNomina(final IdentificacionTipo identificacionTipo, final ExpedienteTipo expedienteTipo,
            final List<ParametroTipo> parametroTipoList, final String sqlQuery) throws AppException {

        String vPeriodoInicio = null;
        String vPeriodoFinal = null;

        for (ParametroTipo parametroTipo : parametroTipoList) {
            if (parametroTipo.getIdLlave().equals(Constants.PERIODO_INICIAL)) {
                vPeriodoInicio = parametroTipo.getValValor();
            } else if (parametroTipo.getIdLlave().equals(Constants.PERIODO_FINAL)) {
                vPeriodoFinal = parametroTipo.getValValor();
            }
        }

        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(1, expedienteTipo.getIdNumExpediente());
        query.setParameter(2, identificacionTipo.getValNumeroIdentificacion());
        query.setParameter(3, vPeriodoInicio);
        query.setParameter(4, vPeriodoFinal);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Método que se encarga de ejecutar el query que cruza el campo VALORNOMINA
     * y VALORNETOMOVIMIENTO de la conciliacionContableDetalle
     *
     * @param expedienteTipo Objeto asociado a la conciliacionContable
     * @param parametroTipoList Listado de parametros para realizar la consulta
     * @param sqlQuery Nombre del Query que se ejecuta
     * @return Diferencia entre el valor de las cuentas
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public List<Object[]> cruzarConciliacionContable(final ExpedienteTipo expedienteTipo,
            final List<ParametroTipo> parametroTipoList, final String sqlQuery) throws AppException {

        String ano = null;

        for (ParametroTipo parametroTipo : parametroTipoList) {
            if (parametroTipo.getIdLlave().equals(Constants.ANO)) {
                ano = parametroTipo.getValValor();
            }
        }
        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(1, expedienteTipo.getIdNumExpediente());
        query.setParameter(2, ano);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
