package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacion;
import java.math.BigDecimal;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class HojaCalculoLiquidacionDao extends AbstractDao<HojaCalculoLiquidacion, BigDecimal> {

    public HojaCalculoLiquidacionDao() {
        super(HojaCalculoLiquidacion.class);
    }

}
