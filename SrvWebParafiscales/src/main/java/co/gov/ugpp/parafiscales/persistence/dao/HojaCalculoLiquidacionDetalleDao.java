package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacionDetalle;
import java.math.BigDecimal;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class HojaCalculoLiquidacionDetalleDao extends AbstractDao<HojaCalculoLiquidacionDetalle, BigDecimal> {

    public HojaCalculoLiquidacionDetalleDao() {
        super(HojaCalculoLiquidacionDetalle.class);
    }

}
