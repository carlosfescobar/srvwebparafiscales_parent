package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaDefiniti;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author vgarcigu
 */
@Stateless
public class InformacionExternaDefinitivaDao extends AbstractDao<InformacionExternaDefiniti, Long> {

    public InformacionExternaDefinitivaDao() {
        super(InformacionExternaDefiniti.class);
    }

    public List<InformacionExternaDefiniti> findByRadicado(final String idRadicado) throws AppException {
        Query query = getEntityManager().createNamedQuery("infoExternaDef.findByRadicado");
        query.setParameter("idRadicado", idRadicado);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
    
    public List<InformacionExternaDefiniti> findByRadicadoFirst(final String idRadicado) throws AppException {
        Query query = getEntityManager().createNamedQuery("infoExternaDef.findByRadicadoFirst");
        query.setMaxResults(1);
        query.setParameter("idRadicado", idRadicado);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
    
    
    
}
