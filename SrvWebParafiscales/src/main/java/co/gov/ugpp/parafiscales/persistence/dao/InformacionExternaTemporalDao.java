package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaTemporal;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author vgarcigu
 */
@Stateless
public class InformacionExternaTemporalDao extends AbstractDao<InformacionExternaTemporal, Long> {

    public InformacionExternaTemporalDao() {
        super(InformacionExternaTemporal.class);
    }

    public List<InformacionExternaTemporal> findPorCriterios(final IdentificacionTipo identificacionTipo,
            final FormatoTipo formatoTipo, final List<String> nombreArchivoList) throws AppException {
        Query query = getEntityManager().createNamedQuery("informacionExternaTemp.findByCriterios");
        query.setParameter("numeroDocumento", identificacionTipo.getValNumeroIdentificacion());
        query.setParameter("tipoDocumento", identificacionTipo.getCodTipoIdentificacion());
        query.setParameter("idFormato", formatoTipo.getIdFormato());
        query.setParameter("idVersion", formatoTipo.getValVersion().longValue());
        query.setParameter("archivos", nombreArchivoList);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    public InformacionExternaTemporal findByArchivoAndEntidadExterna(final ArchivoTipo archivoTipo,
            final IdentificacionTipo identificacionTipo) throws AppException {
        Query query = getEntityManager().createNamedQuery("informacionExternaTemp.findByArchivoAndEntidadExterna");
        query.setParameter("nombreArchivo", archivoTipo.getValNombreArchivo());
        query.setParameter("numeroDocumento", identificacionTipo.getValNumeroIdentificacion());
        query.setParameter("tipoDocumento", identificacionTipo.getCodTipoIdentificacion());
        try {
            return (InformacionExternaTemporal) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
    
}
