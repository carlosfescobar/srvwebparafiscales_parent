package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.InformacionPrueba;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class InformacionPruebaDao extends AbstractDao<InformacionPrueba, Long> {

    public InformacionPruebaDao() {
        super(InformacionPrueba.class);
    }

}
