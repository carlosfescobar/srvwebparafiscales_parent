package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPrueba;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPruebaDocumentos;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class InformacionPruebaDocumentoDao extends AbstractDao<InformacionPruebaDocumentos, Long> {

    public InformacionPruebaDocumentoDao() {
        super(InformacionPruebaDocumentos.class);
    }

    public InformacionPruebaDocumentos findByInformacionPruebaAndDocumento(final InformacionPrueba informacionPrueba, final DocumentoEcm documentoEcm) throws AppException {
        final TypedQuery<InformacionPruebaDocumentos> query = getEntityManager().createNamedQuery("informacionPruebaDocumentosRespuesta.findByInformacionPruebaAndDocumento", InformacionPruebaDocumentos.class);
        query.setParameter("iddocumento", documentoEcm.getId());
        query.setParameter("idinformacionPrueba", informacionPrueba.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
