package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.InstanciaEjecucionValidacion;
import javax.ejb.Stateless;

/**
 *
 * @author jgonzalezt
 */
@Stateless
public class InstanciaEjecucionValidacionDao  extends AbstractDao<InstanciaEjecucionValidacion, Long> {
    
    public InstanciaEjecucionValidacionDao() {
        super(InstanciaEjecucionValidacion.class);
    }    
}
