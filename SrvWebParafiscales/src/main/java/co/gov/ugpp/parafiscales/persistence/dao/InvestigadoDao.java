package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Investigado;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class InvestigadoDao extends AbstractHasPersonaDao<Investigado, Long> {

    public InvestigadoDao() {
        super(Investigado.class);
    }

}
