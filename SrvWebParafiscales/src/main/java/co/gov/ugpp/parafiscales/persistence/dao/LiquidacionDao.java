package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Liquidacion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jdmunoz
 */
@Stateless
public class LiquidacionDao extends AbstractDao<Liquidacion, Long> {

    public LiquidacionDao() {
        super(Liquidacion.class);
    }
    
    
    public Liquidacion findLiquidacionByExpediente(final String idExpediente) throws AppException {

        Query query = getEntityManager().createNamedQuery("liquidacion.findByExpediente");
        query.setParameter("idExpediente", idExpediente);

        try {
            return (Liquidacion) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
}