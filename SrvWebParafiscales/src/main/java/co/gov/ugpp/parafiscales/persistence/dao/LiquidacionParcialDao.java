package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionParcial;
import javax.ejb.Stateless;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class LiquidacionParcialDao extends AbstractDao<LiquidacionParcial, Long> {

    public LiquidacionParcialDao() {
        super(LiquidacionParcial.class);
    }

   

}
