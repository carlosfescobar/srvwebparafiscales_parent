package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.MecanismoSubsidiario;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class MecanismoSubsidiarioDao extends AbstractDao<MecanismoSubsidiario, Long> {

    public MecanismoSubsidiarioDao() {
        super(MecanismoSubsidiario.class);
    }

}
