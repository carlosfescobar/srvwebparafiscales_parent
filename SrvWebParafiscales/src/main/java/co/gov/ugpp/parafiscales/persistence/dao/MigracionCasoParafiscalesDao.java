package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.MigracionCasoParafiscales;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class MigracionCasoParafiscalesDao extends AbstractDao<MigracionCasoParafiscales, Long> {

    public MigracionCasoParafiscalesDao() {
        super(MigracionCasoParafiscales.class);
    }

    /**
     * Método que busca una fiscalización por expediente
     *
     * @param idMigracion el id del caso
     * @param idFila el id de la migración padre
     * @return la asociación entre la migración caso y el padre
     * @throws AppException
     */
    public MigracionCasoParafiscales findByCasoAndMigracionPadre(final Long idMigracion, final String idFila) throws AppException {

        Query query = getEntityManager().createNamedQuery("migracionCasoParafiscales.findByMigracionAndCaso");
        query.setParameter("idMigracion", idMigracion);
        query.setParameter("idFila", idFila);
        try {
            return (MigracionCasoParafiscales) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
