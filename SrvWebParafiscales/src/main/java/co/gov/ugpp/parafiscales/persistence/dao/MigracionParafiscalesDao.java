package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.MigracionParafiscales;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class MigracionParafiscalesDao extends AbstractDao<MigracionParafiscales, Long> {

    public MigracionParafiscalesDao() {
        super(MigracionParafiscales.class);
    }
}
