package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class MunicipioDao extends AbstractDao<Municipio, Long> {

    public MunicipioDao() {
        super(Municipio.class);
    }

    public Municipio findByCodigo(String codigoMunicipio) throws AppException {

        Query query = getEntityManager().createNamedQuery("Municipio.findByCodigo");
        query.setParameter("codigoMunicipio", codigoMunicipio);

        try {
            return (Municipio) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
