package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author franzjr
 */
@Stateless
public class NominaDao extends AbstractDao<Nomina, Long> {

    public NominaDao() {
        super(Nomina.class);
    }

    public List<Nomina> findByExpediente(final String idE) throws AppException {
        final TypedQuery<Nomina> query = getEntityManager().createNamedQuery("Nomina.findByExpediente", Nomina.class);
        query.setParameter("idE", idE);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
