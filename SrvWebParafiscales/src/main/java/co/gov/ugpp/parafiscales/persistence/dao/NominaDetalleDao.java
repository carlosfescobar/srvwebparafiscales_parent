package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.NominaDetalle;
import javax.ejb.Stateless;
import javax.persistence.Query;

/**
 *
 * @author franzjr
 */
@Stateless
public class NominaDetalleDao extends AbstractDao<NominaDetalle, Long> {

    public NominaDetalleDao() {
        super(NominaDetalle.class);
    }

    public void ejecutarUpdate(String query, Long id) throws AppException {
        try {
            Query act = getEntityManager().createQuery(query).setParameter("id", id);
            act.executeUpdate();
        } catch (Exception e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
