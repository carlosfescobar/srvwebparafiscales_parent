package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmunocab
 */
@Stateless
public class NotificacionDao extends AbstractDao<Notificacion, Long> {

    public NotificacionDao() {
        super(Notificacion.class);
    }

    public List<Notificacion> findByEstadosNotificacion(final List<String> estadosNotificacion) throws AppException {

        Query query = getEntityManager().createNamedQuery("notificacion.findyByEstados");
        query.setParameter("estadosNotificacion", estadosNotificacion);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }

    }

}
