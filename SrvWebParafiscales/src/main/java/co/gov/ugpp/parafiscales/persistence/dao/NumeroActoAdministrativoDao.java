package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.NumeroActoAdministrativo;
import co.gov.ugpp.parafiscales.util.Constants;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author rpadilla
 */
@Stateless
public class NumeroActoAdministrativoDao extends AbstractDao<NumeroActoAdministrativo, String> {

    public NumeroActoAdministrativoDao() {
        super(NumeroActoAdministrativo.class);
    }

    public List<NumeroActoAdministrativo> findListByCodTipoActoAdministrativo(final String codTipoActoAdministrativo) throws AppException {
        try {
            final TypedQuery<NumeroActoAdministrativo> query = this.getEntityManager()
                    .createNamedQuery("NumeroActoAdministrativo.findListByCodTipoActoAdministrativo", NumeroActoAdministrativo.class);
            query.setParameter("codTipoActoAdministrativo", codTipoActoAdministrativo);
            return query.getResultList();
        } catch (PersistenceException ex) {
            throw new AppException("Error de persistencia al consultar los numeroActoAdministrativo", ex);
        }
    }
    
    public NumeroActoAdministrativo findConsecutivoByTipoAndAnio(final String prefijoActo, final int anio) throws AppException {
        try {
            Query query = getEntityManager().createNamedQuery("NumeroActoAdministrativo.findConsecutivoByTipoAndAnio");
            query.setParameter("prefijo", prefijoActo);
            query.setParameter("anio", anio);
            query.setMaxResults(Constants.MAX_RESULTS_UNO);
            return (NumeroActoAdministrativo) query.getSingleResult();
        } catch (NoResultException nre){
          return null;  
        } catch (PersistenceException ex) {
            throw new AppException("Error de persistencia al consultar los numeroActoAdministrativo", ex);
        }
    }
}

