package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Parametro;
import co.gov.ugpp.parafiscales.util.Constants;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ParametroDao extends AbstractDao<Parametro, Long> {

    public ParametroDao() {
        super(Parametro.class);
    }

    public List<Parametro> findByFecha() throws AppException {

        Query query = getEntityManager().createNamedQuery("parametro.findByFecha");

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }

    }

    /**
     * Método que ejecuta la consulta de KPI
     *
     * @param fecInicio la fecha de inicio de la consulta
     * @param fecFin la fecha fin de la consulta
     * @param parametroTipoList listado de parametros de consulta
     * @param sqlQuery el nombre del query nativo a ejecutar
     * @return listado de objetos encontrados
     * @throws AppException
     */
    public List<Object[]> ejecutarConsultaKPI(final Calendar fecInicio, final Calendar fecFin,
            final List<ParametroTipo> parametroTipoList, final String sqlQuery) throws AppException {

        Query query = getEntityManager().createNamedQuery(sqlQuery);
        query.setParameter(Constants.FECHA_INICIO, fecInicio);
        query.setParameter(Constants.FECHA_FIN, fecFin);
        if (parametroTipoList != null
                && !parametroTipoList.isEmpty()) {
            for (ParametroTipo parametroTipo : parametroTipoList) {
                query.setParameter(Integer.valueOf(parametroTipo.getIdLlave()), parametroTipo.getValValor());
            }
        }
        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
