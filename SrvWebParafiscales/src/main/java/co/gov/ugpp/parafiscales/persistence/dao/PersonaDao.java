package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author jsaenzar
 */
@Stateless
public class PersonaDao extends AbstractHasPersonaDao<Persona, Long> {

    public PersonaDao() {
        super(Persona.class);
    }

    @Override
    public Persona findByIdentificacion(final Identificacion identificacion) throws AppException {
        final TypedQuery<Persona> query = getEntityManager().createNamedQuery("persona.findByIdentificacion", Persona.class);
        query.setParameter("numero", identificacion.getValNumeroIdentificacion());
        query.setParameter("tipo", identificacion.getCodTipoIdentificacion());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
