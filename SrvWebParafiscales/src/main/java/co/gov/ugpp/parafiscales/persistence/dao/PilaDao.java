package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Pila;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class PilaDao extends AbstractDao<Pila, Long> {

    public PilaDao() {
        super(Pila.class);
    }

}
