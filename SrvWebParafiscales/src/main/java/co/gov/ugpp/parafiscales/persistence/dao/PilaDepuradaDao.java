package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.PilaDepurada;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class PilaDepuradaDao extends AbstractDao<PilaDepurada, Long> {

    public PilaDepuradaDao() {
        super(PilaDepurada.class);
    }

}
