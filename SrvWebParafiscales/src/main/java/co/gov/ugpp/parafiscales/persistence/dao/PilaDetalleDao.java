package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.PilaDetalle;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class PilaDetalleDao extends AbstractDao<PilaDetalle, Long> {

    public PilaDetalleDao() {
        super(PilaDetalle.class);
    }

}
