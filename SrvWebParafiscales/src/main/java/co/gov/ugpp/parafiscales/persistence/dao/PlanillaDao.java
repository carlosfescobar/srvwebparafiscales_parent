package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Planilla;
import javax.ejb.Stateless;

/**
 *
 * @author rpadilla
 */
@Stateless
public class PlanillaDao extends AbstractDao<Planilla, String> {

    public PlanillaDao() {
        super(Planilla.class);
    }

}
