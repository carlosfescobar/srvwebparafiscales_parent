package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.PrefijoNumeroActo;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class PrefijoNumeroActoDao extends AbstractDao<PrefijoNumeroActo, Long> {

    public PrefijoNumeroActoDao() {
        super(PrefijoNumeroActo.class);
    }

}
