package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Prueba;
import javax.ejb.Stateless;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class PruebaDao extends AbstractDao<Prueba, Long> {

    public PruebaDao() {
        super(Prueba.class);
    }
}