package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RadicadoTrazabilidad;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import java.util.Calendar;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class RadicadoTrazabilidadDao extends AbstractDao<RadicadoTrazabilidad, Long> {

    public RadicadoTrazabilidadDao() {
        super(RadicadoTrazabilidad.class);
    }

    public RadicadoTrazabilidad findByRadicadonAndTrazabilidad(final TrazabilidadActoAdministrativo trazabilidadActoAdministrativo, final String idRadicado, final Calendar fechaRadicado, final ValorDominio codRadicado) throws AppException {
        final TypedQuery<RadicadoTrazabilidad> query = getEntityManager().createNamedQuery("radicadoTrazabilidad.findByRadicadonAndTrazabilidad", RadicadoTrazabilidad.class);
        query.setParameter("idtrazabilidadRadicacionActo", trazabilidadActoAdministrativo.getId());
        query.setParameter("idRadicado", idRadicado);
        query.setParameter("fechaRadicado", fechaRadicado);
        query.setParameter("idCodRadicado", codRadicado.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
