package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoCausalFallo;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class RecursoCausalFalloDao extends AbstractDao<RecursoCausalFallo, Long> {

    public RecursoCausalFalloDao() {
        super(RecursoCausalFallo.class);
    }

    public RecursoCausalFallo findByRecursoAndCodCausaFallo(final RecursoReconsideracion reconsideracion, final ValorDominio codCausalesFallo) throws AppException {
        final TypedQuery<RecursoCausalFallo> query = getEntityManager().createNamedQuery("recursohascausalfallo.findByRecursoAndCodCausaFallo", RecursoCausalFallo.class);
        query.setParameter("idcodCausalesFallo", codCausalesFallo.getId());
        query.setParameter("idRecursoReconsideracion", reconsideracion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
