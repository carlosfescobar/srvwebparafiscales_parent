package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoReposicion;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author lquintero
 */
@Stateless
public class RecursoDocAnexoReposicionDao extends AbstractDao<RecursoAnexoReposicion, Long> {

    public RecursoDocAnexoReposicionDao() {
        super(RecursoAnexoReposicion.class);
    }

    public RecursoAnexoReposicion findByRecursoAndCodDocAnexoReposion(final RecursoReconsideracion recursoDocAnexoReposicion, final ValorDominio codDocumentosAnexosRecursoReposicion) throws AppException {
        final TypedQuery<RecursoAnexoReposicion> query = getEntityManager().createNamedQuery("recursohasdocanexoreposi.findByRecursoAndCodDocumAnexReposi", RecursoAnexoReposicion.class);
        query.setParameter("idcodDocumentosAnexosRecursoReposicion", codDocumentosAnexosRecursoReposicion.getId());
        query.setParameter("idRecursoReconsideracion", recursoDocAnexoReposicion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
