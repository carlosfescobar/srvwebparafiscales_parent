package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoRequisitos;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author lquintero
 */
@Stateless
public class RecursoDocAnexoRequisitosDao extends AbstractDao<RecursoAnexoRequisitos, Long> {

    public RecursoDocAnexoRequisitosDao() {
        super(RecursoAnexoRequisitos.class);
    }

    public RecursoAnexoRequisitos findByRecursoAndCodDocAnexoRequisitos(final RecursoReconsideracion recursoDocAnexoReposicion, final ValorDominio codDocumentosAnexosRequisitos) throws AppException {
        final TypedQuery<RecursoAnexoRequisitos> query = getEntityManager().createNamedQuery("recursohasdocanexorequ.findByRecursoAndCodDocumAnexRequis", RecursoAnexoRequisitos.class);
        query.setParameter("idcodDocumentosAnexosRequisitos", codDocumentosAnexosRequisitos.getId());
        query.setParameter("idRecursoReconsideracion", recursoDocAnexoReposicion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
