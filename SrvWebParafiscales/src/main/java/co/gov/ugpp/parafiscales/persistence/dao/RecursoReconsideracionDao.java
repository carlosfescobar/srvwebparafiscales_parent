package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import javax.ejb.Stateless;


@Stateless
public class RecursoReconsideracionDao extends AbstractDao<RecursoReconsideracion, Long> {

    public RecursoReconsideracionDao() {
        super(RecursoReconsideracion.class);
    }

}
