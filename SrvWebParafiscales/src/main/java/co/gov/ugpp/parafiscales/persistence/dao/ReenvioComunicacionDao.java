package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ReenvioComunicacion;
import javax.ejb.Stateless;

/**
 *
 * @author yrinconh
 */
@Stateless
public class ReenvioComunicacionDao extends AbstractDao<ReenvioComunicacion, String> {

    public ReenvioComunicacionDao() {
        super(ReenvioComunicacion.class);
    }
}
