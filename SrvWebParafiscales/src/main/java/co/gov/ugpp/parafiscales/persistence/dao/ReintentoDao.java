package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Reintento;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ReintentoDao extends AbstractDao<Reintento, Long> {
    
    public ReintentoDao() {
        super(Reintento.class);
    }
}
