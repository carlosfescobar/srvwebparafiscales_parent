package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradora;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaAdministradoraDao extends AbstractDao<RespuestaAdministradora, Long> {

    public RespuestaAdministradoraDao() {
        super(RespuestaAdministradora.class);
    }

}
