package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradoraDecisionCaso;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaAdministradoraDecisionCasoDao extends AbstractDao<RespuestaAdministradoraDecisionCaso, Long> {

    public RespuestaAdministradoraDecisionCasoDao() {
        super(RespuestaAdministradoraDecisionCaso.class);
    }

    public RespuestaAdministradoraDecisionCaso findByRespuestaAdminAndCodDecisionCaso(final RespuestaAdministradora respuestaAdministradora, final ValorDominio codDecisionCaso) throws AppException {
        final TypedQuery<RespuestaAdministradoraDecisionCaso> query = getEntityManager().createNamedQuery("respuestaAdmDecisionCaso.findByRespuestaAdminAndCodDecisionCaso", RespuestaAdministradoraDecisionCaso.class);
        query.setParameter("idcodDecisionCaso", codDecisionCaso.getId());
        query.setParameter("idRespuestaAdministradora", respuestaAdministradora.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
