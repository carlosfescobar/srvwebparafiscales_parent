package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPago;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaPagoDao extends AbstractDao<RespuestaPago, Long> {

    public RespuestaPagoDao() {
        super(RespuestaPago.class);
    }

}
