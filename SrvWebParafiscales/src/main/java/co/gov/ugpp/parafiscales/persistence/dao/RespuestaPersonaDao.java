package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPersona;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaPersonaDao extends AbstractDao<RespuestaPersona, Long> {

    public RespuestaPersonaDao() {
        super(RespuestaPersona.class);
    }

}
