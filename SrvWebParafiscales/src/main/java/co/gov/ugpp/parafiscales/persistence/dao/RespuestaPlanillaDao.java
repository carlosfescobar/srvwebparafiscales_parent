
package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPlanilla;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaPlanillaDao extends AbstractDao<RespuestaPlanilla, Long> {

    public RespuestaPlanillaDao() {
        super(RespuestaPlanilla.class);
    }
}
