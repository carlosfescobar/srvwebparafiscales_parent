package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoInformacion;
import javax.ejb.Stateless;


@Stateless
public class RespuestaRequerimientoInformacionDao extends AbstractDao<RespuestaRequerimientoInformacion, Long> {

    public RespuestaRequerimientoInformacionDao() {
        super(RespuestaRequerimientoInformacion.class);
    }

}
