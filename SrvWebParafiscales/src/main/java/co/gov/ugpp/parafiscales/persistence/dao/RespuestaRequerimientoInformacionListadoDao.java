package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoListaChequeo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaRequerimientoInformacionListadoDao extends AbstractDao<RespuestaRequerimientoListaChequeo, Long> {

    public RespuestaRequerimientoInformacionListadoDao() {
        super(RespuestaRequerimientoListaChequeo.class);
    }

    public RespuestaRequerimientoListaChequeo findByRespuestaAndCodListadoChequeo(final RespuestaRequerimientoInformacion respuestaRequerimientoInformacion, final ValorDominio codListadoChequeo) throws AppException {
        final TypedQuery<RespuestaRequerimientoListaChequeo> query = getEntityManager().createNamedQuery("respuestaRequerimientoListadoChequeo.findByRespuestanAndCodListadoChequeo", RespuestaRequerimientoListaChequeo.class);
        query.setParameter("idcodListadoChequeo", codListadoChequeo.getId());
        query.setParameter("idrespuestaRequerimientoInformacion", respuestaRequerimientoInformacion.getId());
        
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}