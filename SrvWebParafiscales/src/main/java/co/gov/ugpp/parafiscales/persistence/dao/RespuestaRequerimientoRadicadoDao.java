package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoInformacion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class RespuestaRequerimientoRadicadoDao extends AbstractDao<RespuestaRequerimientoInformacion, Long> {

    public RespuestaRequerimientoRadicadoDao() {
        super(RespuestaRequerimientoInformacion.class);
    }

    public RespuestaRequerimientoInformacion findByRespuestanAndIdRadicadoEntrada(final String radicadoEntrada) throws AppException {
        Query query = getEntityManager().createNamedQuery("respuestaRequerimiento.findByRespuestanAndIdRadicadoEntrada");
        query.setParameter("idRadicadoEntrada", radicadoEntrada);

        try {
            return (RespuestaRequerimientoInformacion) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }

    }
}
