package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaSolicitud;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class RespuestaSolicitudDao extends AbstractDao<RespuestaSolicitud, Long> {

    public RespuestaSolicitudDao() {
        super(RespuestaSolicitud.class);
    }

}
