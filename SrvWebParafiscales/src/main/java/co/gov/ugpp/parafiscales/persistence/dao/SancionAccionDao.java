package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.Sancion;
import co.gov.ugpp.parafiscales.persistence.entity.SancionHasAccion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class SancionAccionDao extends AbstractDao<SancionHasAccion, Long> {

    public SancionAccionDao() {
        super(SancionHasAccion.class);
    }

    public SancionHasAccion findBySancionAndBusqueda(final Sancion sancion, final Accion accion) throws AppException {
        final TypedQuery<SancionHasAccion> query = getEntityManager().createNamedQuery("sancionHasbusqueda.findBySancionAndBusqueda", SancionHasAccion.class);
        query.setParameter("idaccion", accion.getId());
        query.setParameter("idsancion", sancion.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
