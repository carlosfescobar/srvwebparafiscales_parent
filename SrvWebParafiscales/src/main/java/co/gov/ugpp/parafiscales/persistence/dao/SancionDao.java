package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Sancion;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class SancionDao extends AbstractDao<Sancion, Long> {

    public SancionDao() {
        super(Sancion.class);
    }

}
