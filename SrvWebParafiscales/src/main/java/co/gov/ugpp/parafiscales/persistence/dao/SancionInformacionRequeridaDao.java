package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SancionInformacionRequerida;
import java.io.Serializable;
import javax.ejb.Stateless;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class SancionInformacionRequeridaDao extends AbstractDao<SancionInformacionRequerida, Long> {

    public SancionInformacionRequeridaDao() {
        super(SancionInformacionRequerida.class);
    }

}
