package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SancionPruebaRequerida;
import javax.ejb.Stateless;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class SancionPruebaRequeridaDao extends AbstractDao<SancionPruebaRequerida, Long> {

    public SancionPruebaRequeridaDao() {
        super(SancionPruebaRequerida.class);
    }

}
