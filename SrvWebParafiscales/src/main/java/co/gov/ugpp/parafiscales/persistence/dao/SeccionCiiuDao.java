package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SeccionCiiu;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class SeccionCiiuDao extends AbstractDao<SeccionCiiu, String> {

    public SeccionCiiuDao() {
        super(SeccionCiiu.class);
    }
}
