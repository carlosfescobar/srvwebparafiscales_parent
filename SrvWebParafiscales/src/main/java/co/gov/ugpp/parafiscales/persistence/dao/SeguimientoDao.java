package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Seguimiento;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class SeguimientoDao extends AbstractDao<Seguimiento, Long> {

    public SeguimientoDao() {
        super(Seguimiento.class);
    }

}
