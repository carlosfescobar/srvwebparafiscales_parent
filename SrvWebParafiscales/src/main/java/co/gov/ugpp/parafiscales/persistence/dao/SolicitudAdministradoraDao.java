package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradora;
import javax.ejb.Stateless;

/**
 *
 * @author Everis
 */
@Stateless
public class SolicitudAdministradoraDao extends AbstractDao<SolicitudAdministradora, Long> {

    public SolicitudAdministradoraDao() {
        super(SolicitudAdministradora.class);
    }
}
