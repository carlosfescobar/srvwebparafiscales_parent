package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.enums.TipoSolicitudEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.util.Util;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author rpadilla
 */
@Stateless
public class SolicitudDao extends AbstractDao<Solicitud, Long> {

    public SolicitudDao() {
        super(Solicitud.class);
    }

    /**
     * Método que busca las solicitudes para una entidadExterna
     * @param entidadExterna la entidad externa relacionada a la solicitud
     * @param cantSolicitudes la cantidad de solicitudes a constular para la entidad externa
     * @return listado de solicitudes asociados a la entidad externa
     * @throws AppException 
     */
    public List<Solicitud> findSolicitudByEntidadExternaId(final EntidadExterna entidadExterna,
            final Integer cantSolicitudes) throws AppException {

        Query query = getEntityManager().createNamedQuery("solicitud.findByEntidadExterna");
        query.setParameter("idEntidad", entidadExterna.getId());
        query.setParameter("estadosObviar",Util.obtenerSolicitudesObviarEfectivdad() );
        query.setMaxResults(cantSolicitudes);

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    /**
     * Mètodo que consulta las solicitudes por id y por tipo de solicitud
     * 
     * @param idList listado de identificadores de solicitud
     * @param tipoSolicitudEnum tipo de solicitud
     * @return el listado de solicitudes encontradas por id y por tipo
     * @throws AppException
     */
    public List<Solicitud> findByIdListAndTipo(final List<Long> idList, final TipoSolicitudEnum tipoSolicitudEnum) throws AppException {
        
        Query query = getEntityManager().createNamedQuery("solicitud.findByIdListAndTipo");
        query.setParameter("idList", idList);
        query.setParameter("tipoSolicitud", tipoSolicitudEnum.getCode());

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
