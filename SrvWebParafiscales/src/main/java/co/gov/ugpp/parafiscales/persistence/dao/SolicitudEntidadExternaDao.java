package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudEntidadExterna;
import javax.ejb.Stateless;

/**
 *
 * @author vladimir.garcia
 */
@Stateless
public class SolicitudEntidadExternaDao extends AbstractDao<SolicitudEntidadExterna, Long> {

    public SolicitudEntidadExternaDao() {
        super(SolicitudEntidadExterna.class);
    }

}
