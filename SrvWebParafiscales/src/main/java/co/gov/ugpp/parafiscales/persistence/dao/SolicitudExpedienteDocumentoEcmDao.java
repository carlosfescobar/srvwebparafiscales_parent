package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmunocab
 */
@Stateless
public class SolicitudExpedienteDocumentoEcmDao extends AbstractDao<SolicitudExpedienteDocumentoEcm, Long> {

    public SolicitudExpedienteDocumentoEcmDao() {
        super(SolicitudExpedienteDocumentoEcm.class);
    }

    public SolicitudExpedienteDocumentoEcm findBySolicitudAndExpedienteDocumento(final Solicitud solicitud,
            final ExpedienteDocumentoEcm expedienteDocumentoEcm) throws AppException {

        Query query = getEntityManager().createNamedQuery("solicitudExpedienteDocumento.findByExpedienteSolicitudAndDocumento");
        query.setParameter("idDocumento", expedienteDocumentoEcm.getDocumentoEcm().getId());
        query.setParameter("idExpediente", expedienteDocumentoEcm.getIdExpediente());
        query.setParameter("idSolicitud", solicitud.getId());

        try {
            return (SolicitudExpedienteDocumentoEcm) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    public List<SolicitudExpedienteDocumentoEcm> findByCodOrigenAndSolicitud(final OrigenDocumentoEnum codOrigen,
            final Solicitud solicitud) throws AppException {

        Query query = getEntityManager().createNamedQuery("solicitudExpedienteDocumento.findByCodOrigen");
        query.setParameter("idCodOrigen", codOrigen.getCode());
        query.setParameter("idSolicitud", solicitud.getId());

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
