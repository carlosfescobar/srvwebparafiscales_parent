package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudFuenteInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author rpadilla
 */
@Stateless
public class SolicitudFuentesInfoDao extends AbstractDao<SolicitudFuenteInformacion, Long> {

    public SolicitudFuentesInfoDao() {
        super(SolicitudFuenteInformacion.class);
    }

    public SolicitudFuenteInformacion findBySolicitudAndFuenteInfo(final Solicitud solicitud, final ValorDominio fuenteInformacion) throws AppException {
        final TypedQuery<SolicitudFuenteInformacion> query = getEntityManager().createNamedQuery("solicitudFuentesInfo.findBySolicitudAndFuenteInfo", SolicitudFuenteInformacion.class);
        query.setParameter("idFuenteInformacion", fuenteInformacion.getId());
        query.setParameter("idSolicitud", solicitud.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
