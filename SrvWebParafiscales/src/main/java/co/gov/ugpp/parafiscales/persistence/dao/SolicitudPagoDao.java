package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPago;
import javax.ejb.Stateless;

/**
 *
 * @author rpadilla
 */
@Stateless
public class SolicitudPagoDao extends AbstractDao<SolicitudPago, Long> {

    public SolicitudPagoDao() {
        super(SolicitudPago.class);
    } 

}
