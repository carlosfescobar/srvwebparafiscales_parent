package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;

import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.persistence.entity.SubsistemaAdministradora;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class SubsistemaAdministradoraDao extends AbstractDao<SubsistemaAdministradora, Long> {

    public SubsistemaAdministradoraDao() {
        super(SubsistemaAdministradora.class);
    }

    public SubsistemaAdministradora findBysubsistemaAndentidadVigilanciaControl(final EntidadExterna entidadExterna, final Subsistema subsistema) throws AppException {
        final TypedQuery<SubsistemaAdministradora> query = getEntityManager().createNamedQuery("subsistemaAdministradora.findBySubsistemaAndEntidadexterna", SubsistemaAdministradora.class);
        query.setParameter("idSubsistema", subsistema.getId());
        query.setParameter("idEntidadExterna", entidadExterna.getId());
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
