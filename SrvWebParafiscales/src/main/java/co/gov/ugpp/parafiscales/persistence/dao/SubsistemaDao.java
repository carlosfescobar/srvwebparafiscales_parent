package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import javax.ejb.Stateless;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class SubsistemaDao extends AbstractDao<Subsistema, String> {

    public SubsistemaDao() {
        super(Subsistema.class);
    }
}
