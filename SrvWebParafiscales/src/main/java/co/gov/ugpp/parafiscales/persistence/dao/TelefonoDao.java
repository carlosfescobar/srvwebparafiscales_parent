package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class TelefonoDao extends AbstractDao<Telefono, Long> {

    public TelefonoDao() {
        super(Telefono.class);
    }
    
    
    public Long findTelefono(TelefonoTipo telefonoTipo,final Long idContacto, final ValorDominio codFuente) throws AppException {

        Query query = getEntityManager().createNamedQuery("telefono.findTelefono");
        query.setParameter("valNumero", telefonoTipo.getValNumeroTelefono());
        query.setParameter("codTipoTelefono", telefonoTipo.getCodTipoTelefono());
        query.setParameter("idContacto", idContacto);
        query.setParameter("codFuente", codFuente.getId());
        
        try {
            return (Long) query.getSingleResult();
        } catch (NoResultException nre) {
            return 0L;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
    
    
 
}
