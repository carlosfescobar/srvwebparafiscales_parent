package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.TipoIdentificacion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.TypedQuery;

/**
 * 
 * @author franzjr
 */
@Stateless
public class TipoIdentificacionDao extends AbstractDao<TipoIdentificacion, Long> {

    public TipoIdentificacionDao() {
        super(TipoIdentificacion.class);
    }
    
    public TipoIdentificacion findByDescripcion(final String descripcion) throws AppException {
        final TypedQuery<TipoIdentificacion> query = getEntityManager().createNamedQuery(TipoIdentificacion.NQ_FIND_BY_DESCRIPCION, TipoIdentificacion.class);
        query.setParameter("descripcion", descripcion);
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    
    public TipoIdentificacion findBySigla(final String sigla) throws AppException {
        final TypedQuery<TipoIdentificacion> query = getEntityManager().createNamedQuery(TipoIdentificacion.NQ_FIND_BY_SIGLA, TipoIdentificacion.class);
        query.setParameter("sigla", sigla);
        try {
            return query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
    
    
}
