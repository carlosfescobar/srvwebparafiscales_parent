package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.Traslado;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class TrasladoDao extends AbstractHasPersonaDao<Traslado, Long> {

    public TrasladoDao() {
        super(Traslado.class);
    }

}
