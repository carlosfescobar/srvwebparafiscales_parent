package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class TrazabilidadActoAdministrativoDao extends AbstractDao<TrazabilidadActoAdministrativo, String> {

    public TrazabilidadActoAdministrativoDao() {
        super(TrazabilidadActoAdministrativo.class);
    }

}
