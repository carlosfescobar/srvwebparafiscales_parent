package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadRadicadoSalida;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author zrodrigu
 */
@Stateless
public class TrazabilidadRadicadoSalidaDao extends AbstractDao<TrazabilidadRadicadoSalida, String> {
    
      public TrazabilidadRadicadoSalida findByRadicadoSalida(final String numRadicadoSalida) throws AppException {

        Query query = getEntityManager().createNamedQuery("trazabilidadRadicadoSalida.findByRadicadoSalida");
        query.setParameter("numRadicadoSalida", numRadicadoSalida);

        try {
            return (TrazabilidadRadicadoSalida) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    public TrazabilidadRadicadoSalidaDao() {
        super(TrazabilidadRadicadoSalida.class);
    }

}
