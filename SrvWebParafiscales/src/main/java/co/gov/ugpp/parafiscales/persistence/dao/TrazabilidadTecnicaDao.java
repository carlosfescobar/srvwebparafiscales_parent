package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadTecnicaProceso;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class TrazabilidadTecnicaDao extends AbstractDao<TrazabilidadTecnicaProceso, Long> {

    public TrazabilidadTecnicaDao() {
        super(TrazabilidadTecnicaProceso.class);
    }

}
