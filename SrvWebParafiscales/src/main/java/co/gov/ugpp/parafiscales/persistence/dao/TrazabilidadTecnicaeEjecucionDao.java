package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadTecnicaEjecucion;
import javax.ejb.Stateless;

/**
 *
 * @author jmunocab
 */
@Stateless
public class TrazabilidadTecnicaeEjecucionDao extends AbstractDao<TrazabilidadTecnicaEjecucion, Long> {

    public TrazabilidadTecnicaeEjecucionDao() {
        super(TrazabilidadTecnicaEjecucion.class);
    }

}
