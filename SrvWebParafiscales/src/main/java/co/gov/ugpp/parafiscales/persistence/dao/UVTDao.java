package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.UVT;
import java.util.Calendar;
import java.util.Date;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class UVTDao extends AbstractDao<UVT, Long> {

    public UVTDao() {
        super(UVT.class);
    }

    public UVT findByUVTtActual() throws AppException {
        Query query = getEntityManager().createNamedQuery("UVT.findByfecActual");
       query.setParameter("sysdate", new Date());
        try {
            return (UVT) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
}
