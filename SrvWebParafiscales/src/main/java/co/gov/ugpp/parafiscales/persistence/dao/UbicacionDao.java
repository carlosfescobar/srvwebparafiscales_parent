package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class UbicacionDao extends AbstractDao<Ubicacion, Long> {

    public UbicacionDao() {
        super(Ubicacion.class);
    }
    
    
    public Long findUbicacion(final UbicacionPersonaTipo ubicacion,final Long idContacto) throws AppException {

        Query query = getEntityManager().createNamedQuery("ubicacion.findUbicacion");
        query.setParameter("idMunicipio", ubicacion.getMunicipio().getCodMunicipio());
        query.setParameter("codTipoDireccion", ubicacion.getCodTipoDireccion());
        query.setParameter("valDireccion", ubicacion.getValDireccion());
        query.setParameter("idContacto", idContacto);
        
        
        try {
            return (Long) query.getSingleResult();
        } catch (NoResultException nre) {
            return 0L;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }
    
}
