package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionCreacionDocumento;
import javax.ejb.Stateless;

/**
 *
 * @author zrodriguez
 */
@Stateless
public class ValidacionCreacionDocumentoDao extends AbstractDao<ValidacionCreacionDocumento, Long> {

    public ValidacionCreacionDocumentoDao() {
        super(ValidacionCreacionDocumento.class);
    }
}
