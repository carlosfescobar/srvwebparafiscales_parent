package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionDocumento;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ValidacionDocumentoDao extends AbstractDao<ValidacionDocumento, Long> {

    public ValidacionDocumentoDao() {
        super(ValidacionDocumento.class);
    }
}
