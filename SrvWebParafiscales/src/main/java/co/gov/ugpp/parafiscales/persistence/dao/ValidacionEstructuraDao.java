package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionEstructura;
import javax.ejb.Stateless;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ValidacionEstructuraDao extends AbstractDao<ValidacionEstructura, Long> {

    public ValidacionEstructuraDao() {
        super(ValidacionEstructura.class);
    }
}
