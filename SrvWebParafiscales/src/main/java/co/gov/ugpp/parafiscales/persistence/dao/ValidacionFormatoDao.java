package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionFormato;
import co.gov.ugpp.parafiscales.util.Constants;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jdmunoz, jgonzalezt
 */
@Stateless
public class ValidacionFormatoDao extends AbstractDao<ValidacionFormato, Long> {

    public ValidacionFormatoDao() {
        super(ValidacionFormato.class);
    }

    public ValidacionFormato findByFormato(final FormatoPK pk) throws AppException {
        Query query = getEntityManager().createNamedQuery("validacionFormato.findByFormatoPK");
        query.setParameter("idFormato", pk.getIdFormato());
        query.setParameter("idVersion", pk.getIdVersion());
        query.setParameter("fecActual", new Date());
        query.setMaxResults(Constants.MAX_RESULTS_UNO);

        try {
            return (ValidacionFormato) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

    public List<ValidacionFormato> Validaciones(Long idFormato, Long numVersion) throws AppException {
        try {
            Query query = getEntityManager().createNamedQuery("validacionFormato.validaciones");
            query.setParameter("idFormato", idFormato);
            query.setParameter("idVersion", numVersion);
            query.setParameter("fecActual", new Date());
            return query.getResultList();
        } catch (IllegalStateException e) {
            throw new AppException("IllegalStateException de Persistencia", e);
        } catch (NoResultException e) {
            return null;
        } catch (PersistenceException e) {
            throw new AppException("Exception de Persistencia", e);
        }
    }
}
