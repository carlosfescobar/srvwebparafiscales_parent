package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.enums.DominioEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import java.util.Collections;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceException;
import javax.persistence.Query;

/**
 *
 * @author jmuncab
 */
@Stateless
public class ValorDominioDao extends AbstractDao<ValorDominio, String> {

    public ValorDominioDao() {
        super(ValorDominio.class);
    }

    public List<ValorDominio> findByDominio(final DominioEnum dominioEnum) throws AppException {

        Query query = getEntityManager().createNamedQuery("valorDominio.findByCodDominio");
        query.setParameter("codDominio", dominioEnum.getCode());

        try {
            return query.getResultList();
        } catch (NoResultException nre) {
            return Collections.EMPTY_LIST;
        } catch (PersistenceException e) {
            throw new AppException("Excepción de Persistencia", e);
        }
    }

}
