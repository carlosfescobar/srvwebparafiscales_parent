package co.gov.ugpp.parafiscales.persistence.dao;

import co.gov.ugpp.parafiscales.persistence.entity.ValorFormula;
import javax.ejb.Stateless;

/**
 * 
 * @author franzjr
 */
@Stateless
public class ValorFormulaDao extends AbstractDao<ValorFormula, Long> {

    public ValorFormulaDao() {
        super(ValorFormula.class);
    }

}
