package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.parafiscales.util.DateUtil;
import java.io.Serializable;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 * @param <PK> El tipo del Primary Key
 */
@MappedSuperclass
public abstract class AbstractEntity<PK extends Serializable> implements IEntity<PK> {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USUARIO_CREACION", updatable = false)
    protected String idUsuarioCreacion;

    @Column(name = "ID_USUARIO_MODIFICACION")
    protected String idUsuarioModificacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FEC_CREACION", updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaCreacion;

    @Column(name = "FEC_MODIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaModificacion;

    @Transient
    private boolean useOnlySpecifiedFields;

    public String getIdUsuarioCreacion() {
        return idUsuarioCreacion;
    }

    public void setIdUsuarioCreacion(String idUsuarioCreacion) {
        this.idUsuarioCreacion = idUsuarioCreacion;
    }

    public String getIdUsuarioModificacion() {
        return idUsuarioModificacion;
    }

    public void setIdUsuarioModificacion(String idUsuarioModificacion) {
        this.idUsuarioModificacion = idUsuarioModificacion;
    }

    public Calendar getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Calendar fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Calendar getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Calendar fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public boolean isUseOnlySpecifiedFields() {
        return useOnlySpecifiedFields;
    }

    public void setUseOnlySpecifiedFields(boolean useOnlySpecifiedFields) {
        this.useOnlySpecifiedFields = useOnlySpecifiedFields;
    }

    @PrePersist
    public void prePersist() {
        this.setFechaCreacion(DateUtil.currentCalendar());
    }

    @PreUpdate
    public void preUpdate() {
        this.setFechaModificacion(DateUtil.currentCalendar());
    }

}
