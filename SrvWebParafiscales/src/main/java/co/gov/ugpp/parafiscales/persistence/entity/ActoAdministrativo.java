package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "ACTO_ADMINISTRATIVO")
public class ActoAdministrativo extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private String id;
    @Column(name = "DESC_PARTE_RESOL_ACTO_ADM")
    private String descParteResolActoAdm;
    @Column(name = "VAL_VIGENCIA")
    private String valVigencia;
    @JoinColumn(name = "EXPEDIENTE_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "DOCUMENTO_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentoEcm documentoEcm;
    @JoinColumn(name = "COD_TIPO_ACTO_ADMINISTRATIVO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoActoAdministrativo;
    @Column(name = "FEC_ACTO_ADMINISTRATIVO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecActoAdministrativo;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "actoAdministrativo", fetch = FetchType.LAZY)
    private List<ValidacionActoAdministrativo> validaciones;
    @JoinColumn(name = "ID_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ActoAdministrativoDocumentoAnexo> documentosAnexos;
    @JoinColumn(name = "COD_AREA_ORIGEN", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codAreaOrigen;

    public ActoAdministrativo() {
    }

    public ActoAdministrativo(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<ValidacionActoAdministrativo> getValidaciones() {
        return validaciones;
    }

    public void setValidaciones(List<ValidacionActoAdministrativo> validaciones) {
        this.validaciones = validaciones;
    }

    public String getDescParteResolActoAdm() {
        return descParteResolActoAdm;
    }

    public void setDescParteResolActoAdm(String descParteResolActoAdm) {
        this.descParteResolActoAdm = descParteResolActoAdm;
    }

    public String getValVigencia() {
        return valVigencia;
    }

    public void setValVigencia(String valVigencia) {
        this.valVigencia = valVigencia;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public DocumentoEcm getDocumentoEcm() {
        return documentoEcm;
    }

    public void setDocumentoEcm(DocumentoEcm documentoEcm) {
        this.documentoEcm = documentoEcm;
    }

    public ValorDominio getCodTipoActoAdministrativo() {
        return codTipoActoAdministrativo;
    }

    public void setCodTipoActoAdministrativo(ValorDominio codTipoActoAdministrativo) {
        this.codTipoActoAdministrativo = codTipoActoAdministrativo;
    }

    public Calendar getFecActoAdministrativo() {
        return fecActoAdministrativo;
    }

    public void setFecActoAdministrativo(Calendar fecActoAdministrativo) {
        this.fecActoAdministrativo = fecActoAdministrativo;
    }

    public List<ActoAdministrativoDocumentoAnexo> getDocumentosAnexos() {
        if (documentosAnexos == null) {
            documentosAnexos = new ArrayList<ActoAdministrativoDocumentoAnexo>();
        }
        return documentosAnexos;
    }

    public ValorDominio getCodAreaOrigen() {
        return codAreaOrigen;
    }

    public void setCodAreaOrigen(ValorDominio codAreaOrigen) {
        this.codAreaOrigen = codAreaOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ActoAdministrativo)) {
            return false;
        }
        ActoAdministrativo other = (ActoAdministrativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ActoAdministrativo[ id=" + id + " ]";
    }

}
