package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "ACTO_ADMINISTRATIVO_DOC_ANEXO")
@NamedQueries({
    @NamedQuery(name = "actoAdministrativoDocumentoAnexo.findByActoAndDocumento",
            query = "SELECT e FROM ActoAdministrativoDocumentoAnexo e WHERE e.actoAdministrativo.id = :idActoAdministrativo AND e.documentoAnexo.id = :idDocumentoAnexo")})
public class ActoAdministrativoDocumentoAnexo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "actoAdmDocAnexoIdSeq", sequenceName = "acto_adm_doc_anexo_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "actoAdmDocAnexoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ActoAdministrativo actoAdministrativo;
    @JoinColumn(name = "COD_TIPO_ANEXO_ACTO_ADM", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ValorDominio codTipoAnexoActoAdministrativo;
    @JoinColumn(name = "ID_DOCUMENTO_ANEXO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private DocumentoEcm documentoAnexo;

    public ActoAdministrativoDocumentoAnexo() {

    }

    public ActoAdministrativo getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(ActoAdministrativo actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    public DocumentoEcm getDocumentoAnexo() {
        return documentoAnexo;
    }

    public void setDocumentoAnexo(DocumentoEcm documentoAnexo) {
        this.documentoAnexo = documentoAnexo;
    }

    public ValorDominio getCodTipoAnexoActoAdministrativo() {
        return codTipoAnexoActoAdministrativo;
    }

    public void setCodTipoAnexoActoAdministrativo(ValorDominio codTipoAnexoActoAdministrativo) {
        this.codTipoAnexoActoAdministrativo = codTipoAnexoActoAdministrativo;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ActoAdministrativoDocumentoAnexo)) {
            return false;
        }
        ActoAdministrativoDocumentoAnexo other = (ActoAdministrativoDocumentoAnexo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.AccionActoAdministrativo[ id=" + id + " ]";
    }

}
