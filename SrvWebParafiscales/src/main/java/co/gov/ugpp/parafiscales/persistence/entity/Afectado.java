package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author lquintero
 */
@Entity
@Table(name = "AFECTADO")
public class Afectado extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
    private Persona persona;

    @Column(name = "VAL_IBC")
    private Long valIBC;

    @Column(name = "FEC_INGRESO_EMPRESA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecIngresoEmpresa;

    public Afectado() {
        this.setPersona(new Persona());
    }

    public Afectado(Persona persona) {
        this.setPersona(persona);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Long getvalIBC() {
        return valIBC;
    }

    public void setvalIBC(Long valIBC) {
        this.valIBC = valIBC;
    }

    public Calendar getFecIngresoEmpresa() {
        return fecIngresoEmpresa;
    }

    public void setFecIngresoEmpresa(Calendar fecIngresoEmpresa) {
        this.fecIngresoEmpresa = fecIngresoEmpresa;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Denunciante)) {
            return false;
        }
        Afectado other = (Afectado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.Afectado[ id=" + id + " ]";
    }

}
