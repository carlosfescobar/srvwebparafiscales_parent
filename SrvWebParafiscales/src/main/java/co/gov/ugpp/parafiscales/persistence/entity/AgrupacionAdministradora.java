package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "AGRUPACION_ADMINISTRADORA")
@NamedQueries({
    @NamedQuery(name = "agrupacionAdministradora.findByAgrupacionSubsistemaAdminAndEntidadexterna",
            query = "SELECT aa FROM AgrupacionAdministradora aa WHERE aa.agrupacionSubsistemaAdmin.id = :idAgrupacionSubsistemaAdmin AND aa.entidadExterna.id = :idEntidadExterna")
})
public class AgrupacionAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "agrupacionAdministradoraIdSeq", sequenceName = "agrupacion_administrad_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agrupacionAdministradoraIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EntidadExterna entidadExterna;
    @JoinColumn(name = "ID_AGRUP_SUBSISTEMA_ADMINIS", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin;

    public AgrupacionAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public AgrupacionSubsistemaAdmin getAgrupacionAdministradora() {
        return agrupacionSubsistemaAdmin;
    }

    public void setAgrupacionAdministradora(AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin) {
        this.agrupacionSubsistemaAdmin = agrupacionSubsistemaAdmin;
    }

}
