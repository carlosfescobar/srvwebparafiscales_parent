package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "AGRUPACION_AFILIACIONES_ENT")
public class AgrupacionAfiliacionesPorEntidad extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "agrupacionAfiliaEntExtIdSeq", sequenceName = "agrupacion_afilia_ent_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agrupacionAfiliaEntExtIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private EntidadExterna entidadExterna;
    @JoinColumn(name = "ID_DOC_OFC_AFIL", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DocumentoEcm idDocumentoOficioAfiliaciones;
    @JoinColumn(name = "ID_OFICIO_AFILIA_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento oficioAfiliacionesParcial;
    @JoinColumn(name = "ID_AGRUPACION_AFILIA_EXT_EXT", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgrupacionEntidadExternaPersona> personasAfiliar;
    @JoinColumn(name = "ID_LIQUIDACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Liquidacion liquidacion;

    public AgrupacionAfiliacionesPorEntidad() {
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public List<AgrupacionEntidadExternaPersona> getPersonasAfiliar() {
        if (personasAfiliar == null) {
            personasAfiliar = new ArrayList<AgrupacionEntidadExternaPersona>();
        }
        return personasAfiliar;
    }

    public DocumentoEcm getIdDocumentoOficioAfiliaciones() {
        return idDocumentoOficioAfiliaciones;
    }

    public void setIdDocumentoOficioAfiliaciones(DocumentoEcm idDocumentoOficioAfiliaciones) {
        this.idDocumentoOficioAfiliaciones = idDocumentoOficioAfiliaciones;
    }

    public AprobacionDocumento getOficioAfiliacionesParcial() {
        return oficioAfiliacionesParcial;
    }

    public void setOficioAfiliacionesParcial(AprobacionDocumento oficioAfiliacionesParcial) {
        this.oficioAfiliacionesParcial = oficioAfiliacionesParcial;
    }

    public Liquidacion getLiquidacion() {
        return liquidacion;
    }

    public void setLiquidacion(Liquidacion liquidacion) {
        this.liquidacion = liquidacion;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgrupacionAfiliacionesPorEntidad)) {
            return false;
        }
        AgrupacionAfiliacionesPorEntidad other = (AgrupacionAfiliacionesPorEntidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Dominio[ id=" + id + " ]";
    }

}
