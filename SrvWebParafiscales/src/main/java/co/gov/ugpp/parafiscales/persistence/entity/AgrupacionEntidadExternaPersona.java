package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "AGRUPACION_ENT_EXT_PERSONA")
public class AgrupacionEntidadExternaPersona extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "agrupacionEntidadExternaPersonaIdSeq", sequenceName = "agrupacion_afila_per_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agrupacionEntidadExternaPersonaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona persona;
    @JoinColumn(name = "ID_AGRUPACION_AFILIA_EXT_EXT", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad;

    public AgrupacionEntidadExternaPersona() {
    }

    public AgrupacionEntidadExternaPersona(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public AgrupacionAfiliacionesPorEntidad getAgrupacionAfiliacionesPorEntidad() {
        return agrupacionAfiliacionesPorEntidad;
    }

    public void setAgrupacionAfiliacionesPorEntidad(AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad) {
        this.agrupacionAfiliacionesPorEntidad = agrupacionAfiliacionesPorEntidad;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgrupacionEntidadExternaPersona)) {
            return false;
        }
        AgrupacionEntidadExternaPersona other = (AgrupacionEntidadExternaPersona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Departamento[ id=" + id + " ]";
    }

}
