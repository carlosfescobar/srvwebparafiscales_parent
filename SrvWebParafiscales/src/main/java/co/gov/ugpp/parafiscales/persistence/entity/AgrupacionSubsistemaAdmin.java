package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "AGRUPACION_SUBSISTEMA")
public class AgrupacionSubsistemaAdmin extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "agrupacionsubsistemaadminIdSeq", sequenceName = "agrup_subsist_admin_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agrupacionsubsistemaadminIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_SUBSISTEMA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Subsistema idSubsistema;
    @JoinColumn(name = "ID_AGRUP_SUBSISTEMA_ADMINIS", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgrupacionAdministradora> administradoras;

    public AgrupacionSubsistemaAdmin() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subsistema getIdSubsistema() {
        return idSubsistema;
    }

    public void setIdSubsistema(Subsistema idSubsistema) {
        this.idSubsistema = idSubsistema;
    }

    public List<AgrupacionAdministradora> getAdministradoras() {
        if (administradoras == null) {
            administradoras = new ArrayList<AgrupacionAdministradora>();
        }
        return administradoras;
    }

}
