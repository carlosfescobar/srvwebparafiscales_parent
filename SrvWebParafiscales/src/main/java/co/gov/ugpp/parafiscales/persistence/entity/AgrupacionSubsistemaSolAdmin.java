package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Everis
 */
@Entity
@Table(name = "AGRUPACION_SUB_SOL_ADMINISTRA")
@NamedQueries({
    @NamedQuery(name = "agrupacionSubsitemaSolAdmin.findByAgrupacionSubsistemaAndSolicitudAdmin",
            query = "SELECT asa FROM AgrupacionSubsistemaSolAdmin asa WHERE asa.solicitudAdministradora.id= :idSolicitudAdministradora AND asa.administradora.id= :idAdministradora")
})
public class AgrupacionSubsistemaSolAdmin extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "agrupacionsubsistemasoladminIdSeq", sequenceName = "agrup_sub_sol_administ_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "agrupacionsubsistemasoladminIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SolicitudAdministradora solicitudAdministradora;
    @JoinColumn(name = "ID_AGRUPACION_SUBSISTEMA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AgrupacionSubsistemaAdmin administradora;

    public AgrupacionSubsistemaSolAdmin() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SolicitudAdministradora getSolicitudAdministradora() {
        return solicitudAdministradora;
    }

    public void setSolicitudAdministradora(SolicitudAdministradora solicitudAdministradora) {
        this.solicitudAdministradora = solicitudAdministradora;
    }

    public AgrupacionSubsistemaAdmin getAdministradora() {
        return administradora;
    }

    public void setAdministradora(AgrupacionSubsistemaAdmin administradora) {
        this.administradora = administradora;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgrupacionSubsistemaSolAdmin)) {
            return false;
        }
        AgrupacionSubsistemaSolAdmin other = (AgrupacionSubsistemaSolAdmin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.AgrupacionSubsistemaSolAdmin[ id=" + id + " ]";
    }
}
