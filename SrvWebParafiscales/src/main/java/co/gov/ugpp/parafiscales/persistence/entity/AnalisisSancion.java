package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author vgomezva
 */
@Entity
@Table(name = "ANALISIS_SANCION")
public class AnalisisSancion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "analisisSancionIdSeq", sequenceName = "analisis_sancion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "analisisSancionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "UVT", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UVT idUvt;
    @Column(name = "FEC_VENCIMIENTO_TERMINO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecVencimientoTermino;
    @Column(name = "FEC_LLEGADA_TEMPORANEA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecLlegadaTemporanea;
    @Column(name = "DIAS_INCUMPLIMENTO")
    private String diasIncumplimiento;
    @Column(name = "VAL_SANCION_NUMEROS")
    private String valSancionNumeros;
    @Column(name = "VAL_SANCION_LETRAS")
    private String valSancionLetras;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sancion sancion;
    @Column(name = "DES_OBSER_IMPORTE_SANCION")
    private String desObserImporteSancion;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UVT getUvt() {
        return idUvt;
    }

    public void setUvt(UVT  uvt) {
        this.idUvt = uvt;
    }

    public Calendar getFecVencimientoTermino() {
        return fecVencimientoTermino;
    }

    public void setFecVencimientoTermino(Calendar fecVencimientoTermino) {
        this.fecVencimientoTermino = fecVencimientoTermino;
    }

   

    public Calendar getFecLlegadaTemporanea() {
        return fecLlegadaTemporanea;
    }

    public void setFecLlegadaTemporanea(Calendar fecLlegadaTemporanea) {
        this.fecLlegadaTemporanea = fecLlegadaTemporanea;
    }

    public String getDiasIncumplimiento() {
        return diasIncumplimiento;
    }

    public void setDiasIncumplimiento(String diasIncumplimiento) {
        this.diasIncumplimiento = diasIncumplimiento;
    }

    public String getValSancionNumeros() {
        return valSancionNumeros;
    }

    public void setValSancionNumeros(String valSancionNumeros) {
        this.valSancionNumeros = valSancionNumeros;
    }

    public String getValSancionLetras() {
        return valSancionLetras;
    }

    public void setValSancionLetras(String valSancionLetras) {
        this.valSancionLetras = valSancionLetras;
    }

   

    public String getDesObserImporteSancion() {
        return desObserImporteSancion;
    }

    public void setDesObserImporteSancion(String desObserImporteSancion) {
        this.desObserImporteSancion = desObserImporteSancion;
    }

    public Sancion getSancion() {
        return sancion;
    }

    public void setSancion(Sancion sancion) {
        this.sancion = sancion;
    }

}
