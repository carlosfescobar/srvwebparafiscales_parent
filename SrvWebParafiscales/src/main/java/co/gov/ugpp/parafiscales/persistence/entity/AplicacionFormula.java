package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqaplicacionformula", initialValue = 1, allocationSize = 1)
@Table(name = "APLICACION_FORMULA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AplicacionFormula.findAll", query = "SELECT a FROM AplicacionFormula a"),
    @NamedQuery(name = "AplicacionFormula.findById", query = "SELECT a FROM AplicacionFormula a WHERE a.id = :id"),
    @NamedQuery(name = "AplicacionFormula.findByCampoDestino", query = "SELECT a FROM AplicacionFormula a WHERE a.campoDestino = :campoDestino"),
    @NamedQuery(name = "AplicacionFormula.findByTipoUso", query = "SELECT a FROM AplicacionFormula a WHERE a.tipoUso = :tipoUso"),
    @NamedQuery(name = "AplicacionFormula.findByActivaVigente", 
        query = "SELECT a FROM AplicacionFormula a WHERE a.tipoUso = :tipoUso and "
        + " a.formula.fechaInicial <= :fecha and  "
        + " a.formula.fechaFinal >= :fecha and "
        + " a.formula.estado = 1 ")
})

public class AplicacionFormula extends AbstractEntity<Long>{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqaplicacionformula")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 250)
    @Column(name = "CAMPO_DESTINO")
    private String campoDestino;
    @Size(max = 150)
    @Column(name = "TIPO_USO")
    private String tipoUso;
    @JoinColumn(name = "FORMULA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Formula formula;
    @OneToMany(mappedBy = "aplicacionFormula", fetch = FetchType.LAZY)
    private List<ValorFormula> valorFormulaList;

    public AplicacionFormula() {
    }

    public AplicacionFormula(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCampoDestino() {
        return campoDestino;
    }

    public void setCampoDestino(String campoDestino) {
        this.campoDestino = campoDestino;
    }

    public String getTipoUso() {
        return tipoUso;
    }

    public void setTipoUso(String tipoUso) {
        this.tipoUso = tipoUso;
    }

    public Formula getFormula() {
        return formula;
    }

    public void setFormula(Formula formula) {
        this.formula = formula;
    }

    @XmlTransient
    public List<ValorFormula> getValorFormulaList() {
        return valorFormulaList;
    }

    public void setValorFormulaList(List<ValorFormula> valorFormulaList) {
        this.valorFormulaList = valorFormulaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AplicacionFormula)) {
            return false;
        }
        AplicacionFormula other = (AplicacionFormula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.AplicacionFormula[ id=" + id + " ]";
    }
    
}
