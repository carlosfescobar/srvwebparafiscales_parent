package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author franzjr
 */
@Entity
//@TableGenerator(name = "seqaportante", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_APORTANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Aportante.findByIdentificacion", query = "SELECT a FROM AportanteLIQ a WHERE a.numeroIdentificacion = :numeroIdentificacion and a.tipoIdentificacion.sigla = :tipoIdentificacion"),
    @NamedQuery(name = "Aportante.findByIdentificacionAndIdTipoIdentificacion", query = "SELECT a FROM AportanteLIQ a WHERE a.numeroIdentificacion = :numeroIdentificacion and a.tipoIdentificacion.id = :idTipoIdentificacion"),
    @NamedQuery(name = "Aportante.findByIdentificacionAndTipo", query = "SELECT COUNT(a) FROM AportanteLIQ a WHERE a.numeroIdentificacion = :numeroIdentificacion and a.tipoIdentificacion.id = :tipoIdentificacion")})
public class AportanteLIQ extends AbstractEntity<Long>{
    private static final long serialVersionUID = 1L;
    public static final String NQ_FIND_BY_IDENTIFICACION = "Aportante.findByIdentificacion";
//@GeneratedValue(strategy = GenerationType.TABLE, generator = "seqaportante")
    @Id
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqaportante")
    //@GeneratedValue(strategy = GenerationType.TABLE, generator = "seqaportante")
     @SequenceGenerator(name="seqaportante", sequenceName="seq_aportante_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqaportante")
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 20)
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;
    @Size(max = 250)
    @Column(name = "PRIMER_NOMBRE")
    private String primerNombre;
    @Size(max = 250)
    @Column(name = "SEGUNDO_NOMBRE")
    private String segundoNombre;
    @Size(max = 250)
    @Column(name = "PRIMER_APELLIDO")
    private String primerApellido;
    @Size(max = 250)
    @Column(name = "SEGUNDO_APELLIDO")
    private String segundoApellido;
    @Size(max = 15)
    @Column(name = "SEXO")
    private String sexo;
    @Size(max = 250)
    @Column(name = "TIPO_PERSONA")
    private String tipoPersona;
    @Size(max = 250)
    @Column(name = "TIPO_PERSONA_NATURAL")
    private String tipoPersonaNatural;
    @Size(max = 250)
    @Column(name = "TIPO_PERSONA_JURIDICA")
    private String tipoPersonaJuridica;
    @Size(max = 250)
    @Column(name = "CLASE")
    private String clase;
    
    @Size(max = 2)
    @Column(name = "APORTA_ESAP_Y_MEN")
    private String aportaEsapYMen;
    
    @Size(max = 2)
    @Column(name = "EXCEPCION_LEY_1233_2008")
    private String excepcionLey12332008;
    
    
    @JoinColumn(name = "ACTIVIDAD_ECONOMICA", referencedColumnName = "T_BACE_INT_CLASE")
    @ManyToOne(fetch = FetchType.LAZY)
    private TBactividadeconomica actividadEconomica;
    @JoinColumn(name = "NIVEL_EDUCATIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private PorcentajeNivelEducativo nivelEducativo;
    @JoinColumn(name = "ESTADO_CIVIL", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private EstadoCivil estadoCivil;
    @JoinColumn(name = "TIPO_IDENTIFICACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoIdentificacion tipoIdentificacion;
    
    @OneToMany(mappedBy = "idaportante", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NominaDetalle> nominaLiquidadorDetalleList;
    
    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    
    @Size(max = 2)
    @Column(name = "NATURALEZA_JURIDICA")
    private String naturalezaJuridica;
    
    
    @Size(max = 2)
    @Column(name = "TIPO_APORTANTE")
    private String tipoAportante;
 
    
    @Size(max = 2)
    @Column(name = "SUJETO_PASIVO_IMPUESTO_CREE")
    private String sujetoPasivoImpuestoCree;
    
    //SE ELIMINARON LOS SET DE:
    //  private String clase;    
    //  private String excepcionLey12332008;
    
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//

    public String getSujetoPasivoImpuestoCree() {
        return sujetoPasivoImpuestoCree;
    }
    
    public void setSujetoPasivoImpuestoCree(String sujetoPasivoImpuestoCree) {
        this.sujetoPasivoImpuestoCree = sujetoPasivoImpuestoCree;
    }
    
    public String getTipoAportante() {
        return tipoAportante;
    }
    
    public void setTipoAportante(String tipoAportante) {
        this.tipoAportante = tipoAportante;
    }
    
    public String getNaturalezaJuridica() {
        return naturalezaJuridica;
    }
    
    public void setNaturalezaJuridica(String aportaEsapYMen) {
        this.naturalezaJuridica = aportaEsapYMen;
    }
    
    public AportanteLIQ() {
    }

    public AportanteLIQ(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public String getTipoPersonaNatural() {
        return tipoPersonaNatural;
    }

    public void setTipoPersonaNatural(String tipoPersonaNatural) {
        this.tipoPersonaNatural = tipoPersonaNatural;
    }

    public String getTipoPersonaJuridica() {
        return tipoPersonaJuridica;
    }

    public void setTipoPersonaJuridica(String tipoPersonaJuridica) {
        this.tipoPersonaJuridica = tipoPersonaJuridica;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public TBactividadeconomica getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(TBactividadeconomica actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    public PorcentajeNivelEducativo getNivelEducativo() {
        return nivelEducativo;
    }

    public void setNivelEducativo(PorcentajeNivelEducativo nivelEducativo) {
        this.nivelEducativo = nivelEducativo;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if ( object == null  || id == null || !(object instanceof AportanteLIQ)) {
            return false;
        }
        AportanteLIQ other = (AportanteLIQ) object;
        return this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.intgr.Aportante[ id=" + id + " ]";
    }

    /**
     * @return the nominaLiquidadorDetalleList
     */
    public List<NominaDetalle> getNominaLiquidadorDetalleList() {
        return nominaLiquidadorDetalleList;
    }

    /**
     * @param nominaLiquidadorDetalleList the nominaLiquidadorDetalleList to set
     */
    public void setNominaLiquidadorDetalleList(List<NominaDetalle> nominaLiquidadorDetalleList) {
        this.nominaLiquidadorDetalleList = nominaLiquidadorDetalleList;
    }

    /**
     * @return the aportaEsapYMen
     */
    public String getAportaEsapYMen() {
        return aportaEsapYMen;
    }

    /**
     * @param aportaEsapYMen the aportaEsapYMen to set
     */
    public void setAportaEsapYMen(String aportaEsapYMen) {
        this.aportaEsapYMen = aportaEsapYMen;
    }

    /**
     * @return the excepcionLey12332008
     */
    public String getExcepcionLey12332008() {
        return excepcionLey12332008;
    }

    /**
     * @param excepcionLey12332008 the excepcionLey12332008 to set
     */
    public void setExcepcionLey12332008(String excepcionLey12332008) {
        this.excepcionLey12332008 = excepcionLey12332008;
    }
    
}
