package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_APORTES_INDEPENDIENTE")
public class AportesIndependiente extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name="aportesindependiente_seq", sequenceName="aportesindependiente_seq_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aportesindependiente_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    
    
    @Basic(optional = false)
    @Column(name = "TIPO_INGRESO")
    private BigInteger tipoIngreso;
    @Basic(optional = false)
    @Size(min = 1, max = 200)
    @Column(name = "DESCRIPCION_INGRESO")
    private String descripcionIngreso;
    @Size(max = 50)
    @Column(name = "NUMERO_CONTRATO")
    private String numeroContrato;
    @Column(name = "CONTRATO_FINICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contratoFinicio;
    @Column(name = "CONTRATO_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contratoFfinal;
    @Column(name = "VALOR_CONTRATO")
    private BigDecimal valorContrato;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_INGRESOS")
    private String observacionesIngresos;
    @Column(name = "FECHA_COSTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCosto;
    @Size(max = 500)
    @Column(name = "DESCRIPCION_COSTO")
    private String descripcionCosto;
    @Size(max = 500)
    @Column(name = "PROVEEDOR_COSTO")
    private String proveedorCosto;
    @Column(name = "NUMERO_FACTURA")
    private Long numeroFactura;
    @Column(name = "VALOR_COSTO")
    private BigDecimal valorCosto;
    @Size(max = 2)
    @Column(name = "ACEPTACION_COSTO")
    private String aceptacionCosto;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_COSTO")
    private String observacionesCosto;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_NOVEDADES")
    private String observacionesNovedades;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_SALUD")
    private String observacionesSalud;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_PENSION")
    private String observacionesPension;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ARL")
    private String observacionesArl;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_SENA")
    private String observacionesSena;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ICBF")
    private String observacionesIcbf;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_MEN")
    private String observacionesMen;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_CCF")
    private String observacionesCcf;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ESAP")
    private String observacionesEsap;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;

    @JoinColumn(name = "IDNOMINADETALLE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private NominaDetalle idnominadetalle;

    
    
    
    
    
    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    
    
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_APORTANTE")
    private String observacionesAportante;
    
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_UGPP")
    private String observacionesUgpp;

    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
    
        //**********************************************************//
    // CAMPOS NUEVOS YESID NUEVA ESTRUCTURA NOMINA 29-02-2016
    //**********************************************************//
    
    
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_FSP")
    private String observacionesFsp;
    
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ALTORIESGO")
    private String observacionesAltoRiesgo;    
    

    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
            
    
    
    
    
    
    
    
    public AportesIndependiente() {
    }

    public AportesIndependiente(Long id) {
        this.id = id;
    }

    public AportesIndependiente(Long id, BigInteger tipoIngreso, String descripcionIngreso) {
        this.id = id;
        this.tipoIngreso = tipoIngreso;
        this.descripcionIngreso = descripcionIngreso;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(BigInteger tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public String getDescripcionIngreso() {
        return descripcionIngreso;
    }

    public void setDescripcionIngreso(String descripcionIngreso) {
        this.descripcionIngreso = descripcionIngreso;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Date getContratoFinicio() {
        return contratoFinicio;
    }

    public void setContratoFinicio(Date contratoFinicio) {
        this.contratoFinicio = contratoFinicio;
    }

    public Date getContratoFfinal() {
        return contratoFfinal;
    }

    public void setContratoFfinal(Date contratoFfinal) {
        this.contratoFfinal = contratoFfinal;
    }

    public BigDecimal getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(BigDecimal valorContrato) {
        this.valorContrato = valorContrato;
    }

    public String getObservacionesIngresos() {
        return observacionesIngresos;
    }

    public void setObservacionesIngresos(String observacionesIngresos) {
        this.observacionesIngresos = observacionesIngresos;
    }

    public Date getFechaCosto() {
        return fechaCosto;
    }

    public void setFechaCosto(Date fechaCosto) {
        this.fechaCosto = fechaCosto;
    }

    public String getDescripcionCosto() {
        return descripcionCosto;
    }

    public void setDescripcionCosto(String descripcionCosto) {
        this.descripcionCosto = descripcionCosto;
    }

    public String getProveedorCosto() {
        return proveedorCosto;
    }

    public void setProveedorCosto(String proveedorCosto) {
        this.proveedorCosto = proveedorCosto;
    }

    public Long getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public BigDecimal getValorCosto() {
        return valorCosto;
    }

    public void setValorCosto(BigDecimal valorCosto) {
        this.valorCosto = valorCosto;
    }

    public String getAceptacionCosto() {
        return aceptacionCosto;
    }

    public void setAceptacionCosto(String aceptacionCosto) {
        this.aceptacionCosto = aceptacionCosto;
    }

    public String getObservacionesCosto() {
        return observacionesCosto;
    }

    public void setObservacionesCosto(String observacionesCosto) {
        this.observacionesCosto = observacionesCosto;
    }

    public String getObservacionesNovedades() {
        return observacionesNovedades;
    }

    public void setObservacionesNovedades(String observacionesNovedades) {
        this.observacionesNovedades = observacionesNovedades;
    }

    public String getObservacionesSalud() {
        return observacionesSalud;
    }

    public void setObservacionesSalud(String observacionesSalud) {
        this.observacionesSalud = observacionesSalud;
    }

    public String getObservacionesPension() {
        return observacionesPension;
    }

    public void setObservacionesPension(String observacionesPension) {
        this.observacionesPension = observacionesPension;
    }

    public String getObservacionesArl() {
        return observacionesArl;
    }

    public void setObservacionesArl(String observacionesArl) {
        this.observacionesArl = observacionesArl;
    }

    public String getObservacionesSena() {
        return observacionesSena;
    }

    public void setObservacionesSena(String observacionesSena) {
        this.observacionesSena = observacionesSena;
    }

    public String getObservacionesIcbf() {
        return observacionesIcbf;
    }

    public void setObservacionesIcbf(String observacionesIcbf) {
        this.observacionesIcbf = observacionesIcbf;
    }

    public String getObservacionesMen() {
        return observacionesMen;
    }

    public void setObservacionesMen(String observacionesMen) {
        this.observacionesMen = observacionesMen;
    }

    public String getObservacionesCcf() {
        return observacionesCcf;
    }

    public void setObservacionesCcf(String observacionesCcf) {
        this.observacionesCcf = observacionesCcf;
    }

    public String getObservacionesEsap() {
        return observacionesEsap;
    }

    public void setObservacionesEsap(String observacionesEsap) {
        this.observacionesEsap = observacionesEsap;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AportesIndependiente)) {
            return false;
        }
        AportesIndependiente other = (AportesIndependiente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.AportesIndependiente[ id=" + id + " ]";
    }

    /**
     * @return the idnominadetalle
     */
    public NominaDetalle getIdnominadetalle() {
        return idnominadetalle;
    }

    /**
     * @param idnominadetalle the idnominadetalle to set
     */
    public void setIdnominadetalle(NominaDetalle idnominadetalle) {
        this.idnominadetalle = idnominadetalle;
    }

    /**
     * @return the observacionesAportante
     */
    public String getObservacionesAportante() {
        return observacionesAportante;
    }

    /**
     * @param observacionesAportante the observacionesAportante to set
     */
    public void setObservacionesAportante(String observacionesAportante) {
        this.observacionesAportante = observacionesAportante;
    }

    /**
     * @return the observacionesUgpp
     */
    public String getObservacionesUgpp() {
        return observacionesUgpp;
    }

    /**
     * @param observacionesUgpp the observacionesUgpp to set
     */
    public void setObservacionesUgpp(String observacionesUgpp) {
        this.observacionesUgpp = observacionesUgpp;
    }

    

    
      /**
     * @return the observacionesFsp
     */
    public String getObservacionesFsp() {
        return observacionesFsp;
    }

    /**
     * @param observacionesFsp the observacionesUgpp to set
     */
    public void setObservacionesFsp(String observacionesFsp) {
        this.observacionesFsp = observacionesFsp;
    }

    
    
    
      /**
     * @return the observacionesUgpp
     */
    public String getObservacionesAltoRiesgo() {
        return observacionesAltoRiesgo;
    }

    /**
     * @param observacionesUgpp the observacionesUgpp to set
     */
    public void setObservacionesAltoRiesgo(String observacionesAltoRiesgo) {
        this.observacionesAltoRiesgo = observacionesAltoRiesgo;
    }

}
