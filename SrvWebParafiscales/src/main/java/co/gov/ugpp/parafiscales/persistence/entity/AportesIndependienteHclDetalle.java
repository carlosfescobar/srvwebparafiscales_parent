package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Mauricio Guerrero
 */
@Entity
@Table(name = "APORTES_INDEPENDIENTE_HCL_DETA")
public class AportesIndependienteHclDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqaporindhcldet")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "TIPO_INGRESO")
    private BigInteger tipoIngreso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "DESCRIPCION_INGRESO")
    private String descripcionIngreso;
    @Size(max = 50)
    @Column(name = "NUMERO_CONTRATO")
    private String numeroContrato;
    @Column(name = "CONTRATO_FINICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contratoFinicio;
    @Column(name = "CONTRATO_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date contratoFfinal;
    @Column(name = "VALOR_CONTRATO")
    private BigDecimal valorContrato;
    @Column(name = "HOJATRAD_DURACION_ING_MESES_EZ")
    private BigInteger hojatradDuracionIngMesesEz;
    @Column(name = "HOJATRAD_ANO_FA")
    private BigInteger hojatradAnoFa;
    @Column(name = "HOJATRAD_MES_FB")
    private BigInteger hojatradMesFb;
    @Column(name = "HOJATRAD_VALOR_MENSUALIZADO_FC")
    private BigDecimal hojatradValorMensualizadoFc;
    @Column(name = "HOJATRAD_INGRESO_DEPURADO_FD")
    private BigDecimal hojatradIngresoDepuradoFd;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_INGRESOS")
    private String observacionesIngresos;
    @Column(name = "FECHA_COSTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCosto;
    @Size(max = 500)
    @Column(name = "DESCRIPCION_COSTO")
    private String descripcionCosto;
    @Size(max = 500)
    @Column(name = "PROVEEDOR_COSTO")
    private String proveedorCosto;
    @Column(name = "NUMERO_FACTURA")
    private Long numeroFactura;
    @Column(name = "VALOR_COSTO")
    private BigDecimal valorCosto;
    @Size(max = 2)
    @Column(name = "ACEPTACION_COSTO")
    private String aceptacionCosto;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_COSTO")
    private String observacionesCosto;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_NOVEDADES")
    private String observacionesNovedades;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_SALUD")
    private String observacionesSalud;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_PENSION")
    private String observacionesPension;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ARL")
    private String observacionesArl;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_SENA")
    private String observacionesSena;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ICBF")
    private String observacionesIcbf;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_MEN")
    private String observacionesMen;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_CCF")
    private String observacionesCcf;
    @Size(max = 500)
    @Column(name = "OBSERVACIONES_ESAP")
    private String observacionesEsap;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "IDHOJACALCULOLIQUIDACIONDET", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private HojaCalculoLiquidacionDetalle idhojacalculoliquidaciondet;

    public AportesIndependienteHclDetalle() {
    }

    public AportesIndependienteHclDetalle(BigDecimal id) {
        this.id = id;
    }

    public AportesIndependienteHclDetalle(BigDecimal id, BigInteger tipoIngreso, String descripcionIngreso) {
        this.id = id;
        this.tipoIngreso = tipoIngreso;
        this.descripcionIngreso = descripcionIngreso;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getTipoIngreso() {
        return tipoIngreso;
    }

    public void setTipoIngreso(BigInteger tipoIngreso) {
        this.tipoIngreso = tipoIngreso;
    }

    public String getDescripcionIngreso() {
        return descripcionIngreso;
    }

    public void setDescripcionIngreso(String descripcionIngreso) {
        this.descripcionIngreso = descripcionIngreso;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Date getContratoFinicio() {
        return contratoFinicio;
    }

    public void setContratoFinicio(Date contratoFinicio) {
        this.contratoFinicio = contratoFinicio;
    }

    public Date getContratoFfinal() {
        return contratoFfinal;
    }

    public void setContratoFfinal(Date contratoFfinal) {
        this.contratoFfinal = contratoFfinal;
    }

    public BigDecimal getValorContrato() {
        return valorContrato;
    }

    public void setValorContrato(BigDecimal valorContrato) {
        this.valorContrato = valorContrato;
    }

    public BigInteger getHojatradDuracionIngMesesEz() {
        return hojatradDuracionIngMesesEz;
    }

    public void setHojatradDuracionIngMesesEz(BigInteger hojatradDuracionIngMesesEz) {
        this.hojatradDuracionIngMesesEz = hojatradDuracionIngMesesEz;
    }

    public BigInteger getHojatradAnoFa() {
        return hojatradAnoFa;
    }

    public void setHojatradAnoFa(BigInteger hojatradAnoFa) {
        this.hojatradAnoFa = hojatradAnoFa;
    }

    public BigInteger getHojatradMesFb() {
        return hojatradMesFb;
    }

    public void setHojatradMesFb(BigInteger hojatradMesFb) {
        this.hojatradMesFb = hojatradMesFb;
    }

    public BigDecimal getHojatradValorMensualizadoFc() {
        return hojatradValorMensualizadoFc;
    }

    public void setHojatradValorMensualizadoFc(BigDecimal hojatradValorMensualizadoFc) {
        this.hojatradValorMensualizadoFc = hojatradValorMensualizadoFc;
    }

    public BigDecimal getHojatradIngresoDepuradoFd() {
        return hojatradIngresoDepuradoFd;
    }

    public void setHojatradIngresoDepuradoFd(BigDecimal hojatradIngresoDepuradoFd) {
        this.hojatradIngresoDepuradoFd = hojatradIngresoDepuradoFd;
    }

    public String getObservacionesIngresos() {
        return observacionesIngresos;
    }

    public void setObservacionesIngresos(String observacionesIngresos) {
        this.observacionesIngresos = observacionesIngresos;
    }

    public Date getFechaCosto() {
        return fechaCosto;
    }

    public void setFechaCosto(Date fechaCosto) {
        this.fechaCosto = fechaCosto;
    }

    public String getDescripcionCosto() {
        return descripcionCosto;
    }

    public void setDescripcionCosto(String descripcionCosto) {
        this.descripcionCosto = descripcionCosto;
    }

    public String getProveedorCosto() {
        return proveedorCosto;
    }

    public void setProveedorCosto(String proveedorCosto) {
        this.proveedorCosto = proveedorCosto;
    }

    public Long getNumeroFactura() {
        return numeroFactura;
    }

    public void setNumeroFactura(Long numeroFactura) {
        this.numeroFactura = numeroFactura;
    }

    public BigDecimal getValorCosto() {
        return valorCosto;
    }

    public void setValorCosto(BigDecimal valorCosto) {
        this.valorCosto = valorCosto;
    }

    public String getAceptacionCosto() {
        return aceptacionCosto;
    }

    public void setAceptacionCosto(String aceptacionCosto) {
        this.aceptacionCosto = aceptacionCosto;
    }

    public String getObservacionesCosto() {
        return observacionesCosto;
    }

    public void setObservacionesCosto(String observacionesCosto) {
        this.observacionesCosto = observacionesCosto;
    }

    public String getObservacionesNovedades() {
        return observacionesNovedades;
    }

    public void setObservacionesNovedades(String observacionesNovedades) {
        this.observacionesNovedades = observacionesNovedades;
    }

    public String getObservacionesSalud() {
        return observacionesSalud;
    }

    public void setObservacionesSalud(String observacionesSalud) {
        this.observacionesSalud = observacionesSalud;
    }

    public String getObservacionesPension() {
        return observacionesPension;
    }

    public void setObservacionesPension(String observacionesPension) {
        this.observacionesPension = observacionesPension;
    }

    public String getObservacionesArl() {
        return observacionesArl;
    }

    public void setObservacionesArl(String observacionesArl) {
        this.observacionesArl = observacionesArl;
    }

    public String getObservacionesSena() {
        return observacionesSena;
    }

    public void setObservacionesSena(String observacionesSena) {
        this.observacionesSena = observacionesSena;
    }

    public String getObservacionesIcbf() {
        return observacionesIcbf;
    }

    public void setObservacionesIcbf(String observacionesIcbf) {
        this.observacionesIcbf = observacionesIcbf;
    }

    public String getObservacionesMen() {
        return observacionesMen;
    }

    public void setObservacionesMen(String observacionesMen) {
        this.observacionesMen = observacionesMen;
    }

    public String getObservacionesCcf() {
        return observacionesCcf;
    }

    public void setObservacionesCcf(String observacionesCcf) {
        this.observacionesCcf = observacionesCcf;
    }

    public String getObservacionesEsap() {
        return observacionesEsap;
    }

    public void setObservacionesEsap(String observacionesEsap) {
        this.observacionesEsap = observacionesEsap;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public HojaCalculoLiquidacionDetalle getIdhojacalculoliquidaciondet() {
        return idhojacalculoliquidaciondet;
    }

    public void setIdhojacalculoliquidaciondet(HojaCalculoLiquidacionDetalle idhojacalculoliquidaciondet) {
        this.idhojacalculoliquidaciondet = idhojacalculoliquidaciondet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof AportesIndependienteHclDetalle)) {
            return false;
        }
        AportesIndependienteHclDetalle other = (AportesIndependienteHclDetalle) object;
        if (this.id == null && other.id != null || this.id != null && !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.entidades.negocio.AportesIndependienteHclDeta[ id=" + id + " ]";
    }

}
