package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "APROBACION_DOCUMENTO")
@NamedQueries({
    @NamedQuery(name = "aprobacionDocumento.findByArchivoAndValidacion",
            query = "SELECT a FROM AprobacionDocumento a WHERE a.archivo.id = :idArchivo AND a.validacionCreacionDocumento IN :idValidacionDocumento")})
public class AprobacionDocumento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ARCHIVO", updatable = false)
    private Long id;
    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @PrimaryKeyJoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    private Archivo archivo;
    
    @JoinColumn(name = "ID_APROBACION_DOCUMENTO", referencedColumnName = "ID_ARCHIVO")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AprobacionValidacionDocumento> validacionCreacionDocumento;

    public AprobacionDocumento() {
    }

    public AprobacionDocumento(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
        if(archivo != null){
            this.id = archivo.getId();
        }
    }

    public List<AprobacionValidacionDocumento> getValidacionCreacionDocumento() {
        if (validacionCreacionDocumento == null) {
            validacionCreacionDocumento = new ArrayList<AprobacionValidacionDocumento>();
        }

        return validacionCreacionDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AprobacionDocumento)) {
            return false;
        }
        AprobacionDocumento other = (AprobacionDocumento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ExpedienteDocumentoEcm[ id=" + id + " ]";
    }

}
