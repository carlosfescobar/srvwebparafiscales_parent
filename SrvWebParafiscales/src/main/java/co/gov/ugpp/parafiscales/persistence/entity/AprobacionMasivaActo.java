package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "APROBACION_MASIVA_ACTO")
public class AprobacionMasivaActo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "aprMasivaActoIdSeq", sequenceName = "apr_masiva_acto_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aprMasivaActoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VAL_USUARIO_RESPONSABLE")
    @Size(max = 50)
    private String valUsuarioResponsable;
    @Column(name = "FEC_ENVIO_APROBACION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecEnvioAprobacion;
    @JoinColumn(name = "COD_ESTADO_APROBACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoAprobacion;
    @OneToMany(mappedBy = "aprobacionMasivaActo", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<ControlAprobacionActo> actosAdministrativos;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValUsuarioResponsable() {
        return valUsuarioResponsable;
    }

    public void setValUsuarioResponsable(String valUsuarioResponsable) {
        this.valUsuarioResponsable = valUsuarioResponsable;
    }

    public Calendar getFecEnvioAprobacion() {
        return fecEnvioAprobacion;
    }

    public void setFecEnvioAprobacion(Calendar fecEnvioAprobacion) {
        this.fecEnvioAprobacion = fecEnvioAprobacion;
    }

    public List<ControlAprobacionActo> getActosAdministrativos() {
        if (actosAdministrativos == null) {
            actosAdministrativos = new ArrayList<ControlAprobacionActo>();
        }
        return actosAdministrativos;
    }

    public ValorDominio getCodEstadoAprobacion() {
        return codEstadoAprobacion;
    }

    public void setCodEstadoAprobacion(ValorDominio codEstadoAprobacion) {
        this.codEstadoAprobacion = codEstadoAprobacion;
    }
}
