package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "APROBACION_VALIDACION_DOC")
public class AprobacionValidacionDocumento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "aprobacionvalidacdocumenidseq", sequenceName = "aproba_validac_documen_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "aprobacionvalidacdocumenidseq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_APROBACION_DOCUMENTO", referencedColumnName = "ID_ARCHIVO")
     @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento aprobacionDocumento;
    @JoinColumn(name = "ID_VALIDAC_CREAC_DOCUMENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValidacionCreacionDocumento validacionCreacionDocumento;

    public AprobacionValidacionDocumento() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AprobacionDocumento getAprobacionDocumento() {
        return aprobacionDocumento;
    }

    public void setAprobacionDocumento(AprobacionDocumento aprobacionDocumento) {
        this.aprobacionDocumento = aprobacionDocumento;
    }

    public ValidacionCreacionDocumento getIdValidacionCreacionDocumento() {
        return validacionCreacionDocumento;
    }

    public void setIdValidacionCreacionDocumento(ValidacionCreacionDocumento validacionCreacionDocumento) {
        this.validacionCreacionDocumento = validacionCreacionDocumento;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.aprobacionValidacionDocumento[ id=" + id + " ]";
    }

}
