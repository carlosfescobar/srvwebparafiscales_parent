package co.gov.ugpp.parafiscales.persistence.entity;

import com.jcabi.immutable.Array;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "ARCHIVO")
public class Archivo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "archivoIdSeq", sequenceName = "archivo_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "archivoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VAL_NOMBRE_ARCHIVO")
    @Size(max = 255)
    private String valNombreArchivo;
    @Lob
    @Column(name = "VAL_CONTENIDO_ARCHIVO")
    @NotNull
    @Basic(optional = false)
    private byte[] valContenidoArchivo;
    @Lob
    @Column(name = "VAL_CONTENIDO_FIRMA")
    private byte[] valContenidoFirma;
    @Column(name = "COD_TIPO_MIME_ARCHIVO")
    @Size(max = 20)
    @Basic(optional = false)
    @NotNull
    private String codTipoMimeArchivo;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ControlLiquidadorArchivoTemporal> controlLiquidadorArchivoTemporal;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EntidadNegocioArchivoTemporal> archivosTemporalesEntidadNegocio;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NotificacionAnexoTemporal> archivosTemporalesNotificacion;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MecanismoAnexoTemporal> anexosTemporalesMecanismo;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FiscalizacionArchivoTemp> archivosTemporalesFiscalizacion;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecursoReconArchivoTemp> archivosTemporalesRecursoReconsideracion;

    public Archivo() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValNombreArchivo() {
        return valNombreArchivo;
    }

    public void setValNombreArchivo(String valNombreArchivo) {
        this.valNombreArchivo = valNombreArchivo;
    }

    public byte[] getValContenidoArchivo() {
        return valContenidoArchivo;
    }

    public void setValContenidoArchivo(byte[] valContenidoArchivo) {
        this.valContenidoArchivo = valContenidoArchivo;
    }

    public byte[] getValContenidoFirma() {
        return valContenidoFirma;
    }

    public void setValContenidoFirma(byte[] valContenidoFirma) {
        this.valContenidoFirma = valContenidoFirma;
    }

    public String getCodTipoMimeArchivo() {
        return codTipoMimeArchivo;
    }

    public void setCodTipoMimeArchivo(String codTipoMimeArchivo) {
        this.codTipoMimeArchivo = codTipoMimeArchivo;
    }

    public List<ControlLiquidadorArchivoTemporal> getControlLiquidadorArchivoTemporal() {
        if (controlLiquidadorArchivoTemporal == null) {
            controlLiquidadorArchivoTemporal = new ArrayList<ControlLiquidadorArchivoTemporal>();
        }
        return controlLiquidadorArchivoTemporal;
    }

    public List<EntidadNegocioArchivoTemporal> getArchivosTemporalesEntidadNegocio() {
        if (archivosTemporalesEntidadNegocio == null) {
            archivosTemporalesEntidadNegocio = new ArrayList<EntidadNegocioArchivoTemporal>();
        }
        return archivosTemporalesEntidadNegocio;
    }

    public List<NotificacionAnexoTemporal> getArchivosTemporalesNotificacion() {
        if (archivosTemporalesNotificacion == null) {
            archivosTemporalesNotificacion = new Array<NotificacionAnexoTemporal>();
        }
        return archivosTemporalesNotificacion;
    }

    public List<MecanismoAnexoTemporal> getAnexosTemporalesMecanismo() {
        if (anexosTemporalesMecanismo == null) {
            anexosTemporalesMecanismo = new ArrayList<MecanismoAnexoTemporal>();
        }
        return anexosTemporalesMecanismo;
    }

    public List<FiscalizacionArchivoTemp> getArchivosTemporalesFiscalizacion() {
        if (archivosTemporalesFiscalizacion == null) {
            archivosTemporalesFiscalizacion = new ArrayList<FiscalizacionArchivoTemp>();

        }
        return archivosTemporalesFiscalizacion;
    }

    public List<RecursoReconArchivoTemp> getArchivosTemporalesRecursoReconsideracion() {
        if (archivosTemporalesRecursoReconsideracion == null) {
            archivosTemporalesRecursoReconsideracion = new ArrayList<RecursoReconArchivoTemp>();
        }
        return archivosTemporalesRecursoReconsideracion;
    }

}
