package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 * Clase que define la entidad Auditoria, en esta se guarda la informacion de la transaccion.
 *
 * @author franzjr
 * @author rpadilla
 */
@Entity
@Table(name = "COH_AUDITORIA_SERVICIOS")
@NamedQueries({
    @NamedQuery(name = "Auditoria.findByIdTrx",
            query = "SELECT audit FROM Auditoria audit WHERE audit.idTx = :idTrx ORDER BY audit.id DESC")})
public class Auditoria implements IEntity<Long> {

    @Id
    @SequenceGenerator(name = "auditoriaIdSeq", sequenceName = "auditoria_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auditoriaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_AUDITORIA", updatable = false)
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TRANSACCION")
    private String idTx;

    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHA_INICIO_TRANSACCION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaInicioTx;
    
    @Column(name = "FECHA_FIN_TRANSACCION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaFinTx;

    @Column(name = "ID_INSTANCIA_PROCESO")
    private String idInstanciaProceso;

    @Column(name = "ID_DEFINICION_PROCESO")
    private String idDefinicionProceso;

    @Column(name = "NOMBRE_DEFINICION_PROCESO")
    private String valNombreDefinicionProceso;

    @Column(name = "ID_INSTANCIA_ACTIVIDAD")
    private String idInstanciaActividad;

    @Column(name = "NOMBRE_INSTANCIA_ACTIVIDAD")
    private String valNombreDefinicionActividad;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USUARIO_APLICACION")
    private String idUsuarioAplicacion;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_USUARIO")
    private String idUsuario;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_EMISOR")
    private String idEmisor;

    @Lob
    @Basic(optional = false, fetch = LAZY)
    @NotNull
    @Column(name = "DESC_SOAP_SOLICITUD")
    private byte[] valContextoTransaccionalSOAP;

    @Lob
    @Basic(fetch = LAZY)
    @Column(name = "DESC_SOAP_RESPUESTA")
    private byte[] valContextoRespuestaSOAP;

    public Auditoria() {
    }

    public Auditoria(String idTx, String idInstanciaProceso, String idDefinicionProceso,
            String valNombreDefinicionProceso, String idInstanciaActividad, String valNombreDefinicionActividad,
            String idUsuarioAplicacion, String idUsuario, String idEmisor) {
        this.idTx = idTx;
        this.idInstanciaProceso = idInstanciaProceso;
        this.idDefinicionProceso = idDefinicionProceso;
        this.valNombreDefinicionProceso = valNombreDefinicionProceso;
        this.idInstanciaActividad = idInstanciaActividad;
        this.valNombreDefinicionActividad = valNombreDefinicionActividad;
        this.idUsuarioAplicacion = idUsuarioAplicacion;
        this.idUsuario = idUsuario;
        this.idEmisor = idEmisor;
    }

    public byte[] getValContextoTransaccionalSOAP() {
        return valContextoTransaccionalSOAP;
    }

    public void setValContextoTransaccionalSOAP(byte[] valContextoTransaccionalSOAP) {
        this.valContextoTransaccionalSOAP = valContextoTransaccionalSOAP;
    }

    public byte[] getValContextoRespuestaSOAP() {
        return valContextoRespuestaSOAP;
    }

    public void setValContextoRespuestaSOAP(byte[] valContextoRespuestaSOAP) {
        this.valContextoRespuestaSOAP = valContextoRespuestaSOAP;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdTx() {
        return idTx;
    }

    public void setIdTx(String idTx) {
        this.idTx = idTx;
    }

    public Calendar getFechaInicioTx() {
        return fechaInicioTx;
    }

    public void setFechaInicioTx(Calendar fechaInicioTx) {
        this.fechaInicioTx = fechaInicioTx;
    }
    
    public Calendar getFechaFinTx() {
        return fechaFinTx;
    }

    public void setFechaFinTx(Calendar fechaFinTx) {
        this.fechaFinTx = fechaFinTx;
    }

    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    public void setIdInstanciaProceso(String idInstanciaProceso) {
        this.idInstanciaProceso = idInstanciaProceso;
    }

    public String getIdDefinicionProceso() {
        return idDefinicionProceso;
    }

    public void setIdDefinicionProceso(String idDefinicionProceso) {
        this.idDefinicionProceso = idDefinicionProceso;
    }

    public String getValNombreDefinicionProceso() {
        return valNombreDefinicionProceso;
    }

    public void setValNombreDefinicionProceso(String valNombreDefinicionProceso) {
        this.valNombreDefinicionProceso = valNombreDefinicionProceso;
    }

    public String getIdInstanciaActividad() {
        return idInstanciaActividad;
    }

    public void setIdInstanciaActividad(String idInstanciaActividad) {
        this.idInstanciaActividad = idInstanciaActividad;
    }

    public String getValNombreDefinicionActividad() {
        return valNombreDefinicionActividad;
    }

    public void setValNombreDefinicionActividad(String valNombreDefinicionActividad) {
        this.valNombreDefinicionActividad = valNombreDefinicionActividad;
    }

    public String getIdUsuarioAplicacion() {
        return idUsuarioAplicacion;
    }

    public void setIdUsuarioAplicacion(String idUsuarioAplicacion) {
        this.idUsuarioAplicacion = idUsuarioAplicacion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdEmisor() {
        return idEmisor;
    }

    public void setIdEmisor(String idEmisor) {
        this.idEmisor = idEmisor;
    }

}
