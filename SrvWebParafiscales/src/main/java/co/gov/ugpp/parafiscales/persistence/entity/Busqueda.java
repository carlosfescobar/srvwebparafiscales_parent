package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "BUSQUEDA")
public class Busqueda extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "busquedaIdSeq", sequenceName = "busqueda_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "busquedaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 20)
    @JoinColumn(name = "COD_TIPO_BUSQUEDA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoBusqueda;
    @Size(max = 20)
    @Column(name = "FECHA_PROG_VISITA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaProgVisita;
    @Column(name = "VAL_USUARIO")
    private String valUsuario;
    @JoinColumn(name = "AUTO_COMISORIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DocumentoEcm autoComisorio;
    @JoinColumn(name = "COD_RESULTADO_BUSQUEDA ", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codResultadoBusqeda;
    @Column(name = " DES_OBSERVACION_PROG_VISITA")
    private String desObservacionProgVisita;
    @Column(name = "VAL_PLAZO_ENTREGA")
    private String valPlazoEntrega;
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;
    @Column(name = "VAL_PERSONA_CONTACTADA")
    private String valPersonaContactada;
    @Column(name = "VAL_CARGO_PERSONA_CONTACTADA")
    private String valCargoPersonaContactada;
    
    
    @Column(name = "VAL_TELEFONO_APORTANTE")
    private String valTelefonoAportante;
    
    @Column(name = "VAL_ETAPA")
    private String valEtapa;
    
    @Column(name = "VAL_NOMBRE_FUNCIONARIO")
    private String valNombreFuncionario;
    
    @Column(name = "VAL_EXTENSION")
    private String valExtension;

    
    
    
    
    
    public String getValTelefonoAportante() {
        return valTelefonoAportante;
    }

    public void setValTelefonoAportante(String valTelefonoAportante) {
        this.valTelefonoAportante = valTelefonoAportante;
    }

    public String getValEtapa() {
        return valEtapa;
    }

    public void setValEtapa(String valEtapa) {
        this.valEtapa = valEtapa;
    }

    public String getValNombreFuncionario() {
        return valNombreFuncionario;
    }

    public void setValNombreFuncionario(String valNombreFuncionario) {
        this.valNombreFuncionario = valNombreFuncionario;
    }

    public String getValExtension() {
        return valExtension;
    }

    public void setValExtension(String valExtension) {
        this.valExtension = valExtension;
    }
    
    
    public Busqueda() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    } 

    public Calendar getFechaProgVisita() {
        return fechaProgVisita;
    } 

    public void setFechaProgVisita(Calendar fechaProgVisita) {
        this.fechaProgVisita = fechaProgVisita;
    }            
            
    public String getValUsuario() {
        return valUsuario;
    } 

    public void setValUsuario(String valUsuario) {
        this.valUsuario = valUsuario;
    }

    public DocumentoEcm getAutoComisorio() {
        return autoComisorio;
    }

    public void setAutoComisorio(DocumentoEcm autoComisorio) {
        this.autoComisorio = autoComisorio;
    }

    public ValorDominio getCodResultadoBusqeda() {
        return codResultadoBusqeda;
    }

    public void setCodResultadoBusqeda(ValorDominio codResultadoBusqeda) {
        this.codResultadoBusqeda = codResultadoBusqeda;
    }

    public String getDesObservacionProgVisita() {
        return desObservacionProgVisita;
    }

    public void setDesObservacionProgVisita(String desObservacionProgVisita) {
        this.desObservacionProgVisita = desObservacionProgVisita;
    }

    public String getValPlazoEntrega() {
        return valPlazoEntrega;
    }

    public void setValPlazoEntrega(String valPlazoEntrega) {
        this.valPlazoEntrega = valPlazoEntrega;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    public void setValPersonaContactada(String valPersonaContactada) {
        this.valPersonaContactada = valPersonaContactada;
    }

    public String getValCargoPersonaContactada() {
        return valCargoPersonaContactada;
    }

    public void setValCargoPersonaContactada(String valCargoPersonaContactada) {
        this.valCargoPersonaContactada = valCargoPersonaContactada;
    }

    public ValorDominio getCodTipoBusqueda() {
        return codTipoBusqueda;
    }

    public void setCodTipoBusqueda(ValorDominio codTipoBusqueda) {
        this.codTipoBusqueda = codTipoBusqueda;
    }

    public void setValUsuario(FuncionarioTipo funcionarioTipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
