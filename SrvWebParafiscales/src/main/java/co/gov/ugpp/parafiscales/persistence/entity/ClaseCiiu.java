package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "CLASE_CIIU")
public class ClaseCiiu implements IEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "DESC_CLASE")
    private String descClase;
    @JoinColumn(name = "ID_GRUPO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private GrupoCiiu grupoCiiu;

    public ClaseCiiu() {
    }

    public ClaseCiiu(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getDescClase() {
        return descClase;
    }

    public void setDescClase(String descClase) {
        this.descClase = descClase;
    }

    public GrupoCiiu getGrupoCiiu() {
        return grupoCiiu;
    }

    public void setGrupoCiiu(GrupoCiiu grupoCiiu) {
        this.grupoCiiu = grupoCiiu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ClaseCiiu)) {
            return false;
        }
        ClaseCiiu other = (ClaseCiiu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
