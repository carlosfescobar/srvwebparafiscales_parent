package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "CLASIFICACION_PERSONA")
@NamedQueries({
    @NamedQuery(name = "clasificacionPersona.findByClasificacionAndPersona",
            query = "SELECT cp FROM ClasificacionPersona cp WHERE cp.codClasificacionPersona.id = :codClasificacionPersona AND cp.persona.id = :idPersona ORDER BY cp.fechaCreacion DESC")})
public class ClasificacionPersona extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "clasificacionPersonaIdSeq", sequenceName = "clasificacion_persona_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "clasificacionPersonaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona persona;
    @JoinColumn(name = "COD_TIPO_PERSONA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoPersona;
    @JoinColumn(name = "COD_CLASIFICACION_PERSONA", referencedColumnName = "COD_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private Dominio codClasificacionPersona;

    public ClasificacionPersona() {
    }

    public ClasificacionPersona(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public ValorDominio getCodTipoPersona() {
        return codTipoPersona;
    }

    public void setCodTipoPersona(ValorDominio codTipoPersona) {
        this.codTipoPersona = codTipoPersona;
    }

    public Dominio getCodClasificacionPersona() {
        return codClasificacionPersona;
    }

    public void setCodClasificacionPersona(Dominio codClasificacionPersona) {
        this.codClasificacionPersona = codClasificacionPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ClasificacionPersona)) {
            return false;
        }
        ClasificacionPersona other = (ClasificacionPersona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Departamento[ id=" + id + " ]";
    }

}
