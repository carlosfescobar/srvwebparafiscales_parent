package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "COB_FLEX")
@XmlRootElement
public class CobFlex extends AbstractEntity<Long>
{

   private static final long serialVersionUID = 2782700627242211320L;

   @Id
   @Basic(optional = false)
   @NotNull
   @Column(name = "ID_FLEX")
   private Long id;

   @Column(name = "PORCENTAJE_FLEX")
   private Integer porcentajeFlex;

   @Column(name = "FECHA_INCIAL")
   @Temporal(TemporalType.TIMESTAMP)
   private Calendar fechaInicial;

   @Column(name = "FECHA_FINAL")
   @Temporal(TemporalType.TIMESTAMP)
   private Calendar fechaFinal;

   @Column(name = "OPERADOR")
   private char estado;

   public CobFlex()
   {
   }

   public CobFlex(Long id)
   {
      this.id = id;
   }

   public Long getId()
   {
      return id;
   }

   public void setId(Long id)
   {
      this.id = id;
   }

   public Integer getPorcentajeFlex()
   {
      return porcentajeFlex;
   }

   public void setPorcentajeFlex(Integer porcentajeFlex)
   {
      this.porcentajeFlex = porcentajeFlex;
   }

   public Calendar getFechaInicial()
   {
      return fechaInicial;
   }

   public void setFechaInicial(Calendar fechaInicial)
   {
      this.fechaInicial = fechaInicial;
   }

   public Calendar getFechaFinal()
   {
      return fechaFinal;
   }

   public void setFechaFinal(Calendar fechaFinal)
   {
      this.fechaFinal = fechaFinal;
   }

   public char getEstado()
   {
      return estado;
   }

   public void setEstado(char estado)
   {
      this.estado = estado;
   }

}
