package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "COB_PARAM_GENERAL")
@XmlRootElement
public class CobParamGeneral {//extends AbstractEntity<Long> {

    @SuppressWarnings("unused")
	private static final long serialVersionUID = 3724712104110365204L;

	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID_PARAM_GENERAL")
	private Long id;

	@Column(name = "NOMBRE_PARAM_GENERAL")
	private String nombreParamGeneral;

	@Column(name = "DESCRIPCION")
	private String descripcion;

	@Column(name = "VALOR")
	private Integer valor;

	@Column(name = "UNIDAD_VALOR")
	private Integer unidadValor;

	@Column(name = "FECHA_INICIAL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicial;

	
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "FECHA_FINAL")
   private Date fechaFinal;

	@Column(name = "ESTADO")
	private char estado;

	@Column(name = "COD_PARAMETRO")
	private String codParametro;

	@Column(name = "ID_PARAMETRO")
	private String idParametro;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombreParamGeneral() {
		return nombreParamGeneral;
	}

	public void setNombreParamGeneral(String nombreParamGeneral) {
		this.nombreParamGeneral = nombreParamGeneral;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Integer getUnidadValor() {
		return unidadValor;
	}

	public void setUnidadValor(Integer unidadValor) {
		this.unidadValor = unidadValor;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

	public String getCodParametro() {
		return codParametro;
	}

	public void setCodParametro(String codParametro) {
		this.codParametro = codParametro;
	}
	
	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getIdParametro() {
		return idParametro;
	}

	public void setIdParametro(String idParametro) {
		this.idParametro = idParametro;
	}

}
