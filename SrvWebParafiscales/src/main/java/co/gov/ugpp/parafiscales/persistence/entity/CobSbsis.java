package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "COB_SBSIS")
@XmlRootElement
public class CobSbsis {// extends AbstractEntity<Long> {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 3724712104110655204L;

	@Id
	@Basic(optional = false)
	@NotNull
	@Column(name = "ID_SBSIS")
	private Long id;

	@Column(name = "TIP_PARAM_SBSIS")
	private Integer tipoParamSbsis;

	@Column(name = "CODIGO_SBSIS")
	private Integer codigoSbsis;

	@Column(name = "FECHA_INCIAL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaInicial;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FECHA_FINAL")
	private Date fechaFinal;

	@Column(name = "RANGO_INICIAL")
	private Integer rangoInicial;

	@Column(name = "RANGO_FINAL")
	private Integer rangoFinal;

	@Column(name = "PORCENTAJE_SBSIS")
	private Integer porcentajeSbsis;

	@Column(name = "VALOR_TOPE")
	private Integer valor;

	@Column(name = "PORCENTAJE_INICIAL")
	private Integer porcentajeInicial;

	@Column(name = "PORCENTAJE_FINAL")
	private Integer porcentajeFinal;

	@Column(name = "ESTADO")
	private char estado;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getTipoParamSbsis() {
		return tipoParamSbsis;
	}

	public void setTipoParamSbsis(Integer tipoParamSbsis) {
		this.tipoParamSbsis = tipoParamSbsis;
	}

	public Integer getCodigoSbsis() {
		return codigoSbsis;
	}

	public void setCodigoSbsis(Integer codigoSbsis) {
		this.codigoSbsis = codigoSbsis;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public Date getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public Integer getRangoInicial() {
		return rangoInicial;
	}

	public void setRangoInicial(Integer rangoInicial) {
		this.rangoInicial = rangoInicial;
	}

	public Integer getRangoFinal() {
		return rangoFinal;
	}

	public void setRangoFinal(Integer rangoFinal) {
		this.rangoFinal = rangoFinal;
	}

	public Integer getPorcentajeSbsis() {
		return porcentajeSbsis;
	}

	public void setPorcentajeSbsis(Integer porcentajeSbsis) {
		this.porcentajeSbsis = porcentajeSbsis;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public Integer getPorcentajeInicial() {
		return porcentajeInicial;
	}

	public void setPorcentajeInicial(Integer porcentajeInicial) {
		this.porcentajeInicial = porcentajeInicial;
	}

	public Integer getPorcentajeFinal() {
		return porcentajeFinal;
	}

	public void setPorcentajeFinal(Integer porcentajeFinal) {
		this.porcentajeFinal = porcentajeFinal;
	}

	public char getEstado() {
		return estado;
	}

	public void setEstado(char estado) {
		this.estado = estado;
	}

}
