/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_CONCEPTO_CONTABLE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ConceptoContable.findByCAI", query = "SELECT c FROM ConceptoContable c WHERE c.cuenta like :ncc and c.idnominadetalle.nomina.id = :idN and c.idnominadetalle.ano = :ano")
})
public class ConceptoContable extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="conceptocontable_seq", sequenceName="conceptocontable_seq_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conceptocontable_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_CONCEPTO_UGPP")
    private String nombreConceptoUgpp;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SUBSISTEMAS")
    private String subsistemas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CUENTA")
    private String cuenta;
    @Size(max = 200)
    @Column(name = "NOMBRE_CONCEPTO_APORTANTE")
    private String nombreConceptoAportante;
    @Size(max = 200)
    @Column(name = "NOMBRE_COLUMNA")
    private String nombreColumna;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "IDNOMINADETALLE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private NominaDetalle idnominadetalle;

    
    
    
    
    
    
        
    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    

    @Size(max = 500)
    @Column(name = "JUSTIFICACION_CAMBIO_UGPP")
    private String justificacionCambioUgpp;
    
 
    @Size(max = 200)
    @Column(name = "TIPO_PAGO_UGPP")
    private String tipoPagoUgpp;
    

    @Size(max = 200)
    @Column(name = "CUENTA_CONTABLE")
    private String cuentaContable;

    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
    
    
    
    
    
    
    
    
    
    
    public ConceptoContable() {
    }

    public ConceptoContable(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSubsistemas() {
        return subsistemas;
    }

    public void setSubsistemas(String subsistemas) {
        this.subsistemas = subsistemas;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombreColumna() {
        return nombreColumna;
    }

    public void setNombreColumna(String nombreColumna) {
        this.nombreColumna = nombreColumna;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConceptoContable)) {
            return false;
        }
        ConceptoContable other = (ConceptoContable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.ConceptoContable[ id=" + id + " ]";
    }

    /**
     * @return the idnominadetalle
     */
    public NominaDetalle getIdnominadetalle() {
        return idnominadetalle;
    }

    /**
     * @param idnominadetalle the idnominadetalle to set
     */
    public void setIdnominadetalle(NominaDetalle idnominadetalle) {
        this.idnominadetalle = idnominadetalle;
    }

    /**
     * @return the nombreConceptoUgpp
     */
    public String getNombreConceptoUgpp() {
        return nombreConceptoUgpp;
    }

    /**
     * @param nombreConceptoUgpp the nombreConceptoUgpp to set
     */
    public void setNombreConceptoUgpp(String nombreConceptoUgpp) {
        this.nombreConceptoUgpp = nombreConceptoUgpp;
    }

    /**
     * @return the nombreConceptoAportante
     */
    public String getNombreConceptoAportante() {
        return nombreConceptoAportante;
    }

    /**
     * @param nombreConceptoAportante the nombreConceptoAportante to set
     */
    public void setNombreConceptoAportante(String nombreConceptoAportante) {
        this.nombreConceptoAportante = nombreConceptoAportante;
    }

    /**
     * @return the justificacionCambioUgpp
     */
    public String getJustificacionCambioUgpp() {
        return justificacionCambioUgpp;
    }

    /**
     * @param justificacionCambioUgpp the justificacionCambioUgpp to set
     */
    public void setJustificacionCambioUgpp(String justificacionCambioUgpp) {
        this.justificacionCambioUgpp = justificacionCambioUgpp;
    }

    /**
     * @return the tipoPagoUgpp
     */
    public String getTipoPagoUgpp() {
        return tipoPagoUgpp;
    }

    /**
     * @param tipoPagoUgpp the tipoPagoUgpp to set
     */
    public void setTipoPagoUgpp(String tipoPagoUgpp) {
        this.tipoPagoUgpp = tipoPagoUgpp;
    }

    /**
     * @return the cuentaContable
     */
    public String getCuentaContable() {
        return cuentaContable;
    }

    /**
     * @param cuentaContable the cuentaContable to set
     */
    public void setCuentaContable(String cuentaContable) {
        this.cuentaContable = cuentaContable;
    }

}
