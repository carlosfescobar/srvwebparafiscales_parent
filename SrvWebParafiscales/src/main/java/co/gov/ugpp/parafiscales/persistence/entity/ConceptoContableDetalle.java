/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_CONCEPTO_CONTABLE_DETALLE")
@XmlRootElement
public class ConceptoContableDetalle extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="conceptocdetalle_seq", sequenceName="conceptocdetalle_seq_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conceptocdetalle_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    
    @JoinColumn(name = "IDNOMINADETALLE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private NominaDetalle idnominadetalle;

    
    @Column(name = "IDSUBSISTEMA")
    private BigDecimal idSubsistema;

    
    @Size(max = 40)
    @Column(name = "TIPO_PAGO_UGPP")
    private String tipoPagoUgpp;

    
    @Column(name = "VALOR")
    private BigDecimal valor;
    
        

    
    public ConceptoContableDetalle() {
    }

    public ConceptoContableDetalle(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

   

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.ConceptoContableDetalle[ id=" + id + " ]";
    }

    
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConceptoContable)) {
            return false;
        }
        ConceptoContableDetalle other = (ConceptoContableDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    
    
    
    
    
    /**
     * @return the idnominadetalle
     */
    public NominaDetalle getIdnominadetalle() {
        return idnominadetalle;
    }

    /**
     * @param idnominadetalle the idnominadetalle to set
     */
    public void setIdnominadetalle(NominaDetalle idnominadetalle) {
        this.idnominadetalle = idnominadetalle;
    }

   
    /**
     * @return the tipoPagoUgpp
     */
    public String getTipoPagoUgpp() {
        return tipoPagoUgpp;
    }

    /**
     * @param tipoPagoUgpp the tipoPagoUgpp to set
     */
    public void setTipoPagoUgpp(String tipoPagoUgpp) {
        this.tipoPagoUgpp = tipoPagoUgpp;
    }

    /**
     * @return the idSubsistema
     */
    public BigDecimal getIdSubsistema() {
        return idSubsistema;
    }

    /**
     * @param idSubsistema the idSubsistema to set
     */
    public void setIdSubsistema(BigDecimal idSubsistema) {
        this.idSubsistema = idSubsistema;
    }

    /**
     * @return the valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

   

}
