package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Mauricio Guerrero
 */
@Entity
@Table(name = "CONCEPTO_CONTABLE_HCL_DETALLE")
public class ConceptoContableHclDetalle implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqconconthcldet")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CODIGO")
    private String codigo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "SUBSISTEMAS")
    private String subsistemas;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "CUENTA")
    private String cuenta;
    @Size(max = 200)
    @Column(name = "NOMBRE_CONCEPTO")
    private String nombreConcepto;
    @Column(name = "VALOR")
    private BigDecimal valor;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "IDHOJACALCULOLIQUIDACIONDET", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private HojaCalculoLiquidacionDetalle idhojacalculoliquidaciondet;

    public ConceptoContableHclDetalle() {
    }

    public ConceptoContableHclDetalle(BigDecimal id) {
        this.id = id;
    }

    public ConceptoContableHclDetalle(BigDecimal id, String codigo, String nombre, String subsistemas, String cuenta) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.subsistemas = subsistemas;
        this.cuenta = cuenta;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSubsistemas() {
        return subsistemas;
    }

    public void setSubsistemas(String subsistemas) {
        this.subsistemas = subsistemas;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombreConcepto() {
        return nombreConcepto;
    }

    public void setNombreConcepto(String nombreConcepto) {
        this.nombreConcepto = nombreConcepto;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public HojaCalculoLiquidacionDetalle getIdhojacalculoliquidaciondet() {
        return idhojacalculoliquidaciondet;
    }

    public void setIdhojacalculoliquidaciondet(HojaCalculoLiquidacionDetalle idhojacalculoliquidaciondet) {
        this.idhojacalculoliquidaciondet = idhojacalculoliquidaciondet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ConceptoContableHclDetalle)) {
            return false;
        }
        ConceptoContableHclDetalle other = (ConceptoContableHclDetalle) object;
        if (this.id == null && other.id != null || this.id != null && !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.entidades.negocio.ConceptoContableHclDetalle[ id=" + id + " ]";
    }

}
