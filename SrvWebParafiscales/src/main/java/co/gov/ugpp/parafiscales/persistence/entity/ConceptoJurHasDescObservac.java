
package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "CONCEPTO_JUR_HAS_DESC_OBSERVAC")
public class ConceptoJurHasDescObservac implements IEntity<Long> {
    
    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "conceptoObsIdSeq", sequenceName = "concepto_jur_has_desc_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conceptoObsIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "DESC_OBSERVACIONES_ID")
    private Long descObservacionesId;
    
    @Size(max = 255)
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;
    
    @JoinColumn(name = "ID_CONCEPTO_JURIDICO", referencedColumnName = "ID_CONCEPTO_JURIDICO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConceptoJuridico idConceptoJuridico;

    public ConceptoJurHasDescObservac() {
    }

    public ConceptoJurHasDescObservac(Long descObservacionesId) {
        this.descObservacionesId = descObservacionesId;
    }

    public Long getDescObservacionesId() {
        return descObservacionesId;
    }

    public void setDescObservacionesId(Long descObservacionesId) {
        this.descObservacionesId = descObservacionesId;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public ConceptoJuridico getIdConceptoJuridico() {
        return idConceptoJuridico;
    }

    public void setIdConceptoJuridico(ConceptoJuridico idConceptoJuridico) {
        this.idConceptoJuridico = idConceptoJuridico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (descObservacionesId != null ? descObservacionesId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConceptoJurHasDescObservac)) {
            return false;
        }
        ConceptoJurHasDescObservac other = (ConceptoJurHasDescObservac) object;
        if ((this.descObservacionesId == null && other.descObservacionesId != null) || (this.descObservacionesId != null && !this.descObservacionesId.equals(other.descObservacionesId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ConceptoJurHasDescObservac[ descObservacionesId=" + descObservacionesId + " ]";
    }

    @Override
    public Long getId() {
        return descObservacionesId;
    }
    
}
