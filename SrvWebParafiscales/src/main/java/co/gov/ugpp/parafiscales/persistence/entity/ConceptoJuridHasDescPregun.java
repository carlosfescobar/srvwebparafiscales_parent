package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "CONCEPTO_JURID_HAS_DESC_PREGUN")
public class ConceptoJuridHasDescPregun extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "conceptoJurIdSeq", sequenceName = "concepto_jurid_has_desc_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conceptoJurIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "DESC_PREGUNTAS_ID")
    private Long descPreguntasId;

    @Size(max = 255)
    @Column(name = "DESC_PREGUNTAS")
    private String descPreguntas;

    @JoinColumn(name = "ID_CONCEPTO_JURIDICO", referencedColumnName = "ID_CONCEPTO_JURIDICO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConceptoJuridico idConceptoJuridico;

    public ConceptoJuridHasDescPregun() {
    }

    public ConceptoJuridHasDescPregun(Long descPreguntasId) {
        this.descPreguntasId = descPreguntasId;
    }

    public Long getDescPreguntasId() {
        return descPreguntasId;
    }

    public void setDescPreguntasId(Long descPreguntasId) {
        this.descPreguntasId = descPreguntasId;
    }

    public String getDescPreguntas() {
        return descPreguntas;
    }

    public void setDescPreguntas(String descPreguntas) {
        this.descPreguntas = descPreguntas;
    }

    public ConceptoJuridico getIdConceptoJuridico() {
        return idConceptoJuridico;
    }

    public void setIdConceptoJuridico(ConceptoJuridico idConceptoJuridico) {
        this.idConceptoJuridico = idConceptoJuridico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (descPreguntasId != null ? descPreguntasId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConceptoJuridHasDescPregun)) {
            return false;
        }
        ConceptoJuridHasDescPregun other = (ConceptoJuridHasDescPregun) object;
        if ((this.descPreguntasId == null && other.descPreguntasId != null) || (this.descPreguntasId != null && !this.descPreguntasId.equals(other.descPreguntasId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridHasDescPregun[ descPreguntasId=" + descPreguntasId + " ]";
    }

    @Override
    public Long getId() {
        return descPreguntasId;
    }

}
