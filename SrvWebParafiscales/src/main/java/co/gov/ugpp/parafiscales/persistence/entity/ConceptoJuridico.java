package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "CONCEPTO_JURIDICO")
@NamedQueries({
    @NamedQuery(name = "ConceptoJuridico.findByExpediente", query = "SELECT concepto FROM ConceptoJuridico concepto WHERE concepto.expediente.id = :expedienteNumero")})
public class ConceptoJuridico extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "conceptoJuridicoIdSeq", sequenceName = "concepto_juridico_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "conceptoJuridicoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_CONCEPTO_JURIDICO")
    private Long idConceptoJuridico;

    @JoinColumn(name = "ID_NUMERO_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expediente;

    @Size(max = 255)
    @Column(name = "DESC_ASUNTO_CONCEPTO_JURIDICO")
    private String descAsuntoConceptoJuridico;

    @JoinColumn(name = "COD_PRIORIDAD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codPrioridad;

    @Size(max = 255)
    @Column(name = "DESC_MOTIVO_PRIORIDAD")
    private String descMotivoPrioridad;

    @JoinColumn(name = "DOCUMENTO_ID", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentoEcm documentoEcm;

    @OneToMany(mappedBy = "idConceptoJuridico", fetch = FetchType.LAZY)
    private List<ConceptoJuridHasDescPregun> conceptoJuridHasDescPregunList;

    @OneToMany(mappedBy = "idConceptoJuridico", fetch = FetchType.LAZY)
    private List<ConceptoJurHasDescObservac> conceptoJurHasDescObservacList;

    public ConceptoJuridico() {
    }

    public ConceptoJuridico(Long idConceptoJuridico) {
        this.idConceptoJuridico = idConceptoJuridico;
    }

    public Long getIdConceptoJuridico() {
        return idConceptoJuridico;
    }

    public void setIdConceptoJuridico(Long idConceptoJuridico) {
        this.idConceptoJuridico = idConceptoJuridico;
    }

    public String getDescAsuntoConceptoJuridico() {
        return descAsuntoConceptoJuridico;
    }

    public void setDescAsuntoConceptoJuridico(String descAsuntoConceptoJuridico) {
        this.descAsuntoConceptoJuridico = descAsuntoConceptoJuridico;
    }

    public String getDescMotivoPrioridad() {
        return descMotivoPrioridad;
    }

    public void setDescMotivoPrioridad(String descMotivoPrioridad) {
        this.descMotivoPrioridad = descMotivoPrioridad;
    }

    public List<ConceptoJuridHasDescPregun> getConceptoJuridHasDescPregunList() {
        return conceptoJuridHasDescPregunList;
    }

    public void setConceptoJuridHasDescPregunList(List<ConceptoJuridHasDescPregun> conceptoJuridHasDescPregunList) {
        this.conceptoJuridHasDescPregunList = conceptoJuridHasDescPregunList;
    }

    public List<ConceptoJurHasDescObservac> getConceptoJurHasDescObservacList() {
        return conceptoJurHasDescObservacList;
    }

    public void setConceptoJurHasDescObservacList(List<ConceptoJurHasDescObservac> conceptoJurHasDescObservacList) {
        this.conceptoJurHasDescObservacList = conceptoJurHasDescObservacList;
    }

    public Expediente getExpedienteId() {
        return expediente;
    }

    public void setExpedienteId(Expediente expedienteId) {
        this.expediente = expedienteId;
    }

    public ValorDominio getCodPrioridad() {
        return codPrioridad;
    }

    public void setCodPrioridad(ValorDominio codPrioridad) {
        this.codPrioridad = codPrioridad;
    }

    public DocumentoEcm getDocumentoEcm() {
        return documentoEcm;
    }

    public void setDocumentoEcm(DocumentoEcm documentoEcm) {
        this.documentoEcm = documentoEcm;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idConceptoJuridico != null ? idConceptoJuridico.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConceptoJuridico)) {
            return false;
        }
        ConceptoJuridico other = (ConceptoJuridico) object;
        if ((this.idConceptoJuridico == null && other.idConceptoJuridico != null) || (this.idConceptoJuridico != null && !this.idConceptoJuridico.equals(other.idConceptoJuridico))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridico[ idConceptoJuridico=" + idConceptoJuridico + " ]";
    }

    @Override
    public Long getId() {
        return idConceptoJuridico;
    }

}
