package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_CONCILIACION_CONTABLE")
public class ConciliacionContable extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "CONCILIACIONCONTABLESEQ", sequenceName = "CONCILIACIONCONTABLESEQID", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONCILIACIONCONTABLESEQ")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;

    @Column(name = "ESTADO")
    private String estado;

    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechacreacion;

    @Column(name = "IDINFORMACIONEXTERNADEFINITIVA")
    private Long idinformacionexternadefinitiva;

    @OneToMany(mappedBy = "idconcilicacioncontable", fetch = FetchType.LAZY)
    private List<ConciliacionContableDetalle> conciliacionContableDetalleList;

    @JoinColumn(name = "IDEXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente idexpediente;

    public ConciliacionContable() {
    }

    public ConciliacionContable(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Calendar getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Calendar fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public Long getIdinformacionexternadefinitiva() {
        return idinformacionexternadefinitiva;
    }

    public void setIdinformacionexternadefinitiva(Long idinformacionexternadefinitiva) {
        this.idinformacionexternadefinitiva = idinformacionexternadefinitiva;
    }

    @XmlTransient
    public List<ConciliacionContableDetalle> getConciliacionContableDetalleList() {
        return conciliacionContableDetalleList;
    }

    public void setConciliacionContableDetalleList(List<ConciliacionContableDetalle> conciliacionContableDetalleList) {
        this.conciliacionContableDetalleList = conciliacionContableDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConciliacionContable)) {
            return false;
        }
        ConciliacionContable other = (ConciliacionContable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable[ id=" + id + " ]";
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

}
