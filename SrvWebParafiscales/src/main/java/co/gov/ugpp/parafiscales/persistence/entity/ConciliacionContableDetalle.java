package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_CONCILIACION_CONTABLE_DET")
public class ConciliacionContableDetalle extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    @Column(name = "ANO")
    private BigInteger ano;
    
    @Size(max = 20)
    @Column(name = "NUMEROCUENTACONTABLE")
    private String numerocuentacontable;
    
    @Size(max = 150)
    @Column(name = "NOMBRECUENTACONTABLE")
    private String nombrecuentacontable;
    
    @Column(name = "SALDOINICIAL")
    private BigDecimal saldoinicial;
    
    @Column(name = "SALDOFINAL")
    private BigDecimal saldofinal;
    
    @Column(name = "VALORNETOMOVIMIENTO")
    private BigDecimal valornetomovimiento;
    
    @Column(name = "VALORNOMINA")
    private BigDecimal valornomina;
    
    @Column(name = "DIFERENCIA")
    private BigDecimal diferencia;
    
    @JoinColumn(name = "IDCONCILICACIONCONTABLE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConciliacionContable idconcilicacioncontable;

    public ConciliacionContableDetalle() {
    }

    public ConciliacionContableDetalle(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getAno() {
        return ano;
    }

    public void setAno(BigInteger ano) {
        this.ano = ano;
    }

    public String getNumerocuentacontable() {
        return numerocuentacontable;
    }

    public void setNumerocuentacontable(String numerocuentacontable) {
        this.numerocuentacontable = numerocuentacontable;
    }

    public String getNombrecuentacontable() {
        return nombrecuentacontable;
    }

    public void setNombrecuentacontable(String nombrecuentacontable) {
        this.nombrecuentacontable = nombrecuentacontable;
    }

    public BigDecimal getSaldoinicial() {
        return saldoinicial;
    }

    public void setSaldoinicial(BigDecimal saldoinicial) {
        this.saldoinicial = saldoinicial;
    }

    public BigDecimal getSaldofinal() {
        return saldofinal;
    }

    public void setSaldofinal(BigDecimal saldofinal) {
        this.saldofinal = saldofinal;
    }

    public BigDecimal getValornetomovimiento() {
        return valornetomovimiento;
    }

    public void setValornetomovimiento(BigDecimal valornetomovimiento) {
        this.valornetomovimiento = valornetomovimiento;
    }

    public BigDecimal getValornomina() {
        return valornomina;
    }

    public void setValornomina(BigDecimal valornomina) {
        this.valornomina = valornomina;
    }

    public BigDecimal getDiferencia() {
        return diferencia;
    }

    public void setDiferencia(BigDecimal diferencia) {
        this.diferencia = diferencia;
    }

    public ConciliacionContable getIdconcilicacioncontable() {
        return idconcilicacioncontable;
    }

    public void setIdconcilicacioncontable(ConciliacionContable idconcilicacioncontable) {
        this.idconcilicacioncontable = idconcilicacioncontable;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConciliacionContableDetalle)) {
            return false;
        }
        ConciliacionContableDetalle other = (ConciliacionContableDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContableDetalle[ id=" + id + " ]";
    }

}
