package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "CONSTANCIA_DECISIONES_RECURSO")
public class ConstanciaDecisionesRecurso extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "constanciaDecReqIdSeq", sequenceName = "constancia_dec_rec_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "constanciaDecReqIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConstanciaEjecutoria constanciaEjecutoria;
    @JoinColumn(name = "COD_DECISION_RECURSO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ValorDominio codDecisionRecurso;
    @Column(name = "ES_DECISION_RECURSO")
    private Boolean esDecisionRecurso;

    public ConstanciaDecisionesRecurso() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConstanciaEjecutoria getConstanciaEjecutoria() {
        return constanciaEjecutoria;
    }

    public void setConstanciaEjecutoria(ConstanciaEjecutoria constanciaEjecutoria) {
        this.constanciaEjecutoria = constanciaEjecutoria;
    }

    public ValorDominio getCodDecisionRecurso() {
        return codDecisionRecurso;
    }

    public void setCodDecisionRecurso(ValorDominio codDecisionRecurso) {
        this.codDecisionRecurso = codDecisionRecurso;
    }

    public Boolean getEsDecisionRecurso() {
        return esDecisionRecurso;
    }

    public void setEsDecisionRecurso(Boolean esDecisionRecurso) {
        this.esDecisionRecurso = esDecisionRecurso;
    }
}
