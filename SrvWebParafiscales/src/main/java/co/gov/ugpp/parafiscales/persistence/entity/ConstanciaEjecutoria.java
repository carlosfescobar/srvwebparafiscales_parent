package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "CONSTANCIA_EJECUTORIA")
public class ConstanciaEjecutoria extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "constanciaEjecutoriaIdSeq", sequenceName = "constancia_ejecutoria_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "constanciaEjecutoriaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "FEC_CONSTANCIA_EJECUTORIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecConstanciaEjecutoria;
    @Column(name = "FEC_CREACION_DOC_CONSTANCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecCreacionDocumentoConstancia;
    @JoinColumn(name = "ID_DOC_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentoEcm documentoConstanciaEjecutoria;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ConstanciaFechaRecurso> fechasRecurso;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ConstanciaDecisionesRecurso> decisionesRecurso;
    @JoinColumn(name = "COD_FALLO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codFallo;

    public ConstanciaEjecutoria() {
    }

    public ConstanciaEjecutoria(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getFecConstanciaEjecutoria() {
        return fecConstanciaEjecutoria;
    }

    public void setFecConstanciaEjecutoria(Calendar fecConstanciaEjecutoria) {
        this.fecConstanciaEjecutoria = fecConstanciaEjecutoria;
    }

    public DocumentoEcm getDocumentoConstanciaEjecutoria() {
        return documentoConstanciaEjecutoria;
    }

    public void setDocumentoConstanciaEjecutoria(DocumentoEcm documentoConstanciaEjecutoria) {
        this.documentoConstanciaEjecutoria = documentoConstanciaEjecutoria;
    }

    public Calendar getFecCreacionDocumentoConstancia() {
        return fecCreacionDocumentoConstancia;
    }

    public void setFecCreacionDocumentoConstancia(Calendar fecCreacionDocumentoConstancia) {
        this.fecCreacionDocumentoConstancia = fecCreacionDocumentoConstancia;
    }

    public List<ConstanciaFechaRecurso> getFechasRecurso() {
        if (fechasRecurso == null) {
            fechasRecurso = new ArrayList<ConstanciaFechaRecurso>();
        }
        return fechasRecurso;
    }

    public List<ConstanciaDecisionesRecurso> getDecisionesRecurso() {
        if (decisionesRecurso == null) {
            decisionesRecurso = new ArrayList<ConstanciaDecisionesRecurso>();
        }
        return decisionesRecurso;
    }

    public ValorDominio getCodFallo() {
        return codFallo;
    }

    public void setCodFallo(ValorDominio codFallo) {
        this.codFallo = codFallo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConstanciaEjecutoria)) {
            return false;
        }
        ConstanciaEjecutoria other = (ConstanciaEjecutoria) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Accion[ id=" + id + " ]";
    }

}
