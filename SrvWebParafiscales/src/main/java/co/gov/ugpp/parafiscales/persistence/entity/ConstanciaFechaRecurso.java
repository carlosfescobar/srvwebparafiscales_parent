package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "CONSTANCIA_FECHAS_RECURSO")
public class ConstanciaFechaRecurso extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "constanciaFecReqIdSeq", sequenceName = "constancia_fec_rec_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "constanciaFecReqIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConstanciaEjecutoria constanciaEjecutoria;
    @JoinColumn(name = "COD_TIPO_FEC_RECURSO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ValorDominio codTipoFecRecurso;
    @Column(name = "FEC_RECURSO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRecurso;

    public ConstanciaFechaRecurso() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ConstanciaEjecutoria getConstanciaEjecutoria() {
        return constanciaEjecutoria;
    }

    public void setConstanciaEjecutoria(ConstanciaEjecutoria constanciaEjecutoria) {
        this.constanciaEjecutoria = constanciaEjecutoria;
    }

    public ValorDominio getCodTipoFecRecurso() {
        return codTipoFecRecurso;
    }

    public void setCodTipoFecRecurso(ValorDominio codTipoFecRecurso) {
        this.codTipoFecRecurso = codTipoFecRecurso;
    }

    public Calendar getFecRecurso() {
        return fecRecurso;
    }

    public void setFecRecurso(Calendar fecRecurso) {
        this.fecRecurso = fecRecurso;
    }
}
