package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 *
 * @author rpadilla
 */
@Entity
@Cacheable(true)
@Cache(type = CacheType.HARD_WEAK)
@Table(name = "CONTACTO_PERSONA")
public class ContactoPersona extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "contactoPersonaIdSeq", sequenceName = "contacto_persona_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "contactoPersonaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ES_AUTORIZA_NOTIFICACION_ELECT")
    private Boolean esAutorizaNotificacionElectronica;
    @BatchFetch(value = BatchFetchType.EXISTS)
    @OneToMany(mappedBy = "contactoPersona", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("fechaCreacion DESC")
    private List<Telefono> telefonoList;
    @BatchFetch(value = BatchFetchType.EXISTS)
    @OneToMany(mappedBy = "contactoPersona", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("fechaCreacion DESC")
    private List<CorreoElectronico> correoElectronicoList;
    @BatchFetch(value = BatchFetchType.EXISTS)
    @OneToMany(mappedBy = "contactoPersona", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("fechaCreacion DESC")
    private List<Ubicacion> ubicacionList;
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID")
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Persona persona;

    public ContactoPersona() {
    }

    public ContactoPersona(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {   
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getEsAutorizaNotificacionElectronica() {
        return esAutorizaNotificacionElectronica;
    }

    public void setEsAutorizaNotificacionElectronica(Boolean esAutorizaNotificacionElectronica) {
        this.esAutorizaNotificacionElectronica = esAutorizaNotificacionElectronica;
    }

    public List<Telefono> getTelefonoList() {
        if (telefonoList == null) {
            telefonoList = new ArrayList<Telefono>();
        }
        return telefonoList;
    }

    protected void setTelefonoList(List<Telefono> telefonoList) {
        this.telefonoList = telefonoList;
    }

    public List<CorreoElectronico> getCorreoElectronicoList() {
        if (correoElectronicoList == null) {
            correoElectronicoList = new ArrayList<CorreoElectronico>();
        }
        return correoElectronicoList;
    }

    protected void setCorreoElectronicoList(List<CorreoElectronico> correoElectronicoList) {
        this.correoElectronicoList = correoElectronicoList;
    }

    public List<Ubicacion> getUbicacionList() {
        if (ubicacionList == null) {
            ubicacionList = new ArrayList<Ubicacion>();
        }
        return ubicacionList;
    }

    protected void setUbicacionList(List<Ubicacion> ubicacionList) {
        this.ubicacionList = ubicacionList;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ContactoPersona)) {
            return false;
        }
        ContactoPersona other = (ContactoPersona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ContactoPersona[ id=" + id + " ]";
    }

}
