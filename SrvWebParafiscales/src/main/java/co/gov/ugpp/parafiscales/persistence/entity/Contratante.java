package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seq_contratante", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_CONTRATANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contratante.findByIdentificacion", query = "SELECT a FROM Contratante a WHERE a.numeroIdentificacion = :numeroIdentificacion and a.tipoIdentificacion.sigla = :tipoIdentificacion"),})
public class Contratante extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seq_contratante")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;

    @Size(max = 250)
    @Column(name = "NOMBRE")
    private String nombre;

    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @OneToMany(mappedBy = "idcontratante", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NominaDetalle> nominaLiquidadorDetalleList;

    @JoinColumn(name = "TIPO_IDENTIFICACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoIdentificacion tipoIdentificacion;

    public Contratante() {
    }

    public Contratante(Long id) {
        this.id = id;
    }

    public Contratante(Long id, String numeroIdentificacion) {
        this.id = id;
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contratante)) {
            return false;
        }
        Contratante other = (Contratante) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.Contratante[ id=" + id + " ]";
    }

    /**
     * @return the nominaLiquidadorDetalleList
     */
    public List<NominaDetalle> getNominaLiquidadorDetalleList() {
        return nominaLiquidadorDetalleList;
    }

    /**
     * @param nominaLiquidadorDetalleList the nominaLiquidadorDetalleList to set
     */
    public void setNominaLiquidadorDetalleList(List<NominaDetalle> nominaLiquidadorDetalleList) {
        this.nominaLiquidadorDetalleList = nominaLiquidadorDetalleList;
    }

    /**
     * @return the tipoIdentificacion
     */
    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

}
