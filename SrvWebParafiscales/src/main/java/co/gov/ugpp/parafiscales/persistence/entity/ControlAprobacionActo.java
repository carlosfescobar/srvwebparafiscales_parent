package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author yrinconh
 */
@Entity
@Table(name = "CONTROL_APROBACION_ACTO")
public class ControlAprobacionActo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "controlAprobacionActoIdSeq", sequenceName = "contr_aprobacion_acto_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlAprobacionActoIdSeq")
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_ARCHIVO")
    private String idArchivo;
    @JoinColumn(name = "EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Aportante aportante;
    @Column(name = "ID_FUNCIONARIO_APRUEBA_ACTO")
    private String idFuncionarioApruebaActo;
    @Column(name = "ID_FUNCIONARIO_ELABORA_ACTO")
    private String idFuncionarioElaboraActo;
    @Column(name = "FEC_APROBACION_ACTO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecAprobacionActo;
    @Column(name = "FEC_TAREA_APROBACION_EN_COLA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecTareaAprobacionEnCola;
    @JoinColumn(name = "ID_ACTO_ADMINISTRATIVO_CREADO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ActoAdministrativo idActoAdministrativoCreado;
    @JoinColumn(name = "COD_TIPO_ACTO_APROBACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoActoAprobacion;
    @JoinColumn(name = "COD_ESTADO_APROBACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoAprobacion;
    @Column(name = "VAL_ROL_ENCARGADO_APROBAR")
    private String valRolEncargadoAprobar;
    @JoinColumn(name = "ID_APROBACION_MASIVA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AprobacionMasivaActo aprobacionMasivaActo;
    
    public ControlAprobacionActo() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdArchivo() {
        return idArchivo;
    }

    public void setIdArchivo(String idArchivo) {
        this.idArchivo = idArchivo;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public String getIdFuncionarioApruebaActo() {
        return idFuncionarioApruebaActo;
    }

    public void setIdFuncionarioApruebaActo(String idFuncionarioApruebaActo) {
        this.idFuncionarioApruebaActo = idFuncionarioApruebaActo;
    }

    public String getIdFuncionarioElaboraActo() {
        return idFuncionarioElaboraActo;
    }

    public void setIdFuncionarioElaboraActo(String idFuncionarioElaboraActo) {
        this.idFuncionarioElaboraActo = idFuncionarioElaboraActo;
    }

    public Calendar getFecAprobacionActo() {
        return fecAprobacionActo;
    }

    public void setFecAprobacionActo(Calendar fecAprobacionActo) {
        this.fecAprobacionActo = fecAprobacionActo;
    }

    public Calendar getFecTareaAprobacionEnCola() {
        return fecTareaAprobacionEnCola;
    }

    public void setFecTareaAprobacionEnCola(Calendar fecTareaAprobacionEnCola) {
        this.fecTareaAprobacionEnCola = fecTareaAprobacionEnCola;
    }

    public ActoAdministrativo getIdActoAdministrativoCreado() {
        return idActoAdministrativoCreado;
    }

    public void setIdActoAdministrativoCreado(ActoAdministrativo idActoAdministrativoCreado) {
        this.idActoAdministrativoCreado = idActoAdministrativoCreado;
    }

    public ValorDominio getCodTipoActoAprobacion() {
        return codTipoActoAprobacion;
    }

    public void setCodTipoActoAprobacion(ValorDominio codTipoActoAprobacion) {
        this.codTipoActoAprobacion = codTipoActoAprobacion;
    }

    public ValorDominio getCodEstadoAprobacion() {
        return codEstadoAprobacion;
    }

    public void setCodEstadoAprobacion(ValorDominio codEstadoAprobacion) {
        this.codEstadoAprobacion = codEstadoAprobacion;
    }

    public String getValRolEncargadoAprobar() {
        return valRolEncargadoAprobar;
    }

    public void setValRolEncargadoAprobar(String valRolEncargadoAprobar) {
        this.valRolEncargadoAprobar = valRolEncargadoAprobar;
    }

    public AprobacionMasivaActo getAprobacionMasivaActo() {
        return aprobacionMasivaActo;
    }

    public void setAprobacionMasivaActo(AprobacionMasivaActo aprobacionMasivaActo) {
        this.aprobacionMasivaActo = aprobacionMasivaActo;
    }
}
