package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "CONTROL_ENVIO")
public class ControlEnvio extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "controlEnvioIdSeq", sequenceName = "control_envio_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlEnvioIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_PROCESO_ENVIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codProcesoEnvia;
    @Column(name = "ID_PROCESO_ENVIA")
    private String idProcesoEnvia;
    @JoinColumn(name = "ID_INTERESADO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Persona interesado;
    @JoinColumn(name = "COD_TIPO_ENVIO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoEnvio;
    @Column(name = "ES_PERMITIDO_REENVIO")
    private String esPermitidoReenvio;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "ID_DOCUMENTO_ENVIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private DocumentoEcm idDocumentoParaEnvio;
    @Column(name = "ID_RADICADO_SALIDA_DEFINITIVO")
    private String idRadicadoSalidaDefinitivo;
    @Column(name = "FEC_RADICADO_SALIDA_DEFINIT")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecRadicadoSalidaDefinitvo;
    @JoinColumn(name = "ID_CONTROL_ENVIO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ControlEnvioUbicacion> controlUbicaciones;


    public ControlEnvio() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodProcesoEnvia() {
        return codProcesoEnvia;
    }

    public void setCodProcesoEnvia(ValorDominio codProcesoEnvia) {
        this.codProcesoEnvia = codProcesoEnvia;
    }

    public String getIdProcesoEnvia() {
        return idProcesoEnvia;
    }

    public void setIdProcesoEnvia(String idProcesoEnvia) {
        this.idProcesoEnvia = idProcesoEnvia;
    }

    public Persona getInteresado() {
        return interesado;
    }

    public void setInteresado(Persona interesado) {
        this.interesado = interesado;
    }

    public ValorDominio getCodTipoEnvio() {
        return codTipoEnvio;
    }

    public void setCodTipoEnvio(ValorDominio codTipoEnvio) {
        this.codTipoEnvio = codTipoEnvio;
    }

    public String getEsPermitidoReenvio() {
        return esPermitidoReenvio;
    }

    public void setEsPermitidoReenvio(String esPermitidoReenvio) {
        this.esPermitidoReenvio = esPermitidoReenvio;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public DocumentoEcm getIdDocumentoParaEnvio() {
        return idDocumentoParaEnvio;
    }

    public void setIdDocumentoParaEnvio(DocumentoEcm idDocumentoParaEnvio) {
        this.idDocumentoParaEnvio = idDocumentoParaEnvio;
    }

    public String getIdRadicadoSalidaDefinitivo() {
        return idRadicadoSalidaDefinitivo;
    }

    public void setIdRadicadoSalidaDefinitivo(String idRadicadoSalidaDefinitivo) {
        this.idRadicadoSalidaDefinitivo = idRadicadoSalidaDefinitivo;
    }

    public Calendar getFecRadicadoSalidaDefinitvo() {
        return fecRadicadoSalidaDefinitvo;
    }

    public void setFecRadicadoSalidaDefinitvo(Calendar fecRadicadoSalidaDefinitvo) {
        this.fecRadicadoSalidaDefinitvo = fecRadicadoSalidaDefinitvo;
    }

    public List<ControlEnvioUbicacion> getControlUbicaciones() {
        if (controlUbicaciones == null) {
            controlUbicaciones = new ArrayList<ControlEnvioUbicacion>();
        }
        return controlUbicaciones;
    }

  

}
