

package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */

@Entity
@Table(name = "CONTROL_ENVIO_COMUNICACION")
public class ControlEnvioComunicacion extends AbstractEntity<Long> {
    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "controlenvcomIdSeq", sequenceName = "control_env_comunic_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlenvcomIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Notificacion notificacion;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstado;
    @Column(name = "DESC_OBSERV_RECHAZO")
    private String descObservacionesRechazo;
    @Column(name = "DESC_OBSERV_CORRECION")
    private  String descObservacionesCorreccion;
    @Column (name = "FEC_RECHAZO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecRechazo;
    @Column(name = "FEC_CORRECION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecCorreccion;
    @Column(name = "ID_USUARIO_RECHAZA")
    private String idUsuarioRechaza;
    @Column(name = "ID_USUARIO_CORRIGE")
    private String idUsuarioCorrige;
            
    

    public ControlEnvioComunicacion() {
    }

    @Override
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Notificacion getNotificacion() {
        return notificacion;
    }

    public void setNotificacion(Notificacion notificacion) {
        this.notificacion = notificacion;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public String getDescObservacionesRechazo() {
        return descObservacionesRechazo;
    }

    public void setDescObservacionesRechazo(String descObservacionesRechazo) {
        this.descObservacionesRechazo = descObservacionesRechazo;
    }

    public String getDescObservacionesCorreccion() {
        return descObservacionesCorreccion;
    }

    public void setDescObservacionesCorreccion(String descObservacionesCorreccion) {
        this.descObservacionesCorreccion = descObservacionesCorreccion;
    }

    public Calendar getFecRechazo() {
        return fecRechazo;
    }

    public void setFecRechazo(Calendar fecRechazo) {
        this.fecRechazo = fecRechazo;
    }

    public Calendar getFecCorreccion() {
        return fecCorreccion;
    }

    public void setFecCorreccion(Calendar fecCorreccion) {
        this.fecCorreccion = fecCorreccion;
    }

    public String getIdUsuarioRechaza() {
        return idUsuarioRechaza;
    }

    public void setIdUsuarioRechaza(String idUsuarioRechaza) {
        this.idUsuarioRechaza = idUsuarioRechaza;
    }

    public String getIdUsuarioCorrige() {
        return idUsuarioCorrige;
    }

    public void setIdUsuarioCorrige(String idUsuarioCorrige) {
        this.idUsuarioCorrige = idUsuarioCorrige;
    }

      
    
    
    
}
