package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "CONTROL_ENVIO_UBICACION")
@NamedQueries({
    @NamedQuery(name = "controlenvioubicacion.findByControlEnvioAndControlUbicacion",
            query = "SELECT ceu FROM ControlEnvioUbicacion ceu WHERE ceu.controlEnvio.id= :idControlEnvio AND ceu.controlUbicacionEnvio = :idControlUbicacionEnvio")
})
public class ControlEnvioUbicacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "controlenvioubicacionIdSeq", sequenceName = "control_envio_ubicac_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlenvioubicacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_CONTROL_ENVIO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlEnvio controlEnvio;
    @JoinColumn(name = "ID_CONTROL_UBICACION_ENVIO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlUbicacionEnvio controlUbicacionEnvio;

    public ControlEnvioUbicacion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ControlEnvio getControlEnvio() {
        return controlEnvio;
    }

    public void setControlEnvio(ControlEnvio controlEnvio) {
        this.controlEnvio = controlEnvio;
    }

    public ControlUbicacionEnvio getControlUbicacionEnvio() {
        return controlUbicacionEnvio;
    }

    public void setControlUbicacionEnvio(ControlUbicacionEnvio controlUbicacionEnvio) {
        this.controlUbicacionEnvio = controlUbicacionEnvio;
    }

}
