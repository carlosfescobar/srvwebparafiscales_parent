package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "CONTROL_LIQUIDADOR")
public class ControlLiquidador extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @NotNull
    @Column(name = "ID")
    @Id
    @SequenceGenerator(name = "controlLiquidadorIdSeq", sequenceName = "control_liquidador_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlLiquidadorIdSeq")
    @Basic(optional = false)
    private Long id;
    @JoinColumn(name = "COD_PROCESO_SOLICITADOR", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codProcesoSolicitador;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Aportante aportante;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Expediente expediente;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO_NOMINA", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION_NOMINA", referencedColumnName = "ID_VERSION")
    })
    private Formato formatoNomina;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO_CONCILIACION", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION_CONCILIACION", referencedColumnName = "ID_VERSION")
    })
    private Formato formatoConciliacion;
    @Column(name = "ID_INFO_EXTERNA_NOMINA")
    private Long informacionExterNomina;
    @Column(name = "ID_INFO_EXTERNA_CONCILIACION")
    private Long infoExtConciliacion;
    @Column(name = "ID_ENVIO_NOMINA")
    private Long envioNomina;
    @Column(name = "ID_ENVIO_CONCILIACION")
    private Long envioConciliacion;
    @JoinColumn(name = "ID_NOMINA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Nomina nomina;
    @JoinColumn(name = "ID_CONCILIACION_CONTABLE ", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConciliacionContable conciliacionContable;
    @JoinColumn(name = "ID_HOJA_CALCULO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private HojaCalculoLiquidacion hojaCalculoLiquidacion;
    @JoinColumn(name = "COD_ORIGEN", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codOrigen;
    @JoinColumn(name = "COD_LINEA_ACCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codLineaAccion;
    @JoinColumn(name = "COD_TIPO_DENUNCIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoDenuncia;
    @Size(max = 100)
    @Column(name = "VAL_PROGRAMA")
    private String valPrograma;
    @JoinColumn(name = "ID_HALLAZGO_NOMINA_PILA", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @ManyToOne(fetch = FetchType.LAZY)
    private Hallazgo hallazgoNominaPila;
    @JoinColumn(name = "ID_HALLAZGO_CONCILIA_NOMINA", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @ManyToOne(fetch = FetchType.LAZY)
    private Hallazgo HallazgoConcilNomina;
    @Size(max = 200)
    @Column(name = "DESC_CARGUE_NOM_CONCILIACION")
    private String desCargueNominaConciliacion;
    @Column(name = "ES_SOLICITAR_ACLARACIONES")
    private Boolean esSolicitarAclaraciones;
    @Size(max = 38)
    @Column(name = "ID_PILA_DEPURADA")
    private String idPilaDepurada;
    @Column(name = "FEC_RADICADO_ENTRADA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoEntrada;
    @Column(name = "FEC_RADICADO_SALIDA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoSalida;
    @Size(max = 50)
    @Column(name = "ID_RADICADO_ENTRADA")
    private String idRadicadoEntrada;
    @Size(max = 50)
    @Column(name = "ID_RADICADO_SALIDA")
    private String idRadicadoSalida;
    @Column(name = "VAL_LIQUIDACION_APORTE")
    private String valLiquidacionAporte;
    @Column(name = "VAL_PARTIDAS_GLOBALES")
    private String valPartidasGlobales;
    @Column(name = "VAL_LIQ_SANCION_OMISION")
    private String valLiquidacionSancionOmision;
    @Column(name = "VAL_LIQ_SANCION_INEXACTITUD")
    private String valLiquidacionSancionInexactitud;
    @JoinColumn(name = "COD_EJECUCION_LIQUIDADOR", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoEjecucionLiquidador;
    @Column(name = "ES_INCUMPLIMIENTO")
    private Boolean esIncumplimiento;
    @JoinColumn(name = "COD_CAUSAL_CIERRE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCausalCierre;
    @JoinColumn(name = "ID_CONTROL_LIQUIDADOR", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<ControlLiquidadorArchivoTemporal> archivosTemporales;

    public ControlLiquidador() {
    }

    public ValorDominio getCodProcesoSolicitador() {
        return codProcesoSolicitador;
    }

    public void setCodProcesoSolicitador(ValorDominio codProcesoSolicitador) {
        this.codProcesoSolicitador = codProcesoSolicitador;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Formato getFormatoNomina() {
        return formatoNomina;
    }

    public void setFormatoNomina(Formato formatoNomina) {
        this.formatoNomina = formatoNomina;
    }

    public Formato getFormatoConciliacion() {
        return formatoConciliacion;
    }

    public void setFormatoConciliacion(Formato formatoConciliacion) {
        this.formatoConciliacion = formatoConciliacion;
    }

    public Long getInformacionExterNomina() {
        return informacionExterNomina;
    }

    public void setInformacionExterNomina(Long informacionExterNomina) {
        this.informacionExterNomina = informacionExterNomina;
    }

    public Long getInfoExtConciliacion() {
        return infoExtConciliacion;
    }

    public void setInfoExtConciliacion(Long infoExtConciliacion) {
        this.infoExtConciliacion = infoExtConciliacion;
    }

    public Long getEnvioNomina() {
        return envioNomina;
    }

    public void setEnvioNomina(Long envioNomina) {
        this.envioNomina = envioNomina;
    }

    public Long getEnvioConciliacion() {
        return envioConciliacion;
    }

    public void setEnvioConciliacion(Long envioConciliacion) {
        this.envioConciliacion = envioConciliacion;
    }

    public Nomina getNomina() {
        return nomina;
    }

    public void setNomina(Nomina nomina) {
        this.nomina = nomina;
    }

    public ConciliacionContable getConciliacionContable() {
        return conciliacionContable;
    }

    public void setConciliacionContable(ConciliacionContable conciliacionContable) {
        this.conciliacionContable = conciliacionContable;
    }

    public HojaCalculoLiquidacion getHojaCalculoLiquidacion() {
        return hojaCalculoLiquidacion;
    }

    public void setHojaCalculoLiquidacion(HojaCalculoLiquidacion hojaCalculoLiquidacion) {
        this.hojaCalculoLiquidacion = hojaCalculoLiquidacion;
    }

    public ValorDominio getCodOrigen() {
        return codOrigen;
    }

    public void setCodOrigen(ValorDominio codOrigen) {
        this.codOrigen = codOrigen;
    }

    public ValorDominio getCodLineaAccion() {
        return codLineaAccion;
    }

    public void setCodLineaAccion(ValorDominio codLineaAccion) {
        this.codLineaAccion = codLineaAccion;
    }

    public ValorDominio getCodTipoDenuncia() {
        return codTipoDenuncia;
    }

    public void setCodTipoDenuncia(ValorDominio codTipoDenuncia) {
        this.codTipoDenuncia = codTipoDenuncia;
    }

    public String getValPrograma() {
        return valPrograma;
    }

    public void setValPrograma(String valPrograma) {
        this.valPrograma = valPrograma;
    }

    public Hallazgo getHallazgoNominaPila() {
        return hallazgoNominaPila;
    }

    public void setHallazgoNominaPila(Hallazgo hallazgoNominaPila) {
        this.hallazgoNominaPila = hallazgoNominaPila;
    }

    public Hallazgo getHallazgoConcilNomina() {
        return HallazgoConcilNomina;
    }

    public void setHallazgoConcilNomina(Hallazgo HallazgoConcilNomina) {
        this.HallazgoConcilNomina = HallazgoConcilNomina;
    }

    public String getDesCargueNominaConciliacion() {
        return desCargueNominaConciliacion;
    }

    public void setDesCargueNominaConciliacion(String desCargueNominaConciliacion) {
        this.desCargueNominaConciliacion = desCargueNominaConciliacion;
    }

    public Boolean getEsSolicitarAclaraciones() {
        return esSolicitarAclaraciones;
    }

    public void setEsSolicitarAclaraciones(Boolean esSolicitarAclaraciones) {
        this.esSolicitarAclaraciones = esSolicitarAclaraciones;
    }

    public String getIdPilaDepurada() {
        return idPilaDepurada;
    }

    public void setIdPilaDepurada(String idPilaDepurada) {
        this.idPilaDepurada = idPilaDepurada;
    }

    public Calendar getFecRadicadoEntrada() {
        return fecRadicadoEntrada;
    }

    public void setFecRadicadoEntrada(Calendar fecRadicadoEntrada) {
        this.fecRadicadoEntrada = fecRadicadoEntrada;
    }

    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    public void setFecRadicadoSalida(Calendar fecRadicadoSalida) {
        this.fecRadicadoSalida = fecRadicadoSalida;
    }

    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    public void setIdRadicadoEntrada(String idRadicadoEntrada) {
        this.idRadicadoEntrada = idRadicadoEntrada;
    }

    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    public void setIdRadicadoSalida(String idRadicadoSalida) {
        this.idRadicadoSalida = idRadicadoSalida;
    }

    public String getValLiquidacionAporte() {
        return valLiquidacionAporte;
    }

    public void setValLiquidacionAporte(String valLiquidacionAporte) {
        this.valLiquidacionAporte = valLiquidacionAporte;
    }

    public String getValLiquidacionSancionOmision() {
        return valLiquidacionSancionOmision;
    }

    public void setValLiquidacionSancionOmision(String valLiquidacionSancionOmision) {
        this.valLiquidacionSancionOmision = valLiquidacionSancionOmision;
    }

    public String getValLiquidacionSancionInexactitud() {
        return valLiquidacionSancionInexactitud;
    }

    public void setValLiquidacionSancionInexactitud(String valLiquidacionSancionInexactitud) {
        this.valLiquidacionSancionInexactitud = valLiquidacionSancionInexactitud;
    }

    public ValorDominio getCodTipoEjecucionLiquidador() {
        return codTipoEjecucionLiquidador;
    }

    public void setCodTipoEjecucionLiquidador(ValorDominio codTipoEjecucionLiquidador) {
        this.codTipoEjecucionLiquidador = codTipoEjecucionLiquidador;
    }

    public Boolean getEsIncumplimiento() {
        return esIncumplimiento;
    }

    public void setEsIncumplimiento(Boolean esIncumplimiento) {
        this.esIncumplimiento = esIncumplimiento;
    }

    public ValorDominio getCodCausalCierre() {
        return codCausalCierre;
    }

    public void setCodCausalCierre(ValorDominio codCausalCierre) {
        this.codCausalCierre = codCausalCierre;
    }

    public List<ControlLiquidadorArchivoTemporal> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ControlLiquidadorArchivoTemporal>();
        }
        return archivosTemporales;
    }

    public String getValPartidasGlobales() {
        return valPartidasGlobales;
    }

    public void setValPartidasGlobales(String valPartidasGlobales) {
        this.valPartidasGlobales = valPartidasGlobales;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ControlLiquidador)) {
            return false;
        }
        ControlLiquidador other = (ControlLiquidador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ControlLiquidador[ idPersona=" + id + " ]";
    }

}
