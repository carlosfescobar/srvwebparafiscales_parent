package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "CONTROL_LIQ_ARCHIVO_TEMPORAL")
public class ControlLiquidadorArchivoTemporal extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "ctrlArchivoTemporalIdSeq", sequenceName = "ctrl_archivo_tmp_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ctrlArchivoTemporalIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private Archivo archivo;
    @JoinColumn(name = "ID_CONTROL_LIQUIDADOR", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ControlLiquidador controlLiquidador;
    @JoinColumn(name = "COD_TIPO_ARCHIVO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ValorDominio codTipoArchivo;

    public ControlLiquidadorArchivoTemporal(){
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    public ControlLiquidador getControlLiquidador() {
        return controlLiquidador;
    }

    public void setControlLiquidador(ControlLiquidador controlLiquidador) {
        this.controlLiquidador = controlLiquidador;
    }

    public ValorDominio getCodTipoArchivo() {
        return codTipoArchivo;
    }

    public void setCodTipoArchivo(ValorDominio codTipoArchivo) {
        this.codTipoArchivo = codTipoArchivo;
    }
}
