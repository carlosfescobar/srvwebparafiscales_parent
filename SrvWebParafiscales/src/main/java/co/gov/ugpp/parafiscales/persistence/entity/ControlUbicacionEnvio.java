package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "CONTROL_UBICACION_ENVIO")
public class ControlUbicacionEnvio extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "controlUbicacionEnvioIdSeq", sequenceName = "control_ubicacion_env_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "controlUbicacionEnvioIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_EVENTO_ENVIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EventoEnvio eventosEnvio;
    @JoinColumn(name = "COD_ESTADO_UBICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoUbicacion;
    @JoinColumn(name = "ID_UBICACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ubicacion ubicacion;

    public ControlUbicacionEnvio() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EventoEnvio getEventosEnvio() {
        return eventosEnvio;
    }

    public void setEventosEnvio(EventoEnvio eventosEnvio) {
        this.eventosEnvio = eventosEnvio;
    }

    public ValorDominio getCodEstadoUbicacion() {
        return codEstadoUbicacion;
    }

    public void setCodEstadoUbicacion(ValorDominio codEstadoUbicacion) {
        this.codEstadoUbicacion = codEstadoUbicacion;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

}
