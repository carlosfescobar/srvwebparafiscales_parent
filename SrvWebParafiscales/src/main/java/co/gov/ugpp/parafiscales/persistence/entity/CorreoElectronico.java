package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 *
 * @author rpadilla
 */
@Entity
@Cacheable(true)
@Cache(type = CacheType.HARD_WEAK)
@Table(name = "CORREO_ELECTRONICO")


@NamedQueries({
    @NamedQuery(name = "correoElectronico.findCorreoElectronico",
        query = "SELECT COUNT(c) FROM CorreoElectronico c WHERE c.valCorreoElectronico = :valCorreoElectronico AND c.contactoPersona.id = :idContacto AND c.codFuente.id = :codFuente")
        
})


public class CorreoElectronico extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "correoElectronicoIdSeq", sequenceName = "correo_electronico_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "correoElectronicoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Size(max = 200)
    @Column(name = "VAL_CORREO_ELECTRONICO")
    private String valCorreoElectronico;
    
    
    
    @JoinColumn(name = "COD_FUENTE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codFuente;

    
    
    @JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactoPersona contactoPersona;
    
    
    public CorreoElectronico() {
    }

    public CorreoElectronico(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValCorreoElectronico() {
        return valCorreoElectronico;
    }

    public void setValCorreoElectronico(String valCorreoElectronico) {
        this.valCorreoElectronico = valCorreoElectronico;
    }

    public ContactoPersona getContactoPersona() {
        return contactoPersona;
    }

    public void setContactoPersona(ContactoPersona contactoPersona) {
        this.contactoPersona = contactoPersona;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CorreoElectronico)) {
            return false;
        }
        CorreoElectronico other = (CorreoElectronico) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.CorreoElectronico[ id=" + id + " ]";
    }

    
    
    public ValorDominio getCodFuente() {
        return codFuente;
    }

    public void setCodFuente(ValorDominio codFuente) {
        this.codFuente = codFuente;
    }
    
   

}
