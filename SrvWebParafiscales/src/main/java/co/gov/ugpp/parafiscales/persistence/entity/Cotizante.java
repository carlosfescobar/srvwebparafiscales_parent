package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "COTIZANTE")
public class Cotizante extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
    private Persona persona;
    @OneToOne(mappedBy = "cotizante", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private CotizanteInformacionBasica infoCotizante;

    public Cotizante() {
        this(new Persona());
    }

    public Cotizante(Persona persona) {
        this.setPersona(persona);
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Persona getPersona() {
        return this.persona;
    }

    public CotizanteInformacionBasica getInfoCotizante() {
        return infoCotizante;
    }

    public void setInfoCotizante(CotizanteInformacionBasica infoCotizante) {
        this.infoCotizante = infoCotizante;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
        if (persona != null) {
            this.id = persona.getId();
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof CotizanteTipo) {
            final CotizanteTipo other = (CotizanteTipo) object;
            return this.persona != null && this.persona.equals(other);
        }
        if (object instanceof Cotizante) {
            final Cotizante other = (Cotizante) object;
            return this.persona != null && this.persona.equals(other.persona);
        }
        return false;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Cotizante[ idPersona=" + id + " ]";
    }

}
