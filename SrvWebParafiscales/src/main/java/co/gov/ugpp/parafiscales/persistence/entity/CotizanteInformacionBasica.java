package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "COTIZANTE_INFORMACION_BASICA")
public class CotizanteInformacionBasica extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "cotizanteHasInfoBasicaIdSeq", sequenceName = "cot_has_info_basica_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "cotizanteHasInfoBasicaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "DESC_OBSERVACION")
    private String descObservacion;
    @JoinColumn(name = "ID_COTIZANTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Cotizante cotizante;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Archivo archivo;

    public CotizanteInformacionBasica() {
    }

    public String getDescObservacion() {
        return descObservacion;
    }

    public void setDescObservacion(String descObservacion) {
        this.descObservacion = descObservacion;
    }

    public Cotizante getCotizante() {
        return cotizante;
    }

    public void setCotizante(Cotizante cotizante) {
        this.cotizante = cotizante;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CotizanteInformacionBasica)) {
            return false;
        }
        CotizanteInformacionBasica other = (CotizanteInformacionBasica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.DenunciaHasCodSubsistema[ id=" + id + " ]";
    }

    @Override
    public Long getId() {
        return id;
    }

}
