package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.SequenceGenerator;

/**
 *
 * @author franzjr
 */
@Entity
//@TableGenerator(name = "seq_cotizante", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_COTIZANTE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cotizante.findByIdentificacion", query = "SELECT a FROM CotizanteLIQ a WHERE a.numeroIdentificacion = :numeroIdentificacion and a.tipoIdentificacion.sigla = :tipoIdentificacion"),
    @NamedQuery(name = "Cotizante.findByIdentificacionAndIdTipoIdentificacion", query = "SELECT a FROM CotizanteLIQ a WHERE a.numeroIdentificacion = :numeroIdentificacion AND a.tipoIdentificacion.id = :idTipoIdentificacion")
})
public class CotizanteLIQ extends AbstractEntity<Long> {

    @Id
    @SequenceGenerator(name="seq_cotizante", sequenceName="seq_cotizante_seq_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_cotizante")
    //@GeneratedValue(strategy = GenerationType.TABLE, generator = "seq_cotizante")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Size(max = 20)
    @Column(name = "NUMERO_IDENTIFICACION")
    private String numeroIdentificacion;

    @Size(max = 1000)
    @Column(name = "NOMBRE")
    private String nombre;

    @Size(max = 100)
    @Column(name = "TIPO_COTIZANTE")
    private String tipo_cotizante;

    @Size(max = 100)
    @Column(name = "SUBTIPO_COTIZANTE")
    private String subtipo_cotizante;

    //************************************************************************//
    // ESTOS CAMPOS SE PASARON A NOMINA DETALLE  (ANDRES GARCIA)  16-12-2015 
    //************************************************************************//
    /*
    @Size(max = 2)
    @Column(name = "EXTRANJERO_NO_OBLI_COTIZAR_PEN")
    private String extranjero_no_obligado_a_cotizar_pension;
    /*Lista: Si, No*/

    /*
    @Size(max = 2)
    @Column(name = "COLOMBIANO_EN_EL_EXTERIOR")
    private String colombiano_en_el_exterior;
    /*Lista: Si, No*/
    
    
    @Size(max = 2)
    @Column(name = "ACTIVIDAD_ALTO_RIESGO_PENSION")
    private String actividad_alto_riesgo_pension;
    /*Lista: Si, No*/

    @JoinColumn(name = "TIPO_IDENTIFICACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TipoIdentificacion tipoIdentificacion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idcotizante")
    private List<NominaDetalle> nominaLiquidadorDetalleList;

    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    
    @Size(max = 20)
    @Column(name = "NUM_IDENTIFI_REALIZO_APORTES")
    private String numeroIdentificacionRealizoAportes;
    
    
    @Size(max = 2)
    @Column(name = "TIPO_IDENTIFI_REALIZO_APORTES")
    private String tipoNumeroIdentificacionRealizoAportes;
    
    
    @Size(max = 80)
    @Column(name = "CARGO_TRABAJADOR")
    private String cargoTrabajador;
    

    @Size(max = 200)
    @Column(name = "CONDICION_ESPECIAL_EMPRESA")
    private String condicionEspecialEmpresa;

    //SE ELIMINARON LOS SET DE:
   
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//

    public CotizanteLIQ() {
    }

    public CotizanteLIQ(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the numeroIdentificacion
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * @param numeroIdentificacion the numeroIdentificacion to set
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tipo_cotizante
     */
    public String getTipo_cotizante() {
        return tipo_cotizante;
    }

    /**
     * @param tipo_cotizante the tipo_cotizante to set
     */
    public void setTipo_cotizante(String tipo_cotizante) {
        this.tipo_cotizante = tipo_cotizante;
    }

    /**
     * @return the subtipo_cotizante
     */
    public String getSubtipo_cotizante() {
        return subtipo_cotizante;
    }

    /**
     * @param subtipo_cotizante the subtipo_cotizante to set
     */
    public void setSubtipo_cotizante(String subtipo_cotizante) {
        this.subtipo_cotizante = subtipo_cotizante;
    }



    /**
     * @return the actividad_alto_riesgo_pension
     */
    public String getActividad_alto_riesgo_pension() {
        return actividad_alto_riesgo_pension;
    }

    /**
     * @param actividad_alto_riesgo_pension the actividad_alto_riesgo_pension to
     * set
     */
    public void setActividad_alto_riesgo_pension(String actividad_alto_riesgo_pension) {
        this.actividad_alto_riesgo_pension = actividad_alto_riesgo_pension;
    }

    /**
     * @return the nominaLiquidadorDetalleList
     */
    public List<NominaDetalle> getNominaLiquidadorDetalleList() {
        return nominaLiquidadorDetalleList;
    }

    /**
     * @param nominaLiquidadorDetalleList the nominaLiquidadorDetalleList to set
     */
    public void setNominaLiquidadorDetalleList(List<NominaDetalle> nominaLiquidadorDetalleList) {
        this.nominaLiquidadorDetalleList = nominaLiquidadorDetalleList;
    }

    /**
     * @return the tipoIdentificacion
     */
    public TipoIdentificacion getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(TipoIdentificacion tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the numeroIdentificacionRealizoAportes
     */
    public String getNumeroIdentificacionRealizoAportes() {
        return numeroIdentificacionRealizoAportes;
    }

    /**
     * @param numeroIdentificacionRealizoAportes the numeroIdentificacionRealizoAportes to set
     */
    public void setNumeroIdentificacionRealizoAportes(String numeroIdentificacionRealizoAportes) {
        this.numeroIdentificacionRealizoAportes = numeroIdentificacionRealizoAportes;
    }

  

    /**
     * @return the cargoTrabajador
     */
    public String getCargoTrabajador() {
        return cargoTrabajador;
    }

    /**
     * @param cargoTrabajador the cargoTrabajador to set
     */
    public void setCargoTrabajador(String cargoTrabajador) {
        this.cargoTrabajador = cargoTrabajador;
    }

    /**
     * @return the tipoNumeroIdentificacionRealizoAportes
     */
    public String getTipoNumeroIdentificacionRealizoAportes() {
        return tipoNumeroIdentificacionRealizoAportes;
    }

    /**
     * @param tipoNumeroIdentificacionRealizoAportes the tipoNumeroIdentificacionRealizoAportes to set
     */
    public void setTipoNumeroIdentificacionRealizoAportes(String tipoNumeroIdentificacionRealizoAportes) {
        this.tipoNumeroIdentificacionRealizoAportes = tipoNumeroIdentificacionRealizoAportes;
    }

    /**
     * @return the condicionEspecialEmpresa
     */
    public String getCondicionEspecialEmpresa() {
        return condicionEspecialEmpresa;
    }

    /**
     * @param condicionEspecialEmpresa the condicionEspecialEmpresa to set
     */
    public void setCondicionEspecialEmpresa(String condicionEspecialEmpresa) {
        this.condicionEspecialEmpresa = condicionEspecialEmpresa;
    }

}
