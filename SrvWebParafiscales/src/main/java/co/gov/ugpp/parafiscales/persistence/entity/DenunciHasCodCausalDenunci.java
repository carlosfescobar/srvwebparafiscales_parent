package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DENUNCI_HAS_COD_CAUSAL_DENUNCI")
public class DenunciHasCodCausalDenunci implements IEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "denunciaCausalesIdSeq", sequenceName = "denunci_has_cod_causal_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "denunciaCausalesIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "COD_CAUSAL_DENUNCIA_ID")
    private Long codCausalDenunciaId;

    @Size(max = 255)
    @Column(name = "COD_CAUSAL_DENUNCIA")
    private String codCausalDenuncia;

    @JoinColumn(name = "ID_DENUNCIA", referencedColumnName = "ID_DENUNCIA")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Denuncia idDenuncia;

    public DenunciHasCodCausalDenunci() {
    }

    public DenunciHasCodCausalDenunci(Long codCausalDenunciaId) {
        this.codCausalDenunciaId = codCausalDenunciaId;
    }

    public Long getCodCausalDenunciaId() {
        return codCausalDenunciaId;
    }

    public void setCodCausalDenunciaId(Long codCausalDenunciaId) {
        this.codCausalDenunciaId = codCausalDenunciaId;
    }

    public String getCodCausalDenuncia() {
        return codCausalDenuncia;
    }

    public void setCodCausalDenuncia(String codCausalDenuncia) {
        this.codCausalDenuncia = codCausalDenuncia;
    }

    public Denuncia getIdDenuncia() {
        return idDenuncia;
    }

    public void setIdDenuncia(Denuncia idDenuncia) {
        this.idDenuncia = idDenuncia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codCausalDenunciaId != null ? codCausalDenunciaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DenunciHasCodCausalDenunci)) {
            return false;
        }
        DenunciHasCodCausalDenunci other = (DenunciHasCodCausalDenunci) object;
        if ((this.codCausalDenunciaId == null && other.codCausalDenunciaId != null) || (this.codCausalDenunciaId != null && !this.codCausalDenunciaId.equals(other.codCausalDenunciaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.DenunciHasCodCausalDenunci[ codCausalDenunciaId=" + codCausalDenunciaId + " ]";
    }

    @Override
    public Long getId() {
        return codCausalDenunciaId;
    }

}
