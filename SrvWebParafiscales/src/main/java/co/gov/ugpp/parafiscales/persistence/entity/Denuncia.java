package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DENUNCIA")
@NamedQueries({
    @NamedQuery(name = "Denuncia.findByExpediente", query = "SELECT denuncia FROM Denuncia denuncia WHERE denuncia.expediente.id = :expedienteNumero")})

public class Denuncia extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "denunciaIdSeq", sequenceName = "denuncia_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "denunciaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_DENUNCIA")
    private Long idDenuncia;

    @Column(name = "FEC_INICIO_PERIODO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioPeriodo;

    @Column(name = "FEC_FIN_PERIODO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinPeriodo;

    @Size(max = 255)
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;

    @Size(max = 255)
    @Column(name = "DESC_INFORMACION_FALTANTE")
    private String descInformacionFaltante;

    @Column(name = "ES_COMPETENCIA_UGPP")
    private Boolean esCompetenciaUgPp;

    @Column(name = "ES_CASO_CERRADO")
    private Boolean esCasoCerrado;

    @Column(name = "ES_INFORMACION_COMPLETA")
    private Boolean esInformacionCompleta;

    @Column(name = "ES_EN_FISCALIZACION")
    private Boolean esEnFiscalizacion;

    @Column(name = "ES_PAGO")
    private Boolean esPago;

    @Size(max = 30)
    @Column(name = "ID_RADICADO_DENUNCIA")
    private String idRadicadoDenuncia;

    @JoinColumn(name = "COD_CANAL_DENUNCIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCanaDenuncial;

    @JoinColumn(name = "COD_CATEGORIA_DENUNCIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCategoriaDenuncia;

    @JoinColumn(name = "COD_DECISION_ANALISIS_PAGOS", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codDAPagos;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDenuncia", fetch = FetchType.LAZY)
    private List<DenunciHasCodCausalDenunci> denunciHasCodCausalDenunciList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDenuncia", fetch = FetchType.LAZY)
    private List<DenunciaHasCodSubsistema> denunciaHasCodSubsistemaList;

    @JoinColumn(name = "ID_DENUNCIANTE", referencedColumnName = "IDDENUNCIANTE")
    @ManyToOne(fetch = FetchType.LAZY)
    private Denunciante idDenunciante;

    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Aportante aportante;

    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expediente;

    @Column(name = "ES_SUSPENDER_TERMINOS")
    private Boolean esSuspenderTermino;

    @Column(name = "ES_TIENE_DERECHO_PETICION")
    private Boolean esTieneDerechoPeticion;

    @Column(name = "ES_TIENE_ANEXOS")
    private Boolean esTieneAnexos;
    
    @Column(name = "ES_PAGOS_VALIDADOS")
    private Boolean esPagosValidados;
    
    @JoinColumn(name = "COD_TIPO_DENUNCIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codTipoDenuncia;

    @JoinColumn(name = "COD_TIPO_ENVIO_COMITE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codTipoEnvioComite;

    @Column(name = "ES_APROBADO_CIERRE")
    private Boolean esAprobadoCierre;

    @Column(name = "FEC_RADICADO_DENUNCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoDenuncia;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "denuncia", fetch = FetchType.LAZY)
    private List<DenunciaAccion> denunciaAccionesList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "denuncia", fetch = FetchType.LAZY)
    private List<DenunciaCausalInicial> causalInicial;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "denuncia", fetch = FetchType.LAZY)
    private List<DenunciaCausalAnalisis> causalAnalisis;

    @JoinColumn(name = "COD_ESTADO_DENUNCIA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoDenuncia;

    @JoinColumn(name = "ID_AFECTADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Afectado afectado;
    
    @Column(name = "ES_COMPETENCIA_PARCIAL")
    private Boolean esCompetenciaParcial;

    public Denuncia() {
    }

    public Denuncia(Long idDenuncia) {
        this.idDenuncia = idDenuncia;
    }

    public Long getIdDenuncia() {
        return idDenuncia;
    }

    public void setIdDenuncia(Long idDenuncia) {
        this.idDenuncia = idDenuncia;
    }

    public ValorDominio getCodCanaDenuncial() {
        return codCanaDenuncial;
    }

    public void setCodCanaDenuncial(ValorDominio codCanaDenuncial) {
        this.codCanaDenuncial = codCanaDenuncial;
    }

    public ValorDominio getCodCategoriaDenuncia() {
        return codCategoriaDenuncia;
    }

    public void setCodCategoriaDenuncia(ValorDominio codCategoriaDenuncia) {
        this.codCategoriaDenuncia = codCategoriaDenuncia;
    }

    public ValorDominio getCodDAPagos() {
        return codDAPagos;
    }

    public void setCodDAPagos(ValorDominio codDAPagos) {
        this.codDAPagos = codDAPagos;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Calendar getFecInicioPeriodo() {
        return fecInicioPeriodo;
    }

    public void setFecInicioPeriodo(Calendar fecInicioPeriodo) {
        this.fecInicioPeriodo = fecInicioPeriodo;
    }

    public Boolean getEsPago() {
        return esPago;
    }

    public void setEsPago(Boolean esPago) {
        this.esPago = esPago;
    }

    public Calendar getFecFinPeriodo() {
        return fecFinPeriodo;
    }

    public void setFecFinPeriodo(Calendar fecFinPeriodo) {
        this.fecFinPeriodo = fecFinPeriodo;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public Boolean getEsCompetenciaUgPp() {
        return esCompetenciaUgPp;
    }

    public void setEsCompetenciaUgPp(Boolean esCompetenciaUgPp) {
        this.esCompetenciaUgPp = esCompetenciaUgPp;
    }

    public Boolean getEsCasoCerrado() {
        return esCasoCerrado;
    }

    public void setEsCasoCerrado(Boolean esCasoCerrado) {
        this.esCasoCerrado = esCasoCerrado;
    }

    public Boolean getEsInformacionCompleta() {
        return esInformacionCompleta;
    }

    public void setEsInformacionCompleta(Boolean esInformacionCompleta) {
        this.esInformacionCompleta = esInformacionCompleta;
    }

    public Boolean getEsEnFiscalizacion() {
        return esEnFiscalizacion;
    }

    public void setEsEnFiscalizacion(Boolean esEnFiscalizacion) {
        this.esEnFiscalizacion = esEnFiscalizacion;
    }

    public String getIdRadicadoDenuncia() {
        return idRadicadoDenuncia;
    }

    public void setIdRadicadoDenuncia(String idRadicadoDenuncia) {
        this.idRadicadoDenuncia = idRadicadoDenuncia;
    }

    public List<DenunciHasCodCausalDenunci> getDenunciHasCodCausalDenunciList() {
        return denunciHasCodCausalDenunciList;
    }

    public void setDenunciHasCodCausalDenunciList(List<DenunciHasCodCausalDenunci> denunciHasCodCausalDenunciList) {
        this.denunciHasCodCausalDenunciList = denunciHasCodCausalDenunciList;
    }

    public List<DenunciaHasCodSubsistema> getDenunciaHasCodSubsistemaList() {
        return denunciaHasCodSubsistemaList;
    }

    public void setDenunciaHasCodSubsistemaList(List<DenunciaHasCodSubsistema> denunciaHasCodSubsistemaList) {
        this.denunciaHasCodSubsistemaList = denunciaHasCodSubsistemaList;
    }

    public Denunciante getIdDenunciante() {
        return idDenunciante;
    }

    public void setIdDenunciante(Denunciante idDenunciante) {
        this.idDenunciante = idDenunciante;
    }

    public Boolean getEsSuspenderTermino() {
        return esSuspenderTermino;
    }

    public void setEsSuspenderTermino(Boolean esSuspenderTermino) {
        this.esSuspenderTermino = esSuspenderTermino;
    }

    public Boolean getEsTieneDerechoPeticion() {
        return esTieneDerechoPeticion;
    }

    public void setEsTieneDerechoPeticion(Boolean esTieneDerechoPeticion) {
        this.esTieneDerechoPeticion = esTieneDerechoPeticion;
    }

    public Boolean getEsTieneAnexos() {
        return esTieneAnexos;
    }

    public void setEsTieneAnexos(Boolean esTieneAnexos) {
        this.esTieneAnexos = esTieneAnexos;
    }

    public Boolean getEsPagosValidados() {
        return esPagosValidados;
    }

    public void setEsPagosValidados(Boolean esPagosValidados) {
        this.esPagosValidados = esPagosValidados;
    }

    
    public ValorDominio getCodTipoDenuncia() {
        return codTipoDenuncia;
    }

    public void setCodTipoDenuncia(ValorDominio codTipoDenuncia) {
        this.codTipoDenuncia = codTipoDenuncia;
    }

    public String getDescInformacionFaltante() {
        return descInformacionFaltante;
    }

    public void setDescInformacionFaltante(String descInformacionFaltante) {
        this.descInformacionFaltante = descInformacionFaltante;
    }

    public ValorDominio getCodTipoEnvioComite() {
        return codTipoEnvioComite;
    }

    public void setCodTipoEnvioComite(ValorDominio codTipoEnvioComite) {
        this.codTipoEnvioComite = codTipoEnvioComite;
    }

    public Boolean getEsAprobadoCierre() {
        return esAprobadoCierre;
    }

    public void setEsAprobadoCierre(Boolean esAprobadoCierre) {
        this.esAprobadoCierre = esAprobadoCierre;
    }

    public Calendar getFecRadicadoDenuncia() {
        return fecRadicadoDenuncia;
    }

    public void setFecRadicadoDenuncia(Calendar fecRadicadoDenuncia) {
        this.fecRadicadoDenuncia = fecRadicadoDenuncia;
    }

    public List<DenunciaAccion> getDenunciaAccionesList() {
        if (denunciaAccionesList == null) {
            denunciaAccionesList = new ArrayList<DenunciaAccion>();
        }
        return denunciaAccionesList;
    }

    public List<DenunciaCausalInicial> getCausalInicial() {
        if (causalInicial == null) {
            causalInicial = new ArrayList<DenunciaCausalInicial>();
        }
        return causalInicial;
    }

    public List<DenunciaCausalAnalisis> getCausalAnalisis() {
        if (causalAnalisis == null) {
            causalAnalisis = new ArrayList<DenunciaCausalAnalisis>();
        }
        return causalAnalisis;
    }

    public ValorDominio getCodEstadoDenuncia() {
        return codEstadoDenuncia;
    }

    public void setCodEstadoDenuncia(ValorDominio codEstadoDenuncia) {
        this.codEstadoDenuncia = codEstadoDenuncia;
    }

    public Afectado getAfectado() {
        return afectado;
    }

    public void setAfectado(Afectado afectado) {
        this.afectado = afectado;
    }

    public void addCausalAnalisis(DenunciaCausalAnalisis denunciaCausalAnalisis) {
        denunciaCausalAnalisis.setDenuncia(this);
        this.getCausalAnalisis().add(denunciaCausalAnalisis);
    }

    public void addCausalInicial(DenunciaCausalInicial denunciaCausalInicial) {
        denunciaCausalInicial.setDenuncia(this);
        this.getCausalInicial().add(denunciaCausalInicial);
    }

    public Boolean getEsCompetenciaParcial() {
        return esCompetenciaParcial;
    }

    public void setEsCompetenciaParcial(Boolean esCompetenciaParcial) {
        this.esCompetenciaParcial = esCompetenciaParcial;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDenuncia != null ? idDenuncia.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Denuncia)) {
            return false;
        }
        Denuncia other = (Denuncia) object;
        if ((this.idDenuncia == null && other.idDenuncia != null) || (this.idDenuncia != null && !this.idDenuncia.equals(other.idDenuncia))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.Denuncia[ idDenuncia=" + idDenuncia + " ]";
    }

    @Override
    public Long getId() {
        return idDenuncia;
    }

}
