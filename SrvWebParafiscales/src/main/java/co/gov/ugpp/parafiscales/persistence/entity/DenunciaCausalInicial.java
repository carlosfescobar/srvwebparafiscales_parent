package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "DENUNCIA_CAUSAL_INICIAL")
public class DenunciaCausalInicial extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "denCausalInicialSeq", sequenceName = "denuncia_causal_ini_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "denCausalInicialSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_DENUNCIA", referencedColumnName = "ID_DENUNCIA")
    @ManyToOne(fetch = FetchType.LAZY)
    private Denuncia denuncia;
    @JoinColumn(name = "COD_CAUSAL_INICIAL", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codCausalInicial;

    public DenunciaCausalInicial() {
    }

    public DenunciaCausalInicial(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Denuncia getDenuncia() {
        return denuncia;
    }

    public void setDenuncia(Denuncia denuncia) {
        this.denuncia = denuncia;
    }

    public ValorDominio getCodCausalInicial() {
        return codCausalInicial;
    }

    public void setCodCausalInicial(ValorDominio codCausalInicial) {
        this.codCausalInicial = codCausalInicial;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DenunciaCausalInicial)) {
            return false;
        }
        DenunciaCausalInicial other = (DenunciaCausalInicial) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.DenunciaCausalInicial[ id=" + id + " ]";
    }

}
