
package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DENUNCIA_HAS_COD_SUBSISTEMA")
public class DenunciaHasCodSubsistema implements IEntity<Long>  {
    
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "denunciaSubSistemasIdSeq", sequenceName = "denuncia_has_cod_sub_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "denunciaSubSistemasIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "COD_SUBSISTEMA_ID")
    private Long codSubsistemaId;
    
    @Size(max = 255)
    @Column(name = "COD_SUBSISTEMA")
    private String codSubsistema;
    
    @JoinColumn(name = "ID_DENUNCIA", referencedColumnName = "ID_DENUNCIA")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Denuncia idDenuncia;

    public DenunciaHasCodSubsistema() {
    }

    public DenunciaHasCodSubsistema(Long codSubsistemaId) {
        this.codSubsistemaId = codSubsistemaId;
    }

    public Long getCodSubsistemaId() {
        return codSubsistemaId;
    }

    public void setCodSubsistemaId(Long codSubsistemaId) {
        this.codSubsistemaId = codSubsistemaId;
    }

    public String getCodSubsistema() {
        return codSubsistema;
    }

    public void setCodSubsistema(String codSubsistema) {
        this.codSubsistema = codSubsistema;
    }

    public Denuncia getIdDenuncia() {
        return idDenuncia;
    }

    public void setIdDenuncia(Denuncia idDenuncia) {
        this.idDenuncia = idDenuncia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codSubsistemaId != null ? codSubsistemaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DenunciaHasCodSubsistema)) {
            return false;
        }
        DenunciaHasCodSubsistema other = (DenunciaHasCodSubsistema) object;
        if ((this.codSubsistemaId == null && other.codSubsistemaId != null) || (this.codSubsistemaId != null && !this.codSubsistemaId.equals(other.codSubsistemaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.DenunciaHasCodSubsistema[ codSubsistemaId=" + codSubsistemaId + " ]";
    }

    @Override
    public Long getId() {
        return codSubsistemaId;
    }
    
}
