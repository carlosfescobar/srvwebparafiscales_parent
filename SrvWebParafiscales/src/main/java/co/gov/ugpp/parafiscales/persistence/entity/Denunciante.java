package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DENUNCIANTE")
public class Denunciante extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDDENUNCIANTE")
    private Long iddenunciante;
    
    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @PrimaryKeyJoinColumn(name = "IDDENUNCIANTE", referencedColumnName = "ID")
    private Persona persona;
    
    @JoinColumn(name = "COD_TIPO_DENUNCIANTE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoDenunciante;

    @OneToMany(mappedBy = "idDenunciante", fetch = FetchType.LAZY)
    private List<Denuncia> denunciaList;

    public Denunciante() {
        this.setPersona(new Persona());
    }

    public Denunciante(Persona persona) {
        this.setPersona(persona);
    }

    public Long getIddenunciante() {
        return iddenunciante;
    }

    public void setIddenunciante(Long iddenunciante) {
        this.iddenunciante = iddenunciante;
    }

    public ValorDominio getCodTipoDenunciante() {
        return codTipoDenunciante;
    }

    public void setCodTipoDenunciante(ValorDominio codTipoDenunciante) {
        this.codTipoDenunciante = codTipoDenunciante;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<Denuncia> getDenunciaList() {
        return denunciaList;
    }

    public void setDenunciaList(List<Denuncia> denunciaList) {
        this.denunciaList = denunciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddenunciante != null ? iddenunciante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Denunciante)) {
            return false;
        }
        Denunciante other = (Denunciante) object;
        if ((this.iddenunciante == null && other.iddenunciante != null) || (this.iddenunciante != null && !this.iddenunciante.equals(other.iddenunciante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.Denunciante[ iddenunciante=" + iddenunciante + " ]";
    }

    @Override
    public Long getId() {
        return iddenunciante;
    }

}
