package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "DIVISION_CIIU")
public class DivisionCiiu implements IEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "DESC_DIVISION")
    private String descDivision;
    @JoinColumn(name = "ID_SECCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SeccionCiiu seccionCiiu;

    public DivisionCiiu() {
    }

    public DivisionCiiu(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getDescDivision() {
        return descDivision;
    }

    public void setDescDivision(String descDivision) {
        this.descDivision = descDivision;
    }

    public SeccionCiiu getSeccionCiiu() {
        return seccionCiiu;
    }

    public void setSeccionCiiu(SeccionCiiu seccionCiiu) {
        this.seccionCiiu = seccionCiiu;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DivisionCiiu)) {
            return false;
        }
        DivisionCiiu other = (DivisionCiiu) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
