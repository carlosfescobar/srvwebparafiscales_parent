package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "DOCUMENTACION_ESPERADA")
public class DocumentacionEsperada extends AbstractEntity<Long> {

    private static final long serialVersionUID = 3609751496209235871L;

    @Id
    @SequenceGenerator(name = "documentacionEsperadaIdSeq", sequenceName = "documentacion_esperada_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "documentacionEsperadaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ID_INSTANCIA_PROCESO_REANUD")
    private String idInstanciaProcesoReanudacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "ID_RADICADO_SALIDA_CORRESPOND")
    private String idRadicadoSalidaCorrespondencia;
    @Size(max = 20)
    @Column(name = "ID_RADICADO_ENTRADA_CORRESPOND")
    private String idRadicadoEntradaCorrespondencia;
    @Column(name = "FEC_ENTRADA_DOCUMENTOS")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecEntradaDocumentos;
    @Column(name = "FEC_SALIDA_DOCUMENTOS")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecSalidaDocumentos;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expediente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VAL_NOMBRE_MODELO_PROCESO")
    private String valNombreModeloProceso;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "VAL_DESCRIPCION")
    private String valDescripcion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "FUNCIONARIO")
    private String funcionario;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstado;
    @JoinColumn(name = "ID_RESPUESTA_DOCUMENTACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<DocumentacionEsperadaDocumentos> idDocumentos;

    public DocumentacionEsperada() {
    }

    public DocumentacionEsperada(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getIdInstanciaProcesoReanudacion() {
        return idInstanciaProcesoReanudacion;
    }

    public void setIdInstanciaProcesoReanudacion(String idInstanciaProcesoReanudacion) {
        this.idInstanciaProcesoReanudacion = idInstanciaProcesoReanudacion;
    }

    public String getIdRadicadoSalidaCorrespond() {
        return idRadicadoSalidaCorrespondencia;
    }

    public void setIdRadicadoSalidaCorrespond(String idRadicadoSalidaCorrespond) {
        this.idRadicadoSalidaCorrespondencia = idRadicadoSalidaCorrespond;
    }

    public String getIdRadicadoEntradaCorrespondencia() {
        return idRadicadoEntradaCorrespondencia;
    }

    public void setIdRadicadoEntradaCorrespondencia(String idRadicadoEntradaCorrespondencia) {
        this.idRadicadoEntradaCorrespondencia = idRadicadoEntradaCorrespondencia;
    }

    public Calendar getFecEntradaDocumentos() {
        return fecEntradaDocumentos;
    }

    public void setFecEntradaDocumentos(Calendar fecEntradaDocumentos) {
        this.fecEntradaDocumentos = fecEntradaDocumentos;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public String getValNombreModeloProceso() {
        return valNombreModeloProceso;
    }

    public void setValNombreModeloProceso(String valNombreModeloProceso) {
        this.valNombreModeloProceso = valNombreModeloProceso;
    }

    public String getValDescripcion() {
        return valDescripcion;
    }

    public void setValDescripcion(String valDescripcion) {
        this.valDescripcion = valDescripcion;
    }

    public String getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(String funcionario) {
        this.funcionario = funcionario;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public List<DocumentacionEsperadaDocumentos> getIdDocumentos() {
        if (idDocumentos == null) {
            idDocumentos = new ArrayList<DocumentacionEsperadaDocumentos>();
        }
        return idDocumentos;
    }

    public Calendar getFecSalidaDocumentos() {
        return fecSalidaDocumentos;
    }

    public void setFecSalidaDocumentos(Calendar fecSalidaDocumentos) {
        this.fecSalidaDocumentos = fecSalidaDocumentos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentacionEsperada)) {
            return false;
        }
        DocumentacionEsperada other = (DocumentacionEsperada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.RespuestaDocumentacion[ id=" + id + " ]";
    }

}
