package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "DOCUMENT_ESPERADA_DOCUMENTOS")
public class DocumentacionEsperadaDocumentos extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "documentacionEsperadaDocIdSeq", sequenceName = "doc_esperada_doc_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "documentacionEsperadaDocIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RESPUESTA_DOCUMENTACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentacionEsperada documentacionEsperada;
    @NotNull
    @Size(min = 1, max = 2000)
    @Column(name = "ID_DOCUMENTO")
    private String idDocumento;

    @Override
    public Long getId() {
        return this.id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public DocumentacionEsperada getDocumentacionEsperada() {
        return documentacionEsperada;
    }

    public void setDocumentacionEsperada(DocumentacionEsperada documentacionEsperada) {
        this.documentacionEsperada = documentacionEsperada;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentacionEsperadaDocumentos)) {
            return false;
        }
        DocumentacionEsperadaDocumentos other = (DocumentacionEsperadaDocumentos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.RespuestaDocumentDoc[ respuestaDocumentDocPK=" + id + " ]";
    }

}
