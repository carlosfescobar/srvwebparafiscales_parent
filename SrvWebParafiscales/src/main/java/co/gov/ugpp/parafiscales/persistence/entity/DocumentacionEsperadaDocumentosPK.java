package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author gchavezm
 */
@Embeddable
public class DocumentacionEsperadaDocumentosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RESPUESTA_DOCUMENTACION")
    private Long idRespuestaDocumentacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ID_DOCUMENTO")
    private String idDocumento;

    public DocumentacionEsperadaDocumentosPK() {
    }

    public DocumentacionEsperadaDocumentosPK(Long idRespuestaDocumentacion, String idDocumento) {
        this.idRespuestaDocumentacion = idRespuestaDocumentacion;
        this.idDocumento = idDocumento;
    }

    public Long getIdRespuestaDocumentacion() {
        return idRespuestaDocumentacion;
    }

    public void setIdRespuestaDocumentacion(Long idRespuestaDocumentacion) {
        this.idRespuestaDocumentacion = idRespuestaDocumentacion;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRespuestaDocumentacion != null ? idRespuestaDocumentacion.hashCode() : 0);
        hash += (idDocumento != null ? idDocumento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentacionEsperadaDocumentosPK)) {
            return false;
        }
        DocumentacionEsperadaDocumentosPK other = (DocumentacionEsperadaDocumentosPK) object;
        if ((this.idRespuestaDocumentacion == null && other.idRespuestaDocumentacion != null) || (this.idRespuestaDocumentacion != null && !this.idRespuestaDocumentacion.equals(other.idRespuestaDocumentacion))) {
            return false;
        }
        if ((this.idDocumento == null && other.idDocumento != null) || (this.idDocumento != null && !this.idDocumento.equals(other.idDocumento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.RespuestaDocumentDocPK[ idRespuestaDocumentacion=" + idRespuestaDocumentacion + ", idDocumento=" + idDocumento + " ]";
    }

}
