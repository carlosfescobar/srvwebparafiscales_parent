package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "DOCUMENTO_ECM")
public class DocumentoEcm extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ID", updatable = false)
    private String id;
    @Size(max = 100)
    @Column(name = "VAL_NOMBRE")
    private String valNombre;
    @Column(name = "FEC_DOCUMENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecDocumento;
    @Column(name = "NUMERO_RADICADO")
    private String numeroRadicado;
    @Column(name = "FEC_RADICADO_CORRESPONDENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicado;
    @Column(name = "COD_TIPO_DOCUMENTO")
    private String codTipoDocumento;
    @Column(name = "VAL_PAGINAS")
    private Long valPaginas;
    @Column(name = "VAL_AUTO_ORIGINADOR")
    private String valAutoOriginador;
    @Column(name = "VAL_NATURALEZA_DOCUMENTO")
    private String valNaturalezaDocumento;
    @Column(name = "VAL_ORIGEN_DOCUMENTO")
    private String valOrigenDocumento;
    @Column(name = "DESC_OBSERVACION_LEGIBILIDAD")
    private String descObsLegibilidad;
    @Column(name = "ES_MOVER")
    private Boolean esMover;
    @Column(name = "VAL_NOMBRE_TIPO_DOCUMENTAL")
    private String valNombreTipoDocumental;
    @Column(name = "NUM_FOLIOS")
    private Long numFolios;
    @Column(name = "VAL_LEGIBLE")
    private String valLegible;
    @Column(name = "VAL_TIPO_FIRMA")
    private String valTipoFirma;
    @JoinColumn(name = "ID_METADATA_DOCUMENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MetadataDocumento metadataDocumento;
    @JoinColumn(name = "ID_DOCUMENTO_ECM", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ExpedienteDocumentoEcm> expedienteDocumentoList;

    public DocumentoEcm() {
    }

    public DocumentoEcm(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValNombre() {
        return valNombre;
    }

    public void setValNombre(String valNombre) {
        this.valNombre = valNombre;
    }

    public Calendar getFecDocumento() {
        return fecDocumento;
    }

    public void setFecDocumento(Calendar fecDocumento) {
        this.fecDocumento = fecDocumento;
    }

    public String getNumeroRadicado() {
        return numeroRadicado;
    }

    public void setNumeroRadicado(String numeroRadicado) {
        this.numeroRadicado = numeroRadicado;
    }

    public Calendar getFecRadicado() {
        return fecRadicado;
    }

    public void setFecRadicado(Calendar fecRadicado) {
        this.fecRadicado = fecRadicado;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public void setCodTipoDocumento(String codTipoDocumento) {
        this.codTipoDocumento = codTipoDocumento;
    }

    public Long getValPaginas() {
        return valPaginas;
    }

    public void setValPaginas(Long valPaginas) {
        this.valPaginas = valPaginas;
    }

    public String getValAutoOriginador() {
        return valAutoOriginador;
    }

    public void setValAutoOriginador(String valAutoOriginador) {
        this.valAutoOriginador = valAutoOriginador;
    }

    public String getValNaturalezaDocumento() {
        return valNaturalezaDocumento;
    }

    public void setValNaturalezaDocumento(String valNaturalezaDocumento) {
        this.valNaturalezaDocumento = valNaturalezaDocumento;
    }

    public String getValOrigenDocumento() {
        return valOrigenDocumento;
    }

    public void setValOrigenDocumento(String valOrigenDocumento) {
        this.valOrigenDocumento = valOrigenDocumento;
    }

    public String getDescObsLegibilidad() {
        return descObsLegibilidad;
    }

    public void setDescObsLegibilidad(String descObsLegibilidad) {
        this.descObsLegibilidad = descObsLegibilidad;
    }

    public Boolean getEsMover() {
        return esMover;
    }

    public void setEsMover(Boolean esMover) {
        this.esMover = esMover;
    }

    public String getValNombreTipoDocumental() {
        return valNombreTipoDocumental;
    }

    public void setValNombreTipoDocumental(String valNombreTipoDocumental) {
        this.valNombreTipoDocumental = valNombreTipoDocumental;
    }

    public Long getNumFolios() {
        return numFolios;
    }

    public void setNumFolios(Long numFolios) {
        this.numFolios = numFolios;
    }

    public String getValLegible() {
        return valLegible;
    }

    public void setValLegible(String valLegible) {
        this.valLegible = valLegible;
    }

    public String getValTipoFirma() {
        return valTipoFirma;
    }

    public void setValTipoFirma(String valTipoFirma) {
        this.valTipoFirma = valTipoFirma;
    }

    public MetadataDocumento getMetadataDocumento() {
        return metadataDocumento;
    }

    public void setMetadataDocumento(MetadataDocumento metadataDocumento) {
        this.metadataDocumento = metadataDocumento;
    }
    
      public List<ExpedienteDocumentoEcm> getExpedienteDocumentoList() {
        if (expedienteDocumentoList == null) {
            expedienteDocumentoList = new ArrayList<ExpedienteDocumentoEcm>();
        }
        return expedienteDocumentoList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentoEcm)) {
            return false;
        }
        DocumentoEcm other = (DocumentoEcm) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.DocumentoEcm[ id=" + id + " ]";
    }

}
