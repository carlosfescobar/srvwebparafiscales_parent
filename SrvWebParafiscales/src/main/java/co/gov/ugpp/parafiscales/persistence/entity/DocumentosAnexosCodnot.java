
package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DOCUMENTOS_ANEXOS_CODNOT")
public class DocumentosAnexosCodnot extends AbstractEntity<Long> {
    
    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "docAnexoNotiIdSeq", sequenceName = "doc_anexos_codnot_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "docAnexoNotiIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "DOCUMENTOS_ANEXOS_NOTIF_ID")
    private Long documentosAnexosNotifId;
    
    @Size(max = 255)
    @Column(name = "DOCUMENTOS_ANEXOS_NOTIF")
    private String documentosAnexosNotif;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Notificacion idNotificacion;

    public DocumentosAnexosCodnot() {
    }

    public DocumentosAnexosCodnot(Long documentosAnexosNotifId) {
        this.documentosAnexosNotifId = documentosAnexosNotifId;
    }

    public Long getDocumentosAnexosNotifId() {
        return documentosAnexosNotifId;
    }

    public void setDocumentosAnexosNotifId(Long documentosAnexosNotifId) {
        this.documentosAnexosNotifId = documentosAnexosNotifId;
    }

    public String getDocumentosAnexosNotif() {
        return documentosAnexosNotif;
    }

    public void setDocumentosAnexosNotif(String documentosAnexosNotif) {
        this.documentosAnexosNotif = documentosAnexosNotif;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (documentosAnexosNotifId != null ? documentosAnexosNotifId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentosAnexosCodnot)) {
            return false;
        }
        DocumentosAnexosCodnot other = (DocumentosAnexosCodnot) object;
        if ((this.documentosAnexosNotifId == null && other.documentosAnexosNotifId != null) || (this.documentosAnexosNotifId != null && !this.documentosAnexosNotifId.equals(other.documentosAnexosNotifId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosCodnot[ documentosAnexosNotifId=" + documentosAnexosNotifId + " ]";
    }

    @Override
    public Long getId() {
        return documentosAnexosNotifId;
    }

    public Notificacion getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Notificacion idNotificacion) {
        this.idNotificacion = idNotificacion;
    }
    
}
