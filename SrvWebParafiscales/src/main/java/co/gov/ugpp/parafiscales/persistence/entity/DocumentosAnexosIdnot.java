package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "DOCUMENTOS_ANEXOS_IDNOT")
public class DocumentosAnexosIdnot extends AbstractEntity<Long> {
    

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "docAnexosIdNotIdSeq", sequenceName = "doc_anexos_idnot_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "docAnexosIdNotIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_DOCUMENTOS_ANEXOS_ID")
    private Long idDocumentosAnexosId;

    @Size(max = 255)
    @Column(name = "ID_DOCUMENTOS_ANEXOS")
    private String idDocumentosAnexos;
    
    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Notificacion idNotificacion;

    public DocumentosAnexosIdnot() {
    }

    public DocumentosAnexosIdnot(Long idDocumentosAnexosId) {
        this.idDocumentosAnexosId = idDocumentosAnexosId;
    }

    public Long getIdDocumentosAnexosId() {
        return idDocumentosAnexosId;
    }

    public void setIdDocumentosAnexosId(Long idDocumentosAnexosId) {
        this.idDocumentosAnexosId = idDocumentosAnexosId;
    }

    public String getIdDocumentosAnexos() {
        return idDocumentosAnexos;
    }

    public void setIdDocumentosAnexos(String idDocumentosAnexos) {
        this.idDocumentosAnexos = idDocumentosAnexos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDocumentosAnexosId != null ? idDocumentosAnexosId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DocumentosAnexosIdnot)) {
            return false;
        }
        DocumentosAnexosIdnot other = (DocumentosAnexosIdnot) object;
        if ((this.idDocumentosAnexosId == null && other.idDocumentosAnexosId != null) || (this.idDocumentosAnexosId != null && !this.idDocumentosAnexosId.equals(other.idDocumentosAnexosId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosIdnot[ idDocumentosAnexosId=" + idDocumentosAnexosId + " ]";
    }

    @Override
    public Long getId() {
        return idDocumentosAnexosId;
    }

    public Notificacion getIdNotificacion() {
        return idNotificacion;
    }

    public void setIdNotificacion(Notificacion idNotificacion) {
        this.idNotificacion = idNotificacion;
    }

}
