package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "DOMINIO")
public class Dominio implements IEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "COD_DOMINIO", updatable = false)
    private String id;
    @Column(name = "VAL_NOM_DOMINIO")
    private String valNomDominio;
    @Column(name = "DESC_DOMINIO")
    private String descDominio;
    @Column(name = "COD_DIRECCION")
    private String codDireccion;
    @Column(name = "ID_USUARIO")
    private String idUsuario;
    @Column(name = "FEC_CREACION_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecCreacionRegistro;
    @Column(name = "FEC_ACTUALIZACION_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecActualizacionRegistro;
    @OneToMany(mappedBy = "dominio", fetch = FetchType.LAZY)
    private List<ValorDominio> valorDominioList;

    public Dominio() {
    }

    public Dominio(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    protected void setId(String id) {
        this.id = id;
    }

    public List<ValorDominio> getValorDominioList() {
        return valorDominioList;
    }

    public void setValorDominioList(List<ValorDominio> valorDominioList) {
        this.valorDominioList = valorDominioList;
    }

    public String getValNomDominio() {
        return valNomDominio;
    }

    public void setValNomDominio(String valNomDominio) {
        this.valNomDominio = valNomDominio;
    }

    public String getDescDominio() {
        return descDominio;
    }

    public void setDescDominio(String descDominio) {
        this.descDominio = descDominio;
    }

    public String getCodDireccion() {
        return codDireccion;
    }

    public void setCodDireccion(String codDireccion) {
        this.codDireccion = codDireccion;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Calendar getFecCreacionRegistro() {
        return fecCreacionRegistro;
    }

    public void setFecCreacionRegistro(Calendar fecCreacionRegistro) {
        this.fecCreacionRegistro = fecCreacionRegistro;
    }

    public Calendar getFecActualizacionRegistro() {
        return fecActualizacionRegistro;
    }

    public void setFecActualizacionRegistro(Calendar fecActualizacionRegistro) {
        this.fecActualizacionRegistro = fecActualizacionRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Dominio)) {
            return false;
        }
        Dominio other = (Dominio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Dominio[ id=" + id + " ]";
    }

}
