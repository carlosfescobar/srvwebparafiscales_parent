package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "EFECTIVIDAD")
public class Efectividad extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "efectividadIdSeq", sequenceName = "efectividad_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "efectividadIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "VAL_PROCENTAJE_EFECTIVIDAD")
    private float valProcentajeEfectividad;
    @Column(name = "VAL_NUMERO_SOLICITUDES")
    private Long valNumeroSolicitudes;
    @JoinColumn(name = "COD_RESULTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codResultado;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private EntidadExterna entidadExterna;

    public Efectividad() {
    }

    public Efectividad(Long id) {
        this.id = id;
    }

    public Efectividad(Long id, short valProcentajeEfectividad) {
        this.id = id;
        this.valProcentajeEfectividad = valProcentajeEfectividad;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public float getValProcentajeEfectividad() {
        return valProcentajeEfectividad;
    }

    public void setValProcentajeEfectividad(float valProcentajeEfectividad) {
        this.valProcentajeEfectividad = valProcentajeEfectividad;
    }

    public Long getValNumeroSolicitudes() {
        return valNumeroSolicitudes;
    }

    public void setValNumeroSolicitudes(Long valNumeroSolicitudes) {
        this.valNumeroSolicitudes = valNumeroSolicitudes;
    }

    public ValorDominio getCodResultado() {
        return codResultado;
    }

    public void setCodResultado(ValorDominio codResultado) {
        this.codResultado = codResultado;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Efectividad)) {
            return false;
        }
        Efectividad other = (Efectividad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Efectividad[ id=" + id + " ]";
    }

}
