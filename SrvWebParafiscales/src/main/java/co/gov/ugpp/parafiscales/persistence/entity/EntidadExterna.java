package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.eclipse.persistence.annotations.BatchFetch;
import org.eclipse.persistence.annotations.BatchFetchType;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "ENTIDAD_EXTERNA")
public class EntidadExterna extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
    private Persona persona;
    @Column(name = "ES_OPERADOR_PILA")
    private Boolean esOperadorPila;
    @JoinColumn(name = "COD_TIPO_ENTIDAD_EXTERNA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoEntidadExterna;
    @JoinColumn(name = "ID_TRASLADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Traslado idTraslado;
    @JoinColumn(name = "COD_TIPO_PROVEEDOR_INFO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoProveedor;
    @ElementCollection(fetch = FetchType.LAZY)
    @BatchFetch(value = BatchFetchType.JOIN)
    @Column(name = "COD_TIPO_CONSULTA")
    @CollectionTable(name = "ENT_EXT_TIPO_CONSUL", joinColumns = {
        @JoinColumn(name = "ID_ENTIDAD_EXTERNA")})
    private Set<String> tipoConsulta;

    public EntidadExterna() {
        this(new Persona());
    }

    public EntidadExterna(Persona persona) {
        this.setPersona(persona);
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Traslado getIdTraslado() {
        return idTraslado;
    }

    public void setIdTraslado(Traslado idTraslado) {
        this.idTraslado = idTraslado;
    }

    public Boolean getEsOperadorPila() {
        return esOperadorPila;
    }

    public void setEsOperadorPila(Boolean esOperadorPila) {
        this.esOperadorPila = esOperadorPila;
    }

    public ValorDominio getCodTipoEntidadExterna() {
        return codTipoEntidadExterna;
    }

    public void setCodTipoEntidadExterna(ValorDominio codTipoEntidadExterna) {
        this.codTipoEntidadExterna = codTipoEntidadExterna;
    }

    public Persona getPersona() {
        return persona;
    }

    public ValorDominio getCodTipoProveedor() {
        return codTipoProveedor;
    }

    public void setCodTipoProveedor(ValorDominio codTipoProveedor) {
        this.codTipoProveedor = codTipoProveedor;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
        if (persona != null) {
            this.id = persona.getId();
        }
    }

    public Set<String> getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(Set<String> tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadExterna)) {
            return false;
        }
        EntidadExterna other = (EntidadExterna) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.EntidadExterna[ idPersona=" + id + " ]";
    }

}
