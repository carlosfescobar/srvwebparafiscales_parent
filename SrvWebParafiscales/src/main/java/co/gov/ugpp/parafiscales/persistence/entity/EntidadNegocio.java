package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "ENTIDAD_NEGOCIO")
@NamedQueries({
    @NamedQuery(name = "entidadNegocio.findByIdEntidadNegocio",
        query ="SELECT e FROM EntidadNegocio e WHERE e.idEntidadNegocio = :idEntidadNegocio AND e.codProceso.id = :codProceso")})
public class EntidadNegocio extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "entidadNegocioIdSeq", sequenceName = "entidad_negocio_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entidadNegocioIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "COD_PROCESO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codProceso;
    @Size(max = 2000)
    @Column(name = "ID_ENTIDAD_NEGOCIO")
    private String idEntidadNegocio;
    @JoinColumn(name = "ID_ENTIDAD_NEGOCIO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<EntidadNegocioArchivoTemporal> archivosTemporales;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<EntidadNegocioArchivoTemporal> getArchivosTemporales() {
        if (archivosTemporales == null){
            archivosTemporales = new ArrayList<EntidadNegocioArchivoTemporal>();
        }
        return archivosTemporales;
    }

    public ValorDominio getCodProceso() {
        return codProceso;
    }

    public void setCodProceso(ValorDominio codProceso) {
        this.codProceso = codProceso;
    }

    public String getIdEntidadNegocio() {
        return idEntidadNegocio;
    }

    public void setIdEntidadNegocio(String idEntidadNegocio) {
        this.idEntidadNegocio = idEntidadNegocio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadNegocio)) {
            return false;
        }
        EntidadNegocio other = (EntidadNegocio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.AccionActoAdministrativo[ id=" + id + " ]";
    }

}
