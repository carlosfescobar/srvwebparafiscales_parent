package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "ENTIDAD_NEGOCIO_ARCHIVO_TMP")
public class EntidadNegocioArchivoTemporal extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "entidadNegocioArchivoTmpIdSeq", sequenceName = "archivo_negocio_tmp_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "entidadNegocioArchivoTmpIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private Archivo archivoTemporal;
    @JoinColumn(name = "COD_TIPO_ARCHIVO_TEMPORAL", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ValorDominio codTipoArchivoTemporal;
    @JoinColumn(name = "ID_ENTIDAD_NEGOCIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private EntidadNegocio entidadNegocio;

    public EntidadNegocioArchivoTemporal() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivoTemporal() {
        return archivoTemporal;
    }

    public void setArchivoTemporal(Archivo archivoTemporal) {
        this.archivoTemporal = archivoTemporal;
    }

    public EntidadNegocio getEntidadNegocio() {
        return entidadNegocio;
    }

    public void setEntidadNegocio(EntidadNegocio entidadNegocio) {
        this.entidadNegocio = entidadNegocio;
    }

    public ValorDominio getCodTipoArchivoTemporal() {
        return codTipoArchivoTemporal;
    }

    public void setCodTipoArchivoTemporal(ValorDominio codTipoArchivoTemporal) {
        this.codTipoArchivoTemporal = codTipoArchivoTemporal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EntidadNegocioArchivoTemporal)) {
            return false;
        }
        EntidadNegocioArchivoTemporal other = (EntidadNegocioArchivoTemporal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.AccionActoAdministrativo[ id=" + id + " ]";
    }

}
