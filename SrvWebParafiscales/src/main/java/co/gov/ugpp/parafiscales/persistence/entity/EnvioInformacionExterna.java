package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "ENVIO_INFORMACION_EXTERNA")
public class EnvioInformacionExterna extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_RADICADO_ENVIO", updatable = false)
    private String idRadicadoEnvio;
    @Column(name = "ID_RADICADO_REQUERIMIENTO")
    private String idRadicadoRequerimiento;
    @Column(name = "FEC_RADICADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicado;
    @Size(max = 100)
    @Column(name = "ID_USUARIO_COLOCACION")
    private String idUsuarioColocacion;
    @JoinColumn(name = "COD_TIPO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoSolicitud;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstado;
    @Size(min = 1, max = 50)
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private String valNumeroIdentificacion;
    @Column(name = "COD_TIPO_IDENTIFICACION")
    private String codTipoIdentificacion;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION", referencedColumnName = "ID_VERSION")
    })
    private Formato formato;

    public EnvioInformacionExterna() {
    }

    @Override
    public String getId() {
        return idRadicadoEnvio;
    }

    public void setIdRadicadoEnvio(String idRadicadoEnvio) {
        this.idRadicadoEnvio = idRadicadoEnvio;
    }
    
    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    public String getIdRadicadoRequerimiento() {
        return idRadicadoRequerimiento;
    }

    public void setIdRadicadoRequerimiento(String idRadicadoRequerimiento) {
        this.idRadicadoRequerimiento = idRadicadoRequerimiento;
    }

    public Calendar getFecRadicado() {
        return fecRadicado;
    }

    public void setFecRadicado(Calendar fecRadicado) {
        this.fecRadicado = fecRadicado;
    }

    public String getIdUsuarioColocacion() {
        return idUsuarioColocacion;
    }

    public void setIdUsuarioColocacion(String idUsuarioColocacion) {
        this.idUsuarioColocacion = idUsuarioColocacion;
    }

    public ValorDominio getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    public void setCodTipoSolicitud(ValorDominio codTipoSolicitud) {
        this.codTipoSolicitud = codTipoSolicitud;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio idCodEstado) {
        this.codEstado = idCodEstado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRadicadoEnvio != null ? idRadicadoEnvio.hashCode() : 0);
        return hash;
    }

    public String getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    public void setValNumeroIdentificacion(String valNumeroIdentificacion) {
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    public String getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(String codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the idRadicadoEnvio fields are not set
        if (!(object instanceof EnvioInformacionExterna)) {
            return false;
        }
        EnvioInformacionExterna other = (EnvioInformacionExterna) object;
        if ((this.idRadicadoEnvio == null && other.idRadicadoEnvio != null) || (this.idRadicadoEnvio != null && !this.idRadicadoEnvio.equals(other.idRadicadoEnvio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.EnvioInformacionExterna[ id=" + idRadicadoEnvio + " ]";
    }

}
