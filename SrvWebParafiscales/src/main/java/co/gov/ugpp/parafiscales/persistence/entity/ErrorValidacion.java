package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jgonzalezt
 */
@Entity
@Table(name = "ERROR_VALIDACION")
public class ErrorValidacion  extends AbstractEntity<Long> {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @SequenceGenerator(name = "errorvalidacionSeq", sequenceName = "ERROR_VALIDACION_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "errorvalidacionSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ERROR")
    private Long idError;
    @Column(name = "ID_EJECUCION_VALIDACION")
    private Long idEjecucionValidacion;
    @Column(name = "ID_VALIDACION")
    private Long idValidacion;
    @Size(max = 20)
    @Column(name = "COD_ERROR")
    private String codError;
    @Size(max = 2000)
    @Column(name = "VAL_DESC_ERROR")
    private String valDescError;
    @Size(max = 100)
    @Column(name = "VAL_UBICACION_ERROR")
    private String valUbicacionError;

    public ErrorValidacion() {
    }

    public ErrorValidacion(Long idError) {
        this.idError = idError;
    }

    public Long getIdError() {
        return idError;
    }

    public void setIdError(Long idError) {
        this.idError = idError;
    }

    public Long getIdEjecucionValidacion() {
        return idEjecucionValidacion;
    }

    public void setIdEjecucionValidacion(Long idEjecucionValidacion) {
        this.idEjecucionValidacion = idEjecucionValidacion;
    }

    public Long getIdValidacion() {
        return idValidacion;
    }

    public void setIdValidacion(Long idValidacion) {
        this.idValidacion = idValidacion;
    }

    public String getCodError() {
        return codError;
    }

    public void setCodError(String codError) {
        this.codError = codError;
    }

    public String getValDescError() {
        return valDescError;
    }

    public void setValDescError(String valDescError) {
        this.valDescError = valDescError;
    }

    public String getValUbicacionError() {
        return valUbicacionError;
    }

    public void setValUbicacionError(String valUbicacionError) {
        this.valUbicacionError = valUbicacionError;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idError != null ? idError.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ErrorValidacion)) {
            return false;
        }
        ErrorValidacion other = (ErrorValidacion) object;
        if ((this.idError == null && other.idError != null) || (this.idError != null && !this.idError.equals(other.idError))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.ErrorValidacion[ idError=" + idError + " ]";
    }

    @Override
    public Long getId() {
        return this.idError;
    }
    
}
