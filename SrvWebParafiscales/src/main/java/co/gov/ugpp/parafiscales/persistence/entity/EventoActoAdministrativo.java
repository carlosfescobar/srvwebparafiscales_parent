package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "EVENTO_ACTO_ADMINISTRATIVO")
public class EventoActoAdministrativo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "envioActoAdmIdSeq", sequenceName = "envio_acto_adm_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "envioActoAdmIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_RADICADO_SALIDA")
    private String idRadicadoSalida;
    @Column(name = "FEC_RADICADO_SALIDA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoSalida;
    @JoinColumn(name = "COD_ESTADO_NOTIFICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadNotificacion;
    @JoinColumn(name = "ID_TRAZABILIDAD_ACTO_ADM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TrazabilidadActoAdministrativo trazabilidadActoAdministrativo;
    @Column(name = "DESC_CAUSAL_DEVOLUCION")
    private String descCausalDevolucion;
    @JoinColumn(name = "UBICACION_NOTIFICADA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ubicacion ubicacionNotificada;

    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    public void setIdRadicadoSalida(String idRadicadoSalida) {
        this.idRadicadoSalida = idRadicadoSalida;
    }

    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    public void setFecRadicadoSalida(Calendar fecRadicadoSalida) {
        this.fecRadicadoSalida = fecRadicadoSalida;
    }

    public ValorDominio getCodEstadNotificacion() {
        return codEstadNotificacion;
    }

    public void setCodEstadNotificacion(ValorDominio codEstadNotificacion) {
        this.codEstadNotificacion = codEstadNotificacion;
    }

    public TrazabilidadActoAdministrativo getTrazabilidadActoAdministrativo() {
        return trazabilidadActoAdministrativo;
    }

    public void setTrazabilidadActoAdministrativo(TrazabilidadActoAdministrativo trazabilidadActoAdministrativo) {
        this.trazabilidadActoAdministrativo = trazabilidadActoAdministrativo;
    }

    public String getDescCausalDevolucion() {
        return descCausalDevolucion;
    }

    public void setDescCausalDevolucion(String descCausalDevolucion) {
        this.descCausalDevolucion = descCausalDevolucion;
    }
    
    public Ubicacion getUbicacionNotificada() {
        return ubicacionNotificada;
    }

    public void setUbicacionNotificada(Ubicacion ubicacionNotificada) {
        this.ubicacionNotificada = ubicacionNotificada;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventoActoAdministrativo)) {
            return false;
        }
        EventoActoAdministrativo other = (EventoActoAdministrativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.AccionActoAdministrativo[ id=" + id + " ]";
    }

}
