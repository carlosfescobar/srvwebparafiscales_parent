package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "EVENTO_ENVIO")
public class EventoEnvio extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "eventoEnvioIdSeq", sequenceName = "evento_envio_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "eventoEnvioIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstado;
    @JoinColumn(name = "COD_CAUSAL_DEVOLUCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codCausalDevolucion;
    @Column(name = "ES_APROBADO_DEVOLUCION")
    private Boolean esAprobadoDevolucion;
    @Column(name = "ID_RADICADO_SALIDA")
    private String idRadicadoSalida;
    @Column(name = "FEC_RADICADO_SALIDA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecRadicadoSalida;

    public EventoEnvio() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public ValorDominio getCodCausalDevolucion() {
        return codCausalDevolucion;
    }

    public void setCodCausalDevolucion(ValorDominio codCausalDevolucion) {
        this.codCausalDevolucion = codCausalDevolucion;
    }

    public Boolean getEsAprobadoDevolucion() {
        return esAprobadoDevolucion;
    }

    public void setEsAprobadoDevolucion(Boolean esAprobadoDevolucion) {
        this.esAprobadoDevolucion = esAprobadoDevolucion;
    }

    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    public void setIdRadicadoSalida(String idRadicadoSalida) {
        this.idRadicadoSalida = idRadicadoSalida;
    }

    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    public void setFecRadicadoSalida(Calendar fecRadicadoSalida) {
        this.fecRadicadoSalida = fecRadicadoSalida;
    }

}
