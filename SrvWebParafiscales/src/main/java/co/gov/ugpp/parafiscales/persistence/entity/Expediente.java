package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "EXPEDIENTE")
public class Expediente extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "ID", updatable = false)
    private String id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SECCION")
    private String idSeccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SUBSECCION")
    private String idSubseccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SERIE_DOCUMENTAL")
    private String idSerieDocuemental;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_SUBSERIE_DOCUMENTAL")
    private String idSubserieDocumental;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_TIPO_DOCUMENTAL")
    private String idTipoDocumental;
    @Column(name = "ANO_APERTURA")
    private String anoApertura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FONDO")
    private String idFondo;
    @Size(max = 2000)
    @Column(name = "DESC_DESCRIPCION")
    private String descDescripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_ENTIDAD_PREDECESORA")
    private String idEntidadPredecesora;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "expediente", fetch = FetchType.LAZY)
    private List<Solicitud> solicitudList;
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona persona;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ExpedienteDocumentoEcm> expedienteDocumentoList;
    @Size(max = 250)
    @Column(name = "NOMBRE_SECCION")
    private String seccion;
    @Size(max = 250)
    @Column(name = "NOMBRE_SUBSECCION")
    private String subSeccion;
    @Size(max = 250)
    @Column(name = "NOMBRE_SERIE")
    private String nombreSerie;
    @Size(max = 250)
    @Column(name = "NOMBRE_SUBSERIE")
    private String nombreSubSerie;
    @Size(max = 250)
    @Column(name = "ID_CARPETA_FILE_NET")
    private String idCarpetaFileNet;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstado;
    @OneToMany(mappedBy = "idexpediente", fetch = FetchType.LAZY)
    private List<ConciliacionContable> conciliacionContableList;
    @OneToMany(mappedBy = "idexpediente", fetch = FetchType.LAZY)
    private List<Nomina> nominaLiquidadorList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idexpediente")
    private List<Pila> pilaList;

    public Expediente() {
    }

    public Expediente(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(String idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getAnoApertura() {
        return anoApertura;
    }

    public void setAnoApertura(String anoApertura) {
        this.anoApertura = anoApertura;
    }

    public String getIdSubseccion() {
        return idSubseccion;
    }

    public void setIdSubseccion(String idSubseccion) {
        this.idSubseccion = idSubseccion;
    }

    public String getIdSerieDocuemental() {
        return idSerieDocuemental;
    }

    public void setIdSerieDocuemental(String idSerieDocuemental) {
        this.idSerieDocuemental = idSerieDocuemental;
    }

    public String getIdSubserieDocumental() {
        return idSubserieDocumental;
    }

    public void setIdSubserieDocumental(String idSubserieDocumental) {
        this.idSubserieDocumental = idSubserieDocumental;
    }

    public String getIdTipoDocumental() {
        return idTipoDocumental;
    }

    public void setIdTipoDocumental(String idTipoDocumental) {
        this.idTipoDocumental = idTipoDocumental;
    }

    public String getIdFondo() {
        return idFondo;
    }

    public void setIdFondo(String idFondo) {
        this.idFondo = idFondo;
    }

    public String getDescDescripcion() {
        return descDescripcion;
    }

    public void setDescDescripcion(String descDescripcion) {
        this.descDescripcion = descDescripcion;
    }

    public String getIdEntidadPredecesora() {
        return idEntidadPredecesora;
    }

    public void setIdEntidadPredecesora(String idEntidadPredecesora) {
        this.idEntidadPredecesora = idEntidadPredecesora;
    }

    public List<Solicitud> getSolicitudList() {
        return solicitudList;
    }

    public void setSolicitudList(List<Solicitud> solicitudList) {
        this.solicitudList = solicitudList;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getSubSeccion() {
        return subSeccion;
    }

    public void setSubSeccion(String subSeccion) {
        this.subSeccion = subSeccion;
    }

    public String getNombreSerie() {
        return nombreSerie;
    }

    public void setNombreSerie(String nombreSerie) {
        this.nombreSerie = nombreSerie;
    }

    public String getNombreSubSerie() {
        return nombreSubSerie;
    }

    public void setNombreSubSerie(String nombreSubSerie) {
        this.nombreSubSerie = nombreSubSerie;
    }

    public Persona getPersona() {
        return persona;
    }

    public List<ExpedienteDocumentoEcm> getExpedienteDocumentoList() {
        if (expedienteDocumentoList == null) {
            expedienteDocumentoList = new ArrayList<ExpedienteDocumentoEcm>();
        }
        return expedienteDocumentoList;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public String getIdCarpetaFileNet() {
        return idCarpetaFileNet;
    }

    public void setIdCarpetaFileNet(String idCarpetaFileNet) {
        this.idCarpetaFileNet = idCarpetaFileNet;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Expediente[ id=" + id + " ]";
    }

    /**
     * @return the conciliacionContableList
     */
    public List<ConciliacionContable> getConciliacionContableList() {
        return conciliacionContableList;
    }

    /**
     * @param conciliacionContableList the conciliacionContableList to set
     */
    public void setConciliacionContableList(List<ConciliacionContable> conciliacionContableList) {
        this.conciliacionContableList = conciliacionContableList;
    }

    /**
     * @return the nominaLiquidadorList
     */
    public List<Nomina> getNominaLiquidadorList() {
        return nominaLiquidadorList;
    }

    /**
     * @param nominaLiquidadorList the nominaLiquidadorList to set
     */
    public void setNominaLiquidadorList(List<Nomina> nominaLiquidadorList) {
        this.nominaLiquidadorList = nominaLiquidadorList;
    }

    /**
     * @return the pilaList
     */
    public List<Pila> getPilaList() {
        return pilaList;
    }

    /**
     * @param pilaList the pilaList to set
     */
    public void setPilaList(List<Pila> pilaList) {
        this.pilaList = pilaList;
    }
}
