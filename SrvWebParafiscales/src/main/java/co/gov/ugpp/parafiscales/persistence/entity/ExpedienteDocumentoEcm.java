package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "EXPEDIENTE_DOCUMENTO_ECM")
@NamedQueries({
    @NamedQuery(name = "expedienteDocumento.findByDocumento",
            query = "SELECT e FROM ExpedienteDocumentoEcm e WHERE e.documentoEcm.id = :idDocumento"),
    @NamedQuery(name = "expedienteDocumento.findByExpedienteAndDocumento",
            query = "SELECT e FROM ExpedienteDocumentoEcm e WHERE e.idExpediente = :idExpediente AND e.documentoEcm.id = :idDocumento")})
public class ExpedienteDocumentoEcm extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "expedienteDocEcmIdSeq", sequenceName = "expediente_documento_ec_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "expedienteDocEcmIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Basic(optional = false)
    @Size(max = 30)
    @Column(name = "ID_EXPEDIENTE")
    private String idExpediente;
    @JoinColumn(name = "ID_DOCUMENTO_ECM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentoEcm documentoEcm;
    @JoinColumn(name = "COD_ORIGEN", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codOrigen;

    public ExpedienteDocumentoEcm() {
    }

    public ExpedienteDocumentoEcm(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public DocumentoEcm getDocumentoEcm() {
        return documentoEcm;
    }

    public void setDocumentoEcm(DocumentoEcm idDocumentoEcm) {
        this.documentoEcm = idDocumentoEcm;
    }

    public ValorDominio getCodOrigen() {
        return codOrigen;
    }

    public void setCodOrigen(ValorDominio codOrigen) {
        this.codOrigen = codOrigen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExpedienteDocumentoEcm)) {
            return false;
        }
        ExpedienteDocumentoEcm other = (ExpedienteDocumentoEcm) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ExpedienteDocumentoEcm[ id=" + id + " ]";
    }

}
