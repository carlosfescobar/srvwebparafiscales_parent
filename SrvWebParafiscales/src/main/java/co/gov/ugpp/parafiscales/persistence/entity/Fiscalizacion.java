package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author zrodriguez
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "fiscalizacion.findByExpediente",
            query = "SELECT f FROM Fiscalizacion f WHERE f.expediente.id = :idExpediente"),
    @NamedQuery(name = "fiscalizacion.findByAportanteAndPeriodo",
            query = "SELECT f FROM Fiscalizacion f WHERE f.aportante.persona.valNumeroIdentificacion = :numeroIdentificacion AND f.aportante.persona.codTipoIdentificacion.id = :tipoIdentificacion AND f.fecInicio >= :fechaInicio AND f.fecFin <= :fechaFin"),
    @NamedQuery(name = "fiscalizacion.findByAportante",
            query = "SELECT f FROM Fiscalizacion f WHERE f.aportante.persona.valNumeroIdentificacion = :numeroIdentificacion AND f.aportante.persona.codTipoIdentificacion.id = :tipoIdentificacion")})
@Table(name = "FISCALIZACION")
public class Fiscalizacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "fiscalizacionIdSeq", sequenceName = "fiscalizacion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscalizacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_DENUNCIA", referencedColumnName = "ID_DENUNCIA")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Denuncia denuncia;
    @Size(max = 20)
    @Column(name = "ID_LIQUIDADOR")
    private String idLiquidador;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Expediente expediente;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Aportante aportante;
    @JoinColumn(name = "ID_ACTO_REQUERIMIENTO_INFO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actoRequerimientoInformacion;
    @JoinColumn(name = "ID_ACTO_AUTO_COMISORIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actoAutoComisorio;
    @JoinColumn(name = "ID_REQ_DEC_CORREGIR_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento requerimientoDeclararCorregirParcial;
    @JoinColumn(name = "ID_ACTO_AUTO_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actoAutoArchivo;
    @Column(name = "ES_ENVIO_COMITE")
    private Boolean esEnvioComite;
    @Column(name = "ES_MENOR_CUANTIA")
    private Boolean esMenorCuantia;
    @Column(name = "ES_CONTINUADO_PROCESO")
    private Boolean esContinuadoProceso;
    @Column(name = "ES_OMISO_PENSION")
    private Boolean esOmisoPension;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FiscalizacionIncumplimiento> codIncumplimiento;
    @JoinColumn(name = "ID_ACTO_REQUERIM_DECLAR_CORRE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actorequerimientoDeclararCorregir;
    @JoinColumn(name = "ID_ACTO_REQ_ACLARACIONES", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idDocumentoRequerimientoAclaraciones;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FiscalizacionBusqueda> busquedas;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FiscalizacionActoAdministrativo> actosAdministrativosFiscalizacion;
    @Column(name = "ES_MAS_ACTUACIONES")
    private Boolean esMasActuaciones;
    @JoinColumn(name = "COD_MECANISMO_CONSEGUIR_INFO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codMecanismoConseguirInformacion;
    @JoinColumn(name = "ID_ACTO_INSPECCION_TRIBUTARIA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actoinspeccionTributaria;
    @JoinColumn(name = "ID_AUTO_ARCHIVO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento autoArchivoParcial;
    @Size(max = 2000)
    @Column(name = "DESC_OBS_CONTINUADO_PROCESO")
    private String descObservacionesContinuadoProceso;
    @Column(name = "FEC_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicio;
    @Column(name = "FEC_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFin;
    @Size(max = 2000)
    @Column(name = "DESC_OBS_MAS_ACTUACIONES")
    private String descObservacionesMasActuaciones;
    @Size(max = 2000)
    @Column(name = "DESC_OBS_MECANISMO_CONSEGUIR")
    private String descObservacionesMecanismoConseguir;
    @Size(max = 2000)
    @Column(name = "VAL_FISCALIZADOR")
    private String fiscalizador;
    @JoinColumn(name = "COD_LINEA_ACCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codLineaAccion;
    @Size(max = 2000)
    @Column(name = "VAL_LINEA_ACCION")
    private String valLineaAccion;
    @JoinColumn(name = "COD_ORIGEN_PROCESO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codOrigenProceso;
    @Size(max = 2000)
    @Column(name = "VAL_ORIGEN_PROCESO")
    private String valOrigenProceso;
    @Size(max = 2000)
    @Column(name = "VAL_NOMBRE_PROGRAMA")
    private String valNombreprograma;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<FiscalizacionAccion> acciones;
    @JoinColumn(name = "COD_ESTADO_FISCALIZACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoFiscalizacion;
    @Size(max = 38)
    @Column(name = "ID_RADICADO_INCONSISTENCIAS")
    private String idRadicadoOficioInconsistencias;
    @Column(name = "FEC_RADICADO_INCONSISTENCIAS")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoOficioInconsistencias;
    @JoinColumn(name = "COD_CAUSAL_CIERRE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codCausalCierre;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<FiscalizacionArchivoTemp> archivosTemporales;
    @Column(name="DESC_GESTION_PERSUASIVA")
    private String descGestionPersuasiva;

    public Fiscalizacion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Denuncia getDenuncia() {
        return denuncia;
    }

    public void setDenuncia(Denuncia denuncia) {
        this.denuncia = denuncia;
    }

    public String getIdLiquidador() {
        return idLiquidador;
    }

    public void setIdLiquidador(String idLiquidador) {
        this.idLiquidador = idLiquidador;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public ActoAdministrativo getActoRequerimientoInformacion() {
        return actoRequerimientoInformacion;
    }

    public void setActoRequerimientoInformacion(ActoAdministrativo actoRequerimientoInformacion) {
        this.actoRequerimientoInformacion = actoRequerimientoInformacion;
    }

    public AprobacionDocumento getRequerimientoDeclararCorregirParcial() {
        return requerimientoDeclararCorregirParcial;
    }

    public void setRequerimientoDeclararCorregirParcial(AprobacionDocumento requerimientoDeclararCorregirParcial) {
        this.requerimientoDeclararCorregirParcial = requerimientoDeclararCorregirParcial;
    }

    public AprobacionDocumento getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    public void setAutoArchivoParcial(AprobacionDocumento autoArchivoParcial) {
        this.autoArchivoParcial = autoArchivoParcial;
    }

    public ActoAdministrativo getActoAutoArchivo() {
        return actoAutoArchivo;
    }

    public void setActoAutoArchivo(ActoAdministrativo actoAutoArchivo) {
        this.actoAutoArchivo = actoAutoArchivo;
    }

    public Boolean getEsEnvioComite() {
        return esEnvioComite;
    }

    public void setEsEnvioComite(Boolean esEnvioComite) {
        this.esEnvioComite = esEnvioComite;
    }

    public Boolean getEsMenorCuantia() {
        return esMenorCuantia;
    }

    public void setEsMenorCuantia(Boolean esMenorCuantia) {
        this.esMenorCuantia = esMenorCuantia;
    }

    public Boolean getEsContinuadoProceso() {
        return esContinuadoProceso;
    }

    public void setEsContinuadoProceso(Boolean esContinuadoProceso) {
        this.esContinuadoProceso = esContinuadoProceso;
    }

    public Boolean getEsOmisoPension() {
        return esOmisoPension;
    }

    public void setEsOmisoPension(Boolean esOmisoPension) {
        this.esOmisoPension = esOmisoPension;
    }

    public List<FiscalizacionIncumplimiento> getCodIncumplimiento() {
        if (codIncumplimiento == null) {
            codIncumplimiento = new ArrayList<FiscalizacionIncumplimiento>();
        }
        return codIncumplimiento;
    }

    public ActoAdministrativo getActorequerimientoDeclararCorregir() {
        return actorequerimientoDeclararCorregir;
    }

    public void setActorequerimientoDeclararCorregir(ActoAdministrativo actorequerimientoDeclararCorregir) {
        this.actorequerimientoDeclararCorregir = actorequerimientoDeclararCorregir;
    }

    public List<FiscalizacionBusqueda> getBusquedas() {
        if (busquedas == null) {
            busquedas = new ArrayList<FiscalizacionBusqueda>();
        }
        return busquedas;
    }

    public Boolean getEsMasActuaciones() {
        return esMasActuaciones;
    }

    public void setEsMasActuaciones(Boolean esMasActuaciones) {
        this.esMasActuaciones = esMasActuaciones;
    }

    public ActoAdministrativo getIdDocumentoRequerimientoAclaraciones() {
        return idDocumentoRequerimientoAclaraciones;
    }

    public void setIdDocumentoRequerimientoAclaraciones(ActoAdministrativo idDocumentoRequerimientoAclaraciones) {
        this.idDocumentoRequerimientoAclaraciones = idDocumentoRequerimientoAclaraciones;
    }

    public ValorDominio getCodMecanismoConseguirInformacion() {
        return codMecanismoConseguirInformacion;
    }

    public void setCodMecanismoConseguirInformacion(ValorDominio codMecanismoConseguirInformacion) {
        this.codMecanismoConseguirInformacion = codMecanismoConseguirInformacion;
    }

    public ActoAdministrativo getActoinspeccionTributaria() {
        return actoinspeccionTributaria;
    }

    public void setActoinspeccionTributaria(ActoAdministrativo actoinspeccionTributaria) {
        this.actoinspeccionTributaria = actoinspeccionTributaria;
    }

    public String getDescObservacionesContinuadoProceso() {
        return descObservacionesContinuadoProceso;
    }

    public void setDescObservacionesContinuadoProceso(String descObservacionesContinuadoProceso) {
        this.descObservacionesContinuadoProceso = descObservacionesContinuadoProceso;
    }

    public Calendar getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Calendar fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Calendar getFecFin() {
        return fecFin;
    }

    public void setFecFin(Calendar fecFin) {
        this.fecFin = fecFin;
    }

    public String getDescObservacionesMasActuaciones() {
        return descObservacionesMasActuaciones;
    }

    public void setDescObservacionesMasActuaciones(String descObservacionesMasActuaciones) {
        this.descObservacionesMasActuaciones = descObservacionesMasActuaciones;
    }

    public String getDescObservacionesMecanismoConseguir() {
        return descObservacionesMecanismoConseguir;
    }

    public void setDescObservacionesMecanismoConseguir(String descObservacionesMecanismoConseguir) {
        this.descObservacionesMecanismoConseguir = descObservacionesMecanismoConseguir;
    }

    public String getFiscalizador() {
        return fiscalizador;
    }

    public void setFiscalizador(String fiscalizador) {
        this.fiscalizador = fiscalizador;
    }

    public ValorDominio getCodLineaAccion() {
        return codLineaAccion;
    }

    public void setCodLineaAccion(ValorDominio codLineaAccion) {
        this.codLineaAccion = codLineaAccion;
    }

    public String getValLineaAccion() {
        return valLineaAccion;
    }

    public void setValLineaAccion(String valLineaAccion) {
        this.valLineaAccion = valLineaAccion;
    }

    public ValorDominio getCodOrigenProceso() {
        return codOrigenProceso;
    }

    public void setCodOrigenProceso(ValorDominio codOrigenProceso) {
        this.codOrigenProceso = codOrigenProceso;
    }

    public String getValOrigenProceso() {
        return valOrigenProceso;
    }

    public void setValOrigenProceso(String valOrigenProceso) {
        this.valOrigenProceso = valOrigenProceso;
    }

    public String getValNombreprograma() {
        return valNombreprograma;
    }

    public void setValNombreprograma(String valNombreprograma) {
        this.valNombreprograma = valNombreprograma;
    }

    public List<FiscalizacionAccion> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<FiscalizacionAccion>();
        }
        return acciones;
    }

    public ValorDominio getCodEstadoFiscalizacion() {
        return codEstadoFiscalizacion;
    }

    public void setCodEstadoFiscalizacion(ValorDominio codEstadoFiscalizacion) {
        this.codEstadoFiscalizacion = codEstadoFiscalizacion;
    }

    public String getIdRadicadoOficioInconsistencias() {
        return idRadicadoOficioInconsistencias;
    }

    public void setIdRadicadoOficioInconsistencias(String idRadicadoOficioInconsistencias) {
        this.idRadicadoOficioInconsistencias = idRadicadoOficioInconsistencias;
    }

    public Calendar getFecRadicadoOficioInconsistencias() {
        return fecRadicadoOficioInconsistencias;
    }

    public void setFecRadicadoOficioInconsistencias(Calendar fecRadicadoOficioInconsistencias) {
        this.fecRadicadoOficioInconsistencias = fecRadicadoOficioInconsistencias;
    }

    public List<FiscalizacionActoAdministrativo> getActosAdministrativosFiscalizacion() {
        if (actosAdministrativosFiscalizacion == null) {
            actosAdministrativosFiscalizacion = new ArrayList<FiscalizacionActoAdministrativo>();
        }
        return actosAdministrativosFiscalizacion;
    }

    public ActoAdministrativo getActoAutoComisorio() {
        return actoAutoComisorio;
    }

    public void setActoAutoComisorio(ActoAdministrativo actoAutoComisorio) {
        this.actoAutoComisorio = actoAutoComisorio;
    }

    public ValorDominio getCodCausalCierre() {
        return codCausalCierre;
    }

    public void setCodCausalCierre(ValorDominio codCausalCierre) {
        this.codCausalCierre = codCausalCierre;
    }

    public List<FiscalizacionArchivoTemp> getArchivosTemporales() {
         if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<FiscalizacionArchivoTemp>();
        }
        return archivosTemporales;
    }

    public void setArchivosTemporales(List<FiscalizacionArchivoTemp> archivosTemporales) {
        this.archivosTemporales = archivosTemporales;
    }

    public String getDescGestionPersuasiva() {
        return descGestionPersuasiva;
    }

    public void setDescGestionPersuasiva(String descGestionPersuasiva) {
        this.descGestionPersuasiva = descGestionPersuasiva;
    }
    
    

    
    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Fiscalizacion[ id=" + id + " ]";
    }
}
