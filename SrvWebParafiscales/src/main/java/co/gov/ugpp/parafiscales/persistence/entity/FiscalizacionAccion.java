package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "FISCALIZACION_ACCION")

@NamedQueries({
    @NamedQuery(name = "fiscalizacionhasaccion.findByFiscalizacionAndAccion",
            query = "SELECT fsa FROM FiscalizacionAccion fsa WHERE fsa.accion.id = :idAccion AND fsa.fiscalizacion.id = :idFiscalizacion")
})

public class FiscalizacionAccion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "fiscalHasAccionIdSeq", sequenceName = "fiscaliz_has_acciones_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscalHasAccionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Fiscalizacion fiscalizacion;
    @JoinColumn(name = "ID_ACCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Accion accion;

    public FiscalizacionAccion() {
    }

    public FiscalizacionAccion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Fiscalizacion getFiscalizacion() {
        return fiscalizacion;
    }

    public void setFiscalizacion(Fiscalizacion fiscalizacion) {
        this.fiscalizacion = fiscalizacion;
    }

    public Accion getAccion() {
        return accion;
    }

    public void setAccion(Accion accion) {
        this.accion = accion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FiscalizacionAccion)) {
            return false;
        }
        FiscalizacionAccion other = (FiscalizacionAccion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.FiscalizacionHasAccion[ id=" + id + " ]";
    }

}
