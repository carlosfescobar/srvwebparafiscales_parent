package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cpatingo
 */
@Entity
@Table(name = "FISCALIZA_ACTO_ADMINISTRATIVO")
@NamedQueries({
    @NamedQuery(name = "FiscalizacionHasActoAdmi.findByFiscalizacionAndActoAdminitrativo",
            query = "SELECT sfi FROM FiscalizacionActoAdministrativo sfi WHERE sfi.actoAdministrativo.id = :actoAdministrativo AND sfi.fiscalizacion.id = :idFiscalizacion")
}
)
public class FiscalizacionActoAdministrativo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "fiscalizacionHasActoAdmiIdSeq", sequenceName = "fiscali_acto_admi_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscalizacionHasActoAdmiIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Fiscalizacion fiscalizacion;
    @JoinColumn(name = "ID_ACTO_ADMIN", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActoAdministrativo actoAdministrativo;

    public FiscalizacionActoAdministrativo() {
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Fiscalizacion getFiscalizacion() {
        return fiscalizacion;
    }

    public void setFiscalizacion(Fiscalizacion fiscalizacion) {
        this.fiscalizacion = fiscalizacion;
    }

    public ActoAdministrativo getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(ActoAdministrativo actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FiscalizacionActoAdministrativo)) {
            return false;
        }
        FiscalizacionActoAdministrativo other = (FiscalizacionActoAdministrativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
