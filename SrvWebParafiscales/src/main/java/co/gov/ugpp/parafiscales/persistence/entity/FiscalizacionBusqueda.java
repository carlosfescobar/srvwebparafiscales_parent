package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "FISCALIZACION_BUSQUEDA")

@NamedQueries({
    @NamedQuery(name = "fiscalizacionhasbusqueda.findByFiscalizacionAndBusqueda",
            query = "SELECT fsb FROM FiscalizacionBusqueda fsb WHERE fsb.busqueda.id = :idbusqueda AND fsb.fiscalizacion.id = :idFiscalizacion")
})
public class FiscalizacionBusqueda extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "fiscalHasBusquedaIdSeq", sequenceName = "fis_has_busq_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscalHasBusquedaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Fiscalizacion fiscalizacion;
    @JoinColumn(name = "ID_BUSQUEDA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Busqueda busqueda;

    public FiscalizacionBusqueda() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fiscalizacion getFiscalizacion() {
        return fiscalizacion;
    }

    public void setFiscalizacion(Fiscalizacion fiscalizacion) {
        this.fiscalizacion = fiscalizacion;
    }

    public Busqueda getBusqueda() {
        return busqueda;
    }

    public void setBusqueda(Busqueda busqueda) {
        this.busqueda = busqueda;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FiscalizacionBusqueda)) {
            return false;
        }
        FiscalizacionBusqueda other = (FiscalizacionBusqueda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.FiscalizacionHasBusqueda[ id=" + id + " ]";
    }

}
