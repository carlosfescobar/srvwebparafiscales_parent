package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "FISCALIZACION_INCUMPLIMIENTO")
@NamedQueries({
    @NamedQuery(name = "fiscalizahasincumplimto.findByFiscalizacionAndCodIncumplimiento",
            query = "SELECT fsi FROM FiscalizacionIncumplimiento fsi WHERE fsi.codIncumplimiento.id = :idcodIncumplimiento AND fsi.fiscalizacion.id = :idFiscalizacion")
})
public class FiscalizacionIncumplimiento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "fiscalhasincumplimientoIdSeq", sequenceName = "fiscal_has_incumpli_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "fiscalhasincumplimientoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Fiscalizacion fiscalizacion;
    @JoinColumn(name = "COD_INCUMPLIMIENTO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codIncumplimiento;

    public FiscalizacionIncumplimiento() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fiscalizacion getFiscalizacion() {
        return fiscalizacion;
    }

    public void setFiscalizacion(Fiscalizacion fiscalizacion) {
        this.fiscalizacion = fiscalizacion;
    }

    public ValorDominio getCodIncumplimiento() {
        return codIncumplimiento;
    }

    public void setCodIncumplimiento(ValorDominio codIncumplimiento) {
        this.codIncumplimiento = codIncumplimiento;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FiscalizacionIncumplimiento)) {
            return false;
        }
        FiscalizacionIncumplimiento other = (FiscalizacionIncumplimiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionHasIncumplimiento[ id=" + id + " ]";
    }

}
