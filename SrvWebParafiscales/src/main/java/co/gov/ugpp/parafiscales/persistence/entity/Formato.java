package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "formato.findByFormatoAndVersion",
            query = "SELECT f FROM Formato f WHERE f.formatoPK.idFormato = :idFormato AND f.formatoPK.idVersion = :idVersion")})
@Table(name = "FORMATO")
public class Formato extends AbstractEntity<FormatoPK> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FormatoPK formatoPK;
    @Column(name = "FEC_INICIO_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioVigencia;
    @Column(name = "FEC_FIN_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinVigencia;
    @Size(max = 100)
    @Column(name = "VAL_USUARIO")
    private String valUsuario;
    @Size(max = 2000)
    @Column(name = "DESC_DESCRIPCION")
    private String descripcion;
    @JoinColumn(name = "COD_AREA_NEGOCIO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codAreaNegocio;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Archivo archivo;

    public Formato() {
    }

    @Override
    public FormatoPK getId() {
        return formatoPK;
    }

    public void setId(FormatoPK formatoPK) {
        this.formatoPK = formatoPK;
    }

    public Calendar getFecInicioVigencia() {
        return fecInicioVigencia;
    }

    public void setFecInicioVigencia(Calendar fecInicioVigencia) {
        this.fecInicioVigencia = fecInicioVigencia;
    }

    public Calendar getFecFinVigencia() {
        return fecFinVigencia;
    }

    public void setFecFinVigencia(Calendar fecFinVigencia) {
        this.fecFinVigencia = fecFinVigencia;
    }

    public String getValUsuario() {
        return valUsuario;
    }

    public void setValUsuario(String valUsuario) {
        this.valUsuario = valUsuario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ValorDominio getCodAreaNegocio() {
        return codAreaNegocio;
    }

    public void setCodAreaNegocio(ValorDominio codAreaNegocio) {
        this.codAreaNegocio = codAreaNegocio;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formatoPK != null ? formatoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formato)) {
            return false;
        }
        Formato other = (Formato) object;
        if ((this.formatoPK == null && other.formatoPK != null) || (this.formatoPK != null && !this.formatoPK.equals(other.formatoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Formato[ id=" + formatoPK + " ]";
    }

}
