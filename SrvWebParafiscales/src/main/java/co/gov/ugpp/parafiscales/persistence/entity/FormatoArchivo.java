package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "FORMATO_ARCHIVO")
public class FormatoArchivo extends AbstractEntity<FormatoArchivoPK> {

    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private FormatoArchivoPK formatoArchivoPK;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Column(name = "VAL_CONTENIDO_ARCHVO")
    private byte[] valContenidoArchvo;
    @JoinColumn(name = "COD_TIPO_ARCHIVO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio idTipoArchivo;

    public FormatoArchivo() {
    }

    @Override
    public FormatoArchivoPK getId() {
        return formatoArchivoPK;
    }

    public void setId(FormatoArchivoPK formatoArchivoPK) {
        this.formatoArchivoPK = formatoArchivoPK;
    }

    public byte[] getValContenidoArchvo() {
        return valContenidoArchvo;
    }

    public void setValContenidoArchvo(byte[] valContenidoArchvo) {
        this.valContenidoArchvo = valContenidoArchvo;
    }

    public ValorDominio getIdTipoArchivo() {
        return idTipoArchivo;
    }

    public void setIdTipoArchivo(ValorDominio idTipoArchivo) {
        this.idTipoArchivo = idTipoArchivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (formatoArchivoPK != null ? formatoArchivoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormatoArchivo)) {
            return false;
        }
        FormatoArchivo other = (FormatoArchivo) object;
        if ((this.formatoArchivoPK == null && other.formatoArchivoPK != null) || (this.formatoArchivoPK != null && !this.formatoArchivoPK.equals(other.formatoArchivoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.FormatoArchivo[ id=" + formatoArchivoPK + " ]";
    }

}
