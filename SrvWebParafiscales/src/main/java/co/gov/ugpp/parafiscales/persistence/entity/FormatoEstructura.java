package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 *         jsaenzar
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "formatoEstructura.findByFormatoPK",
            query = "SELECT f FROM FormatoEstructura f WHERE f.formato.formatoPK.idFormato = :idFormato AND f.formato.formatoPK.idVersion = :idVersion")})
@Table(name = "FORMATO_ESTRUCTURA")
public class FormatoEstructura extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "formatoEstructuraIdSeq", sequenceName = "formato_estructura_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "formatoEstructuraIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Archivo archivo;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION", referencedColumnName = "ID_VERSION")
    })
    private Formato formato;

    public FormatoEstructura() {
    }

    public FormatoEstructura(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FormatoEstructura)) {
            return false;
        }
        FormatoEstructura other = (FormatoEstructura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FormatoEstructura{" + "id=" + id + '}';
    }

}
