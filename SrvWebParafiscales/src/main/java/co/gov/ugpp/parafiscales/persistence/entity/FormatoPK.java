package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Embeddable
public class FormatoPK implements Serializable {

    private static final long serialVersionUID = 1L;

    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_FORMATO")
    private Long idFormato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_VERSION")
    private Long idVersion;

    public FormatoPK() {
    }

    public FormatoPK(Long idFormato, Long idVersion) {
        this.idFormato = idFormato;
        this.idVersion = idVersion;
    }

    public Long getIdFormato() {
        return idFormato;
    }

    public void setIdFormato(Long idFormato) {
        this.idFormato = idFormato;
    }

    public Long getIdVersion() {
        return idVersion;
    }

    public void setIdVersion(Long idVersion) {
        this.idVersion = idVersion;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime
                * result
                + ((idFormato == null) ? 0 : idFormato
                .hashCode());
        result = prime * result
                + ((idFormato == null) ? 0 : idFormato.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FormatoPK other = (FormatoPK) obj;
        if (idFormato == null) {
            if (other.idFormato != null) {
                return false;
            }
        } else if (!idFormato.equals(other.idFormato)) {
            return false;
        }
        if (idFormato == null) {
            if (other.idFormato != null) {
                return false;
            }
        } else if (!idFormato.equals(other.idFormato)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FormatoPK{" + "idFormato=" + idFormato + ", idVersion=" + idVersion + '}';
    }
}
