package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqformula", initialValue = 1, allocationSize = 1)
@Table(name = "FORMULA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Formula.findAll", query = "SELECT f FROM Formula f"),
    @NamedQuery(name = "Formula.findById", query = "SELECT f FROM Formula f WHERE f.id = :id"),
    @NamedQuery(name = "Formula.findByNombreFormula", query = "SELECT f FROM Formula f WHERE f.nombreFormula = :nombreFormula"),
    @NamedQuery(name = "Formula.findByDescripcionFormula", query = "SELECT f FROM Formula f WHERE f.descripcionFormula = :descripcionFormula"),
    @NamedQuery(name = "Formula.findByFechaInicial", query = "SELECT f FROM Formula f WHERE f.fechaInicial = :fechaInicial"),
    @NamedQuery(name = "Formula.findByFechaFinal", query = "SELECT f FROM Formula f WHERE f.fechaFinal = :fechaFinal"),
    @NamedQuery(name = "Formula.findByEstado", query = "SELECT f FROM Formula f WHERE f.estado = :estado"),
    @NamedQuery(name = "Formula.findByFormula", query = "SELECT f FROM Formula f WHERE f.formula = :formula"),
    @NamedQuery(name = "Formula.findByAliasFormula", query = "SELECT f FROM Formula f WHERE f.aliasFormula = :aliasFormula")})
public class Formula extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqformula")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 250)
    @Column(name = "NOMBRE_FORMULA")
    private String nombreFormula;
    @Size(max = 700)
    @Column(name = "DESCRIPCION_FORMULA", length = 700)
    private String descripcionFormula;
    @Column(name = "FECHA_INICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicial;
    @Column(name = "FECHA_FINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinal;
    @Column(name = "ESTADO")
    private BigInteger estado;
    @Size(max = 1500)
    @Column(name = "FORMULA", length = 1500)
    private String formula;
    @Size(max = 100)
    @Column(name = "ALIAS_FORMULA")
    private String aliasFormula;
    @OneToMany(mappedBy = "formula", fetch = FetchType.LAZY)
    private List<AplicacionFormula> aplicacionFormulaList;

    public Formula() {
    }

    public Formula(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreFormula() {
        return nombreFormula;
    }

    public void setNombreFormula(String nombreFormula) {
        this.nombreFormula = nombreFormula;
    }

    public String getDescripcionFormula() {
        return descripcionFormula;
    }

    public void setDescripcionFormula(String descripcionFormula) {
        this.descripcionFormula = descripcionFormula;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public BigInteger getEstado() {
        return estado;
    }

    public void setEstado(BigInteger estado) {
        this.estado = estado;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getAliasFormula() {
        return aliasFormula;
    }

    public void setAliasFormula(String aliasFormula) {
        this.aliasFormula = aliasFormula;
    }

    @XmlTransient
    public List<AplicacionFormula> getAplicacionFormulaList() {
        return aplicacionFormulaList;
    }

    public void setAplicacionFormulaList(List<AplicacionFormula> aplicacionFormulaList) {
        this.aplicacionFormulaList = aplicacionFormulaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Formula)) {
            return false;
        }
        Formula other = (Formula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.Formula[ id=" + id + " ]";
    }

}
