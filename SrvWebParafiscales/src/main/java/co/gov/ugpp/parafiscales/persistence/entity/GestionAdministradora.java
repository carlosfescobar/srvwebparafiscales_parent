package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "GESTION_ADMINISTRADORA")
public class GestionAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "gestionAdministradoraIdSeq", sequenceName = "gestion_administradora_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestionAdministradoraIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_SUBSISTEMA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Subsistema subsistema;
    @JoinColumn(name = "ID_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private EntidadExterna administradora;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "COD_TIPO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoSolicitud;
    @JoinColumn(name = "COD_TIPO_RESPUESTA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoRespuesta;
    @JoinColumn(name = "COD_TIPO_PERIODICIDAD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPeriodicidad;
    @JoinColumn(name = "COD_TIPO_PLAZO_ENTREGA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPlazoEntrega;
    @Column(name = "VAL_CANTIDAD_PLAZO_ENTREGA")
    private String valCantidadPlazoEntrega;
    @Column(name = "VAL_NOMBRE_SOLICITUD")
    private String valNombreSolicitud;
    @Column(name = "DESC_INFO_REQUERIDA")
    private String descInformacionRequerida;
    @Column(name = "FEC_INICIO_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioSolicitud;
    @Column(name = "FEC_FIN_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinSolicitud;
    @Column(name = "FEC_PROXIMA_ESPERA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecProximaEspera;
    @JoinColumn(name = "COD_ESTADO_GESTION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoGestion;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO_ELEGIDO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION_ELEGIDO", referencedColumnName = "ID_VERSION")
    })
    private Formato formatoElegido;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<GestionAdministradoraDocumento> idDocumentoSolicitud;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<GestionAdministradoraAccion> acciones;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<GestionAdministradoraControlUbicacion> controlUbicaciones;
    @Column(name = "ID_FUNCIONARIO_SOLICITUD")
    private String idFuncionarioSolicitud;

    public GestionAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subsistema getSubsistema() {
        return subsistema;
    }

    public void setSubsistema(Subsistema subsistema) {
        this.subsistema = subsistema;
    }

    public EntidadExterna getAdministradora() {
        return administradora;
    }

    public void setAdministradora(EntidadExterna administradora) {
        this.administradora = administradora;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public ValorDominio getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    public void setCodTipoSolicitud(ValorDominio codTipoSolicitud) {
        this.codTipoSolicitud = codTipoSolicitud;
    }

    public ValorDominio getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    public void setCodTipoRespuesta(ValorDominio codTipoRespuesta) {
        this.codTipoRespuesta = codTipoRespuesta;
    }

    public ValorDominio getCodTipoPeriodicidad() {
        return codTipoPeriodicidad;
    }

    public void setCodTipoPeriodicidad(ValorDominio codTipoPeriodicidad) {
        this.codTipoPeriodicidad = codTipoPeriodicidad;
    }

    public ValorDominio getCodTipoPlazoEntrega() {
        return codTipoPlazoEntrega;
    }

    public void setCodTipoPlazoEntrega(ValorDominio codTipoPlazoEntrega) {
        this.codTipoPlazoEntrega = codTipoPlazoEntrega;
    }

    public String getValCantidadPlazoEntrega() {
        return valCantidadPlazoEntrega;
    }

    public void setValCantidadPlazoEntrega(String valCantidadPlazoEntrega) {
        this.valCantidadPlazoEntrega = valCantidadPlazoEntrega;
    }

    public String getValNombreSolicitud() {
        return valNombreSolicitud;
    }

    public void setValNombreSolicitud(String valNombreSolicitud) {
        this.valNombreSolicitud = valNombreSolicitud;
    }

    public String getDescInformacionRequerida() {
        return descInformacionRequerida;
    }

    public void setDescInformacionRequerida(String descInformacionRequerida) {
        this.descInformacionRequerida = descInformacionRequerida;
    }

    public Calendar getFecInicioSolicitud() {
        return fecInicioSolicitud;
    }

    public void setFecInicioSolicitud(Calendar fecInicioSolicitud) {
        this.fecInicioSolicitud = fecInicioSolicitud;
    }

    public Calendar getFecFinSolicitud() {
        return fecFinSolicitud;
    }

    public void setFecFinSolicitud(Calendar fecFinSolicitud) {
        this.fecFinSolicitud = fecFinSolicitud;
    }

    public Calendar getFecProximaEspera() {
        return fecProximaEspera;
    }

    public void setFecProximaEspera(Calendar fecProximaEspera) {
        this.fecProximaEspera = fecProximaEspera;
    }

    public ValorDominio getCodEstadoGestion() {
        return codEstadoGestion;
    }

    public void setCodEstadoGestion(ValorDominio codEstadoGestion) {
        this.codEstadoGestion = codEstadoGestion;
    }

    public Formato getFormatoElegido() {
        return formatoElegido;
    }

    public void setFormatoEelegido(Formato formatoElegido) {
        this.formatoElegido = formatoElegido;
    }

    public List<GestionAdministradoraDocumento> getIdDocumentoSolicitud() {
        if (idDocumentoSolicitud == null) {
            idDocumentoSolicitud = new ArrayList<GestionAdministradoraDocumento>();
        }
        return idDocumentoSolicitud;
    }

    public List<GestionAdministradoraAccion> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<GestionAdministradoraAccion>();
        }
        return acciones;
    }

    public List<GestionAdministradoraControlUbicacion> getControlUbicaciones() {
        if (controlUbicaciones == null) {
            controlUbicaciones = new ArrayList<GestionAdministradoraControlUbicacion>();
        }
        return controlUbicaciones;
    }

    public String getIdFuncionarioSolicitud() {
        return idFuncionarioSolicitud;
    }

    public void setIdFuncionarioSolicitud(String idFuncionarioSolicitud) {
        this.idFuncionarioSolicitud = idFuncionarioSolicitud;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.GestionAdministradora[ id=" + id + " ]";
    }
}
