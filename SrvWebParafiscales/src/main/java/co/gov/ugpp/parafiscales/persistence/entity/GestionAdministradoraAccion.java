package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "GESTION_ADMINISTRADORA_ACCION")
@NamedQueries({
    @NamedQuery(name = "gestionAdministradoraAccion.findByGestionAdministradoraAndAccion",
            query = "SELECT gaa FROM GestionAdministradoraAccion gaa WHERE gaa.gestionAdministradora.id= :idGestionAdministradora AND gaa.accion.id= :idAccion")
})
public class GestionAdministradoraAccion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "gestionAdministradoraDocumentoIdSeq", sequenceName = "gestion_admin_documen_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestionAdministradoraDocumentoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private GestionAdministradora gestionAdministradora;
    @JoinColumn(name = "ID_ACCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Accion accion;

    public GestionAdministradoraAccion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GestionAdministradora getGestionAdministradora() {
        return gestionAdministradora;
    }

    public void setGestionAdministradora(GestionAdministradora gestionAdministradora) {
        this.gestionAdministradora = gestionAdministradora;
    }

    public Accion getAccion() {
        return accion;
    }

    public void setAccion(Accion accion) {
        this.accion = accion;
    }

}
