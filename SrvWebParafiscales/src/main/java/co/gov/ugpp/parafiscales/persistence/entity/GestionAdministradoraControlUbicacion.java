package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "GESTION_ADM_CONTROL_UBICACION")
@NamedQueries({
    @NamedQuery(name = "gestionAdministradoraControlUbicacion.findByEstadoUbicacion",
            query = "SELECT gac FROM GestionAdministradoraControlUbicacion gac WHERE gac.controlUbicacionEnvio.codEstadoUbicacion.id= :estado AND gac.gestionAdministradora.id= :idGestionAdministradora"),
    @NamedQuery(name = "gestionAdministradoraControlUbicacion.findByGestionAdministradoraAndControlUbicacion",
            query = "SELECT gac FROM GestionAdministradoraControlUbicacion gac WHERE gac.gestionAdministradora.id= :idGestionAdministradora AND gac.controlUbicacionEnvio.id= :idControlUbicacionEnvio")
})
public class GestionAdministradoraControlUbicacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "gestionAdministradoraDocumentoIdSeq", sequenceName = "gestion_admin_documen_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestionAdministradoraDocumentoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private GestionAdministradora gestionAdministradora;
    @JoinColumn(name = "ID_CONTROL_UBICACION_ENVIO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlUbicacionEnvio controlUbicacionEnvio;

    public GestionAdministradoraControlUbicacion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GestionAdministradora getGestionAdministradora() {
        return gestionAdministradora;
    }

    public void setGestionAdministradora(GestionAdministradora gestionAdministradora) {
        this.gestionAdministradora = gestionAdministradora;
    }

    public ControlUbicacionEnvio getControlUbicacionEnvio() {
        return controlUbicacionEnvio;
    }

    public void setControlUbicacionEnvio(ControlUbicacionEnvio controlUbicacionEnvio) {
        this.controlUbicacionEnvio = controlUbicacionEnvio;
    }

    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GestionAdministradoraControlUbicacion)) {
            return false;
        }
        GestionAdministradoraControlUbicacion other = (GestionAdministradoraControlUbicacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.GestionAdministradoraControlUbicacion[ id=" + id + " ]";
    }
}
