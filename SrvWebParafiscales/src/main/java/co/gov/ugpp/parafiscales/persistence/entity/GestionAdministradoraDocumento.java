package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "GESTION_ADMINISTRA_EXP_DOC_ECM")
@NamedQueries({
    @NamedQuery(name = "gestionAdministradoraDocumento.findByGestionAdministradoraAndDocumento",
            query = "SELECT gad FROM GestionAdministradoraDocumento gad WHERE gad.gestionAdministradora.id= :idGestionAdministradora AND gad.expedienteDocumentoEcm.documentoEcm.id = :idDocumentoEcm")
})
public class GestionAdministradoraDocumento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "gestionAdministradoraDocumentoIdSeq", sequenceName = "gestion_admin_documen_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestionAdministradoraDocumentoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private GestionAdministradora gestionAdministradora;
    @JoinColumn(name = "ID_EXPEDIENTE_DOC_ECM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ExpedienteDocumentoEcm expedienteDocumentoEcm;

    public GestionAdministradoraDocumento() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GestionAdministradora getGestionAdministradora() {
        return gestionAdministradora;
    }

    public void setGestionAdministradora(GestionAdministradora gestionAdministradora) {
        this.gestionAdministradora = gestionAdministradora;
    }

    public ExpedienteDocumentoEcm getExpedienteDocumentoEcm() {
        return expedienteDocumentoEcm;
    }

    public void setExpedienteDocumentoEcm(ExpedienteDocumentoEcm expedienteDocumentoEcm) {
        this.expedienteDocumentoEcm = expedienteDocumentoEcm;
    }

}
