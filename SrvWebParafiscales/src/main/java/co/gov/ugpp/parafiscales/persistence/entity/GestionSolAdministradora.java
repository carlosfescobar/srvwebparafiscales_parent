package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Everis
 */
@Entity
@Table(name = "GESTION_SOL_ADMINISTRADORA")
@NamedQueries({
    @NamedQuery(name = "gestionSolAdministradora.findByGestionAdministradoraAndSolicitudAdmin",
            query = "SELECT gsa FROM GestionSolAdministradora gsa WHERE gsa.solicitudAdministradora.id= :idSolicitudAdministradora AND gsa.gestionAdministradora.id= :idGestionAdministradora")
})
public class GestionSolAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "gestionSolAdministIdSeq", sequenceName = "gestion_sol_administ_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gestionSolAdministIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SolicitudAdministradora solicitudAdministradora;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private GestionAdministradora gestionAdministradora;

    public GestionSolAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
       public SolicitudAdministradora getSolicitudAdministradora() {
        return solicitudAdministradora;
    }

    public void setSolicitudAdministradora(SolicitudAdministradora solicitudAdministradora) {
        this.solicitudAdministradora = solicitudAdministradora;
    }

    public GestionAdministradora getGestionAdministradora() {
        return gestionAdministradora;
    }

    public void setGestionAdministradora(GestionAdministradora gestionAdministradora) {
        this.gestionAdministradora = gestionAdministradora;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GestionSolAdministradora)) {
            return false;
        }
        GestionSolAdministradora other = (GestionSolAdministradora) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.GestionSolAdministradora[ id=" + id + " ]";
    }
}
