package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "LIQ_HOJA_HALLAZGOS")
public class Hallazgo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "hallazgoIdSeq", sequenceName = "hallazgo_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hallazgoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HOJA_HALLAZGOS", updatable = false)
    private Long id;
    @NotNull
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Expediente expediente;
    @Column(name = "ID_NOMINA")
    private Long idNomina;
    @Column(name = "ID_PILA")
    private Long idPila;
    @Column(name = "ID_CONCILIACION")
    private Long idConciliacion;
    @JoinColumn(name = "ID_HOJA_HALLAZGOS", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<HallazgoDetalleNomina> hallazgoDetalleNominaList;
    @JoinColumn(name = "ID_HOJA_HALLAZGOS", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<HallazgoDetalleConciliacionContable> hallazgoDetalleConciliacionContableList;
    @JoinColumn(name = "COD_TIPO_HALLAZGO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codTipoHallazgo;

    public Hallazgo() {
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Long getIdNomina() {
        return idNomina;
    }

    public void setIdNomina(Long idNomina) {
        this.idNomina = idNomina;
    }

    public Long getIdConciliacion() {
        return idConciliacion;
    }

    public void setIdConciliacion(Long idConciliacion) {
        this.idConciliacion = idConciliacion;
    }

    public Long getIdPila() {
        return idPila;
    }

    public void setIdPila(Long idPila) {
        this.idPila = idPila;
    }

    public List<HallazgoDetalleNomina> getHallazgoDetalleNominaList() {
        return hallazgoDetalleNominaList;
    }

    public void setHallazgoDetalleNominaList(List<HallazgoDetalleNomina> hallazgoDetalleNominaList) {
        this.hallazgoDetalleNominaList = hallazgoDetalleNominaList;
    }

    public ValorDominio getCodTipoHallazgo() {
        return codTipoHallazgo;
    }

    public void setCodTipoHallazgo(ValorDominio codTipoHallazgo) {
        this.codTipoHallazgo = codTipoHallazgo;
    }

    public List<HallazgoDetalleConciliacionContable> getHallazgoDetalleConciliacionContableList() {
        return hallazgoDetalleConciliacionContableList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Hallazgo)) {
            return false;
        }
        Hallazgo other = (Hallazgo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Efectividad[ id=" + id + " ]";
    }

}
