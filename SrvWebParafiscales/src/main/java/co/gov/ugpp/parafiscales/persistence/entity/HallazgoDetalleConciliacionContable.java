package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "LIQ_HALLAZGO_DET_CONC_CONTABLE")
public class HallazgoDetalleConciliacionContable extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "hallazgoDetConcContableIdSeq", sequenceName = "hallazgo_det_con_cont_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hallazgoDetConcContableIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HALLAZGO", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_HOJA_HALLAZGO", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Hallazgo hallazgo;
    @Column(name = "ES_HALLAZGO")
    private Boolean esHallazgo;
    @Column(name = "OBS_HALLAZGO")
    private String observacionHallazgo;
    @Column(name = "DESC_HALLAZGO")
    private String descHallazgo;
    @Column(name = "CONCEPTO_CONTABLE")
    private String conceptoContable;
    @Column(name = "VALOR_CONTABILIDAD")
    private Long valorContabilidad;
    @Column(name = "VALOR_NOMINA")
    private Long valorNomina;
    @Column(name = "DIFERENCIA_PROCENTAJE")
    private String diferenciaPorcentaje;
    @Column(name = "DIFERENCIA_PESOS")
    private Long diferenciaPesos;

    public HallazgoDetalleConciliacionContable() {
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getConceptoContable() {
        return conceptoContable;
    }

    public void setConceptoContable(String conceptoContable) {
        this.conceptoContable = conceptoContable;
    }

    public String getDiferenciaPorcentaje() {
        return diferenciaPorcentaje;
    }

    public void setDiferenciaPorcentaje(String diferenciaPorcentaje) {
        this.diferenciaPorcentaje = diferenciaPorcentaje;
    }

    public Long getDiferenciaPesos() {
        return diferenciaPesos;
    }

    public void setDiferenciaPesos(Long diferenciaPesos) {
        this.diferenciaPesos = diferenciaPesos;
    }

    public Hallazgo getHallazgo() {
        return hallazgo;
    }

    public void setHallazgo(Hallazgo hallazgo) {
        this.hallazgo = hallazgo;
    }

    public Boolean getEsHallazgo() {
        return esHallazgo;
    }

    public void setEsHallazgo(Boolean esHallazgo) {
        this.esHallazgo = esHallazgo;
    }

    public String getObservacionHallazgo() {
        return observacionHallazgo;
    }

    public void setObservacionHallazgo(String observacionHallazgo) {
        this.observacionHallazgo = observacionHallazgo;
    }

    public String getDescHallazgo() {
        return descHallazgo;
    }

    public void setDescHallazgo(String descHallazgo) {
        this.descHallazgo = descHallazgo;
    }

    public Long getValorContabilidad() {
        return valorContabilidad;
    }

    public void setValorContabilidad(Long valorContabilidad) {
        this.valorContabilidad = valorContabilidad;
    }

    public Long getValorNomina() {
        return valorNomina;
    }

    public void setValorNomina(Long valorNomina) {
        this.valorNomina = valorNomina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HallazgoDetalleConciliacionContable)) {
            return false;
        }
        HallazgoDetalleConciliacionContable other = (HallazgoDetalleConciliacionContable) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Efectividad[ id=" + id + " ]";
    }

}
