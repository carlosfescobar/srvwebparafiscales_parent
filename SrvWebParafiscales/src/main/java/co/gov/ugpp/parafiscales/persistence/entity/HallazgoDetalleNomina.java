package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "LIQ_HALLAZGO_NOMINA_PILA_DET")
public class HallazgoDetalleNomina extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "hallazgoDetPilaNominaIdSeq", sequenceName = "hallazgo_det_nom_pila_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hallazgoDetPilaNominaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_HALLAZGO", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_HOJA_HALLAZGOS", referencedColumnName = "ID_HOJA_HALLAZGOS")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Hallazgo hallazgo;
    @Column(name = "ES_HALLAZGO")
    private Boolean esHallazgo;
    @Column(name = "OBS_HALLAZGO")
    private String observacionHallazgo;
    @Column(name = "DESC_HALLAZGO")
    private String descHallazgo;
    @Column(name = "COD_TIPO_IDENTIFICACION")
    private String codTipoIdentificacion;
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private String valNumeroIdentificacion;
    @Column(name = "VAL_NOMBRE_COTIZANTE")
    private String valNombreCotizante;
    @Column(name = "PERIODO_RESTO")
    private String periodoResto;
    @Column(name = "TIPO_COTIZANTE")
    private Long tipoCotizante;

    public HallazgoDetalleNomina() {
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(String codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public String getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    public void setValNumeroIdentificacion(String valNumeroIdentificacion) {
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    public String getValNombreCotizante() {
        return valNombreCotizante;
    }

    public void setValNombreCotizante(String valNombreCotizante) {
        this.valNombreCotizante = valNombreCotizante;
    }

    public String getPeriodoResto() {
        return periodoResto;
    }

    public void setPeriodoResto(String periodoResto) {
        this.periodoResto = periodoResto;
    }

    public Long getTipoCotizante() {
        return tipoCotizante;
    }

    public void setTipoCotizante(Long tipoCotizante) {
        this.tipoCotizante = tipoCotizante;
    }

    public Hallazgo getHallazgo() {
        return hallazgo;
    }

    public void setHallazgo(Hallazgo hallazgo) {
        this.hallazgo = hallazgo;
    }

    public Boolean getEsHallazgo() {
        return esHallazgo;
    }

    public void setEsHallazgo(Boolean esHallazgo) {
        this.esHallazgo = esHallazgo;
    }

    public String getObservacionHallazgo() {
        return observacionHallazgo;
    }

    public void setObservacionHallazgo(String observacionHallazgo) {
        this.observacionHallazgo = observacionHallazgo;
    }

    public String getDescHallazgo() {
        return descHallazgo;
    }

    public void setDescHallazgo(String descHallazgo) {
        this.descHallazgo = descHallazgo;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HallazgoDetalleNomina)) {
            return false;
        }
        HallazgoDetalleNomina other = (HallazgoDetalleNomina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Efectividad[ id=" + id + " ]";
    }

}
