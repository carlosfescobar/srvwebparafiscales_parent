package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mauricio Guerrero
 */
@Entity
@Table(name = "LIQ_HOJA_CALCULO_LIQUIDACION")
@XmlRootElement
public class HojaCalculoLiquidacion extends AbstractEntity<BigDecimal>{

    @Size(max = 255)
    @Column(name = "CODESTADO")
    private String codesstado;
    @Size(max = 255)
    @Column(name = "DESESTADO")
    private String desestado;
    @Column(name = "ESINCUMPLIMIENTO")
    private Character esincumplimiento;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqhojacalculoliquidador")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDEXPEDIENTE")
    private String idexpediente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idhojacalculoliquidacion", fetch = FetchType.LAZY)
    private Collection<HojaCalculoLiquidacionDetalle> hojaCalculoLiquidacionDetalCollection;

    public HojaCalculoLiquidacion() {
    }

    public HojaCalculoLiquidacion(BigDecimal id) {
        this.id = id;
    }

    public HojaCalculoLiquidacion(BigDecimal id, String idexpediente, Date fechacreacion) {
        this.id = id;
        this.idexpediente = idexpediente;
        this.fechacreacion = fechacreacion;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public String getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(String idexpediente) {
        this.idexpediente = idexpediente;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    @XmlTransient
    public Collection<HojaCalculoLiquidacionDetalle> getHojaCalculoLiquidacionDetalCollection() {
        return hojaCalculoLiquidacionDetalCollection;
    }

    public void setHojaCalculoLiquidacionDetalCollection(Collection<HojaCalculoLiquidacionDetalle> hojaCalculoLiquidacionDetalCollection) {
        this.hojaCalculoLiquidacionDetalCollection = hojaCalculoLiquidacionDetalCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = true;
        if (!(object instanceof HojaCalculoLiquidacion)) {
            result = false;
        }
        HojaCalculoLiquidacion other = (HojaCalculoLiquidacion) object;
        if (this.id == null && other.id != null || this.id != null && !this.id.equals(other.id)) {
            result = false;
        }
        return result;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.entidades.negocio.HojaCalculoLiquidacion[ id=" + id + " ]";
    }

    public String getCodesstado() {
        return codesstado;
    }

    public void setCodesstado(String codesstado) {
        this.codesstado = codesstado;
    }

    public String getDesestado() {
        return desestado;
    }

    public void setDesestado(String desestado) {
        this.desestado = desestado;
    }

    public Character getEsincumplimiento() {
        return esincumplimiento;
    }

    public void setEsincumplimiento(Character esincumplimiento) {
        this.esincumplimiento = esincumplimiento;
    }

}
