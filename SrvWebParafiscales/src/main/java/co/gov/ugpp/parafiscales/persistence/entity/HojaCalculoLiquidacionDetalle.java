package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Mauricio Guerrero
 */
@Entity
@Table(name = "HOJA_CALCULO_LIQUIDACION_DETAL")
@XmlRootElement
public class HojaCalculoLiquidacionDetalle extends AbstractEntity<BigDecimal>{

    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idhojacalculoliquidaciondet", fetch = FetchType.LAZY)
    private Collection<AportesIndependienteHclDetalle> aportesIndependienteHclDetaCollection;
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idhojacalculoliquidaciondet", fetch = FetchType.LAZY)
    private Collection<ConceptoContableHclDetalle> conceptoContableHclDetalleCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqhojacalculoliquidadordetal")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDNOMINADETALLE")
    private BigInteger idnominadetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDPILADETALLE")
    private BigInteger idpiladetalle;
    @Basic(optional = false)
    @NotNull
    @Column(name = "IDCONCILIACIONCONTABLEDETALLE")
    private BigInteger idconciliacioncontabledetalle;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "APORTA_ID")
    private BigInteger aportaId;
    @Column(name = "APORTA_TIPO_IDENTIFICACION")
    private BigInteger aportaTipoIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "APORTA_NUMERO_IDENTIFICACION")
    private String aportaNumeroIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 250)
    @Column(name = "APORTA_PRIMER_NOMBRE")
    private String aportaPrimerNombre;
    @Size(max = 250)
    @Column(name = "APORTA_CLASE")
    private String aportaClase;
    @Size(max = 2)
    @Column(name = "APORTA_APORTE_ESAP_Y_MEN")
    private String aportaAporteEsapYMen;
    @Size(max = 2)
    @Column(name = "APORTA_EXCEPCION_LEY_1233_2008")
    private String aportaExcepcionLey12332008;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COTIZ_ID")
    private BigInteger cotizId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COTIZ_TIPO_DOCUMENTO")
    private BigInteger cotizTipoDocumento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "COTIZ_NUMERO_IDENTIFICACION")
    private String cotizNumeroIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "COTIZ_NOMBRE")
    private String cotizNombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COTIZ_TIPO_COTIZANTE")
    private BigInteger cotizTipoCotizante;
    @Basic(optional = false)
    @NotNull
    @Column(name = "COTIZ_SUBTIPO_COTIZANTE")
    private BigInteger cotizSubtipoCotizante;
    @Basic(optional = false)
    @Size(min = 1, max = 2)
    @Column(name = "COTIZ_EXTRANJERO_NO_COTIZAR")
    private String cotizExtranjeroNoCotizar;
    @Basic(optional = false)
    @Size(min = 1, max = 2)
    @Column(name = "COTIZ_COLOMBIANO_EN_EL_EXT")
    private String cotizColombianoEnElExt;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "COTIZ_ACTIVIDAD_ALTO_RIESGO_PE")
    private String cotizActividadAltoRiesgoPe;
    @Column(name = "COTIZD_ANO")
    private BigInteger cotizdAno;
    @Column(name = "COTIZD_MES")
    private BigInteger cotizdMes;
    @Size(max = 1)
    @Column(name = "COTIZD_ING")
    private String cotizdIng;
    @Column(name = "COTIZD_ING_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdIngFecha;
    @Size(max = 1)
    @Column(name = "COTIZD_RET")
    private String cotizdRet;
    @Column(name = "COTIZD_RET_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdRetFecha;
    @Size(max = 255)
    @Column(name = "COTIZD_SLN")
    private String cotizdSln;
    @Size(max = 1)
    @Column(name = "COTIZD_CAUSAL_SUSPENSION")
    private String cotizdCausalSuspension;
    @Column(name = "COTIZD_CAUSAL_SUSP_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdCausalSuspFinicial;
    @Column(name = "COTIZD_CAUSAL_SUSP_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdCausalSuspFfinal;
    @Column(name = "DIAS_SUSPENSION")
    private BigInteger diasSuspension;
    @Size(max = 1)
    @Column(name = "COTIZD_IGE")
    private String cotizdIge;
    @Column(name = "COTIZD_IGE_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdIgeFinicial;
    @Column(name = "COTIZD_IGE_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdIgeFfinal;
    @Column(name = "DIAS_INCAPACIDAD_GENERAL")
    private BigInteger diasIncapacidadGeneral;
    @Size(max = 1)
    @Column(name = "COTIZD_LMA")
    private String cotizdLma;
    @Column(name = "COTIZD_LMA_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdLmaFinicial;
    @Column(name = "COTIZD_LMA_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdLmaFfinal;
    @Column(name = "DIAS_LICENCIA_MATERNIDAD")
    private BigInteger diasLicenciaMaternidad;
    @Size(max = 1)
    @Column(name = "COTIZD_VAC")
    private String cotizdVac;
    @Column(name = "COTIZD_VAC_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdVacFinicial;
    @Column(name = "COTIZD_VAC_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdVacFfinal;
    @Column(name = "DIAS_VACACIONES")
    private BigInteger diasVacaciones;
    @Size(max = 255)
    @Column(name = "COTIZD_IRP")
    private String cotizdIrp;
    @Column(name = "COTIZD_IRP_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdIrpFinicial;
    @Column(name = "COTIZD_IRP_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cotizdIrpFfinal;
    @Column(name = "DIAS_INCAPACIDAD_RP")
    private BigInteger diasIncapacidadRp;
    @Size(max = 255)
    @Column(name = "COTIZD_SALARIO_INTEGRAL")
    private String cotizdSalarioIntegral;
    @Column(name = "DIAS_LABORADOS")
    private BigInteger diasLaborados;
    @Size(max = 255)
    @Column(name = "COTIZD_ULTIMO_IBC_SIN_NOVEDAD")
    private String cotizdUltimoIbcSinNovedad;
    @Size(max = 2)
    @Column(name = "ING_PILA_AT")
    private String ingPilaAt;
    @Size(max = 2)
    @Column(name = "RET_PILA_AU")
    private String retPilaAu;
    @Size(max = 2)
    @Column(name = "SLN_PILA_AV")
    private String slnPilaAv;
    @Size(max = 2)
    @Column(name = "IGE_PILA_AW")
    private String igePilaAw;
    @Size(max = 2)
    @Column(name = "LMA_PILA_AX")
    private String lmaPilaAx;
    @Size(max = 2)
    @Column(name = "VAC_PILA_AY")
    private String vacPilaAy;
    @Size(max = 2)
    @Column(name = "IRP_PILA_AZ")
    private String irpPilaAz;
    @Column(name = "PORCENTAJE_PAGOS_NO_SALARIA_BB")
    private BigDecimal porcentajePagosNoSalariaBb;
    @Column(name = "EXCEDE_LIMITE_PAGO_NO_SALAR_BC")
    private BigDecimal excedeLimitePagoNoSalarBc;
    @Size(max = 6)
    @Column(name = "CODIGO_ADMINISTRADORA_BD")
    private String codigoAdministradoraBd;
    @Size(max = 100)
    @Column(name = "NOMBRE_CORTO_ADMINISTRADORA_BE")
    private String nombreCortoAdministradoraBe;
    @Column(name = "IBC_CORE_BF")
    private BigDecimal ibcCoreBf;
    @Column(name = "TARIFA_CORE_BG")
    private BigDecimal tarifaCoreBg;
    @Column(name = "COTIZACION_OBLIGATORIA_BH")
    private BigDecimal cotizacionObligatoriaBh;
    @Column(name = "DIAS_COTIZADOS_PILA_BI")
    private BigInteger diasCotizadosPilaBi;
    @Column(name = "IBC_PILA_BJ")
    private BigDecimal ibcPilaBj;
    @Column(name = "TARIFA_PILA_BK")
    private BigDecimal tarifaPilaBk;
    @Column(name = "COTIZACION_PAGADA_PILA_BL")
    private BigDecimal cotizacionPagadaPilaBl;
    @Column(name = "AJUSTE_BM")
    private BigDecimal ajusteBm;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_BN")
    private String conceptoAjusteBn;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_BO")
    private String tipoIncumplimientoBo;
    @Size(max = 6)
    @Column(name = "CODIGO_ADMINISTRADORA_BP")
    private String codigoAdministradoraBp;
    @Size(max = 100)
    @Column(name = "NOMBRE_CORTO_ADMINISTRADORA_BQ")
    private String nombreCortoAdministradoraBq;
    @Column(name = "IBC_CORE_BR")
    private BigDecimal ibcCoreBr;
    @Column(name = "TARIFA_CORE_PENSION_BS")
    private BigDecimal tarifaCorePensionBs;
    @Column(name = "COTIZACION_OBLIGATO_A_PENSI_BT")
    private BigDecimal cotizacionObligatoAPensiBt;
    @Column(name = "TARIFA_CORE_FSP_BU")
    private BigDecimal tarifaCoreFspBu;
    @Column(name = "COTIZACION_OBLIGATORIA_FSP_BV")
    private BigDecimal cotizacionObligatoriaFspBv;
    @Column(name = "TARIFA_CORE_PENSION_ADIC_AR_BW")
    private BigDecimal tarifaCorePensionAdicArBw;
    @Column(name = "COTIZACION_OBLI_PEN_ADIC_AR_BX")
    private BigDecimal cotizacionObliPenAdicArBx;
    @Column(name = "DIAS_COTIZADOS_PILA_BY")
    private BigInteger diasCotizadosPilaBy;
    @Column(name = "IBC_PILA_BZ")
    private BigDecimal ibcPilaBz;
    @Column(name = "TARIFA_PENSION_CA")
    private BigDecimal tarifaPensionCa;
    @Column(name = "COTIZACION_PAGADA_PENSION_CB")
    private BigDecimal cotizacionPagadaPensionCb;
    @Column(name = "AJUSTE_PENSION_CC")
    private BigDecimal ajustePensionCc;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_PENSION_CD")
    private String conceptoAjustePensionCd;
    @Column(name = "TARIFA_PILA_FSP_CE")
    private BigDecimal tarifaPilaFspCe;
    @Column(name = "COTIZACION_PAGADA_FSP_CF")
    private BigDecimal cotizacionPagadaFspCf;
    @Column(name = "AJUSTE_FSP_CG")
    private BigDecimal ajusteFspCg;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_FSP_CH")
    private String conceptoAjusteFspCh;
    @Column(name = "TARIFA_PILA_PENSI_ADICIO_AR_CI")
    private BigDecimal tarifaPilaPensiAdicioArCi;
    @Column(name = "COTIZACION_PAG_PENS_ADIC_AR_CJ")
    private BigDecimal cotizacionPagPensAdicArCj;
    @Column(name = "AJUSTE_PENSION_ADICIONAL_AR_CK")
    private BigDecimal ajustePensionAdicionalArCk;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_PENSION_AR_CL")
    private String conceptoAjustePensionArCl;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_CM")
    private String tipoIncumplimientoCm;
    @Column(name = "CALCULO_ACTUARIAL_CN")
    private BigDecimal calculoActuarialCn;
    @Size(max = 6)
    @Column(name = "CODIGO_ADMINISTRADORA_CO")
    private String codigoAdministradoraCo;
    @Size(max = 100)
    @Column(name = "NOMBRE_CORTO_ADMINISTRADORA_CP")
    private String nombreCortoAdministradoraCp;
    @Column(name = "IBC_CORE_CQ")
    private BigDecimal ibcCoreCq;
    @Column(name = "COTIZACION_OBLIGATORIA_CR")
    private BigDecimal cotizacionObligatoriaCr;
    @Column(name = "DIAS_COTIZADOS_PILA_CS")
    private BigInteger diasCotizadosPilaCs;
    @Column(name = "IBC_PILA_CT")
    private BigDecimal ibcPilaCt;
    @Column(name = "TARIFA_PILA_CU")
    private BigDecimal tarifaPilaCu;
    @Column(name = "COTIZACION_PAGADA_PILA_CV")
    private BigDecimal cotizacionPagadaPilaCv;
    @Column(name = "AJUSTE_CW")
    private BigDecimal ajusteCw;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_CX")
    private String conceptoAjusteCx;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_CY")
    private String tipoIncumplimientoCy;
    @Size(max = 100)
    @Column(name = "NOMBRE_CORTO_ADMINISTRADORA_CZ")
    private String nombreCortoAdministradoraCz;
    @Column(name = "IBC_CORE_DA")
    private BigDecimal ibcCoreDa;
    @Column(name = "TARIFA_CORE_DB")
    private BigDecimal tarifaCoreDb;
    @Column(name = "COTIZACION_OBLIGATORIA_DC")
    private BigDecimal cotizacionObligatoriaDc;
    @Column(name = "DIAS_COTIZADOS_PILA_DD")
    private BigInteger diasCotizadosPilaDd;
    @Column(name = "IBC_PILA_DE")
    private BigDecimal ibcPilaDe;
    @Column(name = "TARIFA_PILA_DF")
    private BigDecimal tarifaPilaDf;
    @Column(name = "COTIZACION_PAGADA_PILA_DG")
    private BigDecimal cotizacionPagadaPilaDg;
    @Column(name = "AJUSTE_DH")
    private BigDecimal ajusteDh;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_DI")
    private String conceptoAjusteDi;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_DJ")
    private String tipoIncumplimientoDj;
    @Column(name = "IBC_CORE_DK")
    private BigDecimal ibcCoreDk;
    @Column(name = "TARIFA_CORE_DL")
    private BigDecimal tarifaCoreDl;
    @Column(name = "APORTE_DM")
    private BigDecimal aporteDm;
    @Column(name = "TARIFA_PILA_DN")
    private BigDecimal tarifaPilaDn;
    @Column(name = "APORTE_PILA_DO")
    private BigDecimal aportePilaDo;
    @Column(name = "AJUSTE_DP")
    private BigDecimal ajusteDp;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_DQ")
    private String conceptoAjusteDq;
    @Size(max = 100)
    @Column(name = "TIPO_INCUMPLIMIENTO_DR")
    private String tipoIncumplimientoDr;
    @Column(name = "IBC_CORE_DS")
    private BigDecimal ibcCoreDs;
    @Column(name = "TARIFA_CORE_DT")
    private BigDecimal tarifaCoreDt;
    @Column(name = "APORTE_DU")
    private BigDecimal aporteDu;
    @Column(name = "TARIFA_PILA_DV")
    private BigDecimal tarifaPilaDv;
    @Column(name = "APORTE_PILA_DW")
    private BigDecimal aportePilaDw;
    @Column(name = "AJUSTE_DX")
    private BigDecimal ajusteDx;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_DY")
    private String conceptoAjusteDy;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_DZ")
    private String tipoIncumplimientoDz;
    @Column(name = "IBC_EA")
    private BigDecimal ibcEa;
    @Column(name = "TARIFA_CORE_EB")
    private BigDecimal tarifaCoreEb;
    @Column(name = "APORTE_EC")
    private BigDecimal aporteEc;
    @Column(name = "TARIFA_PILA_ED")
    private BigDecimal tarifaPilaEd;
    @Column(name = "APORTE_PILA_EE")
    private BigDecimal aportePilaEe;
    @Column(name = "AJUSTE_EF")
    private BigDecimal ajusteEf;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_EG")
    private String conceptoAjusteEg;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_EH")
    private String tipoIncumplimientoEh;
    @Column(name = "IBC_CORE_EI")
    private BigDecimal ibcCoreEi;
    @Column(name = "TARIFA_CORE_EJ")
    private BigDecimal tarifaCoreEj;
    @Column(name = "APORTE_EK")
    private BigDecimal aporteEk;
    @Column(name = "TARIFA_PILA_EL")
    private BigDecimal tarifaPilaEl;
    @Column(name = "APORTE_EM")
    private BigDecimal aporteEm;
    @Column(name = "AJUSTE_EN")
    private BigDecimal ajusteEn;
    @Size(max = 100)
    @Column(name = "CONCEPTO_AJUSTE_EO")
    private String conceptoAjusteEo;
    @Size(max = 20)
    @Column(name = "TIPO_INCUMPLIMIENTO_EP")
    private String tipoIncumplimientoEp;
    @JoinColumn(name = "IDHOJACALCULOLIQUIDACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private HojaCalculoLiquidacion idhojacalculoliquidacion;
    
    //Campos nuevos - WR_JO
    @Column(name = "IBC_PERMISOS_REMUNERADOS")
    private BigInteger ibcPermisosRemunerados;
    @Column(name = "IBC_SUSP_PERMISOS")
    private BigInteger ibcSuspPermisos;
    @Column(name = "IBC_VACACIONES")
    private BigInteger ibcVacaciones;    
    @Column(name = "IBC_HUELGA")
    private BigInteger ibcHuelga;    
    @Column(name = "DIAS_COT_PENSION")
    private Integer diasCotPension;
    @Column(name = "DIAS_COT_SALUD")
    private Integer diasCotSalud;
    @Column(name = "DIAS_COT_RPROF")
    private Integer diasCotRprof;
    @Column(name = "DIAS_COT_CCF")
    private Integer diasCotCcf;
    @Column(name = "PAGO_NO_SALARIAL")
    private BigInteger pagoNoSalarial;    
    @Column(name = "TOTAL_REMUNERADO")
    private BigInteger totalRemunerado;
    @Column(name = "POR_PAGO_NO_SALARIAL")
    private Integer porPagoNoSalarial;
    @Column(name = "EXC_LIM_PAGO_NO_SALARIAL")
    private BigInteger excLimPagoNoSalarial;
    @Column(name = "COD_ADM_SALUD")
    private String codAdmSalud;
    @Column(name = "IBC_PAGOS_NOM_SALUD")
    private BigInteger ibcPagosNomSalud;    
    @Basic(optional = false)
    @Column(name = "IBC_CALCULADO_SALUD")
    private BigInteger ibcCalculadoSalud;
    @Column(name = "TARIFA_SALUD")
    private BigDecimal tarifaSalud;
    @Column(name = "TARIFA_SALUD_SUSPENSION")
    private BigDecimal tarifaSaludSuspension;
    @Column(name = "COTIZ_OBL_CALCULADA_SALUD")
    private BigInteger cotizOblCalculadaSalud;
    @Column(name = "DIAS_COTIZ_PILA_SALUD")
    private Integer diasCotizPilaSalud;
    @Column(name = "IBC_PILA_SALUD")
    private BigInteger ibcPilaSalud;
    @Column(name = "TARIFA_PILA_SALUD")
    private BigDecimal tarifaPilaSalud;        
    @Column(name = "COTIZ_PAGADA_PILA_SALUD")
    private BigInteger cotizPagadaPilaSalud;        
    @Column(name = "AJUSTE_SALUD")
    private BigInteger ajusteSalud;        
    @Column(name = "CONCEPTO_AJUSTE_SALUD")
    private String conceptoAjusteSalud;
    @Column(name = "TIPO_INCUMPLIMIENTO_SALUD")
    private String tipoIncumplimientoSalud;  
    @Column(name = "IBC_PAGOS_NOM_PENSION")
    private BigInteger ibcPagosNomPension;
    @Column(name = "IBC_CALCULADO_PENSION")
    private BigInteger ibcCalculadoPension;
    @Column(name = "TARIFA_PENSION")
    private BigDecimal tarifaPension;
    @Column(name = "COTIZ_OBL_PENSION")
    private BigInteger cotizOblPension;
    @Column(name = "DIAS_COTIZ_PILA_PENSION")
    private Integer diasCotizPilaPension;
    @Column(name = "IBC_PILA_PENSION")
    private BigInteger ibcPilaPension;
    @Column(name = "TARIFA_PILA_PENSION")
    private BigDecimal tarifaPilaPension;
    @Column(name = "COT_PAGADA_PILA_PENSION")
    private BigInteger cotPagadaPilaPension;
    @Column(name = "AJUSTE_PENSION")
    private BigInteger ajustePension;
    @Column(name = "TARIFA_FSP_SUBCUEN_SOLIDARIDAD")
    private Integer tarifaFspSubcuenSolidaridad;
    @Column(name = "TARIFA_FSP_SUBCUEN_SUBSISTEN")
    private Integer tarifaFspSubcuenSubsisten;
    @Column(name = "COTIZ_OBL_FSP_SUB_SOLIDARIDAD")
    private BigInteger cotizOblFspSubSolidaridad;
    @Column(name = "COTIZ_OBL_FSP_SUB_SUBSISTENCIA")
    private BigInteger cotizOblFspSubSubsistencia;
    @Column(name = "COTIZ_PAG_PILA_FSP_SUB_SOLIDAR")
    private BigInteger cotizPagPilaFspSubSolidar;
    @Column(name = "COTIZ_PAG_PILA_FSP_SUB_SUBSIS")
    private BigInteger cotizPagPilaFspSubSubsis;    
    @Column(name = "AJUSTE_FSP_SUBCUEN_SOLIDARIDAD")
    private BigInteger ajusteFspSubcuenSolidaridad;
    @Column(name = "AJUSTE_FSP_SUBCUEN_SUBSISTEN")
    private BigInteger ajusteFspSubcuenSubsisten;
    @Column(name = "CALCULO_ACTUARIAL")
    private BigInteger calculoActuarial;
    @Column(name = "COD_ADM_ARL")
    private String codAdmArl;    
    @Column(name = "IBC_ARL")
    private BigInteger ibcArl;
    @Column(name = "TARIFA_ARL")
    private BigDecimal tarifaArl;    
    @Column(name = "COTIZ_OBL_ARL")
    private BigInteger cotizOblArl;    
    @Column(name = "DIAS_COTIZ_PILA_ARL")
    private BigInteger diasCotizPilaArl;    
    @Column(name = "IBC_PILA_ARL")
    private BigInteger ibcPilaArl;   
    @Column(name = "TARIFA_PILA_ARL")
    private BigDecimal tarifaPilaArl;        
    @Column(name = "COTIZ_PAGADA_PILA_ARL")
    private BigInteger cotizPagadaPilaArl;        
    @Column(name = "AJUSTE_ARL")
    private BigInteger ajusteArl;        
    @Column(name = "CONCEPTO_AJUSTE_ARL")
    private String conceptoAjusteArl;    
    @Column(name = "TIPO_INCUMPLIMIENTO_ARL")
    private String tipoIncumplimientoArl;        
    @Column(name = "COD_ADM_CCF")
    private String codAdmCcf;        
    @Column(name = "IBC_CCF")
    private BigInteger ibcCcf;            
    @Column(name = "TARIFA_CCF")
    private BigDecimal tarifaCcf;            
    @Column(name = "COTIZ_OBL_CCF")
    private BigInteger cotizOblCcf;            
    @Column(name = "DIAS_COTI_PILA_CCF")
    private BigInteger diasCotiPilaCcf;            
    @Column(name = "IBC_PILA_CCF")
    private BigInteger ibcPilaCcf;            
    @Column(name = "TARIFA_PILA_CCF")
    private BigDecimal tarifaPilaCcf;            
    @Column(name = "COTIZ_PAGADA_PILA_CCF")
    private BigInteger cotizPagadaPilaCcf;            
    @Column(name = "AJUSTE_CCF")
    private BigInteger ajusteCcf;            
    @Column(name = "CONCEPTO_AJUSTE_CCF")
    private String conceptoAjusteCcf;            
    @Column(name = "TIPO_INCUMPLIMIENTO_CCF")
    private String tipoIncumplimientoCcf;                
    @Column(name = "IBC_SENA")
    private BigInteger ibcSena;                
    @Column(name = "TARIFA_SENA")
    private BigDecimal tarifaSena;                    
    @Column(name = "COTIZ_OBL_SENA")
    private BigInteger cotizOblSena;                    
    @Column(name = "TARIFA_PILA_SENA")
    private BigDecimal tarifaPilaSena;                    
    @Column(name = "COTIZ_PAGADA_PILA_SENA")
    private BigInteger cotizPagadaPilaSena;                    
    @Column(name = "AJUSTE_SENA")
    private BigInteger ajusteSena;                    
    @Column(name = "CONCEPTO_AJUSTE_SENA")
    private String conceptoAjusteSena;                    
    @Column(name = "TIPO_INCUMPLIMIENTO_SENA")
    private String tipoIncumplimientoSena;                    
    @Column(name = "IBC_ICBF")
    private BigInteger ibcIcbf;                    
    @Column(name = "TARIFA_ICBF")
    private BigDecimal tarifaIcbf;                 
    @Column(name = "COTIZ_OBL_ICBF")
    private BigInteger cotizOblIcbf;                        
    @Column(name = "TARIFA_PILA_ICBF")
    private BigDecimal tarifaPilaIcbf;                        
    @Column(name = "COTIZ_PAGADA_PILA_ICBF")
    private BigInteger cotizPagadaPilaIcbf;                    
    @Column(name = "AJUSTE_ICBF")
    private BigInteger ajusteIcbf;                        
    @Column(name = "CONCEPTO_AJUSTE_ICBF")
    private String conceptoAjusteIcbf;                        
    @Column(name = "TIPO_INCUMPLIMIENTO_ICBF")
    private String tipoIncumplimientoIcbf;                            
    @Column(name = "PLANILLA_PILA_CARGADA")
    private String planillaPilaCargada;                            
    
    
    @Column(name = "TOTAL_DEVENGADO")
    private BigInteger totalDevengado;
    
    
    public BigInteger getTotalDevengado() {
        return totalDevengado;
    }

    public void setTotalDevengado(BigInteger totalDevengado) {
        this.totalDevengado = totalDevengado;
    }
    
    
    
    public HojaCalculoLiquidacionDetalle() {
    }

    public HojaCalculoLiquidacionDetalle(BigDecimal id) {
        this.id = id;
    }

    public HojaCalculoLiquidacionDetalle(BigDecimal id, BigInteger idnominadetalle, BigInteger idpiladetalle, BigInteger idconciliacioncontabledetalle, BigInteger aportaId, String aportaNumeroIdentificacion, String aportaPrimerNombre, BigInteger cotizId, BigInteger cotizTipoDocumento, String cotizNumeroIdentificacion, String cotizNombre, BigInteger cotizTipoCotizante, BigInteger cotizSubtipoCotizante, String cotizExtranjeroNoCotizar, String cotizColombianoEnElExt, String cotizActividadAltoRiesgoPe) {
        this.id = id;
        this.idnominadetalle = idnominadetalle;
        this.idpiladetalle = idpiladetalle;
        this.idconciliacioncontabledetalle = idconciliacioncontabledetalle;
        this.aportaId = aportaId;
        this.aportaNumeroIdentificacion = aportaNumeroIdentificacion;
        this.aportaPrimerNombre = aportaPrimerNombre;
        this.cotizId = cotizId;
        this.cotizTipoDocumento = cotizTipoDocumento;
        this.cotizNumeroIdentificacion = cotizNumeroIdentificacion;
        this.cotizNombre = cotizNombre;
        this.cotizTipoCotizante = cotizTipoCotizante;
        this.cotizSubtipoCotizante = cotizSubtipoCotizante;
        this.cotizExtranjeroNoCotizar = cotizExtranjeroNoCotizar;
        this.cotizColombianoEnElExt = cotizColombianoEnElExt;
        this.cotizActividadAltoRiesgoPe = cotizActividadAltoRiesgoPe;
    }

    public BigDecimal getId() {
        return id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    public BigInteger getIdnominadetalle() {
        return idnominadetalle;
    }

    public void setIdnominadetalle(BigInteger idnominadetalle) {
        this.idnominadetalle = idnominadetalle;
    }

    public BigInteger getIdpiladetalle() {
        return idpiladetalle;
    }

    public void setIdpiladetalle(BigInteger idpiladetalle) {
        this.idpiladetalle = idpiladetalle;
    }

    public BigInteger getIdconciliacioncontabledetalle() {
        return idconciliacioncontabledetalle;
    }

    public void setIdconciliacioncontabledetalle(BigInteger idconciliacioncontabledetalle) {
        this.idconciliacioncontabledetalle = idconciliacioncontabledetalle;
    }

    public BigInteger getAportaId() {
        return aportaId;
    }

    public void setAportaId(BigInteger aportaId) {
        this.aportaId = aportaId;
    }

    public BigInteger getAportaTipoIdentificacion() {
        return aportaTipoIdentificacion;
    }

    public void setAportaTipoIdentificacion(BigInteger aportaTipoIdentificacion) {
        this.aportaTipoIdentificacion = aportaTipoIdentificacion;
    }

    public String getAportaNumeroIdentificacion() {
        return aportaNumeroIdentificacion;
    }

    public void setAportaNumeroIdentificacion(String aportaNumeroIdentificacion) {
        this.aportaNumeroIdentificacion = aportaNumeroIdentificacion;
    }

    public String getAportaPrimerNombre() {
        return aportaPrimerNombre;
    }

    public void setAportaPrimerNombre(String aportaPrimerNombre) {
        this.aportaPrimerNombre = aportaPrimerNombre;
    }

    public String getAportaClase() {
        return aportaClase;
    }

    public void setAportaClase(String aportaClase) {
        this.aportaClase = aportaClase;
    }

    public String getAportaAporteEsapYMen() {
        return aportaAporteEsapYMen;
    }

    public void setAportaAporteEsapYMen(String aportaAporteEsapYMen) {
        this.aportaAporteEsapYMen = aportaAporteEsapYMen;
    }

    public String getAportaExcepcionLey12332008() {
        return aportaExcepcionLey12332008;
    }

    public void setAportaExcepcionLey12332008(String aportaExcepcionLey12332008) {
        this.aportaExcepcionLey12332008 = aportaExcepcionLey12332008;
    }

    public BigInteger getCotizId() {
        return cotizId;
    }

    public void setCotizId(BigInteger cotizId) {
        this.cotizId = cotizId;
    }

    public BigInteger getCotizTipoDocumento() {
        return cotizTipoDocumento;
    }

    public void setCotizTipoDocumento(BigInteger cotizTipoDocumento) {
        this.cotizTipoDocumento = cotizTipoDocumento;
    }

    public String getCotizNumeroIdentificacion() {
        return cotizNumeroIdentificacion;
    }

    public void setCotizNumeroIdentificacion(String cotizNumeroIdentificacion) {
        this.cotizNumeroIdentificacion = cotizNumeroIdentificacion;
    }

    public String getCotizNombre() {
        return cotizNombre;
    }

    public void setCotizNombre(String cotizNombre) {
        this.cotizNombre = cotizNombre;
    }

    public BigInteger getCotizTipoCotizante() {
        return cotizTipoCotizante;
    }

    public void setCotizTipoCotizante(BigInteger cotizTipoCotizante) {
        this.cotizTipoCotizante = cotizTipoCotizante;
    }

    public BigInteger getCotizSubtipoCotizante() {
        return cotizSubtipoCotizante;
    }

    public void setCotizSubtipoCotizante(BigInteger cotizSubtipoCotizante) {
        this.cotizSubtipoCotizante = cotizSubtipoCotizante;
    }

    public String getCotizExtranjeroNoCotizar() {
        return cotizExtranjeroNoCotizar;
    }

    public void setCotizExtranjeroNoCotizar(String cotizExtranjeroNoCotizar) {
        this.cotizExtranjeroNoCotizar = cotizExtranjeroNoCotizar;
    }

    public String getCotizColombianoEnElExt() {
        return cotizColombianoEnElExt;
    }

    public void setCotizColombianoEnElExt(String cotizColombianoEnElExt) {
        this.cotizColombianoEnElExt = cotizColombianoEnElExt;
    }

    public String getCotizActividadAltoRiesgoPe() {
        return cotizActividadAltoRiesgoPe;
    }

    public void setCotizActividadAltoRiesgoPe(String cotizActividadAltoRiesgoPe) {
        this.cotizActividadAltoRiesgoPe = cotizActividadAltoRiesgoPe;
    }

    public BigInteger getCotizdAno() {
        return cotizdAno;
    }

    public void setCotizdAno(BigInteger cotizdAno) {
        this.cotizdAno = cotizdAno;
    }

    public BigInteger getCotizdMes() {
        return cotizdMes;
    }

    public void setCotizdMes(BigInteger cotizdMes) {
        this.cotizdMes = cotizdMes;
    }

    public String getCotizdIng() {
        return cotizdIng;
    }

    public void setCotizdIng(String cotizdIng) {
        this.cotizdIng = cotizdIng;
    }

    public Date getCotizdIngFecha() {
        return cotizdIngFecha;
    }

    public void setCotizdIngFecha(Date cotizdIngFecha) {
        this.cotizdIngFecha = cotizdIngFecha;
    }

    public String getCotizdRet() {
        return cotizdRet;
    }

    public void setCotizdRet(String cotizdRet) {
        this.cotizdRet = cotizdRet;
    }

    public Date getCotizdRetFecha() {
        return cotizdRetFecha;
    }

    public void setCotizdRetFecha(Date cotizdRetFecha) {
        this.cotizdRetFecha = cotizdRetFecha;
    }

    public String getCotizdSln() {
        return cotizdSln;
    }

    public void setCotizdSln(String cotizdSln) {
        this.cotizdSln = cotizdSln;
    }

    public String getCotizdCausalSuspension() {
        return cotizdCausalSuspension;
    }

    public void setCotizdCausalSuspension(String cotizdCausalSuspension) {
        this.cotizdCausalSuspension = cotizdCausalSuspension;
    }

    public Date getCotizdCausalSuspFinicial() {
        return cotizdCausalSuspFinicial;
    }

    public void setCotizdCausalSuspFinicial(Date cotizdCausalSuspFinicial) {
        this.cotizdCausalSuspFinicial = cotizdCausalSuspFinicial;
    }

    public Date getCotizdCausalSuspFfinal() {
        return cotizdCausalSuspFfinal;
    }

    public void setCotizdCausalSuspFfinal(Date cotizdCausalSuspFfinal) {
        this.cotizdCausalSuspFfinal = cotizdCausalSuspFfinal;
    }

    public BigInteger getDiasSuspension() {
        return diasSuspension;
    }

    public void setDiasSuspension(BigInteger diasSuspension) {
        this.diasSuspension = diasSuspension;
    }

    public String getCotizdIge() {
        return cotizdIge;
    }

    public void setCotizdIge(String cotizdIge) {
        this.cotizdIge = cotizdIge;
    }

    public Date getCotizdIgeFinicial() {
        return cotizdIgeFinicial;
    }

    public void setCotizdIgeFinicial(Date cotizdIgeFinicial) {
        this.cotizdIgeFinicial = cotizdIgeFinicial;
    }

    public Date getCotizdIgeFfinal() {
        return cotizdIgeFfinal;
    }

    public void setCotizdIgeFfinal(Date cotizdIgeFfinal) {
        this.cotizdIgeFfinal = cotizdIgeFfinal;
    }

    public BigInteger getDiasIncapacidadGeneral() {
        return diasIncapacidadGeneral;
    }

    public void setDiasIncapacidadGeneral(BigInteger diasIncapacidadGeneral) {
        this.diasIncapacidadGeneral = diasIncapacidadGeneral;
    }

    public String getCotizdLma() {
        return cotizdLma;
    }

    public void setCotizdLma(String cotizdLma) {
        this.cotizdLma = cotizdLma;
    }

    public Date getCotizdLmaFinicial() {
        return cotizdLmaFinicial;
    }

    public void setCotizdLmaFinicial(Date cotizdLmaFinicial) {
        this.cotizdLmaFinicial = cotizdLmaFinicial;
    }

    public Date getCotizdLmaFfinal() {
        return cotizdLmaFfinal;
    }

    public void setCotizdLmaFfinal(Date cotizdLmaFfinal) {
        this.cotizdLmaFfinal = cotizdLmaFfinal;
    }

    public BigInteger getDiasLicenciaMaternidad() {
        return diasLicenciaMaternidad;
    }

    public void setDiasLicenciaMaternidad(BigInteger diasLicenciaMaternidad) {
        this.diasLicenciaMaternidad = diasLicenciaMaternidad;
    }

    public String getCotizdVac() {
        return cotizdVac;
    }

    public void setCotizdVac(String cotizdVac) {
        this.cotizdVac = cotizdVac;
    }

    public Date getCotizdVacFinicial() {
        return cotizdVacFinicial;
    }

    public void setCotizdVacFinicial(Date cotizdVacFinicial) {
        this.cotizdVacFinicial = cotizdVacFinicial;
    }

    public Date getCotizdVacFfinal() {
        return cotizdVacFfinal;
    }

    public void setCotizdVacFfinal(Date cotizdVacFfinal) {
        this.cotizdVacFfinal = cotizdVacFfinal;
    }

    public BigInteger getDiasVacaciones() {
        return diasVacaciones;
    }

    public void setDiasVacaciones(BigInteger diasVacaciones) {
        this.diasVacaciones = diasVacaciones;
    }

    public String getCotizdIrp() {
        return cotizdIrp;
    }

    public void setCotizdIrp(String cotizdIrp) {
        this.cotizdIrp = cotizdIrp;
    }

    public Date getCotizdIrpFinicial() {
        return cotizdIrpFinicial;
    }

    public void setCotizdIrpFinicial(Date cotizdIrpFinicial) {
        this.cotizdIrpFinicial = cotizdIrpFinicial;
    }

    public Date getCotizdIrpFfinal() {
        return cotizdIrpFfinal;
    }

    public void setCotizdIrpFfinal(Date cotizdIrpFfinal) {
        this.cotizdIrpFfinal = cotizdIrpFfinal;
    }

    public BigInteger getDiasIncapacidadRp() {
        return diasIncapacidadRp;
    }

    public void setDiasIncapacidadRp(BigInteger diasIncapacidadRp) {
        this.diasIncapacidadRp = diasIncapacidadRp;
    }

    public String getCotizdSalarioIntegral() {
        return cotizdSalarioIntegral;
    }

    public void setCotizdSalarioIntegral(String cotizdSalarioIntegral) {
        this.cotizdSalarioIntegral = cotizdSalarioIntegral;
    }

    public BigInteger getDiasLaborados() {
        return diasLaborados;
    }

    public void setDiasLaborados(BigInteger diasLaborados) {
        this.diasLaborados = diasLaborados;
    }

    public String getCotizdUltimoIbcSinNovedad() {
        return cotizdUltimoIbcSinNovedad;
    }

    public void setCotizdUltimoIbcSinNovedad(String cotizdUltimoIbcSinNovedad) {
        this.cotizdUltimoIbcSinNovedad = cotizdUltimoIbcSinNovedad;
    }

    public String getIngPilaAt() {
        return ingPilaAt;
    }

    public void setIngPilaAt(String ingPilaAt) {
        this.ingPilaAt = ingPilaAt;
    }

    public String getRetPilaAu() {
        return retPilaAu;
    }

    public void setRetPilaAu(String retPilaAu) {
        this.retPilaAu = retPilaAu;
    }

    public String getSlnPilaAv() {
        return slnPilaAv;
    }

    public void setSlnPilaAv(String slnPilaAv) {
        this.slnPilaAv = slnPilaAv;
    }

    public String getIgePilaAw() {
        return igePilaAw;
    }

    public void setIgePilaAw(String igePilaAw) {
        this.igePilaAw = igePilaAw;
    }

    public String getLmaPilaAx() {
        return lmaPilaAx;
    }

    public void setLmaPilaAx(String lmaPilaAx) {
        this.lmaPilaAx = lmaPilaAx;
    }

    public String getVacPilaAy() {
        return vacPilaAy;
    }

    public void setVacPilaAy(String vacPilaAy) {
        this.vacPilaAy = vacPilaAy;
    }

    public String getIrpPilaAz() {
        return irpPilaAz;
    }

    public void setIrpPilaAz(String irpPilaAz) {
        this.irpPilaAz = irpPilaAz;
    }

    public BigDecimal getPorcentajePagosNoSalariaBb() {
        return porcentajePagosNoSalariaBb;
    }

    public void setPorcentajePagosNoSalariaBb(BigDecimal porcentajePagosNoSalariaBb) {
        this.porcentajePagosNoSalariaBb = porcentajePagosNoSalariaBb;
    }

    public BigDecimal getExcedeLimitePagoNoSalarBc() {
        return excedeLimitePagoNoSalarBc;
    }

    public void setExcedeLimitePagoNoSalarBc(BigDecimal excedeLimitePagoNoSalarBc) {
        this.excedeLimitePagoNoSalarBc = excedeLimitePagoNoSalarBc;
    }

    public String getCodigoAdministradoraBd() {
        return codigoAdministradoraBd;
    }

    public void setCodigoAdministradoraBd(String codigoAdministradoraBd) {
        this.codigoAdministradoraBd = codigoAdministradoraBd;
    }

    public String getNombreCortoAdministradoraBe() {
        return nombreCortoAdministradoraBe;
    }

    public void setNombreCortoAdministradoraBe(String nombreCortoAdministradoraBe) {
        this.nombreCortoAdministradoraBe = nombreCortoAdministradoraBe;
    }

    public BigDecimal getIbcCoreBf() {
        return ibcCoreBf;
    }

    public void setIbcCoreBf(BigDecimal ibcCoreBf) {
        this.ibcCoreBf = ibcCoreBf;
    }

    public BigDecimal getTarifaCoreBg() {
        return tarifaCoreBg;
    }

    public void setTarifaCoreBg(BigDecimal tarifaCoreBg) {
        this.tarifaCoreBg = tarifaCoreBg;
    }

    public BigDecimal getCotizacionObligatoriaBh() {
        return cotizacionObligatoriaBh;
    }

    public void setCotizacionObligatoriaBh(BigDecimal cotizacionObligatoriaBh) {
        this.cotizacionObligatoriaBh = cotizacionObligatoriaBh;
    }

    public BigInteger getDiasCotizadosPilaBi() {
        return diasCotizadosPilaBi;
    }

    public void setDiasCotizadosPilaBi(BigInteger diasCotizadosPilaBi) {
        this.diasCotizadosPilaBi = diasCotizadosPilaBi;
    }

    public BigDecimal getIbcPilaBj() {
        return ibcPilaBj;
    }

    public void setIbcPilaBj(BigDecimal ibcPilaBj) {
        this.ibcPilaBj = ibcPilaBj;
    }

    public BigDecimal getTarifaPilaBk() {
        return tarifaPilaBk;
    }

    public void setTarifaPilaBk(BigDecimal tarifaPilaBk) {
        this.tarifaPilaBk = tarifaPilaBk;
    }

    public BigDecimal getCotizacionPagadaPilaBl() {
        return cotizacionPagadaPilaBl;
    }

    public void setCotizacionPagadaPilaBl(BigDecimal cotizacionPagadaPilaBl) {
        this.cotizacionPagadaPilaBl = cotizacionPagadaPilaBl;
    }

    public BigDecimal getAjusteBm() {
        return ajusteBm;
    }

    public void setAjusteBm(BigDecimal ajusteBm) {
        this.ajusteBm = ajusteBm;
    }

    public String getConceptoAjusteBn() {
        return conceptoAjusteBn;
    }

    public void setConceptoAjusteBn(String conceptoAjusteBn) {
        this.conceptoAjusteBn = conceptoAjusteBn;
    }

    public String getTipoIncumplimientoBo() {
        return tipoIncumplimientoBo;
    }

    public void setTipoIncumplimientoBo(String tipoIncumplimientoBo) {
        this.tipoIncumplimientoBo = tipoIncumplimientoBo;
    }

    public String getCodigoAdministradoraBp() {
        return codigoAdministradoraBp;
    }

    public void setCodigoAdministradoraBp(String codigoAdministradoraBp) {
        this.codigoAdministradoraBp = codigoAdministradoraBp;
    }

    public String getNombreCortoAdministradoraBq() {
        return nombreCortoAdministradoraBq;
    }

    public void setNombreCortoAdministradoraBq(String nombreCortoAdministradoraBq) {
        this.nombreCortoAdministradoraBq = nombreCortoAdministradoraBq;
    }

    public BigDecimal getIbcCoreBr() {
        return ibcCoreBr;
    }

    public void setIbcCoreBr(BigDecimal ibcCoreBr) {
        this.ibcCoreBr = ibcCoreBr;
    }

    public BigDecimal getTarifaCorePensionBs() {
        return tarifaCorePensionBs;
    }

    public void setTarifaCorePensionBs(BigDecimal tarifaCorePensionBs) {
        this.tarifaCorePensionBs = tarifaCorePensionBs;
    }

    public BigDecimal getCotizacionObligatoAPensiBt() {
        return cotizacionObligatoAPensiBt;
    }

    public void setCotizacionObligatoAPensiBt(BigDecimal cotizacionObligatoAPensiBt) {
        this.cotizacionObligatoAPensiBt = cotizacionObligatoAPensiBt;
    }

    public BigDecimal getTarifaCoreFspBu() {
        return tarifaCoreFspBu;
    }

    public void setTarifaCoreFspBu(BigDecimal tarifaCoreFspBu) {
        this.tarifaCoreFspBu = tarifaCoreFspBu;
    }

    public BigDecimal getCotizacionObligatoriaFspBv() {
        return cotizacionObligatoriaFspBv;
    }

    public void setCotizacionObligatoriaFspBv(BigDecimal cotizacionObligatoriaFspBv) {
        this.cotizacionObligatoriaFspBv = cotizacionObligatoriaFspBv;
    }

    public BigDecimal getTarifaCorePensionAdicArBw() {
        return tarifaCorePensionAdicArBw;
    }

    public void setTarifaCorePensionAdicArBw(BigDecimal tarifaCorePensionAdicArBw) {
        this.tarifaCorePensionAdicArBw = tarifaCorePensionAdicArBw;
    }

    public BigDecimal getCotizacionObliPenAdicArBx() {
        return cotizacionObliPenAdicArBx;
    }

    public void setCotizacionObliPenAdicArBx(BigDecimal cotizacionObliPenAdicArBx) {
        this.cotizacionObliPenAdicArBx = cotizacionObliPenAdicArBx;
    }

    public BigInteger getDiasCotizadosPilaBy() {
        return diasCotizadosPilaBy;
    }

    public void setDiasCotizadosPilaBy(BigInteger diasCotizadosPilaBy) {
        this.diasCotizadosPilaBy = diasCotizadosPilaBy;
    }

    public BigDecimal getIbcPilaBz() {
        return ibcPilaBz;
    }

    public void setIbcPilaBz(BigDecimal ibcPilaBz) {
        this.ibcPilaBz = ibcPilaBz;
    }

    public BigDecimal getTarifaPensionCa() {
        return tarifaPensionCa;
    }

    public void setTarifaPensionCa(BigDecimal tarifaPensionCa) {
        this.tarifaPensionCa = tarifaPensionCa;
    }

    public BigDecimal getCotizacionPagadaPensionCb() {
        return cotizacionPagadaPensionCb;
    }

    public void setCotizacionPagadaPensionCb(BigDecimal cotizacionPagadaPensionCb) {
        this.cotizacionPagadaPensionCb = cotizacionPagadaPensionCb;
    }

    public BigDecimal getAjustePensionCc() {
        return ajustePensionCc;
    }

    public void setAjustePensionCc(BigDecimal ajustePensionCc) {
        this.ajustePensionCc = ajustePensionCc;
    }

    public String getConceptoAjustePensionCd() {
        return conceptoAjustePensionCd;
    }

    public void setConceptoAjustePensionCd(String conceptoAjustePensionCd) {
        this.conceptoAjustePensionCd = conceptoAjustePensionCd;
    }

    public BigDecimal getTarifaPilaFspCe() {
        return tarifaPilaFspCe;
    }

    public void setTarifaPilaFspCe(BigDecimal tarifaPilaFspCe) {
        this.tarifaPilaFspCe = tarifaPilaFspCe;
    }

    public BigDecimal getCotizacionPagadaFspCf() {
        return cotizacionPagadaFspCf;
    }

    public void setCotizacionPagadaFspCf(BigDecimal cotizacionPagadaFspCf) {
        this.cotizacionPagadaFspCf = cotizacionPagadaFspCf;
    }

    public BigDecimal getAjusteFspCg() {
        return ajusteFspCg;
    }

    public void setAjusteFspCg(BigDecimal ajusteFspCg) {
        this.ajusteFspCg = ajusteFspCg;
    }

    public String getConceptoAjusteFspCh() {
        return conceptoAjusteFspCh;
    }

    public void setConceptoAjusteFspCh(String conceptoAjusteFspCh) {
        this.conceptoAjusteFspCh = conceptoAjusteFspCh;
    }

    public BigDecimal getTarifaPilaPensiAdicioArCi() {
        return tarifaPilaPensiAdicioArCi;
    }

    public void setTarifaPilaPensiAdicioArCi(BigDecimal tarifaPilaPensiAdicioArCi) {
        this.tarifaPilaPensiAdicioArCi = tarifaPilaPensiAdicioArCi;
    }

    public BigDecimal getCotizacionPagPensAdicArCj() {
        return cotizacionPagPensAdicArCj;
    }

    public void setCotizacionPagPensAdicArCj(BigDecimal cotizacionPagPensAdicArCj) {
        this.cotizacionPagPensAdicArCj = cotizacionPagPensAdicArCj;
    }

    public BigDecimal getAjustePensionAdicionalArCk() {
        return ajustePensionAdicionalArCk;
    }

    public void setAjustePensionAdicionalArCk(BigDecimal ajustePensionAdicionalArCk) {
        this.ajustePensionAdicionalArCk = ajustePensionAdicionalArCk;
    }

    public String getConceptoAjustePensionArCl() {
        return conceptoAjustePensionArCl;
    }

    public void setConceptoAjustePensionArCl(String conceptoAjustePensionArCl) {
        this.conceptoAjustePensionArCl = conceptoAjustePensionArCl;
    }

    public String getTipoIncumplimientoCm() {
        return tipoIncumplimientoCm;
    }

    public void setTipoIncumplimientoCm(String tipoIncumplimientoCm) {
        this.tipoIncumplimientoCm = tipoIncumplimientoCm;
    }

    public BigDecimal getCalculoActuarialCn() {
        return calculoActuarialCn;
    }

    public void setCalculoActuarialCn(BigDecimal calculoActuarialCn) {
        this.calculoActuarialCn = calculoActuarialCn;
    }

    public String getCodigoAdministradoraCo() {
        return codigoAdministradoraCo;
    }

    public void setCodigoAdministradoraCo(String codigoAdministradoraCo) {
        this.codigoAdministradoraCo = codigoAdministradoraCo;
    }

    public String getNombreCortoAdministradoraCp() {
        return nombreCortoAdministradoraCp;
    }

    public void setNombreCortoAdministradoraCp(String nombreCortoAdministradoraCp) {
        this.nombreCortoAdministradoraCp = nombreCortoAdministradoraCp;
    }

    public BigDecimal getIbcCoreCq() {
        return ibcCoreCq;
    }

    public void setIbcCoreCq(BigDecimal ibcCoreCq) {
        this.ibcCoreCq = ibcCoreCq;
    }

    public BigDecimal getCotizacionObligatoriaCr() {
        return cotizacionObligatoriaCr;
    }

    public void setCotizacionObligatoriaCr(BigDecimal cotizacionObligatoriaCr) {
        this.cotizacionObligatoriaCr = cotizacionObligatoriaCr;
    }

    public BigInteger getDiasCotizadosPilaCs() {
        return diasCotizadosPilaCs;
    }

    public void setDiasCotizadosPilaCs(BigInteger diasCotizadosPilaCs) {
        this.diasCotizadosPilaCs = diasCotizadosPilaCs;
    }

    public BigDecimal getIbcPilaCt() {
        return ibcPilaCt;
    }

    public void setIbcPilaCt(BigDecimal ibcPilaCt) {
        this.ibcPilaCt = ibcPilaCt;
    }

    public BigDecimal getTarifaPilaCu() {
        return tarifaPilaCu;
    }

    public void setTarifaPilaCu(BigDecimal tarifaPilaCu) {
        this.tarifaPilaCu = tarifaPilaCu;
    }

    public BigDecimal getCotizacionPagadaPilaCv() {
        return cotizacionPagadaPilaCv;
    }

    public void setCotizacionPagadaPilaCv(BigDecimal cotizacionPagadaPilaCv) {
        this.cotizacionPagadaPilaCv = cotizacionPagadaPilaCv;
    }

    public BigDecimal getAjusteCw() {
        return ajusteCw;
    }

    public void setAjusteCw(BigDecimal ajusteCw) {
        this.ajusteCw = ajusteCw;
    }

    public String getConceptoAjusteCx() {
        return conceptoAjusteCx;
    }

    public void setConceptoAjusteCx(String conceptoAjusteCx) {
        this.conceptoAjusteCx = conceptoAjusteCx;
    }

    public String getTipoIncumplimientoCy() {
        return tipoIncumplimientoCy;
    }

    public void setTipoIncumplimientoCy(String tipoIncumplimientoCy) {
        this.tipoIncumplimientoCy = tipoIncumplimientoCy;
    }

    public String getNombreCortoAdministradoraCz() {
        return nombreCortoAdministradoraCz;
    }

    public void setNombreCortoAdministradoraCz(String nombreCortoAdministradoraCz) {
        this.nombreCortoAdministradoraCz = nombreCortoAdministradoraCz;
    }

    public BigDecimal getIbcCoreDa() {
        return ibcCoreDa;
    }

    public void setIbcCoreDa(BigDecimal ibcCoreDa) {
        this.ibcCoreDa = ibcCoreDa;
    }

    public BigDecimal getTarifaCoreDb() {
        return tarifaCoreDb;
    }

    public void setTarifaCoreDb(BigDecimal tarifaCoreDb) {
        this.tarifaCoreDb = tarifaCoreDb;
    }

    public BigDecimal getCotizacionObligatoriaDc() {
        return cotizacionObligatoriaDc;
    }

    public void setCotizacionObligatoriaDc(BigDecimal cotizacionObligatoriaDc) {
        this.cotizacionObligatoriaDc = cotizacionObligatoriaDc;
    }

    public BigInteger getDiasCotizadosPilaDd() {
        return diasCotizadosPilaDd;
    }

    public void setDiasCotizadosPilaDd(BigInteger diasCotizadosPilaDd) {
        this.diasCotizadosPilaDd = diasCotizadosPilaDd;
    }

    public BigDecimal getIbcPilaDe() {
        return ibcPilaDe;
    }

    public void setIbcPilaDe(BigDecimal ibcPilaDe) {
        this.ibcPilaDe = ibcPilaDe;
    }

    public BigDecimal getTarifaPilaDf() {
        return tarifaPilaDf;
    }

    public void setTarifaPilaDf(BigDecimal tarifaPilaDf) {
        this.tarifaPilaDf = tarifaPilaDf;
    }

    public BigDecimal getCotizacionPagadaPilaDg() {
        return cotizacionPagadaPilaDg;
    }

    public void setCotizacionPagadaPilaDg(BigDecimal cotizacionPagadaPilaDg) {
        this.cotizacionPagadaPilaDg = cotizacionPagadaPilaDg;
    }

    public BigDecimal getAjusteDh() {
        return ajusteDh;
    }

    public void setAjusteDh(BigDecimal ajusteDh) {
        this.ajusteDh = ajusteDh;
    }

    public String getConceptoAjusteDi() {
        return conceptoAjusteDi;
    }

    public void setConceptoAjusteDi(String conceptoAjusteDi) {
        this.conceptoAjusteDi = conceptoAjusteDi;
    }

    public String getTipoIncumplimientoDj() {
        return tipoIncumplimientoDj;
    }

    public void setTipoIncumplimientoDj(String tipoIncumplimientoDj) {
        this.tipoIncumplimientoDj = tipoIncumplimientoDj;
    }

    public BigDecimal getIbcCoreDk() {
        return ibcCoreDk;
    }

    public void setIbcCoreDk(BigDecimal ibcCoreDk) {
        this.ibcCoreDk = ibcCoreDk;
    }

    public BigDecimal getTarifaCoreDl() {
        return tarifaCoreDl;
    }

    public void setTarifaCoreDl(BigDecimal tarifaCoreDl) {
        this.tarifaCoreDl = tarifaCoreDl;
    }

    public BigDecimal getAporteDm() {
        return aporteDm;
    }

    public void setAporteDm(BigDecimal aporteDm) {
        this.aporteDm = aporteDm;
    }

    public BigDecimal getTarifaPilaDn() {
        return tarifaPilaDn;
    }

    public void setTarifaPilaDn(BigDecimal tarifaPilaDn) {
        this.tarifaPilaDn = tarifaPilaDn;
    }

    public BigDecimal getAportePilaDo() {
        return aportePilaDo;
    }

    public void setAportePilaDo(BigDecimal aportePilaDo) {
        this.aportePilaDo = aportePilaDo;
    }

    public BigDecimal getAjusteDp() {
        return ajusteDp;
    }

    public void setAjusteDp(BigDecimal ajusteDp) {
        this.ajusteDp = ajusteDp;
    }

    public String getConceptoAjusteDq() {
        return conceptoAjusteDq;
    }

    public void setConceptoAjusteDq(String conceptoAjusteDq) {
        this.conceptoAjusteDq = conceptoAjusteDq;
    }

    public String getTipoIncumplimientoDr() {
        return tipoIncumplimientoDr;
    }

    public void setTipoIncumplimientoDr(String tipoIncumplimientoDr) {
        this.tipoIncumplimientoDr = tipoIncumplimientoDr;
    }

    public BigDecimal getIbcCoreDs() {
        return ibcCoreDs;
    }

    public void setIbcCoreDs(BigDecimal ibcCoreDs) {
        this.ibcCoreDs = ibcCoreDs;
    }

    public BigDecimal getTarifaCoreDt() {
        return tarifaCoreDt;
    }

    public void setTarifaCoreDt(BigDecimal tarifaCoreDt) {
        this.tarifaCoreDt = tarifaCoreDt;
    }

    public BigDecimal getAporteDu() {
        return aporteDu;
    }

    public void setAporteDu(BigDecimal aporteDu) {
        this.aporteDu = aporteDu;
    }

    public BigDecimal getTarifaPilaDv() {
        return tarifaPilaDv;
    }

    public void setTarifaPilaDv(BigDecimal tarifaPilaDv) {
        this.tarifaPilaDv = tarifaPilaDv;
    }

    public BigDecimal getAportePilaDw() {
        return aportePilaDw;
    }

    public void setAportePilaDw(BigDecimal aportePilaDw) {
        this.aportePilaDw = aportePilaDw;
    }

    public BigDecimal getAjusteDx() {
        return ajusteDx;
    }

    public void setAjusteDx(BigDecimal ajusteDx) {
        this.ajusteDx = ajusteDx;
    }

    public String getConceptoAjusteDy() {
        return conceptoAjusteDy;
    }

    public void setConceptoAjusteDy(String conceptoAjusteDy) {
        this.conceptoAjusteDy = conceptoAjusteDy;
    }

    public String getTipoIncumplimientoDz() {
        return tipoIncumplimientoDz;
    }

    public void setTipoIncumplimientoDz(String tipoIncumplimientoDz) {
        this.tipoIncumplimientoDz = tipoIncumplimientoDz;
    }

    public BigDecimal getIbcEa() {
        return ibcEa;
    }

    public void setIbcEa(BigDecimal ibcEa) {
        this.ibcEa = ibcEa;
    }

    public BigDecimal getTarifaCoreEb() {
        return tarifaCoreEb;
    }

    public void setTarifaCoreEb(BigDecimal tarifaCoreEb) {
        this.tarifaCoreEb = tarifaCoreEb;
    }

    public BigDecimal getAporteEc() {
        return aporteEc;
    }

    public void setAporteEc(BigDecimal aporteEc) {
        this.aporteEc = aporteEc;
    }

    public BigDecimal getTarifaPilaEd() {
        return tarifaPilaEd;
    }

    public void setTarifaPilaEd(BigDecimal tarifaPilaEd) {
        this.tarifaPilaEd = tarifaPilaEd;
    }

    public BigDecimal getAportePilaEe() {
        return aportePilaEe;
    }

    public void setAportePilaEe(BigDecimal aportePilaEe) {
        this.aportePilaEe = aportePilaEe;
    }

    public BigDecimal getAjusteEf() {
        return ajusteEf;
    }

    public void setAjusteEf(BigDecimal ajusteEf) {
        this.ajusteEf = ajusteEf;
    }

    public String getConceptoAjusteEg() {
        return conceptoAjusteEg;
    }

    public void setConceptoAjusteEg(String conceptoAjusteEg) {
        this.conceptoAjusteEg = conceptoAjusteEg;
    }

    public String getTipoIncumplimientoEh() {
        return tipoIncumplimientoEh;
    }

    public void setTipoIncumplimientoEh(String tipoIncumplimientoEh) {
        this.tipoIncumplimientoEh = tipoIncumplimientoEh;
    }

    public BigDecimal getIbcCoreEi() {
        return ibcCoreEi;
    }

    public void setIbcCoreEi(BigDecimal ibcCoreEi) {
        this.ibcCoreEi = ibcCoreEi;
    }

    public BigDecimal getTarifaCoreEj() {
        return tarifaCoreEj;
    }

    public void setTarifaCoreEj(BigDecimal tarifaCoreEj) {
        this.tarifaCoreEj = tarifaCoreEj;
    }

    public BigDecimal getAporteEk() {
        return aporteEk;
    }

    public void setAporteEk(BigDecimal aporteEk) {
        this.aporteEk = aporteEk;
    }

    public BigDecimal getTarifaPilaEl() {
        return tarifaPilaEl;
    }

    public void setTarifaPilaEl(BigDecimal tarifaPilaEl) {
        this.tarifaPilaEl = tarifaPilaEl;
    }

    public BigDecimal getAporteEm() {
        return aporteEm;
    }

    public void setAporteEm(BigDecimal aporteEm) {
        this.aporteEm = aporteEm;
    }

    public BigDecimal getAjusteEn() {
        return ajusteEn;
    }

    public void setAjusteEn(BigDecimal ajusteEn) {
        this.ajusteEn = ajusteEn;
    }

    public String getConceptoAjusteEo() {
        return conceptoAjusteEo;
    }

    public void setConceptoAjusteEo(String conceptoAjusteEo) {
        this.conceptoAjusteEo = conceptoAjusteEo;
    }

    public String getTipoIncumplimientoEp() {
        return tipoIncumplimientoEp;
    }

    public void setTipoIncumplimientoEp(String tipoIncumplimientoEp) {
        this.tipoIncumplimientoEp = tipoIncumplimientoEp;
    }

    public HojaCalculoLiquidacion getIdhojacalculoliquidacion() {
        return idhojacalculoliquidacion;
    }

    public void setIdhojacalculoliquidacion(HojaCalculoLiquidacion idhojacalculoliquidacion) {
        this.idhojacalculoliquidacion = idhojacalculoliquidacion;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        boolean result = true;
        if (!(object instanceof HojaCalculoLiquidacionDetalle)) {
            result = false;
        }
        HojaCalculoLiquidacionDetalle other = (HojaCalculoLiquidacionDetalle) object;
        if (this.id == null && other.id != null || this.id != null && !this.id.equals(other.id)) {
            result = false;
        }
        return result;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.entidades.negocio.HojaCalculoLiquidacionDetal[ id=" + id + " ]";
    }

    
    @XmlTransient
    public Collection<AportesIndependienteHclDetalle> getAportesIndependienteHclDetaCollection() {
        return aportesIndependienteHclDetaCollection;
    }

    public void setAportesIndependienteHclDetaCollection(Collection<AportesIndependienteHclDetalle> aportesIndependienteHclDetaCollection) {
        this.aportesIndependienteHclDetaCollection = aportesIndependienteHclDetaCollection;
    }

    @XmlTransient
    public Collection<ConceptoContableHclDetalle> getConceptoContableHclDetalleCollection() {
        return conceptoContableHclDetalleCollection;
    }

    public void setConceptoContableHclDetalleCollection(Collection<ConceptoContableHclDetalle> conceptoContableHclDetalleCollection) {
        this.conceptoContableHclDetalleCollection = conceptoContableHclDetalleCollection;
    }
    
    // DEFINICION DE NUEVOS CAMPOS DIC.14.2015

    public BigInteger getIbcVacaciones() {
        return ibcVacaciones;
    }

    public void setIbcVacaciones(BigInteger ibcVacaciones) {
        this.ibcVacaciones = ibcVacaciones;
    }

    public BigInteger getIbcHuelga() {
        return ibcHuelga;
    }

    public void setIbcHuelga(BigInteger ibcHuelga) {
        this.ibcHuelga = ibcHuelga;
    }

    public BigInteger getPagoNoSalarial() {
        return pagoNoSalarial;
    }

    public void setPagoNoSalarial(BigInteger pagoNoSalarial) {
        this.pagoNoSalarial = pagoNoSalarial;
    }

    public BigInteger getTotalRemunerado() {
        return totalRemunerado;
    }

    public void setTotalRemunerado(BigInteger totalRemunerado) {
        this.totalRemunerado = totalRemunerado;
    }

    public BigInteger getCotizOblCalculadaSalud() {
        return cotizOblCalculadaSalud;
    }

    public void setCotizOblCalculadaSalud(BigInteger cotizOblCalculadaSalud) {
        this.cotizOblCalculadaSalud = cotizOblCalculadaSalud;
    }

    public BigInteger getIbcPagosNomPension() {
        return ibcPagosNomPension;
    }

    public void setIbcPagosNomPension(BigInteger ibcPagosNomPension) {
        this.ibcPagosNomPension = ibcPagosNomPension;
    }

    public BigInteger getIbcCalculadoPension() {
        return ibcCalculadoPension;
    }

    public void setIbcCalculadoPension(BigInteger ibcCalculadoPension) {
        this.ibcCalculadoPension = ibcCalculadoPension;
    }

    public BigInteger getCotizOblPension() {
        return cotizOblPension;
    }

    public void setCotizOblPension(BigInteger cotizOblPension) {
        this.cotizOblPension = cotizOblPension;
    }

    public BigInteger getIbcPilaPension() {
        return ibcPilaPension;
    }

    public void setIbcPilaPension(BigInteger ibcPilaPension) {
        this.ibcPilaPension = ibcPilaPension;
    }

    public BigInteger getCotPagadaPilaPension() {
        return cotPagadaPilaPension;
    }

    public void setCotPagadaPilaPension(BigInteger cotPagadaPilaPension) {
        this.cotPagadaPilaPension = cotPagadaPilaPension;
    }

    public BigInteger getCotizOblFspSubSolidaridad() {
        return cotizOblFspSubSolidaridad;
    }

    public void setCotizOblFspSubSolidaridad(BigInteger cotizOblFspSubSolidaridad) {
        this.cotizOblFspSubSolidaridad = cotizOblFspSubSolidaridad;
    }

    public BigInteger getCotizOblFspSubSubsistencia() {
        return cotizOblFspSubSubsistencia;
    }

    public void setCotizOblFspSubSubsistencia(BigInteger cotizOblFspSubSubsistencia) {
        this.cotizOblFspSubSubsistencia = cotizOblFspSubSubsistencia;
    }

    public BigInteger getCotizPagPilaFspSubSolidar() {
        return cotizPagPilaFspSubSolidar;
    }

    public void setCotizPagPilaFspSubSolidar(BigInteger cotizPagPilaFspSubSolidar) {
        this.cotizPagPilaFspSubSolidar = cotizPagPilaFspSubSolidar;
    }

    public BigInteger getCotizPagPilaFspSubSubsis() {
        return cotizPagPilaFspSubSubsis;
    }

    public void setCotizPagPilaFspSubSubsis(BigInteger cotizPagPilaFspSubSubsis) {
        this.cotizPagPilaFspSubSubsis = cotizPagPilaFspSubSubsis;
    }

    public BigInteger getIbcPagosNomSalud() {
        return ibcPagosNomSalud;
    }

    public void setIbcPagosNomSalud(BigInteger ibcPagosNomSalud) {
        this.ibcPagosNomSalud = ibcPagosNomSalud;
    }

    
    public BigInteger getIbcPermisosRemunerados() {
        return ibcPermisosRemunerados;
    }

    public void setIbcPermisosRemunerados(BigInteger ibcPermisosRemunerados) {
        this.ibcPermisosRemunerados = ibcPermisosRemunerados;
    }

    public BigInteger getIbcSuspPermisos() {
        return ibcSuspPermisos;
    }

    public void setIbcSuspPermisos(BigInteger ibcSuspPermisos) {
        this.ibcSuspPermisos = ibcSuspPermisos;
    }
    
    public String getConceptoAjusteSalud()
   {
      return conceptoAjusteSalud;
   }

   public void setConceptoAjusteSalud(String conceptoAjusteSalud)
   {
      this.conceptoAjusteSalud = conceptoAjusteSalud;
   }

   public String getTipoIncumplimientoSalud()
   {
      return tipoIncumplimientoSalud;
   }

   public void setTipoIncumplimientoSalud(String tipoIncumplimientoSalud)
   {
      this.tipoIncumplimientoSalud = tipoIncumplimientoSalud;
   }

    public Integer getDiasCotPension() {
        return diasCotPension;
    }

    public void setDiasCotPension(Integer diasCotPension) {
        this.diasCotPension = diasCotPension;
    }

    public Integer getDiasCotSalud() {
        return diasCotSalud;
    }

    public void setDiasCotSalud(Integer diasCotSalud) {
        this.diasCotSalud = diasCotSalud;
    }

    public Integer getDiasCotRprof() {
        return diasCotRprof;
    }

    public void setDiasCotRprof(Integer diasCotRprof) {
        this.diasCotRprof = diasCotRprof;
    }

    public Integer getDiasCotCcf() {
        return diasCotCcf;
    }

    public void setDiasCotCcf(Integer diasCotCcf) {
        this.diasCotCcf = diasCotCcf;
    }

    public Integer getPorPagoNoSalarial() {
        return porPagoNoSalarial;
    }

    public void setPorPagoNoSalarial(Integer porPagoNoSalarial) {
        this.porPagoNoSalarial = porPagoNoSalarial;
    }

    public BigInteger getExcLimPagoNoSalarial() {
        return excLimPagoNoSalarial;
    }

    public void setExcLimPagoNoSalarial(BigInteger excLimPagoNoSalarial) {
        this.excLimPagoNoSalarial = excLimPagoNoSalarial;
    }

    public String getCodAdmSalud() {
        return codAdmSalud;
    }

    public void setCodAdmSalud(String codAdmSalud) {
        this.codAdmSalud = codAdmSalud;
    }

    public BigInteger getIbcCalculadoSalud() {
        return ibcCalculadoSalud;
    }

    public void setIbcCalculadoSalud(BigInteger ibcCalculadoSalud) {
        this.ibcCalculadoSalud = ibcCalculadoSalud;
    }

    public BigDecimal getTarifaSalud() {
        return tarifaSalud;
    }

    public void setTarifaSalud(BigDecimal tarifaSalud) {
        this.tarifaSalud = tarifaSalud;
    }

    public BigDecimal getTarifaSaludSuspension() {
        return tarifaSaludSuspension;
    }

    public void setTarifaSaludSuspension(BigDecimal tarifaSaludSuspension) {
        this.tarifaSaludSuspension = tarifaSaludSuspension;
    }

    public Integer getDiasCotizPilaSalud() {
        return diasCotizPilaSalud;
    }

    public void setDiasCotizPilaSalud(Integer diasCotizPilaSalud) {
        this.diasCotizPilaSalud = diasCotizPilaSalud;
    }

    public BigInteger getIbcPilaSalud() {
        return ibcPilaSalud;
    }

    public void setIbcPilaSalud(BigInteger ibcPilaSalud) {
        this.ibcPilaSalud = ibcPilaSalud;
    }

    public BigDecimal getTarifaPilaSalud() {
        return tarifaPilaSalud;
    }

    public void setTarifaPilaSalud(BigDecimal tarifaPilaSalud) {
        this.tarifaPilaSalud = tarifaPilaSalud;
    }

    public BigInteger getCotizPagadaPilaSalud() {
        return cotizPagadaPilaSalud;
    }

    public void setCotizPagadaPilaSalud(BigInteger cotizPagadaPilaSalud) {
        this.cotizPagadaPilaSalud = cotizPagadaPilaSalud;
    }

    public BigInteger getAjusteSalud() {
        return ajusteSalud;
    }

    public void setAjusteSalud(BigInteger ajusteSalud) {
        this.ajusteSalud = ajusteSalud;
    }

    public BigDecimal getTarifaPension() {
        return tarifaPension;
    }

    public void setTarifaPension(BigDecimal tarifaPension) {
        this.tarifaPension = tarifaPension;
    }

    public Integer getDiasCotizPilaPension() {
        return diasCotizPilaPension;
    }

    public void setDiasCotizPilaPension(Integer diasCotizPilaPension) {
        this.diasCotizPilaPension = diasCotizPilaPension;
    }

    public BigDecimal getTarifaPilaPension() {
        return tarifaPilaPension;
    }

    public void setTarifaPilaPension(BigDecimal tarifaPilaPension) {
        this.tarifaPilaPension = tarifaPilaPension;
    }

    public BigInteger getAjustePension() {
        return ajustePension;
    }

    public void setAjustePension(BigInteger ajustePension) {
        this.ajustePension = ajustePension;
    }

    public Integer getTarifaFspSubcuenSolidaridad() {
        return tarifaFspSubcuenSolidaridad;
    }

    public void setTarifaFspSubcuenSolidaridad(Integer tarifaFspSubcuenSolidaridad) {
        this.tarifaFspSubcuenSolidaridad = tarifaFspSubcuenSolidaridad;
    }

    public Integer getTarifaFspSubcuenSubsisten() {
        return tarifaFspSubcuenSubsisten;
    }

    public void setTarifaFspSubcuenSubsisten(Integer tarifaFspSubcuenSubsisten) {
        this.tarifaFspSubcuenSubsisten = tarifaFspSubcuenSubsisten;
    }

    public BigInteger getAjusteFspSubcuenSolidaridad() {
        return ajusteFspSubcuenSolidaridad;
    }

    public void setAjusteFspSubcuenSolidaridad(BigInteger ajusteFspSubcuenSolidaridad) {
        this.ajusteFspSubcuenSolidaridad = ajusteFspSubcuenSolidaridad;
    }

    public BigInteger getAjusteFspSubcuenSubsisten() {
        return ajusteFspSubcuenSubsisten;
    }

    public void setAjusteFspSubcuenSubsisten(BigInteger ajusteFspSubcuenSubsisten) {
        this.ajusteFspSubcuenSubsisten = ajusteFspSubcuenSubsisten;
    }

    public BigInteger getCalculoActuarial() {
        return calculoActuarial;
    }

    public void setCalculoActuarial(BigInteger calculoActuarial) {
        this.calculoActuarial = calculoActuarial;
    }

    public String getCodAdmArl() {
        return codAdmArl;
    }

    public void setCodAdmArl(String codAdmArl) {
        this.codAdmArl = codAdmArl;
    }

    public BigInteger getIbcArl() {
        return ibcArl;
    }

    public void setIbcArl(BigInteger ibcArl) {
        this.ibcArl = ibcArl;
    }

    public BigDecimal getTarifaArl() {
        return tarifaArl;
    }

    public void setTarifaArl(BigDecimal tarifaArl) {
        this.tarifaArl = tarifaArl;
    }

    public BigInteger getCotizOblArl() {
        return cotizOblArl;
    }

    public void setCotizOblArl(BigInteger cotizOblArl) {
        this.cotizOblArl = cotizOblArl;
    }

    public BigInteger getDiasCotizPilaArl() {
        return diasCotizPilaArl;
    }

    public void setDiasCotizPilaArl(BigInteger diasCotizPilaArl) {
        this.diasCotizPilaArl = diasCotizPilaArl;
    }

    public BigInteger getIbcPilaArl() {
        return ibcPilaArl;
    }

    public void setIbcPilaArl(BigInteger ibcPilaArl) {
        this.ibcPilaArl = ibcPilaArl;
    }

    public BigDecimal getTarifaPilaArl() {
        return tarifaPilaArl;
    }

    public void setTarifaPilaArl(BigDecimal tarifaPilaArl) {
        this.tarifaPilaArl = tarifaPilaArl;
    }

    public BigInteger getCotizPagadaPilaArl() {
        return cotizPagadaPilaArl;
    }

    public void setCotizPagadaPilaArl(BigInteger cotizPagadaPilaArl) {
        this.cotizPagadaPilaArl = cotizPagadaPilaArl;
    }

    public BigInteger getAjusteArl() {
        return ajusteArl;
    }

    public void setAjusteArl(BigInteger ajusteArl) {
        this.ajusteArl = ajusteArl;
    }

    public String getConceptoAjusteArl() {
        return conceptoAjusteArl;
    }

    public void setConceptoAjusteArl(String conceptoAjusteArl) {
        this.conceptoAjusteArl = conceptoAjusteArl;
    }

    public String getTipoIncumplimientoArl() {
        return tipoIncumplimientoArl;
    }

    public void setTipoIncumplimientoArl(String tipoIncumplimientoArl) {
        this.tipoIncumplimientoArl = tipoIncumplimientoArl;
    }

    public String getCodAdmCcf() {
        return codAdmCcf;
    }

    public void setCodAdmCcf(String codAdmCcf) {
        this.codAdmCcf = codAdmCcf;
    }

    public BigInteger getIbcCcf() {
        return ibcCcf;
    }

    public void setIbcCcf(BigInteger ibcCcf) {
        this.ibcCcf = ibcCcf;
    }

    public BigDecimal getTarifaCcf() {
        return tarifaCcf;
    }

    public void setTarifaCcf(BigDecimal tarifaCcf) {
        this.tarifaCcf = tarifaCcf;
    }

    public BigInteger getCotizOblCcf() {
        return cotizOblCcf;
    }

    public void setCotizOblCcf(BigInteger cotizOblCcf) {
        this.cotizOblCcf = cotizOblCcf;
    }

    public BigInteger getDiasCotiPilaCcf() {
        return diasCotiPilaCcf;
    }

    public void setDiasCotiPilaCcf(BigInteger diasCotiPilaCcf) {
        this.diasCotiPilaCcf = diasCotiPilaCcf;
    }

    public BigInteger getIbcPilaCcf() {
        return ibcPilaCcf;
    }

    public void setIbcPilaCcf(BigInteger ibcPilaCcf) {
        this.ibcPilaCcf = ibcPilaCcf;
    }

    public BigDecimal getTarifaPilaCcf() {
        return tarifaPilaCcf;
    }

    public void setTarifaPilaCcf(BigDecimal tarifaPilaCcf) {
        this.tarifaPilaCcf = tarifaPilaCcf;
    }

    public BigInteger getCotizPagadaPilaCcf() {
        return cotizPagadaPilaCcf;
    }

    public void setCotizPagadaPilaCcf(BigInteger cotizPagadaPilaCcf) {
        this.cotizPagadaPilaCcf = cotizPagadaPilaCcf;
    }

    public BigInteger getAjusteCcf() {
        return ajusteCcf;
    }

    public void setAjusteCcf(BigInteger ajusteCcf) {
        this.ajusteCcf = ajusteCcf;
    }

    public String getConceptoAjusteCcf() {
        return conceptoAjusteCcf;
    }

    public void setConceptoAjusteCcf(String conceptoAjusteCcf) {
        this.conceptoAjusteCcf = conceptoAjusteCcf;
    }

    public String getTipoIncumplimientoCcf() {
        return tipoIncumplimientoCcf;
    }

    public void setTipoIncumplimientoCcf(String tipoIncumplimientoCcf) {
        this.tipoIncumplimientoCcf = tipoIncumplimientoCcf;
    }

    public BigInteger getIbcSena() {
        return ibcSena;
    }

    public void setIbcSena(BigInteger ibcSena) {
        this.ibcSena = ibcSena;
    }

    public BigDecimal getTarifaSena() {
        return tarifaSena;
    }

    public void setTarifaSena(BigDecimal tarifaSena) {
        this.tarifaSena = tarifaSena;
    }

    public BigInteger getCotizOblSena() {
        return cotizOblSena;
    }

    public void setCotizOblSena(BigInteger cotizOblSena) {
        this.cotizOblSena = cotizOblSena;
    }

    public BigDecimal getTarifaPilaSena() {
        return tarifaPilaSena;
    }

    public void setTarifaPilaSena(BigDecimal tarifaPilaSena) {
        this.tarifaPilaSena = tarifaPilaSena;
    }

    public BigInteger getCotizPagadaPilaSena() {
        return cotizPagadaPilaSena;
    }

    public void setCotizPagadaPilaSena(BigInteger cotizPagadaPilaSena) {
        this.cotizPagadaPilaSena = cotizPagadaPilaSena;
    }

    public String getConceptoAjusteSena() {
        return conceptoAjusteSena;
    }

    public void setConceptoAjusteSena(String conceptoAjusteSena) {
        this.conceptoAjusteSena = conceptoAjusteSena;
    }

    public String getTipoIncumplimientoSena() {
        return tipoIncumplimientoSena;
    }

    public void setTipoIncumplimientoSena(String tipoIncumplimientoSena) {
        this.tipoIncumplimientoSena = tipoIncumplimientoSena;
    }

    public BigInteger getIbcIcbf() {
        return ibcIcbf;
    }

    public void setIbcIcbf(BigInteger ibcIcbf) {
        this.ibcIcbf = ibcIcbf;
    }

    public BigDecimal getTarifaIcbf() {
        return tarifaIcbf;
    }

    public void setTarifaIcbf(BigDecimal tarifaIcbf) {
        this.tarifaIcbf = tarifaIcbf;
    }

    public BigInteger getCotizOblIcbf() {
        return cotizOblIcbf;
    }

    public void setCotizOblIcbf(BigInteger cotizOblIcbf) {
        this.cotizOblIcbf = cotizOblIcbf;
    }

    public BigDecimal getTarifaPilaIcbf() {
        return tarifaPilaIcbf;
    }

    public void setTarifaPilaIcbf(BigDecimal tarifaPilaIcbf) {
        this.tarifaPilaIcbf = tarifaPilaIcbf;
    }

    public BigInteger getCotizPagadaPilaIcbf() {
        return cotizPagadaPilaIcbf;
    }

    public void setCotizPagadaPilaIcbf(BigInteger cotizPagadaPilaIcbf) {
        this.cotizPagadaPilaIcbf = cotizPagadaPilaIcbf;
    }

    public BigInteger getAjusteIcbf() {
        return ajusteIcbf;
    }

    public void setAjusteIcbf(BigInteger ajusteIcbf) {
        this.ajusteIcbf = ajusteIcbf;
    }

    public String getConceptoAjusteIcbf() {
        return conceptoAjusteIcbf;
    }

    public void setConceptoAjusteIcbf(String conceptoAjusteIcbf) {
        this.conceptoAjusteIcbf = conceptoAjusteIcbf;
    }

    public String getTipoIncumplimientoIcbf() {
        return tipoIncumplimientoIcbf;
    }

    public void setTipoIncumplimientoIcbf(String tipoIncumplimientoIcbf) {
        this.tipoIncumplimientoIcbf = tipoIncumplimientoIcbf;
    }

    public String getPlanillaPilaCargada() {
        return planillaPilaCargada;
    }

    public void setPlanillaPilaCargada(String planillaPilaCargada) {
        this.planillaPilaCargada = planillaPilaCargada;
    }
   
   

}
