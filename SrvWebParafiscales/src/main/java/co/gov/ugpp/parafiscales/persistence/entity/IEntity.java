package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;

/**
 *
 * @author rpadilla
 * @param <PK> Tipo del Primary Key
 */
public interface IEntity<PK extends Serializable> extends Serializable {

    PK getId();
    
}
