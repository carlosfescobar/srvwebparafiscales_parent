package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.parafiscales.procesos.solicitarinformacion.RespuestaInformacionFacadeImpl;
import java.io.Serializable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
public class Identificacion implements Serializable {
    
    private static final Logger LOG = LoggerFactory.getLogger(Identificacion.class);

    private static final long serialVersionUID = 1L;
    
    private String codTipoIdentificacion;
    private String valNumeroIdentificacion;
    private Municipio municipio;

    public Identificacion() {
    }

    public Identificacion(String codTipoIdentificacion, String valNumeroDocumento) {
        this.codTipoIdentificacion = codTipoIdentificacion;
        this.valNumeroIdentificacion = valNumeroDocumento;
    }

    public String getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(String codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public String getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    public void setValNumeroIdentificacion(String valNumeroIdentificacion) {
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.codTipoIdentificacion != null ? this.codTipoIdentificacion.hashCode() : 0);
        hash = 59 * hash + (this.valNumeroIdentificacion != null ? this.valNumeroIdentificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        
        final Identificacion other = (Identificacion) obj;
        
        if ((this.codTipoIdentificacion == null) ? (other.codTipoIdentificacion != null) : !this.codTipoIdentificacion.equals(other.codTipoIdentificacion)) {
            return false;
        }
        if ((this.valNumeroIdentificacion == null) ? (other.valNumeroIdentificacion != null) : !this.valNumeroIdentificacion.equals(other.valNumeroIdentificacion)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Identificacion{" + "codTipoIdentificacion=" + codTipoIdentificacion + ", valNumeroIdentificacion=" + valNumeroIdentificacion + '}';
    }
    
}
