package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "infoExternaDef.findByRadicado",query = "SELECT i FROM InformacionExternaDefiniti i WHERE i.envioInformacionExterna.idRadicadoEnvio = :idRadicado"),
    @NamedQuery(name = "infoExternaDef.findByRadicadoFirst",query = "SELECT i FROM InformacionExternaDefiniti i WHERE i.envioInformacionExterna.idRadicadoEnvio = :idRadicado ORDER BY i.fechaCreacion DESC")
})

@Table(name = "INFORMACION_EXTERNA_DEFINITI")
public class InformacionExternaDefiniti extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "infoExternaIdSeq", sequenceName = "info_externa_def_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "infoExternaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RADICADO", referencedColumnName = "ID_RADICADO_ENVIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private EnvioInformacionExterna envioInformacionExterna;
    @Column(name = "VAL_NOMBRE_ARCHIVO")
    private String valNombreArchivo;
    @Lob
    @Column(name = "VAL_CONTENIDO_ARCHIVO")
    private byte[] valContenidoArchivo;
    @Column(name = "FEC_COLOCACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecColocacion;
    @Lob
        @Column(name = "VAL_CONTENIDO_FIRMA")
    private byte[] valContenidoFirma;
    @Size(max = 50)
        @Column(name = "ID_USUARIO_COLOCACION")
    private String idUsuarioColocacion;

    public InformacionExternaDefiniti() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public EnvioInformacionExterna getEnvioInformacionExterna() {
        return envioInformacionExterna;
    }

    public void setEnvioInformacionExterna(EnvioInformacionExterna envioInformacionExterna) {
        this.envioInformacionExterna = envioInformacionExterna;
    }

    public String getValNombreArchivo() {
        return valNombreArchivo;
    }

    public void setValNombreArchivo(String valNombreArchivo) {
        this.valNombreArchivo = valNombreArchivo;
    }

    public byte[] getValContenidoArchivo() {
        return valContenidoArchivo;
    }

    public void setValContenidoArchivo(byte[] valContenidoArchivo) {
        this.valContenidoArchivo = valContenidoArchivo;
    }

    public Calendar getFecColocacion() {
        return fecColocacion;
    }

    public void setFecColocacion(Calendar fecColocacion) {
        this.fecColocacion = fecColocacion;
    }

    public byte[] getValContenidoFirma() {
        return valContenidoFirma;
    }

    public void setValContenidoFirma(byte[] valContenidoFirma) {
        this.valContenidoFirma = valContenidoFirma;
    }

    public String getIdUsuarioColocacion() {
        return idUsuarioColocacion;
    }

    public void setIdUsuarioColocacion(String idUsuarioColocacion) {
        this.idUsuarioColocacion = idUsuarioColocacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InformacionExternaDefiniti)) {
            return false;
        }
        InformacionExternaDefiniti other = (InformacionExternaDefiniti) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.InformacionExtrernaDefiniti[ id=" + id + " ]";
    }

}
