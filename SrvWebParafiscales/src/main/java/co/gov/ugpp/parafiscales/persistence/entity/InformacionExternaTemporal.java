package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "informacionExternaTemp.findByCriterios",
            query = "SELECT s FROM InformacionExternaTemporal s WHERE s.numeroDocumento = :numeroDocumento "
                    + "AND s.codTipoIdentificacion.id = :tipoDocumento AND s.formatoArchivoPK.idFormato = :idFormato "
                    + "AND s.formatoArchivoPK.idVersion = :idVersion AND s.valNombreArchivo IN :archivos"),
    @NamedQuery(name = "informacionExternaTemp.findByArchivoAndEntidadExterna",
            query = "SELECT s FROM InformacionExternaTemporal s WHERE s.numeroDocumento = :numeroDocumento "
                    + "AND s.codTipoIdentificacion.id = :tipoDocumento AND s.valNombreArchivo = :nombreArchivo")})
@Table(name = "INFORMACION_EXTERNA_TEMPORAL")
public class InformacionExternaTemporal extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "informacionEntidadExternaTempIdSeq", sequenceName = "informacion_externa_tem_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "informacionEntidadExternaTempIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Size(max = 30)
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private String numeroDocumento;
    @Size(max = 100)
    @Column(name = "VAL_NOMBRE_ARCHIVO")
    private String valNombreArchivo;
    @Lob
    @Column(name = "VAL_CONTENIDO_ARCHIVO")
    private byte[] valContenidoArchivo;
    @Column(name = "FEC_COLOCACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecColocacion;
    @Size(max = 100)
    @Column(name = "ID_USUARIO_COLOCACION")
    private String idUsuarioColocacion;
    @Lob
    @Column(name = "VAL_CONTENIDO_FIRMA")
    private byte[] valContenidoFirma;
    @JoinColumn(name = "COD_TIPO_IDENTIFICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoIdentificacion;
    @Embedded
    private FormatoArchivoPK formatoArchivoPK;

    public InformacionExternaTemporal() {
    }

    public InformacionExternaTemporal(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(String numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getValNombreArchivo() {
        return valNombreArchivo;
    }

    public void setValNombreArchivo(String valNombreArchivo) {
        this.valNombreArchivo = valNombreArchivo;
    }

    public byte[] getValContenidoArchivo() {
        return valContenidoArchivo;
    }

    public void setValContenidoArchivo(byte[] valContenidoArchivo) {
        this.valContenidoArchivo = valContenidoArchivo;
    }

    public Calendar getFecColocacion() {
        return fecColocacion;
    }

    public void setFecColocacion(Calendar fecColocacion) {
        this.fecColocacion = fecColocacion;
    }

    public String getIdUsuarioColocacion() {
        return idUsuarioColocacion;
    }

    public void setIdUsuarioColocacion(String idUsuarioColocacion) {
        this.idUsuarioColocacion = idUsuarioColocacion;
    }

    public byte[] getValContenidoFirma() {
        return valContenidoFirma;
    }

    public void setValContenidoFirma(byte[] valContenidoFirma) {
        this.valContenidoFirma = valContenidoFirma;
    }

    public ValorDominio getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(ValorDominio codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public FormatoArchivoPK getFormatoArchivoPK() {
        return formatoArchivoPK;
    }

    public void setFormatoArchivoPK(FormatoArchivoPK formatoArchivoPK) {
        this.formatoArchivoPK = formatoArchivoPK;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InformacionExternaTemporal)) {
            return false;
        }
        InformacionExternaTemporal other = (InformacionExternaTemporal) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.InformacionExternaTemporal[ id=" + id + " ]";
    }

}
