package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "INFORMACION_PRUEBA")
public class InformacionPrueba extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "informacionPruebaIdSeq", sequenceName = "informacion_prueba_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "informacionPruebaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 50)
    @Column(name = "ES_REQUERIMIENTO_INFORMACION")
    private Boolean esRequerimientoInformacion;
    @JoinColumn(name = "COD_TIPO_REQUERIMIENTO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoRequerimiento;
    @Column(name = "VAL_AREA_SELECCIONADA")
    private String valAreaSeleccionada;
    @Column(name = "DESC_INFO_REQUERIDA")
    private String desInformacionRequerida;
    @JoinColumn(name = "COD_PERSONA_SOLICITUD_EXTERNA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codPersonaSolicitudExterna;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EntidadExterna entidadExterna;
    @Column(name = "VAL_FUNCIONARIO_RESUELVE_INT")
    private String funcionarioResuelveInterna;
    @Column(name = "DESC_RESP_INFORMACION")
    private String desRespuestaInformacion;
    @Column(name = "ES_INFORMACION_EXT_COMPLETA")
    private Boolean esInformacionExternaCompleta;
    @JoinColumn(name = "ID_REQUERIMIENTO_INFO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento requerimientoInformacionParcial;
    @JoinColumn(name = "ID_ACTO_REQUERIMIENTO_INFO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoRequerimientoInformacion;
    @JoinColumn(name = "ID_INFORMACION_PRUEBA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<InformacionPruebaDocumentos> idDocumentosRespuestaInterna;

    public InformacionPrueba() {
    }

    public InformacionPrueba(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getEsRequerimientoInformacion() {
        return esRequerimientoInformacion;
    }

    public void setEsRequerimientoInformacion(Boolean esRequerimientoInformacion) {
        this.esRequerimientoInformacion = esRequerimientoInformacion;
    }

    public ValorDominio getCodTipoRequerimiento() {
        return codTipoRequerimiento;
    }

    public void setCodTipoRequerimiento(ValorDominio codTipoRequerimiento) {
        this.codTipoRequerimiento = codTipoRequerimiento;
    }

    public String getValAreaSeleccionada() {
        return valAreaSeleccionada;
    }

    public void setValAreaSeleccionada(String valAreaSeleccionada) {
        this.valAreaSeleccionada = valAreaSeleccionada;
    }

    public String getDesInformacionRequerida() {
        return desInformacionRequerida;
    }

    public void setDesInformacionRequerida(String desInformacionRequerida) {
        this.desInformacionRequerida = desInformacionRequerida;
    }

    public ValorDominio getCodPersonaSolicitudExterna() {
        return codPersonaSolicitudExterna;
    }

    public void setCodPersonaSolicitudExterna(ValorDominio codPersonaSolicitudExterna) {
        this.codPersonaSolicitudExterna = codPersonaSolicitudExterna;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public String getFuncionarioResuelveInterna() {
        return funcionarioResuelveInterna;
    }

    public void setFuncionarioResuelveInterna(String funcionarioResuelveInterna) {
        this.funcionarioResuelveInterna = funcionarioResuelveInterna;
    }

    public String getDesRespuestaInformacion() {
        return desRespuestaInformacion;
    }

    public void setDesRespuestaInformacion(String desRespuestaInformacion) {
        this.desRespuestaInformacion = desRespuestaInformacion;
    }

    public Boolean getEsInformacionExternaCompleta() {
        return esInformacionExternaCompleta;
    }

    public void setEsInformacionExternaCompleta(Boolean esInformacionExternaCompleta) {
        this.esInformacionExternaCompleta = esInformacionExternaCompleta;
    }

    public AprobacionDocumento getRequerimientoInformacionParcial() {
        return requerimientoInformacionParcial;
    }

    public void setRequerimientoInformacionParcial(AprobacionDocumento requerimientoInformacionParcial) {
        this.requerimientoInformacionParcial = requerimientoInformacionParcial;
    }

    public ActoAdministrativo getIdActoRequerimientoInformacion() {
        return idActoRequerimientoInformacion;
    }

    public void setIdActoRequerimientoInformacion(ActoAdministrativo idActoRequerimientoInformacion) {
        this.idActoRequerimientoInformacion = idActoRequerimientoInformacion;
    }

    public List<InformacionPruebaDocumentos> getIdDocumentosRespuestaInterna() {
        if (idDocumentosRespuestaInterna == null) {
            idDocumentosRespuestaInterna = new ArrayList<InformacionPruebaDocumentos>();
        }
        return idDocumentosRespuestaInterna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InformacionPrueba)) {
            return false;
        }
        InformacionPrueba other = (InformacionPrueba) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.InformacionPrueba[ id=" + id + " ]";
    }

}
