package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "INFORMACION_PRUEBA_DOCUMENTOS")

@NamedQueries({
    @NamedQuery(name = "informacionPruebaDocumentosRespuesta.findByInformacionPruebaAndDocumento",
            query = "SELECT ipd FROM InformacionPruebaDocumentos ipd WHERE ipd.documento.id = :iddocumento AND ipd.informacionPrueba.id = :idinformacionPrueba")
})
public class InformacionPruebaDocumentos extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "informPruebaDocumentoIdSeq", sequenceName = "inf_prueba_doc_resp_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "informPruebaDocumentoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_DOCUMENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DocumentoEcm documento;
    @JoinColumn(name = "ID_INFORMACION_PRUEBA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private InformacionPrueba informacionPrueba;

    public InformacionPruebaDocumentos() {
    }

    public DocumentoEcm getDocumento() {
        return documento;
    }

    public void setDocumento(DocumentoEcm documento) {
        this.documento = documento;
    }

    public InformacionPrueba getInformacionPrueba() {
        return informacionPrueba;
    }

    public void setInformacionPrueba(InformacionPrueba informacionPrueba) {
        this.informacionPrueba = informacionPrueba;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InformacionPruebaDocumentos)) {
            return false;
        }
        InformacionPruebaDocumentos other = (InformacionPruebaDocumentos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.InformacionPruebaDocumentosRespuesta[ id=" + id + " ]";
    }
}
