package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jgonzalezt
 */
@Entity
@Table(name = "INSTANCIA_EJECUCION_VALIDACION")
public class InstanciaEjecucionValidacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "instanciaejecucionSeq", sequenceName = "INSTANCIA_EJECUCION_VALIDA_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "instanciaejecucionSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Size(max = 100)
    @Column(name = "VAL_NOMBRE_ARCHIVO")
    private String valNombreArchivo;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION", referencedColumnName = "ID_VERSION")
    })
    private Formato formato;

    public InstanciaEjecucionValidacion() {
    }

    public InstanciaEjecucionValidacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    //protected void setId(Long id) {
    public void setId(Long id) {
        this.id = id;
    }

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    public String getValNombreArchivo() {
        return valNombreArchivo;
    }

    public void setValNombreArchivo(String valNombreArchivo) {
        this.valNombreArchivo = valNombreArchivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InstanciaEjecucionValidacion)) {
            return false;
        }
        InstanciaEjecucionValidacion other = (InstanciaEjecucionValidacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "InstanciaEjecucionValidacion[ idEjecucionValidacion=" + id + " ]";
    }

}
