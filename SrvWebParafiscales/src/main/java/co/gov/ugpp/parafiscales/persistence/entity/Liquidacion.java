package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author gchavezm
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "liquidacion.findByExpediente",
            query = "SELECT f FROM Liquidacion f WHERE f.expediente.id = :idExpediente"),})

@Table(name = "LIQUIDACION")
public class Liquidacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "liquidacionIdSeq", sequenceName = "liquidacion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "liquidacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Expediente expediente;
    @JoinColumn(name = "ID_LIQUIDACION_OFICIA_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento liquidacionOficialParcial;
    @JoinColumn(name = "ID_AUTO_ARCHIVO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento autoArchivoParcial;
    @Column(name = "ES_AMPLIADO_REQUERIMIENTO")
    private Boolean esAmpliadoRequerimiento;
    @Column(name = "ES_DETERMINA_DEUDA")
    private Boolean esDeterminaDeuda;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Aportante aportante;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ConstanciaEjecutoria idDocumentoConstanciaEjecutoria;
    @Column(name = "ES_CONFIRMADA_DEUDA")
    private Boolean esConfirmadaDeuda;
    @Column(name = "ES_RECIBIDO_RECURSO")
    private Boolean esRecibidoRecursoReconsideracion;
    @JoinColumn(name = "ID_ACTO_AUTO_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ActoAdministrativo idActoAutoArchivo;
    @JoinColumn(name = "ID_ACTO_LIQUIDACION_OFICIAL", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ActoAdministrativo idActoLiquidacionOficial;
    @JoinColumn(name = "ID_ACTO_REQ_DEC_CORR_AMPLIADO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ActoAdministrativo idActoRequerimientoDeclararCorregirAmpliado;
    @JoinColumn(name = "ID_REQ_DEC_AMPLIADO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private AprobacionDocumento requerimientoDeclararAmpliadoParcial;
    @Column(name = "DESC_OBS_AMPLICACION_REQ")
    private String descObservacionesAmpliacionRequerimiento;
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Fiscalizacion idFiscalizacion;
    @JoinColumn(name = "ID_LIQUIDACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<LiquidacionAccion> acciones;
    @JoinColumn(name = "ID_LIQUIDADOR_LIQUIDACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlLiquidador idLiquidadorLiquidacion;
    @JoinColumn(name = "ID_LIQUIDADOR_LIQ_AMPLIADA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlLiquidador idLiquidadorLiquidacionAmpliada;
    @JoinColumn(name = "COD_ESTADO_ENVIO_MEMORANDO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoEnvioMemorandoCobro;
    @Column(name = "VAL_LIQUIDADOR")
    private String liquidador;
    @Column(name = "VAL_SUBDIRECTOR_COBRANZAS")
    private String SubdirectorCobranzas;
    @Column(name = "ES_TODOS_EMPLEADOS_AFILIADOS")
    private Boolean esTodosEmpleadosAfiliados;
    @Column(name = "DESC_OBS_AFILIACION")
    private String descObservacionesAfiliaciones;
    @JoinColumn(name = "ID_LIQUIDACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AgrupacionAfiliacionesPorEntidad> agrupacionAfiliacionesEntidad;
    @JoinColumn(name = "COD_ESTADO_LIQUIDACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoLiquidacion;
    @Column(name = "FEC_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicio;
    @Column(name = "FEC_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFin;
    @JoinColumn(name = "COD_CAUSAL_CIERRE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codCausalCierre;
    @Column(name = "VAL_USUARIO_GENERA_ACTO")
    private String valUsuarioGeneraActo;

    public Liquidacion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public AprobacionDocumento getLiquidacionOficialParcial() {
        return liquidacionOficialParcial;
    }

    public void setLiquidacionOficialParcial(AprobacionDocumento liquidacionOficialParcial) {
        this.liquidacionOficialParcial = liquidacionOficialParcial;
    }

    public AprobacionDocumento getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    public void setAutoArchivoParcial(AprobacionDocumento autoArchivoParcial) {
        this.autoArchivoParcial = autoArchivoParcial;

    }

    public Boolean getEsAmpliadoRequerimiento() {
        return esAmpliadoRequerimiento;
    }

    public void setEsAmpliadoRequerimiento(Boolean esAmpliadoRequerimiento) {
        this.esAmpliadoRequerimiento = esAmpliadoRequerimiento;
    }

    public Boolean getEsDeterminaDeuda() {
        return esDeterminaDeuda;
    }

    public void setEsDeterminaDeuda(Boolean esDeterminaDeuda) {
        this.esDeterminaDeuda = esDeterminaDeuda;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public ConstanciaEjecutoria getidDocumentoConstanciaEjecutoria() {
        return idDocumentoConstanciaEjecutoria;
    }

    public void setidDocumentoConstanciaEjecutoria(ConstanciaEjecutoria idDocumentoConstanciaEjecutoria) {
        this.idDocumentoConstanciaEjecutoria = idDocumentoConstanciaEjecutoria;
    }

    public Boolean getEsConfirmadaDeuda() {
        return esConfirmadaDeuda;
    }

    public void setEsConfirmadaDeuda(Boolean esConfirmadaDeuda) {
        this.esConfirmadaDeuda = esConfirmadaDeuda;
    }

    public Boolean getEsRecibidoRecursoReconsideracion() {
        return esRecibidoRecursoReconsideracion;
    }

    public void setEsRecibidoRecursoReconsideracion(Boolean esRecibidoRecursoReconsideracion) {
        this.esRecibidoRecursoReconsideracion = esRecibidoRecursoReconsideracion;
    }

    public ActoAdministrativo getidActoAutoArchivo() {
        return idActoAutoArchivo;
    }

    public void setidActoAutoArchivo(ActoAdministrativo idActoAutoArchivo) {
        this.idActoAutoArchivo = idActoAutoArchivo;
    }

    public ActoAdministrativo getidActoLiquidacionOficial() {
        return idActoLiquidacionOficial;
    }

    public void setidActoLiquidacionOficial(ActoAdministrativo idActoLiquidacionOficial) {
        this.idActoLiquidacionOficial = idActoLiquidacionOficial;
    }

    public ActoAdministrativo getIdActoRequerimientoDeclararCorregirAmpliado() {
        return idActoRequerimientoDeclararCorregirAmpliado;
    }

    public void setidActoRequerimientoDeclararCorregirAmpliado(ActoAdministrativo idActoRequerimientoDeclararCorregirAmpliado) {
        this.idActoRequerimientoDeclararCorregirAmpliado = idActoRequerimientoDeclararCorregirAmpliado;
    }

    public Boolean getEsTodosEmpleadosAfiliados() {
        return esTodosEmpleadosAfiliados;
    }

    public void setEsTodosEmpleadosAfiliados(Boolean esTodosEmpleadosAfiliados) {
        this.esTodosEmpleadosAfiliados = esTodosEmpleadosAfiliados;
    }

    public String getDescObservacionesAmpliacionRequerimiento() {
        return descObservacionesAmpliacionRequerimiento;
    }

    public void setDescObservacionesAmpliacionRequerimiento(String descObservacionesAmpliacionRequerimiento) {
        this.descObservacionesAmpliacionRequerimiento = descObservacionesAmpliacionRequerimiento;
    }

    public List<AgrupacionAfiliacionesPorEntidad> getAgrupacionAfiliacionesEntidad() {
        if (agrupacionAfiliacionesEntidad == null) {
            agrupacionAfiliacionesEntidad = new ArrayList<AgrupacionAfiliacionesPorEntidad>();
        }
        return agrupacionAfiliacionesEntidad;
    }

    public Fiscalizacion getidFiscalizacion() {
        return idFiscalizacion;
    }

    public void setidFiscalizacion(Fiscalizacion idFiscalizacion) {
        this.idFiscalizacion = idFiscalizacion;
    }

    public List<LiquidacionAccion> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<LiquidacionAccion>();
        }
        return acciones;
    }

    public ControlLiquidador getidLiquidadorLiquidacion() {
        return idLiquidadorLiquidacion;
    }

    public void setidLiquidadorLiquidacion(ControlLiquidador idLiquidadorLiquidacion) {
        this.idLiquidadorLiquidacion = idLiquidadorLiquidacion;
    }

    public ControlLiquidador getidLiquidadorLiquidacionAmpliada() {
        return idLiquidadorLiquidacionAmpliada;
    }

    public void setidLiquidadorLiquidacionAmpliada(ControlLiquidador idLiquidadorLiquidacionAmpliada) {
        this.idLiquidadorLiquidacionAmpliada = idLiquidadorLiquidacionAmpliada;
    }

    public ValorDominio getCodEstadoEnvioMemorandoCobro() {
        return codEstadoEnvioMemorandoCobro;
    }

    public void setCodEstadoEnvioMemorandoCobro(ValorDominio codEstadoEnvioMemorandoCobro) {
        this.codEstadoEnvioMemorandoCobro = codEstadoEnvioMemorandoCobro;
    }

    public String getLiquidador() {
        return liquidador;
    }

    public void setLiquidador(String liquidador) {
        this.liquidador = liquidador;
    }

    public String getSubdirectorCobranzas() {
        return SubdirectorCobranzas;
    }

    public void setSubdirectorCobranzas(String SubdirectorCobranzas) {
        this.SubdirectorCobranzas = SubdirectorCobranzas;
    }

    public String getDescObservacionesAfiliaciones() {
        return descObservacionesAfiliaciones;
    }

    public void setDescObservacionesAfiliaciones(String descObservacionesAfiliaciones) {
        this.descObservacionesAfiliaciones = descObservacionesAfiliaciones;
    }

    public ValorDominio getCodEstadoLiquidacion() {
        return codEstadoLiquidacion;
    }

    public void setCodEstadoLiquidacion(ValorDominio codEstadoLiquidacion) {
        this.codEstadoLiquidacion = codEstadoLiquidacion;
    }

    public Calendar getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Calendar fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Calendar getFecFin() {
        return fecFin;
    }

    public void setFecFin(Calendar fecFin) {
        this.fecFin = fecFin;
    }

    public AprobacionDocumento getRequerimientoDeclararAmpliadoParcial() {
        return requerimientoDeclararAmpliadoParcial;
    }

    public void setRequerimientoDeclararAmpliadoParcial(AprobacionDocumento requerimientoDeclararAmpliadoParcial) {
        this.requerimientoDeclararAmpliadoParcial = requerimientoDeclararAmpliadoParcial;
    }

    public ValorDominio getCodCausalCierre() {
        return codCausalCierre;
    }

    public void setCodCausalCierre(ValorDominio codCausalCierre) {
        this.codCausalCierre = codCausalCierre;
    }

    public String getValUsuarioGeneraActo() {
        return valUsuarioGeneraActo;
    }

    public void setValUsuarioGeneraActo(String valUsuarioGeneraActo) {
        this.valUsuarioGeneraActo = valUsuarioGeneraActo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Liquidacion other = (Liquidacion) obj;
        if ((this.id == null) ? (other.id != null) : !this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

    public void setValSubdirectorCobranzas(FuncionarioTipo funcionarioTipo) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Liquidacion[ id=" + id + " ]";
    }

}
