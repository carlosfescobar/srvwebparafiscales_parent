package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "LIQUIDACION_PARCIAL")
public class LiquidacionParcial extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "liquidacionParcialIdSeq", sequenceName = "liquidacion_parcial_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "liquidacionParcialIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_UVT", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UVT uvt;
    @JoinColumn(name = "COD_ACCION_LIQ_PARCIAL", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codAccionLiquidacionParcial;
    @Column(name = " DESC_OBSERVACIONES")
    private String desObservaciones;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sancion sancion;

    public LiquidacionParcial() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UVT getUvt() {
        return uvt;
    }

    public void setUvt(UVT uvt) {
        this.uvt = uvt;
    }

    public ValorDominio getCodAccionLiquidacionParcial() {
        return codAccionLiquidacionParcial;
    }

    public void setCodAccionLiquidacionParcial(ValorDominio codAccionLiquidacionParcial) {
        this.codAccionLiquidacionParcial = codAccionLiquidacionParcial;
    }

    public String getDesObservaciones() {
        return desObservaciones;
    }

    public void setDesObservaciones(String desObservaciones) {
        this.desObservaciones = desObservaciones;
    }

    public Sancion getSancion() {
        return sancion;
    }

    public void setSancion(Sancion sancion) {
        this.sancion = sancion;
    }

}
