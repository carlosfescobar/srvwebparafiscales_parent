package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "MECANISMO_ANEXO_TEMPORAL")
public class MecanismoAnexoTemporal extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "mecanismoanexotempIdSeq", sequenceName = "mecanismo_anexo_temp_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mecanismoanexotempIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private Archivo archivo;
    @JoinColumn(name = "ID_MECANISMO_SUBSIDIARIO", referencedColumnName = "ID_MECANISMO_SUBSIDIARIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private MecanismoSubsidiario mecanismoSubsidiario;
    @JoinColumn(name = "COD_TIPO_ANEXO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ValorDominio codTipoAnexo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    public MecanismoSubsidiario getMecanismoSubsidiario() {
        return mecanismoSubsidiario;
    }

    public void setMecanismoSubsidiario(MecanismoSubsidiario mecanismoSubsidiario) {
        this.mecanismoSubsidiario = mecanismoSubsidiario;
    }

    public ValorDominio getCodTipoAnexo() {
        return codTipoAnexo;
    }

    public void setCodTipoAnexo(ValorDominio codTipoAnexo) {
        this.codTipoAnexo = codTipoAnexo;
    }


    

}
