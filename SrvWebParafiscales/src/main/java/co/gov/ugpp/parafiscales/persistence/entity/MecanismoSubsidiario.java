package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "MECANISMO_SUBSIDIARIO")
public class MecanismoSubsidiario extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "mecanismoIdSeq", sequenceName = "mecanismo_subsidia_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mecanismoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_MECANISMO_SUBSIDIARIO")
    private Long idMecanismoSubsidiario;

    @Column(name = "FEC_CREACION_MECANISMO_SUBSIDI")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecCreacionMecanismoSubsidi;

    @Column(name = "ES_FIJADO")
    private Boolean esFijado;

    @Column(name = "FEC_FIJACION_CARTELERA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFijacionCartelera;

    @Size(max = 255)
    @Column(name = "DESC_OBSERVACIONES_FIJADO")
    private String descObservacionesFijado;

    @Column(name = "FEC_DESFIJACION_CARTELERA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecDesfijacionCartelera;

    @Size(max = 255)
    @Column(name = "DESC_OBSERVACIONES_DESFIJADO")
    private String descObservacionesDesfijado;

    @Column(name = "ES_PUBLICADO_WEB")
    private Boolean esPublicadoWeb;

    @Column(name = "FEC_PUBLICACION_WEB")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecPublicacionWeb;

    @Column(name = "FEC_DESPUBLICACION_WEB")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecDespublicacionWeb;

    @JoinColumn(name = "ID_DOCUMENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DocumentoEcm documentoEcm;

    @JoinTable(name = "MECANISMO_SUBSID_HAS_NOTIFICA", joinColumns = {
        @JoinColumn(name = "ID_MECANISMO_SUBSIDIARIO", referencedColumnName = "ID_MECANISMO_SUBSIDIARIO")}, inverseJoinColumns = {
        @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID")})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<Notificacion> notificaciones;

    @JoinColumn(name = "ID_MECANISMO_SUBSIDIARIO", referencedColumnName = "ID_MECANISMO_SUBSIDIARIO")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MecanismoAnexoTemporal> anexosTemporales;

    @JoinColumn(name = "COD_TIPO_EDICTO_AVISO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoEdictoAviso;

    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoMecanismoSubsidiario;

    public MecanismoSubsidiario() {
    }

    public MecanismoSubsidiario(Long idMecanismoSubsidiario) {
        this.idMecanismoSubsidiario = idMecanismoSubsidiario;
    }

    public Long getIdMecanismoSubsidiario() {
        return idMecanismoSubsidiario;
    }

    public void setIdMecanismoSubsidiario(Long idMecanismoSubsidiario) {
        this.idMecanismoSubsidiario = idMecanismoSubsidiario;
    }

    public ValorDominio getCodTipoEdictoAviso() {
        return codTipoEdictoAviso;
    }

    public void setCodTipoEdictoAviso(ValorDominio codTipoEdictoAviso) {
        this.codTipoEdictoAviso = codTipoEdictoAviso;
    }

    public DocumentoEcm getDocumentoEcm() {
        return documentoEcm;
    }

    public void setDocumentoEcm(DocumentoEcm documentoEcm) {
        this.documentoEcm = documentoEcm;
    }

    public List<Notificacion> getNotificaciones() {
        return notificaciones;
    }

    public void setNotificaciones(List<Notificacion> notificaciones) {
        this.notificaciones = notificaciones;
    }

    public Calendar getFecCreacionMecanismoSubsidi() {
        return fecCreacionMecanismoSubsidi;
    }

    public void setFecCreacionMecanismoSubsidi(Calendar fecCreacionMecanismoSubsidi) {
        this.fecCreacionMecanismoSubsidi = fecCreacionMecanismoSubsidi;
    }

    public Boolean getEsFijado() {
        return esFijado;
    }

    public void setEsFijado(Boolean esFijado) {
        this.esFijado = esFijado;
    }

    public Calendar getFecFijacionCartelera() {
        return fecFijacionCartelera;
    }

    public void setFecFijacionCartelera(Calendar fecFijacionCartelera) {
        this.fecFijacionCartelera = fecFijacionCartelera;
    }

    public String getDescObservacionesFijado() {
        return descObservacionesFijado;
    }

    public void setDescObservacionesFijado(String descObservacionesFijado) {
        this.descObservacionesFijado = descObservacionesFijado;
    }

    public Calendar getFecDesfijacionCartelera() {
        return fecDesfijacionCartelera;
    }

    public void setFecDesfijacionCartelera(Calendar fecDesfijacionCartelera) {
        this.fecDesfijacionCartelera = fecDesfijacionCartelera;
    }

    public String getDescObservacionesDesfijado() {
        return descObservacionesDesfijado;
    }

    public void setDescObservacionesDesfijado(String descObservacionesDesfijado) {
        this.descObservacionesDesfijado = descObservacionesDesfijado;
    }

    public Boolean getEsPublicadoWeb() {
        return esPublicadoWeb;
    }

    public void setEsPublicadoWeb(Boolean esPublicadoWeb) {
        this.esPublicadoWeb = esPublicadoWeb;
    }

    public Calendar getFecPublicacionWeb() {
        return fecPublicacionWeb;
    }

    public void setFecPublicacionWeb(Calendar fecPublicacionWeb) {
        this.fecPublicacionWeb = fecPublicacionWeb;
    }

    public Calendar getFecDespublicacionWeb() {
        return fecDespublicacionWeb;
    }

    public void setFecDespublicacionWeb(Calendar fecDespublicacionWeb) {
        this.fecDespublicacionWeb = fecDespublicacionWeb;
    }

    public List<MecanismoAnexoTemporal> getAnexosTemporales() {
        if (anexosTemporales == null) {
            anexosTemporales = new ArrayList<MecanismoAnexoTemporal>();
        }
        return anexosTemporales;
    }

    public ValorDominio getCodEstadoMecanismoSubsidiario() {
        return codEstadoMecanismoSubsidiario;
    }

    public void setCodEstadoMecanismoSubsidiario(ValorDominio codEstadoMecanismoSubsidiario) {
        this.codEstadoMecanismoSubsidiario = codEstadoMecanismoSubsidiario;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMecanismoSubsidiario != null ? idMecanismoSubsidiario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MecanismoSubsidiario)) {
            return false;
        }
        MecanismoSubsidiario other = (MecanismoSubsidiario) object;
        if ((this.idMecanismoSubsidiario == null && other.idMecanismoSubsidiario != null) || (this.idMecanismoSubsidiario != null && !this.idMecanismoSubsidiario.equals(other.idMecanismoSubsidiario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.MecanismoSubsidiario[ idMecanismoSubsidiario=" + idMecanismoSubsidiario + " ]";
    }

    @Override
    public Long getId() {
        return idMecanismoSubsidiario;
    }

    public List<Notificacion> getNotificacionList() {
        return notificaciones;
    }

    public void setNotificacionList(List<Notificacion> notificacionList) {
        this.notificaciones = notificacionList;
    }

}
