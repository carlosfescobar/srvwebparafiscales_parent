package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "METADATA_DOC_AGRUPADOR")
public class MetadataDocAgrupador extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "metadataDocumentoAgrupaIdSeq", sequenceName = "metadata_doc_agrupa_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "metadataDocumentoAgrupaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_METADATA_DOCUMENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MetadataDocumento metadataDocumento;
    @Column(name = "VAL_AGRUPADOR")
    private String valAgrupador;

    public MetadataDocAgrupador() {
    }

    public MetadataDocAgrupador(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public MetadataDocumento getMetadataDocumento() {
        return metadataDocumento;
    }

    public void setMetadataDocumento(MetadataDocumento metadataDocumento) {
        this.metadataDocumento = metadataDocumento;
    }

    public String getValAgrupador() {
        return valAgrupador;
    }

    public void setValAgrupador(String valAgrupador) {
        this.valAgrupador = valAgrupador;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MetadataDocAgrupador)) {
            return false;
        }
        MetadataDocAgrupador other = (MetadataDocAgrupador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
