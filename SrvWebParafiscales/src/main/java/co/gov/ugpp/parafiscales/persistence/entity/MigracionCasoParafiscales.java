package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "migracionCasoParafiscales.findByMigracionAndCaso",
            query = "SELECT s FROM MigracionCasoParafiscales s WHERE s.migracionParafiscales.id = :idMigracion AND s.idFila = :idFila")})
@Table(name = "MIGRACION_CASO_PARAFISCALES")
public class MigracionCasoParafiscales extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "migracionCasoParafisIdSeq", sequenceName = "migracion_caso_parafis_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "migracionCasoParafisIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_FILA")
    private String idFila;
    @Column(name = "COD_TIPO_DOCUMENTO")
    private String codTipoDocumento;
    @Column(name = "VAL_NUMERO_DOCUMENTO")
    private String valNumeroDocumento;
    @Column(name = "ID_EXPEDIENTE")
    private String idExpediente;
    @Column(name = "ID_INSTANCIA_PROCESO")
    private String idInstanciaProceso;
    @Column(name = "DESC_ERROR_CASO")
    private String descErrorCaso;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codEstado;
    @JoinColumn(name = "ID_MIGRACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private MigracionParafiscales migracionParafiscales;

    public MigracionCasoParafiscales() {
    }

    public MigracionCasoParafiscales(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public void setCodTipoDocumento(String codTipoDocumento) {
        this.codTipoDocumento = codTipoDocumento;
    }

    public String getValNumeroDocumento() {
        return valNumeroDocumento;
    }

    public void setValNumeroDocumento(String valNumeroDocumento) {
        this.valNumeroDocumento = valNumeroDocumento;
    }

    public String getIdExpediente() {
        return idExpediente;
    }

    public void setIdExpediente(String idExpediente) {
        this.idExpediente = idExpediente;
    }

    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    public void setIdInstanciaProceso(String idInstanciaProceso) {
        this.idInstanciaProceso = idInstanciaProceso;
    }

    public String getDescErrorCaso() {
        return descErrorCaso;
    }

    public void setDescErrorCaso(String descErrorCaso) {
        this.descErrorCaso = descErrorCaso;
    }

    public MigracionParafiscales getMigracionParafiscales() {
        return migracionParafiscales;
    }

    public void setMigracionParafiscales(MigracionParafiscales migracionParafiscales) {
        this.migracionParafiscales = migracionParafiscales;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public String getIdFila() {
        return idFila;
    }

    public void setIdFila(String idFila) {
        this.idFila = idFila;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MigracionCasoParafiscales)) {
            return false;
        }
        MigracionCasoParafiscales other = (MigracionCasoParafiscales) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Accion[ id=" + id + " ]";
    }

}
