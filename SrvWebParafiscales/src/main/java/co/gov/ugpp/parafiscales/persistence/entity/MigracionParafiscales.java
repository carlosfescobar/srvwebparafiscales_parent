package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "MIGRACION_PARAFISCALES")
public class MigracionParafiscales extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "migracionParafisIdSeq", sequenceName = "migracion_parafis_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "migracionParafisIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "FEC_MIGRACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecMigracion;
    @JoinColumn(name = "COD_CASO_MIGRADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCasoMigrado;
    @Column(name = "ID_USUARIO_RESPONSABLE")
    private String idUsuarioResponsable;
    @JoinColumn(name = "ID_MIGRACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<MigracionCasoParafiscales> casosMigrados;

    public MigracionParafiscales() {
    }

    public MigracionParafiscales(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Calendar getFecMigracion() {
        return fecMigracion;
    }

    public void setFecMigracion(Calendar fecMigracion) {
        this.fecMigracion = fecMigracion;
    }

    public ValorDominio getCodCasoMigrado() {
        return codCasoMigrado;
    }

    public void setCodCasoMigrado(ValorDominio codCasoMigrado) {
        this.codCasoMigrado = codCasoMigrado;
    }

    public String getIdUsuarioResponsable() {
        return idUsuarioResponsable;
    }

    public void setIdUsuarioResponsable(String idUsuarioResponsable) {
        this.idUsuarioResponsable = idUsuarioResponsable;
    }

    public List<MigracionCasoParafiscales> getCasosMigrados() {
        if (casosMigrados == null) {
            casosMigrados = new ArrayList<MigracionCasoParafiscales>();
        }
        return casosMigrados;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MigracionParafiscales)) {
            return false;
        }
        MigracionParafiscales other = (MigracionParafiscales) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Accion[ id=" + id + " ]";
    }

}
