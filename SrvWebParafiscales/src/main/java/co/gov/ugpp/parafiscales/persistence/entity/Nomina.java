package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigInteger;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_NOMINA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nomina.findByExpediente", query = "SELECT n FROM Nomina n WHERE n.idexpediente.id = :idE ORDER BY n.fechacreacion DESC"),
})
public class Nomina extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "nomina_seq", sequenceName = "nomina_seq_id", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nomina_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Size(max = 100)
    @Column(name = "NOMBRE_ARCHIVO")
    private String nombreArchivo;

    @Lob
    @Column(name = "ARCHIVO")
    private byte[] archivo;

    @Column(name = "NIT")
    private BigInteger nit;

    @Size(max = 150)
    @Column(name = "RAZON_SOCIAL")
    private String razonSocial;

    @Size(max = 40)
    @Column(name = "ESTADO_HOJA")
    private String estadoHoja;

    @Size(max = 40)
    @Column(name = "ESTADO_NOMINA")
    private String estadoNomina;

    @Column(name = "ANO")
    private Integer ano;

    @Column(name = "MES")
    private Integer mes;

    @Column(name = "FECHA_CARGE")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaCarge;

    @OneToMany(mappedBy = "nomina", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<NominaDetalle> nominaDetalleList;

    @Basic(optional = false)
    @NotNull
    @Column(name = "IDINFORMACIONEXTERNADEFINITIVA")
    private BigInteger idinformacionexternadefinitiva;

    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechacreacion;

    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;

    @JoinColumn(name = "IDEXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente idexpediente;

    
    
    
    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    
    
    @Size(max = 100)
    @Column(name = "TIPO_ACTO")
    private String tipoActo;
        
    
    
    
    
    //SE ELIMINARON LOS SET DE:

    
   
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
    
      
    //**********************************************************//
    // CAMPOS NUEVOS YESID NUEVA ESTRUCTURA NOMINA 03-02-2016
    //**********************************************************//
    
    
    
    @Column(name = "NUM_EMPLEADO")
    private Integer numEmpleado;
        
     //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
     
    public Integer getnumEmpleado() {
        return numEmpleado;
    }
    
    public void setnumEmpleado(Integer numEmpleado) {
        this.numEmpleado = numEmpleado;
    }
    
    
    
    public String getTipoActo() {
        return tipoActo;
    }
    
    public void setTipoActo(String tipoActo) {
        this.tipoActo = tipoActo;
    }
    
    
    
    
    
    
    public Nomina() {
    }

    public Nomina(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getNit() {
        return nit;
    }

    public void setNit(BigInteger nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getEstadoHoja() {
        return estadoHoja;
    }

    public void setEstadoHoja(String estadoHoja) {
        this.estadoHoja = estadoHoja;
    }

    public String getEstadoNomina() {
        return estadoNomina;
    }

    public void setEstadoNomina(String estadoNomina) {
        this.estadoNomina = estadoNomina;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Calendar getFechaCarge() {
        return fechaCarge;
    }

    public void setFechaCarge(Calendar fechaCarge) {
        this.fechaCarge = fechaCarge;
    }

    @XmlTransient
    public List<NominaDetalle> getNominaDetalleList() {
        return nominaDetalleList;
    }

    public void setNominaDetalleList(List<NominaDetalle> nominaDetalleList) {
        this.nominaDetalleList = nominaDetalleList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nomina)) {
            return false;
        }
        Nomina other = (Nomina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.Nomina[ id=" + id + " ]";
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the idinformacionexternadefinitiva
     */
    public BigInteger getIdinformacionexternadefinitiva() {
        return idinformacionexternadefinitiva;
    }

    /**
     * @param idinformacionexternadefinitiva the idinformacionexternadefinitiva
     * to set
     */
    public void setIdinformacionexternadefinitiva(BigInteger idinformacionexternadefinitiva) {
        this.idinformacionexternadefinitiva = idinformacionexternadefinitiva;
    }

    /**
     * @return the fechacreacion
     */
    public Calendar getFechacreacion() {
        return fechacreacion;
    }

    /**
     * @param fechacreacion the fechacreacion to set
     */
    public void setFechacreacion(Calendar fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the idexpediente
     */
    public Expediente getIdexpediente() {
        return idexpediente;
    }

    /**
     * @param idexpediente the idexpediente to set
     */
    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }
}
