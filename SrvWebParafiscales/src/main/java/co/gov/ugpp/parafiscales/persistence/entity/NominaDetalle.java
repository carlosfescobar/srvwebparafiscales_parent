package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_NOMINA_DETALLE")
public class NominaDetalle extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name="nominadetalle_seq", sequenceName="nominadetalle_seq_id", allocationSize=1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "nominadetalle_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
        
    

    @Size(max = 100)
    @Column(name = "COND_ESP_TRAB")
    private String condEspTrab;

    @Column(name = "PREFIJO_CET")
    private BigInteger prefijoCet;

    @Size(max = 100)
    @Column(name = "COND_ESP_EMP")
    private String condEspEmp;

    @Column(name = "prefijo_cee")
    private BigInteger prefijoCee;

    @Column(name = "CEDULA")
    private BigInteger cedula;

    @Size(max = 150)
    @Column(name = "nombre")
    private String nombre;
    
    @Column(name = "sal_basico")
    private BigInteger salBasico;
    
    @Column(name = "DIAS_TRAB")
    private Integer diasTrab;
    
    @Column(name = "salario_int")
    private Boolean salarioInt;
    
    @Size(max = 50)
    @Column(name = "GRUPO_SAL")
    private String grupoSal;
    
    @Size(max = 50)
    @Column(name = "prefijo_grupo_sal")
    private String prefijoGrupoSal;
    
    @Column(name = "DIAS")
    private Integer dias;
    
    @Size(max = 150)
    @Column(name = "novedades")
    private String novedades;
    
    @JoinColumn(name = "nomina", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Nomina nomina;

    @Column(name = "ANO")
    private BigInteger ano;
    
    @Column(name = "MES")
    private BigInteger mes;
    
    @Size(max = 1)
    @Column(name = "ING")
    private String ing;
    
    @Column(name = "ING_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ingFecha;
    
    @Size(max = 1)
    @Column(name = "RET")
    private String ret;
    
    @Column(name = "RET_FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date retFecha;
    
    @Size(max = 1)
    @Column(name = "SLN")
    private String sln;
    
    @Size(max = 1)
    @Column(name = "CAUSAL_SUSPENSION")
    private String causalSuspension;
    
    @Column(name = "CAUSAL_SUSPENSION_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date causalSuspensionFinicial;
    
    @Column(name = "CAUSAL_SUSPENSION_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date causalSuspensionFfinal;
    
    @Size(max = 1)
    @Column(name = "IGE")
    private String ige;
    
    @Column(name = "IGE_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date igeFinicial;
    
    @Column(name = "IGE_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date igeFfinal;
    
    @Size(max = 1)
    @Column(name = "LMA")
    private String lma;
    
    @Column(name = "LMA_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lmaFinicial;
    
    @Column(name = "LMA_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lmaFfinal;
    
    @Size(max = 1)
    @Column(name = "VAC")
    private String vac;
    
    @Column(name = "VAC_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vacFinicial;
    
    @Column(name = "VAC_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vacFfinal;
    
    @Size(max = 1)
    @Column(name = "IRP")
    private String irp;
    
    @Column(name = "IRP_FINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date irpFinicial;
    
    @Column(name = "IRP_FFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date irpFfinal;
    
    @Size(max = 1)
    @Column(name = "SALARIO_INTEGRAL")
    private String salarioIntegral;
    
    @Size(max = 1)
    @Column(name = "ULTIMO_IBC_SIN_NOVEDAD")
    private String ultimoIbcSinNovedad;
    
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;

    @JoinColumn(name = "IDAPORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private AportanteLIQ idaportante;
    
    @JoinColumn(name = "IDCOTIZANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private CotizanteLIQ idcotizante;

    @JoinColumn(name = "IDCONTRATANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Contratante idcontratante;

    @OneToMany(mappedBy = "idnominadetalle", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AportesIndependiente> aportesIndependienteList;

    @OneToMany(mappedBy = "idnominadetalle", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ConceptoContable> conceptoContableList;

    
    
    //**********************************************************//
    // CAMPOS NUEVOS ANDRES NUEVA ESTRUCTURA NOMINA 19-06-2015
    //**********************************************************//
    
    @Size(max = 2)
    @Column(name = "NOVEDAD_INCAPACIDAD")
    private String novedadIncapacidad;
    
    
    @Size(max = 2)
    @Column(name = "NOVE_LICEN_MATERNI_PATERNI")
    private String novedadLicenciaMaternidadPaternidad;

    
    @Size(max = 2)
    @Column(name = "NOVE_PERMI_LICEN_REMUNERADA")
    private String novedadPermisoLicenciaRemunerada;

    
    @Size(max = 2)
    @Column(name = "NOVEDAD_SUSPENSION")
    private String novedadSuspension;
    
    
    @Size(max = 2)
    @Column(name = "NOVEDAD_VACACIONES")
    private String novedadVacaciones;
        
    
    
    
    
    @Column(name = "DIAS_TRABAJADOS_MES")
    private Integer diasTrabajadosMes;
    
    @Column(name = "DIAS_INCAPACIDADES_MES")
    private Integer diasIncapacidadesMes;
     
    @Column(name = "DIA_LICEN_MATERNI_PATERNI_MES")
    private Integer diasLicenciaMaternidadPaternidadMes;
     
    @Column(name = "DIA_LICEN_REMUNERADAS_MES")
    private Integer diasLicenciaRemuneradasMes;
      
    @Column(name = "DIAS_SUSPENSION_MES")
    private Integer diasSuspensionMes;
    
    @Column(name = "DIAS_VACACIONES_MES")
    private Integer diasVacacionesMes;
    
    @Column(name = "DIAS_HUELGA_LEGAL_MES")
    private Integer diasHuelgaLegalMes;
    
    @Column(name = "TOTAL_DIAS_REPORTADOS_MES")
    private Integer totalDiasReportadosMes;
    
    
    @Size(max = 2)
    @Column(name = "CALCULO_ACTUARIAL")
    private String calculoActuarial;
        
    
    @Column(name = "VALOR_CALCULO_ACTUARIAL")
    private BigDecimal valorCalculoActuarial;

    
    @Column(name = "IBC_CONC_INGR_OTR_APORTANTES")
    private BigDecimal ibcConcurrenciaIngresosOtrosAportantes;
    
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
    //**********************************************************//
    // MODIFICACION  AGREGAR CAMPOS  (YESID TAPIAS)  06-10-2015 
    //**********************************************************//
    @Size(max = 100)
    @Column(name = "TIPO_COTIZANTE")
    private String tipoCotizante;

    @Size(max = 100)
    @Column(name = "SUBTIPO_COTIZANTE")
    private String subTipoCotizante;
    
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
     
    
    //**********************************************************//
    // MODIFICACION  AGREGAR CAMPOS  (ANDRES GARCIA)  16-12-2015 
    //**********************************************************//
    
    @Size(max = 2)
    @Column(name = "EXTRANJERO_NO_OBLI_COTIZAR_PEN")
    private String extranjero_no_obligado_a_cotizar_pension;
    /*Lista: Si, No*/

    @Size(max = 2)
    @Column(name = "COLOMBIANO_EN_EL_EXTERIOR")
    private String colombiano_en_el_exterior;
    /*Lista: Si, No*/
    
    
    //**********************************************************//
    // MODIFICACION  AGREGAR CAMPOS  (YESID TAPIAS)  03-02-2016 
    //**********************************************************//
    @Size(max = 100)
    @Column(name = "IBC_PEN_CON_INGR_OTR_APO")
    private BigDecimal ibcPenConIngrOtrApo;

    @Size(max = 100)
    @Column(name = "IBC_ARL_CON_INGR_OTR_APO")
    private BigDecimal ibcArlConIngrOtrApo;
    
    //**********************************************************//
    // FIN MODIFICACION
    //**********************************************************//
    
    
    @Column(name = "DOBLE_LINEA_ANTERIOR")
    private Integer dobleLineaAnterior;

    public Integer getDobleLineaAnterior() {
        return dobleLineaAnterior;
    }

    public void setDobleLineaAnterior(Integer dobleLineaAnterior) {
        this.dobleLineaAnterior = dobleLineaAnterior;
    }
    

    
    //******************************************************************************//
    // MODIFICACION  AGREGAR 7 CAMPOS, CARGUE MANUAL PILA, PLANILLA M  15-12-2017 
    //******************************************************************************//
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_SALUD")
    private BigDecimal cargueManualPilaSalud;

    public BigDecimal getCargueManualPilaSalud() {
        return cargueManualPilaSalud;
    }

    public void setCargueManualPilaSalud(BigDecimal cargueManualPilaSalud) {
        this.cargueManualPilaSalud = cargueManualPilaSalud;
    }
    
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_PENSION")
    private BigDecimal cargueManualPilaPension;

    public BigDecimal getCargueManualPilaPension() {
        return cargueManualPilaPension;
    }

    public void setCargueManualPilaPension(BigDecimal cargueManualPilaPension) {
        this.cargueManualPilaPension = cargueManualPilaPension;
    }

    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_FSP_SOLID")
    private BigDecimal cargueManualPilaFspSolid;

    public BigDecimal getCargueManualPilaFspSolid() {
        return cargueManualPilaFspSolid;
    }

    public void setCargueManualPilaFspSolid(BigDecimal cargueManualPilaFspSolid) {
        this.cargueManualPilaFspSolid = cargueManualPilaFspSolid;
    }
    
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_FSP_SUBSIS")
    private BigDecimal cargueManualPilaFspSubsis;

    public BigDecimal getCargueManualPilaFspSubsis() {
        return cargueManualPilaFspSubsis;
    }

    public void setCargueManualPilaFspSubsis(BigDecimal cargueManualPilaFspSubsis) {
        this.cargueManualPilaFspSubsis = cargueManualPilaFspSubsis;
    }

    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_ALTO_RI_PE")
    private BigDecimal cargueManualPilaAltoRiPe;
    
    
    public BigDecimal getCargueManualPilaAltoRiPe() {
        return cargueManualPilaAltoRiPe;
    }

    public void setCargueManualPilaAltoRiPe(BigDecimal cargueManualPilaAltoRiPe) {
        this.cargueManualPilaAltoRiPe = cargueManualPilaAltoRiPe;
    }
    
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_ARL")
    private BigDecimal cargueManualPilaArl;

    public BigDecimal getCargueManualPilaArl() {
        return cargueManualPilaArl;
    }

    public void setCargueManualPilaArl(BigDecimal cargueManualPilaArl) {
        this.cargueManualPilaArl = cargueManualPilaArl;
    }

 
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_CCF")
    private BigDecimal cargueManualPilaCcf;
    
    public BigDecimal getCargueManualPilaCcf() {
        return cargueManualPilaCcf;
    }

    public void setCargueManualPilaCcf(BigDecimal cargueManualPilaCcf) {
        this.cargueManualPilaCcf = cargueManualPilaCcf;
    }        
    
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_SENA")
    private BigDecimal cargueManualPilaSena;
    
     public BigDecimal getCargueManualPilaSena() {
        return cargueManualPilaSena;
    }

    public void setCargueManualPilaSena(BigDecimal cargueManualPilaSena) {
        this.cargueManualPilaSena = cargueManualPilaSena;
    }          
    
    
    @Size(max = 100)
    @Column(name = "CARGUE_MANUAL_PILA_ICBF")
    private BigDecimal cargueManualPilaIcbf;
    
    public BigDecimal getCargueManualPilaIcbf() {
        return cargueManualPilaIcbf;
    }

    public void setCargueManualPilaIcbf(BigDecimal cargueManualPilaIcbf) {
        this.cargueManualPilaIcbf = cargueManualPilaIcbf;
    }
    

  //******************************************************************************//
  //******************************************************************************//
  //******************************************************************************//
    
    
    
            
    @Size(max = 2)
    @Column(name = "OMISION_SALUD")
    private String omisionSalud;
    
    public String getOmisionSalud() {
        return omisionSalud;
    }

    public void setOmisionSalud(String omisionSalud) {
        this.omisionSalud = omisionSalud;
    }
    
    
    @Size(max = 2)
    @Column(name = "OMISION_PENSION")
    private String omisionPension;
    
    public String getOmisionPension() {
        return omisionPension;
    }

    public void setOmisionPension(String omisionPension) {
        this.omisionPension = omisionPension;
    }
    
    
    
    @Size(max = 2)
    @Column(name = "OMISION_ARL")
    private String omisionArl;
    
    public String getOmisionArl() {
        return omisionArl;
    }

    public void setOmisionArl(String omisionArl) {
        this.omisionArl = omisionArl;
    }
    
    
    
    
    @Size(max = 2)
    @Column(name = "OMISION_CCF")
    private String omisionCcf;
    
    public String getOmisionCcf() {
        return omisionCcf;
    }

    public void setOmisionCcf(String omisionCcf) {
        this.omisionCcf = omisionCcf;
    }
    
    
    //******************************************************************************//
    //******************************************************************************//
    //******************************************************************************//
    
    
            
            
    public NominaDetalle() {
    }

    public NominaDetalle(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCondEspTrab() {
        return condEspTrab;
    }

    public void setCondEspTrab(String condEspTrab) {
        this.condEspTrab = condEspTrab;
    }

    public BigInteger getPrefijoCet() {
        return prefijoCet;
    }

    public void setPrefijoCet(BigInteger prefijoCet) {
        this.prefijoCet = prefijoCet;
    }

    public String getCondEspEmp() {
        return condEspEmp;
    }

    public void setCondEspEmp(String condEspEmp) {
        this.condEspEmp = condEspEmp;
    }

    public BigInteger getPrefijoCee() {
        return prefijoCee;
    }

    public void setPrefijoCee(BigInteger prefijoCee) {
        this.prefijoCee = prefijoCee;
    }

    public BigInteger getCedula() {
        return cedula;
    }

    public void setCedula(BigInteger cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public BigInteger getSalBasico() {
        return salBasico;
    }

    public void setSalBasico(BigInteger salBasico) {
        this.salBasico = salBasico;
    }

    public Integer getDiasTrab() {
        return diasTrab;
    }

    public void setDiasTrab(Integer diasTrab) {
        this.diasTrab = diasTrab;
    }

    public Boolean getSalarioInt() {
        return salarioInt;
    }

    public void setSalarioInt(Boolean salarioInt) {
        this.salarioInt = salarioInt;
    }

    public String getGrupoSal() {
        return grupoSal;
    }

    public void setGrupoSal(String grupoSal) {
        this.grupoSal = grupoSal;
    }

    public String getPrefijoGrupoSal() {
        return prefijoGrupoSal;
    }

    public void setPrefijoGrupoSal(String prefijoGrupoSal) {
        this.prefijoGrupoSal = prefijoGrupoSal;
    }

    public Integer getDias() {
        return dias;
    }

    public void setDias(Integer dias) {
        this.dias = dias;
    }

    public String getNovedades() {
        return novedades;
    }

    public void setNovedades(String novedades) {
        this.novedades = novedades;
    }

    public Nomina getNomina() {
        return nomina;
    }

    public void setNomina(Nomina nomina) {
        this.nomina = nomina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NominaDetalle)) {
            return false;
        }
        NominaDetalle other = (NominaDetalle) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.NominaDetalle[ id=" + id + " ]";
    }

    /**
     * @return the ano
     */
    public BigInteger getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(BigInteger ano) {
        this.ano = ano;
    }

    /**
     * @return the mes
     */
    public BigInteger getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(BigInteger mes) {
        this.mes = mes;
    }

    /**
     * @return the ing
     */
    public String getIng() {
        return ing;
    }

    /**
     * @param ing the ing to set
     */
    public void setIng(String ing) {
        this.ing = ing;
    }

    /**
     * @return the ingFecha
     */
    public Date getIngFecha() {
        return ingFecha;
    }

    /**
     * @param ingFecha the ingFecha to set
     */
    public void setIngFecha(Date ingFecha) {
        this.ingFecha = ingFecha;
    }

    /**
     * @return the ret
     */
    public String getRet() {
        return ret;
    }

    /**
     * @param ret the ret to set
     */
    public void setRet(String ret) {
        this.ret = ret;
    }

    /**
     * @return the retFecha
     */
    public Date getRetFecha() {
        return retFecha;
    }

    /**
     * @param retFecha the retFecha to set
     */
    public void setRetFecha(Date retFecha) {
        this.retFecha = retFecha;
    }

    /**
     * @return the sln
     */
    public String getSln() {
        return sln;
    }

    /**
     * @param sln the sln to set
     */
    public void setSln(String sln) {
        this.sln = sln;
    }

    /**
     * @return the causalSuspension
     */
    public String getCausalSuspension() {
        return causalSuspension;
    }

    /**
     * @param causalSuspension the causalSuspension to set
     */
    public void setCausalSuspension(String causalSuspension) {
        this.causalSuspension = causalSuspension;
    }

    /**
     * @return the causalSuspensionFinicial
     */
    public Date getCausalSuspensionFinicial() {
        return causalSuspensionFinicial;
    }

    /**
     * @param causalSuspensionFinicial the causalSuspensionFinicial to set
     */
    public void setCausalSuspensionFinicial(Date causalSuspensionFinicial) {
        this.causalSuspensionFinicial = causalSuspensionFinicial;
    }

    /**
     * @return the causalSuspensionFfinal
     */
    public Date getCausalSuspensionFfinal() {
        return causalSuspensionFfinal;
    }

    /**
     * @param causalSuspensionFfinal the causalSuspensionFfinal to set
     */
    public void setCausalSuspensionFfinal(Date causalSuspensionFfinal) {
        this.causalSuspensionFfinal = causalSuspensionFfinal;
    }

    /**
     * @return the ige
     */
    public String getIge() {
        return ige;
    }

    /**
     * @param ige the ige to set
     */
    public void setIge(String ige) {
        this.ige = ige;
    }

    /**
     * @return the igeFinicial
     */
    public Date getIgeFinicial() {
        return igeFinicial;
    }

    /**
     * @param igeFinicial the igeFinicial to set
     */
    public void setIgeFinicial(Date igeFinicial) {
        this.igeFinicial = igeFinicial;
    }

    /**
     * @return the igeFfinal
     */
    public Date getIgeFfinal() {
        return igeFfinal;
    }

    /**
     * @param igeFfinal the igeFfinal to set
     */
    public void setIgeFfinal(Date igeFfinal) {
        this.igeFfinal = igeFfinal;
    }

    /**
     * @return the lma
     */
    public String getLma() {
        return lma;
    }

    /**
     * @param lma the lma to set
     */
    public void setLma(String lma) {
        this.lma = lma;
    }

    /**
     * @return the lmaFinicial
     */
    public Date getLmaFinicial() {
        return lmaFinicial;
    }

    /**
     * @param lmaFinicial the lmaFinicial to set
     */
    public void setLmaFinicial(Date lmaFinicial) {
        this.lmaFinicial = lmaFinicial;
    }

    /**
     * @return the lmaFfinal
     */
    public Date getLmaFfinal() {
        return lmaFfinal;
    }

    /**
     * @param lmaFfinal the lmaFfinal to set
     */
    public void setLmaFfinal(Date lmaFfinal) {
        this.lmaFfinal = lmaFfinal;
    }

    /**
     * @return the vac
     */
    public String getVac() {
        return vac;
    }

    /**
     * @param vac the vac to set
     */
    public void setVac(String vac) {
        this.vac = vac;
    }

    /**
     * @return the vacFinicial
     */
    public Date getVacFinicial() {
        return vacFinicial;
    }

    /**
     * @param vacFinicial the vacFinicial to set
     */
    public void setVacFinicial(Date vacFinicial) {
        this.vacFinicial = vacFinicial;
    }

    /**
     * @return the vacFfinal
     */
    public Date getVacFfinal() {
        return vacFfinal;
    }

    /**
     * @param vacFfinal the vacFfinal to set
     */
    public void setVacFfinal(Date vacFfinal) {
        this.vacFfinal = vacFfinal;
    }

    /**
     * @return the irp
     */
    public String getIrp() {
        return irp;
    }

    /**
     * @param irp the irp to set
     */
    public void setIrp(String irp) {
        this.irp = irp;
    }

    /**
     * @return the irpFinicial
     */
    public Date getIrpFinicial() {
        return irpFinicial;
    }

    /**
     * @param irpFinicial the irpFinicial to set
     */
    public void setIrpFinicial(Date irpFinicial) {
        this.irpFinicial = irpFinicial;
    }

    /**
     * @return the irpFfinal
     */
    public Date getIrpFfinal() {
        return irpFfinal;
    }

    /**
     * @param irpFfinal the irpFfinal to set
     */
    public void setIrpFfinal(Date irpFfinal) {
        this.irpFfinal = irpFfinal;
    }

    /**
     * @return the salarioIntegral
     */
    public String getSalarioIntegral() {
        return salarioIntegral;
    }

    /**
     * @param salarioIntegral the salarioIntegral to set
     */
    public void setSalarioIntegral(String salarioIntegral) {
        this.salarioIntegral = salarioIntegral;
    }

    /**
     * @return the ultimoIbcSinNovedad
     */
    public String getUltimoIbcSinNovedad() {
        return ultimoIbcSinNovedad;
    }

    /**
     * @param ultimoIbcSinNovedad the ultimoIbcSinNovedad to set
     */
    public void setUltimoIbcSinNovedad(String ultimoIbcSinNovedad) {
        this.ultimoIbcSinNovedad = ultimoIbcSinNovedad;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    public AportanteLIQ getIdaportante() {
        return idaportante;
    }

    public void setIdaportante(AportanteLIQ idaportante) {
        this.idaportante = idaportante;
    }

    public CotizanteLIQ getIdcotizante() {
        return idcotizante;
    }

    public void setIdcotizante(CotizanteLIQ idcotizante) {
        this.idcotizante = idcotizante;
    }

    /**
     * @return the idcontratante
     */
    public Contratante getIdcontratante() {
        return idcontratante;
    }

    /**
     * @param idcontratante the idcontratante to set
     */
    public void setIdcontratante(Contratante idcontratante) {
        this.idcontratante = idcontratante;
    }

    /**
     * @return the aportesIndependienteList
     */
    public List<AportesIndependiente> getAportesIndependienteList() {
        return aportesIndependienteList;
    }

    /**
     * @param aportesIndependienteList the aportesIndependienteList to set
     */
    public void setAportesIndependienteList(List<AportesIndependiente> aportesIndependienteList) {
        this.aportesIndependienteList = aportesIndependienteList;
    }

    /**
     * @return the conceptoContableList
     */
    public List<ConceptoContable> getConceptoContableList() {
        return conceptoContableList;
    }

    /**
     * @param conceptoContableList the conceptoContableList to set
     */
    public void setConceptoContableList(List<ConceptoContable> conceptoContableList) {
        this.conceptoContableList = conceptoContableList;
    }

    /**
     * @return the novedadIncapacidad
     */
    public String getNovedadIncapacidad() {
        return novedadIncapacidad;
    }

    /**
     * @param novedadIncapacidad the novedadIncapacidad to set
     */
    public void setNovedadIncapacidad(String novedadIncapacidad) {
        this.novedadIncapacidad = novedadIncapacidad;
    }

    /**
     * @return the novedadLicenciaMaternidadPaternidad
     */
    public String getNovedadLicenciaMaternidadPaternidad() {
        return novedadLicenciaMaternidadPaternidad;
    }

    /**
     * @param novedadLicenciaMaternidadPaternidad the novedadLicenciaMaternidadPaternidad to set
     */
    public void setNovedadLicenciaMaternidadPaternidad(String novedadLicenciaMaternidadPaternidad) {
        this.novedadLicenciaMaternidadPaternidad = novedadLicenciaMaternidadPaternidad;
    }

    /**
     * @return the novedadPermisoLicenciaRemunerada
     */
    public String getNovedadPermisoLicenciaRemunerada() {
        return novedadPermisoLicenciaRemunerada;
    }

    /**
     * @param novedadPermisoLicenciaRemunerada the novedadPermisoLicenciaRemunerada to set
     */
    public void setNovedadPermisoLicenciaRemunerada(String novedadPermisoLicenciaRemunerada) {
        this.novedadPermisoLicenciaRemunerada = novedadPermisoLicenciaRemunerada;
    }

    /**
     * @return the novedadSuspension
     */
    public String getNovedadSuspension() {
        return novedadSuspension;
    }

    /**
     * @param novedadSuspension the novedadSuspension to set
     */
    public void setNovedadSuspension(String novedadSuspension) {
        this.novedadSuspension = novedadSuspension;
    }

    /**
     * @return the novedadVacaciones
     */
    public String getNovedadVacaciones() {
        return novedadVacaciones;
    }

    /**
     * @param novedadVacaciones the novedadVacaciones to set
     */
    public void setNovedadVacaciones(String novedadVacaciones) {
        this.novedadVacaciones = novedadVacaciones;
    }

    /**
     * @return the diasTrabajadosMes
     */
    public Integer getDiasTrabajadosMes() {
        return diasTrabajadosMes;
    }

    /**
     * @param diasTrabajadosMes the diasTrabajadosMes to set
     */
    public void setDiasTrabajadosMes(Integer diasTrabajadosMes) {
        this.diasTrabajadosMes = diasTrabajadosMes;
    }

    /**
     * @return the diasIncapacidadesMes
     */
    public Integer getDiasIncapacidadesMes() {
        return diasIncapacidadesMes;
    }

    /**
     * @param diasIncapacidadesMes the diasIncapacidadesMes to set
     */
    public void setDiasIncapacidadesMes(Integer diasIncapacidadesMes) {
        this.diasIncapacidadesMes = diasIncapacidadesMes;
    }

    /**
     * @return the diasLicenciaMaternidadPaternidadMes
     */
    public Integer getDiasLicenciaMaternidadPaternidadMes() {
        return diasLicenciaMaternidadPaternidadMes;
    }

    /**
     * @param diasLicenciaMaternidadPaternidadMes the diasLicenciaMaternidadPaternidadMes to set
     */
    public void setDiasLicenciaMaternidadPaternidadMes(Integer diasLicenciaMaternidadPaternidadMes) {
        this.diasLicenciaMaternidadPaternidadMes = diasLicenciaMaternidadPaternidadMes;
    }

    /**
     * @return the diasLicenciaRemuneradasMes
     */
    public Integer getDiasLicenciaRemuneradasMes() {
        return diasLicenciaRemuneradasMes;
    }

    /**
     * @param diasLicenciaRemuneradasMes the diasLicenciaRemuneradasMes to set
     */
    public void setDiasLicenciaRemuneradasMes(Integer diasLicenciaRemuneradasMes) {
        this.diasLicenciaRemuneradasMes = diasLicenciaRemuneradasMes;
    }

    /**
     * @return the diasSuspensionMes
     */
    public Integer getDiasSuspensionMes() {
        return diasSuspensionMes;
    }

    /**
     * @param diasSuspensionMes the diasSuspensionMes to set
     */
    public void setDiasSuspensionMes(Integer diasSuspensionMes) {
        this.diasSuspensionMes = diasSuspensionMes;
    }

    /**
     * @return the diasVacacionesMes
     */
    public Integer getDiasVacacionesMes() {
        return diasVacacionesMes;
    }

    /**
     * @param diasVacacionesMes the diasVacacionesMes to set
     */
    public void setDiasVacacionesMes(Integer diasVacacionesMes) {
        this.diasVacacionesMes = diasVacacionesMes;
    }

    /**
     * @return the diasHuelgaLegalMes
     */
    public Integer getDiasHuelgaLegalMes() {
        return diasHuelgaLegalMes;
    }

    /**
     * @param diasHuelgaLegalMes the diasHuelgaLegalMes to set
     */
    public void setDiasHuelgaLegalMes(Integer diasHuelgaLegalMes) {
        this.diasHuelgaLegalMes = diasHuelgaLegalMes;
    }

    /**
     * @return the totalDiasReportadosMes
     */
    public Integer getTotalDiasReportadosMes() {
        return totalDiasReportadosMes;
    }

    /**
     * @param totalDiasReportadosMes the totalDiasReportadosMes to set
     */
    public void setTotalDiasReportadosMes(Integer totalDiasReportadosMes) {
        this.totalDiasReportadosMes = totalDiasReportadosMes;
    }

    /**
     * @return the calculoActuarial
     */
    public String getCalculoActuarial() {
        return calculoActuarial;
    }

    /**
     * @param calculoActuarial the calculoActuarial to set
     */
    public void setCalculoActuarial(String calculoActuarial) {
        this.calculoActuarial = calculoActuarial;
    }

    /**
     * @return the valorCalculoActuarial
     */
    public BigDecimal getValorCalculoActuarial() {
        return valorCalculoActuarial;
    }

    /**
     * @param valorCalculoActuarial the valorCalculoActuarial to set
     */
    public void setValorCalculoActuarial(BigDecimal valorCalculoActuarial) {
        this.valorCalculoActuarial = valorCalculoActuarial;
    }


    /**
     * @return the ibcConcurrenciaIngresosOtrosAportantes
     */
    public BigDecimal getIbcConcurrenciaIngresosOtrosAportantes() {
        return ibcConcurrenciaIngresosOtrosAportantes;
    }

    /**
     * @param ibcConcurrenciaIngresosOtrosAportantes the ibcConcurrenciaIngresosOtrosAportantes to set
     */
    public void setIbcConcurrenciaIngresosOtrosAportantes(BigDecimal ibcConcurrenciaIngresosOtrosAportantes) {
        this.ibcConcurrenciaIngresosOtrosAportantes = ibcConcurrenciaIngresosOtrosAportantes;
    }

    
    
    
    
    
    /**
     * @return the tipo_cotizante
     */
    public String getTipoCotizante() {
        return tipoCotizante;
    }

    /**
     * @param tipo_cotizante the tipo_cotizante to set
     */
    public void setTipoCotizante(String tipo_cotizante) {
        this.tipoCotizante = tipo_cotizante;
    }

    /**
     * @return the subtipo_cotizante
     */
    public String getSubTipoCotizante() {
        return subTipoCotizante;
    }

    /**
     * @param subtipo_cotizante the subtipo_cotizante to set
     */
    public void setSubTipoCotizante(String subtipo_cotizante) {
        this.subTipoCotizante = subtipo_cotizante;
    }

    /**
     * @return the extranjero_no_obligado_a_cotizar_pension
     */
    public String getExtranjero_no_obligado_a_cotizar_pension() {
        return extranjero_no_obligado_a_cotizar_pension;
    }

    /**
     * @param extranjero_no_obligado_a_cotizar_pension the extranjero_no_obligado_a_cotizar_pension to set
     */
    public void setExtranjero_no_obligado_a_cotizar_pension(String extranjero_no_obligado_a_cotizar_pension) {
        this.extranjero_no_obligado_a_cotizar_pension = extranjero_no_obligado_a_cotizar_pension;
    }

    /**
     * @return the colombiano_en_el_exterior
     */
    public String getColombiano_en_el_exterior() {
        return colombiano_en_el_exterior;
    }

    /**
     * @param colombiano_en_el_exterior the colombiano_en_el_exterior to set
     */
    public void setColombiano_en_el_exterior(String colombiano_en_el_exterior) {
        this.colombiano_en_el_exterior = colombiano_en_el_exterior;
    }
    
    
    /**
     * @return the IbcPenConIngrOtrApo
     */
    public BigDecimal getIbcPenConIngrOtrApo() {
        return ibcPenConIngrOtrApo;
    }

    /**
     * @param IbcPenConIngrOtrApo the IbcPenConIngrOtrApo to set
     */
    public void setIbcPenConIngrOtrApo(BigDecimal IbcPenConIngrOtrApo) {
        this.ibcPenConIngrOtrApo = IbcPenConIngrOtrApo;
    }


    
    /**
     * @return the ibcArlConIngrOtrApo
     */
    public BigDecimal getIbcArlConIngrOtrApo() {
        return ibcArlConIngrOtrApo;
    }

    /**
     * @param ibcArlConIngrOtrApo the ibcArlConIngrOtrApo to set
     */
    public void setIbcArlConIngrOtrApo(BigDecimal ibcArlConIngrOtrApo) {
        this.ibcArlConIngrOtrApo = ibcArlConIngrOtrApo;
    }

    
    
}
