package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@NamedQueries({
    @NamedQuery(name = "notificacion.findyByEstados",
            query = "SELECT n FROM Notificacion n WHERE n.codEstadoNotificacion.id IN :estadosNotificacion")})
@Table(name = "NOTIFICACION")
public class Notificacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "notificacionIdSeq", sequenceName = "notificacion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "notificacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "DOC_ACTA_NOTIFICACION_PERSO_ID")
    private Long docActaNotPresoId;
    @JoinColumn(name = "ID_INTERESADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona interesadoId;
    @JoinColumn(name = "COD_CALIDAD_NOTIFICADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCalidadNotificado;
    @Size(max = 50)
    @Column(name = "DESC_CALIDAD_NOTIFICADO")
    private String desCalidadNotificado;
    @JoinColumn(name = "COD_TIPO_NOTIFICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoNotificacion;
    @JoinColumn(name = "COD_TIPO_MECANISMO_SUBSIDIARIO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoMecanismoSubsidiario;
    @Column(name = "FEC_NOTIFICACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecNotificacion;
    @Column(name = "ES_ACTA_NOTIFICACION_VALIDADA")
    private Boolean esActaNotificacionValidada;
    @Column(name = "ES_DEVOLUCION_ACTO_ADMINISTRAT")
    private Boolean esDevlucionActoAdministrat;
    @Size(max = 50)
    @Column(name = "DESC_ACTA_NOTIFICACION_VALIDAD")
    private String desActaNotififcacionValidad;
    @Size(max = 50)
    @Column(name = "DESC_DEVOLUCION_ACTO_ADMINISTR")
    private String desDevolucionActoAdministr;
    @Column(name = "ES_MECANISMO_SUBSIDIARIO")
    private Boolean esMecanismoSubsidiario;
    @Size(max = 50)
    @Column(name = "ID_RADICADO_DOCUMENTOS_ANEXOS")
    private String idRadicadoDocumentosAnexos;
    @JoinColumn(name = "ID_PERSONA_NOTIFICADA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona persona;
    @JoinColumn(name = "ID_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo actoAdministrativo;
    @JoinColumn(name = "COD_ESTADO_NOTIFICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoNotificacion;
    @JoinColumn(name = "COD_TIPO_NOT_DEFINITIVA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoNotificacionDefinitva;

    @ManyToMany(cascade = CascadeType.ALL, mappedBy = "notificaciones", fetch = FetchType.LAZY)
    private List<MecanismoSubsidiario> mecanismosNotificacion;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNotificacion", fetch = FetchType.LAZY)
    private List<DocumentosAnexosIdnot> documentosAnexosIdnotList;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idNotificacion", fetch = FetchType.LAZY)
    private List<DocumentosAnexosCodnot> documentosAnexosCodnotList;

    @JoinColumn(name = "ID_NOTIFICACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<NotificacionAnexoTemporal> anexosTemporales;

    public Notificacion() {
    }

    public Notificacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodTipoMecanismoSubsidiario() {
        return codTipoMecanismoSubsidiario;
    }

    public void setCodTipoMecanismoSubsidiario(ValorDominio codTipoMecanismoSubsidiario) {
        this.codTipoMecanismoSubsidiario = codTipoMecanismoSubsidiario;
    }

    public List<DocumentosAnexosIdnot> getDocumentosAnexosIdnotList() {
        return documentosAnexosIdnotList;
    }

    public void setDocumentosAnexosIdnotList(List<DocumentosAnexosIdnot> documentosAnexosIdnotList) {
        this.documentosAnexosIdnotList = documentosAnexosIdnotList;
    }

    public List<DocumentosAnexosCodnot> getDocumentosAnexosCodnotList() {
        return documentosAnexosCodnotList;
    }

    public void setDocumentosAnexosCodnotList(List<DocumentosAnexosCodnot> documentosAnexosCodnotList) {
        this.documentosAnexosCodnotList = documentosAnexosCodnotList;
    }

    public Long getDocActaNotPresoId() {
        return docActaNotPresoId;
    }

    public void setDocActaNotPresoId(Long docActaNotPresoId) {
        this.docActaNotPresoId = docActaNotPresoId;
    }

    public Persona getInteresadoId() {
        return interesadoId;
    }

    public void setInteresadoId(Persona interesadoId) {
        this.interesadoId = interesadoId;
    }

    public ValorDominio getCodCalidadNotificado() {
        return codCalidadNotificado;
    }

    public void setCodCalidadNotificado(ValorDominio codCalidadNotificado) {
        this.codCalidadNotificado = codCalidadNotificado;
    }

    public String getDesCalidadNotificado() {
        return desCalidadNotificado;
    }

    public void setDesCalidadNotificado(String desCalidadNotificado) {
        this.desCalidadNotificado = desCalidadNotificado;
    }

    public ValorDominio getCodTipoNotificacion() {
        return codTipoNotificacion;
    }

    public void setCodTipoNotificacion(ValorDominio codTipoNotificacion) {
        this.codTipoNotificacion = codTipoNotificacion;
    }

    public Calendar getFecNotificacion() {
        return fecNotificacion;
    }

    public void setFecNotificacion(Calendar fecNotificacion) {
        this.fecNotificacion = fecNotificacion;
    }

    public Boolean getEsActaNotificacionValidada() {
        return esActaNotificacionValidada;
    }

    public void setEsActaNotificacionValidada(Boolean esActaNotificacionValidada) {
        this.esActaNotificacionValidada = esActaNotificacionValidada;
    }

    public Boolean getEsDevlucionActoAdministrat() {
        return esDevlucionActoAdministrat;
    }

    public void setEsDevlucionActoAdministrat(Boolean esDevlucionActoAdministrat) {
        this.esDevlucionActoAdministrat = esDevlucionActoAdministrat;
    }

    public String getDesActaNotififcacionValidad() {
        return desActaNotififcacionValidad;
    }

    public void setDesActaNotififcacionValidad(String desActaNotififcacionValidad) {
        this.desActaNotififcacionValidad = desActaNotififcacionValidad;
    }

    public String getDesDevolucionActoAdministr() {
        return desDevolucionActoAdministr;
    }

    public void setDesDevolucionActoAdministr(String desDevolucionActoAdministr) {
        this.desDevolucionActoAdministr = desDevolucionActoAdministr;
    }

    public Boolean getEsMecanismoSubsidiario() {
        return esMecanismoSubsidiario;
    }

    public void setEsMecanismoSubsidiario(Boolean esMecanismoSubsidiario) {
        this.esMecanismoSubsidiario = esMecanismoSubsidiario;
    }

    public String getIdRadicadoDocumentosAnexos() {
        return idRadicadoDocumentosAnexos;
    }

    public void setIdRadicadoDocumentosAnexos(String idRadicadoDocumentosAnexos) {
        this.idRadicadoDocumentosAnexos = idRadicadoDocumentosAnexos;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public ActoAdministrativo getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(ActoAdministrativo actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    public ValorDominio getCodEstadoNotificacion() {
        return codEstadoNotificacion;
    }

    public void setCodEstadoNotificacion(ValorDominio codEstadoNotificacion) {
        this.codEstadoNotificacion = codEstadoNotificacion;
    }

    public ValorDominio getCodTipoNotificacionDefinitva() {
        return codTipoNotificacionDefinitva;
    }

    public void setCodTipoNotificacionDefinitva(ValorDominio codTipoNotificacionDefinitva) {
        this.codTipoNotificacionDefinitva = codTipoNotificacionDefinitva;
    }

    public List<MecanismoSubsidiario> getMecanismosNotificacion() {
        return mecanismosNotificacion;
    }

    public List<NotificacionAnexoTemporal> getAnexosTemporales() {
        if (anexosTemporales == null) {
            anexosTemporales = new ArrayList<NotificacionAnexoTemporal>();
        }
        return anexosTemporales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Notificacion)) {
            return false;
        }
        Notificacion other = (Notificacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Notificacion[ id=" + id + " ]";
    }

}
