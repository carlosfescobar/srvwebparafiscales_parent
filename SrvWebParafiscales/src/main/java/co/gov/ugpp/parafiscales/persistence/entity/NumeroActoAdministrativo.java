package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author rpadilla
 */
@Entity

@NamedQueries({
    @NamedQuery(name = "NumeroActoAdministrativo.findListByCodTipoActoAdministrativo",
            query = "SELECT naa FROM NumeroActoAdministrativo naa WHERE naa.id.codTipoActoAdministrativo = :codTipoActoAdministrativo ORDER BY naa.id.anio DESC"),
    @NamedQuery(name = "NumeroActoAdministrativo.findConsecutivoByTipoAndAnio",
            query = "SELECT naa FROM NumeroActoAdministrativo naa WHERE naa.prefijo = :prefijo AND naa.id.anio = :anio")}
)
@Table(name = "NUMERO_ACTO_ADMINISTRATIVO")
public class NumeroActoAdministrativo extends AbstractEntity<NumeroActoAdministrativoPk> {

    @EmbeddedId
    private NumeroActoAdministrativoPk id;

    @JoinColumn(name = "ID_PREFIJO_NUMERO_ACTO_ADMIN", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private PrefijoNumeroActo prefijonumeroacto;

    @Column(name = "prefijo")
    private String prefijo;

    @Version
    @Column(name = "version")
    private Long version;

    public NumeroActoAdministrativo() {
    }

    public NumeroActoAdministrativo(NumeroActoAdministrativoPk id) {
        this.id = id;
    }

    @Override
    public NumeroActoAdministrativoPk getId() {
        return id;
    }

    public void setId(NumeroActoAdministrativoPk id) {
        this.id = id;
    }

    public PrefijoNumeroActo getPrefijonumeroacto() {
        return prefijonumeroacto;
    }

    public void setPrefijonumeroacto(PrefijoNumeroActo prefijonumeroacto) {
        this.prefijonumeroacto = prefijonumeroacto;
    }

    public String getPrefijo() {
        return prefijo;
    }

    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NumeroActoAdministrativo other = (NumeroActoAdministrativo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

}
