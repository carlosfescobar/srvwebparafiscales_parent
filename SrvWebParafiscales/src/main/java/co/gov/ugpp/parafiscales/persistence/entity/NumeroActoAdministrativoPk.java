package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Embeddable
public class NumeroActoAdministrativoPk implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "cod_tipo_acto_administrativo", updatable = false)
    private String codTipoActoAdministrativo;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "anio", updatable = false)
    private Integer anio;

    public NumeroActoAdministrativoPk() {
    }

    public NumeroActoAdministrativoPk(String codTipoActoAdministrativo, Integer anio) {
        this.codTipoActoAdministrativo = codTipoActoAdministrativo;
        this.anio = anio;
    }

    public String getCodTipoActoAdministrativo() {
        return codTipoActoAdministrativo;
    }

    public void setCodTipoActoAdministrativo(String codTipoActoAdministrativo) {
        this.codTipoActoAdministrativo = codTipoActoAdministrativo;
    }

    public Integer getAnio() {
        return anio;
    }

    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (this.codTipoActoAdministrativo != null ? this.codTipoActoAdministrativo.hashCode() : 0);
        hash = 53 * hash + (this.anio != null ? this.anio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NumeroActoAdministrativoPk other = (NumeroActoAdministrativoPk) obj;
        if ((this.codTipoActoAdministrativo == null) ? (other.codTipoActoAdministrativo != null) : !this.codTipoActoAdministrativo.equals(other.codTipoActoAdministrativo)) {
            return false;
        }
        if (this.anio != other.anio && (this.anio == null || !this.anio.equals(other.anio))) {
            return false;
        }
        return true;
    }

}
