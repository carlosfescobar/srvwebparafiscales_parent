package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "parametro.findByFecha",
            query = "SELECT p FROM Parametro p WHERE CURRENT_TIMESTAMP BETWEEN p.fecInicioVigencia AND p.fecFinVigencia")})
@Table(name = "PARAMETRO")
public class Parametro extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "TIPO_MEDIDA")
    private char tipoMedida;
    @Column(name = "VALOR")
    private Long valor;
    @Column(name = "OPERADOR")
    private char operador;
    @Column(name = "FEC_INICIO_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioVigencia;
    @Column(name = "FEC_FIN_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinVigencia;

    public Parametro() {
    }

    public Parametro(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Calendar getFecInicioVigencia() {
        return fecInicioVigencia;
    }

    public void setFecInicioVigencia(Calendar fecInicioVigencia) {
        this.fecInicioVigencia = fecInicioVigencia;
    }

    public char getTipoMedida() {
        return tipoMedida;
    }

    public void setTipoMedida(char tipoMedida) {
        this.tipoMedida = tipoMedida;
    }

    public Long getValor() {
        return valor;
    }

    public void setValor(Long valor) {
        this.valor = valor;
    }

     public char getOperador() {
        return operador;
    }

    public void setOperador(char operador)  {
        this.operador = operador;
    }
    public Calendar getFecFinVigencia() {
        return fecFinVigencia;
    }

    public void setFecFinVigencia(Calendar fecFinVigencia) {
        this.fecFinVigencia = fecFinVigencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parametro)) {
            return false;
        }
        Parametro other = (Parametro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Seguimiento[ id=" + id + " ]";
    }

}
