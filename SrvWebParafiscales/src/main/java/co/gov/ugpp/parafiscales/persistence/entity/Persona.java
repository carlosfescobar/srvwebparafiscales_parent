package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 *
 * @author rpadilla
 */
@Entity
@Cacheable(true)
@Cache(type = CacheType.HARD_WEAK)
@Table(name = "PERSONA")
@NamedQueries({
    @NamedQuery(name = "persona.findByIdentificacion",
            query = "SELECT p FROM Persona p WHERE p.valNumeroIdentificacion = :numero AND p.codTipoIdentificacion.id = :tipo")
})
public class Persona extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "personaIdSeq", sequenceName = "persona_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "COD_SECCION_ACTIVIDAD_ECON", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private SeccionCiiu codSeccionActividadEcon;
    @JoinColumn(name = "COD_DIVISION_ACTIVIDAD_ECO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private DivisionCiiu codDivisionActividadEco;
    @JoinColumn(name = "COD_GRUPO_ACTIVIDAD_ECONOM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private GrupoCiiu codGrupoActividadEconom;
    @JoinColumn(name = "COD_CLASE_ACTIVIDAD_ECONOM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ClaseCiiu codClaseActividadEconom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private String valNumeroIdentificacion;
    @Size(max = 30)
    @Column(name = "VAL_PRIMER_NOMBRE")
    private String valPrimerNombre;
    @Size(max = 30)
    @Column(name = "VAL_SEGUNDO_NOMBRE")
    private String valSegundoNombre;
    @Size(max = 30)
    @Column(name = "VAL_PRIMER_APELLIDO")
    private String valPrimerApellido;
    @Size(max = 30)
    @Column(name = "VAL_SEGUNDO_APELLIDO")
    private String valSegundoApellido;
    @Size(max = 100)
    @Column(name = "VAL_NOMBRE_RAZON_SOCIAL")
    private String valNombreRazonSocial;
    @Column(name = "VAL_NUMERO_TRABAJADORES")
    private Long valNumeroTrabajadores;
    @JoinColumn(name = "COD_ESTADO_CIVIL", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoCivil;
    @JoinColumn(name = "COD_SEXO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codSexo;
    @JoinColumn(name = "COD_NIVEL_EDUCATIVO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codNivelEducativo;
    @JoinColumn(name = "COD_TIPO_IDENTIFICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoIdentificacion;
    @JoinColumn(name = "COD_NATURALEZA_PERSONA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codNaturalezaPersona;
    @JoinColumn(name = "COD_FUENTE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codFuente;
    @JoinColumn(name = "ID_ABOGADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona abogado;
    @JoinColumn(name = "ID_REPRESENTANTE_LEGAL", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona representanteLegal;
    @JoinColumn(name = "ID_AUTORIZADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona autorizado;
    @JoinColumn(name = "ID_MUNICIPIO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Municipio municipio;
    @OneToOne(mappedBy = "persona", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactoPersona contacto;
    @JoinColumn(name = "ID_PERSONA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<ClasificacionPersona> clasificacionesPersona;
    //Atributo discriminador, identifica si la persona es un cotizante, un aportante etc
    @Transient
    private ClasificacionPersonaEnum clasificacionPersona;

    public Persona() {
    }

    public Persona(Long id) {
        this.id = id;
    }

    public Persona(Long id, String valNumeroIdentificacion) {
        this.id = id;
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    @Transient
    public String getNombreCompleto() {
        final StringBuilder valNombreCompleto = new StringBuilder();
        valNombreCompleto.append(this.getValNombreRazonSocial());
        return valNombreCompleto.toString();
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SeccionCiiu getCodSeccionActividadEcon() {
        return codSeccionActividadEcon;
    }

    public void setCodSeccionActividadEcon(SeccionCiiu codSeccionActividadEcon) {
        this.codSeccionActividadEcon = codSeccionActividadEcon;
    }

    public DivisionCiiu getCodDivisionActividadEco() {
        return codDivisionActividadEco;
    }

    public void setCodDivisionActividadEco(DivisionCiiu codDivisionActividadEco) {
        this.codDivisionActividadEco = codDivisionActividadEco;
    }

    public GrupoCiiu getCodGrupoActividadEconom() {
        return codGrupoActividadEconom;
    }

    public void setCodGrupoActividadEconom(GrupoCiiu codGrupoActividadEconom) {
        this.codGrupoActividadEconom = codGrupoActividadEconom;
    }

    public ClaseCiiu getCodClaseActividadEconom() {
        return codClaseActividadEconom;
    }

    public void setCodClaseActividadEconom(ClaseCiiu codClaseActividadEconom) {
        this.codClaseActividadEconom = codClaseActividadEconom;
    }

    public String getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    public void setValNumeroIdentificacion(String valNumeroIdentificacion) {
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    public String getValPrimerNombre() {
        return valPrimerNombre;
    }

    public void setValPrimerNombre(String valPrimerNombre) {
        this.valPrimerNombre = valPrimerNombre;
    }

    public String getValSegundoNombre() {
        return valSegundoNombre;
    }

    public void setValSegundoNombre(String valSegundoNombre) {
        this.valSegundoNombre = valSegundoNombre;
    }

    public String getValPrimerApellido() {
        return valPrimerApellido;
    }

    public void setValPrimerApellido(String valPrimerApellido) {
        this.valPrimerApellido = valPrimerApellido;
    }

    public String getValSegundoApellido() {
        return valSegundoApellido;
    }

    public void setValSegundoApellido(String valSegundoApellido) {
        this.valSegundoApellido = valSegundoApellido;
    }

    public String getValNombreRazonSocial() {
        return valNombreRazonSocial;
    }

    public void setValNombreRazonSocial(String valNombreRazonSocial) {
        this.valNombreRazonSocial = valNombreRazonSocial;
    }

    public Long getValNumeroTrabajadores() {
        return valNumeroTrabajadores;
    }

    public void setValNumeroTrabajadores(Long valNumeroTrabajadores) {
        this.valNumeroTrabajadores = valNumeroTrabajadores;
    }

    public ValorDominio getCodEstadoCivil() {
        return codEstadoCivil;

    }

    public void setCodEstadoCivil(ValorDominio codEstadoCivil) {
        this.codEstadoCivil = codEstadoCivil;
    }

    public ValorDominio getCodSexo() {
        return codSexo;
    }

    public void setCodSexo(ValorDominio codSexo) {
        this.codSexo = codSexo;
    }

    public ValorDominio getCodNivelEducativo() {
        return codNivelEducativo;
    }

    public void setCodNivelEducativo(ValorDominio codNivelEducativo) {
        this.codNivelEducativo = codNivelEducativo;
    }

    public ValorDominio getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(ValorDominio codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public Persona getAbogado() {
        return abogado;
    }

    public void setAbogado(Persona abogado) {
        this.abogado = abogado;
    }

    public Persona getRepresentanteLegal() {
        return representanteLegal;
    }

    public void setRepresentanteLegal(Persona idRepresentanteLegal) {
        this.representanteLegal = idRepresentanteLegal;
    }

    public Persona getAutorizado() {
        return autorizado;
    }

    public void setAutorizado(Persona autorizado) {
        this.autorizado = autorizado;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public ContactoPersona getContacto() {
        return contacto;
    }

    public void setContacto(ContactoPersona contacto) {
        this.contacto = contacto;
    }

    public ValorDominio getCodNaturalezaPersona() {
        return codNaturalezaPersona;
    }

    public void setCodNaturalezaPersona(ValorDominio codNaturalezaPersona) {
        this.codNaturalezaPersona = codNaturalezaPersona;
    }

    public ValorDominio getCodFuente() {
        return codFuente;
    }

    public void setCodFuente(ValorDominio codFuente) {
        this.codFuente = codFuente;
    }

    public List<ClasificacionPersona> getClasificacionesPersona() {
        if(clasificacionesPersona == null){
            clasificacionesPersona = new ArrayList<ClasificacionPersona>();
        }
        return clasificacionesPersona;
    }

    public void setClasificacionesPersona(List<ClasificacionPersona> clasificacionesPersona) {
        this.clasificacionesPersona = clasificacionesPersona;
    }

    public ClasificacionPersonaEnum getClasificacionPersona() {
        return clasificacionPersona;
    }

    public void setClasificacionPersona(ClasificacionPersonaEnum clasificacionPersona) {
        this.clasificacionPersona = clasificacionPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof PersonaNaturalTipo) {
            final PersonaNaturalTipo other = (PersonaNaturalTipo) object;
            final IdentificacionTipo otherIdPersona = other.getIdPersona();
            return otherIdPersona != null
                    && this.valNumeroIdentificacion.equals(otherIdPersona.getValNumeroIdentificacion())
                    && this.codTipoIdentificacion != null
                    && this.codTipoIdentificacion.getId().equals(otherIdPersona.getCodTipoIdentificacion());
        }
        if (object instanceof PersonaJuridicaTipo) {
            final PersonaJuridicaTipo other = (PersonaJuridicaTipo) object;
            final IdentificacionTipo otherIdPersona = other.getIdPersona();
            return otherIdPersona != null
                    && this.valNumeroIdentificacion.equals(otherIdPersona.getValNumeroIdentificacion())
                    && this.codTipoIdentificacion != null
                    && this.codTipoIdentificacion.getId().equals(otherIdPersona.getCodTipoIdentificacion());
        }
        if (!(object instanceof Persona)) {
            return false;
        }
        final Persona other = (Persona) object;
        if (this.id != null && this.id.equals(other.id)) {
            return true;
        }
        return this.valNumeroIdentificacion != null
                && this.valNumeroIdentificacion.equals(other.valNumeroIdentificacion)
                && this.codTipoIdentificacion != null
                && this.codTipoIdentificacion.equals(other.codTipoIdentificacion);
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Persona[ id=" + id + " ]";
    }

}
