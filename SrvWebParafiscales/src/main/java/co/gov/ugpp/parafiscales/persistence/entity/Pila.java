package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seq_pila", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_PILA")
public class Pila extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    @Id
    @SequenceGenerator(name = "pila_seq", sequenceName = "pila_seq_id", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pila_seq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "FECHACREACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechacreacion;
    @Size(max = 2)
    @Column(name = "ESTADO")
    private String estado;
    @JoinColumn(name = "IDEXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Expediente idexpediente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idpila")
    private List<PilaDetalle> pilaDetalleList;

    public Pila() {
    }

    public Pila(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFechacreacion() {
        return fechacreacion;
    }

    public void setFechacreacion(Date fechacreacion) {
        this.fechacreacion = fechacreacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Expediente getIdexpediente() {
        return idexpediente;
    }

    public void setIdexpediente(Expediente idexpediente) {
        this.idexpediente = idexpediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pila)) {
            return false;
        }
        Pila other = (Pila) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.Pila[ id=" + id + " ]";
    }

    /**
     * @return the pilaDetalleList
     */
    public List<PilaDetalle> getPilaDetalleList() {
        return pilaDetalleList;
    }

    /**
     * @param pilaDetalleList the pilaDetalleList to set
     */
    public void setPilaDetalleList(List<PilaDetalle> pilaDetalleList) {
        this.pilaDetalleList = pilaDetalleList;
    }

}
