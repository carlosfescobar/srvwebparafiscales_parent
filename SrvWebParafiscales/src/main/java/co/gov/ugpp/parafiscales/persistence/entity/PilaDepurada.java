/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_PILA_DEPURADA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PilaDepurada.findByLiquidar", query = "SELECT p FROM PilaDepurada p WHERE p.nit = :nit and p.periodoResto >= :periodo_inicial and p.periodoResto <= :periodo_final")
})
public class PilaDepurada extends AbstractEntity<Integer> {

    private static final long serialVersionUID = 1L;
    @Size(max = 50)
    @Column(name = "NIT")
    private String nit;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "periodo_salud")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoSalud;
    @Size(max = 7)
    @Column(name = "periodo_resto")
    private String periodoResto;
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPago;
    @Size(max = 2147483647)
    @Column(name = "codigo_operador")
    private String codigoOperador;
    @Column(name = "tipo_aportante")
    private Short tipoAportante;
    @Size(max = 2147483647)
    @Column(name = "codigo_arp")
    private String codigoArp;
    @Size(max = 1)
    @Column(name = "tipo_planilla")
    private String tipoPlanilla;
    @Size(max = 2147483647)
    @Column(name = "planilla_asociada")
    private String planillaAsociada;
    @Column(name = "fecha_asociada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsociada;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 16)
    @Column(name = "numero_identificacion")
    private String numeroIdentificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2)
    @Column(name = "tipo_identificacion")
    private String tipoIdentificacion;
    @Column(name = "tipo_cotizante")
    private Short tipoCotizante;
    @Column(name = "subtipo_cotizante")
    private Short subtipoCotizante;
    @Size(max = 1)
    @Column(name = "extranjero_nopension")
    private String extranjeroNopension;
    @Size(max = 1)
    @Column(name = "colombiano_exterior")
    private String colombianoExterior;
    @Size(max = 20)
    @Column(name = "primer_nombre")
    private String primerNombre;
    @Size(max = 30)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Size(max = 20)
    @Column(name = "primer_apellido")
    private String primerApellido;
    @Size(max = 30)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    @Size(max = 1)
    @Column(name = "cambio_tarifa_especial")
    private String cambioTarifaEspecial;
    @Size(max = 1)
    @Column(name = "ingreso")
    private String ingreso;
    @Size(max = 1)
    @Column(name = "retiro")
    private String retiro;
    @Size(max = 1)
    @Column(name = "variacion_permanente_salario")
    private String variacionPermanenteSalario;
    @Size(max = 1)
    @Column(name = "variacion_transitoria_salario")
    private String variacionTransitoriaSalario;
    @Size(max = 1)
    @Column(name = "aporte_voluntario")
    private String aporteVoluntario;
    @Size(max = 1)
    @Column(name = "suspension_temporal")
    private String suspensionTemporal;
    @Size(max = 1)
    @Column(name = "incapacidad_general")
    private String incapacidadGeneral;
    @Size(max = 1)
    @Column(name = "licencia_maternidad")
    private String licenciaMaternidad;
    @Size(max = 1)
    @Column(name = "vacaciones")
    private String vacaciones;
    @Size(max = 2)
    @Column(name = "incapacidas_por_trabajo")
    private String incapacidasPorTrabajo;
    @Size(max = 1)
    @Column(name = "variacion_centros_trabajo")
    private String variacionCentrosTrabajo;
    @Size(max = 1)
    @Column(name = "traslado_aEPS")
    private String trasladoaEPS;
    @Size(max = 1)
    @Column(name = "traslado_aAFP")
    private String trasladoaAFP;
    @Size(max = 1)
    @Column(name = "traslado_desdeEPS")
    private String trasladodesdeEPS;
    @Size(max = 1)
    @Column(name = "traslado_desdeAFP")
    private String trasladodesdeAFP;
    @Column(name = "sector_aportante")
    private Short sectorAportante;
    @Size(max = 1)
    @Column(name = "clase_aportante")
    private String claseAportante;
    @Column(name = "codigo_depto")
    private Long codigoDepto;
    @Column(name = "fecha_matricula_mercantil")
    @Temporal(TemporalType.DATE)
    private Date fechaMatriculaMercantil;
    @Size(max = 1)
    @Column(name = "tipo_persona")
    private String tipoPersona;
    @Column(name = "periodo_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoPago;
    @Size(max = 6)
    @Column(name = "codigo_AFP")
    private String codigoAFP;
    @Size(max = 6)
    @Column(name = "codigo_EPS")
    private String codigoEPS;
    @Size(max = 6)
    @Column(name = "codigo_CCF")
    private String codigoCCF;
    @Column(name = "dias_cot_pension")
    private Integer diasCotPension;
    @Column(name = "dias_cot_salud")
    private Integer diasCotSalud;
    @Column(name = "dias_cot_rprof")
    private Integer diasCotRprof;
    @Column(name = "dias_cot_ccf")
    private Integer diasCotCcf;
    @Column(name = "salario_basico")
    private Integer salarioBasico;
    @Size(max = 1)
    @Column(name = "salario_integral")
    private String salarioIntegral;
    @Column(name = "ibc_pension")
    private Integer ibcPension;
    @Column(name = "ibc_salud")
    private Integer ibcSalud;
    @Column(name = "ibc_rprof")
    private Integer ibcRprof;
    @Column(name = "ibc_ccf")
    private Integer ibcCcf;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "tarifa_pension")
    private BigDecimal tarifaPension;
    @Column(name = "aporte_cot_obligatoria_pension")
    private Integer aporteCotObligatoriaPension;
    @Column(name = "aporte_fsolid_pensional_sol")
    private Integer aporteFsolidPensionalSolidaridad;
    @Column(name = "aporte_fsolid_pensional_sub")
    private Integer aporteFsolidPensionalSubsistencia;
    @Column(name = "total_FSP")
    private BigInteger totalFSP;
    @Column(name = "tarifa_salud")
    private BigDecimal tarifaSalud;
    @Column(name = "cot_obligatoria_salud")
    private Integer cotObligatoriaSalud;
    @Column(name = "valor_autoriza_lic_maternidad")
    private BigInteger valorAutorizaLicMaternidad;
    @Column(name = "tarifa_centro_trabajo")
    private BigDecimal tarifaCentroTrabajo;
    @Column(name = "cot_obligatoria_arp")
    private Integer cotObligatoriaArp;
    @Column(name = "tarifa_aportes_ccf")
    private BigDecimal tarifaAportesCcf;
    @Column(name = "valor_aportes_ccf_ibc_tarifa")
    private Integer valorAportesCcfIbcTarifa;
    @Column(name = "tarifa_aportes_sena")
    private BigDecimal tarifaAportesSena;
    @Column(name = "valor_aportes_parafiscales_sen")
    private Integer valorAportesParafiscalesSena;
    @Column(name = "tarifa_aportes_icbf")
    private BigDecimal tarifaAportesIcbf;
    @Column(name = "valor_aportes_parafiscales_icb")
    private Integer valorAportesParafiscalesIcbf;
    @Column(name = "tarifa_aportes_esap")
    private BigDecimal tarifaAportesEsap;
    @Column(name = "valor_aportes_esap")
    private Integer valorAportesEsap;
    @Column(name = "tarifa_aportes_mined")
    private BigDecimal tarifaAportesMined;
    @Column(name = "valor_aportes_mined")
    private Integer valorAportesMined;
    @Size(max = 2147483647)
    @Column(name = "planilla")
    private String planilla;
    @Column(name = "aporte_vol_afiliado")
    private BigInteger aporteVolAfiliado;
    @Column(name = "cot_vol_aportante")
    private BigInteger cotVolAportante;
    @Column(name = "total_cotizacion")
    private Long totalCotizacion;
    @Column(name = "valor_autoriza_incapa_EG")
    private BigInteger valorautorizaincapaEG;
    @Size(max = 4)
    @Column(name = "ANO_RESTO")
    private String anoResto;
    @Size(max = 2)
    @Column(name = "MES_RESTO")
    private String mesResto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "Apo_pago_salud_ley_1607_2000")
    private String apoexopagosaludley16072000;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "A_ben_art_5_ley_1429_2000_CCF")
    private String apoacobeneficiosart5ley14292000CCF;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1)
    @Column(name = "Cot_salud_ley_1607_2012")
    private String cotexopagoparafsaludley16072012;
    @Size(max = 20)
    @Column(name = "ESTADO_AFILIADO")
    private String estadoAfiliado;
    @Column(name = "MIN_FECHA_AFILIACION_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date minFechaAfiliacionPension;
    @Size(max = 2147483647)
    @Column(name = "MIN_NOMBRE_ADMIN_PENSION")
    private String minNombreAdminPension;
    @Column(name = "MAX_FECHA_AFILIACION_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date maxFechaAfiliacionPension;
    @Size(max = 2147483647)
    @Column(name = "MAX_NOMBRE_ADMIN_PENSION")
    private String maxNombreAdminPension;
    @Column(name = "MAX_FECHA_INICIO_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date maxFechaInicioPension;
    @Size(max = 100)
    @Column(name = "TIPO_PENSION")
    private String tipoPension;
    @Size(max = 2147483647)
    @Column(name = "NOMBRE_ADMIN_PENSIONADO")
    private String nombreAdminPensionado;
    @Column(name = "CESANTE")
    private Short cesante;
    @Size(max = 6)
    @Column(name = "RUA_PERIODO")
    private String ruaPeriodo;
    @Size(max = 109)
    @Column(name = "ULTIMA_AFILIACION_RUA")
    private String ultimaAfiliacionRua;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    
    @Column(name = "TARIFA_MAXIMA")
    private String tarifaMaxima;
    
    
    @Column(name = "APOR_COR_OBLI_PEN_ALT_RIESGO")
    private String aporCorObliPenAltRiesgo;

    public String getAporCorObliPenAltRiesgo() {
        return aporCorObliPenAltRiesgo;
    }

    public void setAporCorObliPenAltRiesgo(String aporCorObliPenAltRiesgo) {
        this.aporCorObliPenAltRiesgo = aporCorObliPenAltRiesgo;
    }
    
    
    @Column(name = "TARI_PILA_PENS_AD_ALTO_RIESGO")
    private String tariPilaPensAdAltoRiesgo;

    public String getTariPilaPensAdAltoRiesgo() {
        return tariPilaPensAdAltoRiesgo;
    }

    public void setTariPilaPensAdAltoRiesgo(String tariPilaPensAdAltoRiesgo) {
        this.tariPilaPensAdAltoRiesgo = tariPilaPensAdAltoRiesgo;
    }
    

    public PilaDepurada() {
    }

    public PilaDepurada(Integer id) {
        this.id = id;
    }

    public PilaDepurada(Integer id, String razonSocial, String numeroIdentificacion, String tipoIdentificacion, String apoexopagosaludley16072000, String apoacobeneficiosart5ley14292000CCF, String cotexopagoparafsaludley16072012) {
        this.id = id;
        this.razonSocial = razonSocial;
        this.numeroIdentificacion = numeroIdentificacion;
        this.tipoIdentificacion = tipoIdentificacion;
        this.apoexopagosaludley16072000 = apoexopagosaludley16072000;
        this.apoacobeneficiosart5ley14292000CCF = apoacobeneficiosart5ley14292000CCF;
        this.cotexopagoparafsaludley16072012 = cotexopagoparafsaludley16072012;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public Date getPeriodoSalud() {
        return periodoSalud;
    }

    public void setPeriodoSalud(Date periodoSalud) {
        this.periodoSalud = periodoSalud;
    }

    public String getPeriodoResto() {
        return periodoResto;
    }

    public void setPeriodoResto(String periodoResto) {
        this.periodoResto = periodoResto;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getCodigoOperador() {
        return codigoOperador;
    }

    public void setCodigoOperador(String codigoOperador) {
        this.codigoOperador = codigoOperador;
    }

    public Short getTipoAportante() {
        return tipoAportante;
    }

    public void setTipoAportante(Short tipoAportante) {
        this.tipoAportante = tipoAportante;
    }

    public String getCodigoArp() {
        return codigoArp;
    }

    public void setCodigoArp(String codigoArp) {
        this.codigoArp = codigoArp;
    }

    public String getTipoPlanilla() {
        return tipoPlanilla;
    }

    public void setTipoPlanilla(String tipoPlanilla) {
        this.tipoPlanilla = tipoPlanilla;
    }

    public String getPlanillaAsociada() {
        return planillaAsociada;
    }

    public void setPlanillaAsociada(String planillaAsociada) {
        this.planillaAsociada = planillaAsociada;
    }

    public Date getFechaAsociada() {
        return fechaAsociada;
    }

    public void setFechaAsociada(Date fechaAsociada) {
        this.fechaAsociada = fechaAsociada;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public Short getTipoCotizante() {
        return tipoCotizante;
    }

    public void setTipoCotizante(Short tipoCotizante) {
        this.tipoCotizante = tipoCotizante;
    }

    public Short getSubtipoCotizante() {
        return subtipoCotizante;
    }

    public void setSubtipoCotizante(Short subtipoCotizante) {
        this.subtipoCotizante = subtipoCotizante;
    }

    public String getExtranjeroNopension() {
        return extranjeroNopension;
    }

    public void setExtranjeroNopension(String extranjeroNopension) {
        this.extranjeroNopension = extranjeroNopension;
    }

    public String getColombianoExterior() {
        return colombianoExterior;
    }

    public void setColombianoExterior(String colombianoExterior) {
        this.colombianoExterior = colombianoExterior;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getCambioTarifaEspecial() {
        return cambioTarifaEspecial;
    }

    public void setCambioTarifaEspecial(String cambioTarifaEspecial) {
        this.cambioTarifaEspecial = cambioTarifaEspecial;
    }

    public String getIngreso() {
        return ingreso;
    }

    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    public String getRetiro() {
        return retiro;
    }

    public void setRetiro(String retiro) {
        this.retiro = retiro;
    }

    public String getVariacionPermanenteSalario() {
        return variacionPermanenteSalario;
    }

    public void setVariacionPermanenteSalario(String variacionPermanenteSalario) {
        this.variacionPermanenteSalario = variacionPermanenteSalario;
    }

    public String getVariacionTransitoriaSalario() {
        return variacionTransitoriaSalario;
    }

    public void setVariacionTransitoriaSalario(String variacionTransitoriaSalario) {
        this.variacionTransitoriaSalario = variacionTransitoriaSalario;
    }

    public String getAporteVoluntario() {
        return aporteVoluntario;
    }

    public void setAporteVoluntario(String aporteVoluntario) {
        this.aporteVoluntario = aporteVoluntario;
    }

    public String getSuspensionTemporal() {
        return suspensionTemporal;
    }

    public void setSuspensionTemporal(String suspensionTemporal) {
        this.suspensionTemporal = suspensionTemporal;
    }

    public String getIncapacidadGeneral() {
        return incapacidadGeneral;
    }

    public void setIncapacidadGeneral(String incapacidadGeneral) {
        this.incapacidadGeneral = incapacidadGeneral;
    }

    public String getLicenciaMaternidad() {
        return licenciaMaternidad;
    }

    public void setLicenciaMaternidad(String licenciaMaternidad) {
        this.licenciaMaternidad = licenciaMaternidad;
    }

    public String getVacaciones() {
        return vacaciones;
    }

    public void setVacaciones(String vacaciones) {
        this.vacaciones = vacaciones;
    }

    public String getIncapacidasPorTrabajo() {
        return incapacidasPorTrabajo;
    }

    public void setIncapacidasPorTrabajo(String incapacidasPorTrabajo) {
        this.incapacidasPorTrabajo = incapacidasPorTrabajo;
    }

    public String getVariacionCentrosTrabajo() {
        return variacionCentrosTrabajo;
    }

    public void setVariacionCentrosTrabajo(String variacionCentrosTrabajo) {
        this.variacionCentrosTrabajo = variacionCentrosTrabajo;
    }

    public String getTrasladoaEPS() {
        return trasladoaEPS;
    }

    public void setTrasladoaEPS(String trasladoaEPS) {
        this.trasladoaEPS = trasladoaEPS;
    }

    public String getTrasladoaAFP() {
        return trasladoaAFP;
    }

    public void setTrasladoaAFP(String trasladoaAFP) {
        this.trasladoaAFP = trasladoaAFP;
    }

    public String getTrasladodesdeEPS() {
        return trasladodesdeEPS;
    }

    public void setTrasladodesdeEPS(String trasladodesdeEPS) {
        this.trasladodesdeEPS = trasladodesdeEPS;
    }

    public String getTrasladodesdeAFP() {
        return trasladodesdeAFP;
    }

    public void setTrasladodesdeAFP(String trasladodesdeAFP) {
        this.trasladodesdeAFP = trasladodesdeAFP;
    }

    public Short getSectorAportante() {
        return sectorAportante;
    }

    public void setSectorAportante(Short sectorAportante) {
        this.sectorAportante = sectorAportante;
    }

    public String getClaseAportante() {
        return claseAportante;
    }

    public void setClaseAportante(String claseAportante) {
        this.claseAportante = claseAportante;
    }

    public Long getCodigoDepto() {
        return codigoDepto;
    }

    public void setCodigoDepto(Long codigoDepto) {
        this.codigoDepto = codigoDepto;
    }

    public Date getFechaMatriculaMercantil() {
        return fechaMatriculaMercantil;
    }

    public void setFechaMatriculaMercantil(Date fechaMatriculaMercantil) {
        this.fechaMatriculaMercantil = fechaMatriculaMercantil;
    }

    public String getTipoPersona() {
        return tipoPersona;
    }

    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    public Date getPeriodoPago() {
        return periodoPago;
    }

    public void setPeriodoPago(Date periodoPago) {
        this.periodoPago = periodoPago;
    }

    public String getCodigoAFP() {
        return codigoAFP;
    }

    public void setCodigoAFP(String codigoAFP) {
        this.codigoAFP = codigoAFP;
    }

    public String getCodigoEPS() {
        return codigoEPS;
    }

    public void setCodigoEPS(String codigoEPS) {
        this.codigoEPS = codigoEPS;
    }

    public String getCodigoCCF() {
        return codigoCCF;
    }

    public void setCodigoCCF(String codigoCCF) {
        this.codigoCCF = codigoCCF;
    }

    public Integer getDiasCotPension() {
        return diasCotPension;
    }

    public void setDiasCotPension(Integer diasCotPension) {
        this.diasCotPension = diasCotPension;
    }

    public Integer getDiasCotSalud() {
        return diasCotSalud;
    }

    public void setDiasCotSalud(Integer diasCotSalud) {
        this.diasCotSalud = diasCotSalud;
    }

    public Integer getDiasCotRprof() {
        return diasCotRprof;
    }

    public void setDiasCotRprof(Integer diasCotRprof) {
        this.diasCotRprof = diasCotRprof;
    }

    public Integer getDiasCotCcf() {
        return diasCotCcf;
    }

    public void setDiasCotCcf(Integer diasCotCcf) {
        this.diasCotCcf = diasCotCcf;
    }

    public Integer getSalarioBasico() {
        return salarioBasico;
    }

    public void setSalarioBasico(Integer salarioBasico) {
        this.salarioBasico = salarioBasico;
    }

    public String getSalarioIntegral() {
        return salarioIntegral;
    }

    public void setSalarioIntegral(String salarioIntegral) {
        this.salarioIntegral = salarioIntegral;
    }

    public Integer getIbcPension() {
        return ibcPension;
    }

    public void setIbcPension(Integer ibcPension) {
        this.ibcPension = ibcPension;
    }

    public Integer getIbcSalud() {
        return ibcSalud;
    }

    public void setIbcSalud(Integer ibcSalud) {
        this.ibcSalud = ibcSalud;
    }

    public Integer getIbcRprof() {
        return ibcRprof;
    }

    public void setIbcRprof(Integer ibcRprof) {
        this.ibcRprof = ibcRprof;
    }

    public Integer getIbcCcf() {
        return ibcCcf;
    }

    public void setIbcCcf(Integer ibcCcf) {
        this.ibcCcf = ibcCcf;
    }

    public BigDecimal getTarifaPension() {
        return tarifaPension;
    }

    public void setTarifaPension(BigDecimal tarifaPension) {
        this.tarifaPension = tarifaPension;
    }

    public Integer getAporteCotObligatoriaPension() {
        return aporteCotObligatoriaPension;
    }

    public void setAporteCotObligatoriaPension(Integer aporteCotObligatoriaPension) {
        this.aporteCotObligatoriaPension = aporteCotObligatoriaPension;
    }

    public Integer getAporteFsolidPensionalSolidaridad() {
        return aporteFsolidPensionalSolidaridad;
    }

    public void setAporteFsolidPensionalSolidaridad(Integer aporteFsolidPensionalSolidaridad) {
        this.aporteFsolidPensionalSolidaridad = aporteFsolidPensionalSolidaridad;
    }

    public Integer getAporteFsolidPensionalSubsistencia() {
        return aporteFsolidPensionalSubsistencia;
    }

    public void setAporteFsolidPensionalSubsistencia(Integer aporteFsolidPensionalSubsistencia) {
        this.aporteFsolidPensionalSubsistencia = aporteFsolidPensionalSubsistencia;
    }

    public BigInteger getTotalFSP() {
        return totalFSP;
    }

    public void setTotalFSP(BigInteger totalFSP) {
        this.totalFSP = totalFSP;
    }

    public BigDecimal getTarifaSalud() {
        return tarifaSalud;
    }

    public void setTarifaSalud(BigDecimal tarifaSalud) {
        this.tarifaSalud = tarifaSalud;
    }

    public Integer getCotObligatoriaSalud() {
        return cotObligatoriaSalud;
    }

    public void setCotObligatoriaSalud(Integer cotObligatoriaSalud) {
        this.cotObligatoriaSalud = cotObligatoriaSalud;
    }

    public BigInteger getValorAutorizaLicMaternidad() {
        return valorAutorizaLicMaternidad;
    }

    public void setValorAutorizaLicMaternidad(BigInteger valorAutorizaLicMaternidad) {
        this.valorAutorizaLicMaternidad = valorAutorizaLicMaternidad;
    }

    public BigDecimal getTarifaCentroTrabajo() {
        return tarifaCentroTrabajo;
    }

    public void setTarifaCentroTrabajo(BigDecimal tarifaCentroTrabajo) {
        this.tarifaCentroTrabajo = tarifaCentroTrabajo;
    }

    public Integer getCotObligatoriaArp() {
        return cotObligatoriaArp;
    }

    public void setCotObligatoriaArp(Integer cotObligatoriaArp) {
        this.cotObligatoriaArp = cotObligatoriaArp;
    }

    public BigDecimal getTarifaAportesCcf() {
        return tarifaAportesCcf;
    }

    public void setTarifaAportesCcf(BigDecimal tarifaAportesCcf) {
        this.tarifaAportesCcf = tarifaAportesCcf;
    }

    public Integer getValorAportesCcfIbcTarifa() {
        return valorAportesCcfIbcTarifa;
    }

    public void setValorAportesCcfIbcTarifa(Integer valorAportesCcfIbcTarifa) {
        this.valorAportesCcfIbcTarifa = valorAportesCcfIbcTarifa;
    }

    public BigDecimal getTarifaAportesSena() {
        return tarifaAportesSena;
    }

    public void setTarifaAportesSena(BigDecimal tarifaAportesSena) {
        this.tarifaAportesSena = tarifaAportesSena;
    }

    public Integer getValorAportesParafiscalesSena() {
        return valorAportesParafiscalesSena;
    }

    public void setValorAportesParafiscalesSena(Integer valorAportesParafiscalesSena) {
        this.valorAportesParafiscalesSena = valorAportesParafiscalesSena;
    }

    public BigDecimal getTarifaAportesIcbf() {
        return tarifaAportesIcbf;
    }

    public void setTarifaAportesIcbf(BigDecimal tarifaAportesIcbf) {
        this.tarifaAportesIcbf = tarifaAportesIcbf;
    }

    public Integer getValorAportesParafiscalesIcbf() {
        return valorAportesParafiscalesIcbf;
    }

    public void setValorAportesParafiscalesIcbf(Integer valorAportesParafiscalesIcbf) {
        this.valorAportesParafiscalesIcbf = valorAportesParafiscalesIcbf;
    }

    public BigDecimal getTarifaAportesEsap() {
        return tarifaAportesEsap;
    }

    public void setTarifaAportesEsap(BigDecimal tarifaAportesEsap) {
        this.tarifaAportesEsap = tarifaAportesEsap;
    }

    public Integer getValorAportesEsap() {
        return valorAportesEsap;
    }

    public void setValorAportesEsap(Integer valorAportesEsap) {
        this.valorAportesEsap = valorAportesEsap;
    }

    public BigDecimal getTarifaAportesMined() {
        return tarifaAportesMined;
    }

    public void setTarifaAportesMined(BigDecimal tarifaAportesMined) {
        this.tarifaAportesMined = tarifaAportesMined;
    }

    public Integer getValorAportesMined() {
        return valorAportesMined;
    }

    public void setValorAportesMined(Integer valorAportesMined) {
        this.valorAportesMined = valorAportesMined;
    }

    public String getPlanilla() {
        return planilla;
    }

    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    public BigInteger getAporteVolAfiliado() {
        return aporteVolAfiliado;
    }

    public void setAporteVolAfiliado(BigInteger aporteVolAfiliado) {
        this.aporteVolAfiliado = aporteVolAfiliado;
    }

    public BigInteger getCotVolAportante() {
        return cotVolAportante;
    }

    public void setCotVolAportante(BigInteger cotVolAportante) {
        this.cotVolAportante = cotVolAportante;
    }

    public Long getTotalCotizacion() {
        return totalCotizacion;
    }

    public void setTotalCotizacion(Long totalCotizacion) {
        this.totalCotizacion = totalCotizacion;
    }

    public BigInteger getValorautorizaincapaEG() {
        return valorautorizaincapaEG;
    }

    public void setValorautorizaincapaEG(BigInteger valorautorizaincapaEG) {
        this.valorautorizaincapaEG = valorautorizaincapaEG;
    }

    public String getAnoResto() {
        return anoResto;
    }

    public void setAnoResto(String anoResto) {
        this.anoResto = anoResto;
    }

    public String getMesResto() {
        return mesResto;
    }

    public void setMesResto(String mesResto) {
        this.mesResto = mesResto;
    }

    public String getApoexopagosaludley16072000() {
        return apoexopagosaludley16072000;
    }

    public void setApoexopagosaludley16072000(String apoexopagosaludley16072000) {
        this.apoexopagosaludley16072000 = apoexopagosaludley16072000;
    }

    public String getApoacobeneficiosart5ley14292000CCF() {
        return apoacobeneficiosart5ley14292000CCF;
    }

    public void setApoacobeneficiosart5ley14292000CCF(String apoacobeneficiosart5ley14292000CCF) {
        this.apoacobeneficiosart5ley14292000CCF = apoacobeneficiosart5ley14292000CCF;
    }

    public String getCotexopagoparafsaludley16072012() {
        return cotexopagoparafsaludley16072012;
    }

    public void setCotexopagoparafsaludley16072012(String cotexopagoparafsaludley16072012) {
        this.cotexopagoparafsaludley16072012 = cotexopagoparafsaludley16072012;
    }

    public String getEstadoAfiliado() {
        return estadoAfiliado;
    }

    public void setEstadoAfiliado(String estadoAfiliado) {
        this.estadoAfiliado = estadoAfiliado;
    }

    public Date getMinFechaAfiliacionPension() {
        return minFechaAfiliacionPension;
    }

    public void setMinFechaAfiliacionPension(Date minFechaAfiliacionPension) {
        this.minFechaAfiliacionPension = minFechaAfiliacionPension;
    }

    public String getMinNombreAdminPension() {
        return minNombreAdminPension;
    }

    public void setMinNombreAdminPension(String minNombreAdminPension) {
        this.minNombreAdminPension = minNombreAdminPension;
    }

    public Date getMaxFechaAfiliacionPension() {
        return maxFechaAfiliacionPension;
    }

    public void setMaxFechaAfiliacionPension(Date maxFechaAfiliacionPension) {
        this.maxFechaAfiliacionPension = maxFechaAfiliacionPension;
    }

    public String getMaxNombreAdminPension() {
        return maxNombreAdminPension;
    }

    public void setMaxNombreAdminPension(String maxNombreAdminPension) {
        this.maxNombreAdminPension = maxNombreAdminPension;
    }

    public Date getMaxFechaInicioPension() {
        return maxFechaInicioPension;
    }

    public void setMaxFechaInicioPension(Date maxFechaInicioPension) {
        this.maxFechaInicioPension = maxFechaInicioPension;
    }

    public String getTipoPension() {
        return tipoPension;
    }

    public void setTipoPension(String tipoPension) {
        this.tipoPension = tipoPension;
    }

    public String getNombreAdminPensionado() {
        return nombreAdminPensionado;
    }

    public void setNombreAdminPensionado(String nombreAdminPensionado) {
        this.nombreAdminPensionado = nombreAdminPensionado;
    }

    public Short getCesante() {
        return cesante;
    }

    public void setCesante(Short cesante) {
        this.cesante = cesante;
    }

    public String getRuaPeriodo() {
        return ruaPeriodo;
    }

    public void setRuaPeriodo(String ruaPeriodo) {
        this.ruaPeriodo = ruaPeriodo;
    }

    public String getUltimaAfiliacionRua() {
        return ultimaAfiliacionRua;
    }

    public void setUltimaAfiliacionRua(String ultimaAfiliacionRua) {
        this.ultimaAfiliacionRua = ultimaAfiliacionRua;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PilaDepurada)) {
            return false;
        }
        PilaDepurada other = (PilaDepurada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.PilaDepurada[ id=" + id + " ]";
    }

    /**
     * @return the tarifaMaxima
     */
    public String getTarifaMaxima() {
        return tarifaMaxima;
    }

    /**
     * @param tarifaMaxima the tarifaMaxima to set
     */
    public void setTarifaMaxima(String tarifaMaxima) {
        this.tarifaMaxima = tarifaMaxima;
    }

}
