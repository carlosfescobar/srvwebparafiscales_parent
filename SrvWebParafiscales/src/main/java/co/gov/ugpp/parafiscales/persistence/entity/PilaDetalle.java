package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * 
 * @author franzjr
 */

@Entity
@TableGenerator(name = "seq_pila_detalle", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_PILA_DETALLE")
public class PilaDetalle extends AbstractEntity<BigDecimal> {
    private static long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seq_pila_detalle")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private BigDecimal id;
        
    //@Size(max = 50)
    @Column(name = "NIT")
    private String nit;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 1)
    @Column(name = "razon_social")
    private String razonSocial;
    @Column(name = "periodo_salud")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoSalud;
    //@Size(max = 7)
    @Column(name = "periodo_resto")
    private String periodoResto;
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaPago;
    //@Size(max = 2147483647)
    @Column(name = "codigo_operador")
    private String codigoOperador;
    @Column(name = "tipo_aportante")
    private Short tipoAportante;
    //@Size(max = 2147483647)
    @Column(name = "codigo_arp")
    private String codigoArp;
    //@Size(max = 1)
    @Column(name = "tipo_planilla")
    private String tipoPlanilla;
    //@Size(max = 2147483647)
    @Column(name = "planilla_asociada")
    private String planillaAsociada;
    @Column(name = "fecha_asociada")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsociada;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 16)
    @Column(name = "numero_identificacion")
    private String numeroIdentificacion;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 2)
    @Column(name = "tipo_identificacion")
    private String tipoIdentificacion;
    @Column(name = "tipo_cotizante")
    private Short tipoCotizante;
    @Column(name = "subtipo_cotizante")
    private Short subtipoCotizante;
    //@Size(max = 1)
    @Column(name = "extranjero_nopension")
    private String extranjeroNopension;
    //@Size(max = 1)
    @Column(name = "colombiano_exterior")
    private String colombianoExterior;
    //@Size(max = 20)
    @Column(name = "primer_nombre")
    private String primerNombre;
    //@Size(max = 30)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    //@Size(max = 20)
    @Column(name = "primer_apellido")
    private String primerApellido;
    //@Size(max = 30)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    //@Size(max = 1)
    @Column(name = "cambio_tarifa_especial")
    private String cambioTarifaEspecial;
    //@Size(max = 1)
    @Column(name = "ingreso")
    private String ingreso;
    //@Size(max = 1)
    @Column(name = "retiro")
    private String retiro;
    //@Size(max = 1)
    @Column(name = "variacion_permanente_salario")
    private String variacionPermanenteSalario;
    //@Size(max = 1)
    @Column(name = "variacion_transitoria_salario")
    private String variacionTransitoriaSalario;
    //@Size(max = 1)
    @Column(name = "aporte_voluntario")
    private String aporteVoluntario;
    //@Size(max = 1)
    @Column(name = "suspension_temporal")
    private String suspensionTemporal;
    //@Size(max = 1)
    @Column(name = "incapacidad_general")
    private String incapacidadGeneral;
    //@Size(max = 1)
    @Column(name = "licencia_maternidad")
    private String licenciaMaternidad;
    //@Size(max = 1)
    @Column(name = "vacaciones")
    private String vacaciones;
    //@Size(max = 2)
    @Column(name = "incapacidas_por_trabajo")
    private String incapacidasPorTrabajo;
    //@Size(max = 1)
    @Column(name = "variacion_centros_trabajo")
    private String variacionCentrosTrabajo;
    //@Size(max = 1)
    @Column(name = "traslado_aEPS")
    private String trasladoaEPS;
    //@Size(max = 1)
    @Column(name = "traslado_aAFP")
    private String trasladoaAFP;
    //@Size(max = 1)
    @Column(name = "traslado_desdeEPS")
    private String trasladodesdeEPS;
    //@Size(max = 1)
    @Column(name = "traslado_desdeAFP")
    private String trasladodesdeAFP;
    @Column(name = "sector_aportante")
    private Short sectorAportante;
    //@Size(max = 1)
    @Column(name = "clase_aportante")
    private String claseAportante;
    @Column(name = "codigo_depto")
    private Long codigoDepto;
    @Column(name = "fecha_matricula_mercantil")
    @Temporal(TemporalType.DATE)
    private Date fechaMatriculaMercantil;
    //@Size(max = 1)
    @Column(name = "tipo_persona")
    private String tipoPersona;
    @Column(name = "periodo_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Date periodoPago;
   
    @Column(name = "dias_cot_rprof")
    private Integer diasCotRprof;
   
    @Column(name = "salario_basico")
    private Integer salarioBasico;
    //@Size(max = 1)
    @Column(name = "salario_integral")
    private String salarioIntegral;

    @Column(name = "ibc_rprof")
    private Integer ibcRprof;
    
    @Column(name = "aporte_cot_obligatoria_pensio")
    private Integer aporteCotObligatoriaPension;
    @Column(name = "aporte_fsolid_pensional_solid")
    private Integer aporteFsolidPensionalSolidaridad;
    @Column(name = "aporte_fsolid_pensional_subsi")
    private Integer aporteFsolidPensionalSubsistencia;
    @Column(name = "total_FSP")
    private BigInteger totalFSP;
    
    @Column(name = "cot_obligatoria_salud")
    private Integer cotObligatoriaSalud;
    @Column(name = "valor_autoriza_lic_maternidad")
    private BigInteger valorAutorizaLicMaternidad;
    @Column(name = "tarifa_centro_trabajo", precision = 9, scale = 7)
    private BigDecimal tarifaCentroTrabajo;
    @Column(name = "cot_obligatoria_arp")
    private Integer cotObligatoriaArp;
   
    @Column(name = "valor_aportes_ccf_ibc_tarifa")
    private Integer valorAportesCcfIbcTarifa;

    @Column(name = "valor_aportes_parafiscales_se")
    private Integer valorAportesParafiscalesSena;

    @Column(name = "valor_aportes_parafiscales_ic")
    private Integer valorAportesParafiscalesIcbf;
    @Column(name = "tarifa_aportes_esap", precision = 7, scale = 5)
    private BigDecimal tarifaAportesEsap;
    @Column(name = "valor_aportes_esap")
    private Integer valorAportesEsap;
    @Column(name = "tarifa_aportes_mined", precision = 7, scale = 5)
    private BigDecimal tarifaAportesMined;
    @Column(name = "valor_aportes_mined")
    private Integer valorAportesMined;
    //@Size(max = 2147483647)

    @Column(name = "aporte_vol_afiliado")
    private BigInteger aporteVolAfiliado;
    @Column(name = "cot_vol_aportante")
    private BigInteger cotVolAportante;
    @Column(name = "total_cotizacion")
    private Long totalCotizacion;
    @Column(name = "valor_autoriza_incapa_EG")
    private BigInteger valorautorizaincapaEG;
    //@Size(max = 4)
    @Column(name = "ANO_RESTO")
    private String anoResto;
    //@Size(max = 2)
    @Column(name = "MES_RESTO")
    private String mesResto;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 1)
    @Column(name = "Apo_exo_pago_salud_ley_1607_2")
    private String apoexopagosaludley16072000;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 1)
    @Column(name = "Apo_aco_beneficios_art_5_ley_")
    private String apoacobeneficiosart5ley14292000CCF;
    @Basic(optional = false)
    //@NotNull
    //@Size(min = 1, max = 1)
    @Column(name = "Cot_exo_pago_paraf_salud_ley_")
    private String cotexopagoparafsaludley16072012;
    //@Size(max = 20)
    @Column(name = "ESTADO_AFILIADO")
    private String estadoAfiliado;
    @Column(name = "MIN_FECHA_AFILIACION_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date minFechaAfiliacionPension;
    //@Size(max = 2147483647)
    @Column(name = "MIN_NOMBRE_ADMIN_PENSION")
    private String minNombreAdminPension;
    @Column(name = "MAX_FECHA_AFILIACION_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date maxFechaAfiliacionPension;
    //@Size(max = 2147483647)
    @Column(name = "MAX_NOMBRE_ADMIN_PENSION")
    private String maxNombreAdminPension;
    @Column(name = "MAX_FECHA_INICIO_PENSION")
    @Temporal(TemporalType.TIMESTAMP)
    private Date maxFechaInicioPension;
    //@Size(max = 100)
    @Column(name = "TIPO_PENSION")
    private String tipoPension;
    //@Size(max = 2147483647)
    @Column(name = "NOMBRE_ADMIN_PENSIONADO")
    private String nombreAdminPensionado;
    @Column(name = "CESANTE")
    private Short cesante;
    //@Size(max = 6)
    @Column(name = "RUA_PERIODO")
    private String ruaPeriodo;
    //@Size(max = 109)
    @Column(name = "ULTIMA_AFILIACION_RUA")
    private String ultimaAfiliacionRua;
    
    
    
    //REVISION ANDRES NUEVA ESTRUCTURA NOMINA VERSION 12 DE JUNIO
    
    //***************NUEVOS*********************

   
    //cotización PAGADA PILA FSP Subcuenta de solidaridad
    @Column(name = "COTIZA_PAGA_SOLIDARIDAD", precision = 7, scale = 5)
    private BigDecimal cotizacionPagadaSolidaridad;

    //cotización PAGADA PILA FSP Subcuenta de Subsistencia
    @Column(name = "COTIZA_PAGA_SUBSISTENCIA", precision = 7, scale = 5)
    private BigDecimal cotizacionPagadaSubsistencia;
   
    //TARIFA PILA PENSIÓN ADICIONAL ACT. ALTO RIESGO
    @Column(name = "TARI_PENS_ACTI_ALT_RIE", precision = 7, scale = 5)
    private BigDecimal tarifaPensionActividadAltoRiesgo;
    
    //cotización PAGADA PILA PENSIÓN ADICIONAL ACT. ALTO RIESGO
    @Column(name = "COTI_PAGA_ACTI_ALT_RIE", precision = 7, scale = 5)
    private BigDecimal cotizacionPagadaPensionAltoRiesgo;
    
    
    //**********************************************

    
    
    //*******************ANTIGUOS****************
    
    //código ADMINISTRADORA SALUD
    @Column(name = "codigo_EPS")
    private String codigoEPS;
    
    //días COTIZADOS PILA SALUD
    @Column(name = "dias_cot_salud")
    private Integer diasCotSalud;
    
    //IBC pila SALUD
    @Column(name = "ibc_salud")
    private Integer ibcSalud;
    
    //TARIFA PILA SALUD
    @Column(name = "tarifa_salud", precision = 7, scale = 5)
    private BigDecimal tarifaSalud;
    
    //código ADMINISTRADORA PENSIÓN
    @Column(name = "codigo_AFP")
    private String codigoAFP;
    
    //días COTIZADOS PILA PENSIÓN
    @Column(name = "dias_cot_pension")
    private Integer diasCotPension;
    
    //IBC pila PENSIÓN
    @Column(name = "ibc_pension")
    private Integer ibcPension;
    
    //TARIFA PILA PENSIÓN
    @Column(name = "tarifa_pension", precision = 7, scale = 5)
    private BigDecimal tarifaPension;
    
    //código ADMINISTRADORA CCF
    @Column(name = "codigo_CCF")
    private String codigoCCF;
    
    //días COTIZADOS PILA CCF
    @Column(name = "dias_cot_ccf")
    private Integer diasCotCcf;
    
    //IBC pila CCF
    @Column(name = "ibc_ccf")
    private Integer ibcCcf;
    
    //TARIFA PILA CCF
    @Column(name = "tarifa_aportes_ccf", precision = 7, scale = 5)
    private BigDecimal tarifaAportesCcf;
    
    //TARIFA PILA SENA
    @Column(name = "tarifa_aportes_sena", precision = 7, scale = 5)
    private BigDecimal tarifaAportesSena;
    
    //TARIFA PILA ICBF
    @Column(name = "tarifa_aportes_icbf", precision = 7, scale = 5)
    private BigDecimal tarifaAportesIcbf;
    
    //PLANILLAS PILA CARGADA
    @Column(name = "planilla")
    private String planilla;
    
    //**********************************************
    
    
    
        @JoinColumn(name = "IDPILA", referencedColumnName = "ID")
        @ManyToOne(optional = false)
        private Pila idpila;

        public PilaDetalle() {
        }

        public PilaDetalle(BigDecimal id) {
           this.id = id;
        }

        @Override
        public int hashCode() {
           int hash = 0;
           hash += (getId() != null ? getId().hashCode() : 0);
           return hash;
        }

        @Override
        public boolean equals(Object object) {
           // TODO: Warning - this method won't work in the case the id fields are not set
           if (!(object instanceof PilaDetalle)) {
               return false;
           }
           PilaDetalle other = (PilaDetalle) object;
           if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
               return false;
           }
           return true;
        }

        @Override
        public String toString() {
           return "org.ugpp.pf.persistence.entities.li.PilaDetalle[ id=" + getId() + " ]";
        }
        
         /**
     * @return the serialVersionUID
     */
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    /**
     * @param aSerialVersionUID the serialVersionUID to set
     */
    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }
    
    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the razonSocial
     */
    public String getRazonSocial() {
        return razonSocial;
    }

    /**
     * @param razonSocial the razonSocial to set
     */
    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    /**
     * @return the periodoSalud
     */
    public Date getPeriodoSalud() {
        return periodoSalud;
    }

    /**
     * @param periodoSalud the periodoSalud to set
     */
    public void setPeriodoSalud(Date periodoSalud) {
        this.periodoSalud = periodoSalud;
    }

    /**
     * @return the periodoResto
     */
    public String getPeriodoResto() {
        return periodoResto;
    }

    /**
     * @param periodoResto the periodoResto to set
     */
    public void setPeriodoResto(String periodoResto) {
        this.periodoResto = periodoResto;
    }

    /**
     * @return the fechaPago
     */
    public Date getFechaPago() {
        return fechaPago;
    }

    /**
     * @param fechaPago the fechaPago to set
     */
    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    /**
     * @return the codigoOperador
     */
    public String getCodigoOperador() {
        return codigoOperador;
    }

    /**
     * @param codigoOperador the codigoOperador to set
     */
    public void setCodigoOperador(String codigoOperador) {
        this.codigoOperador = codigoOperador;
    }

    /**
     * @return the tipoAportante
     */
    public Short getTipoAportante() {
        return tipoAportante;
    }

    /**
     * @param tipoAportante the tipoAportante to set
     */
    public void setTipoAportante(Short tipoAportante) {
        this.tipoAportante = tipoAportante;
    }

    /**
     * @return the codigoArp
     */
    public String getCodigoArp() {
        return codigoArp;
    }

    /**
     * @param codigoArp the codigoArp to set
     */
    public void setCodigoArp(String codigoArp) {
        this.codigoArp = codigoArp;
    }

    /**
     * @return the tipoPlanilla
     */
    public String getTipoPlanilla() {
        return tipoPlanilla;
    }

    /**
     * @param tipoPlanilla the tipoPlanilla to set
     */
    public void setTipoPlanilla(String tipoPlanilla) {
        this.tipoPlanilla = tipoPlanilla;
    }

    /**
     * @return the planillaAsociada
     */
    public String getPlanillaAsociada() {
        return planillaAsociada;
    }

    /**
     * @param planillaAsociada the planillaAsociada to set
     */
    public void setPlanillaAsociada(String planillaAsociada) {
        this.planillaAsociada = planillaAsociada;
    }

    /**
     * @return the fechaAsociada
     */
    public Date getFechaAsociada() {
        return fechaAsociada;
    }

    /**
     * @param fechaAsociada the fechaAsociada to set
     */
    public void setFechaAsociada(Date fechaAsociada) {
        this.fechaAsociada = fechaAsociada;
    }

    /**
     * @return the numeroIdentificacion
     */
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    /**
     * @param numeroIdentificacion the numeroIdentificacion to set
     */
    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    /**
     * @return the tipoIdentificacion
     */
    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    /**
     * @param tipoIdentificacion the tipoIdentificacion to set
     */
    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    /**
     * @return the tipoCotizante
     */
    public Short getTipoCotizante() {
        return tipoCotizante;
    }

    /**
     * @param tipoCotizante the tipoCotizante to set
     */
    public void setTipoCotizante(Short tipoCotizante) {
        this.tipoCotizante = tipoCotizante;
    }

    /**
     * @return the subtipoCotizante
     */
    public Short getSubtipoCotizante() {
        return subtipoCotizante;
    }

    /**
     * @param subtipoCotizante the subtipoCotizante to set
     */
    public void setSubtipoCotizante(Short subtipoCotizante) {
        this.subtipoCotizante = subtipoCotizante;
    }

    /**
     * @return the extranjeroNopension
     */
    public String getExtranjeroNopension() {
        return extranjeroNopension;
    }

    /**
     * @param extranjeroNopension the extranjeroNopension to set
     */
    public void setExtranjeroNopension(String extranjeroNopension) {
        this.extranjeroNopension = extranjeroNopension;
    }

    /**
     * @return the colombianoExterior
     */
    public String getColombianoExterior() {
        return colombianoExterior;
    }

    /**
     * @param colombianoExterior the colombianoExterior to set
     */
    public void setColombianoExterior(String colombianoExterior) {
        this.colombianoExterior = colombianoExterior;
    }

    /**
     * @return the primerNombre
     */
    public String getPrimerNombre() {
        return primerNombre;
    }

    /**
     * @param primerNombre the primerNombre to set
     */
    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    /**
     * @return the segundoNombre
     */
    public String getSegundoNombre() {
        return segundoNombre;
    }

    /**
     * @param segundoNombre the segundoNombre to set
     */
    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    /**
     * @return the primerApellido
     */
    public String getPrimerApellido() {
        return primerApellido;
    }

    /**
     * @param primerApellido the primerApellido to set
     */
    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    /**
     * @return the segundoApellido
     */
    public String getSegundoApellido() {
        return segundoApellido;
    }

    /**
     * @param segundoApellido the segundoApellido to set
     */
    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    /**
     * @return the cambioTarifaEspecial
     */
    public String getCambioTarifaEspecial() {
        return cambioTarifaEspecial;
    }

    /**
     * @param cambioTarifaEspecial the cambioTarifaEspecial to set
     */
    public void setCambioTarifaEspecial(String cambioTarifaEspecial) {
        this.cambioTarifaEspecial = cambioTarifaEspecial;
    }

    /**
     * @return the ingreso
     */
    public String getIngreso() {
        return ingreso;
    }

    /**
     * @param ingreso the ingreso to set
     */
    public void setIngreso(String ingreso) {
        this.ingreso = ingreso;
    }

    /**
     * @return the retiro
     */
    public String getRetiro() {
        return retiro;
    }

    /**
     * @param retiro the retiro to set
     */
    public void setRetiro(String retiro) {
        this.retiro = retiro;
    }

    /**
     * @return the variacionPermanenteSalario
     */
    public String getVariacionPermanenteSalario() {
        return variacionPermanenteSalario;
    }

    /**
     * @param variacionPermanenteSalario the variacionPermanenteSalario to set
     */
    public void setVariacionPermanenteSalario(String variacionPermanenteSalario) {
        this.variacionPermanenteSalario = variacionPermanenteSalario;
    }

    /**
     * @return the variacionTransitoriaSalario
     */
    public String getVariacionTransitoriaSalario() {
        return variacionTransitoriaSalario;
    }

    /**
     * @param variacionTransitoriaSalario the variacionTransitoriaSalario to set
     */
    public void setVariacionTransitoriaSalario(String variacionTransitoriaSalario) {
        this.variacionTransitoriaSalario = variacionTransitoriaSalario;
    }

    /**
     * @return the aporteVoluntario
     */
    public String getAporteVoluntario() {
        return aporteVoluntario;
    }

    /**
     * @param aporteVoluntario the aporteVoluntario to set
     */
    public void setAporteVoluntario(String aporteVoluntario) {
        this.aporteVoluntario = aporteVoluntario;
    }

    /**
     * @return the suspensionTemporal
     */
    public String getSuspensionTemporal() {
        return suspensionTemporal;
    }

    /**
     * @param suspensionTemporal the suspensionTemporal to set
     */
    public void setSuspensionTemporal(String suspensionTemporal) {
        this.suspensionTemporal = suspensionTemporal;
    }

    /**
     * @return the incapacidadGeneral
     */
    public String getIncapacidadGeneral() {
        return incapacidadGeneral;
    }

    /**
     * @param incapacidadGeneral the incapacidadGeneral to set
     */
    public void setIncapacidadGeneral(String incapacidadGeneral) {
        this.incapacidadGeneral = incapacidadGeneral;
    }

    /**
     * @return the licenciaMaternidad
     */
    public String getLicenciaMaternidad() {
        return licenciaMaternidad;
    }

    /**
     * @param licenciaMaternidad the licenciaMaternidad to set
     */
    public void setLicenciaMaternidad(String licenciaMaternidad) {
        this.licenciaMaternidad = licenciaMaternidad;
    }

    /**
     * @return the vacaciones
     */
    public String getVacaciones() {
        return vacaciones;
    }

    /**
     * @param vacaciones the vacaciones to set
     */
    public void setVacaciones(String vacaciones) {
        this.vacaciones = vacaciones;
    }

    /**
     * @return the incapacidasPorTrabajo
     */
    public String getIncapacidasPorTrabajo() {
        return incapacidasPorTrabajo;
    }

    /**
     * @param incapacidasPorTrabajo the incapacidasPorTrabajo to set
     */
    public void setIncapacidasPorTrabajo(String incapacidasPorTrabajo) {
        this.incapacidasPorTrabajo = incapacidasPorTrabajo;
    }

    /**
     * @return the variacionCentrosTrabajo
     */
    public String getVariacionCentrosTrabajo() {
        return variacionCentrosTrabajo;
    }

    /**
     * @param variacionCentrosTrabajo the variacionCentrosTrabajo to set
     */
    public void setVariacionCentrosTrabajo(String variacionCentrosTrabajo) {
        this.variacionCentrosTrabajo = variacionCentrosTrabajo;
    }

    /**
     * @return the trasladoaEPS
     */
    public String getTrasladoaEPS() {
        return trasladoaEPS;
    }

    /**
     * @param trasladoaEPS the trasladoaEPS to set
     */
    public void setTrasladoaEPS(String trasladoaEPS) {
        this.trasladoaEPS = trasladoaEPS;
    }

    /**
     * @return the trasladoaAFP
     */
    public String getTrasladoaAFP() {
        return trasladoaAFP;
    }

    /**
     * @param trasladoaAFP the trasladoaAFP to set
     */
    public void setTrasladoaAFP(String trasladoaAFP) {
        this.trasladoaAFP = trasladoaAFP;
    }

    /**
     * @return the trasladodesdeEPS
     */
    public String getTrasladodesdeEPS() {
        return trasladodesdeEPS;
    }

    /**
     * @param trasladodesdeEPS the trasladodesdeEPS to set
     */
    public void setTrasladodesdeEPS(String trasladodesdeEPS) {
        this.trasladodesdeEPS = trasladodesdeEPS;
    }

    /**
     * @return the trasladodesdeAFP
     */
    public String getTrasladodesdeAFP() {
        return trasladodesdeAFP;
    }

    /**
     * @param trasladodesdeAFP the trasladodesdeAFP to set
     */
    public void setTrasladodesdeAFP(String trasladodesdeAFP) {
        this.trasladodesdeAFP = trasladodesdeAFP;
    }

    /**
     * @return the sectorAportante
     */
    public Short getSectorAportante() {
        return sectorAportante;
    }

    /**
     * @param sectorAportante the sectorAportante to set
     */
    public void setSectorAportante(Short sectorAportante) {
        this.sectorAportante = sectorAportante;
    }

    /**
     * @return the claseAportante
     */
    public String getClaseAportante() {
        return claseAportante;
    }

    /**
     * @param claseAportante the claseAportante to set
     */
    public void setClaseAportante(String claseAportante) {
        this.claseAportante = claseAportante;
    }

    /**
     * @return the codigoDepto
     */
    public Long getCodigoDepto() {
        return codigoDepto;
    }

    /**
     * @param codigoDepto the codigoDepto to set
     */
    public void setCodigoDepto(Long codigoDepto) {
        this.codigoDepto = codigoDepto;
    }

    /**
     * @return the fechaMatriculaMercantil
     */
    public Date getFechaMatriculaMercantil() {
        return fechaMatriculaMercantil;
    }

    /**
     * @param fechaMatriculaMercantil the fechaMatriculaMercantil to set
     */
    public void setFechaMatriculaMercantil(Date fechaMatriculaMercantil) {
        this.fechaMatriculaMercantil = fechaMatriculaMercantil;
    }

    /**
     * @return the tipoPersona
     */
    public String getTipoPersona() {
        return tipoPersona;
    }

    /**
     * @param tipoPersona the tipoPersona to set
     */
    public void setTipoPersona(String tipoPersona) {
        this.tipoPersona = tipoPersona;
    }

    /**
     * @return the periodoPago
     */
    public Date getPeriodoPago() {
        return periodoPago;
    }

    /**
     * @param periodoPago the periodoPago to set
     */
    public void setPeriodoPago(Date periodoPago) {
        this.periodoPago = periodoPago;
    }

    /**
     * @return the codigoAFP
     */
    public String getCodigoAFP() {
        return codigoAFP;
    }

    /**
     * @param codigoAFP the codigoAFP to set
     */
    public void setCodigoAFP(String codigoAFP) {
        this.codigoAFP = codigoAFP;
    }

    /**
     * @return the codigoEPS
     */
    public String getCodigoEPS() {
        return codigoEPS;
    }

    /**
     * @param codigoEPS the codigoEPS to set
     */
    public void setCodigoEPS(String codigoEPS) {
        this.codigoEPS = codigoEPS;
    }

    /**
     * @return the codigoCCF
     */
    public String getCodigoCCF() {
        return codigoCCF;
    }

    /**
     * @param codigoCCF the codigoCCF to set
     */
    public void setCodigoCCF(String codigoCCF) {
        this.codigoCCF = codigoCCF;
    }

    /**
     * @return the diasCotPension
     */
    public Integer getDiasCotPension() {
        return diasCotPension;
    }

    /**
     * @param diasCotPension the diasCotPension to set
     */
    public void setDiasCotPension(Integer diasCotPension) {
        this.diasCotPension = diasCotPension;
    }

    /**
     * @return the diasCotSalud
     */
    public Integer getDiasCotSalud() {
        return diasCotSalud;
    }

    /**
     * @param diasCotSalud the diasCotSalud to set
     */
    public void setDiasCotSalud(Integer diasCotSalud) {
        this.diasCotSalud = diasCotSalud;
    }

    /**
     * @return the diasCotRprof
     */
    public Integer getDiasCotRprof() {
        return diasCotRprof;
    }

    /**
     * @param diasCotRprof the diasCotRprof to set
     */
    public void setDiasCotRprof(Integer diasCotRprof) {
        this.diasCotRprof = diasCotRprof;
    }

    /**
     * @return the diasCotCcf
     */
    public Integer getDiasCotCcf() {
        return diasCotCcf;
    }

    /**
     * @param diasCotCcf the diasCotCcf to set
     */
    public void setDiasCotCcf(Integer diasCotCcf) {
        this.diasCotCcf = diasCotCcf;
    }

    /**
     * @return the salarioBasico
     */
    public Integer getSalarioBasico() {
        return salarioBasico;
    }

    /**
     * @param salarioBasico the salarioBasico to set
     */
    public void setSalarioBasico(Integer salarioBasico) {
        this.salarioBasico = salarioBasico;
    }

    /**
     * @return the salarioIntegral
     */
    public String getSalarioIntegral() {
        return salarioIntegral;
    }

    /**
     * @param salarioIntegral the salarioIntegral to set
     */
    public void setSalarioIntegral(String salarioIntegral) {
        this.salarioIntegral = salarioIntegral;
    }

    /**
     * @return the ibcPension
     */
    public Integer getIbcPension() {
        return ibcPension;
    }

    /**
     * @param ibcPension the ibcPension to set
     */
    public void setIbcPension(Integer ibcPension) {
        this.ibcPension = ibcPension;
    }

    /**
     * @return the ibcSalud
     */
    public Integer getIbcSalud() {
        return ibcSalud;
    }

    /**
     * @param ibcSalud the ibcSalud to set
     */
    public void setIbcSalud(Integer ibcSalud) {
        this.ibcSalud = ibcSalud;
    }

    /**
     * @return the ibcRprof
     */
    public Integer getIbcRprof() {
        return ibcRprof;
    }

    /**
     * @param ibcRprof the ibcRprof to set
     */
    public void setIbcRprof(Integer ibcRprof) {
        this.ibcRprof = ibcRprof;
    }

    /**
     * @return the ibcCcf
     */
    public Integer getIbcCcf() {
        return ibcCcf;
    }

    /**
     * @param ibcCcf the ibcCcf to set
     */
    public void setIbcCcf(Integer ibcCcf) {
        this.ibcCcf = ibcCcf;
    }

    /**
     * @return the tarifaPension
     */
    public BigDecimal getTarifaPension() {
        return tarifaPension;
    }

    /**
     * @param tarifaPension the tarifaPension to set
     */
    public void setTarifaPension(BigDecimal tarifaPension) {
        this.tarifaPension = tarifaPension;
    }

    /**
     * @return the aporteCotObligatoriaPension
     */
    public Integer getAporteCotObligatoriaPension() {
        return aporteCotObligatoriaPension;
    }

    /**
     * @param aporteCotObligatoriaPension the aporteCotObligatoriaPension to set
     */
    public void setAporteCotObligatoriaPension(Integer aporteCotObligatoriaPension) {
        this.aporteCotObligatoriaPension = aporteCotObligatoriaPension;
    }

    /**
     * @return the aporteFsolidPensionalSolidaridad
     */
    public Integer getAporteFsolidPensionalSolidaridad() {
        return aporteFsolidPensionalSolidaridad;
    }

    /**
     * @param aporteFsolidPensionalSolidaridad the aporteFsolidPensionalSolidaridad to set
     */
    public void setAporteFsolidPensionalSolidaridad(Integer aporteFsolidPensionalSolidaridad) {
        this.aporteFsolidPensionalSolidaridad = aporteFsolidPensionalSolidaridad;
    }

    /**
     * @return the aporteFsolidPensionalSubsistencia
     */
    public Integer getAporteFsolidPensionalSubsistencia() {
        return aporteFsolidPensionalSubsistencia;
    }

    /**
     * @param aporteFsolidPensionalSubsistencia the aporteFsolidPensionalSubsistencia to set
     */
    public void setAporteFsolidPensionalSubsistencia(Integer aporteFsolidPensionalSubsistencia) {
        this.aporteFsolidPensionalSubsistencia = aporteFsolidPensionalSubsistencia;
    }

    /**
     * @return the totalFSP
     */
    public BigInteger getTotalFSP() {
        return totalFSP;
    }

    /**
     * @param totalFSP the totalFSP to set
     */
    public void setTotalFSP(BigInteger totalFSP) {
        this.totalFSP = totalFSP;
    }

    /**
     * @return the tarifaSalud
     */
    public BigDecimal getTarifaSalud() {
        return tarifaSalud;
    }

    /**
     * @param tarifaSalud the tarifaSalud to set
     */
    public void setTarifaSalud(BigDecimal tarifaSalud) {
        this.tarifaSalud = tarifaSalud;
    }

    /**
     * @return the cotObligatoriaSalud
     */
    public Integer getCotObligatoriaSalud() {
        return cotObligatoriaSalud;
    }

    /**
     * @param cotObligatoriaSalud the cotObligatoriaSalud to set
     */
    public void setCotObligatoriaSalud(Integer cotObligatoriaSalud) {
        this.cotObligatoriaSalud = cotObligatoriaSalud;
    }

    /**
     * @return the valorAutorizaLicMaternidad
     */
    public BigInteger getValorAutorizaLicMaternidad() {
        return valorAutorizaLicMaternidad;
    }

    /**
     * @param valorAutorizaLicMaternidad the valorAutorizaLicMaternidad to set
     */
    public void setValorAutorizaLicMaternidad(BigInteger valorAutorizaLicMaternidad) {
        this.valorAutorizaLicMaternidad = valorAutorizaLicMaternidad;
    }

    /**
     * @return the tarifaCentroTrabajo
     */
    public BigDecimal getTarifaCentroTrabajo() {
        return tarifaCentroTrabajo;
    }

    /**
     * @param tarifaCentroTrabajo the tarifaCentroTrabajo to set
     */
    public void setTarifaCentroTrabajo(BigDecimal tarifaCentroTrabajo) {
        this.tarifaCentroTrabajo = tarifaCentroTrabajo;
    }

    /**
     * @return the cotObligatoriaArp
     */
    public Integer getCotObligatoriaArp() {
        return cotObligatoriaArp;
    }

    /**
     * @param cotObligatoriaArp the cotObligatoriaArp to set
     */
    public void setCotObligatoriaArp(Integer cotObligatoriaArp) {
        this.cotObligatoriaArp = cotObligatoriaArp;
    }

    /**
     * @return the tarifaAportesCcf
     */
    public BigDecimal getTarifaAportesCcf() {
        return tarifaAportesCcf;
    }

    /**
     * @param tarifaAportesCcf the tarifaAportesCcf to set
     */
    public void setTarifaAportesCcf(BigDecimal tarifaAportesCcf) {
        this.tarifaAportesCcf = tarifaAportesCcf;
    }

    /**
     * @return the valorAportesCcfIbcTarifa
     */
    public Integer getValorAportesCcfIbcTarifa() {
        return valorAportesCcfIbcTarifa;
    }

    /**
     * @param valorAportesCcfIbcTarifa the valorAportesCcfIbcTarifa to set
     */
    public void setValorAportesCcfIbcTarifa(Integer valorAportesCcfIbcTarifa) {
        this.valorAportesCcfIbcTarifa = valorAportesCcfIbcTarifa;
    }

    /**
     * @return the tarifaAportesSena
     */
    public BigDecimal getTarifaAportesSena() {
        return tarifaAportesSena;
    }

    /**
     * @param tarifaAportesSena the tarifaAportesSena to set
     */
    public void setTarifaAportesSena(BigDecimal tarifaAportesSena) {
        this.tarifaAportesSena = tarifaAportesSena;
    }

    /**
     * @return the valorAportesParafiscalesSena
     */
    public Integer getValorAportesParafiscalesSena() {
        return valorAportesParafiscalesSena;
    }

    /**
     * @param valorAportesParafiscalesSena the valorAportesParafiscalesSena to set
     */
    public void setValorAportesParafiscalesSena(Integer valorAportesParafiscalesSena) {
        this.valorAportesParafiscalesSena = valorAportesParafiscalesSena;
    }

    /**
     * @return the tarifaAportesIcbf
     */
    public BigDecimal getTarifaAportesIcbf() {
        return tarifaAportesIcbf;
    }

    /**
     * @param tarifaAportesIcbf the tarifaAportesIcbf to set
     */
    public void setTarifaAportesIcbf(BigDecimal tarifaAportesIcbf) {
        this.tarifaAportesIcbf = tarifaAportesIcbf;
    }

    /**
     * @return the valorAportesParafiscalesIcbf
     */
    public Integer getValorAportesParafiscalesIcbf() {
        return valorAportesParafiscalesIcbf;
    }

    /**
     * @param valorAportesParafiscalesIcbf the valorAportesParafiscalesIcbf to set
     */
    public void setValorAportesParafiscalesIcbf(Integer valorAportesParafiscalesIcbf) {
        this.valorAportesParafiscalesIcbf = valorAportesParafiscalesIcbf;
    }

    /**
     * @return the tarifaAportesEsap
     */
    public BigDecimal getTarifaAportesEsap() {
        return tarifaAportesEsap;
    }

    /**
     * @param tarifaAportesEsap the tarifaAportesEsap to set
     */
    public void setTarifaAportesEsap(BigDecimal tarifaAportesEsap) {
        this.tarifaAportesEsap = tarifaAportesEsap;
    }

    /**
     * @return the valorAportesEsap
     */
    public Integer getValorAportesEsap() {
        return valorAportesEsap;
    }

    /**
     * @param valorAportesEsap the valorAportesEsap to set
     */
    public void setValorAportesEsap(Integer valorAportesEsap) {
        this.valorAportesEsap = valorAportesEsap;
    }

    /**
     * @return the tarifaAportesMined
     */
    public BigDecimal getTarifaAportesMined() {
        return tarifaAportesMined;
    }

    /**
     * @param tarifaAportesMined the tarifaAportesMined to set
     */
    public void setTarifaAportesMined(BigDecimal tarifaAportesMined) {
        this.tarifaAportesMined = tarifaAportesMined;
    }

    /**
     * @return the valorAportesMined
     */
    public Integer getValorAportesMined() {
        return valorAportesMined;
    }

    /**
     * @param valorAportesMined the valorAportesMined to set
     */
    public void setValorAportesMined(Integer valorAportesMined) {
        this.valorAportesMined = valorAportesMined;
    }

    /**
     * @return the planilla
     */
    public String getPlanilla() {
        return planilla;
    }

    /**
     * @param planilla the planilla to set
     */
    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    /**
     * @return the aporteVolAfiliado
     */
    public BigInteger getAporteVolAfiliado() {
        return aporteVolAfiliado;
    }

    /**
     * @param aporteVolAfiliado the aporteVolAfiliado to set
     */
    public void setAporteVolAfiliado(BigInteger aporteVolAfiliado) {
        this.aporteVolAfiliado = aporteVolAfiliado;
    }

    /**
     * @return the cotVolAportante
     */
    public BigInteger getCotVolAportante() {
        return cotVolAportante;
    }

    /**
     * @param cotVolAportante the cotVolAportante to set
     */
    public void setCotVolAportante(BigInteger cotVolAportante) {
        this.cotVolAportante = cotVolAportante;
    }

    /**
     * @return the totalCotizacion
     */
    public Long getTotalCotizacion() {
        return totalCotizacion;
    }

    /**
     * @param totalCotizacion the totalCotizacion to set
     */
    public void setTotalCotizacion(Long totalCotizacion) {
        this.totalCotizacion = totalCotizacion;
    }

    /**
     * @return the valorautorizaincapaEG
     */
    public BigInteger getValorautorizaincapaEG() {
        return valorautorizaincapaEG;
    }

    /**
     * @param valorautorizaincapaEG the valorautorizaincapaEG to set
     */
    public void setValorautorizaincapaEG(BigInteger valorautorizaincapaEG) {
        this.valorautorizaincapaEG = valorautorizaincapaEG;
    }

    /**
     * @return the anoResto
     */
    public String getAnoResto() {
        return anoResto;
    }

    /**
     * @param anoResto the anoResto to set
     */
    public void setAnoResto(String anoResto) {
        this.anoResto = anoResto;
    }

    /**
     * @return the mesResto
     */
    public String getMesResto() {
        return mesResto;
    }

    /**
     * @param mesResto the mesResto to set
     */
    public void setMesResto(String mesResto) {
        this.mesResto = mesResto;
    }

    /**
     * @return the apoexopagosaludley16072000
     */
    public String getApoexopagosaludley16072000() {
        return apoexopagosaludley16072000;
    }

    /**
     * @param apoexopagosaludley16072000 the apoexopagosaludley16072000 to set
     */
    public void setApoexopagosaludley16072000(String apoexopagosaludley16072000) {
        this.apoexopagosaludley16072000 = apoexopagosaludley16072000;
    }

    /**
     * @return the apoacobeneficiosart5ley14292000CCF
     */
    public String getApoacobeneficiosart5ley14292000CCF() {
        return apoacobeneficiosart5ley14292000CCF;
    }

    /**
     * @param apoacobeneficiosart5ley14292000CCF the apoacobeneficiosart5ley14292000CCF to set
     */
    public void setApoacobeneficiosart5ley14292000CCF(String apoacobeneficiosart5ley14292000CCF) {
        this.apoacobeneficiosart5ley14292000CCF = apoacobeneficiosart5ley14292000CCF;
    }

    /**
     * @return the cotexopagoparafsaludley16072012
     */
    public String getCotexopagoparafsaludley16072012() {
        return cotexopagoparafsaludley16072012;
    }

    /**
     * @param cotexopagoparafsaludley16072012 the cotexopagoparafsaludley16072012 to set
     */
    public void setCotexopagoparafsaludley16072012(String cotexopagoparafsaludley16072012) {
        this.cotexopagoparafsaludley16072012 = cotexopagoparafsaludley16072012;
    }

    /**
     * @return the estadoAfiliado
     */
    public String getEstadoAfiliado() {
        return estadoAfiliado;
    }

    /**
     * @param estadoAfiliado the estadoAfiliado to set
     */
    public void setEstadoAfiliado(String estadoAfiliado) {
        this.estadoAfiliado = estadoAfiliado;
    }

    /**
     * @return the minFechaAfiliacionPension
     */
    public Date getMinFechaAfiliacionPension() {
        return minFechaAfiliacionPension;
    }

    /**
     * @param minFechaAfiliacionPension the minFechaAfiliacionPension to set
     */
    public void setMinFechaAfiliacionPension(Date minFechaAfiliacionPension) {
        this.minFechaAfiliacionPension = minFechaAfiliacionPension;
    }

    /**
     * @return the minNombreAdminPension
     */
    public String getMinNombreAdminPension() {
        return minNombreAdminPension;
    }

    /**
     * @param minNombreAdminPension the minNombreAdminPension to set
     */
    public void setMinNombreAdminPension(String minNombreAdminPension) {
        this.minNombreAdminPension = minNombreAdminPension;
    }

    /**
     * @return the maxFechaAfiliacionPension
     */
    public Date getMaxFechaAfiliacionPension() {
        return maxFechaAfiliacionPension;
    }

    /**
     * @param maxFechaAfiliacionPension the maxFechaAfiliacionPension to set
     */
    public void setMaxFechaAfiliacionPension(Date maxFechaAfiliacionPension) {
        this.maxFechaAfiliacionPension = maxFechaAfiliacionPension;
    }

    /**
     * @return the maxNombreAdminPension
     */
    public String getMaxNombreAdminPension() {
        return maxNombreAdminPension;
    }

    /**
     * @param maxNombreAdminPension the maxNombreAdminPension to set
     */
    public void setMaxNombreAdminPension(String maxNombreAdminPension) {
        this.maxNombreAdminPension = maxNombreAdminPension;
    }

    /**
     * @return the maxFechaInicioPension
     */
    public Date getMaxFechaInicioPension() {
        return maxFechaInicioPension;
    }

    /**
     * @param maxFechaInicioPension the maxFechaInicioPension to set
     */
    public void setMaxFechaInicioPension(Date maxFechaInicioPension) {
        this.maxFechaInicioPension = maxFechaInicioPension;
    }

    /**
     * @return the tipoPension
     */
    public String getTipoPension() {
        return tipoPension;
    }

    /**
     * @param tipoPension the tipoPension to set
     */
    public void setTipoPension(String tipoPension) {
        this.tipoPension = tipoPension;
    }

    /**
     * @return the nombreAdminPensionado
     */
    public String getNombreAdminPensionado() {
        return nombreAdminPensionado;
    }

    /**
     * @param nombreAdminPensionado the nombreAdminPensionado to set
     */
    public void setNombreAdminPensionado(String nombreAdminPensionado) {
        this.nombreAdminPensionado = nombreAdminPensionado;
    }

    /**
     * @return the cesante
     */
    public Short getCesante() {
        return cesante;
    }

    /**
     * @param cesante the cesante to set
     */
    public void setCesante(Short cesante) {
        this.cesante = cesante;
    }

    /**
     * @return the ruaPeriodo
     */
    public String getRuaPeriodo() {
        return ruaPeriodo;
    }

    /**
     * @param ruaPeriodo the ruaPeriodo to set
     */
    public void setRuaPeriodo(String ruaPeriodo) {
        this.ruaPeriodo = ruaPeriodo;
    }

    /**
     * @return the ultimaAfiliacionRua
     */
    public String getUltimaAfiliacionRua() {
        return ultimaAfiliacionRua;
    }

    /**
     * @param ultimaAfiliacionRua the ultimaAfiliacionRua to set
     */
    public void setUltimaAfiliacionRua(String ultimaAfiliacionRua) {
        this.ultimaAfiliacionRua = ultimaAfiliacionRua;
    }

    /**
     * @return the id
     */
    public BigDecimal getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(BigDecimal id) {
        this.id = id;
    }

    /**
     * @return the idpila
     */
    public Pila getIdpila() {
        return idpila;
    }

    /**
     * @param idpila the idpila to set
     */
    public void setIdpila(Pila idpila) {
        this.idpila = idpila;
    }


    /**
     * @return the cotizacionPagadaSolidaridad
     */
    public BigDecimal getCotizacionPagadaSolidaridad() {
        return cotizacionPagadaSolidaridad;
    }

    /**
     * @param cotizacionPagadaSolidaridad the cotizacionPagadaSolidaridad to set
     */
    public void setCotizacionPagadaSolidaridad(BigDecimal cotizacionPagadaSolidaridad) {
        this.cotizacionPagadaSolidaridad = cotizacionPagadaSolidaridad;
    }

    /**
     * @return the cotizacionPagadaSubsistencia
     */
    public BigDecimal getCotizacionPagadaSubsistencia() {
        return cotizacionPagadaSubsistencia;
    }

    /**
     * @param cotizacionPagadaSubsistencia the cotizacionPagadaSubsistencia to set
     */
    public void setCotizacionPagadaSubsistencia(BigDecimal cotizacionPagadaSubsistencia) {
        this.cotizacionPagadaSubsistencia = cotizacionPagadaSubsistencia;
    }

    /**
     * @return the tarifaPensionActividadAltoRiesgo
     */
    public BigDecimal getTarifaPensionActividadAltoRiesgo() {
        return tarifaPensionActividadAltoRiesgo;
    }

    /**
     * @param tarifaPensionActividadAltoRiesgo the tarifaPensionActividadAltoRiesgo to set
     */
    public void setTarifaPensionActividadAltoRiesgo(BigDecimal tarifaPensionActividadAltoRiesgo) {
        this.tarifaPensionActividadAltoRiesgo = tarifaPensionActividadAltoRiesgo;
    }

    /**
     * @return the cotizacionPagadaPensionAltoRiesgo
     */
    public BigDecimal getCotizacionPagadaPensionAltoRiesgo() {
        return cotizacionPagadaPensionAltoRiesgo;
    }

    /**
     * @param cotizacionPagadaPensionAltoRiesgo the cotizacionPagadaPensionAltoRiesgo to set
     */
    public void setCotizacionPagadaPensionAltoRiesgo(BigDecimal cotizacionPagadaPensionAltoRiesgo) {
        this.cotizacionPagadaPensionAltoRiesgo = cotizacionPagadaPensionAltoRiesgo;
    }

    
}
