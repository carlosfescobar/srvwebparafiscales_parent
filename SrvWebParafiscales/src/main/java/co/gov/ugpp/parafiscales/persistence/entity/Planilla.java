/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author cpatingo
 */
@Entity
@Table(name = "PLANILLA")
public class Planilla extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private String id;
    @JoinColumn(name = "COD_TIPO_OPERADOR", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoOperador;
    @Column(name = "VAL_PLANILLA")
    private String valPlanilla;
    @JoinColumn(name = "COD_TIPO_OBSERVACIONES", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoObservaciones;
    
    @Column(name = "ESTADO_PLANILLA")
    private String estadoPlanilla;


    public String getEstadoPlanilla() {
        return estadoPlanilla;
    }

    public void setEstadoPlanilla(String estadoPlanilla) {
        this.estadoPlanilla = estadoPlanilla;
    }
    
    
    
    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ValorDominio getCodTipoOperador() {
        return codTipoOperador;
    }

    public void setCodTipoOperador(ValorDominio codTipoOperador) {
        this.codTipoOperador = codTipoOperador;
    }

    public String getValPlanilla() {
        return valPlanilla;
    }

    public void setValPlanilla(String valPlanilla) {
        this.valPlanilla = valPlanilla;
    }

    public ValorDominio getCodTipoObservaciones() {
        return codTipoObservaciones;
    }

    public void setCodTipoObservaciones(ValorDominio codTipoObservaciones) {
        this.codTipoObservaciones = codTipoObservaciones;
    }

}
