package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqporcentajeactividadeconomica", initialValue = 1, allocationSize = 1)
@Table(name = "PORCENTAJE_ACTIVIDAD_ECONOMICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PorcentajeActividadEconomica.findAll", query = "SELECT p FROM PorcentajeActividadEconomica p"),
    @NamedQuery(name = "PorcentajeActividadEconomica.findById", query = "SELECT p FROM PorcentajeActividadEconomica p WHERE p.id = :id"),
    @NamedQuery(name = "PorcentajeActividadEconomica.findByPorcentaje", query = "SELECT p FROM PorcentajeActividadEconomica p WHERE p.porcentaje = :porcentaje")})
public class PorcentajeActividadEconomica extends AbstractEntity<Long>  {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqporcentajeactividadeconomica")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "PORCENTAJE", precision = 5, scale = 2)
    private BigDecimal porcentaje;
    @JoinColumn(name = "T_BACTIVIDADECONOMICA", referencedColumnName = "T_BACE_INT_CLASE")
    @ManyToOne(fetch = FetchType.LAZY)
    private TBactividadeconomica tBactividadeconomica;

    public PorcentajeActividadEconomica() {
    }

    public PorcentajeActividadEconomica(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    public TBactividadeconomica getTBactividadeconomica() {
        return tBactividadeconomica;
    }

    public void setTBactividadeconomica(TBactividadeconomica tBactividadeconomica) {
        this.tBactividadeconomica = tBactividadeconomica;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PorcentajeActividadEconomica)) {
            return false;
        }
        PorcentajeActividadEconomica other = (PorcentajeActividadEconomica) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.intgr.PorcentajeActividadEconomica[ id=" + id + " ]";
    }
    
}
