package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqporcentajeniveleducativo", initialValue = 1, allocationSize = 1)
@Table(name = "PORCENTAJE_NIVEL_EDUCATIVO")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PorcentajeNivelEducativo.findAll", query = "SELECT p FROM PorcentajeNivelEducativo p"),
    @NamedQuery(name = "PorcentajeNivelEducativo.findById", query = "SELECT p FROM PorcentajeNivelEducativo p WHERE p.id = :id"),
    @NamedQuery(name = "PorcentajeNivelEducativo.findByNombreNivelEducativo", query = "SELECT p FROM PorcentajeNivelEducativo p WHERE p.nombreNivelEducativo = :nombreNivelEducativo"),
    @NamedQuery(name = "PorcentajeNivelEducativo.findByPorcentaje", query = "SELECT p FROM PorcentajeNivelEducativo p WHERE p.porcentaje = :porcentaje")})
public class PorcentajeNivelEducativo extends AbstractEntity<Long>  {
    @OneToMany(mappedBy = "nivelEducativo", fetch = FetchType.LAZY)
    private List<AportanteLIQ> aportanteList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqporcentajeniveleducativo")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "NOMBRE_NIVEL_EDUCATIVO")
    private String nombreNivelEducativo;
    @Column(name = "PORCENTAJE", precision =5, scale = 2)
    private BigDecimal porcentaje;

    public PorcentajeNivelEducativo() {
    }

    public PorcentajeNivelEducativo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreNivelEducativo() {
        return nombreNivelEducativo;
    }

    public void setNombreNivelEducativo(String nombreNivelEducativo) {
        this.nombreNivelEducativo = nombreNivelEducativo;
    }

    public BigDecimal getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(BigDecimal porcentaje) {
        this.porcentaje = porcentaje;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PorcentajeNivelEducativo)) {
            return false;
        }
        PorcentajeNivelEducativo other = (PorcentajeNivelEducativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.intgr.PorcentajeNivelEducativo[ id=" + id + " ]";
    }

    @XmlTransient
    public List<AportanteLIQ> getAportanteList() {
        return aportanteList;
    }

    public void setAportanteList(List<AportanteLIQ> aportanteList) {
        this.aportanteList = aportanteList;
    }
    
}
