package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "PRUEBA")
public class Prueba extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "pruebaIdSeq", sequenceName = "prueba_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pruebaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_TIPO_PRUEBA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPrueba;
    @Column(name = "FEC_SOLICITUD_PRUEBA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaSolicitudPrueba;
    @Column(name = "VAL_TIEMPO_ESPERA_PRUEBA_EXT")
    private String valTiempoEsperaPruebaExterna;
    @Column(name = "DESC_INFORMACION_SOLICITADA")
    private String desInformacionSolicitada;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EntidadExterna entidadExterna;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Aportante aportante;
    @JoinColumn(name = "COD_TIPO_PRUEBA_EXTERNA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPruebaExterna;
    @Column(name = "ES_PRUEBA_SOLICITADA")
    private Boolean esPruebaSolicitud;
    @Column(name = "ES_SOLICITADA_PRUEBA_INTERNA")
    private Boolean esSolicitudPruebaInterna;
    @Column(name = "ES_RESUELTA_SOLICITUD_INTERNA")
    private Boolean esResueltaSolicitudInterna;
    @Column(name = "ES_CONTINUADO_PROCESO_INTERNO")
    private Boolean esContinuadoProcesoInterno;
    @JoinColumn(name = "ID_SOL_PRUEBA_EXTERNA_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento solicitudPruebaExternaParcial;
    @JoinColumn(name = "ID_ACTO_SOL_PRUEBA_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoSolicitudPruebaExterna;
    @JoinColumn(name = "ID_INS_TRIBUTARIA_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento inspeccionTributariaParcial;
    @JoinColumn(name = "ID_ACTO_INSPECCION_TRIBUTARIA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoInspeccionTributaria;
    @Column(name = "DESC_OBS_SOLICITUD_INTERNA")
    private String descObservacionesSolicitudInterna;
    @Column(name = "FEC_INSPECCION_TRIBUTARIA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecInspeccionTributaria;
    @Column(name = "VAL_NOMBRE_INSPECTOR")
    private String valNombreInspector;
    @Column(name = "VAL_NOMBRE_PERSONA_CONTACTADA")
    private String valNombrePersonaContactadaInspeccion;
    @Column(name = "VAL_CARGO_PERSONA_CONTACTADA")
    private String valCargoPersonaContactadaInspeccion;
    @Column(name = "ID_DOC_INFORME_INSPECCION")
    private String idDocumentoInformeInspeccion;
    @Column(name = "DESC_OBSERVACIONES_INSPECCION")
    private String descObservacionesInspeccion;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RecursoReconsideracion recursoReconsideracion;

    public Prueba() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodTipoPrueba() {
        return codTipoPrueba;
    }

    public void setCodTipoPrueba(ValorDominio codTipoPrueba) {
        this.codTipoPrueba = codTipoPrueba;
    }

    public Calendar getFechaSolicitudPrueba() {
        return fechaSolicitudPrueba;
    }

    public void setFechaSolicitudPrueba(Calendar fechaSolicitudPrueba) {
        this.fechaSolicitudPrueba = fechaSolicitudPrueba;
    }

    public String getValTiempoEsperaPruebaExterna() {
        return valTiempoEsperaPruebaExterna;
    }

    public void setValTiempoEsperaPruebaExterna(String valTiempoEsperaPruebaExterna) {
        this.valTiempoEsperaPruebaExterna = valTiempoEsperaPruebaExterna;
    }

    public String getDesInformacionSolicitada() {
        return desInformacionSolicitada;
    }

    public void setDesInformacionSolicitada(String desInformacionSolicitada) {
        this.desInformacionSolicitada = desInformacionSolicitada;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public Boolean getEsPruebaSolicitud() {
        return esPruebaSolicitud;
    }

    public void setEsPruebaSolicitud(Boolean esPruebaSolicitud) {
        this.esPruebaSolicitud = esPruebaSolicitud;
    }

    public Boolean getEsSolicitudPruebaInterna() {
        return esSolicitudPruebaInterna;
    }

    public void setEsSolicitudPruebaInterna(Boolean esSolicitudPruebaInterna) {
        this.esSolicitudPruebaInterna = esSolicitudPruebaInterna;
    }

    public Boolean getEsResueltaSolicitudInterna() {
        return esResueltaSolicitudInterna;
    }

    public void setEsResueltaSolicitudInterna(Boolean esResueltaSolicitudInterna) {
        this.esResueltaSolicitudInterna = esResueltaSolicitudInterna;
    }

    public Boolean getEsContinuadoProcesoInterno() {
        return esContinuadoProcesoInterno;
    }

    public void setEsContinuadoProcesoInterno(Boolean esContinuadoProcesoInterno) {
        this.esContinuadoProcesoInterno = esContinuadoProcesoInterno;
    }

    public AprobacionDocumento getSolicitudPruebaExternaParcial() {
        return solicitudPruebaExternaParcial;
    }

    public void setSolicitudPruebaExternaParcial(AprobacionDocumento solicitudPruebaExternaParcial) {
        this.solicitudPruebaExternaParcial = solicitudPruebaExternaParcial;
    }

    public ActoAdministrativo getIdActoSolicitudPruebaExterna() {
        return idActoSolicitudPruebaExterna;
    }

    public void setIdActoSolicitudPruebaExterna(ActoAdministrativo idActoSolicitudPruebaExterna) {
        this.idActoSolicitudPruebaExterna = idActoSolicitudPruebaExterna;
    }

    public AprobacionDocumento getInspeccionTributariaParcial() {
        return inspeccionTributariaParcial;
    }

    public void setInspeccionTributariaParcial(AprobacionDocumento inspeccionTributariaParcial) {
        this.inspeccionTributariaParcial = inspeccionTributariaParcial;
    }

    public ActoAdministrativo getIdActoInspeccionTributaria() {
        return idActoInspeccionTributaria;
    }

    public void setIdActoInspeccionTributaria(ActoAdministrativo idActoInspeccionTributaria) {
        this.idActoInspeccionTributaria = idActoInspeccionTributaria;
    }

    public String getDescObservacionesSolicitudInterna() {
        return descObservacionesSolicitudInterna;
    }

    public void setDescObservacionesSolicitudInterna(String descObservacionesSolicitudInterna) {
        this.descObservacionesSolicitudInterna = descObservacionesSolicitudInterna;
    }

    public Calendar getFecInspeccionTributaria() {
        return fecInspeccionTributaria;
    }

    public void setFecInspeccionTributaria(Calendar fecInspeccionTributaria) {
        this.fecInspeccionTributaria = fecInspeccionTributaria;
    }

    public String getValNombreInspector() {
        return valNombreInspector;
    }

    public void setValNombreInspector(String valNombreInspector) {
        this.valNombreInspector = valNombreInspector;
    }

    public String getValNombrePersonaContactadaInspeccion() {
        return valNombrePersonaContactadaInspeccion;
    }

    public void setValNombrePersonaContactadaInspeccion(String valNombrePersonaContactadaInspeccion) {
        this.valNombrePersonaContactadaInspeccion = valNombrePersonaContactadaInspeccion;
    }

    public String getValCargoPersonaContactadaInspeccion() {
        return valCargoPersonaContactadaInspeccion;
    }

    public void setValCargoPersonaContactadaInspeccion(String valCargoPersonaContactadaInspeccion) {
        this.valCargoPersonaContactadaInspeccion = valCargoPersonaContactadaInspeccion;
    }

    public String getIdDocumentoInformeInspeccion() {
        return idDocumentoInformeInspeccion;
    }

    public void setIdDocumentoInformeInspeccion(String idDocumentoInformeInspeccion) {
        this.idDocumentoInformeInspeccion = idDocumentoInformeInspeccion;
    }

    public String getDescObservacionesInspeccion() {
        return descObservacionesInspeccion;
    }

    public void setDescObservacionesInspeccion(String descObservacionesInspeccion) {
        this.descObservacionesInspeccion = descObservacionesInspeccion;
    }

    public RecursoReconsideracion getRecursoReconsideracion() {
        return recursoReconsideracion;
    }

    public void setRecursoReconsideracion(RecursoReconsideracion recursoReconsideracion) {
        this.recursoReconsideracion = recursoReconsideracion;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public ValorDominio getCodTipoPruebaExterna() {
        return codTipoPruebaExterna;
    }

    public void setCodTipoPruebaExterna(ValorDominio codTipoPruebaExterna) {
        this.codTipoPruebaExterna = codTipoPruebaExterna;
    }

}
