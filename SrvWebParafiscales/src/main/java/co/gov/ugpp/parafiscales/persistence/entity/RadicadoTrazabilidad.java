package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "RADICADO_TRAZABILIDAD")

@NamedQueries({
    @NamedQuery(name = "radicadoTrazabilidad.findByRadicadonAndTrazabilidad",
            query = "SELECT rt FROM RadicadoTrazabilidad rt WHERE rt.idRadicado = :idRadicado AND rt.fechaRadicado = :fechaRadicado  AND rt.codRadicado.id = :idCodRadicado AND rt.trazabilidadActoAdministrativo.id = :idtrazabilidadRadicacionActo")
})
public class RadicadoTrazabilidad extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "radicadoTrazabilidadIdSeq", sequenceName = "radicado_trazabilidad_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "radicadoTrazabilidadIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_RADICADO")
    private String idRadicado;
    @Column(name = "FEC_RADICADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaRadicado;
    @JoinColumn(name = "COD_RADICADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codRadicado;
    @JoinColumn(name = "ID_TRAZA_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private TrazabilidadActoAdministrativo trazabilidadActoAdministrativo;

    public RadicadoTrazabilidad() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdRadicado() {
        return idRadicado;
    }

    public void setIdRadicado(String radicado) {
        this.idRadicado = radicado;
    }

    public Calendar getFechaRadicado() {
        return fechaRadicado;
    }

    public void setFechaRadicado(Calendar fechaRadicado) {
        this.fechaRadicado = fechaRadicado;
    }

    public ValorDominio getCodRadicado() {
        return codRadicado;
    }

    public void setCodRadicado(ValorDominio codRadicado) {
        this.codRadicado = codRadicado;
    }

    public TrazabilidadActoAdministrativo getTrazabilidadActoAdministrativo() {
        return trazabilidadActoAdministrativo;
    }

    public void setTrazabilidadActoAdministrativo(TrazabilidadActoAdministrativo trazabilidadActoAdministrativo) {
        this.trazabilidadActoAdministrativo = trazabilidadActoAdministrativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RadicadoTrazabilidad)) {
            return false;
        }
        RadicadoTrazabilidad other = (RadicadoTrazabilidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RadicadoTrazabilidad[ id=" + id + " ]";
    }

}
