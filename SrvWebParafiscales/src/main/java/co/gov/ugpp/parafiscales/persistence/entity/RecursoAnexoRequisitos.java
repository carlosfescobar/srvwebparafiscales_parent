package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "RECURSO_ANEXO_REQUISITOS")
@NamedQueries({
    @NamedQuery(name = "recursohasdocanexorequ.findByRecursoAndCodDocumAnexRequis",
            query = "SELECT fsi FROM RecursoAnexoRequisitos fsi WHERE fsi.codDocumentosAnexosRequisitos.id = :idcodDocumentosAnexosRequisitos AND fsi.recursoReconsideracion.id = :idRecursoReconsideracion")
})
public class RecursoAnexoRequisitos extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "recursoHasCodAnexoRequisistosIdSeq", sequenceName = "recu_rec_doc_anex_requi_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recursoHasCodAnexoRequisistosIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private RecursoReconsideracion recursoReconsideracion;
    @JoinColumn(name = "COD_DOCUMENTO_ANEXO_REQUISITO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codDocumentosAnexosRequisitos;

    public RecursoAnexoRequisitos() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecursoReconsideracion getRecursoReconsideracion() {
        return recursoReconsideracion;
    }

    public void setRecursoReconsideracion(RecursoReconsideracion recursoReconsideracion) {
        this.recursoReconsideracion = recursoReconsideracion;
    }

    public ValorDominio getCodDocumentosAnexosRequisitos() {
        return codDocumentosAnexosRequisitos;
    }

    public void setCodDocumentosAnexosRequisitos(ValorDominio codDocumentosAnexosRequisitos) {
        this.codDocumentosAnexosRequisitos = codDocumentosAnexosRequisitos;
    }

   

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecursoAnexoRequisitos)) {
            return false;
        }
        RecursoAnexoRequisitos other = (RecursoAnexoRequisitos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionHasIncumplimiento[ id=" + id + " ]";
    }

}
