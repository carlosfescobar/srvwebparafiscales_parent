package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "RECURSO_CAUSAL_FALLO")
@NamedQueries({
    @NamedQuery(name = "recursohascausalfallo.findByRecursoAndCodCausaFallo",
            query = "SELECT fsi FROM RecursoCausalFallo fsi WHERE fsi.codCausalesFallo.id = :idcodCausalesFallo AND fsi.recursoReconsideracion.id = :idRecursoReconsideracion")
})
public class RecursoCausalFallo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "recursoHasCodCausalFalloIdSeq", sequenceName = "recu_rec_caus_fallo_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recursoHasCodCausalFalloIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private RecursoReconsideracion recursoReconsideracion;
    @JoinColumn(name = "COD_CAUSAL_FALLO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codCausalesFallo;

    public RecursoCausalFallo() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RecursoReconsideracion getRecursoReconsideracion() {
        return recursoReconsideracion;
    }

    public void setRecursoReconsideracion(RecursoReconsideracion recursoReconsideracion) {
        this.recursoReconsideracion = recursoReconsideracion;
    }

    public ValorDominio getCodCausalesFallo() {
        return codCausalesFallo;
    }

    public void setCodCausalesFallo(ValorDominio codCausalesFallo) {
        this.codCausalesFallo = codCausalesFallo;
    }

  

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecursoCausalFallo)) {
            return false;
        }
        RecursoCausalFallo other = (RecursoCausalFallo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionHasIncumplimiento[ id=" + id + " ]";
    }

}
