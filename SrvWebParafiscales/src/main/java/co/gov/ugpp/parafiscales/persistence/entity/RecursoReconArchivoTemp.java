package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "RECURSO_RECON_ARCHIVO_TEMP")
public class RecursoReconArchivoTemp extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "recuRecAnexoTmpIdSeq", sequenceName = "recurso_recon_arch_temp_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recuRecAnexoTmpIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private Archivo archivo;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private RecursoReconsideracion recursoReconsideracion;
    @JoinColumn(name = "COD_TIPO_ANEXO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH, CascadeType.PERSIST})
    private ValorDominio codTipoAnexo;

    public RecursoReconArchivoTemp() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    public RecursoReconsideracion getRecursoReconsideracion() {
        return recursoReconsideracion;
    }

    public void setRecursoReconsideracion(RecursoReconsideracion recursoReconsideracion) {
        this.recursoReconsideracion = recursoReconsideracion;
    }

    public ValorDominio getCodTipoAnexo() {
        return codTipoAnexo;
    }

    public void setCodTipoAnexo(ValorDominio codTipoAnexo) {
        this.codTipoAnexo = codTipoAnexo;
    }
}
