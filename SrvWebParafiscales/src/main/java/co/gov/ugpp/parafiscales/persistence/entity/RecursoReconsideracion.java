package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 * @author jmunocab
 */
@Entity
@Table(name = "RECURSO_RECONSIDERACION")
public class RecursoReconsideracion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "recursoReconsideracionIdSeq", sequenceName = "recurso_reconsideracion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recursoReconsideracionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_PROCESO_EJECUTADOR", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codProcesoEjecutador;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Expediente expediente;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Aportante aportante;
    @Column(name = "FEC_ENTRADA_RECURSO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecEntradaRecursoReconsideracion;
    @Column(name = "ID_RADICADO_ENTRADA_RECURSO")
    private String idRadicadoEntradaRecursoReconsideracion;
    @JoinColumn(name = "ID_ACTO_REQ_DECLARAR_CORREGIR", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoRequerimientoDeclararCorregir;
    @Column(name = "ES_DIGITALIZADO_EXPEDIENTE")
    private Boolean esDigitalizadoExpediente;
    @Column(name = "DESC_OBS_DIGITALIZACION")
    private String descObservacionesDigitalizacion;
    @Column(name = "ES_ADMISIBLE_RECURSO")
    private Boolean esAdmisibleRecurso;
    @Column(name = "ES_SUBSANABLE")
    private Boolean esSubsanable;
    @Column(name = "DESC_OBS_REQUISITOS")
    private String descObservacionesRequisitos;
    @JoinColumn(name = "ID_AUTO_ADMISORIO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento autoAdmisorioParcial;
    @JoinColumn(name = "ID_ACTO_AUTO_ADMISORIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoAutoAdmisorio;
    @JoinColumn(name = "ID_AUTO_INADMISORIO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento autoInadmisorioParcial;
    @JoinColumn(name = "ID_ACTO_AUTO_INADMISORIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoAutoInadmisorio;
    @JoinColumn(name = "ID_LIQUIDADOR", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ControlLiquidador idLiquidador;
    @JoinColumn(name = "COD_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codEstado;
    @Column(name = "ES_RECURSO_REPOSICION")
    private Boolean esPresentadoRecursoReposicion;
    @Column(name = "ES_SUBSANADA_INADMISIBILIDAD")
    private Boolean esSubsanadaInadmisibilidad;
    @Column(name = "FEC_ENT_RECURSO_REPOSICION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecEntradaRecursoReposicion;
    @Column(name = "ID_RAD_ENT_RECURSO_REPOSICION")
    private String idRadicadoEntradaRecursoReposicion;
    @Column(name = "DESC_OBS_RECURSO_REPOSICION")
    private String descObservacionesRecursoReposicion;
    @JoinColumn(name = "ID_ACTO_INADMISION_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento actoAprobacionInadmisionParcial;
    @JoinColumn(name = "ID_ACTO_APROBACION_INADMISION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoAprobacionInadmision;
    @JoinColumn(name = "ID_ACTO_REPONE_ADMITE_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento actoReponeAdmiteParcial;
    @JoinColumn(name = "ID_ACTO_REPONE_ADMITE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoReponeAdmite;
    @Column(name = "ES_PRUEBAS_SUFICIENTES")
    private Boolean esPruebasSuficientes;
    @Column(name = "ES_NECESARIO_EJEC_LIQUIDADOR")
    private Boolean esNecesarioEjecutarLiquidador;
    @Column(name = "VAL_IMPORTE_SANCION")
    private String valImporteSancion;
    @Column(name = "VAL_NUEVO_IMPORTE_SANCION")
    private String valNuevoImporteSancion;
    @Column(name = "DESC_OBS_CALCULO_SANCION")
    private String descObservacionesCalculoSancion;
    @JoinColumn(name = "COD_FALLO_FINAL", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codFalloFinal;
    @Column(name = "VAL_FINAL_ACTO_ADMINISTRATIVO")
    private String valFinalActoAdministrativo;
    @Column(name = "DESC_OBS_FALLO")
    private String descObservacionesFallo;
    @JoinColumn(name = "ID_ACTO_FALLO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private AprobacionDocumento actoFalloParcial;
    @JoinColumn(name = "ID_ACTO_FALLO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ActoAdministrativo idActoFallo;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Prueba> pruebas;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecursoAnexoRequisitos> codDocumentosAnexosRequisitos;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecursoAnexoReposicion> codDocumentosAnexosRecursoReposicion;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RecursoCausalFallo> codCausalesFallo;
    @Column(name = "FEC_PRESENTACION_RECURSO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecPresentacionRecursoReconsideracion;
    @Column(name = "FEC_PRESENTACION_DEBIDA_FORMA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecPresentacionEnDebidaForma;
    @Column(name = "ID_LIQUIDACION")
    private String idLiquidacion;
    @Column(name = "ID_SANCION")
    private String idSancion;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RecursoReconArchivoTemp> archivosTemporales;
    
    public RecursoReconsideracion() {
    }

    public RecursoReconsideracion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodProcesoEjecutador() {
        return codProcesoEjecutador;
    }

    public void setCodProcesoEjecutador(ValorDominio codProcesoEjecutador) {
        this.codProcesoEjecutador = codProcesoEjecutador;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public Calendar getFecEntradaRecursoReconsideracion() {
        return fecEntradaRecursoReconsideracion;
    }

    public void setFecEntradaRecursoReconsideracion(Calendar fecEntradaRecursoReconsideracion) {
        this.fecEntradaRecursoReconsideracion = fecEntradaRecursoReconsideracion;
    }

    public String getIdRadicadoEntradaRecursoReconsideracion() {
        return idRadicadoEntradaRecursoReconsideracion;
    }

    public void setIdRadicadoEntradaRecursoReconsideracion(String idRadicadoEntradaRecursoReconsideracion) {
        this.idRadicadoEntradaRecursoReconsideracion = idRadicadoEntradaRecursoReconsideracion;
    }

    public ActoAdministrativo getIdActoRequerimientoDeclararCorregir() {
        return idActoRequerimientoDeclararCorregir;
    }

    public void setIdActoRequerimientoDeclararCorregir(ActoAdministrativo idActoRequerimientoDeclararCorregir) {
        this.idActoRequerimientoDeclararCorregir = idActoRequerimientoDeclararCorregir;
    }

    public Boolean getEsDsEsDigitalizadoExpediente() {
        return esDigitalizadoExpediente;
    }

    public void setEsDigitalizadoExpediente(Boolean esDigitalizadoExpediente) {
        this.esDigitalizadoExpediente = esDigitalizadoExpediente;
    }

    public String getDescObservacionesDigitalizacion() {
        return descObservacionesDigitalizacion;
    }

    public void setDescObservacionesDigitalizacion(String descObservacionesDigitalizacion) {
        this.descObservacionesDigitalizacion = descObservacionesDigitalizacion;
    }

    public Boolean getEsAdmisibleRecurso() {
        return esAdmisibleRecurso;
    }

    public void setEsAdmisibleRecurso(Boolean esAdmisibleRecurso) {
        this.esAdmisibleRecurso = esAdmisibleRecurso;
    }

    public Boolean getEsSubsanable() {
        return esSubsanable;
    }

    public void setEsSubsanable(Boolean esSubsanable) {
        this.esSubsanable = esSubsanable;
    }

    public String getDescObservacionesRequisitos() {
        return descObservacionesRequisitos;
    }

    public void setDescObservacionesRequisitos(String descObservacionesRequisitos) {
        this.descObservacionesRequisitos = descObservacionesRequisitos;
    }

    public AprobacionDocumento getAutoAdmisorioParcial() {
        return autoAdmisorioParcial;
    }

    public void setAutoAdmisorioParcial(AprobacionDocumento autoAdmisorioParcial) {
        this.autoAdmisorioParcial = autoAdmisorioParcial;
    }

    public ActoAdministrativo getIdActoAutoAdmisorio() {
        return idActoAutoAdmisorio;
    }

    public void setIdActoAutoAdmisorio(ActoAdministrativo idActoAutoAdmisorio) {
        this.idActoAutoAdmisorio = idActoAutoAdmisorio;
    }

    public AprobacionDocumento getAutoInadmisorioParcial() {
        return autoInadmisorioParcial;
    }

    public void setAutoInadmisorioParcial(AprobacionDocumento autoInadmisorioParcial) {
        this.autoInadmisorioParcial = autoInadmisorioParcial;
    }

    public ActoAdministrativo getIdActoAutoInadmisorio() {
        return idActoAutoInadmisorio;
    }

    public void setIdActoAutoInadmisorio(ActoAdministrativo idActoAutoInadmisorio) {
        this.idActoAutoInadmisorio = idActoAutoInadmisorio;
    }

    public ControlLiquidador getIdLiquidador() {
        return idLiquidador;
    }

    public void setIdLiquidador(ControlLiquidador idLiquidador) {
        this.idLiquidador = idLiquidador;
    }

    public ValorDominio getCodEstado() {
        return codEstado;
    }

    public void setCodEstado(ValorDominio codEstado) {
        this.codEstado = codEstado;
    }

    public Boolean getEsPresentadoRecursoReposicion() {
        return esPresentadoRecursoReposicion;
    }

    public void setEsPresentadoRecursoReposicion(Boolean esPresentadoRecursoReposicion) {
        this.esPresentadoRecursoReposicion = esPresentadoRecursoReposicion;
    }

    public Boolean getEsSubsanadaInadmisibilidad() {
        return esSubsanadaInadmisibilidad;
    }

    public void setEsSubsanadaInadmisibilidad(Boolean esSubsanadaInadmisibilidad) {
        this.esSubsanadaInadmisibilidad = esSubsanadaInadmisibilidad;
    }

    public Calendar getFecEntradaRecursoReposicion() {
        return fecEntradaRecursoReposicion;
    }

    public void setFecEntradaRecursoReposicion(Calendar fecEntradaRecursoReposicion) {
        this.fecEntradaRecursoReposicion = fecEntradaRecursoReposicion;
    }

    public String getIdRadicadoEntradaRecursoReposicion() {
        return idRadicadoEntradaRecursoReposicion;
    }

    public void setIdRadicadoEntradaRecursoReposicion(String idRadicadoEntradaRecursoReposicion) {
        this.idRadicadoEntradaRecursoReposicion = idRadicadoEntradaRecursoReposicion;
    }

    public String getDescObservacionesRecursoReposicion() {
        return descObservacionesRecursoReposicion;
    }

    public void setDescObservacionesRecursoReposicion(String descObservacionesRecursoReposicion) {
        this.descObservacionesRecursoReposicion = descObservacionesRecursoReposicion;
    }

    public AprobacionDocumento getActoAprobacionInadmisionParcial() {
        return actoAprobacionInadmisionParcial;
    }

    public void setActoAprobacionInadmisionParcial(AprobacionDocumento actoAprobacionInadmisionParcial) {
        this.actoAprobacionInadmisionParcial = actoAprobacionInadmisionParcial;
    }

    public ActoAdministrativo getIdActoAprobacionInadmision() {
        return idActoAprobacionInadmision;
    }

    public void setIdActoAprobacionInadmision(ActoAdministrativo idActoAprobacionInadmision) {
        this.idActoAprobacionInadmision = idActoAprobacionInadmision;
    }

    public AprobacionDocumento getActoReponeAdmiteParcial() {
        return actoReponeAdmiteParcial;
    }

    public void setActoReponeAdmiteParcial(AprobacionDocumento actoReponeAdmiteParcial) {
        this.actoReponeAdmiteParcial = actoReponeAdmiteParcial;
    }

    public ActoAdministrativo getIdActoReponeAdmite() {
        return idActoReponeAdmite;
    }

    public void setIdActoReponeAdmite(ActoAdministrativo idActoReponeAdmite) {
        this.idActoReponeAdmite = idActoReponeAdmite;
    }

    public Boolean getEsPruebasSuficientes() {
        return esPruebasSuficientes;
    }

    public void setEsPruebasSuficientes(Boolean esPruebasSuficientes) {
        this.esPruebasSuficientes = esPruebasSuficientes;
    }

    public Boolean getEsNecesarioEjecutarLiquidador() {
        return esNecesarioEjecutarLiquidador;
    }

    public void setEsNecesarioEjecutarLiquidador(Boolean esNecesarioEjecutarLiquidador) {
        this.esNecesarioEjecutarLiquidador = esNecesarioEjecutarLiquidador;
    }

    public String getValImporteSancion() {
        return valImporteSancion;
    }

    public void setValImporteSancion(String valImporteSancion) {
        this.valImporteSancion = valImporteSancion;
    }

    public String getValNuevoImporteSancion() {
        return valNuevoImporteSancion;
    }

    public void setValNuevoImporteSancion(String valNuevoImporteSancion) {
        this.valNuevoImporteSancion = valNuevoImporteSancion;
    }

    public String getDescObservacionesCalculoSancion() {
        return descObservacionesCalculoSancion;
    }

    public void setDescObservacionesCalculoSancion(String descObservacionesCalculoSancion) {
        this.descObservacionesCalculoSancion = descObservacionesCalculoSancion;
    }

    public ValorDominio getCodFalloFinal() {
        return codFalloFinal;
    }

    public void setCodFalloFinal(ValorDominio codFalloFinal) {
        this.codFalloFinal = codFalloFinal;
    }

    public String getValFinalActoAdministrativo() {
        return valFinalActoAdministrativo;
    }

    public void setValFinalActoAdministrativo(String valFinalActoAdministrativo) {
        this.valFinalActoAdministrativo = valFinalActoAdministrativo;
    }

    public String getDescObservacionesFallo() {
        return descObservacionesFallo;
    }

    public void setDescObservacionesFallo(String descObservacionesFallo) {
        this.descObservacionesFallo = descObservacionesFallo;
    }

    public AprobacionDocumento getActoFalloParcial() {
        return actoFalloParcial;
    }

    public void setActoFalloParcial(AprobacionDocumento actoFalloParcial) {
        this.actoFalloParcial = actoFalloParcial;
    }

    public ActoAdministrativo getIdActoFallo() {
        return idActoFallo;
    }

    public void setIdActoFallo(ActoAdministrativo idActoFallo) {
        this.idActoFallo = idActoFallo;
    }

    public List<Prueba> getPruebas() {
        if (pruebas == null) {
            pruebas = new ArrayList<Prueba>();
        }
        return pruebas;
    }

    public List<RecursoAnexoRequisitos> getCodDocumentosAnexosRequisitos() {
        if (codDocumentosAnexosRequisitos == null) {
            codDocumentosAnexosRequisitos = new ArrayList<RecursoAnexoRequisitos>();
        }
        return codDocumentosAnexosRequisitos;
    }

    public List<RecursoAnexoReposicion> getCodDocumentosAnexosRecursoReposicion() {
        if (codDocumentosAnexosRecursoReposicion == null) {
            codDocumentosAnexosRecursoReposicion = new ArrayList<RecursoAnexoReposicion>();
        }
        return codDocumentosAnexosRecursoReposicion;
    }

    public List<RecursoCausalFallo> getcodCausalesFallo() {
        if (codCausalesFallo == null) {
            codCausalesFallo = new ArrayList<RecursoCausalFallo>();
        }
        return codCausalesFallo;
    }

    public Calendar getFecPresentacionRecursoReconsideracion() {
        return fecPresentacionRecursoReconsideracion;
    }

    public void setFecPresentacionRecursoReconsideracion(Calendar fecPresentacionRecursoReconsideracion) {
        this.fecPresentacionRecursoReconsideracion = fecPresentacionRecursoReconsideracion;
    }

    public Calendar getFecPresentacionEnDebidaForma() {
        return fecPresentacionEnDebidaForma;
    }

    public void setFecPresentacionEnDebidaForma(Calendar fecPresentacionEnDebidaForma) {
        this.fecPresentacionEnDebidaForma = fecPresentacionEnDebidaForma;
    }

    public String getIdLiquidacion() {
        return idLiquidacion;
    }

    public void setIdLiquidacion(String idLiquidacion) {
        this.idLiquidacion = idLiquidacion;
    }

    public String getIdSancion() {
        return idSancion;
    }

    public void setIdSancion(String idSancion) {
        this.idSancion = idSancion;
    }

    public List<RecursoReconArchivoTemp> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<RecursoReconArchivoTemp>();
        }
        return archivosTemporales;
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecursoReconsideracion)) {
            return false;
        }
        RecursoReconsideracion other = (RecursoReconsideracion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RecursoReconsideracion[ id=" + id + " ]";
    }

}
