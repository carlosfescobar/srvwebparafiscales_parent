package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author yrinconh
 */
@Entity
@Table(name = "REENVIO_COMUNICACION")
public class ReenvioComunicacion extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;
    @Id
    @NotNull
    @Column(name = "ID_RADICADO")
    private String idRadicado;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "ID_PERSONA_DESTINATARIO", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Persona idPersonaDestinatario;
    @JoinColumn(name = "COD_PROCESO_ENVIA_COMUNICACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codProcesoEnviaComunicacion;
    @JoinColumn(name = "COD_TIPO_DIRECCION_ENVIADA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoDireccionEnviada;
    @Column(name = "VAL_DIRECCION_ENVIADA")
    private String valDireccionEnviada;
    @JoinColumn(name = "COD_FUENTE_INFORMACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codFuenteInformacion;
    @Column(name = "ES_DEVOLUCION")
    private Boolean esDevolucion;
    @Column(name = "ES_REENVIO")
    private Boolean esReenvio;
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;
    @JoinColumn(name = "ID_RADICADO_SALIDA", referencedColumnName = "ID_RADICADO")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<TrazabilidadRadicadoSalida> trazabilidadRadicadoSalida;

    public ReenvioComunicacion() {
    }

    @Override
    public String getId() {
        return idRadicado;
    }

    public void setId(String id) {
        this.idRadicado = id;
    }

    public String getIdRadicado() {
        return idRadicado;
    }

    public void setIdRadicado(String idRadicado) {
        this.idRadicado = idRadicado;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Persona getIdPersonaDestinatario() {
        return idPersonaDestinatario;
    }

    public void setIdPersonaDestinatario(Persona idPersonaDestinatario) {
        this.idPersonaDestinatario = idPersonaDestinatario;
    }

    public ValorDominio getCodProcesoEnviaComunicacion() {
        return codProcesoEnviaComunicacion;
    }

    public void setCodProcesoEnviaComunicacion(ValorDominio codProcesoEnviaComunicacion) {
        this.codProcesoEnviaComunicacion = codProcesoEnviaComunicacion;
    }

    public ValorDominio getCodTipoDireccionEnviada() {
        return codTipoDireccionEnviada;
    }

    public void setCodTipoDireccionEnviada(ValorDominio codTipoDireccionEnviada) {
        this.codTipoDireccionEnviada = codTipoDireccionEnviada;
    }

    public String getValDireccionEnviada() {
        return valDireccionEnviada;
    }

    public void setValDireccionEnviada(String valDireccionEnviada) {
        this.valDireccionEnviada = valDireccionEnviada;
    }

    public ValorDominio getCodFuenteInformacion() {
        return codFuenteInformacion;
    }

    public void setCodFuenteInformacion(ValorDominio codFuenteInformacion) {
        this.codFuenteInformacion = codFuenteInformacion;
    }

    public Boolean getEsDevolucion() {
        return esDevolucion;
    }

    public void setEsDevolucion(Boolean esDevolucion) {
        this.esDevolucion = esDevolucion;
    }

    public Boolean getEsReenvio() {
        return esReenvio;
    }

    public void setEsReenvio(Boolean esReenvio) {
        this.esReenvio = esReenvio;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public List<TrazabilidadRadicadoSalida> getTrazabilidadRadicadoSalida() {
        if(trazabilidadRadicadoSalida==null){
            trazabilidadRadicadoSalida= new ArrayList<TrazabilidadRadicadoSalida>();
        }
        return trazabilidadRadicadoSalida;
    }

}
