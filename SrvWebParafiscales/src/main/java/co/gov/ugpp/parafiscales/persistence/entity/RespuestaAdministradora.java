package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "RESPUESTA_ADMINISTRADORA")
public class RespuestaAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respuestaAdministradoraIdSeq", sequenceName = "respuesta_adm_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaAdministradoraIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_TIPO_RESPUESTA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoRespuesta;
    @JoinColumn(name = "ID_RESPUESTA_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RespuestaAdministradoraDecisionCaso> codDecisionCaso;
    @Column(name = "ES_PRORROGA")
    private Boolean esProrroga;
    @Column(name = "ES_CONCEDIDA_PRORROGA")
    private Boolean esConcedidaProrroga;
    @Column(name = "ES_INFORMACION_CORRECTA")
    private Boolean esInformacionCorrecta;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @Column(name = "ID_RADICADO_ENTRADA")
    private String idRadicadoEntrada;
    @Column(name = "FEC_INICIO_ESPERA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecInicioEspera;
    @Column(name = "FEC_RESPUESTA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecRespuesta;
    @JoinColumn(name = "ID_GESTION_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GestionAdministradora gestionAdministradora;
    @Column(name = "VAL_DIAS_CONCEDIDOS_PRORROGA")
    private Long diasConcedidosProrroga;
    @JoinColumn(name = "COD_TIPO_SANCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoSancion;
    @Column(name = "DESC_OBSERVACIONES_REENVIO")
    private String descObservacionesReenvio;
    @Column(name = "DESC_OBS_ENTIDAD_VIGILANCIA")
    private String descObservacionesEntidadVigilancia;
    @Column(name = "DESC_OBSERVACIONES_SANCION")
    private String descObservacionesSancion;

    public RespuestaAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    public void setCodTipoRespuesta(ValorDominio codTipoRespuesta) {
        this.codTipoRespuesta = codTipoRespuesta;
    }

    public List<RespuestaAdministradoraDecisionCaso> getCodDecisionCaso() {
        if (codDecisionCaso == null) {
            codDecisionCaso = new ArrayList<RespuestaAdministradoraDecisionCaso>();
        }
        return codDecisionCaso;
    }

    public Boolean getEsProrroga() {
        return esProrroga;
    }

    public void setEsProrroga(Boolean esProrroga) {
        this.esProrroga = esProrroga;
    }

    public Boolean getEsConcedidaProrroga() {
        return esConcedidaProrroga;
    }

    public void setEsConcedidaProrroga(Boolean esConcedidaProrroga) {
        this.esConcedidaProrroga = esConcedidaProrroga;
    }

    public Boolean getEsInformacionCorrecta() {
        return esInformacionCorrecta;
    }

    public void setEsInformacionCorrecta(Boolean esInformacionCorrecta) {
        this.esInformacionCorrecta = esInformacionCorrecta;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    public void setIdRadicadoEntrada(String idRadicadoEntrada) {
        this.idRadicadoEntrada = idRadicadoEntrada;
    }

    public Calendar getFecInicioEspera() {
        return fecInicioEspera;
    }

    public void setFecInicioEspera(Calendar fecInicioEspera) {
        this.fecInicioEspera = fecInicioEspera;
    }

    public Calendar getFecRespuesta() {
        return fecRespuesta;
    }

    public void setFecRespuesta(Calendar fecRespuesta) {
        this.fecRespuesta = fecRespuesta;
    }

    public GestionAdministradora getGestionAdministradora() {
        return gestionAdministradora;
    }

    public void setGestionAdministradora(GestionAdministradora gestionAdministradora) {
        this.gestionAdministradora = gestionAdministradora;
    }

    public Long getDiasConcedidosProrroga() {
        return diasConcedidosProrroga;
    }

    public void setDiasConcedidosProrroga(Long diasConcedidosProrroga) {
        this.diasConcedidosProrroga = diasConcedidosProrroga;
    }

    public ValorDominio getCodTipoSancion() {
        return codTipoSancion;
    }

    public void setCodTipoSancion(ValorDominio codTipoSancion) {
        this.codTipoSancion = codTipoSancion;
    }

    public String getDescObservacionesReenvio() {
        return descObservacionesReenvio;
    }

    public void setDescObservacionesReenvio(String descObservacionesReenvio) {
        this.descObservacionesReenvio = descObservacionesReenvio;
    }

    public String getDescObservacionesEntidadVigilancia() {
        return descObservacionesEntidadVigilancia;
    }

    public void setDescObservacionesEntidadVigilancia(String descObservacionesEntidadVigilancia) {
        this.descObservacionesEntidadVigilancia = descObservacionesEntidadVigilancia;
    }

    public String getDescObservacionesSancion() {
        return descObservacionesSancion;
    }

    public void setDescObservacionesSancion(String descObservacionesSancion) {
        this.descObservacionesSancion = descObservacionesSancion;
    }

}
