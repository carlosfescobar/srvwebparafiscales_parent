package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "RESPUESTA_ADMIN_DECISION_CASO")
@NamedQueries({
    @NamedQuery(name = "respuestaAdmDecisionCaso.findByRespuestaAdminAndCodDecisionCaso",
            query = "SELECT rac FROM RespuestaAdministradoraDecisionCaso rac WHERE rac.codDecisionCaso.id = :idcodDecisionCaso AND rac.respuestaAdministradora.id = :idRespuestaAdministradora")
})
public class RespuestaAdministradoraDecisionCaso extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respuestaAdmindecisioncasoIdSeq", sequenceName = "resp_admin_dec_caso_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaAdmindecisioncasoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RESPUESTA_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private RespuestaAdministradora respuestaAdministradora;
    @JoinColumn(name = "COD_DECISION_CASO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codDecisionCaso;

    public RespuestaAdministradoraDecisionCaso() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RespuestaAdministradora getRespuestaAdministradora() {
        return respuestaAdministradora;
    }

    public void setRespuestaAdministradora(RespuestaAdministradora respuestaAdministradora) {
        this.respuestaAdministradora = respuestaAdministradora;
    }

    public ValorDominio getCodDecisionCaso() {
        return codDecisionCaso;
    }

    public void setCodDecisionCaso(ValorDominio codDecisionCaso) {
        this.codDecisionCaso = codDecisionCaso;
    }


    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaAdministradoraDecisionCaso)) {
            return false;
        }
        RespuestaAdministradoraDecisionCaso other = (RespuestaAdministradoraDecisionCaso) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradoraDecisionCaso[ id=" + id + " ]";
    }

}
