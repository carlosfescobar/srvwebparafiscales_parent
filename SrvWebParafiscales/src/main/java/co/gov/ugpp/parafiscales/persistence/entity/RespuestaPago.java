package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "RESPUESTA_PAGO")
public class RespuestaPago extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respuestaPagoIdSeq", sequenceName = "RESPUESTA_PAGO_ID_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaPagoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_ANALISIS_INFORMACION_SOLIC")
    private Long idAnalisisInformacionSolic;
    @Size(max = 30)
    @Column(name = "VAL_TIPO_DOCUMENTO")
    private String tipoDocumento;
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private Long numeroDocumento;
    @Size(max = 200)
    @Column(name = "VAL_RAZON_SOCIAL")
    private String razonSocial;
    @Column(name = "FEC_PAGO")
//    @Temporal(TemporalType.TIMESTAMP)
    private String fecPago;
    @Column(name = "VAL_VALOR_PAGADO")
    private Long valorPagado;
    @Size(max = 100)
    @Column(name = "VAL_FUNCIONARIO_VALIDADOR")
    private String funcionarioValidador;
    @Size(max = 2000)
    @Column(name = "DESC_OBSERVACIONES")
    private String observaciones;
    @Size(max = 100)
    @Column(name = "CONFIRMACION")
    private String confirmacion;

    public RespuestaPago() {
    }

    public RespuestaPago(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Long getIdAnalisisInformacionSolic() {
        return idAnalisisInformacionSolic;
    }

    public void setIdAnalisisInformacionSolic(Long idAnalisisInformacionSolic) {
        this.idAnalisisInformacionSolic = idAnalisisInformacionSolic;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getFecPago() {
        return fecPago;
    }

    public void setFecPago(String fecPago) {
        this.fecPago = fecPago;
    }

    public Long getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(Long valorPagado) {
        this.valorPagado = valorPagado;
    }

    public String getFuncionarioValidador() {
        return funcionarioValidador;
    }

    public void setFuncionarioValidador(String funcionarioValidador) {
        this.funcionarioValidador = funcionarioValidador;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(String confirmacion) {
        this.confirmacion = confirmacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPago)) {
            return false;
        }
        RespuestaPago other = (RespuestaPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RespuestaPago[ id=" + id + " ]";
    }

}
