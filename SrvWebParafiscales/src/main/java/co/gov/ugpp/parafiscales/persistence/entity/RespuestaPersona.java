package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "RESPUESTA_PERSONA")
public class RespuestaPersona extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respuestaPersonaIdSeq", sequenceName = "RESPUESTA_PERSONA_ID_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaPersonaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_ANALISIS_INFORMACION_SOLIC")
    private Long idAnalisisInformacionSolic;
    @Size(max = 30)
    @Column(name = "VAL_NUMERO_CUENTA")
    private Long valNumeroCuenta;
    @Size(max = 30)
    @Column(name = "COD_TIPO_IDENTIFICACION")
    private String codTipoIdentificacion;
    @Column(name = "VAL_NUMERO_IDENTIFICACION")
    private Long valNumeroIdentificacion;
    @Size(max = 300)
    @Column(name = "VAL_RAZON_SOCIAL")
    private String valRazonSocial;
    @Size(max = 300)
    @Column(name = "VAL_NOMBRE_COMPLETO")
    private String valNombreCompleto;
    @Size(max = 200)
    @Column(name = "VAL_DIRECCION1")
    private String valDireccion1;
    @Size(max = 100)
    @Column(name = "VAL_DEPARTAMENTO1")
    private String valDepartamento1;

    @Size(max = 200)
    @Column(name = "VAL_DIRECCION2")
    private String valDireccion2;
    @Size(max = 100)
    @Column(name = "VAL_DEPARTAMENTO2")
    private String valDepartamento2;

    @Size(max = 30)
    @Column(name = "VAL_TELEFONO1")
    private String valTelefono1;
    @Size(max = 30)
    @Column(name = "VAL_TELEFONO2")
    private String valTelefono2;
    @Size(max = 10)
    @Column(name = "VAL_CELULAR")
    private Long valCelular;
    @Size(max = 200)
    @Column(name = "VAL_EMAIL")
    private String valEmail;
    @Size(max = 30)
    @Column(name = "VAL_FAX")
    private Long valFax;
    @Size(max = 30)
    @Column(name = "COD_TIPO_CUENTA")
    private String codTipoCuenta;
    @Size(max = 2000)
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;
    @Size(max = 200)
    @Column(name = "VAL_CIUDAD1")
    private String valCiudad1;
    @Size(max = 100)
    @Column(name = "VAL_CIUDAD2")
    private String valCiudad2;
    @Size(max = 100)
    @Column(name = "NOMBRE_BANCO")
    private String nombreBanco;
    @Size(max = 200)
    @Column(name = "VAL_DIREC_RESIDENCIA")
    private String valDireccionResidencia;
    @Size(max = 200)
    @Column(name = "VAL_DIREC_TRABAJO")
    private String valDireccionTrabajo;

    public RespuestaPersona() {
    }

    public RespuestaPersona(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Long getIdAnalisisInformacionSolic() {
        return idAnalisisInformacionSolic;
    }

    public void setIdAnalisisInformacionSolic(Long idAnalisisInformacionSolic) {
        this.idAnalisisInformacionSolic = idAnalisisInformacionSolic;
    }

    public Long getValNumeroCuenta() {
        return valNumeroCuenta;
    }

    public void setValNumeroCuenta(Long valNumeroCuenta) {
        this.valNumeroCuenta = valNumeroCuenta;
    }

    public String getCodTipoIdentificacion() {
        return codTipoIdentificacion;
    }

    public void setCodTipoIdentificacion(String codTipoIdentificacion) {
        this.codTipoIdentificacion = codTipoIdentificacion;
    }

    public Long getValNumeroIdentificacion() {
        return valNumeroIdentificacion;
    }

    public void setValNumeroIdentificacion(Long valNumeroIdentificacion) {
        this.valNumeroIdentificacion = valNumeroIdentificacion;
    }

    public String getValRazonSocial() {
        return valRazonSocial;
    }

    public void setValRazonSocial(String valRazonSocial) {
        this.valRazonSocial = valRazonSocial;
    }

    public String getValNombreCompleto() {
        return valNombreCompleto;
    }

    public void setValNombreCompleto(String valNombreCompleto) {
        this.valNombreCompleto = valNombreCompleto;
    }

    public String getValDireccion1() {
        return valDireccion1;
    }

    public void setValDireccion1(String valDireccion1) {
        this.valDireccion1 = valDireccion1;
    }

    public String getValDepartamento1() {
        return valDepartamento1;
    }

    public void setValDepartamento1(String valDepartamento1) {
        this.valDepartamento1 = valDepartamento1;
    }

    public String getValDireccion2() {
        return valDireccion2;
    }

    public void setValDireccion2(String valDireccion2) {
        this.valDireccion2 = valDireccion2;
    }

    public String getValDepartamento2() {
        return valDepartamento2;
    }

    public void setValDepartamento2(String valDepartamento2) {
        this.valDepartamento2 = valDepartamento2;
    }

    public String getValTelefono1() {
        return valTelefono1;
    }

    public void setValTelefono1(String valTelefono1) {
        this.valTelefono1 = valTelefono1;
    }

    public String getValTelefono2() {
        return valTelefono2;
    }

    public void setValTelefono2(String valTelefono2) {
        this.valTelefono2 = valTelefono2;
    }

    public Long getValCelular() {
        return valCelular;
    }

    public void setValCelular(Long valCelular) {
        this.valCelular = valCelular;
    }

    public String getValEmail() {
        return valEmail;
    }

    public void setValEmail(String valEmail) {
        this.valEmail = valEmail;
    }

    public Long getValFax() {
        return valFax;
    }

    public void setValFax(Long valFax) {
        this.valFax = valFax;
    }

    public String getCodTipoCuenta() {
        return codTipoCuenta;
    }

    public void setCodTipoCuenta(String codTipoCuenta) {
        this.codTipoCuenta = codTipoCuenta;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public String getValCiudad1() {
        return valCiudad1;
    }

    public void setValCiudad1(String valCiudad1) {
        this.valCiudad1 = valCiudad1;
    }

    public String getValCiudad2() {
        return valCiudad2;
    }

    public void setValCiudad2(String valCiudad2) {
        this.valCiudad2 = valCiudad2;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getValDireccionResidencia() {
        return valDireccionResidencia;
    }

    public void setValDireccionResidencia(String valDireccionResidencia) {
        this.valDireccionResidencia = valDireccionResidencia;
    }

    public String getValDireccionTrabajo() {
        return valDireccionTrabajo;
    }

    public void setValDireccionTrabajo(String valDireccionTrabajo) {
        this.valDireccionTrabajo = valDireccionTrabajo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPersona)) {
            return false;
        }
        RespuestaPersona other = (RespuestaPersona) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RespuestaPersona[ id=" + id + " ]";
    }

}
