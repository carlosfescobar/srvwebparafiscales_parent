package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "RESPUESTA_PLANTILLA")
public class RespuestaPlantilla extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_ANALISIS_INFORMACION_SOLIC")
    private Long idAnalisisInformacionSolic;
    @Size(max = 30)
    @Column(name = "TIPO_DOCUMENTO")
    private String tipoDocumento;
    @Column(name = "NUMERO_DOCUMENTO")
    private Long numeroDocumento;
    @Column(name = "COD_OPERADOR")
    private Long codOperador;
    @Column(name = "FEC_CARGUE_PILA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecCarguePila;
    @Size(max = 100)
    @Column(name = "NOMBRE_LOTE")
    private String nombreLote;
    @Size(max = 2000)
    @Column(name = "OBSERVACIONES")
    private String observaciones;

    public RespuestaPlantilla() {
    }

    public RespuestaPlantilla(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Long getIdAnalisisInformacionSolic() {
        return idAnalisisInformacionSolic;
    }

    protected void setIdAnalisisInformacionSolic(Long idAnalisisInformacionSolic) {
        this.idAnalisisInformacionSolic = idAnalisisInformacionSolic;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Long getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Long numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Long getCodOperador() {
        return codOperador;
    }

    public void setCodOperador(Long codOperador) {
        this.codOperador = codOperador;
    }

    public Calendar getFecCarguePila() {
        return fecCarguePila;
    }

    public void setFecCarguePila(Calendar fecCarguePila) {
        this.fecCarguePila = fecCarguePila;
    }

    public String getNombreLote() {
        return nombreLote;
    }

    public void setNombreLote(String nombreLote) {
        this.nombreLote = nombreLote;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaPlantilla)) {
            return false;
        }
        RespuestaPlantilla other = (RespuestaPlantilla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RespuestaPlantilla[ id=" + id + " ]";
    }

}
