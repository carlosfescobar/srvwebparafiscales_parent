package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "RESPUESTA_REQUERIMIENTO_INFO")
@NamedQueries({
    @NamedQuery(name = "respuestaRequerimiento.findByRespuestanAndIdRadicadoEntrada",
            query = "SELECT rri FROM RespuestaRequerimientoInformacion rri WHERE rri.idRadicadoEntrada = :idRadicadoEntrada")
})
public class RespuestaRequerimientoInformacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respuestaRequerimientoInformacionIdSeq", sequenceName = "resp_req_inf_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaRequerimientoInformacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 38)
    @JoinColumn(name = "ID_FISCALIZACION", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Fiscalizacion idFiscalizacion;
    @Column(name = "FEC_RADIC_ENTRADA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaRadicadoEntrada;
    @Column(name = "ID_RADIC_ENTRADA")
    private String idRadicadoEntrada;
    @JoinColumn(name = "ID_RESPUESTA_REQUERIMIENTO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RespuestaRequerimientoListaChequeo> codListadoChequeo;

    public RespuestaRequerimientoInformacion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Fiscalizacion getIdFiscalizacion() {
        return idFiscalizacion;
    }

    public void setIdFiscalizacion(Fiscalizacion idFiscalizacion) {
        this.idFiscalizacion = idFiscalizacion;
    }

    public Calendar getFechaRadicadoEntrada() {
        return fechaRadicadoEntrada;
    }

    public void setFechaRadicadoEntrada(Calendar fechaRadicadoEntrada) {
        this.fechaRadicadoEntrada = fechaRadicadoEntrada;
    }

    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    public void setIdRadicadoEntrada(String idRadicadoEntrada) {
        this.idRadicadoEntrada = idRadicadoEntrada;
    }

     public List<RespuestaRequerimientoListaChequeo> getCodListadoChequeo() {
        if (codListadoChequeo == null) {
            codListadoChequeo = new ArrayList<RespuestaRequerimientoListaChequeo>();
        }
        return codListadoChequeo;
    }
}
