package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "RESPUESTA_REQ_LISTA_CHEQUEO")
@NamedQueries({
    @NamedQuery(name = "respuestaRequerimientoListadoChequeo.findByRespuestanAndCodListadoChequeo",
            query = "SELECT rri FROM RespuestaRequerimientoListaChequeo rri WHERE rri.codListadoChequeo.id = :idcodListadoChequeo AND rri.respuestaRequerimientoInformacion.id = :idrespuestaRequerimientoInformacion")
})
public class RespuestaRequerimientoListaChequeo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "respRequerimientoInformacion", sequenceName = "resp_req_inf_list_cheq_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respRequerimientoInformacion")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_RESPUESTA_REQUERIMIENTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private RespuestaRequerimientoInformacion respuestaRequerimientoInformacion;
    @JoinColumn(name = "COD_LISTADO_CHEQUEO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ValorDominio codListadoChequeo;

    public RespuestaRequerimientoListaChequeo() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RespuestaRequerimientoInformacion getRespuestaRequerimientoInformacion() {
        return respuestaRequerimientoInformacion;
    }

    public void setRespuestaRequerimientoInformacion(RespuestaRequerimientoInformacion respuestaRequerimientoInformacion) {
        this.respuestaRequerimientoInformacion = respuestaRequerimientoInformacion;
    }

    public ValorDominio getCodListadoChequeo() {
        return codListadoChequeo;
    }

    public void setCodListadoChequeo(ValorDominio codListadoChequeo) {
        this.codListadoChequeo = codListadoChequeo;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaRequerimientoListaChequeo)) {
            return false;
        }
        RespuestaRequerimientoListaChequeo other = (RespuestaRequerimientoListaChequeo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionHasIncumplimiento[ id=" + id + " ]";
    }

}
