package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "RESPUESTA_SOLICITUD")
public class RespuestaSolicitud extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "respuestaSolicitudIdSeq", sequenceName = "RESPUESTA_SOLICITUD_ID_SEQ", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "respuestaSolicitudIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_ENVIO_INFO_EXTERNA")
    private String idRadicado;

    public RespuestaSolicitud() {
    }

    public RespuestaSolicitud(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getIdRadicado() {
        return idRadicado;
    }

    public void setIdRadicado(String idRadicado) {
        this.idRadicado = idRadicado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RespuestaSolicitud)) {
            return false;
        }
        RespuestaSolicitud other = (RespuestaSolicitud) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.RespuestaSolicitud[ id=" + id + " ]";
    }

}
