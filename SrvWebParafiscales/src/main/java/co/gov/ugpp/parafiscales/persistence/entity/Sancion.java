package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "SANCION")
public class Sancion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "sancionIdSeq", sequenceName = "sancion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sancionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_INVESTIGADO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona investigado;
    @JoinColumn(name = "COD_PROCESO_PADRE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codProcesoPadre;
    @Column(name = "ID_PROCESO_PADRE")
    private String idProcesoPadre;
    @JoinColumn(name = "ID_EXPEDIENTE_CASO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expedienteCaso;
    @JoinColumn(name = "ID_EXPEDIENTE_SANCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expedienteSancion;
    @JoinColumn(name = "COD_TIPO_SANCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoSancion;
    @JoinColumn(name = "COD_ESTADO_SANCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoSancion;
    @Column(name = "VAL_GESTOR_SANCION")
    private String gestorSancion;
    @Column(name = "ES_SANCION")
    private Boolean esSancion;
    @Column(name = "DESC_OBS_NO_PROCEDE_SANCION")
    private String desObservacionesNoProcedeSancion;
    @JoinColumn(name = "ID_PLIEGO_GARGOS_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(fetch = FetchType.LAZY)
    private AprobacionDocumento pliegoCargosParcial;
    @JoinColumn(name = "ID_ACTO_PLIEGO_CARGOS", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActoAdministrativo idActoPliegoCargos;
    @JoinColumn(name = "ID_RESOLUCION_SANCION_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(fetch = FetchType.LAZY)
    private AprobacionDocumento resolucionSancionParcial;
    @JoinColumn(name = "ID_ACTO_RESOLUCION_SANCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActoAdministrativo idActoResolucionSancion;
    @JoinColumn(name = "ID_AUTO_ARCHIVO_PARCIAL", referencedColumnName = "ID_ARCHIVO")
    @ManyToOne(fetch = FetchType.LAZY)
    private AprobacionDocumento autoArchivoParcial;
    @JoinColumn(name = "ID_ACTO_AUTO_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActoAdministrativo idActoAutoArchivo;
    @JoinColumn(name = "ID_RECURSO_RECONSIDERACION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private RecursoReconsideracion idRecursoReconsideracion;
    @JoinColumn(name = "ID_CONSTANCIA_EJECUTORIA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ConstanciaEjecutoria idConstanciaEjecutoria;
    @Column(name = "FEC_LLEGADA_EXT_FISCALIZACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecLlegadaExtemporaneaFiscalizacion;
    @JoinColumn(name = "COD_DECISION_EXT_LIQUIDACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codDecisionExtemporaneaLiquidacion;
    @Column(name = "DESC_OBS_DECISION_EXT")
    private String desObservacionDecisionExtemporanea;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SancionPruebaRequerida> pruebaRequerida;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SancionInformacionRequerida> informacionRequerida;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SancionHasAccion> acciones;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<AnalisisSancion> analisisSancion;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<LiquidacionParcial> liquidacionesParciales;

    public Sancion() {
    }

    public Sancion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Persona getInvestigado() {
        return investigado;
    }

    public void setInvestigado(Persona investigado) {
        this.investigado = investigado;
    }

    public ValorDominio getCodProcesoPadre() {
        return codProcesoPadre;
    }

    public void setCodProcesoPadre(ValorDominio codProcesoPadre) {
        this.codProcesoPadre = codProcesoPadre;
    }

    public String getIdProcesoPadre() {
        return idProcesoPadre;
    }

    public void setIdProcesoPadre(String idProcesoPadre) {
        this.idProcesoPadre = idProcesoPadre;
    }

    public Expediente getExpedienteCaso() {
        return expedienteCaso;
    }

    public void setExpedienteCaso(Expediente expedienteCaso) {
        this.expedienteCaso = expedienteCaso;
    }

    public Expediente getExpedienteSancion() {
        return expedienteSancion;
    }

    public void setExpedienteSancion(Expediente expedienteSancion) {
        this.expedienteSancion = expedienteSancion;
    }

    public ValorDominio getCodTipoSancion() {
        return codTipoSancion;
    }

    public void setCodTipoSancion(ValorDominio codTipoSancion) {
        this.codTipoSancion = codTipoSancion;
    }

    public ValorDominio getCodEstadoSancion() {
        return codEstadoSancion;
    }

    public void setCodEstadoSancion(ValorDominio codEstadoSancion) {
        this.codEstadoSancion = codEstadoSancion;
    }

    public String getGestorSancion() {
        return gestorSancion;
    }

    public void setGestorSancion(String gestorSancion) {
        this.gestorSancion = gestorSancion;
    }

    public Boolean getEsSancion() {
        return esSancion;
    }

    public void setEsSancion(Boolean esSancion) {
        this.esSancion = esSancion;
    }

    public String getDesObservacionesNoProcedeSancion() {
        return desObservacionesNoProcedeSancion;
    }

    public void setDesObservacionesNoProcedeSancion(String desObservacionesNoProcedeSancion) {
        this.desObservacionesNoProcedeSancion = desObservacionesNoProcedeSancion;
    }

    public AprobacionDocumento getPliegoCargosParcial() {
        return pliegoCargosParcial;
    }

    public void setPliegoCargosParcial(AprobacionDocumento pliegoCargosParcial) {
        this.pliegoCargosParcial = pliegoCargosParcial;
    }

    public ActoAdministrativo getIdActoPliegoCargos() {
        return idActoPliegoCargos;
    }

    public void setIdActoPliegoCargos(ActoAdministrativo idActoPliegoCargos) {
        this.idActoPliegoCargos = idActoPliegoCargos;
    }

    public AprobacionDocumento getResolucionSancionParcial() {
        return resolucionSancionParcial;
    }

    public void setResolucionSancionParcial(AprobacionDocumento resolucionSancionParcial) {
        this.resolucionSancionParcial = resolucionSancionParcial;
    }

    public ActoAdministrativo getIdActoResolucionSancion() {
        return idActoResolucionSancion;
    }

    public void setIdActoResolucionSancion(ActoAdministrativo idActoResolucionSancion) {
        this.idActoResolucionSancion = idActoResolucionSancion;
    }

    public AprobacionDocumento getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    public void setAutoArchivoParcial(AprobacionDocumento autoArchivoParcial) {
        this.autoArchivoParcial = autoArchivoParcial;
    }

    public ActoAdministrativo getIdActoAutoArchivo() {
        return idActoAutoArchivo;
    }

    public void setIdActoAutoArchivo(ActoAdministrativo idActoAutoArchivo) {
        this.idActoAutoArchivo = idActoAutoArchivo;
    }

    public RecursoReconsideracion getIdRecursoReconsideracion() {
        return idRecursoReconsideracion;
    }

    public void setIdRecursoReconsideracion(RecursoReconsideracion idRecursoReconsideracion) {
        this.idRecursoReconsideracion = idRecursoReconsideracion;
    }

    public ConstanciaEjecutoria getIdConstanciaEjecutoria() {
        return idConstanciaEjecutoria;
    }

    public void setIdConstanciaEjecutoria(ConstanciaEjecutoria idConstanciaEjecutoria) {
        this.idConstanciaEjecutoria = idConstanciaEjecutoria;
    }

    public Calendar getFecLlegadaExtemporaneaFiscalizacion() {
        return fecLlegadaExtemporaneaFiscalizacion;
    }

    public void setFecLlegadaExtemporaneaFiscalizacion(Calendar fecLlegadaExtemporaneaFiscalizacion) {
        this.fecLlegadaExtemporaneaFiscalizacion = fecLlegadaExtemporaneaFiscalizacion;
    }

    public ValorDominio getCodDecisionExtemporaneaLiquidacion() {
        return codDecisionExtemporaneaLiquidacion;
    }

    public void setCodDecisionExtemporaneaLiquidacion(ValorDominio codDecisionExtemporaneaLiquidacion) {
        this.codDecisionExtemporaneaLiquidacion = codDecisionExtemporaneaLiquidacion;
    }

    public String getDesObservacionDecisionExtemporanea() {
        return desObservacionDecisionExtemporanea;
    }

    public void setDesObservacionDecisionExtemporanea(String desObservacionDecisionExtemporanea) {
        this.desObservacionDecisionExtemporanea = desObservacionDecisionExtemporanea;
    }

    public List<SancionPruebaRequerida> getPruebaRequerida() {
        if (pruebaRequerida == null) {
            pruebaRequerida = new ArrayList<SancionPruebaRequerida>();
        }
        return pruebaRequerida;
    }

    public List<SancionInformacionRequerida> getInformacionRequerida() {
        if (informacionRequerida == null) {
            informacionRequerida = new ArrayList<SancionInformacionRequerida>();
        }
        return informacionRequerida;
    }

    public List<SancionHasAccion> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<SancionHasAccion>();
        }
        return acciones;
    }

    public List<AnalisisSancion> getAnalisisSancion() {
        if (analisisSancion == null) {
            analisisSancion = new ArrayList<AnalisisSancion>();
        }
        return analisisSancion;
    }

    public List<LiquidacionParcial> getLiquidacionesParciales() {
        if (liquidacionesParciales == null) {
            liquidacionesParciales = new ArrayList<LiquidacionParcial>();
        }
        return liquidacionesParciales;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sancion)) {
            return false;
        }
        Sancion other = (Sancion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Sancion[ id=" + id + " ]";
    }

}
