package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "SANCION_INFORMACION_REQUERIDA")

public class SancionInformacionRequerida extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "informacionRequeridaIdSeq", sequenceName = "informacion_requerida_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "informacionRequeridaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_INFORMACION_PRUEBA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY,cascade ={CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH})
    private InformacionPrueba informacionPrueba;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
      @ManyToOne(fetch = FetchType.LAZY,cascade = {CascadeType.REFRESH, CascadeType.PERSIST, CascadeType.MERGE,CascadeType.DETACH})
    private Sancion sancion;

    public SancionInformacionRequerida() {
    }

    public SancionInformacionRequerida(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public InformacionPrueba getInformacionPrueba() {
        return informacionPrueba;
    }

    public void setInformacionPrueba(InformacionPrueba informacionPrueba) {
        this.informacionPrueba = informacionPrueba;
    }

    public Sancion getSancion() {
        return sancion;
    }

    public void setSancion(Sancion sancion) {
        this.sancion = sancion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SancionInformacionRequerida)) {
            return false;
        }
        SancionInformacionRequerida other = (SancionInformacionRequerida) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.InformacionRequerida[ id=" + id + " ]";
    }

}
