package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "SEGUIMIENTO")
public class Seguimiento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "seguimientoIdSeq", sequenceName = "seguimiento_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seguimientoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Size(max = 2000)
    @Column(name = "DESC_OBSERVACIONES")
    private String descObservaciones;
    @Size(max = 100)
    @Column(name = "VAL_PERSONA_CONTACTADA")
    private String valPersonaContactada;
    @Size(max = 100)
    @Column(name = "VAL_CARGO_PER_CONTACTADA")
    private String cargoPersonaContactada;
    @Size(max = 30)
    @Column(name = "VAL_PLAZO_ENTREGA")
    private String plazoEntrega;
    @Column(name = "FEC_PROGRAMADA_SEGUIMIENTO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecProgramadaSeguimiento;
    @Size(max = 50)
    @Column(name = "VAL_USUARIO_ENCARGADO")
    private String valUsuarioEncargado;
    @Size(max = 2000)
    @Column(name = "DESC_OBS_PROGRAMACION")
    private String descObsProgramacion;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Expediente expediente;

    public Seguimiento() {
    }

    public Seguimiento(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public String getDescObservaciones() {
        return descObservaciones;
    }

    public void setDescObservaciones(String descObservaciones) {
        this.descObservaciones = descObservaciones;
    }

    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    public void setValPersonaContactada(String valPersonaContactada) {
        this.valPersonaContactada = valPersonaContactada;
    }

    public String getCargoPersonaContactada() {
        return cargoPersonaContactada;
    }

    public void setCargoPersonaContactada(String cargoPersonaContactada) {
        this.cargoPersonaContactada = cargoPersonaContactada;
    }

    public String getPlazoEntrega() {
        return plazoEntrega;
    }

    public void setPlazoEntrega(String plazoEntrega) {
        this.plazoEntrega = plazoEntrega;
    }

    public Calendar getFecProgramadaSeguimiento() {
        return fecProgramadaSeguimiento;
    }

    public void setFecProgramadaSeguimiento(Calendar fecProgramadaSeguimiento) {
        this.fecProgramadaSeguimiento = fecProgramadaSeguimiento;
    }

    public String getValUsuarioEncargado() {
        return valUsuarioEncargado;
    }

    public void setValUsuarioEncargado(String valUsuarioEncargado) {
        this.valUsuarioEncargado = valUsuarioEncargado;
    }

    public String getDescObsProgramacion() {
        return descObsProgramacion;
    }

    public void setDescObsProgramacion(String descObsProgramacion) {
        this.descObsProgramacion = descObsProgramacion;
    }
    
    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguimiento)) {
            return false;
        }
        Seguimiento other = (Seguimiento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Seguimiento[ id=" + id + " ]";
    }    

}
