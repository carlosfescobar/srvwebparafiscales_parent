package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "SERIE_DOCUMENTAL")
public class SerieDocumental extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "serieDocumentalIdSeq", sequenceName = "serie_documental_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "serieDocumentalIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "VAL_NOMBRE_SERIE")
    private String nombreSerie;
    @Column(name = "COD_SERIE")
    private String codSerie;
    @Column(name = "VAL_NOMBRE_SUBSERIE")
    private String nombreSubSerie;
    @Column(name = "COD_SUBSERIE")
    private String codSubSerie;
    @Column(name = "COD_TIPO_DOCUMENTAL")
    private String codTipoDocumental;

    public SerieDocumental() {
    }

    public SerieDocumental(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getNombreSerie() {
        return nombreSerie;
    }

    public void setNombreSerie(String nombreSerie) {
        this.nombreSerie = nombreSerie;
    }

    public String getCodSerie() {
        return codSerie;
    }

    public void setCodSerie(String codSerie) {
        this.codSerie = codSerie;
    }

    public String getNombreSubSerie() {
        return nombreSubSerie;
    }

    public void setNombreSubSerie(String nombreSubSerie) {
        this.nombreSubSerie = nombreSubSerie;
    }

    public String getCodSubSerie() {
        return codSubSerie;
    }

    public void setCodSubSerie(String codSubSerie) {
        this.codSubSerie = codSubSerie;
    }

    public String getCodTipoDocumental() {
        return codTipoDocumental;
    }

    public void setCodTipoDocumental(String codTipoDocumental) {
        this.codTipoDocumental = codTipoDocumental;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SerieDocumental)) {
            return false;
        }
        SerieDocumental other = (SerieDocumental) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
