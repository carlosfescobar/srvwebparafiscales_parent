package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "solicitud.findByEntidadExterna",
            query = "SELECT s FROM Solicitud s WHERE s.entidadExterna.id = :idEntidad AND s.codEstadoSolicitud.id NOT IN :estadosObviar ORDER BY s.fecSolicitud DESC"),
    @NamedQuery(name = "solicitud.findByIdListAndTipo",
            query = "SELECT e FROM Solicitud e WHERE e.id IN :idList AND e.codTipoSolicitud.id = :tipoSolicitud")})
@Table(name = "SOLICITUD")
public class Solicitud extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudIdSeq", sequenceName = "solicitud_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Aportante aportante;
    @JoinColumn(name = "ID_RADICADO_ENTRADA", referencedColumnName = "ID_RADICADO_ENVIO")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private EnvioInformacionExterna radicadoEntrada;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FEC_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecSolicitud;
    @Column(name = "ES_CONSULTA_NO_APORTANTE")
    private Boolean esConsultaNoAportante;
    @Basic(optional = true)
    @NotNull
    @Column(name = "ES_CONSULTA_APROBADA")
    private Boolean esConsultaAprobada;
    @Column(name = "ES_APROBAR_SOLICITUD")
    private Boolean esAprobarSolicitud;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "VAL_USUARIO")
    private String valUsuario;
    @Size(max = 2000)
    @Column(name = "DESC_OBSERVACION")
    private String descObservacion;
    @Size(max = 2000)
    @Column(name = "DESC_OBSERVACION_APROBADA")
    private String descObservacionAprobada;
    @Size(max = 2000)
    @Column(name = "VAL_RESUMEN")
    private String valResumen;
    @Size(max = 2000)
    @Column(name = "DESC_FILTRO")
    private String descFiltro;
    @Size(max = 2000)
    @Column(name = "DESC_CRUCES")
    private String descCruces;
    @Size(max = 2000)
    @Column(name = "DESC_RESULTADO_ESPERADO")
    private String descResultadoEsperado;
    @Size(max = 2000)
    @Column(name = "VAL_RUTA_ARCHIVO_RESULTADO")
    private String valRutaArchivoResultado;
    @Size(max = 2000)
    @Column(name = "DESC_OBS_REQUERIMIENTO")
    private String vlaObservacionRequerimiento;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudSeguimiento> solicitudSeguimientoList;
    @JoinColumn(name = "COD_MEDIO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codMedio;
    @JoinColumn(name = "COD_TIPO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ValorDominio codTipoSolicitud;
    @JoinColumn(name = "COD_ESTADO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private ValorDominio codEstadoSolicitud;
    @JoinColumn(name = "COD_TIPO_CONSULTA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codTipoConsulta;
    @JoinColumn(name = "ID_NO_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private Persona noAportante;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Expediente expediente;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private EntidadExterna entidadExterna;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.EAGER)
    private List<SolicitudPago> solicitudPagoList;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudPlanilla> solicitudPlanillaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "soliciutd", fetch = FetchType.EAGER)
    private List<SolicitudCotizante> soliciutdCotizanteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitud", fetch = FetchType.EAGER)
    private List<FuenteInformacion> fuenteInformacionList;
    @Column(name = "ES_ACTUALIZACION")
    private Boolean esActualizacion;
    @JoinColumn(name = "COD_TIPO_PERIODICIDAD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codTipoPeriodicidad;
    @JoinColumn(name = "COD_TIPO_SUBSISTEMA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codTipoSubsistema;
    @Size(max = 50)
    @Column(name = "VAL_NOMBRE_PROGRAMA")
    private String valNombrePrograma;
    @Size(max = 50)
    @Column(name = "VAL_PERIODO_ENVIO")
    private String valPeriodoEnvio;
    @Column(name = "FEC_INICIO_PROGRAMA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioPrograma;
    @Column(name = "FEC_FIN_PROGRAMA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinPrograma;
    @Size(max = 50)
    @Column(name = "VAL_PLAZO_ESPERA")
    private String valPlazoEspera;
    @JoinColumn(name = "COD_TIPO_PLAZO_ESPERA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codTipoPlazoEspera;
    @Size(max = 2000)
    @Column(name = "DES_INFO_REQUERIDA")
    private String desInfoRequerida;
    @Column(name = "ES_NUEVA_ESTRUCTURA_DATOS")
    private Boolean esNuevaEstructuraDatos;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudDocAnexo> solicitudDocAnexoList;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudArchivoAnexo> solicitudArchivoAnexoList;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudFuenteInformacion> solicitudFuenteInformacionList;
    @JoinColumn(name = "DOC_SOLICITUD_APORTANTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Archivo docSolicitudAportante;
    @JoinColumn(name = "ESTRUCTURA_SUGERIDA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Archivo estructuraSugerida;
    @Size(max = 50)
    @Column(name = "ID_RADICADO_SALIDA")
    private String idRadicadoSalida;
    @Size(max = 2000)
    @Column(name = "DESC_OBSER_NO_APORTANTE")
    private String descObserNoAportante;
    @Size(max = 50)
    @Column(name = "VAL_PORCENTAJE_EFECTIVIDAD")
    private String valPorcentajeEjectividad;
    @JoinColumn(name = "ID_FORMATO_ESTRUCTURA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private FormatoEstructura formatoEstructura;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<SolicitudExpedienteDocumentoEcm> documentosSolicitud;
    @Column(name = "FEC_RTA_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRtaSolicitud;
    @Column(name = "FEC_DEVOLUCION_ENTIDAD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecDevolucionEntidad;
    @Column(name = "ES_DEVUELTO")
    private Boolean esDevuelto;

    public Solicitud() {
    }

    public Solicitud(Long id) {
        this.id = id;
    }

    public Solicitud(Long id, Calendar fecSolicitud, Boolean esConsultaAprobada, String valUsuario) {
        this.id = id;
        this.fecSolicitud = fecSolicitud;
        this.esConsultaAprobada = esConsultaAprobada;
        this.valUsuario = valUsuario;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Aportante getAportante() {
        return aportante;
    }

    public void setAportante(Aportante aportante) {
        this.aportante = aportante;
    }

    public String getDescObserNoAportante() {
        return descObserNoAportante;
    }

    public void setDescObserNoAportante(String descObserNoAportante) {
        this.descObserNoAportante = descObserNoAportante;
    }

    public EnvioInformacionExterna getRadicadoEntrada() {
        return radicadoEntrada;
    }

    public void setRadicadoEntrada(EnvioInformacionExterna radicadoEntrada) {
        this.radicadoEntrada = radicadoEntrada;
    }

    public Calendar getFecSolicitud() {
        return fecSolicitud;
    }

    public void setFecSolicitud(Calendar fecSolicitud) {
        this.fecSolicitud = fecSolicitud;
    }

    public Boolean getEsConsultaNoAportante() {
        return esConsultaNoAportante;
    }

    public void setEsConsultaNoAportante(Boolean esConsultaNoAportante) {
        this.esConsultaNoAportante = esConsultaNoAportante;
    }

    public Boolean getEsConsultaAprobada() {
        return esConsultaAprobada;
    }

    public void setEsConsultaAprobada(Boolean esConsultaAprobada) {
        this.esConsultaAprobada = esConsultaAprobada;
    }

    public String getValUsuario() {
        return valUsuario;
    }

    public void setValUsuario(String valUsuario) {
        this.valUsuario = valUsuario;
    }

    public String getDescObservacion() {
        return descObservacion;
    }

    public void setDescObservacion(String descObservacion) {
        this.descObservacion = descObservacion;
    }

    public String getDescObservacionAprobada() {
        return descObservacionAprobada;
    }

    public void setDescObservacionAprobada(String descObservacionAprobada) {
        this.descObservacionAprobada = descObservacionAprobada;
    }

    public String getValResumen() {
        return valResumen;
    }

    public void setValResumen(String valResumen) {
        this.valResumen = valResumen;
    }

    public String getDescFiltro() {
        return descFiltro;
    }

    public void setDescFiltro(String descFiltro) {
        this.descFiltro = descFiltro;
    }

    public String getDescCruces() {
        return descCruces;
    }

    public void setDescCruces(String descCruces) {
        this.descCruces = descCruces;
    }

    public String getDescResultadoEsperado() {
        return descResultadoEsperado;
    }

    public void setDescResultadoEsperado(String descResultadoEsperado) {
        this.descResultadoEsperado = descResultadoEsperado;
    }

    public String getValRutaArchivoResultado() {
        return valRutaArchivoResultado;
    }

    public void setValRutaArchivoResultado(String valRutaArchivoResultado) {
        this.valRutaArchivoResultado = valRutaArchivoResultado;
    }

    public List<SolicitudSeguimiento> getSolicitudSeguimientoList() {
        if (solicitudSeguimientoList == null) {
            solicitudSeguimientoList = new ArrayList<SolicitudSeguimiento>();
        }
        return solicitudSeguimientoList;
    }

    public void setSolicitudSeguimientoList(List<SolicitudSeguimiento> solicitudSeguimientoList) {
        this.solicitudSeguimientoList = solicitudSeguimientoList;
    }

    public ValorDominio getCodMedio() {
        return codMedio;
    }

    public void setCodMedio(ValorDominio codMedio) {
        this.codMedio = codMedio;
    }

    public ValorDominio getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    public void setCodTipoSolicitud(ValorDominio codTipoSolicitud) {
        this.codTipoSolicitud = codTipoSolicitud;
    }

    public ValorDominio getCodEstadoSolicitud() {
        return codEstadoSolicitud;
    }

    public void setCodEstadoSolicitud(ValorDominio codEstadoSolicitud) {
        this.codEstadoSolicitud = codEstadoSolicitud;
    }

    public ValorDominio getCodTipoConsulta() {
        return codTipoConsulta;
    }

    public void setCodTipoConsulta(ValorDominio codTipoConsulta) {
        this.codTipoConsulta = codTipoConsulta;
    }

    public Persona getNoAportante() {
        return noAportante;
    }

    public void setNoAportante(Persona noAportante) {
        this.noAportante = noAportante;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public List<SolicitudPago> getSolicitudPagoList() {
        if (solicitudPagoList == null) {
            solicitudPagoList = new ArrayList<SolicitudPago>();
        }
        return solicitudPagoList;
    }

    public void setSolicitudPagoList(List<SolicitudPago> solicitudPagoList) {
        this.solicitudPagoList = solicitudPagoList;
    }

    public List<SolicitudPlanilla> getSolicitudPlanillaList() {
        if (solicitudPlanillaList == null) {
            solicitudPlanillaList = new ArrayList<SolicitudPlanilla>();
        }
        return solicitudPlanillaList;
    }

    public void setSolicitudPlanillaList(List<SolicitudPlanilla> solicitudPlanillaList) {
        this.solicitudPlanillaList = solicitudPlanillaList;
    }

    public List<SolicitudCotizante> getSoliciutdCotizanteList() {
        if (soliciutdCotizanteList == null) {
            soliciutdCotizanteList = new ArrayList<SolicitudCotizante>();
        }

        return soliciutdCotizanteList;
    }

    public void setSoliciutdCotizanteList(List<SolicitudCotizante> soliciutdCotizanteList) {
        this.soliciutdCotizanteList = soliciutdCotizanteList;
    }

    public List<FuenteInformacion> getFuenteInformacionList() {
        if (fuenteInformacionList == null) {
            fuenteInformacionList = new ArrayList<FuenteInformacion>();
        }
        return fuenteInformacionList;
    }

    public void setFuenteInformacionList(List<FuenteInformacion> fuenteInformacionList) {
        this.fuenteInformacionList = fuenteInformacionList;
    }

    public String getVlaObservacionRequerimiento() {
        return vlaObservacionRequerimiento;
    }

    public void setVlaObservacionRequerimiento(String vlaObservacionRequerimiento) {
        this.vlaObservacionRequerimiento = vlaObservacionRequerimiento;
    }

    public Boolean getEsActualizacion() {
        return esActualizacion;
    }

    public void setEsActualizacion(Boolean esActualizacion) {
        this.esActualizacion = esActualizacion;
    }

    public ValorDominio getCodTipoPeriodicidad() {
        return codTipoPeriodicidad;
    }

    public void setCodTipoPeriodicidad(ValorDominio codTipoPeriodicidad) {
        this.codTipoPeriodicidad = codTipoPeriodicidad;
    }

    public ValorDominio getCodTipoSubsistema() {
        return codTipoSubsistema;
    }

    public void setCodTipoSubsistema(ValorDominio codTipoSubsistema) {
        this.codTipoSubsistema = codTipoSubsistema;
    }

    public String getValNombrePrograma() {
        return valNombrePrograma;
    }

    public void setValNombrePrograma(String valNombrePrograma) {
        this.valNombrePrograma = valNombrePrograma;
    }

    public String getValPeriodoEnvio() {
        return valPeriodoEnvio;
    }

    public void setValPeriodoEnvio(String valPeriodoEnvio) {
        this.valPeriodoEnvio = valPeriodoEnvio;
    }

    public Calendar getFecInicioPrograma() {
        return fecInicioPrograma;
    }

    public void setFecInicioPrograma(Calendar fecInicioPrograma) {
        this.fecInicioPrograma = fecInicioPrograma;
    }

    public Calendar getFecFinPrograma() {
        return fecFinPrograma;
    }

    public void setFecFinPrograma(Calendar fecFinPrograma) {
        this.fecFinPrograma = fecFinPrograma;
    }

    public String getValPlazoEspera() {
        return valPlazoEspera;
    }

    public void setValPlazoEspera(String valPlazoEspera) {
        this.valPlazoEspera = valPlazoEspera;
    }

    public ValorDominio getCodTipoPlazoEspera() {
        return codTipoPlazoEspera;
    }

    public void setCodTipoPlazoEspera(ValorDominio codTipoPlazoEspera) {
        this.codTipoPlazoEspera = codTipoPlazoEspera;
    }

    public String getDesInfoRequerida() {
        return desInfoRequerida;
    }

    public void setDesInfoRequerida(String desInfoRequerida) {
        this.desInfoRequerida = desInfoRequerida;
    }

    public Boolean getEsNuevaEstructuraDatos() {
        return esNuevaEstructuraDatos;
    }

    public void setEsNuevaEstructuraDatos(Boolean esNuevaEstructuraDatos) {
        this.esNuevaEstructuraDatos = esNuevaEstructuraDatos;
    }

    public List<SolicitudDocAnexo> getSolicitudDocAnexoList() {
        if (solicitudDocAnexoList == null) {
            solicitudDocAnexoList = new ArrayList<SolicitudDocAnexo>();
        }
        return solicitudDocAnexoList;
    }

    public Archivo getDocSolicitudAportante() {
        return docSolicitudAportante;
    }

    public void setDocSolicitudAportante(Archivo docSolicitudAportante) {
        this.docSolicitudAportante = docSolicitudAportante;
    }

    public String getValPorcentajeEjectividad() {
        return valPorcentajeEjectividad;
    }

    public void setValPorcentajeEjectividad(String valPorcentajeEjectividad) {
        this.valPorcentajeEjectividad = valPorcentajeEjectividad;

    }

    public Calendar getFecRtaSolicitud() {
        return fecRtaSolicitud;
    }

    public void setFecRtaSolicitud(Calendar fecRtaSolicitud) {
        this.fecRtaSolicitud = fecRtaSolicitud;
    }

    public List<SolicitudArchivoAnexo> getSolicitudArchivoAnexoList() {
        if (solicitudArchivoAnexoList == null) {
            solicitudArchivoAnexoList = new ArrayList<SolicitudArchivoAnexo>();
        }
        return solicitudArchivoAnexoList;
    }

    public void setSolicitudArchivoAnexoList(List<SolicitudArchivoAnexo> solicitudArchivoAnexoList) {
        this.solicitudArchivoAnexoList = solicitudArchivoAnexoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public List<SolicitudFuenteInformacion> getSolicitudFuenteInformacionList() {
        if (solicitudFuenteInformacionList == null) {
            solicitudFuenteInformacionList = new ArrayList<SolicitudFuenteInformacion>();
        }
        return solicitudFuenteInformacionList;
    }

    public void setSolicitudFuenteInformacionList(List<SolicitudFuenteInformacion> solicitudFuenteInformacionList) {
        this.solicitudFuenteInformacionList = solicitudFuenteInformacionList;
    }

    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    public void setIdRadicadoSalida(String idRadicadoSalida) {
        this.idRadicadoSalida = idRadicadoSalida;
    }

    public Archivo getEstructuraSugerida() {
        return estructuraSugerida;
    }

    public void setEstructuraSugerida(Archivo estructuraSugerida) {
        this.estructuraSugerida = estructuraSugerida;
    }

    public FormatoEstructura getFormatoEstructura() {
        return formatoEstructura;
    }

    public void setFormatoEstructura(FormatoEstructura formatoEstructura) {
        this.formatoEstructura = formatoEstructura;
    }

    public Boolean getEsAprobarSolicitud() {
        return esAprobarSolicitud;
    }

    public void setEsAprobarSolicitud(Boolean esAprobarSolicitud) {
        this.esAprobarSolicitud = esAprobarSolicitud;
    }

    public List<SolicitudExpedienteDocumentoEcm> getDocumentosSolicitud() {
        if (documentosSolicitud == null) {
            documentosSolicitud = new ArrayList<SolicitudExpedienteDocumentoEcm>();
        }

        return documentosSolicitud;
    }

    public Calendar getFecDevolucionEntidad() {
        return fecDevolucionEntidad;
    }

    public void setFecDevolucionEntidad(Calendar fecDevolucionEntidad) {
        this.fecDevolucionEntidad = fecDevolucionEntidad;
    }

    public Boolean getEsDevuelto() {
        return esDevuelto;
    }

    public void setEsDevuelto(Boolean esDevuelto) {
        this.esDevuelto = esDevuelto;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitud)) {
            return false;
        }
        Solicitud other = (Solicitud) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Solicitud[ id=" + id + " ]";
    }

}
