package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Everis
 */
@Entity
@Table(name = "SOLICITUD_ADMINISTRADORA")
public class SolicitudAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudadminIdSeq", sequenceName = "solic_administradora_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudadminIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_EXPEDIENTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Expediente expediente;
    @JoinColumn(name = "ID_VALIDACION_ESTRUCTURA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValidacionEstructura validacionEstructura;
    @JoinColumn(name = "COD_ESTADO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoSolicitud;
    @JoinColumn(name = "COD_TIPO_SOLICITUD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoSolicitud;
    @JoinColumn(name = "COD_TIPO_RESPUESTA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoRespuesta;
    @JoinColumn(name = "COD_TIPO_PERIODICIDAD", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPeriodicidad;
    @JoinColumn(name = "COD_TIPO_PLAZO_ENTREGA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codTipoPlazoEntrega;
    @Column(name = "DESC_INFO_REQUERIDA")
    private String descInformacionRequerida;
    @Column(name = "VAL_CANTIDAD_PLAZO_ENTREGA")
    private String valCantidadPlazoEntrega;
    @Column(name = "VAL_NOMBRE_SOLICITUD")
    private String valNombreSolicitud;
    @Column(name = "FEC_INICIO_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicioSolicitud;
    @Column(name = "FEC_FIN_SOLICITUD")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinSolicitud;
    @Column(name = "ES_NECESITA_CORRECCION")
    private Boolean esNecesitaCorreccion;
    @Column(name = "ES_SOLICITUD_ACEPTADA")
    private Boolean esSolicitudAceptada;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<AgrupacionSubsistemaSolAdmin> administradorasSeleccionadas;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SolicitudAdministradoraAccion> acciones;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SolicitudAdministradoraCorreccion> solicitudesCorreccion;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<GestionSolAdministradora> administradorasDefinitivas;
    @JoinColumn(name = "ID_ARCHIVO_SOLICUTD_INICIAL", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Archivo idArchivoSolicitudInicial;
    @Column(name = "ID_FUNCIONARIO_SOLICITUD")
    private String idFuncionarioSolicitud;

    public SolicitudAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public ValorDominio getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    public void setCodTipoSolicitud(ValorDominio codTipoSolicitud) {
        this.codTipoSolicitud = codTipoSolicitud;
    }

    public ValorDominio getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    public void setCodTipoRespuesta(ValorDominio codTipoRespuesta) {
        this.codTipoRespuesta = codTipoRespuesta;
    }

    public ValorDominio getCodTipoPeriodicidad() {
        return codTipoPeriodicidad;
    }

    public void setCodTipoPeriodicidad(ValorDominio codTipoPeriodicidad) {
        this.codTipoPeriodicidad = codTipoPeriodicidad;
    }

    public ValorDominio getCodTipoPlazoEntrega() {
        return codTipoPlazoEntrega;
    }

    public void setCodTipoPlazoEntrega(ValorDominio codTipoPlazoEntrega) {
        this.codTipoPlazoEntrega = codTipoPlazoEntrega;
    }

    public String getValCantidadPlazoEntrega() {
        return valCantidadPlazoEntrega;
    }

    public void setValCantidadPlazoEntrega(String valCantidadPlazoEntrega) {
        this.valCantidadPlazoEntrega = valCantidadPlazoEntrega;
    }

    public String getValNombreSolicitud() {
        return valNombreSolicitud;
    }

    public void setValNombreSolicitud(String valNombreSolicitud) {
        this.valNombreSolicitud = valNombreSolicitud;
    }

    public String getDescInformacionRequerida() {
        return descInformacionRequerida;
    }

    public void setDescInformacionRequerida(String descInformacionRequerida) {
        this.descInformacionRequerida = descInformacionRequerida;
    }

    public Calendar getFecInicioSolicitud() {
        return fecInicioSolicitud;
    }

    public void setFecInicioSolicitud(Calendar fecInicioSolicitud) {
        this.fecInicioSolicitud = fecInicioSolicitud;
    }

    public Calendar getFecFinSolicitud() {
        return fecFinSolicitud;
    }

    public void setFecFinSolicitud(Calendar fecFinSolicitud) {
        this.fecFinSolicitud = fecFinSolicitud;
    }

    public ValorDominio getCodEstadoSolicitud() {
        return codEstadoSolicitud;
    }

    public void setCodEstadoSolicitud(ValorDominio codEstadoSolicitud) {
        this.codEstadoSolicitud = codEstadoSolicitud;
    }

    public Boolean getEsNecesitaCorreccion() {
        return esNecesitaCorreccion;
    }

    public void setEsNecesitaCorreccion(Boolean esNecesitaCorreccion) {
        this.esNecesitaCorreccion = esNecesitaCorreccion;
    }

    public Boolean getEsSolicitudAceptada() {
        return esSolicitudAceptada;
    }

    public void setEsSolicitudAceptada(Boolean esSolicitudAceptada) {
        this.esSolicitudAceptada = esSolicitudAceptada;
    }

    public ValidacionEstructura getValidacionEstructura() {
        return validacionEstructura;
    }

    public void setValidacionEstructura(ValidacionEstructura validacionEstructura) {
        this.validacionEstructura = validacionEstructura;
    }

    public List<SolicitudAdministradoraAccion> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<SolicitudAdministradoraAccion>();
        }
        return acciones;
    }

    public List<AgrupacionSubsistemaSolAdmin> getAdministradorasSeleccionadas() {
        if (administradorasSeleccionadas == null) {
            administradorasSeleccionadas = new ArrayList<AgrupacionSubsistemaSolAdmin>();
        }
        return administradorasSeleccionadas;
    }

    public List<SolicitudAdministradoraCorreccion> getSolicitudesCorreccion() {
        if (solicitudesCorreccion == null) {
            solicitudesCorreccion = new ArrayList<SolicitudAdministradoraCorreccion>();
        }
        return solicitudesCorreccion;
    }

    public List<GestionSolAdministradora> getAdministradorasDefinitivas() {
        if (administradorasDefinitivas == null) {
            administradorasDefinitivas = new ArrayList<GestionSolAdministradora>();
        }
        return administradorasDefinitivas;
    }

    public Archivo getIdArchivoSolicitudInicial() {
        return idArchivoSolicitudInicial;
    }

    public void setIdArchivoSolicitudInicial(Archivo idArchivoSolicitudInicial) {
        this.idArchivoSolicitudInicial = idArchivoSolicitudInicial;
    }

    public String getIdFuncionarioSolicitud() {
        return idFuncionarioSolicitud;
    }

    public void setIdFuncionarioSolicitud(String idFuncionarioSolicitud) {
        this.idFuncionarioSolicitud = idFuncionarioSolicitud;
    }

}
