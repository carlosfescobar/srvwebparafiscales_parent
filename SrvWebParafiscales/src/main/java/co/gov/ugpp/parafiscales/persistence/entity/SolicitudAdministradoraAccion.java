package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Everis
 */
@Entity
@Table(name = "SOLICITUD_ADMINISTRAD_ACCION")
@NamedQueries({
    @NamedQuery(name = "solicitudAdministradoraAccion.findBySolicitudAdministradoraAndAccion",
            query = "SELECT saa FROM SolicitudAdministradoraAccion saa WHERE saa.solicitudAdministradora.id= :idSolicitudAdministradora AND saa.accion.id= :idAccion")
})
public class SolicitudAdministradoraAccion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudadmaccionIdSeq", sequenceName = "solic_administ_accion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudadmaccionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SolicitudAdministradora solicitudAdministradora;
    @JoinColumn(name = "ID_ACCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Accion accion;

    public SolicitudAdministradoraAccion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SolicitudAdministradora getSolicitudAdministradora() {
        return solicitudAdministradora;
    }

    public void setSolicitudAdministradora(SolicitudAdministradora solictudAdministradora) {
        this.solicitudAdministradora = solictudAdministradora;
    }

    public Accion getAccion() {
        return accion;
    }

    public void setAccion(Accion accion) {
        this.accion = accion;
    }
}