package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "SOLICITUD_ADMINIST_CORRECCION")
@NamedQueries({
    @NamedQuery(name = "solicitudAdministradoraCorreccion.findBySolicitudAdministradoraAndDescCorreccion",
            query = "SELECT sac FROM SolicitudAdministradoraCorreccion sac WHERE sac.solicitudAdministradora.id= :idSolicitudAdministradora AND sac.descSolicitudCorreccion =:descSolicitudCorreccion")
})
public class SolicitudAdministradoraCorreccion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudadmicorreccionIdSeq", sequenceName = "solic_administ_correcc_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudadmicorreccionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD_ADMINISTRADORA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private SolicitudAdministradora solicitudAdministradora;
    @Column(name = "DESC_SOLICITUD_CORRECCION")
    private String descSolicitudCorreccion;

    public SolicitudAdministradoraCorreccion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

     public SolicitudAdministradora getSolicitudAdministradora() {
        return solicitudAdministradora;
    }

    public void setSolicitudAdministradora(SolicitudAdministradora solicitudAdministradora) {
        this.solicitudAdministradora = solicitudAdministradora;
    }
    
     public String getDescSolicitudCorreccion() {
        return descSolicitudCorreccion;
    }

    public void setDescSolicitudCorreccion(String descSolicitudCorreccion) {
        this.descSolicitudCorreccion = descSolicitudCorreccion;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudAdministradoraCorreccion)) {
            return false;
        }
        SolicitudAdministradoraCorreccion other = (SolicitudAdministradoraCorreccion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraCorreccion[ id=" + id + " ]";
    }
}
