package co.gov.ugpp.parafiscales.persistence.entity;

import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "SOLICITUD_COTIZANTE")
public class SolicitudCotizante extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudCotizanteIdSeq", sequenceName = "soliciutd_cotizante_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudCotizanteIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_SOLICIUTD", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Solicitud soliciutd;
    @JoinColumn(name = "ID_COTIZANTE", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Cotizante cotizante;
    @JoinColumn(name = "ID_ARCHIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Archivo archivo;

    public SolicitudCotizante() {
    }

    public SolicitudCotizante(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Solicitud getSoliciutd() {
        return soliciutd;
    }

    public void setSoliciutd(Solicitud soliciutd) {
        this.soliciutd = soliciutd;
    }

    public Cotizante getCotizante() {
        return cotizante;
    }

    public void setCotizante(Cotizante cotizante) {
        this.cotizante = cotizante;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(final Object object) {
        if (object instanceof CotizanteTipo) {
            final CotizanteTipo other = (CotizanteTipo) object;
            return this.cotizante.getPersona() != null && this.cotizante.getPersona().equals(other);
        }
        if (object instanceof Cotizante) {
            final Cotizante other = (Cotizante) object;
            return this.cotizante.getPersona() != null && this.cotizante.getPersona().equals(other.getPersona());
        }
        return false;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.SoliciutdCotizante[ id=" + id + " ]";
    }

}
