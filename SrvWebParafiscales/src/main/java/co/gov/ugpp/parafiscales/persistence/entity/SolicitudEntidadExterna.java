
package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "SOLICITUD_ENTIDAD_EXTERNA")
public class SolicitudEntidadExterna extends AbstractEntity<Long> {

    private static final long serialVersionUID = 530994698709098L;

    @Id
    @SequenceGenerator(name = "solicitudEntidadExterIdSeq", sequenceName = "solicitud_entidad_exter_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudEntidadExterIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "NUMERO_RADICADO_RESPUESTA")
    private Long numeroRadicadoRespuesta;
    @Column(name = "FEC_RADICADO_RESPUESTA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoRespuesta;
    @Column(name = "ES_VALIDA_RESPUESTA")
    private Boolean esValidaRespuesta;
    @Size(max = 255)
    @Column(name = "VAL_OBSERVACIONES_RESPUESTA")
    private String valObservacionesRespuesta;
    @Column(name = "VAL_NUMERO_REITERACION")
    private Long valNumeroReiteracion;
    @JoinColumn(name = "COD_TIPO_ESTADO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoEstado;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private EntidadExterna entidadExterna;

    public SolicitudEntidadExterna() {
    }

    public SolicitudEntidadExterna(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public Long getNumeroRadicadoRespuesta() {
        return numeroRadicadoRespuesta;
    }

    public void setNumeroRadicadoRespuesta(Long numeroRadicadoRespuesta) {
        this.numeroRadicadoRespuesta = numeroRadicadoRespuesta;
    }

    public Calendar getFecRadicadoRespuesta() {
        return fecRadicadoRespuesta;
    }

    public void setFecRadicadoRespuesta(Calendar fecRadicadoRespuesta) {
        this.fecRadicadoRespuesta = fecRadicadoRespuesta;
    }

    public Boolean getEsValidaRespuesta() {
        return esValidaRespuesta;
    }

    public void setEsValidaRespuesta(Boolean esValidaRespuesta) {
        this.esValidaRespuesta = esValidaRespuesta;
    }

    public String getValObservacionesRespuesta() {
        return valObservacionesRespuesta;
    }

    public void setValObservacionesRespuesta(String valObservacionesRespuesta) {
        this.valObservacionesRespuesta = valObservacionesRespuesta;
    }

    public Long getValNumeroReiteracion() {
        return valNumeroReiteracion;
    }

    public void setValNumeroReiteracion(Long valNumeroReiteracion) {
        this.valNumeroReiteracion = valNumeroReiteracion;
    }

    public ValorDominio getCodTipoEstado() {
        return codTipoEstado;
    }

    public void setCodTipoEstado(ValorDominio codTipoEstado) {
        this.codTipoEstado = codTipoEstado;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudEntidadExterna)) {
            return false;
        }
        SolicitudEntidadExterna other = (SolicitudEntidadExterna) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.SolicitudEntidadExterna[ id=" + id + " ]";
    }
    
}
