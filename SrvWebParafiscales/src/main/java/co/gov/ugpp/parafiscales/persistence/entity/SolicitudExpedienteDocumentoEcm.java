package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "SOLICITUD_EXP_DOCUMENTO_ECM")
@NamedQueries({
    @NamedQuery(name = "solicitudExpedienteDocumento.findByExpedienteSolicitudAndDocumento",
            query = "SELECT e FROM SolicitudExpedienteDocumentoEcm e WHERE e.expedienteDocumentoEcm.documentoEcm.id = :idDocumento AND e.expedienteDocumentoEcm.idExpediente = :idExpediente AND e.solicitud.id = :idSolicitud"),
    @NamedQuery(name = "solicitudExpedienteDocumento.findByCodOrigen",
            query = "SELECT e FROM SolicitudExpedienteDocumentoEcm e WHERE e.expedienteDocumentoEcm.codOrigen.id = :idCodOrigen AND e.solicitud.id = :idSolicitud ORDER BY e.fechaCreacion DESC")})
public class SolicitudExpedienteDocumentoEcm extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solExpedienteDocEcmIdSeq", sequenceName = "sol_exp_documento_ecm_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solExpedienteDocEcmIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_EXP_DOCUMENTO_ECM", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private ExpedienteDocumentoEcm expedienteDocumentoEcm;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Solicitud solicitud;

    public SolicitudExpedienteDocumentoEcm() {
    }

    public SolicitudExpedienteDocumentoEcm(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudExpedienteDocumentoEcm)) {
            return false;
        }
        SolicitudExpedienteDocumentoEcm other = (SolicitudExpedienteDocumentoEcm) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ExpedienteDocumentoEcm[ id=" + id + " ]";
    }

    public ExpedienteDocumentoEcm getExpedienteDocumentoEcm() {
        return expedienteDocumentoEcm;
    }

    public void setExpedienteDocumentoEcm(ExpedienteDocumentoEcm expedienteDocumentoEcm) {
        this.expedienteDocumentoEcm = expedienteDocumentoEcm;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

}
