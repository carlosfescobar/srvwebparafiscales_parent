package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "SOLICITUD_FUENTES_INFO")
@NamedQueries({
    @NamedQuery(name = "solicitudFuentesInfo.findBySolicitudAndFuenteInfo",
            query = "SELECT sfi FROM SolicitudFuenteInformacion sfi WHERE sfi.codFuenteInformacion.id = :idFuenteInformacion AND sfi.solicitud.id = :idSolicitud")
})
public class SolicitudFuenteInformacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudCodFuenteId", sequenceName = "sol_cod_fuente_info_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudCodFuenteId")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Solicitud solicitud;
    @JoinColumn(name = "COD_FUENTE_INFORMACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.EAGER)
    private ValorDominio codFuenteInformacion;

    public SolicitudFuenteInformacion() {
    }

    public SolicitudFuenteInformacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodFuenteInformacion() {
        return codFuenteInformacion;
    }

    public void setCodFuenteInformacion(ValorDominio codFuenteInformacion) {
        this.codFuenteInformacion = codFuenteInformacion;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudFuenteInformacion)) {
            return false;
        }
        SolicitudFuenteInformacion other = (SolicitudFuenteInformacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Pais[ id=" + id + " ]";
    }

}
