package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "SOLICITUD_PAGO")
public class SolicitudPago extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudPagoIdSeq", sequenceName = "solicitud_pago_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudPagoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "VAL_VALOR")
    private Long valValor;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Solicitud solicitud;
    @Column(name = "FEC_PERIODO_PAGO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecPeriodoPago;

    public SolicitudPago() {
    }

    public SolicitudPago(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Calendar getFecPeriodoPago() {
        return fecPeriodoPago;
    }

    public void setFecPeriodoPago(Calendar fecPeriodoPago) {
        this.fecPeriodoPago = fecPeriodoPago;
    }
    
    public Long getValValor() {
        return valValor;
    }

    public void setValValor(Long valValor) {
        this.valValor = valValor;
    }

    public Solicitud getIdSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudPago)) {
            return false;
        }
        SolicitudPago other = (SolicitudPago) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.SolicitudPago[ id=" + id + " ]";
    }

}
