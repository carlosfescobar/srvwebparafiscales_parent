package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "SOLICITUD_PLANILLA")
public class SolicitudPlanilla extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "solicitudPlanillaIdSeq", sequenceName = "solicitud_planilla_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "solicitudPlanillaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_SOLICITUD", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Solicitud solicitud;
    @JoinColumn(name = "ID_PLANILLA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Planilla planilla;
    @Column(name = "FEC_PERIODO_PAGO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecPeriodoPago;

    public SolicitudPlanilla() {
    }

    public SolicitudPlanilla(Long id) {
        this.id = id;
    }

    public SolicitudPlanilla(Long id, short valMes, Long valAnio) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    public Planilla getPlanilla() {
        return planilla;
    }

    public void setPlanilla(Planilla planilla) {
        this.planilla = planilla;
    }

    public Calendar getFecPeriodoPago() {
        return fecPeriodoPago;
    }

    public void setFecPeriodoPago(Calendar fecPeriodoPago) {
        this.fecPeriodoPago = fecPeriodoPago;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudPlanilla)) {
            return false;
        }
        SolicitudPlanilla other = (SolicitudPlanilla) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.SolicitudPlanilla[ id=" + id + " ]";
    }

}
