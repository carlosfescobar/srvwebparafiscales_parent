package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "SUBSISTEMA")
public class Subsistema extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private String id;
    @JoinColumn(name = "ID_ENTIDAD_VIGILANCIA_CONTROL", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Persona entidadVigilanciaControl;
    @JoinColumn(name = "ID_SUBSISTEMA", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<SubsistemaAdministradora> administradoras;

    public Subsistema() {
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Persona getEntidadVigilanciaControl() {
        return entidadVigilanciaControl;
    }

    public void setEntidadVigilanciaControl(Persona entidadVigilanciaControl) {
        this.entidadVigilanciaControl = entidadVigilanciaControl;
    }

    public List<SubsistemaAdministradora> getAdministradoras() {
        if (administradoras == null) {
            administradoras = new ArrayList<SubsistemaAdministradora>();
        }
        return administradoras;
    }
}
