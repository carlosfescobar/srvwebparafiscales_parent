package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@Table(name = "SUBSISTEMA_ADMINISTRADORA")
@NamedQueries({
    @NamedQuery(name = "subsistemaAdministradora.findBySubsistemaAndEntidadexterna",
            query = "SELECT sa FROM SubsistemaAdministradora sa WHERE sa.subsistema.id = :idSubsistema AND sa.entidadExterna.id = :idEntidadExterna")
})
public class SubsistemaAdministradora extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "subsistemaAdminIdSeq", sequenceName = "subsistema_administra_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subsistemaAdminIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EntidadExterna entidadExterna;
    @JoinColumn(name = "ID_SUBSISTEMA", referencedColumnName = "ID")
   @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Subsistema subsistema;

    public SubsistemaAdministradora() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public Subsistema getSubsistema() {
        return subsistema;
    }

    public void setSubsistema(Subsistema subsistema) {
        this.subsistema = subsistema;
    }

}
