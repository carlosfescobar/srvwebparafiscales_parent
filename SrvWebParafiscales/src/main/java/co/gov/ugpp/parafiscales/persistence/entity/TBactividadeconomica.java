package co.gov.ugpp.parafiscales.persistence.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * 
 * @author franzjr
 */
@Entity
@Table(name = "T_BACTIVIDADECONOMICA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TBactividadeconomica.findAll", query = "SELECT t FROM TBactividadeconomica t"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceVchSeccion", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceVchSeccion = :tBaceVchSeccion"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceVchDesseccion", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceVchDesseccion = :tBaceVchDesseccion"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceIntDivision", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceIntDivision = :tBaceIntDivision"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceVchDesdivision", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceVchDesdivision = :tBaceVchDesdivision"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceIntGrupo", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceIntGrupo = :tBaceIntGrupo"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceVchDesgrupo", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceVchDesgrupo = :tBaceVchDesgrupo"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceIntClase", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceIntClase = :tBaceIntClase"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceVchDesclase", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceVchDesclase = :tBaceVchDesclase"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceIntNumrevision", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceIntNumrevision = :tBaceIntNumrevision"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceDateFechainicial", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceDateFechainicial = :tBaceDateFechainicial"),
    @NamedQuery(name = "TBactividadeconomica.findByTBaceDateFechafinal", query = "SELECT t FROM TBactividadeconomica t WHERE t.tBaceDateFechafinal = :tBaceDateFechafinal")})
public class TBactividadeconomica implements Serializable {
    @OneToMany(mappedBy = "actividadEconomica", fetch = FetchType.LAZY)
    private List<AportanteLIQ> aportanteList;
    private static final long serialVersionUID = 1L;
    @Size(max = 2)
    @Column(name = "T_BACE_VCH_SECCION")
    private String tBaceVchSeccion;
    @Size(max = 250)
    @Column(name = "T_BACE_VCH_DESSECCION")
    private String tBaceVchDesseccion;
    @Column(name = "T_BACE_INT_DIVISION")
    private BigInteger tBaceIntDivision;
    @Size(max = 250)
    @Column(name = "T_BACE_VCH_DESDIVISION")
    private String tBaceVchDesdivision;
    @Column(name = "T_BACE_INT_GRUPO")
    private BigInteger tBaceIntGrupo;
    @Size(max = 250)
    @Column(name = "T_BACE_VCH_DESGRUPO")
    private String tBaceVchDesgrupo;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "T_BACE_INT_CLASE")
    private BigDecimal tBaceIntClase;
    @Size(max = 250)
    @Column(name = "T_BACE_VCH_DESCLASE")
    private String tBaceVchDesclase;
    @Column(name = "T_BACE_INT_NUMREVISION")
    private BigInteger tBaceIntNumrevision;
    @Column(name = "T_BACE_DATE_FECHAINICIAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tBaceDateFechainicial;
    @Column(name = "T_BACE_DATE_FECHAFINAL")
    @Temporal(TemporalType.TIMESTAMP)
    private Date tBaceDateFechafinal;
    @OneToMany(mappedBy = "tBactividadeconomica", fetch = FetchType.LAZY)
    private List<PorcentajeActividadEconomica> porcentajeActividadEconomicaList;

    public TBactividadeconomica() {
    }

    public TBactividadeconomica(BigDecimal tBaceIntClase) {
        this.tBaceIntClase = tBaceIntClase;
    }

    public String getTBaceVchSeccion() {
        return tBaceVchSeccion;
    }

    public void setTBaceVchSeccion(String tBaceVchSeccion) {
        this.tBaceVchSeccion = tBaceVchSeccion;
    }

    public String getTBaceVchDesseccion() {
        return tBaceVchDesseccion;
    }

    public void setTBaceVchDesseccion(String tBaceVchDesseccion) {
        this.tBaceVchDesseccion = tBaceVchDesseccion;
    }

    public BigInteger getTBaceIntDivision() {
        return tBaceIntDivision;
    }

    public void setTBaceIntDivision(BigInteger tBaceIntDivision) {
        this.tBaceIntDivision = tBaceIntDivision;
    }

    public String getTBaceVchDesdivision() {
        return tBaceVchDesdivision;
    }

    public void setTBaceVchDesdivision(String tBaceVchDesdivision) {
        this.tBaceVchDesdivision = tBaceVchDesdivision;
    }

    public BigInteger getTBaceIntGrupo() {
        return tBaceIntGrupo;
    }

    public void setTBaceIntGrupo(BigInteger tBaceIntGrupo) {
        this.tBaceIntGrupo = tBaceIntGrupo;
    }

    public String getTBaceVchDesgrupo() {
        return tBaceVchDesgrupo;
    }

    public void setTBaceVchDesgrupo(String tBaceVchDesgrupo) {
        this.tBaceVchDesgrupo = tBaceVchDesgrupo;
    }

    public BigDecimal getTBaceIntClase() {
        return tBaceIntClase;
    }

    public void setTBaceIntClase(BigDecimal tBaceIntClase) {
        this.tBaceIntClase = tBaceIntClase;
    }

    public String getTBaceVchDesclase() {
        return tBaceVchDesclase;
    }

    public void setTBaceVchDesclase(String tBaceVchDesclase) {
        this.tBaceVchDesclase = tBaceVchDesclase;
    }

    public BigInteger getTBaceIntNumrevision() {
        return tBaceIntNumrevision;
    }

    public void setTBaceIntNumrevision(BigInteger tBaceIntNumrevision) {
        this.tBaceIntNumrevision = tBaceIntNumrevision;
    }

    public Date getTBaceDateFechainicial() {
        return tBaceDateFechainicial;
    }

    public void setTBaceDateFechainicial(Date tBaceDateFechainicial) {
        this.tBaceDateFechainicial = tBaceDateFechainicial;
    }

    public Date getTBaceDateFechafinal() {
        return tBaceDateFechafinal;
    }

    public void setTBaceDateFechafinal(Date tBaceDateFechafinal) {
        this.tBaceDateFechafinal = tBaceDateFechafinal;
    }

    @XmlTransient
    public List<PorcentajeActividadEconomica> getPorcentajeActividadEconomicaList() {
        return porcentajeActividadEconomicaList;
    }

    public void setPorcentajeActividadEconomicaList(List<PorcentajeActividadEconomica> porcentajeActividadEconomicaList) {
        this.porcentajeActividadEconomicaList = porcentajeActividadEconomicaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tBaceIntClase != null ? tBaceIntClase.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TBactividadeconomica)) {
            return false;
        }
        TBactividadeconomica other = (TBactividadeconomica) object;
        if ((this.tBaceIntClase == null && other.tBaceIntClase != null) || (this.tBaceIntClase != null && !this.tBaceIntClase.equals(other.tBaceIntClase))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.intgr.TBactividadeconomica[ tBaceIntClase=" + tBaceIntClase + " ]";
    }

    @XmlTransient
    public List<AportanteLIQ> getAportanteList() {
        return aportanteList;
    }

    public void setAportanteList(List<AportanteLIQ> aportanteList) {
        this.aportanteList = aportanteList;
    }
    
}
