package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 *
 * @author rpadilla
 */
@Entity
@Cacheable(true)
@Cache(type = CacheType.HARD_WEAK)
@Table(name = "TELEFONO")


@NamedQueries({
    @NamedQuery(name = "telefono.findTelefono",
        query = "SELECT COUNT(t) FROM Telefono t WHERE t.valNumero = :valNumero AND t.codTipoTelefono.id = :codTipoTelefono AND t.contactoPersona.id = :idContacto AND t.codFuente.id = :codFuente")
        
})


public class Telefono extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "telefonoIdSeq", sequenceName = "telefono_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "telefonoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Size(max = 30)
    @Column(name = "VAL_NUMERO")
    private String valNumero;
    @JoinColumn(name = "COD_TIPO_TELEFONO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoTelefono;
    @JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactoPersona contactoPersona;
    
    
    @JoinColumn(name = "COD_FUENTE", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codFuente;
    

    public Telefono() {
    }

    public Telefono(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getValNumero() {
        return valNumero;
    }

    public void setValNumero(String valNumero) {
        this.valNumero = valNumero;
    }

    public ValorDominio getCodTipoTelefono() {
        return codTipoTelefono;
    }

    public void setCodTipoTelefono(ValorDominio codTipoTelefono) {
        this.codTipoTelefono = codTipoTelefono;
    }

    public ContactoPersona getContactoPersona() {
        return contactoPersona;
    }

    public void setContactoPersona(ContactoPersona contactoPersona) {
        this.contactoPersona = contactoPersona;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Telefono)) {
            return false;
        }
        Telefono other = (Telefono) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    
        
    public ValorDominio getCodFuente() {
        return codFuente;
    }

    public void setCodFuente(ValorDominio codFuente) {
        this.codFuente = codFuente;
    }
    
    
    
    
    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Telefono[ id=" + id + " ]";
    }

}
