package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqtipoidentificacion", initialValue = 1, allocationSize = 1)
@Table(name = "LIQ_TIPO_IDENTIFICACION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = TipoIdentificacion.NQ_FIND_BY_DESCRIPCION, query = "SELECT t FROM TipoIdentificacion t WHERE t.descripcion = :descripcion"),
    @NamedQuery(name = TipoIdentificacion.NQ_FIND_BY_SIGLA, query = "SELECT t FROM TipoIdentificacion t WHERE t.sigla = :sigla")
})

public class TipoIdentificacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    public static final String NQ_FIND_BY_DESCRIPCION = "TipoIdentificacion.findByDescripcion";
    public static final String NQ_FIND_BY_SIGLA = "TipoIdentificacion.findBySigla";
    
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqtipoidentificacion")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Column(name = "sigla")
    private String sigla;

    @Size(max = 500)
    @Column(name = "descripcion", length = 500)
    private String descripcion;

    public TipoIdentificacion() {
    }

    public TipoIdentificacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
