package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franzjr
 */
@Entity
@Table(name = "TRASLADO")
public class Traslado extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "trasladoIdSeq", sequenceName = "traslado_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trasladoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ID_NUMERO_EXPEDIENTE")
    private String idNumeroExpediente;

    @Column(name = "FUNCIONARIO_ID")
    private String funcionarioId;

    @Column(name = "FEC_TRASLADO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecTraslado;

    @Size(max = 255)
    @Column(name = "DESC_MOTIVO_TRASLADO")
    private String descMotivoTraslado;

    @Size(max = 255)
    @Column(name = "ESTADO_TRASLADO")
    private String estadoTraslado;

    @JoinColumn(name = "ID_ENTIDAD_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private EntidadExterna entidadExterna;

    @JoinColumn(name = "ID_DOCUMENTO_TRAS_DENUNCIANTE", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DocumentoEcm documentoTrasladoDenunciante;

    @JoinColumn(name = "ID_DOCUMENTO_TRAS_ENT_EXTERNA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private DocumentoEcm documentoTrasladoEntidadExterna;

    @JoinColumn(name = "ID_DENUNCIA", referencedColumnName = "ID_DENUNCIA")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Denuncia denuncia;

    @Column(name = "FEC_RADIC_DOC_ENTIDAD_EXTERNA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaRadicadoDocumentoEntidadExterna;

    @Size(max = 50)
    @Column(name = "ID_RADIC_DOC_ENTIDAD_EXTERNA")
    private String idRadicadoDocumentoEntidadExterna;

    public Traslado() {
    }

    public Traslado(Long id) {
        this.id = id;
    }

    public Traslado(Long id, String idNumeroExpediente) {
        this.id = id;
        this.idNumeroExpediente = idNumeroExpediente;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdNumeroExpediente() {
        return idNumeroExpediente;
    }

    public void setIdNumeroExpediente(String idNumeroExpediente) {
        this.idNumeroExpediente = idNumeroExpediente;
    }

    public String getFuncionarioId() {
        return funcionarioId;
    }

    public void setFuncionarioId(String funcionarioId) {
        this.funcionarioId = funcionarioId;
    }

    public Calendar getFecTraslado() {
        return fecTraslado;
    }

    public void setFecTraslado(Calendar fecTraslado) {
        this.fecTraslado = fecTraslado;
    }

    public String getDescMotivoTraslado() {
        return descMotivoTraslado;
    }

    public void setDescMotivoTraslado(String descMotivoTraslado) {
        this.descMotivoTraslado = descMotivoTraslado;
    }

    public String getEstadoTraslado() {
        return estadoTraslado;
    }

    public void setEstadoTraslado(String estadoTraslado) {
        this.estadoTraslado = estadoTraslado;
    }

    public EntidadExterna getEntidadExterna() {
        return entidadExterna;
    }

    public void setEntidadExterna(EntidadExterna entidadExterna) {
        this.entidadExterna = entidadExterna;
    }

    public DocumentoEcm getDocumentoTrasladoDenunciante() {
        return documentoTrasladoDenunciante;
    }

    public void setDocumentoTrasladoDenunciante(DocumentoEcm documentoTrasladoDenunciante) {
        this.documentoTrasladoDenunciante = documentoTrasladoDenunciante;
    }

    public DocumentoEcm getDocumentoTrasladoEntidadExterna() {
        return documentoTrasladoEntidadExterna;
    }

    public void setDocumentoTrasladoEntidadExterna(DocumentoEcm documentoTrasladoEntidadExterna) {
        this.documentoTrasladoEntidadExterna = documentoTrasladoEntidadExterna;
    }

    public Denuncia getDenuncia() {
        return denuncia;
    }

    public void setDenuncia(Denuncia denuncia) {
        this.denuncia = denuncia;
    }

    public Calendar getFechaRadicadoDocumentoEntidadExterna() {
        return fechaRadicadoDocumentoEntidadExterna;
    }

    public void setFechaRadicadoDocumentoEntidadExterna(Calendar fechaRadicadoDocumentoEntidadExterna) {
        this.fechaRadicadoDocumentoEntidadExterna = fechaRadicadoDocumentoEntidadExterna;
    }

    public String getIdRadicadoDocumentoEntidadExterna() {
        return idRadicadoDocumentoEntidadExterna;
    }

    public void setIdRadicadoDocumentoEntidadExterna(String idRadicadoDocumentoEntidadExterna) {
        this.idRadicadoDocumentoEntidadExterna = idRadicadoDocumentoEntidadExterna;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Traslado)) {
            return false;
        }
        Traslado other = (Traslado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.Traslado[ id=" + id + " ]";
    }

}
