package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "TRAZABILIDAD_ACTO_ADMIN")
public class TrazabilidadActoAdministrativo extends AbstractEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "ID", updatable = false)
    private String id;
    @OneToOne(optional = false, fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.REFRESH, CascadeType.DETACH})
    @PrimaryKeyJoinColumn(name = "ID", referencedColumnName = "ID")
    private ActoAdministrativo actoAdministrativo;
    @Column(name = "ID_RADICADO_SALIDA_DEFINITIVO")
    private String idRadicadoSalidaDefinitivo;
    @Column(name = "FEC_RAD_SALIDA_DEFINITIVO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecRadicadoSalidaDefinitivo;
    @Column(name = "VAL_DIAS_PRORROGA")
    private String valDiasProrroga;
    @JoinColumn(name = "COD_TIPO_NOTIFICA_DEFINITIVA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoNotificacionDefinitiva;
    @Column(name = "FEC_NOTIFICACION_DEFINITIVA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecNotificacionDefinitiva;
    @JoinColumn(name = "ID_TRAZABILIDAD_ACTO_ADM", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<EventoActoAdministrativo> envioActoAdministrativoList;
    @JoinColumn(name = "ID_TRAZA_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @OneToMany(orphanRemoval = true, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<RadicadoTrazabilidad> radicacionesEntrada;
    @Column(name = "DESC_TIPO_NOTIF_DEFIN")
    private String descTipoNotificacionDefinitiva;
    @JoinColumn(name = "UBICACION_DEFINITIVA", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Ubicacion ubicacionDefinitiva;

    public String getIdRadicadoSalidaDefinitivo() {
        return idRadicadoSalidaDefinitivo;
    }

    public void setIdRadicadoSalidaDefinitivo(String idRadicadoSalidaDefinitivo) {
        this.idRadicadoSalidaDefinitivo = idRadicadoSalidaDefinitivo;
    }

    public Calendar getFecRadicadoSalidaDefinitivo() {
        return fecRadicadoSalidaDefinitivo;
    }

    public void setFecRadicadoSalidaDefinitivo(Calendar fecRadicadoSalidaDefinitivo) {
        this.fecRadicadoSalidaDefinitivo = fecRadicadoSalidaDefinitivo;
    }

    public String getValDiasProrroga() {
        return valDiasProrroga;
    }

    public void setValDiasProrroga(String valDiasProrroga) {
        this.valDiasProrroga = valDiasProrroga;
    }

    public ValorDominio getCodTipoNotificacionDefinitiva() {
        return codTipoNotificacionDefinitiva;
    }

    public void setCodTipoNotificacionDefinitiva(ValorDominio codTipoNotificacionDefinitiva) {
        this.codTipoNotificacionDefinitiva = codTipoNotificacionDefinitiva;
    }

    public Calendar getFecNotificacionDefinitiva() {
        return fecNotificacionDefinitiva;
    }

    public void setFecNotificacionDefinitiva(Calendar fecNotificacionDefinitiva) {
        this.fecNotificacionDefinitiva = fecNotificacionDefinitiva;
    }

    public List<EventoActoAdministrativo> getEnvioActoAdministrativoList() {
        if (envioActoAdministrativoList == null) {
            envioActoAdministrativoList = new ArrayList<EventoActoAdministrativo>();
        }
        return envioActoAdministrativoList;
    }

    public List<RadicadoTrazabilidad> getRadicacionesEntrada() {
        if (radicacionesEntrada == null) {
            radicacionesEntrada = new ArrayList<RadicadoTrazabilidad>();
        }
        return radicacionesEntrada;
    }

    public String getDescTipoNotificacionDefinitiva() {
        return descTipoNotificacionDefinitiva;
    }

    public void setDescTipoNotificacionDefinitiva(String descTipoNotificacionDefinitiva) {
        this.descTipoNotificacionDefinitiva = descTipoNotificacionDefinitiva;
    }

    public Ubicacion getUbicacionDefinitiva() {
        return ubicacionDefinitiva;
    }

    public void setUbicacionDefinitiva(Ubicacion ubicacionDefinitiva) {
        this.ubicacionDefinitiva = ubicacionDefinitiva;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ActoAdministrativo getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(ActoAdministrativo actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TrazabilidadActoAdministrativo)) {
            return false;
        }
        TrazabilidadActoAdministrativo other = (TrazabilidadActoAdministrativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.persistence.entity.AccionActoAdministrativo[ id=" + id + " ]";
    }

}
