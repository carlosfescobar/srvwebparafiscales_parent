package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "trazabilidadRadicadoSalida.findByRadicadoSalida",
            query = "SELECT trs FROM TrazabilidadRadicadoSalida trs WHERE trs.numRadicadoSalida = :numRadicadoSalida")})
@Table(name = "TRAZABILIDAD_RADICADO_SALIDA")
public class TrazabilidadRadicadoSalida extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "trazaradicadosalidaIdSeq", sequenceName = "traza_radicado_salida_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trazaradicadosalidaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_RADICADO_SALIDA")
    private String numRadicadoSalida;
    @Column(name = "ID_GUIA")
    private String idGuia;
    @Column(name = "DESC_CAUSAL_DEVOLUCION")
    private String descCausalDevolucion;
    @JoinColumn(name = "COD_ESTADO_ENTREGA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoEntrega;
    @JoinColumn(name = "COD_ESTADO_ENVIO", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoEnvio;
    @Column(name = "FEC_ENTREGA_DEVOLUCION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecEntregaDevolucion;
    @Column(name = "FEC_ENVIO")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fecEnvio;

    public TrazabilidadRadicadoSalida() {

    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumRadicadoSalida() {
        return numRadicadoSalida;
    }

    public void setNumRadicadoSalida(String numRadicadoSalida) {
        this.numRadicadoSalida = numRadicadoSalida;
    }

    public String getIdGuia() {
        return idGuia;
    }

    public void setIdGuia(String idGuia) {
        this.idGuia = idGuia;
    }

    public String getDescCausalDevolucion() {
        return descCausalDevolucion;
    }

    public void setDescCausalDevolucion(String descCausalDevolucion) {
        this.descCausalDevolucion = descCausalDevolucion;
    }

    public ValorDominio getCodEstadoEntrega() {
        return codEstadoEntrega;
    }

    public void setCodEstadoEntrega(ValorDominio codEstadoEntrega) {
        this.codEstadoEntrega = codEstadoEntrega;
    }

    public ValorDominio getCodEstadoEnvio() {
        return codEstadoEnvio;
    }

    public void setCodEstadoEnvio(ValorDominio codEstadoEnvio) {
        this.codEstadoEnvio = codEstadoEnvio;
    }

    public Calendar getFecEntregaDevolucion() {
        return fecEntregaDevolucion;
    }

    public void setFecEntregaDevolucion(Calendar fecEntregaDevolucion) {
        this.fecEntregaDevolucion = fecEntregaDevolucion;
    }

    public Calendar getFecEnvio() {
        return fecEnvio;
    }

    public void setFecEnvio(Calendar fecEnvio) {
        this.fecEnvio = fecEnvio;
    }

}
