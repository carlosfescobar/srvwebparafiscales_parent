package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "TRAZA_TECNICA_EJECUCION")
public class TrazabilidadTecnicaEjecucion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "trazabilidadTecnicaEjecucionIdSeq", sequenceName = "traza_tecnic_ejecucion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trazabilidadTecnicaEjecucionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_ESTADO_EJECUCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private ValorDominio codEstadoEjecucion;
    @Column(name = "FEC_INICIO_EJECUCION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaInicioEjecucion;
    @Column(name = "FEC_FIN_EJECUCION")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaFinEjecucion;
    @Column(name = "CANTIDAD_INTENTOS")
    private Long camtidadIntentos;

    public TrazabilidadTecnicaEjecucion() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodEstadoEjecucion() {
        return codEstadoEjecucion;
    }

    public void setCodEstadoEjecucion(ValorDominio codEstadoEjecucion) {
        this.codEstadoEjecucion = codEstadoEjecucion;
    }

    public Calendar getFechaInicioEjecucion() {
        return fechaInicioEjecucion;
    }

    public void setFechaInicioEjecucion(Calendar fechaInicioEjecucion) {
        this.fechaInicioEjecucion = fechaInicioEjecucion;
    }

    public Calendar getFechaFinEjecucion() {
        return fechaFinEjecucion;
    }

    public void setFechaFinEjecucion(Calendar fechaFinEjecucion) {
        this.fechaFinEjecucion = fechaFinEjecucion;
    }

    public Long getCamtidadIntentos() {
        return camtidadIntentos;
    }

    public void setCamtidadIntentos(Long camtidadIntentos) {
        this.camtidadIntentos = camtidadIntentos;
    }

}
