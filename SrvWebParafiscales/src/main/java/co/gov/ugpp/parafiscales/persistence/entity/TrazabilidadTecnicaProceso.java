package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodriguez
 */
@Entity
@Table(name = "TRAZABILIDAD_TECNICA_PROCESO")
public class TrazabilidadTecnicaProceso extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "trazabilidadTecnicaIdSeq", sequenceName = "trazabilidadtecnica_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trazabilidadTecnicaIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "ID_TRAZABILIDAD_TECNICA_PROCES")
    private String idTrazabilidadTecnicaProceso;
    @Column(name = "COD_ESTADO_PROCESO")
    private String cosEstadoProceso;
    @Column(name = "DESC_ESTADO_PROCESO")
    private String descEstadoProceso;
    @Column(name = "COD_ESTADO_ACTIVIDAD")
    private String cdEstadoActividad;
    @Column(name = "DESC_ESTADO_ACTIVIDAD")
    private String descEstadoActividad;
    @Column(name = "ID_INSTANCIA_ACTIVIDAD")
    private String idInstanciaActividad;
    @Column(name = "VAL_NOMBRE_ACTIVIDAD")
    private String valNombreActividad;
    @Column(name = "ID_DEFINICION_ACTIVIDAD")
    private String idDefinicionActividad;
    @Column(name = "ID_USUARIO")
    private String idUsuario;
    @Column(name = "COD_ROL")
    private String codRol;
    @Column(name = "VAL_NOMBRE_ROL")
    private String valNombreRol;
    @Column(name = "VAL_DIAS_HABILES_FIN_TAREA")
    private String valDiasHabilesFinTarea;
    @Column(name = "FEC_ESTADO_ACTIVIDAD")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaEstadoActividad;

    public TrazabilidadTecnicaProceso() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdTrazabilidadTecnicaProceso() {
        return idTrazabilidadTecnicaProceso;
    }

    public void setIdTrazabilidadTecnicaProceso(String idTrazabilidadTecnicaProceso) {
        this.idTrazabilidadTecnicaProceso = idTrazabilidadTecnicaProceso;
    }

    public String getCosEstadoProceso() {
        return cosEstadoProceso;
    }

    public void setCosEstadoProceso(String cosEstadoProceso) {
        this.cosEstadoProceso = cosEstadoProceso;
    }

    public String getDescEstadoProceso() {
        return descEstadoProceso;
    }

    public void setDescEstadoProceso(String descEstadoProceso) {
        this.descEstadoProceso = descEstadoProceso;
    }

    public String getCdEstadoActividad() {
        return cdEstadoActividad;
    }

    public void setCdEstadoActividad(String cdEstadoActividad) {
        this.cdEstadoActividad = cdEstadoActividad;
    }

    public String getDescEstadoActividad() {
        return descEstadoActividad;
    }

    public void setDescEstadoActividad(String descEstadoActividad) {
        this.descEstadoActividad = descEstadoActividad;
    }

    public String getIdInstanciaActividad() {
        return idInstanciaActividad;
    }

    public void setIdInstanciaActividad(String idInstanciaActividad) {
        this.idInstanciaActividad = idInstanciaActividad;
    }

    public String getValNombreActividad() {
        return valNombreActividad;
    }

    public void setValNombreActividad(String valNombreActividad) {
        this.valNombreActividad = valNombreActividad;
    }

    public String getIdDefinicionActividad() {
        return idDefinicionActividad;
    }

    public void setIdDefinicionActividad(String idDefinicionActividad) {
        this.idDefinicionActividad = idDefinicionActividad;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getCodRol() {
        return codRol;
    }

    public void setCodRol(String codRol) {
        this.codRol = codRol;
    }

    public String getValNombreRol() {
        return valNombreRol;
    }

    public void setValNombreRol(String valNombreRol) {
        this.valNombreRol = valNombreRol;
    }

    public String getValDiasHabilesFinTarea() {
        return valDiasHabilesFinTarea;
    }

    public void setValDiasHabilesFinTarea(String valDiasHabilesFinTarea) {
        this.valDiasHabilesFinTarea = valDiasHabilesFinTarea;
    }

    public Calendar getFechaEstadoActividad() {
        return fechaEstadoActividad;
    }

    public void setFechaEstadoActividad(Calendar fechaEstadoActividad) {
        this.fechaEstadoActividad = fechaEstadoActividad;
    }

}
