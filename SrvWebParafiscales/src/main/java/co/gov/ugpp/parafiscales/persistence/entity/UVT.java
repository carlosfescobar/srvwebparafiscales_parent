package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author zrodrigu
 */
@Entity

@NamedQuery(name = "UVT.findByfecActual",
        query = "SELECT f FROM UVT f WHERE :sysdate BETWEEN f.fecInicio and f.fecFin ORDER BY f.fechaCreacion DESC")
@Table(name = "UVT")
public class UVT extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "UVTIdSeq", sequenceName = "UVT_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UVTIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "VAL_UVT_NUMEROS")
    private String valUVTNumeros;
    @Column(name = "VAL_UVT_LETRAS")
    private String valUVTLetras;
    @Column(name = "VAL_CINCO_LETRAS_UVT_NUM")
    private String valCincoLetrasUVTNum;
    @Column(name = "VAL_CINCO_LETRAS_UVT_LETRAS")
    private String valCincoLetrasUVTLetras;
    @Column(name = "VAL_NUMERO_RESOLUCION")
    private String valNumeroResolucion;
    @Column(name = "FEC_RESOLUCION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecResolucion;
    @Column(name = "FEC_INICIO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicio;
    @Column(name = "FEC_FIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFin;

    public UVT() {
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValUVTNumeros() {
        return valUVTNumeros;
    }

    public void setValUVTNumeros(String valUVTNumeros) {
        this.valUVTNumeros = valUVTNumeros;
    }

    public String getValUVTLetras() {
        return valUVTLetras;
    }

    public void setValUVTLetras(String valUVTLetras) {
        this.valUVTLetras = valUVTLetras;
    }

    public String getValCincoLetrasUVTNum() {
        return valCincoLetrasUVTNum;
    }

    public void setValCincoLetrasUVTNum(String valCincoLetrasUVTNum) {
        this.valCincoLetrasUVTNum = valCincoLetrasUVTNum;
    }

    public String getValCincoLetrasUVTLetras() {
        return valCincoLetrasUVTLetras;
    }

    public void setValCincoLetrasUVTLetras(String valCincoLetrasUVTLetras) {
        this.valCincoLetrasUVTLetras = valCincoLetrasUVTLetras;
    }

    public String getValNumeroResolucion() {
        return valNumeroResolucion;
    }

    public void setValNumeroResolucion(String valNumeroResolucion) {
        this.valNumeroResolucion = valNumeroResolucion;
    }

    public Calendar getFecResolucion() {
        return fecResolucion;
    }

    public void setFecResolucion(Calendar fecResolucion) {
        this.fecResolucion = fecResolucion;
    }

    public Calendar getFecInicio() {
        return fecInicio;
    }

    public void setFecInicio(Calendar fecInicio) {
        this.fecInicio = fecInicio;
    }

    public Calendar getFecFin() {
        return fecFin;
    }

    public void setFecFin(Calendar fecFin) {
        this.fecFin = fecFin;
    }

}
