package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.eclipse.persistence.annotations.Cache;
import org.eclipse.persistence.annotations.CacheType;

/**
 *
 * @author rpadilla
 */
@Entity
@Cacheable(true)
@Cache(type = CacheType.HARD_WEAK)
@Table(name = "UBICACION")


@NamedQueries({
    @NamedQuery(name = "ubicacion.findUbicacion",
            query = "SELECT COUNT(u) FROM Ubicacion u WHERE u.codTipoDireccion.id = :codTipoDireccion AND u.valDireccion LIKE :valDireccion AND u.municipio.codigo = :idMunicipio AND u.contactoPersona.id = :idContacto")
})


public class Ubicacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 8798345872637600541L;

    @Id
    @SequenceGenerator(name = "ubicacionIdSeq", sequenceName = "ubicacion_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ubicacionIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 200)
    @Column(name = "VAL_DIRECCION")
    private String valDireccion;
    @JoinColumn(name = "COD_TIPO_DIRECCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codTipoDireccion;
    @JoinColumn(name = "ID_MUNICIPIO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Municipio municipio;
    @JoinColumn(name = "ID_CONTACTO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ContactoPersona contactoPersona;

    public ContactoPersona getContactoPersona() {
        return contactoPersona;
    }

    public void setContactoPersona(ContactoPersona contactoPersona) {
        this.contactoPersona = contactoPersona;
    }

    public Ubicacion() {
    }

    public Ubicacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getValDireccion() {
        return valDireccion;
    }

    public void setValDireccion(String valDireccion) {
        this.valDireccion = valDireccion;
    }

    public ValorDominio getCodTipoDireccion() {
        return codTipoDireccion;
    }

    public void setCodTipoDireccion(ValorDominio codTipoDireccion) {
        this.codTipoDireccion = codTipoDireccion;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ubicacion)) {
            return false;
        }
        Ubicacion other = (Ubicacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Ubicacion[ id=" + id + " ]";
    }

}
