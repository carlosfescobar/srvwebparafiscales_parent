package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rpadilla
 */
@Entity
@Table(name = "VALIDACION")
public class Validacion extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ID_VALIDACION_INFORMACION_EXT")
    private Long idValidacionInformacionExt;
    @Size(max = 100)
    @Column(name = "VAL_USUARIO")
    private String valUsuario;

    public Validacion() {
    }

    public Validacion(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Long getIdValidacionInformacionExt() {
        return idValidacionInformacionExt;
    }

    protected void setIdValidacionInformacionExt(Long idValidacionInformacionExt) {
        this.idValidacionInformacionExt = idValidacionInformacionExt;
    }

    public String getValUsuario() {
        return valUsuario;
    }

    public void setValUsuario(String valUsuario) {
        this.valUsuario = valUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Validacion)) {
            return false;
        }
        Validacion other = (Validacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.Validacion[ id=" + id + " ]";
    }

}
