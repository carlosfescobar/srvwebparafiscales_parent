package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "VALIDACION_ACTO_ADMINISTRATIVO")
public class ValidacionActoAdministrativo extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "validacionActoAdmIdSeq", sequenceName = "validacion_acto_adm_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validacionActoAdmIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @JoinColumn(name = "COD_ETAPA_VALIDACION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEtapaValidacion;
    @JoinColumn(name = "COD_ESTADO_ACEPTACION_ETAPA", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codEstadoAceptacionEtapa;
    @Size(max = 50)
    @Column(name = "DES_OBSERVACION")
    private String desObservacion;
    @JoinColumn(name = "ID_ACTO_ADMINISTRATIVO", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private ActoAdministrativo actoAdministrativo;

    public ValidacionActoAdministrativo() {
    }

    public ValidacionActoAdministrativo(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ValorDominio getCodEtapaValidacion() {
        return codEtapaValidacion;
    }

    public void setCodEtapaValidacion(ValorDominio codEtapaValidacion) {
        this.codEtapaValidacion = codEtapaValidacion;
    }

    public ValorDominio getCodEstadoAceptacionEtapa() {
        return codEstadoAceptacionEtapa;
    }

    public void setCodEstadoAceptacionEtapa(ValorDominio codEstadoAceptacionEtapa) {
        this.codEstadoAceptacionEtapa = codEstadoAceptacionEtapa;
    }

    public String getDesObservacion() {
        return desObservacion;
    }

    public void setDesObservacion(String desObservacion) {
        this.desObservacion = desObservacion;
    }

    public ActoAdministrativo getActoAdministrativo() {
        return actoAdministrativo;
    }

    public void setActoAdministrativo(ActoAdministrativo actoAdministrativo) {
        this.actoAdministrativo = actoAdministrativo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidacionActoAdministrativo)) {
            return false;
        }
        ValidacionActoAdministrativo other = (ValidacionActoAdministrativo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValidacionActoAdministrativo[ id=" + id + " ]";
    }

}
