package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author jmuncab
 */
@Entity
@Table(name = "VALIDACION_CREACION_DOCUMENTO")
public class ValidacionCreacionDocumento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "validacionCreaDocuIdSeq", sequenceName = "validacion_creaci_docum_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validacionCreaDocuIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "ES_APROBADO")
    private Boolean esAprobado;
    @Column(name = "FEC_VALIDACION")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecvalidacion;
    @Column(name = "VAL_COMENTARIOS")
    private String valcomentarios;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getESAprobado() {
        return esAprobado;
    }

    public void setESAprobado(Boolean esAprobado) {
        this.esAprobado = esAprobado;
    }

    public Calendar getFecValidacion() {
        return fecvalidacion;
    }

    public void setFecValidacion(Calendar fecvalidacion) {
        this.fecvalidacion = fecvalidacion;
    }

    public String getValComentarios() {
        return valcomentarios;
    }

    public void setValComentarios(String valcomentarios) {
        this.valcomentarios = valcomentarios;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValidacionCreacionDocumento[ id=" + id + " ]";
    }
}
