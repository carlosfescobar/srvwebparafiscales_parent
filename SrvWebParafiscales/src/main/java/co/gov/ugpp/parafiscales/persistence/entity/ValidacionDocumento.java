package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author jmunocab
 */
@Entity
@Table(name = "VALIDACION_DOCUMENTO")
public class ValidacionDocumento extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @SequenceGenerator(name = "validacionDocumentoIdSeq", sequenceName = "validacion_documento_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validacionDocumentoIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Size(max = 50)
    @Column(name = "ID_DOCUMENTO")
    private String idDocumento;
    @Size(max = 50)
    @Column(name = "ES_DEVOLUCION")
    private Boolean esDevolucion;
    @JoinColumn(name = "COD_CASUSAL_DEVOLUCION", referencedColumnName = "ID_VALOR_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private ValorDominio codCasusalDevolucion;
    @Size(max = 50)
    @Column(name = "DES_CAUSAL_DEVOLUCION")
    private String desCausalDevolucion;
    @JoinColumn(name = "ID_SANCION", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Sancion sancion;

    public ValidacionDocumento() {
    }

    public ValidacionDocumento(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdDocumento() {
        return idDocumento;
    }

    public void setIdDocumento(String idDocumento) {
        this.idDocumento = idDocumento;
    }

    public Boolean getEsDevolucion() {
        return esDevolucion;
    }

    public void setEsDevolucion(Boolean esDevolucion) {
        this.esDevolucion = esDevolucion;
    }

    public ValorDominio getCodCasusalDevolucion() {
        return codCasusalDevolucion;
    }

    public void setCodCasusalDevolucion(ValorDominio codCasusalDevolucion) {
        this.codCasusalDevolucion = codCasusalDevolucion;
    }

    public String getDesCausalDevolucion() {
        return desCausalDevolucion;
    }

    public void setDesCausalDevolucion(String desCausalDevolucion) {
        this.desCausalDevolucion = desCausalDevolucion;
    }

    public Sancion getSancion() {
        return sancion;
    }

    public void setSancion(Sancion sancion) {
        this.sancion = sancion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidacionDocumento)) {
            return false;
        }
        ValidacionDocumento other = (ValidacionDocumento) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValidacionDocumento[ id=" + id + " ]";
    }

}
