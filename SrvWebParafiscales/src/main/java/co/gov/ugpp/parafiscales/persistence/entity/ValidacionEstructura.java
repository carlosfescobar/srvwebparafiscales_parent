package co.gov.ugpp.parafiscales.persistence.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
/**
 *
 * @author Everis
 */
@Entity
@Table(name = "VALIDACION_ESTRUCTURA")
public class ValidacionEstructura extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;
    
    @Id
    @SequenceGenerator(name = "validacionEstructuraIdSeq", sequenceName = "validacion_estructura_id_seq", allocationSize = 1, initialValue = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "validacionEstructuraIdSeq")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @JoinColumn(name = "ID_ARCHIVO_ESTRUCTURA_SUGERIR", referencedColumnName = "ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Archivo archivo;
    @Column(name = "ES_NECESARIO_NUEVA_ESTRUCTURA")
    private Boolean esNecesarioNuevaEstructura;
    @Column(name = "ES_APROBADA_NUEVA_ESTRUCTURA")
    private Boolean esAprobadaNuevaEstructura;
    @Column(name = "DESC_ESTRUCTURA_NO_APROBADA")
    private String descEstructuraNoAprobada;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO_ESTRUCUTRA_NUEVA", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION_ESTRUCTURA_NUEVA", referencedColumnName = "ID_VERSION")
    })
    private Formato formatoEstructuraNueva;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO_ELEGIDO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION_ELEGIDO", referencedColumnName = "ID_VERSION")
    })
    private Formato formatoElegido;

    
    
    public ValidacionEstructura() {
    }

    public ValidacionEstructura(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public Archivo getArchivo() {
        return archivo;
    }

    public void setArchivo(Archivo archivo) {
        this.archivo = archivo;
    }

    public Boolean getEsNecesarioNuevaEstructura() {
        return esNecesarioNuevaEstructura;
    }

    public void setEsNecesarioNuevaEstructura(Boolean esNecesarioNuevaEstructura) {
        this.esNecesarioNuevaEstructura = esNecesarioNuevaEstructura;
    }

    public Boolean getEsAprobadaNuevaEstructura() {
        return esAprobadaNuevaEstructura;
    }

    public void setEsAprobadaNuevaEstructura(Boolean esAprobadaNuevaEstructura) {
        this.esAprobadaNuevaEstructura = esAprobadaNuevaEstructura;
    }
    
    public String getDescEstructuraNoAprobada() {
        return descEstructuraNoAprobada;
    }

    public void setDescEstructuraNoAprobada(String descEstructuraNoAprobada) {
        this.descEstructuraNoAprobada = descEstructuraNoAprobada;
    }
  
    public Formato getFormatoEstructuraNueva() {
        return formatoEstructuraNueva;
    }

    public void setFormatoEstructuraNueva(Formato formatoEstructuraNueva) {
        this.formatoEstructuraNueva = formatoEstructuraNueva;
    }
    
    public Formato getFormatoElegido() {
        return formatoElegido;
    }

    public void setFormatoElegido(Formato formatoElegido) {
        this.formatoElegido = formatoElegido;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidacionEstructura)) {
            return false;
        }
        ValidacionEstructura other = (ValidacionEstructura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValidacionEstructura[ id=" + id + " ]";
    }
}