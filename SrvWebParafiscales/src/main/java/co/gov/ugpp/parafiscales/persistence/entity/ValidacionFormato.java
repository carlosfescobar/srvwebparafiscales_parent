package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla, jgonzalezt
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "validacionFormato.findByFormatoPK",
            query = "SELECT f FROM ValidacionFormato f WHERE f.formato.formatoPK.idFormato = :idFormato AND f.formato.formatoPK.idVersion = :idVersion AND :fecActual BETWEEN f.fechaInicioVigencia and f.fechaFinVigencia ORDER BY f.fechaCreacion DESC"),
    @NamedQuery(name = "validacionFormato.validaciones",
            query = "SELECT f FROM ValidacionFormato f WHERE f.formato.formatoPK.idFormato = :idFormato AND f.formato.formatoPK.idVersion = :idVersion AND :fecActual BETWEEN f.fechaInicioVigencia and f.fechaFinVigencia")
//        
})
@Table(name = "VALIDACION_FORMATO")
public class ValidacionFormato extends AbstractEntity<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID", updatable = false)
    private Long id;
    @Column(name = "NOMBRE_CLASE")
    private String nombreClase;
    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION", referencedColumnName = "ID_VERSION")
    })
    private Formato formato;
    @Column(name = "FEC_INICIO_VIGENCA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaInicioVigencia;
    @Column(name = "FEC_FINAL_VIGENCA")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Calendar fechaFinVigencia;
    @Lob
    @Column(name = "VAL_SCRIPT")
    private byte[] valScript;
    @Column(name = "COD_TIPO_VALIDACION")
    private String codTipoValidacion;

    public ValidacionFormato() {
    }

    public ValidacionFormato(Long id) {
        this.id = id;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setId(Long id) {
        this.id = id;
    }

    public String getNombreClase() {
        return nombreClase;
    }

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    protected void setNombreClase(String nombreClase) {
        this.nombreClase = nombreClase;
    }

    public Calendar getFechaInicioVigencia() {
        return fechaInicioVigencia;
    }

    public void setFechaInicioVigencia(Calendar fechaInicioVigencia) {
        this.fechaInicioVigencia = fechaInicioVigencia;
    }

    public Calendar getFechaFinVigencia() {
        return fechaFinVigencia;
    }

    public void setFechaFinVigencia(Calendar fechaFinVigencia) {
        this.fechaFinVigencia = fechaFinVigencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValidacionFormato)) {
            return false;
        }
        ValidacionFormato other = (ValidacionFormato) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValidacionFormato[ id=" + id + " ]";
    }

    /**
     * @return the valScript
     */
    public byte[] getValScript() {
        return valScript;
    }

    /**
     * @param valScript the valScript to set
     */
    public void setValScript(byte[] valScript) {
        this.valScript = valScript;
    }

    /**
     * @return the codTipoValidacion
     */
    public String getCodTipoValidacion() {
        return codTipoValidacion;
    }

    /**
     * @param codTipoValidacion the codTipoValidacion to set
     */
    public void setCodTipoValidacion(String codTipoValidacion) {
        this.codTipoValidacion = codTipoValidacion;
    }

}
