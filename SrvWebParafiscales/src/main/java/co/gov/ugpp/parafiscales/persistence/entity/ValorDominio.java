package co.gov.ugpp.parafiscales.persistence.entity;

import java.util.Calendar;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 *
 * @author rpadilla
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "valorDominio.findByCodDominio",
            query = "SELECT vd FROM ValorDominio vd WHERE vd.dominio.id = :codDominio")})
@Table(name = "VALOR_DOMINIO")
public class ValorDominio implements IEntity<String> {

    private static final long serialVersionUID = 1L;

    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID_VALOR_DOMINIO", updatable = false)
    private String id;
    @JoinColumn(name = "COD_DOMINIO", referencedColumnName = "COD_DOMINIO")
    @ManyToOne(fetch = FetchType.LAZY)
    private Dominio dominio;
    @Column(name = "VAL_NOM_VALOR_DOMINIO")
    private String nombre;
    @Column(name = "DESC_VALOR_DOMINIO")
    private String descValorDominio;
    @Column(name = "FEC_INICIAL_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecInicialVigencia;
    @Column(name = "FEC_FINAL_VIGENCIA")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecFinalVigencia;
    @Column(name = "ID_USUARIO")
    private String idUsuario;
    @Column(name = "FEC_CREACION_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecCreacionRegistro;
    @Column(name = "FEC_ACTUALIZACION_REGISTRO")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecActualizacionRegistro;

    public ValorDominio() {
    }

    public ValorDominio(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescValorDominio() {
        return descValorDominio;
    }

    public void setDescValorDominio(String descValorDominio) {
        this.descValorDominio = descValorDominio;
    }

    public Calendar getFecInicialVigencia() {
        return fecInicialVigencia;
    }

    public void setFecInicialVigencia(Calendar fecInicialVigencia) {
        this.fecInicialVigencia = fecInicialVigencia;
    }

    public Calendar getFecFinalVigencia() {
        return fecFinalVigencia;
    }

    public void setFecFinalVigencia(Calendar fecFinalVigencia) {
        this.fecFinalVigencia = fecFinalVigencia;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Calendar getFecCreacionRegistro() {
        return fecCreacionRegistro;
    }

    public void setFecCreacionRegistro(Calendar fecCreacionRegistro) {
        this.fecCreacionRegistro = fecCreacionRegistro;
    }

    public Calendar getFecActualizacionRegistro() {
        return fecActualizacionRegistro;
    }

    public void setFecActualizacionRegistro(Calendar fecActualizacionRegistro) {
        this.fecActualizacionRegistro = fecActualizacionRegistro;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorDominio)) {
            return false;
        }
        ValorDominio other = (ValorDominio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.procesos.persistence.entity.ValorDominio[ id=" + id + " ]";
    }

}
