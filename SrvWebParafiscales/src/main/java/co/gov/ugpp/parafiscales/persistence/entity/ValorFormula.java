package co.gov.ugpp.parafiscales.persistence.entity;

import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author franzjr
 */
@Entity
@TableGenerator(name = "seqvalorformula", initialValue = 1, allocationSize = 1)
@Table(name = "VALOR_FORMULA")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ValorFormula.findAll", query = "SELECT v FROM ValorFormula v"),
    @NamedQuery(name = "ValorFormula.findById", query = "SELECT v FROM ValorFormula v WHERE v.id = :id"),
    @NamedQuery(name = "ValorFormula.findByValor", query = "SELECT v FROM ValorFormula v WHERE v.valor = :valor"),
    @NamedQuery(name = "ValorFormula.findByFecha", query = "SELECT v FROM ValorFormula v WHERE v.fecha = :fecha"),
    @NamedQuery(name = "ValorFormula.findByEstado", query = "SELECT v FROM ValorFormula v WHERE v.estado = :estado")})
public class ValorFormula extends AbstractEntity<Long> {
    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "seqvalorformula")
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Long id;
    @Column(name = "FECHA")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    
    @Column(name = "ID_TABLA")
    private BigInteger idTabla;
    
    @Column(name = "VALOR")
    private BigInteger valor;
    
    @Column(name = "ESTADO")
    private BigInteger estado;
    @JoinColumn(name = "APLICACION_FORMULA", referencedColumnName = "ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private AplicacionFormula aplicacionFormula;

    public ValorFormula() {
    }

    public ValorFormula(Long id) {
        this.id = id;
    }

   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigInteger getEstado() {
        return estado;
    }

    public void setEstado(BigInteger estado) {
        this.estado = estado;
    }

    public AplicacionFormula getAplicacionFormula() {
        return aplicacionFormula;
    }

    public void setAplicacionFormula(AplicacionFormula aplicacionFormula) {
        this.aplicacionFormula = aplicacionFormula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ValorFormula)) {
            return false;
        }
        ValorFormula other = (ValorFormula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "org.ugpp.pf.persistence.entities.li.ValorFormula[ id=" + id + " ]";
    }

    public BigInteger getValor() {
        return valor;
    }

    public void setValor(BigInteger valor) {
        this.valor = valor;
    }

    public BigInteger getIdTabla() {
        return idTabla;
    }

    public void setIdTabla(BigInteger idTabla) {
        this.idTabla = idTabla;
    }

    /**
     * @return the tabla
     */
    /*public String getTabla() {
        return tabla;
    }*/

    /**
     * @param tabla the tabla to set
     */
    /*public void setTabla(String tabla) {
        this.tabla = tabla;
    }*/
    
}
