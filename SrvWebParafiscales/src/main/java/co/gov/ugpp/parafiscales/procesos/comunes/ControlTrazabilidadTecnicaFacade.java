package co.gov.ugpp.parafiscales.procesos.comunes;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.trazabilidadtecnicaprocesotipo.v1.TrazabilidadTecnicaProcesoTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface ControlTrazabilidadTecnicaFacade extends Serializable {

   void  OpRegistrarControlTrazabilidadTecnica(List<TrazabilidadTecnicaProcesoTipo> trazabilidadTecnicaProcesoTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
