package co.gov.ugpp.parafiscales.procesos.comunes;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.trazabilidadtecnicaprocesotipo.v1.TrazabilidadTecnicaProcesoTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.EstadoEjecucionTrazabilidadEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.TrazabilidadTecnicaDao;
import co.gov.ugpp.parafiscales.persistence.dao.TrazabilidadTecnicaeEjecucionDao;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.PropsReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz ControlTrazabilidadTecnicaFacade que contiene
 * las operaciones del servicio SrvAplControlTrazabilidadTecnica
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ControlTrazabilidadTecnicaFacadeImpl extends AbstractFacade implements ControlTrazabilidadTecnicaFacade {

    private static final Logger LOG = LoggerFactory.getLogger(ControlTrazabilidadTecnicaFacadeImpl.class);

    @EJB
    private TrazabilidadTecnicaDao trazabilidadTecnicaDao;

    @EJB
    private TrazabilidadTecnicaeEjecucionDao trazabilidadTecnicaeEjecucionDao;

    /**
     * Método que realiza la inserción de los registros de trazabilidad técnica
     *
     * @param trazabilidadTecnicaProcesoTipoList el listado de trazabilidad
     * técnica que contiene la información de las instancias de proceso y sus
     * steps
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @throws AppException
     */
    @Override
    @Asynchronous
    public void OpRegistrarControlTrazabilidadTecnica(List<TrazabilidadTecnicaProcesoTipo> trazabilidadTecnicaProcesoTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (trazabilidadTecnicaProcesoTipoList == null || trazabilidadTecnicaProcesoTipoList.isEmpty()) {
            throw new AppException("Debe enviar los objetos a crear");
        }
        final Date fecInicioEjecucion = new Date();
        List<Object[]> parametrosIngresoTrazabilidadEjecucion = this.buildParametrosIngresoEjecucionTrazabilidadOnCreate(contextoTransaccionalTipo, fecInicioEjecucion);
        try {
            trazabilidadTecnicaeEjecucionDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.INSERT_TRAZABILIDAD_EJECUCION_TECNICA), parametrosIngresoTrazabilidadEjecucion);
        } catch (SQLException sqe) {
            LOG.info("..:: EXCEPCIÓN CREACIÓN REGISTRO EJECUCIÓN TRAZABILIDAD TÉCNICA ::.." + sqe.getMessage());
            throw new AppException(sqe.getMessage());
        }
        LOG.info("..:: INICIO CREACIÓN REGISTROS TRAZABILIDAD TÉCNICA ::..");
        //Se crea registro de la ejecución de la trazabilidad técnica
        final List<Object[]> parametrosIngresoTrazabilidad = this.buildParametrosIngresoTrazabilidad(trazabilidadTecnicaProcesoTipoList, contextoTransaccionalTipo);
        try {
            trazabilidadTecnicaDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.INSERT_TRAZABILIDAD_TECNICA), parametrosIngresoTrazabilidad);
        } catch (SQLException sqe) {
            LOG.info("..:: EXCEPCIÓN CREACIÓN REGISTROS TRAZABILIDAD TÉCNICA ::.." + sqe.getMessage());
            //Se coloca la ejecución de la trazabilidad como fallida
            List<Object[]> parametrosActualizacionTrazabilidadEjecucion = this.buildParametrosIngresoEjecucionTrazabilidadOnUpdate(contextoTransaccionalTipo, EstadoEjecucionTrazabilidadEnum.PROCESO_TRAZABILIDAD_FALLIDO, fecInicioEjecucion);
            try {
                trazabilidadTecnicaDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.ACTUALIZAR_TRAZABILIDAD_EJECUCION_TECNICA), parametrosActualizacionTrazabilidadEjecucion);
            } catch (SQLException sqex) {
                LOG.info("..:: EXCEPCIÓN ACTUALIZANDO LA EJECUCIÓN TRAZABILIDAD TÉCNICA A FALLIDO::.." + sqex.getMessage());
            }
            throw new AppException(sqe.getMessage());
        }
        LOG.info("..:: SE FINALIZA LA CREACIÓN REGISTROS TRAZABILIDAD TÉCNICA ::..");
        List<Object[]> parametrosActualizacionTrazabilidadEjecucion = this.buildParametrosIngresoEjecucionTrazabilidadOnUpdate(contextoTransaccionalTipo, EstadoEjecucionTrazabilidadEnum.PROCESO_TRAZABILIDAD_COMPLETADO, fecInicioEjecucion);
        try {
            trazabilidadTecnicaDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.ACTUALIZAR_TRAZABILIDAD_EJECUCION_TECNICA), parametrosActualizacionTrazabilidadEjecucion);
        } catch (SQLException sqe) {
            LOG.info("..:: EXCEPCIÓN ACTUALIZANDO LA EJECUCIÓN TRAZABILIDAD TÉCNICA A COMPLETADO::.." + sqe.getMessage());
            throw new AppException(sqe.getMessage());
        }
    }

    /**
     * Método que arma los parametros de inserción de la trazabilidad técnica
     *
     * @param trazabilidadTecnicaProcesoTipo el objeto origen
     * @return
     */
    private List<Object[]> buildParametrosIngresoTrazabilidad(final List<TrazabilidadTecnicaProcesoTipo> trazabilidadTecnicaProcesoTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) {
        List<Object[]> parametrosTrazabilidad = new ArrayList<Object[]>();
        for (TrazabilidadTecnicaProcesoTipo trazabilidadTecnicaProcesoTipo : trazabilidadTecnicaProcesoTipoList) {
            Object[] parametrosIngresoTrazabilidad = new Object[Constants.CAMPOS_PERSISITR_TRAZABILIDAD_TECNICA];
            parametrosIngresoTrazabilidad[Constants.ID_TRAZABILIDAD_INSTANCIA_PROC] = trazabilidadTecnicaProcesoTipo.getIdInstanciaProceso();
            parametrosIngresoTrazabilidad[Constants.COD_ESTADO_PROCESO] = trazabilidadTecnicaProcesoTipo.getCodEstadoProceso();
            parametrosIngresoTrazabilidad[Constants.DESC_ESTADO_PROCESO] = trazabilidadTecnicaProcesoTipo.getDescEstadoProceso();
            parametrosIngresoTrazabilidad[Constants.VAL_NOMBRE_ACTIVIDAD] = trazabilidadTecnicaProcesoTipo.getValNombreActividad();
            parametrosIngresoTrazabilidad[Constants.COD_ESTADO_ACTIVIDAD] = trazabilidadTecnicaProcesoTipo.getCodEstadoActividad();
            parametrosIngresoTrazabilidad[Constants.DESC_ESTADO_ACTIVIDAD] = trazabilidadTecnicaProcesoTipo.getDescEstadoActividad();
            parametrosIngresoTrazabilidad[Constants.ID_INSTANCIA_ACTIVIDAD] = trazabilidadTecnicaProcesoTipo.getIdInstanciaActividad();
            parametrosIngresoTrazabilidad[Constants.ID_DEFINICION_ACTIVIDAD] = trazabilidadTecnicaProcesoTipo.getIdDefinicionActividad();
            parametrosIngresoTrazabilidad[Constants.ID_USUARIO] = trazabilidadTecnicaProcesoTipo.getIdUsuario();
            parametrosIngresoTrazabilidad[Constants.COD_ROL] = trazabilidadTecnicaProcesoTipo.getCodRol();
            parametrosIngresoTrazabilidad[Constants.VAL_NOMBRE_ROL] = trazabilidadTecnicaProcesoTipo.getValNombreRol();
            parametrosIngresoTrazabilidad[Constants.FEC_ESTADO_ACTIVIDAD] = (trazabilidadTecnicaProcesoTipo.getFecEstadoActividad() == null ? null : trazabilidadTecnicaProcesoTipo.getFecEstadoActividad().getTime());
            parametrosIngresoTrazabilidad[Constants.FEC_CREACION] = (trazabilidadTecnicaProcesoTipo.getFecCreacionRegistro() == null ? null : trazabilidadTecnicaProcesoTipo.getFecCreacionRegistro().getTime());
            parametrosIngresoTrazabilidad[Constants.FEC_ACTUALIZACION] = (trazabilidadTecnicaProcesoTipo.getFecActualizacionRegistro() == null ? null : trazabilidadTecnicaProcesoTipo.getFecActualizacionRegistro().getTime());
            parametrosIngresoTrazabilidad[Constants.ID_USUARIO_CREACION] = contextoTransaccionalTipo.getIdUsuario();
            parametrosIngresoTrazabilidad[Constants.ID_USUARIO_MODIFICACION] = null;
            parametrosIngresoTrazabilidad[Constants.VAL_DIAS_FIN_TAREA] = trazabilidadTecnicaProcesoTipo.getValDiasCompletarTarea();
            parametrosTrazabilidad.add(parametrosIngresoTrazabilidad);
        }
        return parametrosTrazabilidad;
    }

    /**
     * Método que arma los parametros para persistir la tabla trazabilidad
     * ejecución
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @return
     */
    private List<Object[]> buildParametrosIngresoEjecucionTrazabilidadOnCreate(final ContextoTransaccionalTipo contextoTransaccionalTipo, final Date fecInicioEjecucion) {
        List<Object[]> parametrosCreateEjecucionTrazabilidad = new ArrayList<Object[]>();
        Object[] parametrosEjecucionTrazabilidad = new Object[Constants.CAMPOS_PERSISITR_EJECUCION_TRAZABILIDAD_TECNICA];
        parametrosEjecucionTrazabilidad[Constants.COD_ESTADO_EJECUCION] = EstadoEjecucionTrazabilidadEnum.PROCESO_TRAZABILIDAD_INICIADO.getCode();
        parametrosEjecucionTrazabilidad[Constants.FEC_INICIO_EJECUCION] = fecInicioEjecucion;
        parametrosEjecucionTrazabilidad[Constants.FEC_FIN_EJECUCION] = null;
        parametrosEjecucionTrazabilidad[Constants.CANTIDAD_INTENTOS] = Constants.FIRST_TRY;
        parametrosEjecucionTrazabilidad[Constants.ID_USUARIO_CREACION_TRAZABILIDAD] = contextoTransaccionalTipo.getIdUsuario();
        parametrosEjecucionTrazabilidad[Constants.ID_USUARIO_MODIFICACION_TRAZABILIDAD] = null;
        parametrosEjecucionTrazabilidad[Constants.FEC_CREACION_TRAZABILIDAD] = new Date();
        parametrosEjecucionTrazabilidad[Constants.FEC_ACTUALIZACION_TRAZABILIDAD] = null;
        parametrosCreateEjecucionTrazabilidad.add(parametrosEjecucionTrazabilidad);
        return parametrosCreateEjecucionTrazabilidad;
    }

    /**
     * Método que arma los parametros para persistir la tabla trazabilidad
     * ejecución para realizar update
     *
     * @param contextoTransaccionalTipo
     * @return
     */
    private List<Object[]> buildParametrosIngresoEjecucionTrazabilidadOnUpdate(final ContextoTransaccionalTipo contextoTransaccionalTipo, final EstadoEjecucionTrazabilidadEnum estadoEjecucion, final Date fecInicioEjecucion) {
        List<Object[]> parametrosUpdateEjecucionTrazabilidad = new ArrayList<Object[]>();
        Object[] parametrosEjecucionTrazabilidad = new Object[Constants.CAMPOS_ACTUALIZAR_EJECUCION_TRAZABILIDAD_TECNICA];
        parametrosEjecucionTrazabilidad[Constants.COD_ESTADO_EJECUCION_UPDATE] = estadoEjecucion.getCode();
        parametrosEjecucionTrazabilidad[Constants.FEC_FIN_EJECUCION_UPDATE] = new Date();
        parametrosEjecucionTrazabilidad[Constants.ID_USUARIO_MODIFICACION_TRAZABILIDAD_UPDATE] = contextoTransaccionalTipo.getIdUsuario();
        parametrosEjecucionTrazabilidad[Constants.FEC_MODIFICACION_TRAZABILIDAD_UPDATE] = new Date();
        parametrosEjecucionTrazabilidad[Constants.FEC_FIN_EJECUCION_PARAMETER] = fecInicioEjecucion;
        parametrosUpdateEjecucionTrazabilidad.add(parametrosEjecucionTrazabilidad);
        return parametrosUpdateEjecucionTrazabilidad;
    }
}
