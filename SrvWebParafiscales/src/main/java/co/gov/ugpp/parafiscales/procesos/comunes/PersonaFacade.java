package co.gov.ugpp.parafiscales.procesos.comunes;

import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personatipo.v1.PersonaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface PersonaFacade extends Serializable {

    List<ConceptoPersonaTipo> buscarPorIdPersona(List<IdentificacionTipo> identificacionTipos,String codDominioPersona, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
