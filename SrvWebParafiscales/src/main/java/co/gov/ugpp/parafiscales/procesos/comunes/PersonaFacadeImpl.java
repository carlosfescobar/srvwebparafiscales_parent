package co.gov.ugpp.parafiscales.procesos.comunes;

import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personatipo.v1.PersonaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.Util;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz PersonaFacade que contiene las operaciones del
 * servicio SrvAplPersona
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class PersonaFacadeImpl extends AbstractFacade implements PersonaFacade {

    @EJB
    private PersonaDao personaDao;

    @Override
    public List<ConceptoPersonaTipo> buscarPorIdPersona(List<IdentificacionTipo> identificacionTipos, String codDominioPersona, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
         if (identificacionTipos == null || identificacionTipos.isEmpty()) {
            throw new AppException("Debe proporcionar un listado de identificaciones para poder realizar la busqueda");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTipos, IdentificacionTipo.class, Identificacion.class);

        ClasificacionPersonaEnum personasClasificacion = Util.obtenerClasificacionPersonaFromString(codDominioPersona);
        final List<Persona> personas = personaDao.findByIdentificacionList(identificacionList, personasClasificacion);

        List<ConceptoPersonaTipo> personaTipoList = new ArrayList<ConceptoPersonaTipo>();
        for (Persona personaIdentificacion : personas) {
            personaIdentificacion.setClasificacionPersona(personasClasificacion);
            ConceptoPersonaTipo personaTipo = mapper.map(personaIdentificacion, ConceptoPersonaTipo.class);
            personaTipoList.add(personaTipo);
        }

        return personaTipoList;
    }

}
