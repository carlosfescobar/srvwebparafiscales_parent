/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.comunes;


import co.gov.ugpp.comun.srvaplconceptopersona.v1.MsjOpBuscarPorIdPersonaFallo;
import co.gov.ugpp.comun.srvaplconceptopersona.v1.OpBuscarPorIdPersonaRespTipo;
import co.gov.ugpp.comun.srvaplconceptopersona.v1.OpBuscarPorIdPersonaSolTipo;
import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personatipo.v1.PersonaTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplConceptoPersona",
        portName = "portSrvAplConceptoPersonaSOAP",
        endpointInterface = "co.gov.ugpp.comun.srvaplconceptopersona.v1.PortSrvAplConceptoPersonaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Comun/SrvAplConceptoPersona/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplConceptoPersona extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplConceptoPersona.class);

    @EJB
    private PersonaFacade personaFacade;

    public OpBuscarPorIdPersonaRespTipo opBuscarPorIdPersona(OpBuscarPorIdPersonaSolTipo msjOpBuscarPorIdPersonaSol) throws MsjOpBuscarPorIdPersonaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdPersonaSol.getContextoTransaccional();
        final List<IdentificacionTipo> identificacionTipoList = msjOpBuscarPorIdPersonaSol.getIdentificacion();
        final String codDominioPersona = msjOpBuscarPorIdPersonaSol.getCodDominioPersona();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final List<ConceptoPersonaTipo> personaTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            personaTipoList = personaFacade.buscarPorIdPersona(identificacionTipoList, codDominioPersona, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdPersonaRespTipo resp = new OpBuscarPorIdPersonaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getPersona().addAll(personaTipoList);
        return resp;

    }

}
