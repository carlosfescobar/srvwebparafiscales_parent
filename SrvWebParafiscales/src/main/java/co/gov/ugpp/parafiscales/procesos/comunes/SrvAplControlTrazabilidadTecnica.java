package co.gov.ugpp.parafiscales.procesos.comunes;

import co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1.MsjOpRegistrarControlTrazabilidadTecnicaFallo;
import co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1.OpRegistrarControlTrazabilidadTecnicaRespTipo;
import co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1.OpRegistrarControlTrazabilidadTecnicaSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.trazabilidadtecnicaprocesotipo.v1.TrazabilidadTecnicaProcesoTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplControlTrazabilidadTecnica",
        portName = "portSrvAplControlTrazabilidadTecnicaSOAP",
        endpointInterface = "co.gov.ugpp.comun.srvaplcontroltrazabilidadtecnica.v1.PortSrvAplControlTrazabilidadTecnicaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Comun/SrvAplControlTrazabilidadTecnica/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplControlTrazabilidadTecnica extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplControlTrazabilidadTecnica.class);

    @EJB
    private ControlTrazabilidadTecnicaFacade trazabilidadTecnicaFacade;

    public OpRegistrarControlTrazabilidadTecnicaRespTipo opRegistrarControlTrazabilidadTecnica(OpRegistrarControlTrazabilidadTecnicaSolTipo msjOpRegistrarControlTrazabilidadTecnicaSol) throws MsjOpRegistrarControlTrazabilidadTecnicaFallo {
        LOG.info("OPERACION: opRegistrarControlTrazabilidadTecnica ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpRegistrarControlTrazabilidadTecnicaSol.getContextoTransaccional();
        final List<TrazabilidadTecnicaProcesoTipo> trazabilidadTecnicaProcesoList = msjOpRegistrarControlTrazabilidadTecnicaSol.getTrazabilidadTecnicaProceso();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            trazabilidadTecnicaFacade.OpRegistrarControlTrazabilidadTecnica(trazabilidadTecnicaProcesoList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpRegistrarControlTrazabilidadTecnicaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpRegistrarControlTrazabilidadTecnicaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        OpRegistrarControlTrazabilidadTecnicaRespTipo registrarControlTrazabilidadTecnicaRespTipo = new OpRegistrarControlTrazabilidadTecnicaRespTipo();
        registrarControlTrazabilidadTecnicaRespTipo.setContextoRespuesta(cr);
        LOG.info("OPERACION: opRegistrarControlTrazabilidadTecnica ::: FIN");
        return registrarControlTrazabilidadTecnicaRespTipo;
    }
}
