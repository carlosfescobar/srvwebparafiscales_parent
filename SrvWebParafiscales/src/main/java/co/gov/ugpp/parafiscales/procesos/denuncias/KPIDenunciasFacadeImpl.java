package co.gov.ugpp.parafiscales.procesos.denuncias;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz KPIDenunciasFacade que contiene las operaciones
 * del servicio SrvAplKPIDenuncias
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class KPIDenunciasFacadeImpl extends AbstractFacade implements KPIDenunciasFacade {

    @EJB
    private DenunciaDao denunciaDao;

    @Override
    public PagerData<DenunciaTipo> consultarDenunciaKPI(final List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Denuncia> denunciasPagerData = denunciaDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<DenunciaTipo> denunciaTipoList = new ArrayList<DenunciaTipo>();

        for (final Denuncia denuncia : denunciasPagerData.getData()) {
            denuncia.setUseOnlySpecifiedFields(true);
            final DenunciaTipo denunciaTipo = mapper.map(denuncia, DenunciaTipo.class);
            denunciaTipoList.add(denunciaTipo);
        }
        return new PagerData(denunciaTipoList, denunciasPagerData.getNumPages());
    }

}
