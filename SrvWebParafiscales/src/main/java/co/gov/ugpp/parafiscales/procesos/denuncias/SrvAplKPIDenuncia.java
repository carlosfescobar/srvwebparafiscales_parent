package co.gov.ugpp.parafiscales.procesos.denuncias;

import co.gov.ugpp.denuncias.srvaplkpidenuncia.v1.MsjOpConsultarDenunciaKPIFallo;
import co.gov.ugpp.denuncias.srvaplkpidenuncia.v1.OpConsultarDenunciaKPIRespTipo;
import co.gov.ugpp.denuncias.srvaplkpidenuncia.v1.OpConsultarDenunciaKPISolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplKPIDenuncia",
        portName = "portSrvAplKPIDenunciaSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplkpidenuncia.v1.PortSrvAplKPIDenunciaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplKPIDenuncia/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPIDenuncia extends AbstractSrvApl {

    @EJB
    private KPIDenunciasFacade kPIDenunciasFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPIDenuncia.class);

    public OpConsultarDenunciaKPIRespTipo opConsultarDenunciaKPI(OpConsultarDenunciaKPISolTipo msjOpConsultarDenunciaKPISol) throws MsjOpConsultarDenunciaKPIFallo {

        LOG.info("OPERACION: opConsultarDenunciaKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarDenunciaKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarDenunciaKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<DenunciaTipo> denunciaTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            denunciaTipoPagerData = kPIDenunciasFacade.consultarDenunciaKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarDenunciaKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarDenunciaKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarDenunciaKPIRespTipo resp = new OpConsultarDenunciaKPIRespTipo();

        cr.setValCantidadPaginas(denunciaTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getDenuncias().addAll(denunciaTipoPagerData.getData());
        resp.setCantidadDenuncias(Integer.toString(denunciaTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarDenunciaKPI ::: FIN");

        return resp;
    }
}
