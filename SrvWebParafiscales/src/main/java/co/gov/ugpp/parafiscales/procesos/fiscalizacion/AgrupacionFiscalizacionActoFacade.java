package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface AgrupacionFiscalizacionActoFacade extends Serializable {

    void crearAgrupacionFiscalizacioActo(AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<AgrupacionFiscalizacionActoTipo> buscarPorCriteriosAgrupacionFiscalizacioActo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
