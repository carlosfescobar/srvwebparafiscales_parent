package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionHasActoAdmiDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionActoAdministrativo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz AgrupacionFiscalizacionActoFacade que contiene
 * las operaciones del servicio SrvAplAgrupacionFiscalizacionActo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AgrupacionFiscalizacionActoImpl extends AbstractFacade implements AgrupacionFiscalizacionActoFacade {

    @EJB
    private FiscalizacionDao fiscalizacionDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private FiscalizacionHasActoAdmiDao fiscalizacionHasActoAdmiDao;

    @Override
    public void crearAgrupacionFiscalizacioActo(AgrupacionFiscalizacionActoTipo agrupacionFiscalizacioActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (agrupacionFiscalizacioActoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkAgrupacionFiscalizacionActo(agrupacionFiscalizacioActoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Fiscalizacion fiscalizacion = fiscalizacionDao.find(Long.valueOf(agrupacionFiscalizacioActoTipo.getFiscalizacion().getIdFiscalizacion()));

        if (fiscalizacion == null) {
            throw new AppException("No se encontró una fiscalización con el ID: " + agrupacionFiscalizacioActoTipo.getFiscalizacion().getIdFiscalizacion());
        }

        fiscalizacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAgrupacionFiscalizacionActo(agrupacionFiscalizacioActoTipo, fiscalizacion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        fiscalizacionDao.edit(fiscalizacion);
    }

    @Override
    public PagerData<AgrupacionFiscalizacionActoTipo> buscarPorCriteriosAgrupacionFiscalizacioActo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Fiscalizacion> pagerDataEntity
                = fiscalizacionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValNumPagina(), contextoTransaccionalTipo.getValTamPagina());

        final List<AgrupacionFiscalizacionActoTipo> agrupacionFiscalizacionActoTipoList = new ArrayList<AgrupacionFiscalizacionActoTipo>();

        for (final Fiscalizacion fiscalizacion : pagerDataEntity.getData()) {
            final AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo = mapper.map(fiscalizacion, AgrupacionFiscalizacionActoTipo.class);

            agrupacionFiscalizacionActoTipoList.add(agrupacionFiscalizacionActoTipo);
        }

        return new PagerData<AgrupacionFiscalizacionActoTipo>(agrupacionFiscalizacionActoTipoList, pagerDataEntity.getNumPages());

    }

    private void populateAgrupacionFiscalizacionActo(AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo, Fiscalizacion fiscalizacion, List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (agrupacionFiscalizacionActoTipo.getActosAdministrativos() != null
                && !agrupacionFiscalizacionActoTipo.getActosAdministrativos().isEmpty()) {

            for (ActoAdministrativoTipo actoAdministrativoTipo : agrupacionFiscalizacionActoTipo.getActosAdministrativos()) {

                if (actoAdministrativoTipo != null
                        && StringUtils.isNotBlank(actoAdministrativoTipo.getIdActoAdministrativo())) {
                    final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(actoAdministrativoTipo.getIdActoAdministrativo());
                    if (actoAdministrativo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un Acto Administrativo con el  ID: " + actoAdministrativoTipo.getIdActoAdministrativo()));
                    } else {

                        FiscalizacionActoAdministrativo agrupacionFiscalizacionActo = fiscalizacionHasActoAdmiDao.findByfindByFiscalizacionAndActoAdminitrativo(fiscalizacion, actoAdministrativo);

                        if (agrupacionFiscalizacionActo != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre la Fiscalización con ID: "
                                    + fiscalizacion.getId().toString() + ", y el Acto Administrativo con ID: " + actoAdministrativoTipo.getIdActoAdministrativo()));
                        } else {
                            agrupacionFiscalizacionActo = new FiscalizacionActoAdministrativo();
                            agrupacionFiscalizacionActo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            agrupacionFiscalizacionActo.setFiscalizacion(fiscalizacion);
                            agrupacionFiscalizacionActo.setActoAdministrativo(actoAdministrativo);
                            fiscalizacion.getActosAdministrativosFiscalizacion().add(agrupacionFiscalizacionActo);
                        }
                    }
                }
            }
        }
    }
}
