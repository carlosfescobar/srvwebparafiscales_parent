package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface BusquedaInformacionPersonaFacade extends Serializable {

    Long crearBusqueda(BusquedaTipo busquedaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<BusquedaTipo> buscarPorCriteriosBusqueda(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarBusqueda(BusquedaTipo busquedaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
