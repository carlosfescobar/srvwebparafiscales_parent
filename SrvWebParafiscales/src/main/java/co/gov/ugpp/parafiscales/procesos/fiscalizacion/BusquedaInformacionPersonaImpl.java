package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.BusquedaDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Busqueda;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz BiusquedaFacade que contiene las operaciones del servicio SrvAplBusqueda
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class BusquedaInformacionPersonaImpl extends AbstractFacade implements BusquedaInformacionPersonaFacade {

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private BusquedaDao busquedaDao;
    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @Override
    public Long crearBusqueda(BusquedaTipo busquedaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (busquedaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        //Validator.checkBusqueda(busquedaTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Busqueda busqueda = new Busqueda();

        busqueda.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateBusqueda(busquedaTipo, busqueda, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idBusqueda = busquedaDao.create(busqueda);

        return idBusqueda;
    }

    @Override
    public PagerData<BusquedaTipo> buscarPorCriteriosBusqueda(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        
        final PagerData<Busqueda> pagerDataEntity
                = busquedaDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<BusquedaTipo> busquedaTipoList = new ArrayList<BusquedaTipo>();

        for (final Busqueda busqueda : pagerDataEntity.getData()) {
            final BusquedaTipo busquedaTipo = mapper.map(busqueda, BusquedaTipo.class);
            if (busquedaTipo.getFuncionarioEncargado() != null
                    && StringUtils.isNotBlank(busquedaTipo.getFuncionarioEncargado().getIdFuncionario())) {
                final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(busquedaTipo.getFuncionarioEncargado(), contextoTransaccionalTipo);
                busquedaTipo.setFuncionarioEncargado(funcionarioTipo);
            }
            busquedaTipoList.add(busquedaTipo);
        }

        return new PagerData<BusquedaTipo>(busquedaTipoList, pagerDataEntity.getNumPages());

    }

    @Override
    public void actualizarBusqueda(BusquedaTipo busquedaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

  
        if (busquedaTipo == null || StringUtils.isBlank(busquedaTipo.getIdBusqueda())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Busqueda busqueda = busquedaDao.find(Long.valueOf(busquedaTipo.getIdBusqueda()));

        if (busqueda == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontó un idBusqueda con el valor:" + busquedaTipo.getIdBusqueda()));
            throw new AppException(errorTipoList);
        }

        busqueda.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateBusqueda(busquedaTipo, busqueda, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        busquedaDao.edit(busqueda);
    }

    private void populateBusqueda(BusquedaTipo busquedaTipo, Busqueda busqueda, List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isNotBlank(busquedaTipo.getCodTipoBusqueda())) {
            ValorDominio codTipoBusqueda = valorDominioDao.find(busquedaTipo.getCodTipoBusqueda());
            if (codTipoBusqueda == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoBusqueda proporcionado no existe con el ID: " + busquedaTipo.getCodTipoBusqueda()));
            } else {
                busqueda.setCodTipoBusqueda(codTipoBusqueda);
            }
        }
        if (busquedaTipo.getFecProgramadaVisita() != null) {
            busqueda.setFechaProgVisita(busquedaTipo.getFecProgramadaVisita());
        }

        if ((busquedaTipo.getFuncionarioEncargado() != null) && (StringUtils.isNotBlank(busquedaTipo.getFuncionarioEncargado().getIdFuncionario()))) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(busquedaTipo.getFuncionarioEncargado(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Debe proporcionar un Solicitante (Funcionario) existente, ID: "
                        + busquedaTipo.getFuncionarioEncargado().getIdFuncionario()));
            } else {
                busqueda.setValUsuario(funcionarioTipo.getIdFuncionario());
            }
        }
        if (StringUtils.isNotBlank(busquedaTipo.getDesObservacionProgramacionVisita())) {
            busqueda.setDesObservacionProgVisita(busquedaTipo.getDesObservacionProgramacionVisita());
        }

        if (busquedaTipo.getAutoComisorio() != null
                && StringUtils.isNotBlank(busquedaTipo.getAutoComisorio().getIdDocumento())) {
            final DocumentoEcm documentoEcm = documentoDao.find(String.valueOf(busquedaTipo.getAutoComisorio().getIdDocumento()));
            if (documentoEcm == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El campo autoComisorio proporcionado no existe con el ID: " + busquedaTipo.getAutoComisorio().getIdDocumento()));
            } else {
                busqueda.setAutoComisorio(documentoEcm);
            }
        }

        if (StringUtils.isNotBlank(busquedaTipo.getCodResultadoBusqueda())) {
            ValorDominio codResultadoBusqueda = valorDominioDao.find(busquedaTipo.getCodResultadoBusqueda());
            if (codResultadoBusqueda == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codResultadoBusqueda proporcionado no existe con el ID: " + busquedaTipo.getCodResultadoBusqueda()));
            } else {
                busqueda.setCodResultadoBusqeda(codResultadoBusqueda);
            }
        }

        if (StringUtils.isNotBlank(busquedaTipo.getDescObservaciones())) {
            busqueda.setDescObservaciones(busquedaTipo.getDescObservaciones());
        }

        if (StringUtils.isNotBlank(busquedaTipo.getValPlazoEntrega())) {
            busqueda.setValPlazoEntrega(busquedaTipo.getValPlazoEntrega());
        }

        if (StringUtils.isNotBlank(busquedaTipo.getValCargoPersonaContactada())) {
            busqueda.setValCargoPersonaContactada(busquedaTipo.getValCargoPersonaContactada());
        }

        if (StringUtils.isNotBlank(busquedaTipo.getValPersonaContactada())) {
            busqueda.setValPersonaContactada(busquedaTipo.getValPersonaContactada());
        }
        
        
        if (StringUtils.isNotBlank(busquedaTipo.getValTelefonoAportante())) {
            busqueda.setValTelefonoAportante(busquedaTipo.getValTelefonoAportante());
        }
        
        if (StringUtils.isNotBlank(busquedaTipo.getValEtapa())) {
            busqueda.setValEtapa(busquedaTipo.getValEtapa());
        }
                
        if (StringUtils.isNotBlank(busquedaTipo.getValNombreFuncionario())) {
            busqueda.setValNombreFuncionario(busquedaTipo.getValNombreFuncionario());
        }
                        
        if (StringUtils.isNotBlank(busquedaTipo.getValExtension())) {
            busqueda.setValExtension(busquedaTipo.getValExtension());
        }
        

    }
}
