package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface FiscalizacionFacade extends Serializable {

    Long crearFiscalizacion(FiscalizacionTipo fiscalizacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<FiscalizacionTipo> buscarPorCriteriosFiscalizacion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<FiscalizacionTipo> buscarPorIdFiscalizacion(List<String> idFiscalizacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarFiscalizacion(FiscalizacionTipo fiscalizacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
