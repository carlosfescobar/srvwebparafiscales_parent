package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.enums.OrigenProcesoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.BusquedaDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionBusquedaDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionIncumplimientoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionValidacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Busqueda;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionAccion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionArchivoTemp;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionBusqueda;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionIncumplimiento;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionCreacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz FiscalizacionFacade que contiene las
 * operaciones del servicio SrvAplFiscalizacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class FiscalizacionFacadeImpl extends AbstractFacade implements FiscalizacionFacade {

    @EJB
    private FiscalizacionDao fiscalizacionDao;

    @EJB
    private DenunciaDao denunciaDao;

    @EJB
    private FiscalizacionBusquedaDao fiscalizacionBusquedaDao;

    @EJB
    private FiscalizacionIncumplimientoDao fiscalizacionIncumplimientoDao;

    @EJB
    private BusquedaDao busquedaDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private ExpedienteFacade expedienteFacade;

    @EJB
    private ArchivoDao archivoDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @Override
    public Long crearFiscalizacion(FiscalizacionTipo fiscalizacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (fiscalizacionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkFiscalizacion(fiscalizacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Fiscalizacion fiscalizacion = new Fiscalizacion();

        checkBusinessFiscalizacion(fiscalizacion, fiscalizacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        fiscalizacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateFiscalizacion(fiscalizacionTipo, fiscalizacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idFiscalizacion = fiscalizacionDao.create(fiscalizacion);
        return idFiscalizacion;
    }

    @Override
    public PagerData<FiscalizacionTipo> buscarPorCriteriosFiscalizacion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final PagerData<Fiscalizacion> pagerDataEntity = fiscalizacionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<FiscalizacionTipo> fiscalizacionTipoList = mapper.map(pagerDataEntity.getData(),
                Fiscalizacion.class, FiscalizacionTipo.class);

        if (fiscalizacionTipoList != null) {
            for (final FiscalizacionTipo fiscalizacionTipo : fiscalizacionTipoList) {
                if (fiscalizacionTipo.getFiscalizador() != null
                        && StringUtils.isNotBlank(fiscalizacionTipo.getFiscalizador().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(fiscalizacionTipo.getFiscalizador(), contextoTransaccionalTipo);
                    fiscalizacionTipo.setFiscalizador(funcionarioTipo);
                }
                if (fiscalizacionTipo.getBusquedas() != null) {
                    for (BusquedaTipo busquedaTipo : fiscalizacionTipo.getBusquedas()) {
                        if (busquedaTipo.getFuncionarioEncargado() != null
                                && StringUtils.isNotBlank(busquedaTipo.getFuncionarioEncargado().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(busquedaTipo.getFuncionarioEncargado(), contextoTransaccionalTipo);
                            busquedaTipo.setFuncionarioEncargado(funcionarioTipo);
                        }
                    }
                }
            }
        }
        return new PagerData<FiscalizacionTipo>(fiscalizacionTipoList, pagerDataEntity.getNumPages());
    }

    @Override
    public void actualizarFiscalizacion(FiscalizacionTipo fiscalizacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (fiscalizacionTipo == null || StringUtils.isBlank(fiscalizacionTipo.getIdFiscalizacion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Fiscalizacion fiscalizacion = fiscalizacionDao.find(Long.valueOf(fiscalizacionTipo.getIdFiscalizacion()));

        if (fiscalizacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un IdFiscalizacion con el valor:" + fiscalizacionTipo.getIdFiscalizacion()));
            throw new AppException(errorTipoList);
        }

        checkBusinessFiscalizacion(fiscalizacion, fiscalizacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        fiscalizacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateFiscalizacion(fiscalizacionTipo, fiscalizacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        fiscalizacionDao.edit(fiscalizacion);
    }

    @Override
    public List<FiscalizacionTipo> buscarPorIdFiscalizacion(List<String> idFiscalizacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idFiscalizacionTipos == null || idFiscalizacionTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idFiscalizacionTipos, String.class, Long.class);

        final List<Fiscalizacion> fiscalizacionList = fiscalizacionDao.findByIdList(ids);

        final List<FiscalizacionTipo> fiscalizacionTipoList
                = mapper.map(fiscalizacionList, Fiscalizacion.class, FiscalizacionTipo.class);

        if (fiscalizacionTipoList != null) {
            for (final FiscalizacionTipo fiscalizacionTipo : fiscalizacionTipoList) {
                if (fiscalizacionTipo.getFiscalizador() != null
                        && StringUtils.isNotBlank(fiscalizacionTipo.getFiscalizador().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(fiscalizacionTipo.getFiscalizador(), contextoTransaccionalTipo);
                    fiscalizacionTipo.setFiscalizador(funcionarioTipo);
                }
                if (fiscalizacionTipo.getBusquedas() != null) {
                    for (BusquedaTipo busquedaTipo : fiscalizacionTipo.getBusquedas()) {
                        if (busquedaTipo.getFuncionarioEncargado() != null
                                && StringUtils.isNotBlank(busquedaTipo.getFuncionarioEncargado().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(busquedaTipo.getFuncionarioEncargado(), contextoTransaccionalTipo);
                            busquedaTipo.setFuncionarioEncargado(funcionarioTipo);
                        }
                    }
                }
            }
        }
        return fiscalizacionTipoList;
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad Fiscalizacion
     *
     * @param fiscalizacionTipo Objeto origne
     * @param fiscalizacion Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateFiscalizacion(final FiscalizacionTipo fiscalizacionTipo, final Fiscalizacion fiscalizacion, final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdLiquidador())) {
            fiscalizacion.setIdLiquidador(fiscalizacionTipo.getIdLiquidador());
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdRadicadoOficioInconsistencias())) {
            fiscalizacion.setIdRadicadoOficioInconsistencias(fiscalizacionTipo.getIdRadicadoOficioInconsistencias());
        }

        if (fiscalizacionTipo.getFecRadicadoOficioInconsistencias() != null) {
            fiscalizacion.setFecRadicadoOficioInconsistencias(fiscalizacionTipo.getFecRadicadoOficioInconsistencias());
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getEsEnvioComite())) {
            fiscalizacion.setEsEnvioComite(Boolean.valueOf(fiscalizacionTipo.getEsEnvioComite()));
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getEsMenorCuantia())) {
            fiscalizacion.setEsMenorCuantia(Boolean.valueOf(fiscalizacionTipo.getEsMenorCuantia()));
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getEsContinuadoProceso())) {
            fiscalizacion.setEsContinuadoProceso(Boolean.valueOf(fiscalizacionTipo.getEsContinuadoProceso()));
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getDescObservacionesContinuadoProceso())) {
            fiscalizacion.setDescObservacionesContinuadoProceso(fiscalizacionTipo.getDescObservacionesContinuadoProceso());
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getEsOmisoPension())) {
            fiscalizacion.setEsOmisoPension(Boolean.valueOf(fiscalizacionTipo.getEsOmisoPension()));
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getEsMasActuaciones())) {
            fiscalizacion.setEsMasActuaciones(Boolean.valueOf(fiscalizacionTipo.getEsMasActuaciones()));
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getDescObservacionesMasActuaciones())) {
            fiscalizacion.setDescObservacionesMasActuaciones(fiscalizacionTipo.getDescObservacionesMasActuaciones());
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getDescObservacionesMecanismoConseguir())) {
            fiscalizacion.setDescObservacionesMecanismoConseguir(fiscalizacionTipo.getDescObservacionesMecanismoConseguir());
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getValLineaAccion())) {
            fiscalizacion.setValLineaAccion(fiscalizacionTipo.getValLineaAccion());
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getValOrigenProceso())) {
            fiscalizacion.setValOrigenProceso(fiscalizacionTipo.getValOrigenProceso());
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getValNombrePrograma())) {
            fiscalizacion.setValNombreprograma(fiscalizacionTipo.getValNombrePrograma());
        }

        //Validacion adicional, si no se envía el expediente, puede que si se quieran cambiar los documentos asociados al expediente
        if ((fiscalizacion.getExpediente() != null && StringUtils.isNotBlank(fiscalizacion.getExpediente().getId()))
                && (fiscalizacionTipo.getExpediente() == null || StringUtils.isBlank(fiscalizacionTipo.getExpediente().getIdNumExpediente()))) {

            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(fiscalizacion.getExpediente().getId());
            fiscalizacionTipo.setExpediente(expedienteTipo);
        }

        if (fiscalizacionTipo.getExpediente() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getExpediente().getIdNumExpediente())) {

            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos
                    = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);

            List<DocumentoTipo> documentosxOrigen;

            if (StringUtils.isNotBlank(fiscalizacionTipo.getIdDocumentoAfiliacionCalculoActuarial())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoAfiliacionCalculoActuarial = new DocumentoTipo();
                documentoAfiliacionCalculoActuarial.setIdDocumento(fiscalizacionTipo.getIdDocumentoAfiliacionCalculoActuarial());
                documentosxOrigen.add(documentoAfiliacionCalculoActuarial);
                documentos.put(OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_AFIL_CALC_ACTUA, documentosxOrigen);
            }
            if (StringUtils.isNotBlank(fiscalizacionTipo.getIdDocumentoCalculoActuarial())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoCalculoActuarial = new DocumentoTipo();
                documentoCalculoActuarial.setIdDocumento(fiscalizacionTipo.getIdDocumentoCalculoActuarial());
                documentosxOrigen.add(documentoCalculoActuarial);
                documentos.put(OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_CAL_ACTUAR, documentosxOrigen);
            }
            if (StringUtils.isNotBlank(fiscalizacionTipo.getIdDocumentoHojaTrabajo())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoHojaTrabajo = new DocumentoTipo();
                documentoHojaTrabajo.setIdDocumento(fiscalizacionTipo.getIdDocumentoHojaTrabajo());
                documentosxOrigen.add(documentoHojaTrabajo);
                documentos.put(OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_HOJA_TRABAJO, documentosxOrigen);
            }
            Expediente expediente = expedienteFacade.persistirExpedienteDocumento(fiscalizacionTipo.getExpediente(),
                    documentos, errorTipoList, true, contextoTransaccionalTipo);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                fiscalizacion.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdDocumentoAutoComisorio())) {
            ActoAdministrativo autoComisorio = actoAdministrativoDao.find(fiscalizacionTipo.getIdDocumentoAutoComisorio());
            if (autoComisorio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró autoComisorio con el ID: " + fiscalizacionTipo.getIdDocumentoAutoComisorio()));
            } else {
                fiscalizacion.setActoAutoComisorio(autoComisorio);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodCausalCierre())) {
            final ValorDominio codCausalCierre = valorDominioDao.find(fiscalizacionTipo.getCodCausalCierre());
            if (codCausalCierre == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codCausalCierre con el ID: " + fiscalizacionTipo.getCodCausalCierre()));
            } else {
                fiscalizacion.setCodCausalCierre(codCausalCierre);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodEstadoFiscalizacion())) {
            final ValorDominio codEstadoFiscalizacion = valorDominioDao.find(fiscalizacionTipo.getCodEstadoFiscalizacion());
            if (codEstadoFiscalizacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codEstadoFiscalizacion con el ID: " + fiscalizacionTipo.getCodEstadoFiscalizacion()));
            } else {
                fiscalizacion.setCodEstadoFiscalizacion(codEstadoFiscalizacion);
            }
        }

        if (fiscalizacionTipo.getDenuncia() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getDenuncia().getIdDenuncia())) {
            final Denuncia denuncia = denunciaDao.find(Long.valueOf(fiscalizacionTipo.getDenuncia().getIdDenuncia()));
            if (denuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una Denuncia con el ID: " + fiscalizacionTipo.getDenuncia().getIdDenuncia()));
            } else {
                fiscalizacion.setDenuncia(denuncia);
            }
        }

        if (fiscalizacionTipo.getPeriodoFiscalizacion() != null) {
            if (fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio().
                    after(fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "La Fecha Fin del periodo de la Fiscalización debe ser mayor a la Fecha de Inicio"));
            } else {
                fiscalizacion.setFecInicio(fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio());
                fiscalizacion.setFecFin(fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin());
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdActoRequerimientoInformacion())) {
            final ActoAdministrativo actoRequerimientoInformacion
                    = actoAdministrativoDao.find(fiscalizacionTipo.getIdActoRequerimientoInformacion());
            if (actoRequerimientoInformacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró requerimientoInformacion con el ID: " + fiscalizacionTipo.getIdActoRequerimientoInformacion()));
            } else {
                fiscalizacion.setActoRequerimientoInformacion(actoRequerimientoInformacion);
            }
        }

        if (fiscalizacionTipo.getAutoArchivoParcial() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getAutoArchivoParcial().getIdArchivo())) {
            final Archivo archivo = archivoDao.find(Long.valueOf(fiscalizacionTipo.getAutoArchivoParcial().getIdArchivo()));
            if (archivo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un archivo con el ID: " + fiscalizacionTipo.getAutoArchivoParcial().getIdArchivo()));
            } else {
                AprobacionDocumento aprobacionDocumento;
                aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(fiscalizacionTipo.getAutoArchivoParcial().getIdArchivo()));
                if (aprobacionDocumento != null) {
                    throw new AppException("Ya existe un aprobacionDocumento con el idArchivo: " + fiscalizacionTipo.getAutoArchivoParcial().getIdArchivo());

                }
                aprobacionDocumento = new AprobacionDocumento();
                aprobacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                aprobacionDocumento.setArchivo(archivo);

                if (fiscalizacionTipo.getAutoArchivoParcial().getValidacionCreacionDocumento() != null
                        && !fiscalizacionTipo.getAutoArchivoParcial().getValidacionCreacionDocumento().isEmpty()) {

                    for (ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo : fiscalizacionTipo.getAutoArchivoParcial().getValidacionCreacionDocumento()) {

                        final ValidacionCreacionDocumento validacionCreacionDocumento = mapper.map(validacionCreacionDocumentoTipo, ValidacionCreacionDocumento.class);
                        validacionCreacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        AprobacionValidacionDocumento aprobacionValidacionDocumento = new AprobacionValidacionDocumento();
                        aprobacionValidacionDocumento.setAprobacionDocumento(aprobacionDocumento);
                        aprobacionValidacionDocumento.setIdValidacionCreacionDocumento(validacionCreacionDocumento);
                        aprobacionValidacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        aprobacionDocumento.getValidacionCreacionDocumento().add(aprobacionValidacionDocumento);
                    }
                }
                fiscalizacion.setAutoArchivoParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdActoAutoArchivo())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(fiscalizacionTipo.getIdActoAutoArchivo()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró actoAutoArchivo con el ID: " + fiscalizacionTipo.getIdActoAutoArchivo()));
            } else {
                fiscalizacion.setActoAutoArchivo(actoAdministrativo);
            }
        }

        if (fiscalizacionTipo.getCodIncumplimiento() != null
                && !fiscalizacionTipo.getCodIncumplimiento().isEmpty()) {
            FiscalizacionIncumplimiento fiscalizacionHasIncumplimiento;
            for (String codIncumplimento : fiscalizacionTipo.getCodIncumplimiento()) {
                if (StringUtils.isNotBlank(codIncumplimento)) {
                    ValorDominio valorDominio = valorDominioDao.find(codIncumplimento);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codIncumplimento con el ID: " + codIncumplimento));
                    } else if (fiscalizacion.getId() != null) {
                        fiscalizacionHasIncumplimiento = fiscalizacionIncumplimientoDao.findByFiscalizacionAndCodIncumplimiento(fiscalizacion, valorDominio);
                        if (fiscalizacionHasIncumplimiento != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre codIncumplimiento: " + valorDominio.getId() + ", y la fiscalizacion:" + fiscalizacion.getId()));
                        } else {
                            fiscalizacionHasIncumplimiento = crearFiscalizacionHasIncumplimiento(valorDominio, fiscalizacion, contextoTransaccionalTipo);
                            fiscalizacion.getCodIncumplimiento().add(fiscalizacionHasIncumplimiento);
                        }
                    } else {
                        fiscalizacionHasIncumplimiento = crearFiscalizacionHasIncumplimiento(valorDominio, fiscalizacion, contextoTransaccionalTipo);
                        fiscalizacion.getCodIncumplimiento().add(fiscalizacionHasIncumplimiento);
                    }
                }
            }
        }

        if (fiscalizacionTipo.getRequerimientoDeclararCorregirParcial() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getIdArchivo())) {

            final Archivo archivo = archivoDao.find(Long.valueOf(fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getIdArchivo()));
            if (archivo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un archivo con el ID: " + fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getIdArchivo()));
            } else {
                AprobacionDocumento aprobacionDocumento;
                aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getIdArchivo()));
                if (aprobacionDocumento != null) {
                    throw new AppException("Ya existe un aprobacionDocumento con el idArchivo: " + fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getIdArchivo());
                }
                aprobacionDocumento = new AprobacionDocumento();
                aprobacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                aprobacionDocumento.setArchivo(archivo);

                if (fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getValidacionCreacionDocumento() != null
                        && !fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getValidacionCreacionDocumento().isEmpty()) {

                    for (ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo : fiscalizacionTipo.getRequerimientoDeclararCorregirParcial().getValidacionCreacionDocumento()) {

                        final ValidacionCreacionDocumento validacionCreacionDocumento = mapper.map(validacionCreacionDocumentoTipo, ValidacionCreacionDocumento.class);
                        validacionCreacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        AprobacionValidacionDocumento aprobacionValidacionDocumento = new AprobacionValidacionDocumento();
                        aprobacionValidacionDocumento.setAprobacionDocumento(aprobacionDocumento);
                        aprobacionValidacionDocumento.setIdValidacionCreacionDocumento(validacionCreacionDocumento);
                        aprobacionValidacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        aprobacionDocumento.getValidacionCreacionDocumento().add(aprobacionValidacionDocumento);
                    }
                }
                fiscalizacion.setRequerimientoDeclararCorregirParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdActoRequerimientoDeclararCorregir())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(fiscalizacionTipo.getIdActoRequerimientoDeclararCorregir()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró actoRequerimientoDeclararCorregir con el ID: " + fiscalizacionTipo.getIdActoRequerimientoDeclararCorregir()));
            } else {
                fiscalizacion.setActorequerimientoDeclararCorregir(actoAdministrativo);
            }
        }

        if (fiscalizacionTipo.getBusquedas() != null
                && !fiscalizacionTipo.getBusquedas().isEmpty()) {
            FiscalizacionBusqueda fiscalizacionHasBuqueda;
            for (BusquedaTipo busquedaTipo : fiscalizacionTipo.getBusquedas()) {
                if (busquedaTipo != null
                        && StringUtils.isNotBlank(busquedaTipo.getIdBusqueda())) {
                    Busqueda busqueda = busquedaDao.find(Long.parseLong(busquedaTipo.getIdBusqueda()));
                    if (busqueda == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se contró una busqueda con el ID: " + busquedaTipo.getIdBusqueda()));
                    } else if (fiscalizacion.getId() != null) {
                        
                        
                        fiscalizacionHasBuqueda = fiscalizacionBusquedaDao.findByFiscalizacionAndBusqueda(fiscalizacion, busqueda);
                        

                        if (StringUtils.isNotBlank(busquedaTipo.getValTelefonoAportante())) {
                            busqueda.setValTelefonoAportante(busquedaTipo.getValTelefonoAportante());
                        }

                        if (StringUtils.isNotBlank(busquedaTipo.getValEtapa())) {
                            busqueda.setValEtapa(busquedaTipo.getValEtapa());
                        }

                        if (StringUtils.isNotBlank(busquedaTipo.getValNombreFuncionario())) {
                            busqueda.setValNombreFuncionario(busquedaTipo.getValNombreFuncionario());
                        }

                        if (StringUtils.isNotBlank(busquedaTipo.getValExtension())) {
                            busqueda.setValExtension(busquedaTipo.getValExtension());
                        }
                        
                        
                        
                        if (fiscalizacionHasBuqueda != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre la búsqueda: " + busqueda.getId() + ", y la fiscalizacion con el ID:" + fiscalizacion.getId()));
                        } else {
                            fiscalizacionHasBuqueda = crearFiscalizacionHasHasBusqueda(busqueda, fiscalizacion, contextoTransaccionalTipo);
                            fiscalizacion.getBusquedas().add(fiscalizacionHasBuqueda);
                        }
                    } else {
                        fiscalizacionHasBuqueda = crearFiscalizacionHasHasBusqueda(busqueda, fiscalizacion, contextoTransaccionalTipo);
                        fiscalizacion.getBusquedas().add(fiscalizacionHasBuqueda);
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodMecanismoConseguirInformacion())) {
            ValorDominio codMecanismoConseguirInformacion = valorDominioDao.find(fiscalizacionTipo.getCodMecanismoConseguirInformacion());
            if (codMecanismoConseguirInformacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codMecanismoConseguirInformacion con el ID: " + fiscalizacionTipo.getCodMecanismoConseguirInformacion()));
            } else {
                fiscalizacion.setCodMecanismoConseguirInformacion(codMecanismoConseguirInformacion);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodLineaAccion())) {
            ValorDominio codLineaAccion = valorDominioDao.find(fiscalizacionTipo.getCodLineaAccion());
            if (codLineaAccion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codLineaAccion proporcionado con el ID: " + fiscalizacionTipo.getCodLineaAccion()));
            } else {
                fiscalizacion.setCodLineaAccion(codLineaAccion);
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodOrigenProceso())) {
            ValorDominio codOrigenProceso = valorDominioDao.find(fiscalizacionTipo.getCodOrigenProceso());
            if (codOrigenProceso == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codOrigenproceso con el ID: " + fiscalizacionTipo.getCodOrigenProceso()));
            } else {
                fiscalizacion.setCodOrigenProceso(codOrigenProceso);
            }
        }
        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdActoInspeccionTributaria())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(fiscalizacionTipo.getIdActoInspeccionTributaria()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró actoInspeccionTributaria con el ID: " + fiscalizacionTipo.getIdActoInspeccionTributaria()));
            } else {
                fiscalizacion.setActoinspeccionTributaria(actoAdministrativo);
            }

        }

        if (fiscalizacionTipo.getFiscalizador() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getFiscalizador().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(fiscalizacionTipo.getFiscalizador(), contextoTransaccionalTipo);
            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró Fiscalizar (Funcionario) existente, con el ID: " + fiscalizacionTipo.getFiscalizador().getIdFuncionario()));
            } else {
                fiscalizacion.setFiscalizador(funcionarioTipo.getIdFuncionario());
            }
        }

        if (fiscalizacionTipo.getAcciones() != null
                && !fiscalizacionTipo.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : fiscalizacionTipo.getAcciones()) {
                final Accion accion = mapper.map(accionTipo, Accion.class);
                accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = valorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        accion.setCodAccion(codAccion);
                    }
                }
                FiscalizacionAccion fiscalizacionHasAccion = new FiscalizacionAccion();
                fiscalizacionHasAccion.setAccion(accion);
                fiscalizacionHasAccion.setFiscalizacion(fiscalizacion);
                fiscalizacionHasAccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                fiscalizacion.getAcciones().add(fiscalizacionHasAccion);
            }
        }

        if (fiscalizacionTipo.getAportante() != null) {
            if (fiscalizacionTipo.getAportante().getPersonaJuridica() != null
                    && fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                    && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un Aportante con la identificación :" + identificacion));
                } else {
                    fiscalizacion.setAportante(aportante);
                }
            } else if (fiscalizacionTipo.getAportante().getPersonaNatural() != null
                    && fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona() != null
                    && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un Aportante con la identificación :" + identificacion));
                } else {
                    fiscalizacion.setAportante(aportante);
                }
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getIdDocumentoRequerimientoAclaraciones())) {
            ActoAdministrativo idDocumentoRequerimientoAclaraciones = actoAdministrativoDao.find(fiscalizacionTipo.getIdDocumentoRequerimientoAclaraciones());
            if (idDocumentoRequerimientoAclaraciones == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "no se encontro un documentoRequerimientoAclaraciones con el id: " + fiscalizacionTipo.getIdDocumentoRequerimientoAclaraciones()));
            } else {
                fiscalizacion.setIdDocumentoRequerimientoAclaraciones(idDocumentoRequerimientoAclaraciones);
            }
        }
        if (fiscalizacionTipo.getArchivosTemporales() != null
                && !fiscalizacionTipo.getArchivosTemporales().isEmpty()) {
            for (ParametroValoresTipo archivoTemporal : fiscalizacionTipo.getArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivoTemporal.getIdLlave())) {
                    final ValorDominio codTipoAnexo = valorDominioDao.find(archivoTemporal.getIdLlave());
                    if (codTipoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoAnexo con el ID: " + archivoTemporal.getIdLlave()));
                    } else {
                        for (ParametroTipo archivo : archivoTemporal.getValValor()) {
                            if (StringUtils.isNotBlank(archivo.getIdLlave())) {
                                Archivo anexo = archivoDao.find(Long.valueOf(archivo.getIdLlave()));
                                if (anexo == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró Archivo con el ID: " + archivo.getIdLlave()));
                                } else {
                                    FiscalizacionArchivoTemp fiscalizacionArchivoTemp = new FiscalizacionArchivoTemp();
                                    fiscalizacionArchivoTemp.setFiscalizacion(fiscalizacion);
                                    fiscalizacionArchivoTemp.setArchivo(anexo);
                                    fiscalizacionArchivoTemp.setCodTipoAnexo(codTipoAnexo);
                                    fiscalizacionArchivoTemp.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                    fiscalizacion.getArchivosTemporales().add(fiscalizacionArchivoTemp);
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoAnexo")));
                }
            }
        }

        if (StringUtils.isNotBlank(fiscalizacionTipo.getDescGestionPersuasiva())) {
            fiscalizacion.setDescGestionPersuasiva(fiscalizacionTipo.getDescGestionPersuasiva());
        }

    }

    private FiscalizacionIncumplimiento crearFiscalizacionHasIncumplimiento(ValorDominio valorDominio, Fiscalizacion fiscalizacion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        FiscalizacionIncumplimiento fiscalizacionHasIncumplimiento = new FiscalizacionIncumplimiento();
        fiscalizacionHasIncumplimiento.setCodIncumplimiento(valorDominio);
        fiscalizacionHasIncumplimiento.setFiscalizacion(fiscalizacion);
        fiscalizacionHasIncumplimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return fiscalizacionHasIncumplimiento;
    }

    private FiscalizacionBusqueda crearFiscalizacionHasHasBusqueda(Busqueda busqueda, Fiscalizacion fiscalizacion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        FiscalizacionBusqueda fiscalizacionHasBuqueda = new FiscalizacionBusqueda();
        fiscalizacionHasBuqueda.setBusqueda(busqueda);
        fiscalizacionHasBuqueda.setFiscalizacion(fiscalizacion);
        fiscalizacionHasBuqueda.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return fiscalizacionHasBuqueda;
    }

    /**
     * Método que valida las restricciones únicas de la tabla fiscalizacion
     *
     * @param fiscalizacion la fiscalización a crear/actualizar
     * @param fiscalizacionTipo la fiscalización que contiene los datos a
     * ingresar
     * @param errorTipoList la lista de errores que se almacenan en toda la
     * transacción
     */
    private void checkBusinessFiscalizacion(final Fiscalizacion fiscalizacion,
            final FiscalizacionTipo fiscalizacionTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        //Si se va a cambiar el expediente, se valida primero que el nuevo expediente no exista en otra fiscalización
        if (fiscalizacionTipo.getExpediente() != null
                && StringUtils.isNotBlank(fiscalizacionTipo.getExpediente().getIdNumExpediente())) {
            if (fiscalizacion.getExpediente() != null) {
                if (!fiscalizacion.getExpediente().getId().equals(fiscalizacionTipo.getExpediente().getIdNumExpediente())) {
                    Fiscalizacion fiscalizacionOtroExpediente = fiscalizacionDao.findFiscalizacionByExpediente(fiscalizacionTipo.getExpediente().getIdNumExpediente());
                    if (fiscalizacionOtroExpediente != null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "La fiscalización cod ID: " + fiscalizacionOtroExpediente.getId().toString() + ", ya está tiene asociado el expediente con ID :" + fiscalizacionTipo.getExpediente().getIdNumExpediente()));
                    }
                }
            } else {
                Fiscalizacion fiscalizacionOtroExpediente = fiscalizacionDao.findFiscalizacionByExpediente(fiscalizacionTipo.getExpediente().getIdNumExpediente());
                if (fiscalizacionOtroExpediente != null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La fiscalización cod ID: " + fiscalizacionOtroExpediente.getId().toString() + ", ya está tiene asociado el expediente con ID :" + fiscalizacionTipo.getExpediente().getIdNumExpediente()));
                }
            }
        }

        /**
         * Se valida el codOrigenProceso en el siguiente escenario: Se actualiza
         * o crea la fiscalización con el codOrigenProceso: DENUNCIA
         */
        if ((StringUtils.isNotBlank(fiscalizacionTipo.getCodOrigenProceso())
                && OrigenProcesoEnum.DENUNCIA.getCode().equals(fiscalizacionTipo.getCodOrigenProceso()))
                || (StringUtils.isBlank(fiscalizacionTipo.getCodOrigenProceso())
                && fiscalizacion != null && fiscalizacion.getCodOrigenProceso() != null
                && OrigenProcesoEnum.DENUNCIA.getCode().equals(fiscalizacion.getCodOrigenProceso().getId()))) {

            //determina si se deben aplicar las validaciones de negocio
            boolean validarExpediente = false;
            boolean validarAportante = false;
            boolean validarDenuncia = false;
            boolean validarPeriodoFiscalizacion = false;
            //Variables que se van a utilizar en la validación
            ExpedienteTipo expedienteTipoFiscalizacion = null;
            DenunciaTipo denunciaTipoFiscalizacion = null;
            RangoFechaTipo periodoFiscalizacion = null;
            Denuncia denunciaFiscalizacion = null;
            Expediente expedienteFiscalizacion = null;

            //Asignaciòn del denunciaTipo
            if (fiscalizacionTipo.getDenuncia() != null
                    && StringUtils.isNotBlank(fiscalizacionTipo.getDenuncia().getIdDenuncia())) {
                denunciaFiscalizacion = denunciaDao.find(Long.valueOf(fiscalizacionTipo.getDenuncia().getIdDenuncia()));
                validarDenuncia = true;
            } else if (fiscalizacion != null && fiscalizacion.getDenuncia() != null) {
                denunciaFiscalizacion = fiscalizacion.getDenuncia();
            }
            if (denunciaFiscalizacion != null) {
                denunciaTipoFiscalizacion = mapper.map(denunciaFiscalizacion, DenunciaTipo.class);
            }
            //Asignación del expediente
            if (fiscalizacionTipo.getExpediente() != null
                    && StringUtils.isNotBlank(fiscalizacionTipo.getExpediente().getIdNumExpediente())) {
                expedienteFiscalizacion = expedienteDao.find(fiscalizacionTipo.getExpediente().getIdNumExpediente());
                validarExpediente = true;
            } else if (fiscalizacion != null && fiscalizacion.getExpediente() != null) {
                expedienteFiscalizacion = fiscalizacion.getExpediente();
            }
            if (expedienteFiscalizacion != null) {
                expedienteTipoFiscalizacion = mapper.map(expedienteFiscalizacion, ExpedienteTipo.class);
            }
            //Asignación del rangoFechaTipo
            if (fiscalizacionTipo.getPeriodoFiscalizacion() != null
                    && fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio() != null
                    && fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin() != null) {
                periodoFiscalizacion = fiscalizacionTipo.getPeriodoFiscalizacion();
                validarPeriodoFiscalizacion = true;
            } else if (fiscalizacion != null && fiscalizacion.getFecInicio() != null
                    && fiscalizacion.getFecFin() != null) {
                periodoFiscalizacion = new RangoFechaTipo();
                periodoFiscalizacion.setFecInicio(fiscalizacion.getFecInicio());
                periodoFiscalizacion.setFecFin(fiscalizacion.getFecFin());
            }
            //Asignaciòn del aportanteTipo
            if (fiscalizacionTipo.getAportante() != null) {
                IdentificacionTipo identificacionAportante = Util.obtenerIdentificacionTipoFromChoice(fiscalizacionTipo.getAportante().getPersonaNatural(),
                        fiscalizacionTipo.getAportante().getPersonaJuridica());
                if (identificacionAportante != null) {
                    validarAportante = true;
                }
            }
            if (denunciaTipoFiscalizacion != null) {
                //Se realizan las validaciones de negocio correspondientes
                if (validarDenuncia || validarExpediente) {
                    if (expedienteTipoFiscalizacion != null
                            && !denunciaTipoFiscalizacion.getExpediente().getIdNumExpediente().equals(expedienteTipoFiscalizacion.getIdNumExpediente())) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "El Expediente de la Denuncia asociada a la Fiscalización :" + denunciaTipoFiscalizacion.getExpediente().getIdNumExpediente()
                                + ", es diferente al Expediente asociado a la Fiscalización: " + expedienteTipoFiscalizacion.getIdNumExpediente()));
                    }
                }
                if (validarDenuncia || validarAportante) {
                    if (!Util.aportanteEquals(fiscalizacionTipo.getAportante(), denunciaTipoFiscalizacion.getAportante())) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "El Aportante de la Denuncia asociada a la Fiscalización y el Aportante asociado a la Fiscalización son diferentes"));
                    }
                }
                if (validarDenuncia || validarPeriodoFiscalizacion) {
                    if (!Util.rangoFechaEquals(periodoFiscalizacion, denunciaTipoFiscalizacion.getFecInicioPeriodo(), denunciaTipoFiscalizacion.getFecFinPeriodo())) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "El periodo de la Denuncia asociada a la Fiscalización es diferente al periodo de la Fiscalización"));
                    }
                }
            }
        }

        //Se entra a validar que el aportante no tenga mas de una fiscalización asociada
        // en el mismo periodo
        if (fiscalizacionTipo.getAportante() != null
                || (fiscalizacionTipo.getPeriodoFiscalizacion() != null
                && fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio() != null
                && fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin() != null)) {

            //Objetos con los cuales se consultan los periodos
            RangoFechaTipo rangoFechaTipo;

            //Se valida el primer caso, se va a ingresar un nuevo aportante y un nuevo periodo
            if (fiscalizacionTipo.getAportante() != null
                    && fiscalizacionTipo.getPeriodoFiscalizacion() != null) {

                IdentificacionTipo identificacionTipo = null;

                if (fiscalizacionTipo.getAportante().getPersonaJuridica() != null
                        && fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                        && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                    identificacionTipo = fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona();

                } else if (fiscalizacionTipo.getAportante().getPersonaNatural() != null
                        && fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona() != null
                        && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                    identificacionTipo = fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona();
                }
                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                
                if (fiscalizacionTipo.getPeriodoFiscalizacion() != null) {
                    rangoFechaTipo = Util.aproximarPeriodo(fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio(), fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin());
                    List<Fiscalizacion> fiscalizacionPeriodoAportante
                            = fiscalizacionDao.findFiscalizacionByAportanteAndPeriodo(identificacion, rangoFechaTipo.getFecInicio(), rangoFechaTipo.getFecFin());

                    if (fiscalizacionPeriodoAportante != null
                            && !fiscalizacionPeriodoAportante.isEmpty()) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El aportante con la identificación :" + identificacion + ", ya tiene asociada una fiscalización en el periodo ingresado"));
                    }
                }

                //Se valida el segundo caso, solo se va a cambiar el aportante, por lo que se envía el periodo de la   
                //fiscalización que está creada actualmente  
            } else if (fiscalizacionTipo.getAportante() != null) {

                IdentificacionTipo identificacionTipo = null;

                if (fiscalizacionTipo.getAportante().getPersonaJuridica() != null
                        && fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                        && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                    identificacionTipo = fiscalizacionTipo.getAportante().getPersonaJuridica().getIdPersona();

                } else if (fiscalizacionTipo.getAportante().getPersonaNatural() != null
                        && fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona() != null
                        && (StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isNotBlank(fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                    identificacionTipo = fiscalizacionTipo.getAportante().getPersonaNatural().getIdPersona();
                }
                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

                if (fiscalizacion != null &&  fiscalizacionTipo.getPeriodoFiscalizacion() != null) {

                    rangoFechaTipo = Util.aproximarPeriodo(fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio(), fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin());
                    List<Fiscalizacion> fiscalizacionNuevoAportantePeriodo
                            = fiscalizacionDao.findFiscalizacionByAportanteAndPeriodo(identificacion, rangoFechaTipo.getFecInicio(), rangoFechaTipo.getFecFin());

                    if (fiscalizacionNuevoAportantePeriodo != null
                            && !fiscalizacionNuevoAportantePeriodo.isEmpty()) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El aportante con la identificación :" + identificacion + ", ya tiene asociada una fiscalización en el periodo ingresado"));
                    }
                }
                //Se valida el tercer caso, se va a cambiar el periodo pero no el aportante   
            } else if (fiscalizacion != null && fiscalizacionTipo.getPeriodoFiscalizacion() != null
                    && fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio() != null
                    && fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin() != null) {

                Identificacion identificacion = new Identificacion();
                identificacion.setCodTipoIdentificacion(fiscalizacion.getAportante().getPersona().getCodTipoIdentificacion().getId());
                identificacion.setValNumeroIdentificacion(fiscalizacion.getAportante().getPersona().getValNumeroIdentificacion());

                rangoFechaTipo = Util.aproximarPeriodo(fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio(), fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin());

                List<Fiscalizacion> fiscalizacionAportanteNuevoPeriodo
                        = fiscalizacionDao.findFiscalizacionByAportanteAndPeriodo(identificacion, rangoFechaTipo.getFecInicio(), rangoFechaTipo.getFecFin());

                if (fiscalizacionAportanteNuevoPeriodo != null
                        && !fiscalizacionAportanteNuevoPeriodo.isEmpty()) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El aportante con la identificación :" + identificacion + ", ya tiene asociada una fiscalización en el periodo ingresado"));
                }
            }
        }
    }
}
