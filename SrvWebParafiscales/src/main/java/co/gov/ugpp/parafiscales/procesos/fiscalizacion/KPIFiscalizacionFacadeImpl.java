package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionDao;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz KPIFiscalizacionFacade que contiene las
 * operaciones del servicio SrvAplKPIFiscalizacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class KPIFiscalizacionFacadeImpl extends AbstractFacade implements KPIFiscalizacionFacade {

    @EJB
    private FiscalizacionDao fiscalizacionDao;

    @Override
    public PagerData<FiscalizacionTipo> consultarFiscalizacionKPI(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
       
        final PagerData<Fiscalizacion> fiscalizacionesPagerData = fiscalizacionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<FiscalizacionTipo> fiscalizacionTipoList = new ArrayList<FiscalizacionTipo>();

        for (final Fiscalizacion fiscalizacion : fiscalizacionesPagerData.getData()) {
            fiscalizacion.setUseOnlySpecifiedFields(true);
            final FiscalizacionTipo fiscalizacionTipo = mapper.map(fiscalizacion, FiscalizacionTipo.class);
            fiscalizacionTipoList.add(fiscalizacionTipo);
        }
        return new PagerData(fiscalizacionTipoList, fiscalizacionesPagerData.getNumPages());
    }
}
