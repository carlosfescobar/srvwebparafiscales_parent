package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaRequerimientoInformacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaRequerimientoInformacionListadoDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaRequerimientoRadicadoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoListaChequeo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientoinformaciontipo.v1.RespuestaRequerimientoInformacionTipo;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientotipo.v1.RespuestaRequerimientoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz RespuestaRequerimientoFacade que
 contiene las operaciones del servicio SrvAplRespuestaRequerimientoInformacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RespuestaRequerimientoFacadeImpl extends AbstractFacade implements RespuestaRequerimientoFacade {

    @EJB
    private RespuestaRequerimientoInformacionDao respuestaRequerimientoInformacionDao;

    @EJB
    private RespuestaRequerimientoInformacionListadoDao respuestaRequerimientoInformacionListadoDao;

    @EJB
    private FiscalizacionDao fiscalizacionDao;

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    RespuestaRequerimientoRadicadoDao respuestaRequerimientoRadicadoDao;

    @Override

    public Long crearRespuestaRequerimientoInformacion(RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (respuestaRequerimientoInformacionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkRespuestaRequerimientoInformacion(respuestaRequerimientoInformacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        RespuestaRequerimientoInformacion respuestaRequerimientoInformacion = new RespuestaRequerimientoInformacion();
        
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        respuestaRequerimientoInformacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateRespuestaRequerimientoInformacion(respuestaRequerimientoInformacion, respuestaRequerimientoInformacionTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idRespuestaRequerimientoInformacion = respuestaRequerimientoInformacionDao.create(respuestaRequerimientoInformacion);

        return idRespuestaRequerimientoInformacion;
    }

    @Override
    public PagerData<RespuestaRequerimientoTipo> buscarPorCriteriosRespuestaRequerimientoInformacion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<RespuestaRequerimientoInformacion> pagerDataEntity
                = respuestaRequerimientoInformacionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<RespuestaRequerimientoTipo> respuestaRequerimientoInformacionTipoList = new ArrayList<RespuestaRequerimientoTipo>();

        for (final RespuestaRequerimientoInformacion respuestaRequerimientoInformacion : pagerDataEntity.getData()) {
            final RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo = mapper.map(respuestaRequerimientoInformacion, RespuestaRequerimientoTipo.class);

            respuestaRequerimientoInformacionTipoList.add(respuestaRequerimientoInformacionTipo);
        }
        return new PagerData<RespuestaRequerimientoTipo>(respuestaRequerimientoInformacionTipoList, pagerDataEntity.getNumPages());
    }

    @Override
    public void actualizarRespuestaRequerimientoInformacion(RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (respuestaRequerimientoInformacionTipo == null || StringUtils.isBlank(respuestaRequerimientoInformacionTipo.getIdRespuestaRequerimientoInformacion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        RespuestaRequerimientoInformacion respuestaRequerimientoInformacion = respuestaRequerimientoInformacionDao.find(Long.valueOf(respuestaRequerimientoInformacionTipo.getIdRespuestaRequerimientoInformacion()));

        if (respuestaRequerimientoInformacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontó un idrespuestaRequerimientoInformacion con el valor:" + respuestaRequerimientoInformacion));
            throw new AppException(errorTipoList);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        respuestaRequerimientoInformacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateRespuestaRequerimientoInformacion(respuestaRequerimientoInformacion, respuestaRequerimientoInformacionTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        respuestaRequerimientoInformacionDao.edit(respuestaRequerimientoInformacion);
    }

    private void populateRespuestaRequerimientoInformacion(RespuestaRequerimientoInformacion respuestaRequerimientoInformacion, RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo, List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(respuestaRequerimientoInformacionTipo.getIdFiscalizacion())) {
            Fiscalizacion fiscalizacion = fiscalizacionDao.find(Long.valueOf(respuestaRequerimientoInformacionTipo.getIdFiscalizacion()));
            if (fiscalizacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontro una fiscalización con el ID: " + respuestaRequerimientoInformacionTipo.getIdFiscalizacion()));
            } else {
                respuestaRequerimientoInformacion.setIdFiscalizacion(fiscalizacion);
            }
        }
        if (StringUtils.isNotBlank(respuestaRequerimientoInformacionTipo.getIdRadicadoEntrada())) {
            respuestaRequerimientoInformacion.setIdRadicadoEntrada(respuestaRequerimientoInformacionTipo.getIdRadicadoEntrada());
        }

        if (respuestaRequerimientoInformacionTipo.getFecRadicadoEntrada() != null) {
            respuestaRequerimientoInformacion.setFechaRadicadoEntrada(respuestaRequerimientoInformacionTipo.getFecRadicadoEntrada());
        }

        if (respuestaRequerimientoInformacionTipo.getCodListadoChequeo() != null
                && !respuestaRequerimientoInformacionTipo.getCodListadoChequeo().isEmpty()) {
            RespuestaRequerimientoListaChequeo respuestaRequerimientoListadoChequeo;
            for (String codListadoChequeo : respuestaRequerimientoInformacionTipo.getCodListadoChequeo()) {
                if (StringUtils.isNotBlank(codListadoChequeo)) {
                    ValorDominio valorDominio = valorDominioDao.find(codListadoChequeo);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codListadoChequeo con el ID: " + codListadoChequeo));
                    } else if (respuestaRequerimientoInformacion.getId() != null) {
                        respuestaRequerimientoListadoChequeo = respuestaRequerimientoInformacionListadoDao.findByRespuestaAndCodListadoChequeo(respuestaRequerimientoInformacion, valorDominio);
                        if (respuestaRequerimientoListadoChequeo != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre el codListadoChequeo: " + valorDominio.getId() + ", y el id de respuestaRequerimientoInformacion: " + respuestaRequerimientoInformacion.getId()));
                        } else {
                            respuestaRequerimientoListadoChequeo = crearRespuestaRequerimientoListadoChequeo(valorDominio, respuestaRequerimientoInformacion, contextoTransaccionalTipo);
                            respuestaRequerimientoInformacion.getCodListadoChequeo().add(respuestaRequerimientoListadoChequeo);
                        }
                    } else {
                        respuestaRequerimientoListadoChequeo = crearRespuestaRequerimientoListadoChequeo(valorDominio, respuestaRequerimientoInformacion, contextoTransaccionalTipo);
                        respuestaRequerimientoInformacion.getCodListadoChequeo().add(respuestaRequerimientoListadoChequeo);
                    }
                }
            }
        }
    }

    private RespuestaRequerimientoListaChequeo crearRespuestaRequerimientoListadoChequeo(ValorDominio valorDominio, RespuestaRequerimientoInformacion respuestaRequerimientoInformacion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        RespuestaRequerimientoListaChequeo requerimientoListadoChequeo = new RespuestaRequerimientoListaChequeo();
        requerimientoListadoChequeo.setCodListadoChequeo(valorDominio);
        requerimientoListadoChequeo.setRespuestaRequerimientoInformacion(respuestaRequerimientoInformacion);
        requerimientoListadoChequeo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return requerimientoListadoChequeo;

    }
}
