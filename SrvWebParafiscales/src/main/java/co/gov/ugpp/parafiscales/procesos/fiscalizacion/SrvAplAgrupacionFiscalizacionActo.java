/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.MsjOpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.MsjOpCrearAgrupacionFiscalizacionActoFallo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.OpCrearAgrupacionFiscalizacionActoRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.OpCrearAgrupacionFiscalizacionActoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@WebService(serviceName = "SrvAplAgrupacionFiscalizacionActo",
        portName = "portSrvAplAgrupacionFiscalizacionActoSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvaplagrupacionfiscalizacionacto.v1.PortSrvAplAgrupacionFiscalizacionActoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplAgrupacionFiscalizacionActo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAgrupacionFiscalizacionActo extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAgrupacionFiscalizacionActo.class);

    @EJB
    private AgrupacionFiscalizacionActoFacade agrupacionFiscalizacionActoFacade;

    public OpCrearAgrupacionFiscalizacionActoRespTipo opCrearAgrupacionFiscalizacionActo(OpCrearAgrupacionFiscalizacionActoSolTipo msjOpCrearAgrupacionFiscalizacionActoSol) throws MsjOpCrearAgrupacionFiscalizacionActoFallo, AppException {
        
        LOG.info("OPERACION: opCrearAgrupacionFiscalizacionActo ::: INICIO");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAgrupacionFiscalizacionActoSol.getContextoTransaccional();
        final AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo = msjOpCrearAgrupacionFiscalizacionActoSol.getAgrupacionFiscalizacionActo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(),
                    contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );

             agrupacionFiscalizacionActoFacade.crearAgrupacionFiscalizacioActo(agrupacionFiscalizacionActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionFiscalizacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionFiscalizacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearAgrupacionFiscalizacionActoRespTipo resp = new OpCrearAgrupacionFiscalizacionActoRespTipo();
        resp.setContextoRespuesta(cr);
        
        
        LOG.info("OPERACION: opCrearAgrupacionFiscalizacionActo ::: FIN");
        
        return resp;

    }

    public OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo opBuscarPorCriteriosAgrupacionFiscalizacionActo(OpBuscarPorCriteriosAgrupacionFiscalizacionActoSolTipo msjOpBuscarPorCriteriosAgrupacionFiscalizacionActoSol) throws MsjOpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo {
        
        LOG.info("OPERACION: opBuscarPorCriteriosAgrupacionFiscalizacionActo ::: INICIO");
        
        ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAgrupacionFiscalizacionActoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosAgrupacionFiscalizacionActoSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();

        final PagerData<AgrupacionFiscalizacionActoTipo> AgrupacionesFiscalizacionTipo;

        try {

            this.initContextoRespuesta(contextoTransaccionalTipo, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            AgrupacionesFiscalizacionTipo = agrupacionFiscalizacionActoFacade.buscarPorCriteriosAgrupacionFiscalizacioActo(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAgrupacionFiscalizacionActoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo buscarPorCriteriosAgrupacionFiscalizacionActoRespTipo = new OpBuscarPorCriteriosAgrupacionFiscalizacionActoRespTipo();
        contextoRespuesta.setValCantidadPaginas(AgrupacionesFiscalizacionTipo.getNumPages());
        buscarPorCriteriosAgrupacionFiscalizacionActoRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosAgrupacionFiscalizacionActoRespTipo.getAgrupacionesFiscalizacionActos().addAll(AgrupacionesFiscalizacionTipo.getData());

        LOG.info("OPERACION: opBuscarPorCriteriosAgrupacionFiscalizacionActo ::: FIN");
        
        return buscarPorCriteriosAgrupacionFiscalizacionActoRespTipo;
    }



}
