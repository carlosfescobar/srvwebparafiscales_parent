package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.MsjOpActualizarBusquedaFallo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.MsjOpBuscarPorCriteriosBusquedaFallo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.MsjOpCrearBusquedaFallo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpActualizarBusquedaRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpActualizarBusquedaSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpBuscarPorCriteriosBusquedaRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpBuscarPorCriteriosBusquedaSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpCrearBusquedaRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.OpCrearBusquedaSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@WebService(serviceName = "SrvAplBusquedaInformacionPersona",
        portName = "portSrvAplBusquedaInformacionPersonaSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvaplbusquedainformacionpersona.v1.PortSrvAplBusquedaInformacionPersonaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplBusquedaInformacionPersona/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplBusquedaInformacionPersona extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplBusquedaInformacionPersona.class);

    @EJB
    private BusquedaInformacionPersonaFacade busquedaFacade;

    public OpCrearBusquedaRespTipo opCrearBusqueda(OpCrearBusquedaSolTipo msjOpCrearBusquedaSol) throws MsjOpCrearBusquedaFallo, AppException {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearBusquedaSol.getContextoTransaccional();
        final BusquedaTipo busquedaTipo = msjOpCrearBusquedaSol.getBusqueda();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long busquedaPk;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), 
                    contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );

            busquedaPk = busquedaFacade.crearBusqueda(busquedaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearBusquedaRespTipo resp = new OpCrearBusquedaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdBusqueda(busquedaPk.toString());

        return resp;
    }

    public OpActualizarBusquedaRespTipo opActualizarBusqueda(OpActualizarBusquedaSolTipo msjOpActualizarBusquedaSol) throws MsjOpActualizarBusquedaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarBusquedaSol.getContextoTransaccional();
        final BusquedaTipo busquedaTipo = msjOpActualizarBusquedaSol.getBusqueda();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            busquedaFacade.actualizarBusqueda(busquedaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarBusquedaRespTipo resp = new OpActualizarBusquedaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosBusquedaRespTipo opBuscarPorCriteriosBusqueda(OpBuscarPorCriteriosBusquedaSolTipo msjOpBuscarPorCriteriosBusquedaSol) throws MsjOpBuscarPorCriteriosBusquedaFallo {
        
        LOG.info("Op: opBuscarPorCriteriosBusqueda ::: INIT");
        ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosBusquedaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosBusquedaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();

        final PagerData<BusquedaTipo> busquedasTipo;

        try {

            this.initContextoRespuesta(contextoTransaccionalTipo, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            busquedasTipo = busquedaFacade.buscarPorCriteriosBusqueda(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosBusquedaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        LOG.info("Op: opBuscarPorCriteriosEntidadExterna ::: END");

        OpBuscarPorCriteriosBusquedaRespTipo buscarPorCriteriosBusquedaRespTipo = new OpBuscarPorCriteriosBusquedaRespTipo();
        contextoRespuesta.setValCantidadPaginas(busquedasTipo.getNumPages());
        buscarPorCriteriosBusquedaRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosBusquedaRespTipo.getBusquedas().addAll(busquedasTipo.getData());

        return buscarPorCriteriosBusquedaRespTipo;
    }

}
