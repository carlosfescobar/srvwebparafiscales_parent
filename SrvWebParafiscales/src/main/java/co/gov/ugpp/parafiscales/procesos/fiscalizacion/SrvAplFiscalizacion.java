package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.MsjOpActualizarFiscalizacionFallo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.MsjOpBuscarPorCriteriosFiscalizacionFallo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.MsjOpBuscarPorIdFiscalizacionFallo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.MsjOpCrearFiscalizacionFallo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpActualizarFiscalizacionRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpActualizarFiscalizacionSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpBuscarPorCriteriosFiscalizacionRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpBuscarPorCriteriosFiscalizacionSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpBuscarPorIdFiscalizacionRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpBuscarPorIdFiscalizacionSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpCrearFiscalizacionRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.OpCrearFiscalizacionSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author everis
 */
@WebService(serviceName = "SrvAplFiscalizacion",
        portName = "portSrvAplFiscalizacionSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvaplfiscalizacion.v1.PortSrvAplFiscalizacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplFiscalizacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplFiscalizacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplFiscalizacion.class);
    @EJB
    private FiscalizacionFacade fiscalizacionFacade;

    public OpCrearFiscalizacionRespTipo opCrearFiscalizacion(OpCrearFiscalizacionSolTipo msjOpCrearFiscalizacionSol) throws MsjOpCrearFiscalizacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearFiscalizacionSol.getContextoTransaccional();
        final FiscalizacionTipo fiscalizacionTipo = msjOpCrearFiscalizacionSol.getFiscalizacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long fiscalizacionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            fiscalizacionPK = fiscalizacionFacade.crearFiscalizacion(fiscalizacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearFiscalizacionRespTipo resp = new OpCrearFiscalizacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdFiscalizacion(fiscalizacionPK.toString());
        return resp;
    }

    public OpBuscarPorCriteriosFiscalizacionRespTipo opBuscarPorCriteriosFiscalizacion(OpBuscarPorCriteriosFiscalizacionSolTipo msjOpBuscarPorCriteriosFiscalizacionSol) throws MsjOpBuscarPorCriteriosFiscalizacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosFiscalizacionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosFiscalizacionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<FiscalizacionTipo> fiscalizacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            fiscalizacionTipoPagerData = fiscalizacionFacade.buscarPorCriteriosFiscalizacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFiscalizacionFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFiscalizacionFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);

        }
        final OpBuscarPorCriteriosFiscalizacionRespTipo resp = new OpBuscarPorCriteriosFiscalizacionRespTipo();
        cr.setValCantidadPaginas(fiscalizacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getFiscalizacion().addAll(fiscalizacionTipoPagerData.getData());

        return resp;
    }

    public OpBuscarPorIdFiscalizacionRespTipo opBuscarPorIdFiscalizacion(OpBuscarPorIdFiscalizacionSolTipo msjOpBuscarPorIdFiscalizacionSol) throws MsjOpBuscarPorIdFiscalizacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdFiscalizacionSol.getContextoTransaccional();
        final List<String> idFiscalizacion = msjOpBuscarPorIdFiscalizacionSol.getIdFiscalizacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<FiscalizacionTipo> fiscalizacionTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            fiscalizacionTipos = fiscalizacionFacade.buscarPorIdFiscalizacion(idFiscalizacion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdFiscalizacionRespTipo resp = new OpBuscarPorIdFiscalizacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getFiscalizacion().addAll(fiscalizacionTipos);

        return resp;

    }

    public OpActualizarFiscalizacionRespTipo opActualizarFiscalizacion(OpActualizarFiscalizacionSolTipo msjOpActualizarFiscalizacionSol) throws MsjOpActualizarFiscalizacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarFiscalizacionSol.getContextoTransaccional();
        final FiscalizacionTipo fiscalizacionTipo = msjOpActualizarFiscalizacionSol.getFiscalizacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            fiscalizacionFacade.actualizarFiscalizacion(fiscalizacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarFiscalizacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarFiscalizacionRespTipo resp = new OpActualizarFiscalizacionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
