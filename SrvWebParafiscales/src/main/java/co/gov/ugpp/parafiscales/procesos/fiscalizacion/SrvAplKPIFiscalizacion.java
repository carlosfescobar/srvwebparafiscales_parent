/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1.MsjOpConsultarFiscalizacionKPIFallo;
import co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1.OpConsultarFiscalizacionKPIRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1.OpConsultarFiscalizacionKPISolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplKPIFiscalizacion", portName = "portSrvAplKPIFiscalizacionSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvaplkpifiscalizacion.v1.PortSrvAplKPIFiscalizacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplKPIFiscalizacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPIFiscalizacion extends  AbstractSrvApl {

    @EJB
    private KPIFiscalizacionFacade kPIFiscalizacionFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPIFiscalizacion.class);
    
    public OpConsultarFiscalizacionKPIRespTipo opConsultarFiscalizacionKPI(OpConsultarFiscalizacionKPISolTipo msjOpConsultarFiscalizacionKPISol) throws MsjOpConsultarFiscalizacionKPIFallo {
           LOG.info("OPERACION: opConsultarFiscalizacionKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarFiscalizacionKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarFiscalizacionKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<FiscalizacionTipo> fiscalizacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            fiscalizacionTipoPagerData = kPIFiscalizacionFacade.consultarFiscalizacionKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarFiscalizacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarFiscalizacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarFiscalizacionKPIRespTipo resp = new OpConsultarFiscalizacionKPIRespTipo();

        cr.setValCantidadPaginas(fiscalizacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getFiscalizaciones().addAll(fiscalizacionTipoPagerData.getData());
        resp.setCantidadFiscalizaciones(Integer.toString(fiscalizacionTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarFiscalizacionKPI ::: FIN");

        return resp;
    }
    
}
