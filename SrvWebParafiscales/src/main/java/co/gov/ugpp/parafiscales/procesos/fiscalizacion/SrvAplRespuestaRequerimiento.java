package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.MsjOpActualizarRespuestaRequerimientoFallo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.MsjOpBuscarPorCriteriosRespuestaRequerimientoFallo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.MsjOpCrearRespuestaRequerimientoFallo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpActualizarRespuestaRequerimientoRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpActualizarRespuestaRequerimientoSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpBuscarPorCriteriosRespuestaRequerimientoRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpBuscarPorCriteriosRespuestaRequerimientoSolTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpCrearRespuestaRequerimientoRespTipo;
import co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.OpCrearRespuestaRequerimientoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientotipo.v1.RespuestaRequerimientoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplRespuestaRequerimiento",
        portName = "portSrvAplRespuestaRequerimientoSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvaplrespuestarequerimiento.v1.PortSrvAplRespuestaRequerimientoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplRespuestaRequerimiento/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplRespuestaRequerimiento extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRespuestaRequerimiento.class);
    @EJB
    private RespuestaRequerimientoFacade respuestaRequerimientoInformacionFacade;

    public OpBuscarPorCriteriosRespuestaRequerimientoRespTipo opBuscarPorCriteriosRespuestaRequerimiento(OpBuscarPorCriteriosRespuestaRequerimientoSolTipo msjOpBuscarPorCriteriosRespuestaRequerimientoSol) throws MsjOpBuscarPorCriteriosRespuestaRequerimientoFallo {
        LOG.info("Op: opBuscarPorCriteriosRespuestaRequerimientoInformacion ::: INIT");

        ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosRespuestaRequerimientoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosRespuestaRequerimientoSol.getParametroTipo();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();

        final PagerData<RespuestaRequerimientoTipo> respuestaRequerimientoInformacionTipoPargerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            respuestaRequerimientoInformacionTipoPargerData = respuestaRequerimientoInformacionFacade.buscarPorCriteriosRespuestaRequerimientoInformacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        LOG.info("Op: opBuscarPorCriteriosEntidadExterna ::: END");

        OpBuscarPorCriteriosRespuestaRequerimientoRespTipo buscarPorCriteriosRespuestaRequerimientoInformacionRespTipo = new OpBuscarPorCriteriosRespuestaRequerimientoRespTipo();
        contextoRespuesta.setValCantidadPaginas(respuestaRequerimientoInformacionTipoPargerData.getNumPages());
        buscarPorCriteriosRespuestaRequerimientoInformacionRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosRespuestaRequerimientoInformacionRespTipo.getRespuestaRequerimiento().addAll(respuestaRequerimientoInformacionTipoPargerData.getData());

        return buscarPorCriteriosRespuestaRequerimientoInformacionRespTipo;
    }

    public OpActualizarRespuestaRequerimientoRespTipo opActualizarRespuestaRequerimiento(OpActualizarRespuestaRequerimientoSolTipo msjOpActualizarRespuestaRequerimientoSol) throws MsjOpActualizarRespuestaRequerimientoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarRespuestaRequerimientoSol.getContextoTransaccional();
        final RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo = msjOpActualizarRespuestaRequerimientoSol.getRespuestaRequerimiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaRequerimientoInformacionFacade.actualizarRespuestaRequerimientoInformacion(respuestaRequerimientoInformacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarRespuestaRequerimientoRespTipo resp = new OpActualizarRespuestaRequerimientoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpCrearRespuestaRequerimientoRespTipo opCrearRespuestaRequerimiento(OpCrearRespuestaRequerimientoSolTipo msjOpCrearRespuestaRequerimientoSol) throws MsjOpCrearRespuestaRequerimientoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearRespuestaRequerimientoSol.getContextoTransaccional();
        final RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo = msjOpCrearRespuestaRequerimientoSol.getRespuestaRequerimiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long respuestaRequerimientoInformacionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(),
                    contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );

            respuestaRequerimientoInformacionPK = respuestaRequerimientoInformacionFacade.crearRespuestaRequerimientoInformacion(respuestaRequerimientoInformacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRespuestaRequerimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearRespuestaRequerimientoRespTipo resp = new OpCrearRespuestaRequerimientoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdRespuestaRequerimiento(respuestaRequerimientoInformacionPK.toString());
        return resp;
    }
}
