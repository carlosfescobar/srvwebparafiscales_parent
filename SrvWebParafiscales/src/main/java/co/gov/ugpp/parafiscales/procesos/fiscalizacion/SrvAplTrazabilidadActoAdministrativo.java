package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.MsjOpActualizarTrazabilidadActoAdministrativoFallo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.MsjOpBuscarPorIdTrazabilidadActoAdministrativoFallo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.MsjOpCrearTrazabilidadActoAdministrativoFallo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpActualizarTrazabilidadActoAdministrativoRespTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpActualizarTrazabilidadActoAdministrativoSolTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpCrearTrazabilidadActoAdministrativoRespTipo;
import co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.OpCrearTrazabilidadActoAdministrativoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.fiscalizacion.trazabilidadactoadministrativotipo.v1.TrazabilidadActoAdministrativoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplTrazabilidadActoAdministrativo",
        portName = "portSrvAplTrazabilidadActoAdministrativoSOAP",
        endpointInterface = "co.gov.ugpp.fiscalizacion.srvapltrazabilidadactoadministrativo.v1.PortSrvAplTrazabilidadActoAdministrativoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Fiscalizacion/SrvAplTrazabilidadActoAdministrativo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplTrazabilidadActoAdministrativo extends AbstractSrvApl {

    @EJB
    private TrazabilidadActoAdministrativoFacade trazabilidadActoAdministrativoFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplTrazabilidadActoAdministrativo.class);

    public OpCrearTrazabilidadActoAdministrativoRespTipo opCrearTrazabilidadActoAdministrativo(OpCrearTrazabilidadActoAdministrativoSolTipo msjOpCrearTrazabilidadActoAdministrativoSol) throws MsjOpCrearTrazabilidadActoAdministrativoFallo {

        LOG.info("OPERACION: opCrearTrazabilidadActoAdministrativo ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearTrazabilidadActoAdministrativoSol.getContextoTransaccional();
        final TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo = msjOpCrearTrazabilidadActoAdministrativoSol.getTrazabilidadActoAdministrativo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            trazabilidadActoAdministrativoFacade.crearTrazabilidadActoAdministrativo(trazabilidadActoAdministrativoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearTrazabilidadActoAdministrativoRespTipo resp = new OpCrearTrazabilidadActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opCrearTrazabilidadActoAdministrativo ::: FIN");

        return resp;
    }

    public OpActualizarTrazabilidadActoAdministrativoRespTipo opActualizarTrazabilidadActoAdministrativo(OpActualizarTrazabilidadActoAdministrativoSolTipo msjOpActualizarTrazabilidadActoAdministrativoSol) throws MsjOpActualizarTrazabilidadActoAdministrativoFallo {

        LOG.info("OPERACION: opActualizarTrazabilidadActoAdministrativo ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarTrazabilidadActoAdministrativoSol.getContextoTransaccional();
        final TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo = msjOpActualizarTrazabilidadActoAdministrativoSol.getTrazabilidadActoAdministrativo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            trazabilidadActoAdministrativoFacade.actualizarTrazabilidadActoAdministrativo(trazabilidadActoAdministrativoTipo, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarTrazabilidadActoAdministrativoRespTipo resp = new OpActualizarTrazabilidadActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opActualizarTrazabilidadActoAdministrativo ::: FIN");

        return resp;
    }

    public OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo opBuscarPorIdTrazabilidadActoAdministrativo(OpBuscarPorIdTrazabilidadActoAdministrativoSolTipo msjOpBuscarPorIdTrazabilidadActoAdministrativoSol) throws MsjOpBuscarPorIdTrazabilidadActoAdministrativoFallo {

        LOG.info("OPERACION: opBuscarPorIdTrazabilidadActoAdministrativo ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdTrazabilidadActoAdministrativoSol.getContextoTransaccional();
        final List<String> idAccionActoAdministrativo = msjOpBuscarPorIdTrazabilidadActoAdministrativoSol.getIdActoTrazabilidadActoAdministrativo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<TrazabilidadActoAdministrativoTipo> trazabilidadActoAdministrativoTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            trazabilidadActoAdministrativoTipos = trazabilidadActoAdministrativoFacade.buscarPorIdTrazabilidadActoAdministrativo(idAccionActoAdministrativo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdTrazabilidadActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo resp = new OpBuscarPorIdTrazabilidadActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getTrazabilidadActoAdministrativo().addAll(trazabilidadActoAdministrativoTipos);

        LOG.info("OPERACION: opBuscarPorIdTrazabilidadActoAdministrativo ::: FIN");

        return resp;
    }

}
