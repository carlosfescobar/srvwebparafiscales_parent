package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.fiscalizacion.trazabilidadactoadministrativotipo.v1.TrazabilidadActoAdministrativoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface TrazabilidadActoAdministrativoFacade extends Serializable {

    void crearTrazabilidadActoAdministrativo(final TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarTrazabilidadActoAdministrativo(final TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<TrazabilidadActoAdministrativoTipo> buscarPorIdTrazabilidadActoAdministrativo(final List<String> idTrzabilidadActosAdministrativos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
