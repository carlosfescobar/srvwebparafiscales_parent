package co.gov.ugpp.parafiscales.procesos.fiscalizacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoRadicadoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.TrazabilidadActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.EventoActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.RadicadoTrazabilidad;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;
import co.gov.ugpp.schema.fiscalizacion.trazabilidadactoadministrativotipo.v1.TrazabilidadActoAdministrativoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz TrazabilidadActoAdministrativoFacade que
 * contiene las operaciones del servicio SrvAplAccionActoAdministrativo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class TrazabilidadActoAdministrativoFacadeImpl extends AbstractFacade implements TrazabilidadActoAdministrativoFacade {

    @EJB
    private TrazabilidadActoAdministrativoDao accionActoAdministrativoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private Populator populator;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @Override
    public void crearTrazabilidadActoAdministrativo(final TrazabilidadActoAdministrativoTipo accionActoAdministrativoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (accionActoAdministrativoTipo == null || StringUtils.isBlank(accionActoAdministrativoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        TrazabilidadActoAdministrativo accionActoAdministrativo = accionActoAdministrativoDao.find(accionActoAdministrativoTipo.getIdActoAdministrativo());

        if (accionActoAdministrativo != null) {
            throw new AppException("Ya existe un accionActoAdministrativo con el ID: " + accionActoAdministrativoTipo.getIdActoAdministrativo());
        } else {
            accionActoAdministrativo = new TrazabilidadActoAdministrativo();
            accionActoAdministrativo.setId(accionActoAdministrativoTipo.getIdActoAdministrativo());
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        this.populateAccionActoAdministrativo(accionActoAdministrativoTipo, accionActoAdministrativo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        accionActoAdministrativo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        accionActoAdministrativoDao.create(accionActoAdministrativo);
    }

    @Override
    public void actualizarTrazabilidadActoAdministrativo(final TrazabilidadActoAdministrativoTipo accionActoAdministrativoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (accionActoAdministrativoTipo == null || StringUtils.isBlank(accionActoAdministrativoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        TrazabilidadActoAdministrativo accionActoAdministrativo = accionActoAdministrativoDao.find(accionActoAdministrativoTipo.getIdActoAdministrativo());

        if (accionActoAdministrativo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un accionActoAdministrativo con el valor:" + accionActoAdministrativoTipo.getIdActoAdministrativo()));
            throw new AppException(errorTipoList);
        }

        accionActoAdministrativo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAccionActoAdministrativo(accionActoAdministrativoTipo, accionActoAdministrativo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        accionActoAdministrativoDao.edit(accionActoAdministrativo);
    }

    @Override
    public List<TrazabilidadActoAdministrativoTipo> buscarPorIdTrazabilidadActoAdministrativo(List<String> idAccionActosAdministrativos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idAccionActosAdministrativos == null || idAccionActosAdministrativos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }

        List<TrazabilidadActoAdministrativo> accionActoAdministrativoList = accionActoAdministrativoDao.findByIdList(idAccionActosAdministrativos);

        final List<TrazabilidadActoAdministrativoTipo> trazabilidadActoAdministrativoTipoList
                = mapper.map(accionActoAdministrativoList, TrazabilidadActoAdministrativo.class, TrazabilidadActoAdministrativoTipo.class);

        return trazabilidadActoAdministrativoTipoList;
    }

    /**
     * Método que se encarga de llenar el entity AccionActoAdministrativoTipo
     *
     * @param trazabilidadActoAdministrativoTipo el dto con la información a
     * llenar el entity
     * @param trazabilidadActoAdministrativo el entity a ser llenado
     * @param contextoTransaccionalTipo contiene los datos de auditoría para
     * persistir en la base de datos
     * @param errorTipoList la lista con la pila de errores
     * @throws AppException
     */
    private void populateAccionActoAdministrativo(final TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo,
            final TrazabilidadActoAdministrativo trazabilidadActoAdministrativo, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getIdActoAdministrativo())) {
            final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(String.valueOf(trazabilidadActoAdministrativoTipo.getIdActoAdministrativo()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un actoAdministrativo con el ID: " + trazabilidadActoAdministrativoTipo.getIdActoAdministrativo()));
            } else {
                trazabilidadActoAdministrativo.setActoAdministrativo(actoAdministrativo);
            }
        }
        if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getValDiasProrroga())) {
            trazabilidadActoAdministrativo.setValDiasProrroga(trazabilidadActoAdministrativoTipo.getValDiasProrroga());
        }
        if (trazabilidadActoAdministrativoTipo.getFecNotificacionDefinitiva() != null) {
            trazabilidadActoAdministrativo.setFecNotificacionDefinitiva(trazabilidadActoAdministrativoTipo.getFecNotificacionDefinitiva());
        }
        if (trazabilidadActoAdministrativoTipo.getFecRadicadoEntrada() != null
                || StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getIdRadicadoEntrada())) {
            if (trazabilidadActoAdministrativo.getRadicacionesEntrada() != null
                    && !trazabilidadActoAdministrativo.getRadicacionesEntrada().isEmpty()) {
                if (trazabilidadActoAdministrativoTipo.getFecRadicadoEntrada() != null) {
                    trazabilidadActoAdministrativo.getRadicacionesEntrada().get(Constants.CANTIDAD_RADICADOS_TRAZABILIDAD_ACTO).setFechaRadicado(trazabilidadActoAdministrativoTipo.getFecRadicadoEntrada());
                } else if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getIdRadicadoEntrada())) {
                    trazabilidadActoAdministrativo.getRadicacionesEntrada().get(Constants.CANTIDAD_RADICADOS_TRAZABILIDAD_ACTO).setIdRadicado(trazabilidadActoAdministrativoTipo.getIdRadicadoEntrada());
                }
            } else {
                RadicadoTrazabilidad radicadoTrazabilidad = new RadicadoTrazabilidad();
                radicadoTrazabilidad.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                radicadoTrazabilidad.setFechaRadicado(trazabilidadActoAdministrativoTipo.getFecRadicadoEntrada());
                radicadoTrazabilidad.setIdRadicado(trazabilidadActoAdministrativoTipo.getIdRadicadoEntrada());
                radicadoTrazabilidad.setTrazabilidadActoAdministrativo(trazabilidadActoAdministrativo);
                ValorDominio tipoRadicado = valorDominioDao.find(TipoRadicadoEnum.RADICADO_ENTRADA.getTipoRadicado());
                if (tipoRadicado == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un tipoRadicado con el ID: " + TipoRadicadoEnum.RADICADO_ENTRADA.getTipoRadicado()));
                } else {
                    radicadoTrazabilidad.setCodRadicado(tipoRadicado);
                }
                trazabilidadActoAdministrativo.getRadicacionesEntrada().add(radicadoTrazabilidad);
            }
        }
        if (trazabilidadActoAdministrativoTipo.getFecRadicadoSalidaDefinitivo() != null) {
            trazabilidadActoAdministrativo.setFecRadicadoSalidaDefinitivo(trazabilidadActoAdministrativoTipo.getFecRadicadoSalidaDefinitivo());
        }
        if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getIdRadicadoSalidaDefinitivo())) {
            trazabilidadActoAdministrativo.setIdRadicadoSalidaDefinitivo(trazabilidadActoAdministrativoTipo.getIdRadicadoSalidaDefinitivo());
        }

        if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getCodTipoNotificacionDefinitiva())) {
            ValorDominio codTipoNotificacionDefinitiva = valorDominioDao.find(trazabilidadActoAdministrativoTipo.getCodTipoNotificacionDefinitiva());
            if (codTipoNotificacionDefinitiva == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoNotificacionDefinitiva con el ID: " + trazabilidadActoAdministrativoTipo.getCodTipoNotificacionDefinitiva()));
            } else {
                trazabilidadActoAdministrativo.setCodTipoNotificacionDefinitiva(codTipoNotificacionDefinitiva);
            }
        }

        if (StringUtils.isNotBlank(trazabilidadActoAdministrativoTipo.getDescTipoNotificacionDefinitiva())) {
            trazabilidadActoAdministrativo.setDescTipoNotificacionDefinitiva(trazabilidadActoAdministrativoTipo.getDescTipoNotificacionDefinitiva());
        }

        if (trazabilidadActoAdministrativoTipo.getEnvioActoAdministrativo() != null
                && !trazabilidadActoAdministrativoTipo.getEnvioActoAdministrativo().isEmpty()) {
            for (EnvioActoAdministrativoTipo envioActoAdministrativoTipo : trazabilidadActoAdministrativoTipo.getEnvioActoAdministrativo()) {
                if (envioActoAdministrativoTipo != null) {
                    EventoActoAdministrativo envioActoAdministrativo = mapper.map(envioActoAdministrativoTipo, EventoActoAdministrativo.class);
                    envioActoAdministrativo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    if (StringUtils.isNotBlank(envioActoAdministrativoTipo.getCodEstadoNotificacion())) {
                        ValorDominio codEstadoNotificacion = valorDominioDao.find(envioActoAdministrativoTipo.getCodEstadoNotificacion());
                        if (codEstadoNotificacion == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró codEstadoNotificacion con el ID: " + envioActoAdministrativoTipo.getCodEstadoNotificacion()));
                        } else {
                            envioActoAdministrativo.setCodEstadNotificacion(codEstadoNotificacion);
                        }
                    }
                    if (envioActoAdministrativoTipo.getUbicacionNotificada() != null) {
                        Ubicacion ubicacion = new Ubicacion();
                        ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        populator.crearUbicacion(envioActoAdministrativoTipo.getUbicacionNotificada(), ubicacion, errorTipoList);
                        envioActoAdministrativo.setUbicacionNotificada(ubicacion);
                    }
                    envioActoAdministrativo.setTrazabilidadActoAdministrativo(trazabilidadActoAdministrativo);
                    trazabilidadActoAdministrativo.getEnvioActoAdministrativoList().add(envioActoAdministrativo);
                }
            }
        }
        if (trazabilidadActoAdministrativoTipo.getUbicacionDefinitiva() != null) {
            Ubicacion ubicacion = new Ubicacion();
            ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            populator.crearUbicacion(trazabilidadActoAdministrativoTipo.getUbicacionDefinitiva(), ubicacion, errorTipoList);
            trazabilidadActoAdministrativo.setUbicacionDefinitiva(ubicacion);
        }
    }
}
