package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionSubsistemaAdminDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SubsistemaDao;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz AgrupacionSubsistemaAdminFacade que contiene
 * las operaciones del servicio SrvAplAgrupacionSubsistemaAdmin.
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AgrupacionSubsistemFacadeImpl extends AbstractFacade implements AgrupacionSubsistemaFacade {

    @EJB
    private EntidadExternaDao entidadExternaDao;
    @EJB
    private AgrupacionAdministradoraDao agrupacionAdministradoraDao;

    @EJB
    private AgrupacionSubsistemaAdminDao agrupacionSubsistemaAdminDao;

    @EJB
    private SubsistemaDao subsistemaDao;

    @Override
    public Long crearAgrupacionSubsistemaAdmin(AgrupacionSubsistemaTipo agrupacionSubsistemaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (agrupacionSubsistemaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin = new AgrupacionSubsistemaAdmin();
        agrupacionSubsistemaAdmin.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAgrupacionSubsistemaAdmin(agrupacionSubsistemaTipo, agrupacionSubsistemaAdmin, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return agrupacionSubsistemaAdminDao.create(agrupacionSubsistemaAdmin);
    }

    @Override
    public void actualizarAgrupacionSubsistemaAdmin(AgrupacionSubsistemaTipo agrupacionSubsistemaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (StringUtils.isBlank(agrupacionSubsistemaTipo.getIdAgrupacionAdministradora())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin = agrupacionSubsistemaAdminDao.find(Long.valueOf(agrupacionSubsistemaTipo.getIdAgrupacionAdministradora()));

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (agrupacionSubsistemaAdmin == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idAgrupacionSubsistemaAdmin con el valor:" + agrupacionSubsistemaTipo.getIdAgrupacionAdministradora()));
            throw new AppException(errorTipoList);
        }
        agrupacionSubsistemaAdmin.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        populateAgrupacionSubsistemaAdmin(agrupacionSubsistemaTipo, agrupacionSubsistemaAdmin, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        agrupacionSubsistemaAdminDao.edit(agrupacionSubsistemaAdmin);
    }

    @Override
    public PagerData<AgrupacionSubsistemaTipo> buscarPorCriteriosAgrupacionSubsistemaAdmin(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final PagerData<AgrupacionSubsistemaAdmin> pagerDataEntity = agrupacionSubsistemaAdminDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<AgrupacionSubsistemaTipo> agrupacionSubsistemaAdminTipoList = mapper.map(pagerDataEntity.getData(), AgrupacionSubsistemaAdmin.class, AgrupacionSubsistemaTipo.class);

        return new PagerData<AgrupacionSubsistemaTipo>(agrupacionSubsistemaAdminTipoList, pagerDataEntity.getNumPages());
    }

    public void populateAgrupacionSubsistemaAdmin(AgrupacionSubsistemaTipo agrupacionSubsistemaTipo, AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin, ContextoTransaccionalTipo contextoTransaccionalTipo,
            List<ErrorTipo> errorTipoList) throws AppException {

        if (agrupacionSubsistemaTipo.getSubsistema() != null
                && StringUtils.isNotBlank(agrupacionSubsistemaTipo.getSubsistema().getIdSubsistema())) {
            Subsistema subsistema = subsistemaDao.find(agrupacionSubsistemaTipo.getSubsistema().getIdSubsistema());
            if (subsistema == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un subsistema con el ID: " + agrupacionSubsistemaTipo.getSubsistema().getIdSubsistema()));
            } else {
                agrupacionSubsistemaAdmin.setIdSubsistema(subsistema);
            }
        }

        if (agrupacionSubsistemaTipo.getAdministradora() != null
                && !agrupacionSubsistemaTipo.getAdministradora().isEmpty()) {
            AgrupacionAdministradora agrupacionAdministradora;
            this.entidadExternaDuplicada(agrupacionSubsistemaTipo.getAdministradora(), errorTipoList);
            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            } else {
                for (EntidadExternaTipo entidadExternaTipo : agrupacionSubsistemaTipo.getAdministradora()) {

                    if (entidadExternaTipo != null && entidadExternaTipo.getIdPersona() != null) {
                        IdentificacionTipo identificacionTipo = entidadExternaTipo.getIdPersona();
                        Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                        EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);
                        if (entidadExterna == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontrarón una administradora con la identificación: " + identificacion));
                        } else if (agrupacionSubsistemaAdmin.getId() != null) {
                            agrupacionAdministradora = agrupacionAdministradoraDao.findByAgrupacionAdministradoraAdminAndEntidadexterna(entidadExterna, agrupacionSubsistemaAdmin);
                            if (agrupacionAdministradora != null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "Ya existe la relación entre la Entidad Externa " + entidadExterna.getId() + " y la Agrupación Subsistema " + agrupacionSubsistemaAdmin.getId()));
                            } else {
                                agrupacionAdministradora = crearAgrupacionAdministradora(entidadExterna, agrupacionSubsistemaAdmin, contextoTransaccionalTipo);
                                agrupacionSubsistemaAdmin.getAdministradoras().add(agrupacionAdministradora);
                            }
                        } else {
                            agrupacionAdministradora = crearAgrupacionAdministradora(entidadExterna, agrupacionSubsistemaAdmin, contextoTransaccionalTipo);
                            agrupacionSubsistemaAdmin.getAdministradoras().add(agrupacionAdministradora);
                        }
                    }
                }
            }
        }
    }

    private AgrupacionAdministradora crearAgrupacionAdministradora(EntidadExterna entidadExterna, AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        AgrupacionAdministradora agrupacionAdministradora = new AgrupacionAdministradora();
        agrupacionAdministradora.setEntidadExterna(entidadExterna);
        agrupacionAdministradora.setAgrupacionAdministradora(agrupacionSubsistemaAdmin);
        agrupacionAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return agrupacionAdministradora;

    }

    private void entidadExternaDuplicada(List<EntidadExternaTipo> entidadExternaTipoList, List<ErrorTipo> errorTipoList) throws AppException {

        for (EntidadExternaTipo entidadExternaTipoInicial : entidadExternaTipoList) {
            int vecesRepetidas = 0;
            for (EntidadExternaTipo entidadExternaTipoValidada : entidadExternaTipoList) {
                if (entidadExternaTipoInicial.getIdPersona().getCodTipoIdentificacion().equals(entidadExternaTipoValidada.getIdPersona().getCodTipoIdentificacion())
                        && entidadExternaTipoInicial.getIdPersona().getValNumeroIdentificacion().equals(entidadExternaTipoValidada.getIdPersona().getValNumeroIdentificacion())) {
                    vecesRepetidas++;
                }
            }
            if (vecesRepetidas > 1) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Por favor verifique que las administradoras enviadas con las que se va a crear el registro sean diferentes"));
                break;
            }
        }
    }
}
