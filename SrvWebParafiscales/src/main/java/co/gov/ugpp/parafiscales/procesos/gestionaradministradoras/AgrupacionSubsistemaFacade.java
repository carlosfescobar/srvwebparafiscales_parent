package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface AgrupacionSubsistemaFacade extends Serializable {

    Long crearAgrupacionSubsistemaAdmin(final AgrupacionSubsistemaTipo agrupacionSubsistemaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarAgrupacionSubsistemaAdmin(final AgrupacionSubsistemaTipo agrupacionSubsistemaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<AgrupacionSubsistemaTipo> buscarPorCriteriosAgrupacionSubsistemaAdmin(List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
