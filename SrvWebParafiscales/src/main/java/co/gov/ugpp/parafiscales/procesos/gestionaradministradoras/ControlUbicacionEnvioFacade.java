package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface ControlUbicacionEnvioFacade extends Serializable {

    Long crearControlUbicacionEnvio(final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarControlUbicacionEnvio(final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<ControlUbicacionEnvioTipo> buscarPorIdControlUbicacionEnvio(final List<String> idEventoEnvioTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
