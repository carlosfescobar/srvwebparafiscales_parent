package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ControlUbicacionEnvioDao;
import co.gov.ugpp.parafiscales.persistence.dao.EventoEnvioDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ControlUbicacionEnvio;
import co.gov.ugpp.parafiscales.persistence.entity.EventoEnvio;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ControlUbicacionEnvioFacade que contiene las
 * operaciones del servicio SrvAplControlUbicacionEnvio
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ControlUbicacionEnvioFacadeImp extends AbstractFacade implements ControlUbicacionEnvioFacade {

    @EJB
    private ControlUbicacionEnvioDao controlUbicacionEnvioDao;

    @EJB
    private EventoEnvioDao eventoEnvioDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private MunicipioDao municipioDao;

    /**
     * Método que implementa la operación OpCrearUbicacionEnvio del servicio
     * SrvAplControlUbicacionEnvio
     *
     * @param controlUbicacionEnvioTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return el id de negocio creado
     * @throws AppException
     */
    @Override
    public Long crearControlUbicacionEnvio(final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlUbicacionEnvioTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ControlUbicacionEnvio controlUbicacionEnvio = new ControlUbicacionEnvio();
        controlUbicacionEnvio.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateControlUbicacionEnvio(contextoTransaccionalTipo, controlUbicacionEnvioTipo, controlUbicacionEnvio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return controlUbicacionEnvioDao.create(controlUbicacionEnvio);
    }

    /**
     * Método que implementa la operación OpActualizarUbicacionEnvio del
     * servicio SrvAplControlUbicacionEnvio
     *
     * @param controlUbicacionEnvioTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @throws AppException
     */
    @Override
    public void actualizarControlUbicacionEnvio(final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlUbicacionEnvioTipo == null || StringUtils.isBlank(controlUbicacionEnvioTipo.getIdControlUbicacion())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final ControlUbicacionEnvio controlUbicacionEnvio = controlUbicacionEnvioDao.find(Long.valueOf(controlUbicacionEnvioTipo.getIdControlUbicacion()));
        if (controlUbicacionEnvio == null) {
            throw new AppException("No se encontró Control Ubicación Envío con el ID: " + controlUbicacionEnvioTipo.getIdControlUbicacion());
        }
        controlUbicacionEnvio.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateControlUbicacionEnvio(contextoTransaccionalTipo, controlUbicacionEnvioTipo, controlUbicacionEnvio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        controlUbicacionEnvioDao.edit(controlUbicacionEnvio);
    }

    /**
     * Método que implementa la operación OpBuscarPorIdUbicacionEnvio del
     * servicio SrvAplControlUbicacionEnvio
     *
     * @param idEventoEnvioTipoList los id's de negocio a buscar
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return los objetos de negocio encontrados
     * @throws AppException
     */
    @Override
    public List<ControlUbicacionEnvioTipo> buscarPorIdControlUbicacionEnvio(final List<String> idEventoEnvioTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idEventoEnvioTipoList == null || idEventoEnvioTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }
        final List<Long> ids = mapper.map(idEventoEnvioTipoList, String.class, Long.class);
        final List<ControlUbicacionEnvio> controlUbicacionEnvioList = controlUbicacionEnvioDao.findByIdList(ids);
        final List<ControlUbicacionEnvioTipo> controlUbicacionEnvioTipoList = mapper.map(controlUbicacionEnvioList, ControlUbicacionEnvio.class, ControlUbicacionEnvioTipo.class);
        return controlUbicacionEnvioTipoList;
    }

    /**
     * Método que llena la entidad ControlUbicacion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditiría
     * @param controlUbicacionEnvioTipo el objeto origen
     * @param controlUbicacionEnvio el objeto destino
     * @param errorTipoList el listado de errores de la operación
     * @throws AppException
     */
    private void populateControlUbicacionEnvio(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo,
            final ControlUbicacionEnvio controlUbicacionEnvio, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(controlUbicacionEnvioTipo.getCodEstadoUbicacion())) {
            ValorDominio codEstadoUbicacion = valorDominioDao.find(controlUbicacionEnvioTipo.getCodEstadoUbicacion());
            if (codEstadoUbicacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codEstadoUbicacion con el ID: " + controlUbicacionEnvioTipo.getCodEstadoUbicacion()));
            } else {
                controlUbicacionEnvio.setCodEstadoUbicacion(codEstadoUbicacion);
            }
        }
        if (controlUbicacionEnvioTipo.getEventoEnvio() != null
                && StringUtils.isNotBlank(controlUbicacionEnvioTipo.getEventoEnvio().getIdEventoEnvio())) {
            EventoEnvio eventoEnvio = eventoEnvioDao.find(Long.valueOf(controlUbicacionEnvioTipo.getEventoEnvio().getIdEventoEnvio()));
            if (eventoEnvio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró eventoEnvio con el ID: " + controlUbicacionEnvioTipo.getEventoEnvio().getIdEventoEnvio()));
            } else {
                controlUbicacionEnvio.setEventosEnvio(eventoEnvio);
            }
        }
        if (controlUbicacionEnvioTipo.getUbicacion() != null
                && StringUtils.isNotBlank(controlUbicacionEnvioTipo.getUbicacion().getValDireccion())) {
            Ubicacion ubicacion = new Ubicacion();
            ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            ubicacion.setValDireccion(controlUbicacionEnvioTipo.getUbicacion().getValDireccion());
            if (controlUbicacionEnvioTipo.getUbicacion().getMunicipio() != null
                    && StringUtils.isNotBlank(controlUbicacionEnvioTipo.getUbicacion().getMunicipio().getCodMunicipio())) {
                Municipio municipio = municipioDao.findByCodigo(controlUbicacionEnvioTipo.getUbicacion().getMunicipio().getCodMunicipio());
                if (municipio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró Municipio con el ID: " + controlUbicacionEnvioTipo.getUbicacion().getMunicipio().getCodMunicipio()));
                } else {
                    ubicacion.setMunicipio(municipio);
                }
            }
            if (StringUtils.isNotBlank(controlUbicacionEnvioTipo.getUbicacion().getCodTipoDireccion())) {
                ValorDominio codTipoDireccion = valorDominioDao.find(controlUbicacionEnvioTipo.getUbicacion().getCodTipoDireccion());
                if (codTipoDireccion == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró Tipo de Dirección con el ID: " + controlUbicacionEnvioTipo.getUbicacion().getCodTipoDireccion()));
                } else {
                    ubicacion.setCodTipoDireccion(codTipoDireccion);
                }
            }
            controlUbicacionEnvio.setUbicacion(ubicacion);
        }
    }
}
