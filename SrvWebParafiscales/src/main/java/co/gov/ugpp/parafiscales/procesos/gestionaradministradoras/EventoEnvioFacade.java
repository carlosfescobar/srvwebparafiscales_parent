package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface EventoEnvioFacade extends Serializable {

    Long crearEventoEnvio(final EventoEnvioTipo eventoEnvioTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarEventoEnvio(final EventoEnvioTipo eventoEnvioTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<EventoEnvioTipo> buscarPorIdEventoEnvio(final List<String> idEventoEnvioTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
