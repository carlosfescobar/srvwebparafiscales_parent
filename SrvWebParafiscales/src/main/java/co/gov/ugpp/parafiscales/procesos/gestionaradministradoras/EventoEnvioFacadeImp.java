package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.EventoEnvioDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.EventoEnvio;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz EventoEnvioFacade que contiene las operaciones
 * del servicio SrvAplEventoEnvio
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class EventoEnvioFacadeImp extends AbstractFacade implements EventoEnvioFacade {

    @EJB
    private EventoEnvioDao eventoEnvioDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * Método que implementa la operación OpCrearEventoEnvio del servicio
     * SrvAplEventoEnvio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param eventoEnvioTipo el objeto origen
     * @return el id de negocio creado
     * @throws AppException
     */
    @Override
    public Long crearEventoEnvio(final EventoEnvioTipo eventoEnvioTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (eventoEnvioTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        EventoEnvio eventoEnvio = new EventoEnvio();
        eventoEnvio.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateEventoEnvio(contextoTransaccionalTipo, eventoEnvioTipo, eventoEnvio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return eventoEnvioDao.create(eventoEnvio);
    }

    /**
     * Método que implementa la operación OpActualizarEventoEnvio del servicio
     * SrvAplEventoEnvio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param eventoEnvioTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarEventoEnvio(final EventoEnvioTipo eventoEnvioTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (eventoEnvioTipo == null || StringUtils.isBlank(eventoEnvioTipo.getIdEventoEnvio())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        EventoEnvio eventoEnvio = eventoEnvioDao.find(Long.valueOf(eventoEnvioTipo.getIdEventoEnvio()));
        if (eventoEnvio == null) {
            throw new AppException("No se encontró Evento Envío con el ID: " + eventoEnvioTipo.getIdEventoEnvio());
        }
        eventoEnvio.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateEventoEnvio(contextoTransaccionalTipo, eventoEnvioTipo, eventoEnvio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        eventoEnvioDao.edit(eventoEnvio);
    }

    /**
     * Método que implementa la operación OpBuscarPorIdEventoEnvio del servicio
     * SrvAplEventoEnvio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param idEventoEnvioTipoList los id's de los objetos a buscar
     * @return el listado de objetos encontrados
     * @throws AppException
     */
    @Override
    public List<EventoEnvioTipo> buscarPorIdEventoEnvio(final List<String> idEventoEnvioTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idEventoEnvioTipoList == null || idEventoEnvioTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }
        final List<Long> ids = mapper.map(idEventoEnvioTipoList, String.class, Long.class);
        final List<EventoEnvio> eventoEnvioList = eventoEnvioDao.findByIdList(ids);
        final List<EventoEnvioTipo> eventoEnvioTipoList = mapper.map(eventoEnvioList, EventoEnvio.class, EventoEnvioTipo.class);
        return eventoEnvioTipoList;
    }

    /**
     * Mètodo que llena la entidad envioTipo
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param eventoEnvioTipo el objeto origen
     * @param eventoEnvio el objeto destino
     * @param errorTipoList el listado de errores de la operaciòn
     */
    private void populateEventoEnvio(final ContextoTransaccionalTipo contextoTransaccionalTipo, final EventoEnvioTipo eventoEnvioTipo,
            final EventoEnvio eventoEnvio, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(eventoEnvioTipo.getCodCausalDevolucion())) {
            ValorDominio codCausalDevolucion = valorDominioDao.find(eventoEnvioTipo.getCodCausalDevolucion());
            if (codCausalDevolucion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codCausalDevolucion con el ID: " + eventoEnvioTipo.getCodCausalDevolucion()));
            } else {
                eventoEnvio.setCodCausalDevolucion(codCausalDevolucion);
            }
        }
        if (StringUtils.isNotBlank(eventoEnvioTipo.getCodEstado())) {
            ValorDominio codEstado = valorDominioDao.find(eventoEnvioTipo.getCodEstado());
            if (codEstado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codEstado con el ID: " + eventoEnvioTipo.getCodEstado()));
            } else {
                eventoEnvio.setCodEstado(codEstado);
            }
        }
        if (StringUtils.isNotBlank(eventoEnvioTipo.getIdRadicadoSalida())) {
            eventoEnvio.setIdRadicadoSalida(eventoEnvioTipo.getIdRadicadoSalida());
        }
        if (eventoEnvioTipo.getFecRadicadoSalida() != null) {
            eventoEnvio.setFecRadicadoSalida(eventoEnvioTipo.getFecRadicadoSalida());
        }
        if (StringUtils.isNotBlank(eventoEnvioTipo.getEsAprobadoDevolucion())) {
            Validator.parseBooleanFromString(eventoEnvioTipo.getEsAprobadoDevolucion(),
                    eventoEnvioTipo.getClass().getSimpleName(),
                    "esAprobadoDevolucion", errorTipoList);
            eventoEnvio.setEsAprobadoDevolucion(mapper.map(eventoEnvioTipo.getEsAprobadoDevolucion(), Boolean.class));
        }
    }
}
