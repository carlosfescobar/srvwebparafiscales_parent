package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface GestionAdministradoraFacade extends Serializable {

    Long crearGestionAdministradora(final GestionAdministradoraTipo gestionAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarGestionAdministradora(final GestionAdministradoraTipo gestionAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<GestionAdministradoraTipo> buscarPorIdGestionAdministradora(final List<String> idGestionAdministradoraTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<GestionAdministradoraTipo> buscarPorCriteriosGestionAdministradora(List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
