package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ControlUbicacionEnvioDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.GestionAdministradoraControlUbicacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.GestionAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.GestionAdministradoraDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.SubsistemaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.ControlUbicacionEnvio;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraAccion;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraControlUbicacion;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import co.gov.ugpp.parafiscales.enums.EstadoEnvioEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.EnumMap;
import java.util.Map;

/**
 * implementación de la interfaz GestionAdministradora que contiene las
 * operaciones del servicio SrvAplGestionAdministradora.
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class GestionAdministradoraFacadeImpl extends AbstractFacade implements GestionAdministradoraFacade {

    @EJB
    private GestionAdministradoraDao gestionAdministradoraDao;

    @EJB
    private GestionAdministradoraDocumentoDao gestionAdministradoraDocumentoDao;

    @EJB
    private GestionAdministradoraControlUbicacionDao gestionAdministradoraControlUbicacionDao;

    @EJB
    private SubsistemaDao subsistemaDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FormatoDao formatoDao;
    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private ControlUbicacionEnvioDao controlUbicacionEnvioDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private ExpedienteFacade expedienteFacade;

    @EJB
    private ExpedienteDocumentoDao expedienteDocumentoDao;

    @Override
    public Long crearGestionAdministradora(GestionAdministradoraTipo gestionAdministradoraTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (gestionAdministradoraTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final GestionAdministradora gestionAdministradora = new GestionAdministradora();
        checkBusinessGestionAdministradora(gestionAdministradora, gestionAdministradoraTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        gestionAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateGestionAdministradora(gestionAdministradoraTipo, gestionAdministradora, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idGestionAdministradora = gestionAdministradoraDao.create(gestionAdministradora);
        return idGestionAdministradora;
    }

    @Override
    public void actualizarGestionAdministradora(GestionAdministradoraTipo gestionAdministradoraTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (gestionAdministradoraTipo == null || StringUtils.isBlank(gestionAdministradoraTipo.getIdGestionAdministradora())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        GestionAdministradora gestionAdministradora = gestionAdministradoraDao.find(Long.valueOf(gestionAdministradoraTipo.getIdGestionAdministradora()));
        if (gestionAdministradora == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idGestionAdministradora con el valor:" + gestionAdministradoraTipo.getIdGestionAdministradora()));
            throw new AppException(errorTipoList);
        }
        checkBusinessGestionAdministradora(gestionAdministradora, gestionAdministradoraTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        gestionAdministradora.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateGestionAdministradora(gestionAdministradoraTipo, gestionAdministradora, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        gestionAdministradoraDao.edit(gestionAdministradora);
    }

    @Override
    public List<GestionAdministradoraTipo> buscarPorIdGestionAdministradora(List<String> idGestionAdministradoraTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idGestionAdministradoraTipoList == null || idGestionAdministradoraTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idGestionAdministradoraTipoList, String.class, Long.class
        );

        final List<GestionAdministradora> gestionAdministradoraList = gestionAdministradoraDao.findByIdList(ids);
        final List<GestionAdministradoraTipo> gestionAdministradoraTipoList
                = mapper.map(gestionAdministradoraList, GestionAdministradora.class, GestionAdministradoraTipo.class);
        if (gestionAdministradoraList != null) {
            for (GestionAdministradoraTipo gestionAdministradoraTipo : gestionAdministradoraTipoList) {
                if (gestionAdministradoraTipo.getIdFuncionarioSolicitud() != null
                        && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(gestionAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                    gestionAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
                }
            }
        }

        return gestionAdministradoraTipoList;
    }

    @Override
    public PagerData<GestionAdministradoraTipo> buscarPorCriteriosGestionAdministradora(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<GestionAdministradora> pagerDataEntity = gestionAdministradoraDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        List<GestionAdministradoraTipo> gestionAdministradoraTipoList = new ArrayList<GestionAdministradoraTipo>();

        for (GestionAdministradora gestionAdministradora : pagerDataEntity.getData()) {
            final GestionAdministradoraTipo gestionAdministradoraTipo = mapper.map(gestionAdministradora, GestionAdministradoraTipo.class);
            if (gestionAdministradoraTipo.getIdFuncionarioSolicitud() != null
                    && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(gestionAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                gestionAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
            }

            gestionAdministradoraTipoList.add(gestionAdministradoraTipo);
        }
        return new PagerData(gestionAdministradoraTipoList, pagerDataEntity.getNumPages());
    }

    public void populateGestionAdministradora(GestionAdministradoraTipo gestionAdministradoraTipo, GestionAdministradora gestionAdministradora, ContextoTransaccionalTipo contextoTransaccionalTipo, List<ErrorTipo> errorTipoList) throws AppException {

        if ((gestionAdministradora.getExpediente() != null && StringUtils.isNotBlank(gestionAdministradora.getExpediente().getId()))
                && (gestionAdministradoraTipo.getExpediente() == null || StringUtils.isBlank(gestionAdministradoraTipo.getExpediente().getIdNumExpediente()))) {
            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(gestionAdministradora.getExpediente().getId());
            gestionAdministradoraTipo.setExpediente(expedienteTipo);
        }
        if (gestionAdministradoraTipo.getExpediente() != null
                && StringUtils.isNotBlank(gestionAdministradoraTipo.getExpediente().getIdNumExpediente())) {
            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);
            if (gestionAdministradoraTipo.getIdDocumentoSolicitud() != null
                    && !gestionAdministradoraTipo.getIdDocumentoSolicitud().isEmpty()) {
                for (String idDocumentoSolicitud : gestionAdministradoraTipo.getIdDocumentoSolicitud()) {
                    if (StringUtils.isNotBlank(idDocumentoSolicitud)) {
                        DocumentoEcm documentoEcm = documentoDao.find(idDocumentoSolicitud);
                        if (documentoEcm == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró un documento con el id :" + idDocumentoSolicitud));
                        } else if (gestionAdministradora.getId() != null) {
                            GestionAdministradoraDocumento gestionAdministradoraDocumentosEncontrados = gestionAdministradoraDocumentoDao.findByGestionAdministradoraAndDocumento(gestionAdministradora, documentoEcm);
                            if (gestionAdministradoraDocumentosEncontrados != null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "Ya existe la relación entre el idDocumentoSolicitud: " + documentoEcm.getId() + ", y la gestionAdministradora:" + gestionAdministradora.getId()));
                                continue;
                            }
                        }
                        ExpedienteDocumentoEcm expedienteDocumentoEcm = expedienteDocumentoDao.findDocumentoExpedienteByExpedienteAndDocumento(gestionAdministradoraTipo.getExpediente().getIdNumExpediente(), idDocumentoSolicitud);
                        if (expedienteDocumentoEcm == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró la relación entre el Expediente con ID: " + gestionAdministradoraTipo.getExpediente().getIdNumExpediente()
                                    + ", y el Documento con ID: " + idDocumentoSolicitud));
                        } else {
                            GestionAdministradoraDocumento gestionAdministradoraDocumento = new GestionAdministradoraDocumento();
                            gestionAdministradoraDocumento.setExpedienteDocumentoEcm(expedienteDocumentoEcm);
                            gestionAdministradoraDocumento.setGestionAdministradora(gestionAdministradora);
                            gestionAdministradoraDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            gestionAdministradora.getIdDocumentoSolicitud().add(gestionAdministradoraDocumento);
                        }
                    }
                }
                if (!errorTipoList.isEmpty()) {
                    throw new AppException(errorTipoList);
                }
                documentos.put(OrigenDocumentoEnum.GESTION_ADMINISTRADORA_ID_DOCUMENTO_SOLICITUD, Util.buildDocumentosFromStringList(gestionAdministradoraTipo.getIdDocumentoSolicitud()));
            }
            Expediente expediente = expedienteFacade.persistirExpedienteDocumento(gestionAdministradoraTipo.getExpediente(),
                    documentos, errorTipoList, false, contextoTransaccionalTipo);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                gestionAdministradora.setExpediente(expediente);
            }
        }
        if (gestionAdministradoraTipo.getSubsistema() != null
                && StringUtils.isNotBlank(gestionAdministradoraTipo.getSubsistema().getIdSubsistema())) {
            Subsistema subsistema = subsistemaDao.find(gestionAdministradoraTipo.getSubsistema().getIdSubsistema());
            if (subsistema == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un susbsitema con el id: " + gestionAdministradoraTipo.getSubsistema().getIdSubsistema()));
            } else {
                gestionAdministradora.setSubsistema(subsistema);
            }
        }
        if (gestionAdministradoraTipo.getAdministradora() != null
                && gestionAdministradoraTipo.getAdministradora().getIdPersona() != null) {
            IdentificacionTipo identificacionTipo = gestionAdministradoraTipo.getAdministradora().getIdPersona();
            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);
            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una Admnistradora con el valor: " + identificacion));
            } else {
                gestionAdministradora.setAdministradora(entidadExterna);
            }
        }

        if (gestionAdministradoraTipo.getExpediente() != null
                && StringUtils.isNotBlank(gestionAdministradoraTipo.getExpediente().getIdNumExpediente())) {
            Expediente expediente = expedienteDao.find(gestionAdministradoraTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un expediente con el id: " + gestionAdministradoraTipo.getExpediente().getIdNumExpediente()));
            } else {
                gestionAdministradora.setExpediente(expediente);
            }
        }
        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getCodTipoSolicitud())) {
            ValorDominio codTipoSolicitud = valorDominioDao.find(gestionAdministradoraTipo.getCodTipoSolicitud());
            if (codTipoSolicitud == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un codTipoSolicitud con el valor: " + gestionAdministradoraTipo.getCodTipoSolicitud()));
            } else {
                gestionAdministradora.setCodTipoSolicitud(codTipoSolicitud);
            }
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getCodTipoRespuesta())) {
            ValorDominio codTipoRespuesta = valorDominioDao.find(gestionAdministradoraTipo.getCodTipoRespuesta());
            if (codTipoRespuesta == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoRespuesta con el valor: " + gestionAdministradoraTipo.getCodTipoRespuesta()));
            } else {
                gestionAdministradora.setCodTipoRespuesta(codTipoRespuesta);
            }
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getCodTipoPeriodicidad())) {
            ValorDominio codTipoPeriodicidad = valorDominioDao.find(gestionAdministradoraTipo.getCodTipoPeriodicidad());
            if (codTipoPeriodicidad == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoPeriodicidad con el valor: " + gestionAdministradoraTipo.getCodTipoPeriodicidad()));
            } else {
                gestionAdministradora.setCodTipoPeriodicidad(codTipoPeriodicidad);
            }
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getCodTipoPlazoEntrega())) {
            ValorDominio codTipoPlazoEntrega = valorDominioDao.find(gestionAdministradoraTipo.getCodTipoPlazoEntrega());
            if (codTipoPlazoEntrega == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoPlazoEntrega con el valor:" + gestionAdministradoraTipo.getCodTipoPlazoEntrega()));
            } else {
                gestionAdministradora.setCodTipoPlazoEntrega(codTipoPlazoEntrega);
            }
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getValCantidadPlazoEntrega())) {
            gestionAdministradora.setValCantidadPlazoEntrega(gestionAdministradoraTipo.getValCantidadPlazoEntrega());
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getValNombreSolicitud())) {
            gestionAdministradora.setValNombreSolicitud(gestionAdministradoraTipo.getValNombreSolicitud());
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getDescInformacionRequerida())) {
            gestionAdministradora.setDescInformacionRequerida(gestionAdministradoraTipo.getDescInformacionRequerida());
        }

        if (gestionAdministradoraTipo.getIdFuncionarioSolicitud() != null
                && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(gestionAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Debe proporcionar un funcionario existente, ID: " + gestionAdministradoraTipo.getIdFuncionarioSolicitud()));
            } else {
                gestionAdministradora.setIdFuncionarioSolicitud(funcionarioTipo.getIdFuncionario());
            }

        }

        if (gestionAdministradoraTipo.getFecFinSolicitud() != null) {
            gestionAdministradora.setFecFinSolicitud(gestionAdministradoraTipo.getFecFinSolicitud());
        }

        if (gestionAdministradoraTipo.getFecInicioSolicitud() != null) {
            gestionAdministradora.setFecInicioSolicitud(gestionAdministradoraTipo.getFecInicioSolicitud());
        }

        if (gestionAdministradoraTipo.getFecProximaEspera() != null) {
            gestionAdministradora.setFecProximaEspera(gestionAdministradoraTipo.getFecProximaEspera());

        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getCodEstadoGestion())) {
            ValorDominio codEstadoGestion = valorDominioDao.find(gestionAdministradoraTipo.getCodEstadoGestion());
            if (codEstadoGestion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoGestion con el valor: " + gestionAdministradoraTipo.getCodEstadoGestion()));
            } else {
                gestionAdministradora.setCodEstadoGestion(codEstadoGestion);
            }
        }

        if (StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFormatoElegido())
                && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdVersionElegido())) {
            Formato formato = formatoDao.findFormatoByFormatoAndVersion(Long.valueOf(gestionAdministradoraTipo.getIdFormatoElegido()), Long.valueOf(gestionAdministradoraTipo.getIdVersionElegido()));
            if (formato == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idFormato con el valor " + gestionAdministradoraTipo.getIdFormatoElegido() + ",  y la versión " + gestionAdministradoraTipo.getIdVersionElegido()));
            } else {
                gestionAdministradora.setFormatoEelegido(formato);
            }

        }

        if (gestionAdministradoraTipo.getAcciones() != null
                && !gestionAdministradoraTipo.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : gestionAdministradoraTipo.getAcciones()) {
                final Accion accion = mapper.map(accionTipo, Accion.class);
                accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = valorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        accion.setCodAccion(codAccion);
                    }
                }
                GestionAdministradoraAccion gestionAdministradoraAccion = new GestionAdministradoraAccion();
                gestionAdministradoraAccion.setAccion(accion);
                gestionAdministradoraAccion.setGestionAdministradora(gestionAdministradora);
                gestionAdministradoraAccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                gestionAdministradora.getAcciones().add(gestionAdministradoraAccion);
            }
        }

        if (gestionAdministradoraTipo.getControlUbicaciones() != null
                && !gestionAdministradoraTipo.getControlUbicaciones().isEmpty()) {
            GestionAdministradoraControlUbicacion gestionAdministradoraControlUbicacion = new GestionAdministradoraControlUbicacion();
            for (ControlUbicacionEnvioTipo controlUbicacionEnvioTipo : gestionAdministradoraTipo.getControlUbicaciones()) {
                if (controlUbicacionEnvioTipo != null && StringUtils.isNotBlank(controlUbicacionEnvioTipo.getIdControlUbicacion())) {
                    ControlUbicacionEnvio controlUbicacionEnvio = controlUbicacionEnvioDao.find(Long.valueOf(controlUbicacionEnvioTipo.getIdControlUbicacion()));
                    if (controlUbicacionEnvio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un controlUbicacionEnvio con el id :" + controlUbicacionEnvioTipo.getIdControlUbicacion()));
                    } else if (gestionAdministradora.getId() != null) {
                        GestionAdministradoraControlUbicacion gestionAdministradoraUbicacion = gestionAdministradoraControlUbicacionDao.findByGestionAdministradoraAndControlUbicacion(gestionAdministradora, controlUbicacionEnvio);
                        if (gestionAdministradoraUbicacion != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre el controlUbicacionEnvio: " + controlUbicacionEnvio.getId() + ", y la gestionAdministradora:" + gestionAdministradora.getId()));
                        } else {

                            gestionAdministradoraControlUbicacion.setControlUbicacionEnvio(controlUbicacionEnvio);
                            gestionAdministradoraControlUbicacion.setGestionAdministradora(gestionAdministradora);
                            gestionAdministradoraControlUbicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        }
                    }

                    gestionAdministradoraControlUbicacion.setControlUbicacionEnvio(controlUbicacionEnvio);
                    gestionAdministradoraControlUbicacion.setGestionAdministradora(gestionAdministradora);
                    gestionAdministradoraControlUbicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    gestionAdministradora.getControlUbicaciones().add(gestionAdministradoraControlUbicacion);
                }
            }
        }

    }

    /**
     * metodo para validar si ya existe más de un Control Envío Ubicación en
     * estado enviado asocioado a una Gestión Administradora
     *
     * @param gestionAdministradora la Gestión Administradora a crear o
     * actualizar
     * @param gestionAdministradoraTipo objeto que contiene los datos a ingresar
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void checkBusinessGestionAdministradora(GestionAdministradora gestionAdministradora, GestionAdministradoraTipo gestionAdministradoraTipo,
            List<ErrorTipo> errorTipoList) throws AppException {

        if (gestionAdministradoraTipo.getControlUbicaciones() != null
                && !gestionAdministradoraTipo.getControlUbicaciones().isEmpty()) {

            int estadoEnvio = 0;
            if (gestionAdministradora.getId() != null) {
                GestionAdministradoraControlUbicacion estadoUbicacion = gestionAdministradoraControlUbicacionDao.findByEstadoUbicacion(gestionAdministradora, EstadoEnvioEnum.ENVIADO.getCode());

                if (estadoUbicacion != null) {
                    estadoEnvio++;
                }
            }
            for (ControlUbicacionEnvioTipo controlUbicacionEnvioTipo : gestionAdministradoraTipo.getControlUbicaciones()) {
                if (controlUbicacionEnvioTipo != null && StringUtils.isNotBlank(controlUbicacionEnvioTipo.getIdControlUbicacion())) {
                    ControlUbicacionEnvio controlUbicacion = controlUbicacionEnvioDao.find(Long.valueOf(controlUbicacionEnvioTipo.getIdControlUbicacion()));
                    if (controlUbicacion != null
                            && StringUtils.isNotBlank(controlUbicacion.getCodEstadoUbicacion().getId())) {
                        if (controlUbicacion.getCodEstadoUbicacion().getId().equals(EstadoEnvioEnum.ENVIADO.getCode())) {
                            estadoEnvio++;
                        }
                    }
                }
            }
            if (estadoEnvio > Constants.ENVIADO) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, " ya existe más de un Control Ubicación asociado a Gestion Administradora en estado de envío"));
            }
        }
    }

}
