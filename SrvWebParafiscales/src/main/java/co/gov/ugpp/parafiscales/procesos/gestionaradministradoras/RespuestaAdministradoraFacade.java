package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1.RespuestaAdministradoraTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface RespuestaAdministradoraFacade extends Serializable {

    Long crearRespuestaAdministradora(final RespuestaAdministradoraTipo respuestaAdministradoraTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarRespuestaAdministradora(final RespuestaAdministradoraTipo respuestaAdministradoraTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<RespuestaAdministradoraTipo> buscarPorIdRespuestaAdministradora(final List<String> idRespuestaAdministradoraList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
