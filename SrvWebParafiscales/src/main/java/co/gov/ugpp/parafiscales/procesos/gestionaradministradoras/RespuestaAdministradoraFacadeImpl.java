package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.GestionAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaAdministradoraDecisionCasoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradoraDecisionCaso;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1.RespuestaAdministradoraTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz RespuestaAdministradoraFacade que contiene las
 * operaciones del servicio SrvAplRespuestaAdministradora
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RespuestaAdministradoraFacadeImpl extends AbstractFacade implements RespuestaAdministradoraFacade {

    @EJB
    private ExpedienteFacade expedienteFacade;

    @EJB
    private RespuestaAdministradoraDao respuestaAdministradoraDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private GestionAdministradoraDao gestionAdministradoraDao;

    @EJB
    private RespuestaAdministradoraDecisionCasoDao respuestaAdministradoraDecisionCasoDao;

    /**
     * Método que implementa la operación OpCrearRespuestaAdministradora del
     * servicio SrvAplRespuestaAdministradora
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param respuestaAdministradoraTipo el objeto origen
     * @return el id de negocio creado
     * @throws AppException
     */
    @Override
    public Long crearRespuestaAdministradora(final RespuestaAdministradoraTipo respuestaAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (respuestaAdministradoraTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        RespuestaAdministradora respuestaAdministradora = new RespuestaAdministradora();
        respuestaAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateRespuestaAdministradora(contextoTransaccionalTipo, errorTipoList, respuestaAdministradora, respuestaAdministradoraTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return respuestaAdministradoraDao.create(respuestaAdministradora);
    }

    /**
     * Método que implementa la operación OpActualizarRespuestaAdministradora
     * del servicio SrvAplRespuestaAdministradora
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param respuestaAdministradoraTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarRespuestaAdministradora(final RespuestaAdministradoraTipo respuestaAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (respuestaAdministradoraTipo == null || StringUtils.isBlank(respuestaAdministradoraTipo.getIdRespuestaAdministradora())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final RespuestaAdministradora respuestaAdministradora = respuestaAdministradoraDao.find(Long.valueOf(respuestaAdministradoraTipo.getIdRespuestaAdministradora()));
        if (respuestaAdministradora == null) {
            throw new AppException("No se encontró Respuesta Administradora con el ID: " + respuestaAdministradoraTipo.getIdRespuestaAdministradora());
        }
        respuestaAdministradora.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateRespuestaAdministradora(contextoTransaccionalTipo, errorTipoList, respuestaAdministradora, respuestaAdministradoraTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        respuestaAdministradoraDao.edit(respuestaAdministradora);
    }

    /**
     * Método que implementa la operación OpBuscarPorIdRespuestaAdministradora
     * del servicio SrvAplRespuestaAdministradora
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param idRespuestaAdministradoraList los id's de los objetos a buscar
     * @return el listado de objetos encontrados
     * @throws AppException
     */
    @Override
    public List<RespuestaAdministradoraTipo> buscarPorIdRespuestaAdministradora(final List<String> idRespuestaAdministradoraList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idRespuestaAdministradoraList == null || idRespuestaAdministradoraList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }
        final List<Long> ids = mapper.map(idRespuestaAdministradoraList, String.class, Long.class);
        final List<RespuestaAdministradora> respuestaAdministradoraList = respuestaAdministradoraDao.findByIdList(ids);
        final List<RespuestaAdministradoraTipo> respuestaAdministradoraTipoList = mapper.map(respuestaAdministradoraList, RespuestaAdministradora.class, RespuestaAdministradoraTipo.class);
        return respuestaAdministradoraTipoList;
    }

    /**
     * Mètodo que llena la respuestaAdministradora
     *
     * @param contextoTransaccionalTipo contiene los datos de auditorìa
     * @param errorTipoList la pila de erroes de la operaciòn
     * @param respuestaAdministradora el objeto destino
     * @param respuestaAdministradoraTipo el objeto origen
     * @throws AppException
     */
    private void populateRespuestaAdministradora(final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList,
            final RespuestaAdministradora respuestaAdministradora, final RespuestaAdministradoraTipo respuestaAdministradoraTipo) throws AppException {

        if ((respuestaAdministradora.getExpediente() != null && StringUtils.isNotBlank(respuestaAdministradora.getExpediente().getId()))
                && (respuestaAdministradoraTipo.getExpediente() == null || StringUtils.isBlank(respuestaAdministradoraTipo.getExpediente().getIdNumExpediente()))) {
            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(respuestaAdministradora.getExpediente().getId());
            respuestaAdministradoraTipo.setExpediente(expedienteTipo);
        }
        if (respuestaAdministradoraTipo.getExpediente() != null
                && StringUtils.isNotBlank(respuestaAdministradoraTipo.getExpediente().getIdNumExpediente())) {
            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);

            List<DocumentoTipo> documentosxOrigen;

            if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getIdDocumentoEntidadVigilancia())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo docuumentoEntidadVigilancia = new DocumentoTipo();
                docuumentoEntidadVigilancia.setIdDocumento(respuestaAdministradoraTipo.getIdDocumentoEntidadVigilancia());
                documentosxOrigen.add(docuumentoEntidadVigilancia);
                documentos.put(OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_ENTIDAD_VIGILANCIA, documentosxOrigen);
            }
            if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getIdDocumentoProrroga())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoProrroga = new DocumentoTipo();
                documentoProrroga.setIdDocumento(respuestaAdministradoraTipo.getIdDocumentoProrroga());
                documentosxOrigen.add(documentoProrroga);
                documentos.put(OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_PRORROGA, documentosxOrigen);
            }
            if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getIdDocumentoReenvio())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoReenvio = new DocumentoTipo();
                documentoReenvio.setIdDocumento(respuestaAdministradoraTipo.getIdDocumentoReenvio());
                documentosxOrigen.add(documentoReenvio);
                documentos.put(OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_REENVIO, documentosxOrigen);
            }
            Expediente expediente = expedienteFacade.persistirExpedienteDocumento(respuestaAdministradoraTipo.getExpediente(),
                    documentos, errorTipoList, true, contextoTransaccionalTipo);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                respuestaAdministradora.setExpediente(expediente);
            }
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getIdGestionAdministradoraTipo())) {
            GestionAdministradora gestionAdministradora = gestionAdministradoraDao.find(Long.valueOf(respuestaAdministradoraTipo.getIdGestionAdministradoraTipo()));
            if (gestionAdministradora == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró Gestión Administradora con el ID: " + respuestaAdministradoraTipo.getIdGestionAdministradoraTipo()));
            } else {
                respuestaAdministradora.setGestionAdministradora(gestionAdministradora);
            }
        }
        if (respuestaAdministradoraTipo.getCodDecisionCaso() != null
                && !respuestaAdministradoraTipo.getCodDecisionCaso().isEmpty()) {
            RespuestaAdministradoraDecisionCaso respuestaAdministradoraDecisionCaso;
            for (String codDecisionCaso : respuestaAdministradoraTipo.getCodDecisionCaso()) {
                if (StringUtils.isNotBlank(codDecisionCaso)) {
                    ValorDominio valorDominio = valorDominioDao.find(codDecisionCaso);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codDecisionCaso con el ID: " + codDecisionCaso));
                    } else if (respuestaAdministradora.getId() != null) {
                        respuestaAdministradoraDecisionCaso = respuestaAdministradoraDecisionCasoDao.findByRespuestaAdminAndCodDecisionCaso(respuestaAdministradora, valorDominio);
                        if (respuestaAdministradoraDecisionCaso != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre codDecisionCaso: " + valorDominio.getId() + ", y la respuesta Administradora:" + respuestaAdministradora.getId()));
                        } else {
                            respuestaAdministradoraDecisionCaso = crearRespuestaAdministradoraDecisionCaso(valorDominio, respuestaAdministradora, contextoTransaccionalTipo);
                            respuestaAdministradora.getCodDecisionCaso().add(respuestaAdministradoraDecisionCaso);

                        }
                    } else {
                        respuestaAdministradoraDecisionCaso = crearRespuestaAdministradoraDecisionCaso(valorDominio, respuestaAdministradora, contextoTransaccionalTipo);
                        respuestaAdministradora.getCodDecisionCaso().add(respuestaAdministradoraDecisionCaso);
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getCodTipoSancion())) {
            ValorDominio codTipoSancion = valorDominioDao.find(respuestaAdministradoraTipo.getCodTipoSancion());
            if (codTipoSancion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoSancion con el ID: " + respuestaAdministradoraTipo.getCodTipoSancion()));
            } else {
                respuestaAdministradora.setCodTipoSancion(codTipoSancion);
            }
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getCodTipoRespuesta())) {
            ValorDominio codTipoRespuesta = valorDominioDao.find(respuestaAdministradoraTipo.getCodTipoRespuesta());
            if (codTipoRespuesta == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoRespuesta con el ID: " + respuestaAdministradoraTipo.getCodTipoRespuesta()));
            } else {
                respuestaAdministradora.setCodTipoRespuesta(codTipoRespuesta);
            }
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getEsConcedidaProrroga())) {
            Validator.parseBooleanFromString(respuestaAdministradoraTipo.getEsConcedidaProrroga(),
                    respuestaAdministradoraTipo.getClass().getSimpleName(),
                    "esConcedidaProrroga", errorTipoList);
            respuestaAdministradora.setEsConcedidaProrroga(mapper.map(respuestaAdministradoraTipo.getEsConcedidaProrroga(), Boolean.class));
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getEsInformacionCorrecta())) {
            Validator.parseBooleanFromString(respuestaAdministradoraTipo.getEsInformacionCorrecta(),
                    respuestaAdministradoraTipo.getClass().getSimpleName(),
                    "esInformacionCorrecta", errorTipoList);
            respuestaAdministradora.setEsInformacionCorrecta(mapper.map(respuestaAdministradoraTipo.getEsInformacionCorrecta(), Boolean.class));
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getEsProrroga())) {
            Validator.parseBooleanFromString(respuestaAdministradoraTipo.getEsProrroga(),
                    respuestaAdministradoraTipo.getClass().getSimpleName(),
                    "esProrroga", errorTipoList);
            respuestaAdministradora.setEsProrroga(mapper.map(respuestaAdministradoraTipo.getEsProrroga(), Boolean.class));
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getValDiasConcedidosProrroga())) {
            respuestaAdministradora.setDiasConcedidosProrroga(Long.valueOf(respuestaAdministradoraTipo.getValDiasConcedidosProrroga()));
        }
        if (respuestaAdministradoraTipo.getFecInicioEspera() != null) {
            respuestaAdministradora.setFecInicioEspera(respuestaAdministradoraTipo.getFecInicioEspera());
        }
        if (respuestaAdministradoraTipo.getFecRespuesta() != null) {
            respuestaAdministradora.setFecRespuesta(respuestaAdministradoraTipo.getFecRespuesta());
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getIdRadicadoEntrada())) {
            respuestaAdministradora.setIdRadicadoEntrada(respuestaAdministradoraTipo.getIdRadicadoEntrada());
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getDescObservacionesEntidadVigilancia())) {
            respuestaAdministradora.setDescObservacionesEntidadVigilancia(respuestaAdministradoraTipo.getDescObservacionesEntidadVigilancia());
        }
        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getDescObservacionesReenvio())) {
            respuestaAdministradora.setDescObservacionesReenvio(respuestaAdministradoraTipo.getDescObservacionesReenvio());
        }

        if (StringUtils.isNotBlank(respuestaAdministradoraTipo.getDescObservacionesSancion())) {
            respuestaAdministradora.setDescObservacionesSancion(respuestaAdministradoraTipo.getDescObservacionesSancion());
        }
    }

    private RespuestaAdministradoraDecisionCaso crearRespuestaAdministradoraDecisionCaso(ValorDominio valorDominio, RespuestaAdministradora respuestaAdministradora, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        RespuestaAdministradoraDecisionCaso respuestaAdministradoraDecisionCaso = new RespuestaAdministradoraDecisionCaso();
        respuestaAdministradoraDecisionCaso.setCodDecisionCaso(valorDominio);
        respuestaAdministradoraDecisionCaso.setRespuestaAdministradora(respuestaAdministradora);
        respuestaAdministradoraDecisionCaso.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return respuestaAdministradoraDecisionCaso;
    }
}
