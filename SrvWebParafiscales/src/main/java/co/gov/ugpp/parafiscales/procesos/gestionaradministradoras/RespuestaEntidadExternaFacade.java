package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author vladimir.garcia
 */
public interface RespuestaEntidadExternaFacade extends Serializable {

    void actualizarSolicitudEntidadExterna(SolicitudEntidadExternaTipo solicitudEntidadExternaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

     PagerData<SolicitudEntidadExternaTipo> buscarPorCriteriosSolicitudEntidadExternaRespTipo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
}
