package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudEntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudEntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz RespuestaEntidadExternaFacade que contiene las operaciones del servicio
 * SrvAplRespuestaEntidadExterna
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RespuestaEntidadExternaFacadeImpl extends AbstractFacade implements RespuestaEntidadExternaFacade {

    @EJB
    private SolicitudEntidadExternaDao solicitudEntidadExternaDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * implementación de la operación actualizarSolicitudEntidadExterna esta se encarga de actualizar una
     * solicitudEntidadExterna de acuerdo a un id enviado
     *
     * @param solicitudEntidadExternaTipo Objeto a Actualizar en la base de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public void actualizarSolicitudEntidadExterna(final SolicitudEntidadExternaTipo solicitudEntidadExternaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (solicitudEntidadExternaTipo == null
                || StringUtils.isBlank(solicitudEntidadExternaTipo.getIdSolicitudEntidadExterna())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final SolicitudEntidadExterna solicitudEntidadExterna = solicitudEntidadExternaDao.find(Long.valueOf(solicitudEntidadExternaTipo.getIdSolicitudEntidadExterna()));

        if (solicitudEntidadExterna == null) {
            throw new AppException("La solicitudEntidadExterna que desea Actualizar no existe con el id:"
                    + solicitudEntidadExternaTipo.getIdSolicitudEntidadExterna());
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        solicitudEntidadExterna.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.pupulateSolicitudEntidadExterna(solicitudEntidadExternaTipo, solicitudEntidadExterna, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        solicitudEntidadExternaDao.edit(solicitudEntidadExterna);
    }

    /**
     * implementación de la operación buscarPorCriteriosSolicitudEntidadExterna se encarga de buscar una
     * SolicitudEntidadExterna por medio de un listado de parametros y ordenar la consulta de acuerdo a los criterios
     * establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a buscar una SolicitudEntidadExterna
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return listado de SolicitudEntidadExterna encontrado que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de servicios.
     */
    @Override
    public PagerData<SolicitudEntidadExternaTipo> buscarPorCriteriosSolicitudEntidadExternaRespTipo(
            final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<SolicitudEntidadExterna> pagerDataEntity
                = solicitudEntidadExternaDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<SolicitudEntidadExternaTipo> solicitudEntidadExternaTipoList = mapper.map(pagerDataEntity.getData(),
                SolicitudEntidadExterna.class, SolicitudEntidadExternaTipo.class);

        return new PagerData<SolicitudEntidadExternaTipo>(solicitudEntidadExternaTipoList, pagerDataEntity.getNumPages());
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad noAportante
     *
     * @param solicitudEntidadExternaTipo Objeto Origen
     * @param solicitudEntidadExterna Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de servicios.
     */
    private void pupulateSolicitudEntidadExterna(final SolicitudEntidadExternaTipo solicitudEntidadExternaTipo,
            final SolicitudEntidadExterna solicitudEntidadExterna, final List<ErrorTipo> errorTipoList) throws AppException {

        if (solicitudEntidadExternaTipo.getEntidadExterna() != null
                && StringUtils.isNotBlank(solicitudEntidadExternaTipo.getEntidadExterna().getIdEntidadExterna())) {
            final EntidadExterna entidadExterna
                    = entidadExternaDao.find(Long.valueOf(solicitudEntidadExternaTipo.getEntidadExterna().getIdEntidadExterna()));
            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La entidadExterna proporcionada no existe con el ID: " + solicitudEntidadExternaTipo.getEntidadExterna().getIdEntidadExterna()));
            } else {
                solicitudEntidadExterna.setEntidadExterna(entidadExterna);
            }
        }

        if (solicitudEntidadExternaTipo.getFecRadicadoRespuesta() != null) {
            solicitudEntidadExterna.setFecRadicadoRespuesta(solicitudEntidadExternaTipo.getFecRadicadoRespuesta());
        }

        if (StringUtils.isNotBlank(solicitudEntidadExternaTipo.getIdRadicadoRespuesta())) {
            solicitudEntidadExterna.setNumeroRadicadoRespuesta(Long.valueOf(solicitudEntidadExternaTipo.getIdRadicadoRespuesta()));
        }

        if (StringUtils.isNotBlank(solicitudEntidadExternaTipo.getEsValidaRespuesta())) {
            Validator.parseBooleanFromString(solicitudEntidadExternaTipo.getEsValidaRespuesta(),
                    solicitudEntidadExternaTipo.getClass().getSimpleName(),
                    "esValidaRespuesta", errorTipoList);
            solicitudEntidadExterna.setEsValidaRespuesta(mapper.map(solicitudEntidadExternaTipo.getEsValidaRespuesta(), Boolean.class));
        }

        if (StringUtils.isNotBlank(solicitudEntidadExternaTipo.getValObservacionesRespuesta())) {
            solicitudEntidadExterna.setValObservacionesRespuesta(solicitudEntidadExternaTipo.getValObservacionesRespuesta());
        }

        if (StringUtils.isNotBlank(solicitudEntidadExternaTipo.getValNumeroReiteracion())) {
            solicitudEntidadExterna.setValNumeroReiteracion(Long.valueOf(solicitudEntidadExternaTipo.getValNumeroReiteracion()));
        }

        if (StringUtils.isNotBlank(solicitudEntidadExternaTipo.getCodTipoEstado())) {
            final ValorDominio codTipoEstado = valorDominioDao.find(solicitudEntidadExternaTipo.getCodTipoEstado());
            if (codTipoEstado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoEstado no existe con el ID: " + solicitudEntidadExternaTipo.getCodTipoEstado()));
            } else {
                solicitudEntidadExterna.setCodTipoEstado(codTipoEstado);
            }
        }
    }
}
