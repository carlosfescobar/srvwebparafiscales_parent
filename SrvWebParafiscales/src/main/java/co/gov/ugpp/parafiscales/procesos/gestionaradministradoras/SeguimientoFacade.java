package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import java.io.Serializable;

/**
 *
 * @author jmuncab
 */
public interface SeguimientoFacade extends Serializable {

    Long crearSeguimiento(SeguimientoTipo seguimientoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
