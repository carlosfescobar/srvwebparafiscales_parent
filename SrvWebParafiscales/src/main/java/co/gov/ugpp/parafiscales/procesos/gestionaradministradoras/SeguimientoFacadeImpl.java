package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.SeguimientoDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Seguimiento;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz SeguimientoFacade que contiene las operaciones del servicio SrvAplSeguimiento
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SeguimientoFacadeImpl extends AbstractFacade implements SeguimientoFacade {

    @EJB
    private SeguimientoDao seguimientoDao;

    @EJB
    private ExpedienteDao expedienteDao;

    /**
     * implementación de la operación crearSeguimiento se encarga de crear un seguimiento nuevo en la base de datos a
     * partir del SeguimientoTipo enviado
     *
     * @param seguimientoTipo objeto a partir del cual se va a crear un nuevo seguimiento en la base de datos.
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria.
     * @return seguimiento creado que se retorna junto con el contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public Long crearSeguimiento(final SeguimientoTipo seguimientoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkSeguimiento(seguimientoTipo, errorTipoList);

        final Seguimiento seguimiento = new Seguimiento();

        seguimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSeguimiento(seguimientoTipo, seguimiento, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        return seguimientoDao.create(seguimiento);
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad Seguimiento
     *
     * @param seguimientoTipo Objeto origen
     * @param seguimiento Objeto destino
     * @param ctt Contiene la información para almacenar la auditoria.
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    private void populateSeguimiento(final SeguimientoTipo seguimientoTipo, final Seguimiento seguimiento,
            final ContextoTransaccionalTipo ctt, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(seguimientoTipo.getIdExpediente())) {
            final Expediente expediente = expedienteDao.find(seguimientoTipo.getIdExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente proporcionado no existe con el ID: " + seguimientoTipo.getIdExpediente()));
            } else {
                seguimiento.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(seguimientoTipo.getValCargoPersonaContactada())) {
            seguimiento.setCargoPersonaContactada(seguimientoTipo.getValCargoPersonaContactada());
        }

        if (StringUtils.isNotBlank(seguimientoTipo.getDesObservacionProgramacionSeguimiento())) {
            seguimiento.setDescObsProgramacion(seguimientoTipo.getDesObservacionProgramacionSeguimiento());
        }

        if (StringUtils.isNotBlank(seguimientoTipo.getDescObservaciones())) {
            seguimiento.setDescObservaciones(seguimientoTipo.getDescObservaciones());
        }

        if (StringUtils.isNotBlank(seguimientoTipo.getValEncargadoSeguimiento())) {
            seguimiento.setValUsuarioEncargado(seguimientoTipo.getValEncargadoSeguimiento());
        }

        if (StringUtils.isNotBlank(seguimientoTipo.getValPlazoEntrega())) {
            seguimiento.setPlazoEntrega(seguimientoTipo.getValPlazoEntrega());
        }

        if (seguimientoTipo.getFecProgramadaSeguimiento() != null) {
            seguimiento.setFecProgramadaSeguimiento(seguimientoTipo.getFecProgramadaSeguimiento());
        }
    }
}
