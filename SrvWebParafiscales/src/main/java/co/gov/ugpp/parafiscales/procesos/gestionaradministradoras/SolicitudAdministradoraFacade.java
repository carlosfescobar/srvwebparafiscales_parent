package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1.SolicitudAdministradoraTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Everis
 */
public interface SolicitudAdministradoraFacade extends Serializable {

    Long crearSolicitudAdministradora(final SolicitudAdministradoraTipo solicitudAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarSolicitudAdministradora(final SolicitudAdministradoraTipo solicitudAdministradoraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SolicitudAdministradoraTipo> buscarPorIdSolicitudAdministradora(final List<String> idSolicitudAdministradoraTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<SolicitudAdministradoraTipo> buscarPorCriteriosSolicitudAdministradora(List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
