package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionSubsistemaAdminDao;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionSubsistemaSolAdminDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.GestionAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.GestionSolAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionEstructuraDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaSolAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionSolAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraAccion;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraCorreccion;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionEstructura;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1.SolicitudAdministradoraTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import com.jcabi.immutable.Array;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz GestionAdministradora que contiene las
 * operaciones del servicio SrvAplGestionAdministradora.
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SolicitudAdministradoraFacadeImpl extends AbstractFacade implements SolicitudAdministradoraFacade {

    @EJB
    private SolicitudAdministradoraDao solicitudAdministradoraDao;

    @EJB
    private AgrupacionSubsistemaAdminDao agrupacionSubsistemaDao;

    @EJB
    private AgrupacionSubsistemaSolAdminDao agrupacionSubsistemaSolDao;

    @EJB
    private GestionSolAdministradoraDao gestionSolAdministradoraDao;

    @EJB
    private GestionAdministradoraDao gestionAdministradoraDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private ValidacionEstructuraDao validacionEstructuraDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ArchivoDao archivoDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @Override
    public Long crearSolicitudAdministradora(SolicitudAdministradoraTipo solicitudAdministradoraTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        if (solicitudAdministradoraTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final SolicitudAdministradora solicitudAdministradora = new SolicitudAdministradora();
        solicitudAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateSolicitudAdministradora(solicitudAdministradoraTipo, solicitudAdministradora, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idGestionAdministradora = solicitudAdministradoraDao.create(solicitudAdministradora);
        return idGestionAdministradora;
    }

    @Override
    public void actualizarSolicitudAdministradora(SolicitudAdministradoraTipo solicitudAdministradoraTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (solicitudAdministradoraTipo == null || StringUtils.isBlank(solicitudAdministradoraTipo.getIdSolicitudAdministradoras())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        SolicitudAdministradora solicitudAdministradora = solicitudAdministradoraDao.find(Long.valueOf(solicitudAdministradoraTipo.getIdSolicitudAdministradoras()));

        if (solicitudAdministradora == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idSolicitudAdministradora con el valor:" + solicitudAdministradoraTipo.getIdSolicitudAdministradoras()));
            throw new AppException(errorTipoList);
        }
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        solicitudAdministradora.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateSolicitudAdministradora(solicitudAdministradoraTipo, solicitudAdministradora, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        solicitudAdministradoraDao.edit(solicitudAdministradora);
    }

    @Override
    public List<SolicitudAdministradoraTipo> buscarPorIdSolicitudAdministradora(List<String> idSolicitudAdministradoraTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idSolicitudAdministradoraTipoList == null || idSolicitudAdministradoraTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idSolicitudAdministradoraTipoList, String.class, Long.class);
        final List<SolicitudAdministradora> solicitudAdministradoraList = solicitudAdministradoraDao.findByIdList(ids);

        final List<SolicitudAdministradoraTipo> solicitudAdministradoraTipoList
                = mapper.map(solicitudAdministradoraList, SolicitudAdministradora.class, SolicitudAdministradoraTipo.class);

        if (solicitudAdministradoraList != null) {
            for (SolicitudAdministradoraTipo solicitudAdministradoraTipo : solicitudAdministradoraTipoList) {
                if (solicitudAdministradoraTipo.getIdFuncionarioSolicitud() != null
                        && StringUtils.isNotBlank(solicitudAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                    solicitudAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
                }
                if (solicitudAdministradoraTipo.getAdministradorasDefinitivas() != null) {
                    for (GestionAdministradoraTipo gestionAdministradoraTipo : solicitudAdministradoraTipo.getAdministradorasDefinitivas()) {
                        if (gestionAdministradoraTipo.getIdFuncionarioSolicitud() != null
                                && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(gestionAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                            gestionAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
                        }

                    }
                }
            }

        }

        return solicitudAdministradoraTipoList;
    }

    @Override
    public PagerData<SolicitudAdministradoraTipo> buscarPorCriteriosSolicitudAdministradora(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<SolicitudAdministradora> pagerDataEntity = solicitudAdministradoraDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<SolicitudAdministradoraTipo> solicitudAdministradoraTipoList = new ArrayList<SolicitudAdministradoraTipo>();

        for (final SolicitudAdministradora solicitudAdministradora : pagerDataEntity.getData()) {
            final SolicitudAdministradoraTipo solicitudAdministradoraTipo = mapper.map(solicitudAdministradora, SolicitudAdministradoraTipo.class);
            if (solicitudAdministradoraTipo.getIdFuncionarioSolicitud() != null
                    && StringUtils.isNotBlank(solicitudAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                solicitudAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
            }
             if (solicitudAdministradoraTipo.getAdministradorasDefinitivas() != null) {
                    for (GestionAdministradoraTipo gestionAdministradoraTipo : solicitudAdministradoraTipo.getAdministradorasDefinitivas()) {
                        if (gestionAdministradoraTipo.getIdFuncionarioSolicitud() != null
                                && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(gestionAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);
                            gestionAdministradoraTipo.setIdFuncionarioSolicitud(funcionarioTipo);
                        }

                    }
                }
            solicitudAdministradoraTipoList.add(solicitudAdministradoraTipo);
        }

        return new PagerData(solicitudAdministradoraTipoList, pagerDataEntity.getNumPages());

    }

    public void populateSolicitudAdministradora(SolicitudAdministradoraTipo solicitudAdministradoraTipo, SolicitudAdministradora solicitudAdministradora, ContextoTransaccionalTipo contextoTransaccionalTipo, List<ErrorTipo> errorTipoList) throws AppException {
        //Validacion Expediente
        if (solicitudAdministradoraTipo.getExpediente() != null
                && StringUtils.isNotBlank(solicitudAdministradoraTipo.getExpediente().getIdNumExpediente())) {
            Expediente expediente = expedienteDao.find(solicitudAdministradoraTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un expediente con el id: " + solicitudAdministradoraTipo.getExpediente().getIdNumExpediente()));
            } else {
                solicitudAdministradora.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getIdArchivoSolicitudInicial())) {
            Archivo archivo = archivoDao.find(Long.valueOf(solicitudAdministradoraTipo.getIdArchivoSolicitudInicial()));
            if (archivo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un archivo con el valor: " + solicitudAdministradoraTipo.getIdArchivoSolicitudInicial()));
            } else {
                solicitudAdministradora.setIdArchivoSolicitudInicial(archivo);
            }
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getCodTipoSolicitud())) {
            ValorDominio codTipoSolicitud = valorDominioDao.find(solicitudAdministradoraTipo.getCodTipoSolicitud());
            if (codTipoSolicitud == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un codTipoSolicitud con el valor: " + solicitudAdministradoraTipo.getCodTipoSolicitud()));
            } else {
                solicitudAdministradora.setCodTipoSolicitud(codTipoSolicitud);
            }
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getCodTipoRespuesta())) {
            ValorDominio codTipoRespuesta = valorDominioDao.find(solicitudAdministradoraTipo.getCodTipoRespuesta());
            if (codTipoRespuesta == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoRespuesta con el valor: " + solicitudAdministradoraTipo.getCodTipoRespuesta()));
            } else {
                solicitudAdministradora.setCodTipoRespuesta(codTipoRespuesta);
            }
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getCodTipoPeriodicidad())) {
            ValorDominio codTipoPeriodicidad = valorDominioDao.find(solicitudAdministradoraTipo.getCodTipoPeriodicidad());
            if (codTipoPeriodicidad == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoPeriodicidad con el valor: " + solicitudAdministradoraTipo.getCodTipoPeriodicidad()));
            } else {
                solicitudAdministradora.setCodTipoPeriodicidad(codTipoPeriodicidad);
            }
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getCodTipoPlazoEntrega())) {
            ValorDominio codTipoPlazoEntrega = valorDominioDao.find(solicitudAdministradoraTipo.getCodTipoPlazoEntrega());
            if (codTipoPlazoEntrega == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoPlazoEntrega con el valor:" + solicitudAdministradoraTipo.getCodTipoPlazoEntrega()));
            } else {
                solicitudAdministradora.setCodTipoPlazoEntrega(codTipoPlazoEntrega);
            }
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getValCantidadPlazoEntrega())) {
            solicitudAdministradora.setValCantidadPlazoEntrega(solicitudAdministradoraTipo.getValCantidadPlazoEntrega());
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getValNombreSolicitud())) {
            solicitudAdministradora.setValNombreSolicitud(solicitudAdministradoraTipo.getValNombreSolicitud());
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getDescInformacionRequerida())) {
            solicitudAdministradora.setDescInformacionRequerida(solicitudAdministradoraTipo.getDescInformacionRequerida());
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getEsSolicitudAceptada())) {
            Validator.parseBooleanFromString(solicitudAdministradoraTipo.getEsSolicitudAceptada(),
                    solicitudAdministradoraTipo.getClass().getSimpleName(),
                    "esSolicitudAceptada", errorTipoList);
            solicitudAdministradora.setEsSolicitudAceptada(mapper.map(solicitudAdministradoraTipo.getEsSolicitudAceptada(), Boolean.class));
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getEsNecesitaCorreccion())) {
            Validator.parseBooleanFromString(solicitudAdministradoraTipo.getEsNecesitaCorreccion(),
                    solicitudAdministradoraTipo.getClass().getSimpleName(),
                    "esNecesitaCorreccion", errorTipoList);
            solicitudAdministradora.setEsNecesitaCorreccion(mapper.map(solicitudAdministradoraTipo.getEsNecesitaCorreccion(), Boolean.class));
        }
        if (solicitudAdministradoraTipo.getFecFinSolicitud() != null) {
            solicitudAdministradora.setFecFinSolicitud(solicitudAdministradoraTipo.getFecFinSolicitud());
        }
        if (solicitudAdministradoraTipo.getFecInicioSolicitud() != null) {
            solicitudAdministradora.setFecInicioSolicitud(solicitudAdministradoraTipo.getFecInicioSolicitud());
        }
        if (StringUtils.isNotBlank(solicitudAdministradoraTipo.getCodEstadoSolicitud())) {
            ValorDominio codEstadoGestion = valorDominioDao.find(solicitudAdministradoraTipo.getCodEstadoSolicitud());
            if (codEstadoGestion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoSolicitud con el valor: " + solicitudAdministradoraTipo.getCodEstadoSolicitud()));
            } else {
                solicitudAdministradora.setCodEstadoSolicitud(codEstadoGestion);
            }
        }
        if (solicitudAdministradoraTipo.getValidacionEstructura() != null
                && StringUtils.isNotBlank(solicitudAdministradoraTipo.getValidacionEstructura().getIdValidacionEstructura())) {
            ValidacionEstructura validacionEstructura = validacionEstructuraDao.find(Long.valueOf(solicitudAdministradoraTipo.getValidacionEstructura().getIdValidacionEstructura()));
            if (validacionEstructura == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una validaciòn estructura con el id: " + solicitudAdministradoraTipo.getValidacionEstructura().getIdValidacionEstructura()));
            } else {
                solicitudAdministradora.setValidacionEstructura(validacionEstructura);
            }
        }
        if (solicitudAdministradoraTipo.getAdministradorasSeleccionadas() != null
                && !solicitudAdministradoraTipo.getAdministradorasSeleccionadas().isEmpty()) {

            for (AgrupacionSubsistemaTipo agrupacionSubsitemaTipo : solicitudAdministradoraTipo.getAdministradorasSeleccionadas()) {
                if (agrupacionSubsitemaTipo != null && StringUtils.isNotBlank(agrupacionSubsitemaTipo.getIdAgrupacionAdministradora())) {
                    AgrupacionSubsistemaSolAdmin agrupacionSubsistemaSolAdmin = new AgrupacionSubsistemaSolAdmin();
                    AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin = agrupacionSubsistemaDao.find(Long.valueOf(agrupacionSubsitemaTipo.getIdAgrupacionAdministradora()));
                    if (agrupacionSubsistemaAdmin == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una agrupación subsistema con el id :" + agrupacionSubsitemaTipo.getIdAgrupacionAdministradora()));
                    } else if (solicitudAdministradora.getId() != null) {
                        AgrupacionSubsistemaSolAdmin agrupacionSubsistemaSolAdminEncontrados = agrupacionSubsistemaSolDao.findByAgrupacionSubsistemaAndSolicitudAdmin(agrupacionSubsistemaAdmin, solicitudAdministradora);
                        if (agrupacionSubsistemaSolAdminEncontrados != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre la agrupacion subsistema: " + agrupacionSubsistemaAdmin.getId() + ", y la solicitid Administradora:" + solicitudAdministradora.getId()));
                        } else {
                            agrupacionSubsistemaSolAdmin.setAdministradora(agrupacionSubsistemaAdmin);
                            agrupacionSubsistemaSolAdmin.setSolicitudAdministradora(solicitudAdministradora);
                            agrupacionSubsistemaSolAdmin.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            solicitudAdministradora.getAdministradorasSeleccionadas().add(agrupacionSubsistemaSolAdmin);
                        }
                    } else {
                        agrupacionSubsistemaSolAdmin.setAdministradora(agrupacionSubsistemaAdmin);
                        agrupacionSubsistemaSolAdmin.setSolicitudAdministradora(solicitudAdministradora);
                        agrupacionSubsistemaSolAdmin.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        solicitudAdministradora.getAdministradorasSeleccionadas().add(agrupacionSubsistemaSolAdmin);
                    }
                }
            }
        }
        if (solicitudAdministradoraTipo.getAdministradorasDefinitivas() != null
                && !solicitudAdministradoraTipo.getAdministradorasDefinitivas().isEmpty()) {
            for (GestionAdministradoraTipo gestionAdministradoraTipo : solicitudAdministradoraTipo.getAdministradorasDefinitivas()) {
                if (gestionAdministradoraTipo != null && StringUtils.isNotBlank(gestionAdministradoraTipo.getIdGestionAdministradora())) {
                    GestionSolAdministradora gestionSolAdministradora = new GestionSolAdministradora();
                    GestionAdministradora gestionAdministradora = gestionAdministradoraDao.find(Long.valueOf(gestionAdministradoraTipo.getIdGestionAdministradora()));
                    if (gestionAdministradora == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una gestiòn administradora con el id :" + gestionAdministradoraTipo.getIdGestionAdministradora()));
                    } else if (solicitudAdministradora.getId() != null) {
                        GestionSolAdministradora gestionSolAdministradoraEncontrados = gestionSolAdministradoraDao.findByGestionAdministradoraAndSolicitudAdmin(solicitudAdministradora, gestionAdministradora);
                        if (gestionSolAdministradoraEncontrados != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre la gestión administradora: " + gestionAdministradora.getId() + ", y la solicitid Administradora:" + solicitudAdministradora.getId()));
                        } else {
                            gestionSolAdministradora.setGestionAdministradora(gestionAdministradora);
                            gestionSolAdministradora.setSolicitudAdministradora(solicitudAdministradora);
                            gestionSolAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            solicitudAdministradora.getAdministradorasDefinitivas().add(gestionSolAdministradora);
                        }
                    } else {
                        gestionSolAdministradora.setGestionAdministradora(gestionAdministradora);
                        gestionSolAdministradora.setSolicitudAdministradora(solicitudAdministradora);
                        gestionSolAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        solicitudAdministradora.getAdministradorasDefinitivas().add(gestionSolAdministradora);
                    }
                }
            }
        }
        if (solicitudAdministradoraTipo.getDescSolicitudCorreccion() != null
                && !solicitudAdministradoraTipo.getDescSolicitudCorreccion().isEmpty()) {
            for (String descripcion : solicitudAdministradoraTipo.getDescSolicitudCorreccion()) {
                SolicitudAdministradoraCorreccion solicitudAdministradoraCorreccion = new SolicitudAdministradoraCorreccion();
                solicitudAdministradoraCorreccion.setDescSolicitudCorreccion(descripcion);
                solicitudAdministradoraCorreccion.setSolicitudAdministradora(solicitudAdministradora);
                solicitudAdministradoraCorreccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                solicitudAdministradora.getSolicitudesCorreccion().add(solicitudAdministradoraCorreccion);
            }
        }
        if (solicitudAdministradoraTipo.getAcciones() != null
                && !solicitudAdministradoraTipo.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : solicitudAdministradoraTipo.getAcciones()) {
                final Accion accion = mapper.map(accionTipo, Accion.class);
                accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = valorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        SolicitudAdministradoraAccion solicitudAdministradoraAccion = new SolicitudAdministradoraAccion();
                        accion.setCodAccion(codAccion);
                        solicitudAdministradoraAccion.setAccion(accion);
                        solicitudAdministradoraAccion.setSolicitudAdministradora(solicitudAdministradora);
                        solicitudAdministradoraAccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        solicitudAdministradora.getAcciones().add(solicitudAdministradoraAccion);
                    }
                }

            }
        }

        if (solicitudAdministradoraTipo.getIdFuncionarioSolicitud() != null && StringUtils.isNotBlank(solicitudAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudAdministradoraTipo.getIdFuncionarioSolicitud(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un funcionario con el ID: " + solicitudAdministradoraTipo.getIdFuncionarioSolicitud().getIdFuncionario()));
            } else {
                solicitudAdministradora.setIdFuncionarioSolicitud(funcionarioTipo.getIdFuncionario());
            }
        }
    }

}
