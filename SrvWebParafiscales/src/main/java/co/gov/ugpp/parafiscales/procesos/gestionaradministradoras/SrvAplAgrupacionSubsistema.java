/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.MsjOpActualizarAgrupacionSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.MsjOpBuscarPorCriteriosAgrupacionSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.MsjOpCrearAgrupacionSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpActualizarAgrupacionSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpActualizarAgrupacionSubsistemaSolTipo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpCrearAgrupacionSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.OpCrearAgrupacionSubsistemaSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplAgrupacionSubsistema",
        portName = "portSrvAplAgrupacionSubsistemaSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplagrupacionsubsistema.v1.PortSrvAplAgrupacionSubsistemaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplAgrupacionSubsistema/v1")
@HandlerChain(file = "handler-chain.xml")

public class SrvAplAgrupacionSubsistema extends AbstractSrvApl {

    @EJB
    private AgrupacionSubsistemaFacade agrupacionSubsistemaAdminFacade;
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAgrupacionSubsistema.class);

    public OpCrearAgrupacionSubsistemaRespTipo opCrearAgrupacionSubsistema(OpCrearAgrupacionSubsistemaSolTipo msjOpCrearAgrupacionSubsistemaSol) throws MsjOpCrearAgrupacionSubsistemaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAgrupacionSubsistemaSol.getContextoTransaccional();
        final AgrupacionSubsistemaTipo agrupacionSubsistemaTipo = msjOpCrearAgrupacionSubsistemaSol.getAgrupacionSubsistema();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long agrupacionSubsistemaAdminPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            agrupacionSubsistemaAdminPK = agrupacionSubsistemaAdminFacade.crearAgrupacionSubsistemaAdmin(agrupacionSubsistemaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearAgrupacionSubsistemaRespTipo resp = new OpCrearAgrupacionSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdAgrupacionSubsistema(agrupacionSubsistemaAdminPK.toString());

        return resp;
    }

    public OpActualizarAgrupacionSubsistemaRespTipo opActualizarAgrupacionSubsistema(OpActualizarAgrupacionSubsistemaSolTipo msjOpActualizarAgrupacionSubsistemaSol) throws MsjOpActualizarAgrupacionSubsistemaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAgrupacionSubsistemaSol.getContextoTransaccional();
        final AgrupacionSubsistemaTipo agrupacionSubsistemaTipo = msjOpActualizarAgrupacionSubsistemaSol.getAgrupacionSubsistema();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            agrupacionSubsistemaAdminFacade.actualizarAgrupacionSubsistemaAdmin(agrupacionSubsistemaTipo, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarAgrupacionSubsistemaRespTipo resp = new OpActualizarAgrupacionSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo opBuscarPorCriteriosAgrupacionSubsistema(OpBuscarPorCriteriosAgrupacionSubsistemaSolTipo msjOpBuscarPorCriteriosAgrupacionSubsistemaSol) throws MsjOpBuscarPorCriteriosAgrupacionSubsistemaFallo {
         final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAgrupacionSubsistemaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosAgrupacionSubsistemaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criteriosOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AgrupacionSubsistemaTipo> agrupacionSubsitemaTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            agrupacionSubsitemaTipoPagerData = agrupacionSubsistemaAdminFacade.buscarPorCriteriosAgrupacionSubsistemaAdmin(parametroTipoList, criteriosOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAgrupacionSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo resp = new OpBuscarPorCriteriosAgrupacionSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getAgrupacionSubsistema().addAll(agrupacionSubsitemaTipoPagerData.getData());
        
        return resp;
    }

}
