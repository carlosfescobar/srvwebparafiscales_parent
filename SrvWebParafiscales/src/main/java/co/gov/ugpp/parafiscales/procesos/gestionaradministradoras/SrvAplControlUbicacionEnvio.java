package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.MsjOpActualizarControlUbicacionEnvioFallo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.MsjOpBuscarPorIdControlUbicacionEnvioFallo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.MsjOpCrearControlUbicacionEnvioFallo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpActualizarControlUbicacionEnvioRespTipo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpActualizarControlUbicacionEnvioSolTipo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpBuscarPorIdControlUbicacionEnvioRespTipo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpBuscarPorIdControlUbicacionEnvioSolTipo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpCrearControlUbicacionEnvioRespTipo;
import co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.OpCrearControlUbicacionEnvioSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplControlUbicacionEnvio",
        portName = "portSrvAplControlUbicacionEnvioSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplcontrolubicacionenvio.v1.PortSrvAplControlUbicacionEnvioSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplControlUbicacionEnvio/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplControlUbicacionEnvio extends AbstractSrvApl {

    @EJB
    private ControlUbicacionEnvioFacade controlUbicacionEnvioFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplControlUbicacionEnvio.class);

    public OpCrearControlUbicacionEnvioRespTipo opCrearControlUbicacionEnvio(OpCrearControlUbicacionEnvioSolTipo msjOpCrearControlUbicacionEnvioSol) throws MsjOpCrearControlUbicacionEnvioFallo {
        LOG.info("OPERACION: opCrearControlUbicacionEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearControlUbicacionEnvioSol.getContextoTransaccional();
        final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo = msjOpCrearControlUbicacionEnvioSol.getControlUbicacionEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idControlUbicacionEnvio;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            idControlUbicacionEnvio = controlUbicacionEnvioFacade.crearControlUbicacionEnvio(controlUbicacionEnvioTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearControlUbicacionEnvioRespTipo resp = new OpCrearControlUbicacionEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdControlUbicacionEnvio(idControlUbicacionEnvio.toString());
        LOG.info("OPERACION: opCrearControlUbicacionEnvio ::: FIN");
        return resp;
    }

    public OpActualizarControlUbicacionEnvioRespTipo opActualizarControlUbicacionEnvio(OpActualizarControlUbicacionEnvioSolTipo msjOpActualizarControlUbicacionEnvioSol) throws MsjOpActualizarControlUbicacionEnvioFallo {
        LOG.info("OPERACION: opActualizarControlUbicacionEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarControlUbicacionEnvioSol.getContextoTransaccional();
        final ControlUbicacionEnvioTipo controlUbicacionEnvioTipo = msjOpActualizarControlUbicacionEnvioSol.getControlUbicacionEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlUbicacionEnvioFacade.actualizarControlUbicacionEnvio(controlUbicacionEnvioTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarControlUbicacionEnvioRespTipo resp = new OpActualizarControlUbicacionEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarControlUbicacionEnvio ::: FIN");
        return resp;
    }

    public OpBuscarPorIdControlUbicacionEnvioRespTipo opBuscarPorIdControlUbicacionEnvio(OpBuscarPorIdControlUbicacionEnvioSolTipo msjOpBuscarPorIdControlUbicacionEnvioSol) throws MsjOpBuscarPorIdControlUbicacionEnvioFallo {
        LOG.info("OPERACION: opBuscarPorIdControlUbicacionEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdControlUbicacionEnvioSol.getContextoTransaccional();
        final List<String> idEventoEnvioList = msjOpBuscarPorIdControlUbicacionEnvioSol.getIdControlUbicacionEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        List<ControlUbicacionEnvioTipo> controlUbicacionEnvioTipoList;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlUbicacionEnvioTipoList = controlUbicacionEnvioFacade.buscarPorIdControlUbicacionEnvio(idEventoEnvioList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlUbicacionEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdControlUbicacionEnvioRespTipo resp = new OpBuscarPorIdControlUbicacionEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getControlesUbicacionEnvio().addAll(controlUbicacionEnvioTipoList);
        LOG.info("OPERACION: opBuscarPorIdControlUbicacionEnvio ::: FIN");
        return resp;
    }
}
