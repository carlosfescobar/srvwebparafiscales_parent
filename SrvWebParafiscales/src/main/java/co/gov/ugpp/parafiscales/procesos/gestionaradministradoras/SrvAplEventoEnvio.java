package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvapleventoenvio.v1.MsjOpActualizarEventoEnvioFallo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.MsjOpBuscarPorIdEventoEnvioFallo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.MsjOpCrearEventoEnvioFallo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpActualizarEventoEnvioRespTipo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpActualizarEventoEnvioSolTipo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpBuscarPorIdEventoEnvioRespTipo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpBuscarPorIdEventoEnvioSolTipo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpCrearEventoEnvioRespTipo;
import co.gov.ugpp.administradora.srvapleventoenvio.v1.OpCrearEventoEnvioSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplEventoEnvio",
        portName = "portSrvAplEventoEnvioSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvapleventoenvio.v1.PortSrvAplEventoEnvioSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplEventoEnvio/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplEventoEnvio extends AbstractSrvApl {

    @EJB
    private EventoEnvioFacade eventoEnvioFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplEventoEnvio.class);

    public OpCrearEventoEnvioRespTipo opCrearEventoEnvio(OpCrearEventoEnvioSolTipo msjOpCrearEventoEnvioSol) throws MsjOpCrearEventoEnvioFallo {
        LOG.info("OPERACION: opCrearEventoEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearEventoEnvioSol.getContextoTransaccional();
        final EventoEnvioTipo eventoEnvioTipo = msjOpCrearEventoEnvioSol.getEventoEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idEventoEnvio;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            idEventoEnvio = eventoEnvioFacade.crearEventoEnvio(eventoEnvioTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearEventoEnvioRespTipo resp = new OpCrearEventoEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdEventoEnvio(idEventoEnvio.toString());
        LOG.info("OPERACION: opCrearEventoEnvio ::: FIN");
        return resp;
    }

    public OpActualizarEventoEnvioRespTipo opActualizarEventoEnvio(OpActualizarEventoEnvioSolTipo msjOpActualizarEventoEnvioSol) throws MsjOpActualizarEventoEnvioFallo {
        LOG.info("OPERACION: opActualizarEventoEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarEventoEnvioSol.getContextoTransaccional();
        final EventoEnvioTipo eventoEnvioTipo = msjOpActualizarEventoEnvioSol.getEventoEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            eventoEnvioFacade.actualizarEventoEnvio(eventoEnvioTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarEventoEnvioRespTipo resp = new OpActualizarEventoEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarEventoEnvio ::: FIN");
        return resp;
    }

    public OpBuscarPorIdEventoEnvioRespTipo opBuscarPorIdEventoEnvio(OpBuscarPorIdEventoEnvioSolTipo msjOpBuscarPorIdEventoEnvioSol) throws MsjOpBuscarPorIdEventoEnvioFallo {
        LOG.info("OPERACION: opBuscarPorIdEventoEnvio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdEventoEnvioSol.getContextoTransaccional();
        final List<String> idRespuestaAdministradora = msjOpBuscarPorIdEventoEnvioSol.getIdEventoEnvio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        List<EventoEnvioTipo> eventoEnvioTipoList;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            eventoEnvioTipoList = eventoEnvioFacade.buscarPorIdEventoEnvio(idRespuestaAdministradora, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdEventoEnvioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdEventoEnvioRespTipo resp = new OpBuscarPorIdEventoEnvioRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getEnventosEnvio().addAll(eventoEnvioTipoList);
        LOG.info("OPERACION: opBuscarPorIdEventoEnvio ::: FIN");
        return resp;
    }

}
