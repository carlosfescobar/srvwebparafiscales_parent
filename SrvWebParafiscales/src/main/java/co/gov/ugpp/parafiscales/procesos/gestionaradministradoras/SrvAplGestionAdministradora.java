/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.MsjOpActualizarGestionAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.MsjOpBuscarPorCriteriosGestionAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.MsjOpBuscarPorIdGestionAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.MsjOpCrearGestionAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpActualizarGestionAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpActualizarGestionAdministradoraSolTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpBuscarPorCriteriosGestionAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpBuscarPorCriteriosGestionAdministradoraSolTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpBuscarPorIdGestionAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpBuscarPorIdGestionAdministradoraSolTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpCrearGestionAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplgestionadministradora.v1.OpCrearGestionAdministradoraSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplGestionAdministradora",
        portName = "portSrvAplGestionAdministradoraSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplgestionadministradora.v1.PortSrvAplGestionAdministradoraSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplGestionAdministradora/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplGestionAdministradora extends AbstractSrvApl {

    @EJB
    private GestionAdministradoraFacade gestionAdministradoraFacade;
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplGestionAdministradora.class);

    public OpCrearGestionAdministradoraRespTipo opCrearGestionAdministradora(OpCrearGestionAdministradoraSolTipo msjOpCrearGestionAdministradoraSol) throws MsjOpCrearGestionAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearGestionAdministradoraSol.getContextoTransaccional();
        final GestionAdministradoraTipo gestionAdministradoraTipo = msjOpCrearGestionAdministradoraSol.getGestionAdministradora();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long gestionAdministradoraPk;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            gestionAdministradoraPk = gestionAdministradoraFacade.crearGestionAdministradora(gestionAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearGestionAdministradoraRespTipo resp = new OpCrearGestionAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdGestionAdministradora(gestionAdministradoraPk.toString());

        return resp;
    }

    public OpBuscarPorIdGestionAdministradoraRespTipo opBuscarPorIdGestionAdministradora(OpBuscarPorIdGestionAdministradoraSolTipo msjOpBuscarPorIdGestionAdministradoraSol) throws MsjOpBuscarPorIdGestionAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdGestionAdministradoraSol.getContextoTransaccional();
        final List<String> idGestionAdministradora = msjOpBuscarPorIdGestionAdministradoraSol.getIdGestionAdministradora();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<GestionAdministradoraTipo> gestionAdministradoraTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            gestionAdministradoraTipos = gestionAdministradoraFacade.buscarPorIdGestionAdministradora(idGestionAdministradora, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdGestionAdministradoraRespTipo resp = new OpBuscarPorIdGestionAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getGestionAdministradora().addAll(gestionAdministradoraTipos);

        return resp;
    }

    public OpActualizarGestionAdministradoraRespTipo opActualizarGestionAdministradora(OpActualizarGestionAdministradoraSolTipo msjOpActualizarGestionAdministradoraSol) throws MsjOpActualizarGestionAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarGestionAdministradoraSol.getContextoTransaccional();
        final GestionAdministradoraTipo gestionAdministradoraTipo = msjOpActualizarGestionAdministradoraSol.getGestionAdministradora();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            gestionAdministradoraFacade.actualizarGestionAdministradora(gestionAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarGestionAdministradoraRespTipo resp = new OpActualizarGestionAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosGestionAdministradoraRespTipo opBuscarPorCriteriosGestionAdministradora(OpBuscarPorCriteriosGestionAdministradoraSolTipo msjOpBuscarPorCriteriosGestionAdministradoraSol) throws MsjOpBuscarPorCriteriosGestionAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosGestionAdministradoraSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosGestionAdministradoraSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<GestionAdministradoraTipo> gestionAdminitradoraTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            gestionAdminitradoraTipoPagerData = gestionAdministradoraFacade.buscarPorCriteriosGestionAdministradora(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosGestionAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosGestionAdministradoraRespTipo resp = new OpBuscarPorCriteriosGestionAdministradoraRespTipo();
        cr.setValCantidadPaginas(gestionAdminitradoraTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getGestionAdministradora().addAll(gestionAdminitradoraTipoPagerData.getData());

        return resp;
    }

}
