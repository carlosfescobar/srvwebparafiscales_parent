package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.MsjOpActualizarRespuestaAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.MsjOpBuscarPorIdRespuestaAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.MsjOpCrearRespuestaAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpActualizarRespuestaAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpActualizarRespuestaAdministradoraSolTipo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpBuscarPorIdRespuestaAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpBuscarPorIdRespuestaAdministradoraSolTipo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpCrearRespuestaAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.OpCrearRespuestaAdministradoraSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1.RespuestaAdministradoraTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplRespuestaAdministradora",
        portName = "portSrvAplRespuestaAdministradoraSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplrespuestaadministradora.v1.PortSrvAplRespuestaAdministradoraSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplRespuestaAdministradora/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplRespuestaAdministradora extends AbstractSrvApl {

    @EJB
    private RespuestaAdministradoraFacade respuestaAdministradoraFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRespuestaAdministradora.class);

    public OpCrearRespuestaAdministradoraRespTipo opCrearRespuestaAdministradora(OpCrearRespuestaAdministradoraSolTipo msjOpCrearRespuestaAdministradoraSol) throws MsjOpCrearRespuestaAdministradoraFallo {
        LOG.info("OPERACION: opCrearRespuestaAdministradora ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearRespuestaAdministradoraSol.getContextoTransaccional();
        final RespuestaAdministradoraTipo respuestaAdministradoraTipo = msjOpCrearRespuestaAdministradoraSol.getRespuestaAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idRespuestaAdministradora;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            idRespuestaAdministradora = respuestaAdministradoraFacade.crearRespuestaAdministradora(respuestaAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearRespuestaAdministradoraRespTipo resp = new OpCrearRespuestaAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdRespuestaAdministradora(idRespuestaAdministradora.toString());
        LOG.info("OPERACION: opCrearRespuestaAdministradora ::: FIN");
        return resp;
    }

    public OpActualizarRespuestaAdministradoraRespTipo opActualizarRespuestaAdministradora(OpActualizarRespuestaAdministradoraSolTipo msjOpActualizarRespuestaAdministradoraSol) throws MsjOpActualizarRespuestaAdministradoraFallo {
        LOG.info("OPERACION: opActualizarRespuestaAdministradora ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarRespuestaAdministradoraSol.getContextoTransaccional();
        final RespuestaAdministradoraTipo respuestaAdministradoraTipo = msjOpActualizarRespuestaAdministradoraSol.getRespuestaAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaAdministradoraFacade.actualizarRespuestaAdministradora(respuestaAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarRespuestaAdministradoraRespTipo resp = new OpActualizarRespuestaAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarRespuestaAdministradora ::: FIN");
        return resp;
    }

    public OpBuscarPorIdRespuestaAdministradoraRespTipo opBuscarPorIdRespuestaAdministradora(OpBuscarPorIdRespuestaAdministradoraSolTipo msjOpBuscarPorIdRespuestaAdministradoraSol) throws MsjOpBuscarPorIdRespuestaAdministradoraFallo {
        LOG.info("OPERACION: opBuscarPorIdRespuestaAdministradora ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdRespuestaAdministradoraSol.getContextoTransaccional();
        final List<String> idRespuestaAdministradora = msjOpBuscarPorIdRespuestaAdministradoraSol.getIdRespuestaAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        List<RespuestaAdministradoraTipo> respuestaAdministradoraList;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaAdministradoraList = respuestaAdministradoraFacade.buscarPorIdRespuestaAdministradora(idRespuestaAdministradora, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdRespuestaAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdRespuestaAdministradoraRespTipo resp = new OpBuscarPorIdRespuestaAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getRespuestasAdministradoras().addAll(respuestaAdministradoraList);
        LOG.info("OPERACION: opBuscarPorIdRespuestaAdministradora ::: FIN");
        return resp;
    }

}
