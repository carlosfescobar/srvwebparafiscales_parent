package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.MsjOpActualizarSolicitudEntidadExternaFallo;
import co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.MsjOpBuscarPorCriteriosSolicitudEntidadExternaFallo;
import co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.OpActualizarSolicitudEntidadExternaRespTipo;
import co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo;
import co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vladimir.garcia
 */
@WebService(serviceName = "SrvAplRespuestaEntidadExterna",
        portName = "portSrvAplRespuestaEntidadExternaSOAP",
        endpointInterface = "co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.PortSrvAplRespuestaEntidadExternaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradoras/SrvAplRespuestaEntidadExterna/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplRespuestaEntidadExterna extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRespuestaEntidadExterna.class);

    @EJB
    private RespuestaEntidadExternaFacade respuestaEntidadExternaFacade;

    public OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo opBuscarPorCriteriosSolicitudEntidadExterna(OpBuscarPorCriteriosSolicitudEntidadExternaSolTipo msjOpBuscarPorCriteriosSolicitudEntidadExternaSol) throws MsjOpBuscarPorCriteriosSolicitudEntidadExternaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosSolicitudEntidadExternaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosSolicitudEntidadExternaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SolicitudEntidadExternaTipo> solicitudEntidadExternaTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudEntidadExternaTipoPagerData = respuestaEntidadExternaFacade.buscarPorCriteriosSolicitudEntidadExternaRespTipo(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudEntidadExternaFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudEntidadExternaFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo resp = new OpBuscarPorCriteriosSolicitudEntidadExternaRespTipo();
        cr.setValCantidadPaginas(solicitudEntidadExternaTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSolicitudEntidadExterna().addAll(solicitudEntidadExternaTipoPagerData.getData());

        return resp;

    }

    public co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.OpActualizarSolicitudEntidadExternaRespTipo opActualizarSolicitudEntidadExterna(co.gov.ugpp.administradoras.srvaplrespuestaentidadexterna.v1.OpActualizarSolicitudEntidadExternaSolTipo msjOpActualizarSolicitudEntidadExternaSol) throws MsjOpActualizarSolicitudEntidadExternaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSolicitudEntidadExternaSol.getContextoTransaccional();
        final SolicitudEntidadExternaTipo respuestaEntidadExternaTipo = msjOpActualizarSolicitudEntidadExternaSol.getSolicitudEntidadExterna();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaEntidadExternaFacade.actualizarSolicitudEntidadExterna(respuestaEntidadExternaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarSolicitudEntidadExternaRespTipo resp = new OpActualizarSolicitudEntidadExternaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
