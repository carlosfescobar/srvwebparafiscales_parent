package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradoras.srvaplseguimiento.v1.MsjOpCrearSeguimientoFallo;
import co.gov.ugpp.administradoras.srvaplseguimiento.v1.OpCrearSeguimientoRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jsaenzar
 */
@WebService(serviceName = "SrvAplSeguimiento",
        portName = "portSrvAplSeguimientoSOAP",
        endpointInterface = "co.gov.ugpp.administradoras.srvaplseguimiento.v1.PortSrvAplSeguimientoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradoras/SrvAplSeguimiento/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplSeguimiento extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSeguimiento.class);

    @EJB
    private SeguimientoFacade seguimientoFacade;

    public co.gov.ugpp.administradoras.srvaplseguimiento.v1.OpCrearSeguimientoRespTipo opCrearSeguimiento(co.gov.ugpp.administradoras.srvaplseguimiento.v1.OpCrearSeguimientoSolTipo msjOpCrearSeguimientoSol) throws MsjOpCrearSeguimientoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSeguimientoSol.getContextoTransaccional();
        final SeguimientoTipo seguimientoTipo = msjOpCrearSeguimientoSol.getSeguimiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long seguimientoPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            seguimientoPK = seguimientoFacade.crearSeguimiento(seguimientoTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSeguimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSeguimientoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } 

        final OpCrearSeguimientoRespTipo resp = new OpCrearSeguimientoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSeguimiento(seguimientoPK.toString());

        return resp;
    }
}
