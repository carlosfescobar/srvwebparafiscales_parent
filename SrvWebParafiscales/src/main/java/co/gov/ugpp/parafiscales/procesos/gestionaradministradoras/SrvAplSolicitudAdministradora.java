package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.MsjOpActualizarSolicitudAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.MsjOpBuscarPorCriteriosSolicitudAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.MsjOpBuscarPorIdSolicitudAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.MsjOpCrearSolicitudAdministradoraFallo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpActualizarSolicitudAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorCriteriosSolicitudAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorIdSolicitudAdministradoraRespTipo;
import co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpCrearSolicitudAdministradoraRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1.SolicitudAdministradoraTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Everis
 */
@WebService(serviceName = "SrvAplSolicitudAdministradora", 
        portName = "portSrvAplSolicitudAdministradoraSOAP", 
        endpointInterface = "co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.PortSrvAplSolicitudAdministradoraSOAP", 
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplSolicitudAdministradora/v1")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplSolicitudAdministradora extends AbstractSrvApl {

    
    @EJB
    private SolicitudAdministradoraFacade solicitudAdministradoraFacade;
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSolicitudAdministradora.class);
    
    public co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpCrearSolicitudAdministradoraRespTipo opCrearSolicitudAdministradora(co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpCrearSolicitudAdministradoraSolTipo msjOpCrearSolicitudAdministradoraSol) throws MsjOpCrearSolicitudAdministradoraFallo {
       final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSolicitudAdministradoraSol.getContextoTransaccional();
        final SolicitudAdministradoraTipo solicitudAdministradoraTipo = msjOpCrearSolicitudAdministradoraSol.getSolicitudAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final Long solicitudAdministradoraPk;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudAdministradoraPk = solicitudAdministradoraFacade.crearSolicitudAdministradora(solicitudAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearSolicitudAdministradoraRespTipo resp = new OpCrearSolicitudAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSolicitudAdministradora(solicitudAdministradoraPk.toString());
        return resp;
        
    }

    public co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpActualizarSolicitudAdministradoraRespTipo opActualizarSolicitudAdministradora(co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpActualizarSolicitudAdministradoraSolTipo msjOpActualizarSolicitudAdministradoraSol) throws MsjOpActualizarSolicitudAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSolicitudAdministradoraSol.getContextoTransaccional();
        final SolicitudAdministradoraTipo gestionAdministradoraTipo = msjOpActualizarSolicitudAdministradoraSol.getSolicitudAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudAdministradoraFacade.actualizarSolicitudAdministradora(gestionAdministradoraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarSolicitudAdministradoraRespTipo resp = new OpActualizarSolicitudAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;
    }

    public co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorCriteriosSolicitudAdministradoraRespTipo opBuscarPorCriteriosSolicitudAdministradora(co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorCriteriosSolicitudAdministradoraSolTipo msjOpBuscarPorCriteriosSolicitudAdministradoraSol) throws MsjOpBuscarPorCriteriosSolicitudAdministradoraFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosSolicitudAdministradoraSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosSolicitudAdministradoraSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SolicitudAdministradoraTipo> solicitudAdminitradoraTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            solicitudAdminitradoraTipoPagerData = solicitudAdministradoraFacade.buscarPorCriteriosSolicitudAdministradora(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosSolicitudAdministradoraRespTipo resp = new OpBuscarPorCriteriosSolicitudAdministradoraRespTipo();
        cr.setValCantidadPaginas(solicitudAdminitradoraTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSolicitudAdministradora().addAll(solicitudAdminitradoraTipoPagerData.getData());
        return resp;
    }

    public co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorIdSolicitudAdministradoraRespTipo opBuscarPorIdSolicitudAdministradora(co.gov.ugpp.administradora.srvaplsolicitudadministradora.v1.OpBuscarPorIdSolicitudAdministradoraSolTipo msjOpBuscarPorIdSolicitudAdministradoraSol) throws MsjOpBuscarPorIdSolicitudAdministradoraFallo {
       final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdSolicitudAdministradoraSol.getContextoTransaccional();
        final List<String> idSolicitudAdministradora = msjOpBuscarPorIdSolicitudAdministradoraSol.getIdSolicitudAdministradora();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final List<SolicitudAdministradoraTipo> solicitudAdministradoraTipos;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudAdministradoraTipos = solicitudAdministradoraFacade.buscarPorIdSolicitudAdministradora(idSolicitudAdministradora, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudAdministradoraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdSolicitudAdministradoraRespTipo resp = new OpBuscarPorIdSolicitudAdministradoraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getSolicitudAdministradora().addAll(solicitudAdministradoraTipos);
        return resp;
    }
}
