package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplsubsistema.v1.MsjOpActualizarSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.MsjOpBuscarPorCriteriosSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.MsjOpBuscarPorIdSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.MsjOpCrearSubsistemaFallo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpActualizarSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpActualizarSubsistemaSolTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpBuscarPorCriteriosSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpBuscarPorCriteriosSubsistemaSolTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpBuscarPorIdSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpBuscarPorIdSubsistemaSolTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpCrearSubsistemaRespTipo;
import co.gov.ugpp.administradora.srvaplsubsistema.v1.OpCrearSubsistemaSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplSubsistema",
        portName = "portSrvAplSubsistemaSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplsubsistema.v1.PortSrvAplSubsistemaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplSubsistema/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplSubsistema extends AbstractSrvApl {

    @EJB
    private SubsistemaFacade subsistemaFacade;
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSubsistema.class);

    public OpCrearSubsistemaRespTipo opCrearSubsistema(OpCrearSubsistemaSolTipo msjOpCrearSubsistemaSol) throws MsjOpCrearSubsistemaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSubsistemaSol.getContextoTransaccional();
        final SubsistemaTipo sancionTipo = msjOpCrearSubsistemaSol.getSubsistema();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final String subsistemaPK;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            subsistemaPK = subsistemaFacade.crearSubsistema(sancionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearSubsistemaRespTipo resp = new OpCrearSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSubsistema(subsistemaPK);
        return resp;
    }

    public OpActualizarSubsistemaRespTipo opActualizarSubsistema(OpActualizarSubsistemaSolTipo msjOpActualizarSubsistemaSol) throws MsjOpActualizarSubsistemaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSubsistemaSol.getContextoTransaccional();
        final SubsistemaTipo subsistemaTipo = msjOpActualizarSubsistemaSol.getSubsistema();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            subsistemaFacade.actualizarSubsistema(subsistemaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarSubsistemaRespTipo resp = new OpActualizarSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdSubsistemaRespTipo opBuscarPorIdSubsistema(OpBuscarPorIdSubsistemaSolTipo msjOpBuscarPorIdSubsistemaSol) throws MsjOpBuscarPorIdSubsistemaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdSubsistemaSol.getContextoTransaccional();
        final List<String> idSubsitema = msjOpBuscarPorIdSubsistemaSol.getIdsubsistema();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<SubsistemaTipo> subsistemaTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            subsistemaTipos = subsistemaFacade.buscarPorIdSubsitema(idSubsitema, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdSubsistemaRespTipo resp = new OpBuscarPorIdSubsistemaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getSubsistema().addAll(subsistemaTipos);

        return resp;
    }

    public OpBuscarPorCriteriosSubsistemaRespTipo opBuscarPorCriteriosSubsistema(OpBuscarPorCriteriosSubsistemaSolTipo msjOpBuscarPorCriteriosSubsistemaSol) throws MsjOpBuscarPorCriteriosSubsistemaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosSubsistemaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosSubsistemaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SubsistemaTipo> subsistemaTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            subsistemaTiposPagerData = subsistemaFacade.buscarPorCriteriosSubsistema(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSubsistemaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosSubsistemaRespTipo resp = new OpBuscarPorCriteriosSubsistemaRespTipo();
        cr.setValCantidadPaginas(subsistemaTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSubsistema().addAll(subsistemaTiposPagerData.getData());

        return resp;
    }

}
