package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.MsjOpActualizarValidacionEstructuraFallo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.MsjOpBuscarPorIdValidacionEstructuraFallo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.MsjOpCrearValidacionEstructuraFallo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpActualizarValidacionEstructuraRespTipo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpActualizarValidacionEstructuraSolTipo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpBuscarPorIdValidacionEstructuraRespTipo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpBuscarPorIdValidacionEstructuraSolTipo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpCrearValidacionEstructuraRespTipo;
import co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.OpCrearValidacionEstructuraSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplValidacionEstructura",
        portName = "portSrvAplValidacionEstructuraSOAP",
        endpointInterface = "co.gov.ugpp.administradora.srvaplvalidacionestructura.v1.PortSrvAplValidacionEstructuraSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Administradora/SrvAplValidacionEstructura/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplValidacionEstructura extends AbstractSrvApl {

    @EJB
    private ValidacionEstructuraFacade validacionEstructuraFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplValidacionEstructura.class);

    public OpCrearValidacionEstructuraRespTipo opCrearValidacionEstructura(OpCrearValidacionEstructuraSolTipo msjOpCrearValidacionEstructuraSol) throws MsjOpCrearValidacionEstructuraFallo {
        LOG.info("OPERACION: opCrearValidacionEstructura ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearValidacionEstructuraSol.getContextoTransaccional();
        final ValidacionEstructuraTipo validacionEstructuraTipo = msjOpCrearValidacionEstructuraSol.getValidacionEstructura();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idValidacionEstructura;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            idValidacionEstructura = validacionEstructuraFacade.crearRespuestaAdministradora(validacionEstructuraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearValidacionEstructuraRespTipo resp = new OpCrearValidacionEstructuraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdValidacionEstructura(idValidacionEstructura.toString());
        LOG.info("OPERACION: opCrearValidacionEstructura ::: FIN");
        return resp;
    }

    public OpActualizarValidacionEstructuraRespTipo opActualizarValidacionEstructura(OpActualizarValidacionEstructuraSolTipo msjOpActualizarValidacionEstructuraSol) throws MsjOpActualizarValidacionEstructuraFallo {
        LOG.info("OPERACION: opActualizarValidacionEstructura ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarValidacionEstructuraSol.getContextoTransaccional();
        final ValidacionEstructuraTipo validacionEstructuraTipo = msjOpActualizarValidacionEstructuraSol.getValidacionEstructura();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            validacionEstructuraFacade.actualizarRespuestaAdministradora(validacionEstructuraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarValidacionEstructuraRespTipo resp = new OpActualizarValidacionEstructuraRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarValidacionEstructura ::: FIN");
        return resp;
    }

    public OpBuscarPorIdValidacionEstructuraRespTipo opBuscarPorIdValidacionEstructura(OpBuscarPorIdValidacionEstructuraSolTipo msjOpBuscarPorIdValidacionEstructuraSol) throws MsjOpBuscarPorIdValidacionEstructuraFallo {
        LOG.info("OPERACION: opBuscarPorIdValidacionEstructura ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdValidacionEstructuraSol.getContextoTransaccional();
        final List<String> idRespuestaAdministradora = msjOpBuscarPorIdValidacionEstructuraSol.getIdValidacionesEstructura();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        List<ValidacionEstructuraTipo> validacionEstructuraTipoList;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            validacionEstructuraTipoList = validacionEstructuraFacade.buscarPorIdRespuestaAdministradora(idRespuestaAdministradora, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdValidacionEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdValidacionEstructuraRespTipo resp = new OpBuscarPorIdValidacionEstructuraRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getValidacionesEstructura().addAll(validacionEstructuraTipoList);
        LOG.info("OPERACION: opBuscarPorIdValidacionEstructura ::: FIN");
        return resp;
    }
}
