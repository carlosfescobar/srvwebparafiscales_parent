package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface SubsistemaFacade extends Serializable {

    String crearSubsistema(final SubsistemaTipo subsistemaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarSubsistema(final SubsistemaTipo subsistemaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SubsistemaTipo> buscarPorIdSubsitema(List<String> idSubsistemaTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<SubsistemaTipo> buscarPorCriteriosSubsistema(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
