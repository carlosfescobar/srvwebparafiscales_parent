package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SubsistemaAdministradoraDao;
import co.gov.ugpp.parafiscales.persistence.dao.SubsistemaDao;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.persistence.entity.SubsistemaAdministradora;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz SubsistemaFacade que contiene las operaciones
 * del servicio SrvAplSubsistema.
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SubsistemaFacadeImpl extends AbstractFacade implements SubsistemaFacade {

    @EJB
    private EntidadExternaDao entidadExternaDao;
    @EJB
    private SubsistemaAdministradoraDao subsistemaAdministradoraDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private SubsistemaDao subsistemaDao;

    /**
     * implementación de la operación crearSubsistema, se encarga de crear un
     * subsistema de acuerdo al dto SubsistemaTipo.
     *
     * @param subsistemaTipo objeto a través del cual se va a crear un
     * subsistema en la base de datos
     * @param contextoTransaccionalTipo ntiene la información para almacenar la
     * auditoria.
     * @return Nuevo subsistema creado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public String crearSubsistema(SubsistemaTipo subsistemaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (subsistemaTipo == null || StringUtils.isBlank(subsistemaTipo.getIdSubsistema())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        Subsistema subsistema = subsistemaDao.find(subsistemaTipo.getIdSubsistema());
        if (subsistema != null) {
            throw new AppException("Ya existe un subsistema con el ID: " + subsistemaTipo.getIdSubsistema());
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        subsistema = new Subsistema();
        subsistema.setId(subsistemaTipo.getIdSubsistema());
        subsistema.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateSubsistema(subsistemaTipo, subsistema, errorTipoList, contextoTransaccionalTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return subsistemaDao.create(subsistema);
    }

    /**
     * implementación de la operación actualizarSubsistema, se encarga de
     * actualizar un subsistema de acuerdo a el id enviado.
     *
     * @param subsistemaTipo objeto a través del cual se actualiza un subsistema
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void actualizarSubsistema(SubsistemaTipo subsistemaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(subsistemaTipo.getIdSubsistema())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        Subsistema subsistema = subsistemaDao.find(subsistemaTipo.getIdSubsistema());
        if (subsistema == null) {
            throw new AppException("No se encontró Subsistema con el ID: " + subsistemaTipo.getIdSubsistema());
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        subsistema.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        populateSubsistema(subsistemaTipo, subsistema, errorTipoList, contextoTransaccionalTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        subsistemaDao.edit(subsistema);
    }

    /**
     * Implementación de la operación buscarPorIdSubsistema, esta se encarga de
     * buscar un listado de susistemas de acuerdo al listado de ids enviados.
     *
     * @param idSubsistemaTipoList listado de subsistemas a buscar por medio de
     * un listado de ids en la base de datos.
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Listado de subsistemas obtenidos que se retorna junto con el
     * contexto Transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public List<SubsistemaTipo> buscarPorIdSubsitema(List<String> idSubsistemaTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idSubsistemaTipoList == null || idSubsistemaTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Subsistema> subsistemaList = subsistemaDao.findByIdList(idSubsistemaTipoList);
        final List<SubsistemaTipo> subsistemaTipoList = mapper.map(subsistemaList, Subsistema.class, SubsistemaTipo.class);
        return subsistemaTipoList;
    }

    /**
     * implementación de la operación buscarPorCriteriosSubsistema esta se
     * encarga de buscar un subsistema por medio de los parametros enviado y
     * ordenar la consulta de acuerdo a los criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar el susbsistema.
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de subsistemas encontradas que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<SubsistemaTipo> buscarPorCriteriosSubsistema(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Subsistema> pagerDataEntity = subsistemaDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<SubsistemaTipo> subsistemaTipoList = mapper.map(pagerDataEntity.getData(), Subsistema.class, SubsistemaTipo.class);

        return new PagerData<SubsistemaTipo>(subsistemaTipoList, pagerDataEntity.getNumPages());
    }

    /**
     * Método para crear/actualizar la entidad de Subsistema.
     *
     * @param subsistemaTipo objeto destino
     * @param subsistema objeto Origen.
     * @param errorTipoList Listado que almacena errores de negocio.
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    public void populateSubsistema(SubsistemaTipo subsistemaTipo, Subsistema subsistema, List<ErrorTipo> errorTipoList,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (subsistemaTipo.getAdministradoras() != null
                && !subsistemaTipo.getAdministradoras().isEmpty()) {

            SubsistemaAdministradora subsistemaAdministradora;
            this.entidadExternaDuplicada(subsistemaTipo.getAdministradoras(), errorTipoList);
            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            } else {
                for (EntidadExternaTipo entidadExternaTipo : subsistemaTipo.getAdministradoras()) {
                    if (entidadExternaTipo != null && entidadExternaTipo.getIdPersona() != null) {
                    }
                    IdentificacionTipo identificacionTipo = entidadExternaTipo.getIdPersona();
                    Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                    EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

                    if (entidadExterna == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontrarón una administradora con la identificación: " + identificacion));
                    } else if (subsistema.getId() != null) {
                        subsistemaAdministradora = subsistemaAdministradoraDao.findBysubsistemaAndentidadVigilanciaControl(entidadExterna, subsistema);
                        if (subsistemaAdministradora != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre subsistema: " + subsistema.getId() + ", y  la Entidad Externa: " + entidadExterna.getId()));

                        } else {
                            subsistemaAdministradora = crearSubsistemaAdministradora(subsistema, entidadExterna, contextoTransaccionalTipo);
                            subsistema.getAdministradoras().add(subsistemaAdministradora);
                        }
                    } else {
                        subsistemaAdministradora = crearSubsistemaAdministradora(subsistema, entidadExterna, contextoTransaccionalTipo);
                        subsistema.getAdministradoras().add(subsistemaAdministradora);
                    }

                }
            }
        }

        if (subsistemaTipo.getEntidadVigilanciaControl() != null) {
            IdentificacionTipo identificacionTipo = subsistemaTipo.getEntidadVigilanciaControl().getIdPersona();

            if (StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())) {
                Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Persona entidadVigilacionControl = personaDao.findByIdentificacion(identificacion);
                if (entidadVigilacionControl == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró una entidad vigilancia control  con la Identificación :" + identificacion));
                } else {
                    subsistema.setEntidadVigilanciaControl(entidadVigilacionControl);
                }
            }
        }
    }

    private SubsistemaAdministradora crearSubsistemaAdministradora(Subsistema subsistema, EntidadExterna entidadExterna, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        SubsistemaAdministradora subsistemaAdministradora = new SubsistemaAdministradora();
        subsistemaAdministradora.setEntidadExterna(entidadExterna);
        subsistemaAdministradora.setSubsistema(subsistema);
        subsistemaAdministradora.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return subsistemaAdministradora;
    }

    private void entidadExternaDuplicada(List<EntidadExternaTipo> entidadExternaTipoList, List<ErrorTipo> errorTipoList) throws AppException {

        for (EntidadExternaTipo entidadExternaTipoInicial : entidadExternaTipoList) {
            int vecesRepetidas = 0;
            for (EntidadExternaTipo entidadExternaTipoValidada : entidadExternaTipoList) {
                if (entidadExternaTipoInicial.getIdPersona().getCodTipoIdentificacion().equals(entidadExternaTipoValidada.getIdPersona().getCodTipoIdentificacion())
                        && entidadExternaTipoInicial.getIdPersona().getValNumeroIdentificacion().equals(entidadExternaTipoValidada.getIdPersona().getValNumeroIdentificacion())) {
                    vecesRepetidas++;
                }
            }
            if (vecesRepetidas > 1) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Por favor verifique que las administradoras enviadas sean diferentes"));
                break;
            }
        }
    }

}
