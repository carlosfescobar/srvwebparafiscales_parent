package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface ValidacionEstructuraFacade extends Serializable {

    Long crearRespuestaAdministradora(final ValidacionEstructuraTipo validacionEstructuraTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarRespuestaAdministradora(final ValidacionEstructuraTipo validacionEstructuraTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<ValidacionEstructuraTipo> buscarPorIdRespuestaAdministradora(final List<String> idValidacionEstructuraList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
