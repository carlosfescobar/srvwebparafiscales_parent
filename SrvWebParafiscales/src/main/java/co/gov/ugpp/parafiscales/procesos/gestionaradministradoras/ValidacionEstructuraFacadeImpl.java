package co.gov.ugpp.parafiscales.procesos.gestionaradministradoras;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionEstructuraDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionEstructura;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ValidacionEstructuraFacade que contiene las
 * operaciones del servicio SrvAplValidacionEstructura
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ValidacionEstructuraFacadeImpl extends AbstractFacade implements ValidacionEstructuraFacade {

    @EJB
    private ArchivoDao archivoDao;

    @EJB
    private FormatoDao formatoDao;

    @EJB
    private ValidacionEstructuraDao validacionEstructuraDao;

    /**
     * Método que implementa la operación OpCrearValidacionEstructura del
     * servicio SrvAplValidacionEstructura
     *
     * @param validacionEstructuraTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @return el id de negocio creado
     * @throws AppException
     */
    @Override
    public Long crearRespuestaAdministradora(final ValidacionEstructuraTipo validacionEstructuraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (validacionEstructuraTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ValidacionEstructura validacionEstructura = new ValidacionEstructura();
        validacionEstructura.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateValidacionEstructura(contextoTransaccionalTipo, validacionEstructuraTipo, validacionEstructura, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return validacionEstructuraDao.create(validacionEstructura);
    }

    /**
     * Método que implementa la operación OpActualizarValidacionEstructura del
     * servicio SrvAplValidacionEstructura
     *
     * @param validacionEstructuraTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException
     */
    @Override
    public void actualizarRespuestaAdministradora(final ValidacionEstructuraTipo validacionEstructuraTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (validacionEstructuraTipo == null || StringUtils.isBlank(validacionEstructuraTipo.getIdValidacionEstructura())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final ValidacionEstructura validacionEstructura = validacionEstructuraDao.find(Long.valueOf(validacionEstructuraTipo.getIdValidacionEstructura()));
        if (validacionEstructura == null) {
            throw new AppException("No se encontró Validación Estructura con el ID: " + validacionEstructuraTipo.getIdValidacionEstructura());
        }
        validacionEstructura.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateValidacionEstructura(contextoTransaccionalTipo, validacionEstructuraTipo, validacionEstructura, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        validacionEstructuraDao.edit(validacionEstructura);
    }

    /**
     * Método que implementa la operación OpBuscarPorIdRespuestaAdministradora
     * del servicio SrvAplValidacionEstructura
     *
     * @param idValidacionEstructuraList los identificadores de los objetos a
     * buscar
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @return los objetos encontrados
     * @throws AppException
     */
    @Override
    public List<ValidacionEstructuraTipo> buscarPorIdRespuestaAdministradora(final List<String> idValidacionEstructuraList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idValidacionEstructuraList == null || idValidacionEstructuraList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }
        final List<Long> ids = mapper.map(idValidacionEstructuraList, String.class, Long.class);
        final List<ValidacionEstructura> validacionEstructuraList = validacionEstructuraDao.findByIdList(ids);
        final List<ValidacionEstructuraTipo> validacionEstructuraTipo = mapper.map(validacionEstructuraList, ValidacionEstructura.class, ValidacionEstructuraTipo.class);
        return validacionEstructuraTipo;
    }

    /**
     * Método que llena la entidad de negocio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param validacionEstructuraTipo el objeto origen
     * @param validacionEstructura el objeto destino
     * @param errorTipoList el listado de errores de la operación
     * @throws AppException
     */
    private void populateValidacionEstructura(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValidacionEstructuraTipo validacionEstructuraTipo,
            final ValidacionEstructura validacionEstructura, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(validacionEstructuraTipo.getIdFormatoElegido())
                && StringUtils.isNotBlank(validacionEstructuraTipo.getIdVersionElegido())) {
            Formato formatoElegido = formatoDao.findFormatoByFormatoAndVersion(Long.valueOf(validacionEstructuraTipo.getIdFormatoElegido()), Long.valueOf(validacionEstructuraTipo.getIdVersionElegido()));
            if (formatoElegido == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró formatoElegido con el Formato: " + validacionEstructuraTipo.getIdFormatoElegido()
                        + " y la Versión: " + validacionEstructuraTipo.getIdVersionElegido()));
            } else {
                validacionEstructura.setFormatoElegido(formatoElegido);
            }
        }
        if (StringUtils.isNotBlank(validacionEstructuraTipo.getIdFormatoEstructuraNueva())
                && StringUtils.isNotBlank(validacionEstructuraTipo.getIdVersionEstructuraNueva())) {
            Formato formatoNuevo = formatoDao.findFormatoByFormatoAndVersion(Long.valueOf(validacionEstructuraTipo.getIdFormatoEstructuraNueva()), Long.valueOf(validacionEstructuraTipo.getIdVersionEstructuraNueva()));
            if (formatoNuevo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró formatoNuevo con el Formato: " + validacionEstructuraTipo.getIdFormatoEstructuraNueva()
                        + " y la Versión: " + validacionEstructuraTipo.getIdVersionEstructuraNueva()));
            } else {
                validacionEstructura.setFormatoEstructuraNueva(formatoNuevo);
            }
        }
        if (StringUtils.isNotBlank(validacionEstructuraTipo.getIdArchivoEstructuraSugerida())) {
            Archivo estructuraSugerida = archivoDao.find(Long.valueOf(validacionEstructuraTipo.getIdArchivoEstructuraSugerida()));
            if (estructuraSugerida == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró idArchivoEstructuraSugerida con el ID: " + validacionEstructuraTipo.getIdArchivoEstructuraSugerida()));
            } else {
                validacionEstructura.setArchivo(estructuraSugerida);
            }
        }
        if (StringUtils.isNotBlank(validacionEstructuraTipo.getDescEstructuraNoAprobada())) {
            validacionEstructura.setDescEstructuraNoAprobada(validacionEstructuraTipo.getDescEstructuraNoAprobada());
        }
        if (StringUtils.isNotBlank(validacionEstructuraTipo.getEsAprobadaNuevaEstructura())) {
            Validator.parseBooleanFromString(validacionEstructuraTipo.getEsAprobadaNuevaEstructura(),
                    validacionEstructuraTipo.getClass().getSimpleName(),
                    "esAprobadaNuevaEstructura", errorTipoList);
            validacionEstructura.setEsAprobadaNuevaEstructura(mapper.map(validacionEstructuraTipo.getEsAprobadaNuevaEstructura(), Boolean.class));
        }
        if (StringUtils.isNotBlank(validacionEstructuraTipo.getEsNecesarioNuevaEstructura())) {
            Validator.parseBooleanFromString(validacionEstructuraTipo.getEsNecesarioNuevaEstructura(),
                    validacionEstructuraTipo.getClass().getSimpleName(),
                    "esNecesarioNuevaEstructura", errorTipoList);
            validacionEstructura.setEsNecesarioNuevaEstructura(mapper.map(validacionEstructuraTipo.getEsNecesarioNuevaEstructura(), Boolean.class));
        }
    }
}
