package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author gchavezm
 */
public interface AprobacionDocumentoFacade extends Serializable {

    Long crearAprobacionDocumento(final AprobacionDocumentoTipo aprobacionDocumentoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
   
    void actualizarAprobacionDocumento(final AprobacionDocumentoTipo aprobacionDocumentoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<AprobacionDocumentoTipo> buscarPorIdAprobacionDocumento(final List<String> idAprobacionDocumentoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
