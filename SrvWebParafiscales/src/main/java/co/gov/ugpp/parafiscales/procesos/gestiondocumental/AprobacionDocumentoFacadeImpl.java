package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionCreacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionValidacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionCreacionDocumento;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ArchivoFacade que contiene las operaciones del
 * servicio SrvAplAprobacionDocumento
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AprobacionDocumentoFacadeImpl extends AbstractFacade implements AprobacionDocumentoFacade {

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private ArchivoDao archivoDao;

    @EJB
    private ValidacionCreacionDocumentoDao validacionCreacionDocumentoDao;
    
    @Override
    public Long crearAprobacionDocumento(AprobacionDocumentoTipo aprobacionDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (aprobacionDocumentoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        
        AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(aprobacionDocumentoTipo.getIdArchivo()));
        
        if(aprobacionDocumento != null){
            throw new AppException("Ya existe un aprobacionDocumento con el idArchivo: "+aprobacionDocumentoTipo.getIdArchivo());
        }
        
        aprobacionDocumento = new AprobacionDocumento();

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        aprobacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAprobacionDocumento(aprobacionDocumentoTipo, aprobacionDocumento,errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idAprobacionDocumento = aprobacionDocumentoDao.create(aprobacionDocumento);

        return idAprobacionDocumento;
    }

    @Override
    public void actualizarAprobacionDocumento(AprobacionDocumentoTipo aprobacionDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(aprobacionDocumentoTipo.getIdArchivo())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(aprobacionDocumentoTipo.getIdArchivo()));

        if (aprobacionDocumento == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idAprobacionDocumento con el valor:" + aprobacionDocumentoTipo.getIdArchivo()));
            throw new AppException(errorTipoList);
        }

        aprobacionDocumento.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAprobacionDocumento(aprobacionDocumentoTipo, aprobacionDocumento,errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        aprobacionDocumentoDao.edit(aprobacionDocumento);
    }


    private void populateAprobacionDocumento(final AprobacionDocumentoTipo aprobacionDocumentoTipo, final AprobacionDocumento aprobacionDocumento, 
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isNotBlank(aprobacionDocumentoTipo.getIdArchivo())) {
            final Archivo archivo = archivoDao.find(Long.valueOf(aprobacionDocumentoTipo.getIdArchivo()));
            if (archivo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un archivo con el ID: " + aprobacionDocumentoTipo.getIdArchivo()));
            } else {
                aprobacionDocumento.setArchivo(archivo);
            }
         }
        

          if (aprobacionDocumentoTipo.getValidacionCreacionDocumento()!= null) {
            final AprobacionValidacionDocumento aprobacionValidacionDocumento = new AprobacionValidacionDocumento();
  
            if (aprobacionDocumentoTipo.getValidacionCreacionDocumento()!= null
                    && !aprobacionDocumentoTipo.getValidacionCreacionDocumento().isEmpty()) {
                for (ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo : aprobacionDocumentoTipo.getValidacionCreacionDocumento()) {
                    if (StringUtils.isNotBlank(validacionCreacionDocumentoTipo.getIdValidacionCreacionDocumento())) {
                        ValidacionCreacionDocumento validacionCreacionDocumento = validacionCreacionDocumentoDao.find(Long.parseLong(validacionCreacionDocumentoTipo.getIdValidacionCreacionDocumento()));
                        if (validacionCreacionDocumento == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró validacionCreacionDocumento con el ID: " + validacionCreacionDocumentoTipo.getIdValidacionCreacionDocumento()));
                        } else {
                            aprobacionValidacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            aprobacionValidacionDocumento.setIdValidacionCreacionDocumento(validacionCreacionDocumento);
                            aprobacionValidacionDocumento.setAprobacionDocumento(aprobacionDocumento);
                            aprobacionDocumento.getValidacionCreacionDocumento().add(aprobacionValidacionDocumento);

                        }
                    }
                }
            }
   
          }
    
    } 

   

    @Override
    public List<AprobacionDocumentoTipo> buscarPorIdAprobacionDocumento(List<String> idAprobacionDocumentoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idAprobacionDocumentoList == null || idAprobacionDocumentoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }

        final List<Long> ids = mapper.map(idAprobacionDocumentoList, String.class, Long.class);

        List<AprobacionDocumento> aprobacionDocumentoList = aprobacionDocumentoDao.findByIdList(ids);

        final List<AprobacionDocumentoTipo> aprobacionDocuementoTipoList
                = mapper.map(aprobacionDocumentoList, AprobacionDocumento.class, AprobacionDocumentoTipo.class);

        return aprobacionDocuementoTipoList;
    }
}


