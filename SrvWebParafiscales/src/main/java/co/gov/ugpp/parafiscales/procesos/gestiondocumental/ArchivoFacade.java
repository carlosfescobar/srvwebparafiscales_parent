package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface ArchivoFacade extends Serializable {

    Long crearArchivo(final ArchivoTipo archivoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarArchivo(final ArchivoTipo archivoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<ArchivoTipo> buscarPorIdArchivo(final List<String> idArhivoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    void eliminarPorIdArchivo (final List<String> idArhivoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
