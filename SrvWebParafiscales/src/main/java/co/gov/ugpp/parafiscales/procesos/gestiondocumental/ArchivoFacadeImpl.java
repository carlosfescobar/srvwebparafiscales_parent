package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ArchivoFacade que contiene las operaciones del
 * servicio SrvAplArchivo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ArchivoFacadeImpl extends AbstractFacade implements ArchivoFacade {

    @EJB
    private ArchivoDao archivoDao;

    /**
     * implementacion de la operación crearArchivo esta se encarga de crear un
     * nuevo archivo a partir del objeto ArchivoTipo
     *
     * @param archivoTipo Objeto a partir del cual se crea un nuevo archivo
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Archivo creado que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearArchivo(final ArchivoTipo archivoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (archivoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkArchivo(archivoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Archivo archivo = new Archivo();

        archivo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateArchivo(archivoTipo, archivo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idArchivo = archivoDao.create(archivo);

        return idArchivo;
    }

    /**
     * implementación de la operación actualizarArchivo se encarga de actualizar
     * un Archivo de acuerdo al id enviado
     *
     * @param archivoTipo Objeto a partir del cual se va actualizar el archivo
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void actualizarArchivo(final ArchivoTipo archivoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(archivoTipo.getIdArchivo())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Archivo archivo = archivoDao.find(Long.valueOf(archivoTipo.getIdArchivo()));

        if (archivo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontó un IdArchivo con el valor:" + archivoTipo.getIdArchivo()));
            throw new AppException(errorTipoList);
        }

        archivo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateArchivo(archivoTipo, archivo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        archivoDao.edit(archivo);
    }

    @Override
    public List<ArchivoTipo> buscarPorIdArchivo(final List<String> idArhivoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idArhivoList == null || idArhivoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID a buscar");
        }
        final List<Long> ids = mapper.map(idArhivoList, String.class, Long.class);
        List<Archivo> archivoList = archivoDao.findByIdList(ids);
        final List<ArchivoTipo> archivoTipoList = mapper.map(archivoList, Archivo.class, ArchivoTipo.class);

        return archivoTipoList;
    }

    @Override
    public void eliminarPorIdArchivo(final List<String> idArhivoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idArhivoList == null || idArhivoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID's a eliminar");
        }
        final List<Long> ids = mapper.map(idArhivoList, String.class, Long.class);
        List<Archivo> archivoList = archivoDao.findByIdList(ids);
        for (Archivo archivo: archivoList){
            archivoDao.remove(archivo);
        }
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad Archivo
     *
     * @param archivoTipo Objeto origen
     * @param archivo Objeto destino
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateArchivo(final ArchivoTipo archivoTipo, final Archivo archivo) throws AppException {

        if (StringUtils.isNotBlank(archivoTipo.getValNombreArchivo())) {
            archivo.setValNombreArchivo(archivoTipo.getValNombreArchivo());
        }
        if (archivoTipo.getValContenidoArchivo() != null) {
            archivo.setValContenidoArchivo(archivoTipo.getValContenidoArchivo());
        }
        if (archivoTipo.getValContenidoFirma() != null) {
            archivo.setValContenidoFirma(archivoTipo.getValContenidoFirma());
        }
        if (StringUtils.isNotBlank(archivoTipo.getCodTipoMIMEArchivo())) {
            archivo.setCodTipoMimeArchivo(archivoTipo.getCodTipoMIMEArchivo());
        }
    }
}
