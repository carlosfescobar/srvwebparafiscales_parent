package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.List;
import java.util.Map;

/**
 *
 * @author rpadilla
 */
public interface ExpedienteFacade {

    Expediente persistirExpedienteDocumento(final ExpedienteTipo expedienteTipo,
            final Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos,
            final List<ErrorTipo> errorTipoList, final boolean actualizacionMasiva, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void crearExpediente(final ExpedienteTipo expedienteTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<ExpedienteTipo> buscarPorIdExpediente(final List<String> idExpedienteList) throws AppException;
}
