package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDocumentoDao;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.ApplicationException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * Implementación de interfaz ExpedienteFacade que contiene las operaciones del
 * servicio SrvAplExpedienteCORE
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@ApplicationException
@TransactionManagement
public class ExpedienteFacadeImpl extends AbstractFacade implements ExpedienteFacade {

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private ExpedienteDao expedienteDao;
    @EJB
    private PersonaDao personaDao;
    @EJB
    private ExpedienteDocumentoDao expedienteDocumentoDao;

    /**
     * Método que actualiza el origen del documento cuando se consumen
     * operaciones que tienen asociados documentos
     *
     * @param expedienteTipo el expediente de los documentos
     * @param documentos el listado de documentos a actualizar
     * @param errorTipoList Listado que almacena errores de negocio
     * @param actualizacionMasiva indica si, una vez realizada la modificaciòn del codOrigen, se deben actualizar los demàs documentos que estaban asociados al codOrigen a SIN_REFISTRAR
     * @param contextoTransaccionalTipo contiene los parámetros de auditoría
     * @return el expediente con su listado de documentos asociado actualizado
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public Expediente persistirExpedienteDocumento(final ExpedienteTipo expedienteTipo,
            final Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos, 
            final List<ErrorTipo> errorTipoList, final boolean actualizacionMasiva, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (expedienteTipo == null
                || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            throw new AppException("El Expediente enviado no puede ser null");
        }

        Expediente expediente = expedienteDao.find(expedienteTipo.getIdNumExpediente());

        if (expediente == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró el expediente con el ID: " + expedienteTipo.getIdNumExpediente()));
        } else if (documentos != null && !documentos.isEmpty()) {

            for (Entry<OrigenDocumentoEnum, List<DocumentoTipo>> entry : documentos.entrySet()) {

                List<DocumentoTipo> documentoTipoList = entry.getValue();
                OrigenDocumentoEnum origenDocumentoCode = entry.getKey();

                if (documentoTipoList != null && !documentoTipoList.isEmpty()) {
                    if (expediente.getExpedienteDocumentoList() != null
                            && !expediente.getExpedienteDocumentoList().isEmpty()) {
                        for (DocumentoTipo documentoTipo : documentoTipoList) {
                            if (documentoTipo != null && StringUtils.isNotBlank(documentoTipo.getIdDocumento())) {
                                //Se valida que el documento enviado tenga correspondencia con alguno de los documentos del expediente
                                if (!this.verificarCorrespondenciaDocumento(documentoTipo.getIdDocumento(),
                                        expediente.getExpedienteDocumentoList(), origenDocumentoCode, actualizacionMasiva, contextoTransaccionalTipo)) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró un Documento asociado al Expediente(" + expedienteTipo.getIdNumExpediente() + ") con el ID: " + documentoTipo.getIdDocumento()));
                                }
                            }
                        }
                    } else if (!documentoTipoList.isEmpty()) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se puede verificar la correspondencia de los documento asociados al expediente, ya que el expediente con ID: " + expedienteTipo.getIdNumExpediente() + " no tiene documentos asociados)"));
                        break;
                    }
                }
            }
        }
        return expediente;
    }

    /**
     * Implementación de la operación crearExpedienteCORE, que se encarga de
     * crear un nuevo expedientes a partir de un expedienteTipo
     *
     * @param expedienteTipo objeto a partir del cual se crea un nuevo
     * expediente en la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public void crearExpediente(final ExpedienteTipo expedienteTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (expedienteTipo == null
                || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            throw new AppException("El Expediente enviado no puede ser null");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Expediente expediente = expedienteDao.find(expedienteTipo.getIdNumExpediente());

        if (expediente != null) {
            throw new AppException("Ya existe un Expediente con el ID: " + expedienteTipo.getIdNumExpediente(),
                    ErrorEnum.ERROR_ENTIDAD_EXISTENTE);
        }

        Validator.checkExpediente(expedienteTipo, errorTipoList);

        expediente = mapper.map(expedienteTipo, Expediente.class);
        expediente.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        //Por definición funcional, se define que en el CORE, el expediente no tenga relación directa con persona
//        final IdentificacionTipo identificacionTipo = expedienteTipo.getIdentificacion();
//
//        if (identificacionTipo != null
//                && StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())
//                && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())) {
//
//            final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
//            final Persona persona = personaDao.findByIdentificacion(identificacion);
//            if (persona == null) {
//                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
//                        "No se encontró una Persona con la identificacion: " + identificacion));
//            }
//            expediente.setPersona(persona);
//        }
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        expedienteDao.create(expediente);
    }

    /**
     * Método que se encarga de verificar si un idDocumento está asociado a un
     * expediente
     *
     * @param idDocumentoTipo el identificador del documento
     * @param expedienteDocumentoEcmList los documentos asociados al expediente
     * @param origenDocumentoEnum el origen del documento con el cual se va
     * actualizar la relación entre expediente y documento
     * @return true si encontró que el idDocumento enviado está asociado al
     * expediente.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private boolean verificarCorrespondenciaDocumento(final String idDocumentoTipo,
            final List<ExpedienteDocumentoEcm> expedienteDocumentoEcmList, 
            final OrigenDocumentoEnum origenDocumentoEnum, final boolean actualizacionMasiva, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        ExpedienteDocumentoEcm expedienteDocumentoNoIncluir = null;
        boolean hayCorrespondencia = false;

        for (ExpedienteDocumentoEcm expedienteDocumentoEcm : expedienteDocumentoEcmList) {
            if (expedienteDocumentoEcm != null
                    && expedienteDocumentoEcm.getDocumentoEcm() != null
                    && expedienteDocumentoEcm.getDocumentoEcm().getId().equals(idDocumentoTipo)) {
                ValorDominio valorDominio = valorDominioDao.find(origenDocumentoEnum.getCode());
                expedienteDocumentoEcm.setCodOrigen(valorDominio);
                expedienteDocumentoEcm.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                expedienteDocumentoNoIncluir = expedienteDocumentoEcm;
                hayCorrespondencia = true;
                break;
            }
        }
        
        /*
        //Se actualizan los demás documentos asociados al cod origen
        if (hayCorrespondencia && actualizacionMasiva) {
            
            
            System.out.println("Op: ::ANDRES20:: verificarCorrespondenciaDocumento expedienteDocumentoNoIncluir.getId(): " + expedienteDocumentoNoIncluir.getId());
            
            expedienteDocumentoDao.actualizarCodOrigen(OrigenDocumentoEnum.SIN_REGISTRAR, origenDocumentoEnum, expedienteDocumentoNoIncluir);
        }
        
        */
        return hayCorrespondencia;
    }

    /**
     * Implementación de la operación buscarPorIdExpedienteCORE, que se encarga
     * de buscar un listado de expedientes a partir de un listado de
     * identificadores de expediente
     *
     * @param idExpedienteList contiene el listado de id's de expedientes a
     * buscar
     * @return el listado de expedientes encontrado a partir del listado de id's
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public List<ExpedienteTipo> buscarPorIdExpediente(List<String> idExpedienteList) throws AppException {

        if (idExpedienteList == null
                || idExpedienteList.isEmpty()) {
            throw new AppException("Debe proporcionar una lista de ID's a buscar");
        }
        final List<Expediente> expedienteList
                = expedienteDao.findByIdList(idExpedienteList);

        final List<ExpedienteTipo> expedienteTipoList
                = mapper.map(expedienteList, Expediente.class, ExpedienteTipo.class);

        return expedienteTipoList;
    }
}
