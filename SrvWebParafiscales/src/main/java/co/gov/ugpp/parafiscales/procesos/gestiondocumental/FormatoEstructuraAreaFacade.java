package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1.FormatoEstructuraAreaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface FormatoEstructuraAreaFacade extends Serializable {

    void crearFormatoEstructuraArea(final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarFormatoEstructuraArea(final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<FormatoEstructuraAreaTipo> buscarPorCriteriosFormatoEstructuraArea(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}