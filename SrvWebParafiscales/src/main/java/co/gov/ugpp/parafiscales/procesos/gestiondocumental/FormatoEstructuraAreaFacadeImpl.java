package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1.FormatoEstructuraAreaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz FormatoEstructuraAreaFacade que contiene las
 * operaciones del servicio SrvAplFormatoEstructuraArea
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class FormatoEstructuraAreaFacadeImpl extends AbstractFacade implements FormatoEstructuraAreaFacade {

    private static final Logger LOG = LoggerFactory.getLogger(FormatoEstructuraAreaFacadeImpl.class);

    @EJB
    private FormatoDao formatoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ArchivoDao archivoDao;

    /**
     * Método que implementa la operación OpCrearFormatoEstructuraArea del
     * servicio SrvAplFormatoEstructuraArea
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param formatoEstructuraAreaTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void crearFormatoEstructuraArea(final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (formatoEstructuraAreaTipo == null || formatoEstructuraAreaTipo.getFormato() == null
                || formatoEstructuraAreaTipo.getFormato().getIdFormato() == null
                || formatoEstructuraAreaTipo.getFormato().getValVersion() == null) {
            throw new AppException("Debe proporcionar el Objeto a Crear: El Identificador del Formato y la Versión son valores Requeridos");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        Formato formato = formatoDao.findFormatoByFormatoAndVersion(formatoEstructuraAreaTipo.getFormato().getIdFormato(), formatoEstructuraAreaTipo.getFormato().getValVersion().longValue());
        if (formato != null) {
            throw new AppException("Ya existe el Formato con el ID Formato: " + formatoEstructuraAreaTipo.getFormato().getIdFormato().toString()
                    + " con el ID Versión: " + formatoEstructuraAreaTipo.getFormato().getValVersion().toString());
        }
        formato = new Formato();
        formato.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateFormatoEstructuraArea(contextoTransaccionalTipo, formatoEstructuraAreaTipo, formato, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        formatoDao.create(formato);
    }

    /**
     * Método que implementa la operación OpActualizarFormatoEstructuraArea del
     * servicio SrvAplFormatoEstructuraArea
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param formatoEstructuraAreaTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarFormatoEstructuraArea(FormatoEstructuraAreaTipo formatoEstructuraAreaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (formatoEstructuraAreaTipo == null || formatoEstructuraAreaTipo.getFormato() == null
                || formatoEstructuraAreaTipo.getFormato().getIdFormato() == null
                || formatoEstructuraAreaTipo.getFormato().getValVersion() == null) {
            throw new AppException("Debe proporcionar el Objeto a Actualizar: El Identificador del Formato y la Versión son valores Requeridos");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        Formato formato = formatoDao.findFormatoByFormatoAndVersion(formatoEstructuraAreaTipo.getFormato().getIdFormato(), formatoEstructuraAreaTipo.getFormato().getValVersion().longValue());
        if (formato == null) {
            throw new AppException("No se encontró el Formato con el ID Formato: " + formatoEstructuraAreaTipo.getFormato().getIdFormato().toString()
                    + " con el ID Versión: " + formatoEstructuraAreaTipo.getFormato().getValVersion().toString());
        }
        formato.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateFormatoEstructuraArea(contextoTransaccionalTipo, formatoEstructuraAreaTipo, formato, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        formatoDao.edit(formato);
    }

    /**
     * Método que implementa la operación
     * OpBuscarPorCriteriosFormatoEstructuraArea del servicio
     * SrvAplFormatoEstructuraArea
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param parametroTipoList los parámetros de búqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @return Las entidades de negocio encontradas en base de datos
     * @throws AppException
     */
    @Override
    public PagerData<FormatoEstructuraAreaTipo> buscarPorCriteriosFormatoEstructuraArea(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        Util.buildParametrosBusquedaFormato(parametroTipoList);
        PagerData<Formato> formatoPagerData = formatoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        List<FormatoEstructuraAreaTipo> formatoEstructuraAreaTipoList = mapper.map(formatoPagerData.getData(), Formato.class, FormatoEstructuraAreaTipo.class);
        return new PagerData<FormatoEstructuraAreaTipo>(formatoEstructuraAreaTipoList, formatoPagerData.getNumPages());
    }

    /**
     * Método que se encarga de llenar la entidad formato
     *
     * @param contextoTransaccionalTipo contiene los datos de auditotía
     * @param formatoEstructuraAreaTipo el objeto origen
     * @param formato el objeto destino
     * @param errorTipoList listado de errores de la operación
     * @throws AppException
     */
    private void populateFormatoEstructuraArea(final ContextoTransaccionalTipo contextoTransaccionalTipo, final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo,
            final Formato formato, final List<ErrorTipo> errorTipoList) throws AppException {

        if (formatoEstructuraAreaTipo.getFormato() != null
                && formatoEstructuraAreaTipo.getFormato().getIdFormato() != null
                && formatoEstructuraAreaTipo.getFormato().getValVersion() != null) {
            FormatoPK formatoPK = new FormatoPK();
            formatoPK.setIdFormato(formatoEstructuraAreaTipo.getFormato().getIdFormato());
            formatoPK.setIdVersion(formatoEstructuraAreaTipo.getFormato().getValVersion().longValue());
            formato.setId(formatoPK);
        }

        if (StringUtils.isNotBlank(formatoEstructuraAreaTipo.getCodAreaNegocio())) {
            ValorDominio areaNegocio = valorDominioDao.find(formatoEstructuraAreaTipo.getCodAreaNegocio());
            if (areaNegocio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codAreaNegocio con el ID: " + formatoEstructuraAreaTipo.getCodAreaNegocio()));
            } else {
                formato.setCodAreaNegocio(areaNegocio);
            }
        }
        if (formatoEstructuraAreaTipo.getArchivo() != null
                && StringUtils.isNotBlank(formatoEstructuraAreaTipo.getArchivo().getIdArchivo())) {
            Archivo archivo = archivoDao.find(Long.valueOf(formatoEstructuraAreaTipo.getArchivo().getIdArchivo()));
            if (archivo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró Archivo con el ID: " + formatoEstructuraAreaTipo.getArchivo().getIdArchivo()));
            } else {
                formato.setArchivo(archivo);
            }
        }
        if (formatoEstructuraAreaTipo.getFecInicioVigencia() != null
                && formatoEstructuraAreaTipo.getFecFinVigencia() != null) {
            if (formatoEstructuraAreaTipo.getFecInicioVigencia().after(formatoEstructuraAreaTipo.getFecFinVigencia())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        "Las Fechas de Inicio y Fin de Vigecia no son válidas, la Fecha de Inicio debe ser Anterior a la Fecha Fin"));
            }
        }
        if (formatoEstructuraAreaTipo.getFecInicioVigencia() != null) {
            formato.setFecInicioVigencia(formatoEstructuraAreaTipo.getFecInicioVigencia());
        }
        if (formatoEstructuraAreaTipo.getFecFinVigencia() != null) {
            formato.setFecFinVigencia(formatoEstructuraAreaTipo.getFecFinVigencia());
        }
        if (StringUtils.isNotBlank(formatoEstructuraAreaTipo.getDescDescripcion())) {
            formato.setDescripcion(formatoEstructuraAreaTipo.getDescDescripcion());
        }
        if (StringUtils.isNotBlank(formatoEstructuraAreaTipo.getValUsuario())) {
            formato.setValUsuario(formatoEstructuraAreaTipo.getValUsuario());
        }
    }
}
