package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoEstructuraDao;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoEstructura;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz FormatoEstructuraFacade que contiene las operaciones del servicio
 * SrvAplFormatoEstructura
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class FormatoEstructuraFacadeImpl extends AbstractFacade implements FormatoEstructuraFacade {

    private static final Logger LOG = LoggerFactory.getLogger(FormatoEstructuraFacadeImpl.class);

    @EJB
    private FormatoEstructuraDao formatoEstructuraDao;

    @EJB
    private FormatoDao formatoDao;

    /**
     * implementación de la operación buscarPorCriteriosFormatoEstructura esta se encarga de buscar un listado de
     * formatos en la base de datos por medio de una lista de parametros y ordena la consulta de acuerdo a los criterios
     * establecidos
     *
     * @param parametroTipoList listado de parametros de busqueda
     * @param criterioOrdenamientoTipos Atributos por los cuales se ordena la consulta 
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return listado de formatoEstructura encontrados que se retorna  junto con el  contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override

    public PagerData<FormatoEstructuraTipo> buscarPorCriteriosFormatoEstructura(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        LOG.debug("parametroTipoList: {}", parametroTipoList);

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkParametrosFormatoEstructura(parametroTipoList, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        List<FormatoEstructuraTipo> formatoEstructuraTipoList = new ArrayList<FormatoEstructuraTipo>();

//        PagerData<FormatoArchivo> formatoArchivoPagerData = new PagerData<FormatoArchivo>(null, 0L);
//        Funcionalidad para Actualizar Formato en Data Base       
//        this.actualizarFormatoEstructura(parametroTipoList);
        parametroTipoList = this.insertarPrefijoBusqueda(parametroTipoList);
        PagerData<Formato> formatoPagerData
                = formatoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                        contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        for (Formato formato : formatoPagerData.getData()) {
            FormatoEstructuraTipo formatoEstructuraTipo = mapper.map(formato, FormatoEstructuraTipo.class);
            formatoEstructuraTipoList.add(formatoEstructuraTipo);
        }
        return new PagerData(formatoEstructuraTipoList, formatoPagerData.getNumPages());
    }

    private List<ParametroTipo> insertarPrefijoBusqueda(List<ParametroTipo> parametroTipoList) {

        final String PREFIJO = "formatoPK.";
        for (ParametroTipo parametroTipo : parametroTipoList) {
            parametroTipo.setIdLlave(PREFIJO.concat(parametroTipo.getIdLlave()));
        }
        return parametroTipoList;

    }
}
