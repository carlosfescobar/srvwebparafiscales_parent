package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.MsjOpActualizarAprobacionDocumentoFallo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.MsjOpBuscarPorIdAprobacionDocumentoFallo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.MsjOpCrearAprobacionDocumentoFallo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpActualizarAprobacionDocumentoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpActualizarAprobacionDocumentoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpBuscarPorIdAprobacionDocumentoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpBuscarPorIdAprobacionDocumentoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpCrearAprobacionDocumentoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.OpCrearAprobacionDocumentoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gchavezm
 */
@WebService(serviceName = "SrvAplAprobacionDocumento", 
        portName = "portSrvAplAprobacionDocumentoSOAP", 
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplaprobaciondocumento.v1.PortSrvAplAprobacionDocumentoSOAP", 
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplAprobacionDocumento/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAprobacionDocumento extends AbstractSrvApl {
    
    
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAprobacionDocumento.class);
    @EJB
    private AprobacionDocumentoFacade aprobacionDocumentoFacade;
    

    public OpCrearAprobacionDocumentoRespTipo opCrearAprobacionDocumento(OpCrearAprobacionDocumentoSolTipo msjOpCrearAprobacionDocumentoSol) throws MsjOpCrearAprobacionDocumentoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAprobacionDocumentoSol.getContextoTransaccional();
        final AprobacionDocumentoTipo aprobacionDocumentoTipo = msjOpCrearAprobacionDocumentoSol.getAprobacionDocumento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long aprobacionDocumentoPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            aprobacionDocumentoPK = aprobacionDocumentoFacade.crearAprobacionDocumento(aprobacionDocumentoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearAprobacionDocumentoRespTipo resp = new OpCrearAprobacionDocumentoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdAprobacionDocumento(aprobacionDocumentoPK.toString());
        return resp;
        
       }

    public OpActualizarAprobacionDocumentoRespTipo opActualizarAprobacionDocumento(OpActualizarAprobacionDocumentoSolTipo msjOpActualizarAprobacionDocumentoSol) throws MsjOpActualizarAprobacionDocumentoFallo {
     
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAprobacionDocumentoSol.getContextoTransaccional();
        final AprobacionDocumentoTipo aprobacionDocumentoTipo = msjOpActualizarAprobacionDocumentoSol.getAprobacionDocumento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            aprobacionDocumentoFacade.actualizarAprobacionDocumento(aprobacionDocumentoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarAprobacionDocumentoRespTipo resp = new OpActualizarAprobacionDocumentoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdAprobacionDocumentoRespTipo opBuscarPorIdAprobacionDocumento( OpBuscarPorIdAprobacionDocumentoSolTipo msjOpBuscarPorIdAprobacionDocumentoSol) throws MsjOpBuscarPorIdAprobacionDocumentoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdAprobacionDocumentoSol.getContextoTransaccional();
        final List<String> idAprobacionDocumento = msjOpBuscarPorIdAprobacionDocumentoSol.getIdAprobacionDocumento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<AprobacionDocumentoTipo> aprobacionDocumentoTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            aprobacionDocumentoTipos = aprobacionDocumentoFacade.buscarPorIdAprobacionDocumento(idAprobacionDocumento, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAprobacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdAprobacionDocumentoRespTipo resp = new OpBuscarPorIdAprobacionDocumentoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getAprobacionDocumento().addAll(aprobacionDocumentoTipos);

        return resp;

    }

}
