package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.MsjOpActualizarArchivoFallo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.MsjOpBuscarPorIdArchivoFallo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.MsjOpCrearArchivoFallo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.MsjOpEliminarPorIdArchivoFallo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpActualizarArchivoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpActualizarArchivoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpBuscarPorIdArchivoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpBuscarPorIdArchivoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpCrearArchivoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpCrearArchivoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpEliminarPorIdArchivoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.OpEliminarPorIdArchivoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.BindingType;
import javax.xml.ws.soap.SOAPBinding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodriguez
 */
@WebService(serviceName = "SrvAplArchivo",
        portName = "portSrvAplArchivoSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplarchivo.v1.PortSrvAplArchivoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplArchivo/v1")
@HandlerChain(file = "handler-chain.xml")

//Andres: agregar cuando confirmen version de SOAP 1.2
//@BindingType(value = SOAPBinding.SOAP12HTTP_MTOM_BINDING)
public class SrvAplArchivo extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplArchivo.class);

    @EJB
    private ArchivoFacade archivoFacade;

    public OpCrearArchivoRespTipo opCrearArchivo(OpCrearArchivoSolTipo msjOpCrearArchivoSol) throws MsjOpCrearArchivoFallo {
        
        
        LOG.info("Op: opCrearArchivo ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearArchivoSol.getContextoTransaccional();
        final ArchivoTipo archivoTipo = msjOpCrearArchivoSol.getArchivo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long archivoPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            archivoPK = archivoFacade.crearArchivo(archivoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearArchivoRespTipo resp = new OpCrearArchivoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdArchivo(archivoPK.toString());
        
        LOG.info("Op: opCrearArchivo ::: END");

        return resp;
    }

    public OpActualizarArchivoRespTipo opActualizarArchivo(OpActualizarArchivoSolTipo msjOpActualizarArchivoSol) throws MsjOpActualizarArchivoFallo {
        
        
        LOG.info("Op: opActualizarArchivo ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarArchivoSol.getContextoTransaccional();
        final ArchivoTipo archivoTipo = msjOpActualizarArchivoSol.getArchivo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            archivoFacade.actualizarArchivo(archivoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarArchivoRespTipo resp = new OpActualizarArchivoRespTipo();
        resp.setContextoRespuesta(cr);

        
        LOG.info("Op: opActualizarArchivo ::: END");
        
        return resp;
    }

    public OpBuscarPorIdArchivoRespTipo opBuscarPorIdArchivo(OpBuscarPorIdArchivoSolTipo msjOpBuscarPorIdArchivo) throws MsjOpBuscarPorIdArchivoFallo {

        
        LOG.info("Op: opBuscarPorIdArchivo ::: INIT");
        
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdArchivo.getContextoTransaccional();
        final List<String> idArchivoList = msjOpBuscarPorIdArchivo.getIdArchivo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<ArchivoTipo> archivoTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            archivoTipoList
                    = archivoFacade.buscarPorIdArchivo(idArchivoList, contextoTransaccionalTipo);
            
            
            //System.out.println("::ANDRES4:: isEmpty " + archivoTipoList.isEmpty());
            //System.out.println("::ANDRES4:: size " + archivoTipoList.size());
            //System.out.println("::ANDRES4:: get(0).getValContenidoFirma " + archivoTipoList.get(0).getValContenidoFirma().length);
            //archivoTipoList.get(0).setValContenidoFirma("0".getBytes());
            
            
            for (ArchivoTipo archivoTipo : archivoTipoList) {
                if(archivoTipo.getValContenidoFirma() == null)
                    archivoTipo.setValContenidoFirma("0".getBytes());
            }
            
            
            //System.out.println("::ANDRES4:: isEmpty " + archivoTipoList.isEmpty());
            //System.out.println("::ANDRES4:: size " + archivoTipoList.size());
            //System.out.println("::ANDRES4:: get(0).getValContenidoFirma " + archivoTipoList.get(0).getValContenidoFirma().length);
            //archivoTipoList.get(0).setValContenidoFirma("0".getBytes());
                    

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdArchivoRespTipo resp = new OpBuscarPorIdArchivoRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getArchivo().addAll(archivoTipoList);

        
        LOG.info("Op: opBuscarPorIdArchivo ::: END");
        
        return resp;
    }
    
    
    
    public OpEliminarPorIdArchivoRespTipo opEliminarPorIdArchivo(OpEliminarPorIdArchivoSolTipo msjOpEliminarPorIdArchivo) throws MsjOpEliminarPorIdArchivoFallo{
        
        LOG.info("Op: opEliminarPorIdArchivo ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpEliminarPorIdArchivo.getContextoTransaccional();
        final List<String> idArchivoList = msjOpEliminarPorIdArchivo.getIdArchivo();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            archivoFacade.eliminarPorIdArchivo(idArchivoList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpEliminarPorIdArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpEliminarPorIdArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        
        final OpEliminarPorIdArchivoRespTipo resp = new OpEliminarPorIdArchivoRespTipo();
        resp.setContextoRespuesta(cr);
        
        
        LOG.info("Op: opEliminarPorIdArchivo ::: END");
        
        return resp;
    }

}
