package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.MsjOpBuscarPorIdExpedienteCOREFallo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.MsjOpCrearExpedienteCOREFallo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.OpBuscarPorIdExpedienteCORERespTipo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.OpBuscarPorIdExpedienteCORESolTipo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.OpCrearExpedienteCORERespTipo;
import co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.OpCrearExpedienteCORESolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplExpedienteCORE",
        portName = "portSrvAplExpedienteCORESOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplexpedientecore.v1.PortSrvAplExpedienteCORESOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplExpedienteCORE/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplExpedienteCORE extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplExpedienteCORE.class);

    @EJB
    private ExpedienteFacade expedienteFacade;

    public OpCrearExpedienteCORERespTipo opCrearExpedienteCORE(OpCrearExpedienteCORESolTipo msjOpCrearExpedienteCORESol) throws MsjOpCrearExpedienteCOREFallo {

        
        LOG.info("Op: opCrearExpedienteCORE ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearExpedienteCORESol.getContextoTransaccional();
        final ExpedienteTipo expedienteTipo = msjOpCrearExpedienteCORESol.getExpediente();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            expedienteFacade.crearExpediente(expedienteTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearExpedienteCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearExpedienteCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearExpedienteCORERespTipo resp = new OpCrearExpedienteCORERespTipo();
        resp.setContextoRespuesta(cr);

        
        LOG.info("Op: opCrearExpedienteCORE ::: END");
        
        return resp;
    }

    public OpBuscarPorIdExpedienteCORERespTipo opBuscarPorIdExpedienteCORE(OpBuscarPorIdExpedienteCORESolTipo msjOpBuscarPorIdExpedienteCORESol) throws MsjOpBuscarPorIdExpedienteCOREFallo {

        
        LOG.info("Op: opBuscarPorIdExpedienteCORE ::: INIT");
        
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdExpedienteCORESol.getContextoTransaccional();
        final List<String> idExpedienteList = msjOpBuscarPorIdExpedienteCORESol.getIdExpediente();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final List<ExpedienteTipo> expedienteTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            expedienteTipoList = expedienteFacade.buscarPorIdExpediente(idExpedienteList);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdExpedienteCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdExpedienteCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdExpedienteCORERespTipo resp = new OpBuscarPorIdExpedienteCORERespTipo();
        resp.setContextoRespuesta(cr);
        resp.getExpediente().addAll(expedienteTipoList);

        
        LOG.info("Op: opBuscarPorIdExpedienteCORE ::: END");
        
        return resp;
    }

}
