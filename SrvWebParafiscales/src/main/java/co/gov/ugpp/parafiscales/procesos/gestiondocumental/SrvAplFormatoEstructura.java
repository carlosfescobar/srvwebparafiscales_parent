package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1.MsjOpBuscarPorCriteriosFormatoEstructuraFallo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1.OpBuscarPorCriteriosFormatoEstructuraRespTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplFormatoEstructura",
        portName = "portSrvAplFormatoEstructuraSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1.PortSrvAplFormatoEstructuraSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructura/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplFormatoEstructura extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplFormatoEstructura.class);

    @EJB
    private FormatoEstructuraFacade formatoEstructuraFacade;

    public co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1.OpBuscarPorCriteriosFormatoEstructuraRespTipo opBuscarPorCriteriosFormatoEstructura(co.gov.ugpp.gestiondocumental.srvaplformatoestructura.v1.OpBuscarPorCriteriosFormatoEstructuraSolTipo msjOpBuscarPorCriteriosFormatoEstructuraSol) throws MsjOpBuscarPorCriteriosFormatoEstructuraFallo {

        
        LOG.info("Op: opBuscarPorCriteriosFormatoEstructura ::: INIT");
        
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosFormatoEstructuraSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosFormatoEstructuraSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<FormatoEstructuraTipo> formatoEstructuraTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            formatoEstructuraTiposPagerData = formatoEstructuraFacade.buscarPorCriteriosFormatoEstructura(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFormatoEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFormatoEstructuraFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosFormatoEstructuraRespTipo resp = new OpBuscarPorCriteriosFormatoEstructuraRespTipo();
        cr.setValCantidadPaginas(formatoEstructuraTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getEstructuraDocumento().addAll(formatoEstructuraTiposPagerData.getData());

        
        LOG.info("Op: opBuscarPorCriteriosFormatoEstructura ::: END");
        
        return resp;
    }

}
