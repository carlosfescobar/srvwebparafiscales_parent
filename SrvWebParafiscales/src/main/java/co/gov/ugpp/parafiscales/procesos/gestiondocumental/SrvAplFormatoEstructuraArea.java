package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.MsjOpActualizarFormatoEstructuraAreaFallo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.MsjOpBuscarPorCriteriosFormatoEstructuraAreaFallo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.MsjOpCrearFormatoEstructuraAreaFallo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpActualizarFormatoEstructuraAreaRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpActualizarFormatoEstructuraAreaSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpCrearFormatoEstructuraAreaRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.OpCrearFormatoEstructuraAreaSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1.FormatoEstructuraAreaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplFormatoEstructuraArea",
        portName = "portSrvAplFormatoEstructuraAreaSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplformatoestructuraarea.v1.PortSrvAplFormatoEstructuraAreaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFormatoEstructuraArea/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplFormatoEstructuraArea extends AbstractSrvApl {

    @EJB
    private FormatoEstructuraAreaFacade formatoEstructuraAreaFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplFormatoEstructuraArea.class);

    public OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo opBuscarPorCriteriosFormatoEstructuraArea(OpBuscarPorCriteriosFormatoEstructuraAreaSolTipo msjOpBuscarPorCriteriosFormatoEstructuraAreaSol) throws MsjOpBuscarPorCriteriosFormatoEstructuraAreaFallo {
        
        LOG.info("Op: opBuscarPorCriteriosFormatoEstructuraArea ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosFormatoEstructuraAreaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosFormatoEstructuraAreaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<FormatoEstructuraAreaTipo> formatoAreaTipoPagerData;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            formatoAreaTipoPagerData = formatoEstructuraAreaFacade.buscarPorCriteriosFormatoEstructuraArea(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo resp = new OpBuscarPorCriteriosFormatoEstructuraAreaRespTipo();
        cr.setValCantidadPaginas(formatoAreaTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getFormatoEstructuraArea().addAll(formatoAreaTipoPagerData.getData());
        
        LOG.info("Op: opBuscarPorCriteriosFormatoEstructuraArea ::: END");
        
        return resp;
    }

    public OpActualizarFormatoEstructuraAreaRespTipo opActualizarFormatoEstructuraArea(OpActualizarFormatoEstructuraAreaSolTipo msjOpActualizarFormatoEstructuraAreaSol) throws MsjOpActualizarFormatoEstructuraAreaFallo {
       
        
        LOG.info("Op: opActualizarFormatoEstructuraArea ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarFormatoEstructuraAreaSol.getContextoTransaccional();
        final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo = msjOpActualizarFormatoEstructuraAreaSol.getFormatoEstructuraArea();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            formatoEstructuraAreaFacade.actualizarFormatoEstructuraArea(formatoEstructuraAreaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarFormatoEstructuraAreaRespTipo resp = new OpActualizarFormatoEstructuraAreaRespTipo();
        resp.setContextoRespuesta(cr);
        
        LOG.info("Op: opActualizarFormatoEstructuraArea ::: END");
        
        return resp;
    }

    public OpCrearFormatoEstructuraAreaRespTipo opCrearFormatoEstructuraArea(OpCrearFormatoEstructuraAreaSolTipo msjOpCrearFormatoEstructuraAreaSol) throws MsjOpCrearFormatoEstructuraAreaFallo {
        
        LOG.info("Op: opCrearFormatoEstructuraArea ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearFormatoEstructuraAreaSol.getContextoTransaccional();
        final FormatoEstructuraAreaTipo formatoEstructuraAreaTipo = msjOpCrearFormatoEstructuraAreaSol.getFormatoEstructuraArea();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            formatoEstructuraAreaFacade.crearFormatoEstructuraArea(formatoEstructuraAreaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFormatoEstructuraAreaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearFormatoEstructuraAreaRespTipo resp = new OpCrearFormatoEstructuraAreaRespTipo();
        resp.setContextoRespuesta(cr);
        
        LOG.info("Op: opCrearFormatoEstructuraArea ::: END");
        
        return resp;
    }

}
