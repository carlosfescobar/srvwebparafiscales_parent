/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.MsjOpActualizarTrazabilidadRadicadoSalidaFallo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.MsjOpBuscarPorCriteriosTrazaRadicadoSalidaFallo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.MsjOpCrearTrazabilidadRadicadoSalidaFallo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpActualizarTrazabilidadRadicadoSalidaRespTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpActualizarTrazabilidadRadicadoSalidaSolTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpCrearTrazabilidadRadicadoSalidaRespTipo;
import co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.OpCrearTrazabilidadRadicadoSalidaSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplTrazabilidadRadicadoSalida",
        portName = "portSrvAplTrazabilidadRadicadoSalidaSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvapltrazabilidadradicadosalida.v1.PortSrvAplTrazabilidadRadicadoSalidaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplTrazabilidadRadicadoSalida/v1")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplTrazabilidadRadicadoSalida extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplArchivo.class);
    @EJB
    private TrazabilidadRadicadoSalidaFacade trazabilidadRadicadoSalidaFacade;

    public OpCrearTrazabilidadRadicadoSalidaRespTipo opCrearTrazabilidadRadicadoSalida(OpCrearTrazabilidadRadicadoSalidaSolTipo msjOpCrearTrazabilidadRadicadoSalidaSol) throws MsjOpCrearTrazabilidadRadicadoSalidaFallo {
        
        LOG.info("Op: opCrearTrazabilidadRadicadoSalida ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearTrazabilidadRadicadoSalidaSol.getContextoTransaccional();
        final AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo = msjOpCrearTrazabilidadRadicadoSalidaSol.getAcuseEntregaDocumento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            trazabilidadRadicadoSalidaFacade.crearTrazabilidadRadicadoSalida(acuseEntregaDocumentoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadRadicadoSalidaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadRadicadoSalidaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearTrazabilidadRadicadoSalidaRespTipo resp = new OpCrearTrazabilidadRadicadoSalidaRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("Op: opCrearTrazabilidadRadicadoSalida ::: END");
        
        return resp;
    }

    public OpActualizarTrazabilidadRadicadoSalidaRespTipo opActualizarTrazabilidadRadicadoSalida(OpActualizarTrazabilidadRadicadoSalidaSolTipo msjOpActualizarTrazabilidadRadicadoSalidaSol) throws MsjOpActualizarTrazabilidadRadicadoSalidaFallo {
        
        LOG.info("Op: opActualizarTrazabilidadRadicadoSalida ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarTrazabilidadRadicadoSalidaSol.getContextoTransaccional();
        final AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo = msjOpActualizarTrazabilidadRadicadoSalidaSol.getAcuseEntregaDocumento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            trazabilidadRadicadoSalidaFacade.actualizarTrazabilidadRadicado(acuseEntregaDocumentoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadRadicadoSalidaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadRadicadoSalidaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarTrazabilidadRadicadoSalidaRespTipo resp = new OpActualizarTrazabilidadRadicadoSalidaRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("Op: opActualizarTrazabilidadRadicadoSalida ::: END");
        
        return resp;
    }

    public OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo opBuscarPorCriteriosTrazaRadicadoSalida(OpBuscarPorCriteriosTrazaRadicadoSalidaSolTipo msjOpBuscarPorCriteriosTrazaRadicadoSalidaSol) throws MsjOpBuscarPorCriteriosTrazaRadicadoSalidaFallo {

        LOG.info("Op: opBuscarPorCriteriosTrazaRadicadoSalida ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosTrazaRadicadoSalidaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosTrazaRadicadoSalidaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AcuseEntregaDocumentoTipo> acuseEntregaDocumentoTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            acuseEntregaDocumentoTipoPagerData = trazabilidadRadicadoSalidaFacade.buscarPorCriteriosTrazaRadicadoSalida(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosTrazaRadicadoSalidaFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosTrazaRadicadoSalidaFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);

        }
        final OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo resp = new OpBuscarPorCriteriosTrazaRadicadoSalidaRespTipo();
        cr.setValCantidadPaginas(acuseEntregaDocumentoTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getAcuseEntregaDocumento().addAll(acuseEntregaDocumentoTipoPagerData.getData());

        LOG.info("Op: opBuscarPorCriteriosTrazaRadicadoSalida ::: END");
        
        return resp;
    }

}
