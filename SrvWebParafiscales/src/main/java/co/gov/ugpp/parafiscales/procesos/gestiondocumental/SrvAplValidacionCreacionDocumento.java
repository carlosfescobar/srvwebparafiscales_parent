package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.MsjOpBuscarPorCriteriosValidacionCreacionDocumentoFallo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.MsjOpCrearValidacionCreacionDocumentoFallo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.OpCrearValidacionCreacionDocumentoRespTipo;
import co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.OpCrearValidacionCreacionDocumentoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodriguez
 */
@WebService(serviceName = "SrvAplValidacionCreacionDocumento",
        portName = "portSrvAplValidacionCreacionDocumentoSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplvalidacioncreaciondocumento.v1.PortSrvAplValidacionCreacionDocumentoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplValidacionCreacionDocumento/v1")
public class SrvAplValidacionCreacionDocumento extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplValidacionCreacionDocumento.class);
    @EJB
    private ValidacioCreacionDocumentoFacade validacioCreacionDocumentoFacade;

    public OpCrearValidacionCreacionDocumentoRespTipo opCrearValidacionCreacionDocumento(OpCrearValidacionCreacionDocumentoSolTipo msjOpCrearValidacionCreacionDocumentoSol) throws MsjOpCrearValidacionCreacionDocumentoFallo {
        
        LOG.info("Op: opCrearValidacionCreacionDocumento ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearValidacionCreacionDocumentoSol.getContextoTransaccional();
        final ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo = msjOpCrearValidacionCreacionDocumentoSol.getValidacionCreacionDocumento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long validacionCreacionDocumentoPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            validacionCreacionDocumentoPK = validacioCreacionDocumentoFacade.crearValidacionCreacionDocumento(validacionCreacionDocumentoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearValidacionCreacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearValidacionCreacionDocumentoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearValidacionCreacionDocumentoRespTipo resp = new OpCrearValidacionCreacionDocumentoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdValidacionCreacionDocumento(validacionCreacionDocumentoPK.toString());

        
        LOG.info("Op: opCrearValidacionCreacionDocumento ::: END");
        
        return resp;
    }

    public OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo opBuscarPorCriteriosValidacionCreacionDocumento(OpBuscarPorCriteriosValidacionCreacionDocumentoSolTipo msjOpBuscarPorCriteriosValidacionCreacionDocumentoSol) throws MsjOpBuscarPorCriteriosValidacionCreacionDocumentoFallo {
        
        LOG.info("Op: opBuscarPorCriteriosValidacionCreacionDocumento ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosValidacionCreacionDocumentoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosValidacionCreacionDocumentoSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<ValidacionCreacionDocumentoTipo> ValidacionCreacionDocumentoTipoaPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            ValidacionCreacionDocumentoTipoaPagerData = validacioCreacionDocumentoFacade.buscarPorCriteriosValidacionCreacionDocumento(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosValidacionCreacionDocumentoFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosValidacionCreacionDocumentoFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo resp = new OpBuscarPorCriteriosValidacionCreacionDocumentoRespTipo();
        cr.setValCantidadPaginas(ValidacionCreacionDocumentoTipoaPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getValidacionCreacionDocumento().addAll(ValidacionCreacionDocumentoTipoaPagerData.getData());

        LOG.info("Op: opBuscarPorCriteriosValidacionCreacionDocumento ::: END");
        
        return resp;

    }

}
