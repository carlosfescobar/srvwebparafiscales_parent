package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface TrazabilidadRadicadoSalidaFacade extends Serializable {

    void actualizarTrazabilidadRadicado(AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void crearTrazabilidadRadicadoSalida(AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<AcuseEntregaDocumentoTipo> buscarPorCriteriosTrazaRadicadoSalida(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
