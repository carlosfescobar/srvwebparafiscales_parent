package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.TrazabilidadRadicadoSalidaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadRadicadoSalida;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author zrodrigu
 */
@Stateless
@TransactionManagement
public class TrazabilidadRadicadoSalidaFacadeImpl extends AbstractFacade implements TrazabilidadRadicadoSalidaFacade {

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private TrazabilidadRadicadoSalidaDao trazabilidadRadicadoSalidaDao;

    @Override

    public void actualizarTrazabilidadRadicado(AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (acuseEntregaDocumentoTipo == null || StringUtils.isBlank(acuseEntregaDocumentoTipo.getNumRadicadoSalida())) {
            throw new AppException("Debe proporcionar el IdRadicado a actualizar");
        }
        List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        TrazabilidadRadicadoSalida trazabilidadRadicadoSalida = trazabilidadRadicadoSalidaDao.findByRadicadoSalida((acuseEntregaDocumentoTipo.getNumRadicadoSalida()));

        if (trazabilidadRadicadoSalida == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un IdRadicadoSalida con el valor:" + acuseEntregaDocumentoTipo.getNumRadicadoSalida()));
            throw new AppException(errorTipoList);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        trazabilidadRadicadoSalida.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populeteTrazabilidadRadicadoSalida(trazabilidadRadicadoSalida, acuseEntregaDocumentoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        trazabilidadRadicadoSalidaDao.edit(trazabilidadRadicadoSalida);

    }

    @Override
    public void crearTrazabilidadRadicadoSalida(AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (acuseEntregaDocumentoTipo == null) {
            throw new AppException("se debe proporcionar el objeto a crear");
        }
        List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checktrazabilidadRadicadoSalida(acuseEntregaDocumentoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        final TrazabilidadRadicadoSalida trazabilidadRadicadoSalida = new TrazabilidadRadicadoSalida();

        trazabilidadRadicadoSalida.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populeteTrazabilidadRadicadoSalida(trazabilidadRadicadoSalida, acuseEntregaDocumentoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        trazabilidadRadicadoSalidaDao.create(trazabilidadRadicadoSalida);
    }

    @Override
    public PagerData<AcuseEntregaDocumentoTipo> buscarPorCriteriosTrazaRadicadoSalida(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<TrazabilidadRadicadoSalida> pagerDataEntity = trazabilidadRadicadoSalidaDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipoList,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<AcuseEntregaDocumentoTipo> acuseEntregaDocumentoTipoList = mapper.map(pagerDataEntity.getData(),
                TrazabilidadRadicadoSalida.class, AcuseEntregaDocumentoTipo.class);

        return new PagerData<AcuseEntregaDocumentoTipo>(acuseEntregaDocumentoTipoList, pagerDataEntity.getNumPages());
    }

    public void populeteTrazabilidadRadicadoSalida(final TrazabilidadRadicadoSalida trazabilidadRadicadoSalida,
            final AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(acuseEntregaDocumentoTipo.getNumRadicadoSalida())) {
            trazabilidadRadicadoSalida.setNumRadicadoSalida(acuseEntregaDocumentoTipo.getNumRadicadoSalida());

        }
        if (StringUtils.isNotBlank(acuseEntregaDocumentoTipo.getNumGuia())) {
            trazabilidadRadicadoSalida.setIdGuia(acuseEntregaDocumentoTipo.getNumGuia());
        }
        if (StringUtils.isNotBlank(acuseEntregaDocumentoTipo.getDescCausalDevolucion())) {
            trazabilidadRadicadoSalida.setDescCausalDevolucion(acuseEntregaDocumentoTipo.getDescCausalDevolucion());
        }

        if (acuseEntregaDocumentoTipo.getFecEntregaDevolucion() != null) {
            trazabilidadRadicadoSalida.setFecEntregaDevolucion(acuseEntregaDocumentoTipo.getFecEntregaDevolucion());

        }
        if (acuseEntregaDocumentoTipo.getFecEnvio() != null) {
            trazabilidadRadicadoSalida.setFecEnvio(acuseEntregaDocumentoTipo.getFecEnvio());
        }
        if (StringUtils.isNotBlank(acuseEntregaDocumentoTipo.getCodEstadoEntrega())) {
            ValorDominio codEstadoEntregaRadicado = valorDominioDao.find(acuseEntregaDocumentoTipo.getCodEstadoEntrega());
            if (codEstadoEntregaRadicado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoEntregaRadicado con el ID: " + acuseEntregaDocumentoTipo.getCodEstadoEntrega()));
            } else {
                trazabilidadRadicadoSalida.setCodEstadoEntrega(codEstadoEntregaRadicado);
            }
        }

        if (StringUtils.isNotBlank(acuseEntregaDocumentoTipo.getCodEstadoEnvio())) {
            ValorDominio codEstadoEnvio = valorDominioDao.find(acuseEntregaDocumentoTipo.getCodEstadoEnvio());
            if (codEstadoEnvio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoEnvio con el ID: " + acuseEntregaDocumentoTipo.getCodEstadoEnvio()));
            } else {
                trazabilidadRadicadoSalida.setCodEstadoEnvio(codEstadoEnvio);
            }
        }

    }
}
