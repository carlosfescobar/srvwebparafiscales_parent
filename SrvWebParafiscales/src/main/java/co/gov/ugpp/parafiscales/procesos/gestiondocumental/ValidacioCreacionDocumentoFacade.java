package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface ValidacioCreacionDocumentoFacade extends Serializable {

    Long crearValidacionCreacionDocumento(ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<ValidacionCreacionDocumentoTipo> buscarPorCriteriosValidacionCreacionDocumento(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
