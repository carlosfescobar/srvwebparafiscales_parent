package co.gov.ugpp.parafiscales.procesos.gestiondocumental;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionCreacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionCreacionDocumento;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ValidacioCreacionDocumentoFacade que contiene las operaciones del servicio
 * SrvAplValidacioCreacionDocumento
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ValidacionCreacionDocumentoFacadeImpl extends AbstractFacade implements ValidacioCreacionDocumentoFacade {

    @EJB
    private ValidacionCreacionDocumentoDao validacionCreacionDocumentoDao;

    /**
     * implementacion de la operación crearValidacionCreacionDocumento esta se encarga de crear una validacion de la
     * creacion de un documento
     *
     * @param validacionCreacionDocumentoTipo Objeto a partir del cual se crea la validacion
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return idValidacionCreacionDocumento creado que se retorna junto con el contexto transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public Long crearValidacionCreacionDocumento(ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (validacionCreacionDocumentoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final ValidacionCreacionDocumento validacionCreacionDocumento = new ValidacionCreacionDocumento();

        validacionCreacionDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateValidacionCreacionDocumento(validacionCreacionDocumentoTipo, validacionCreacionDocumento, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idValidacionCreacionDocumento = validacionCreacionDocumentoDao.create(validacionCreacionDocumento);

        return idValidacionCreacionDocumento;
    }
/**
 *  Método utilizado para Crear  la entidad validacionCreacionDocumento
 * @param validacionCreacionDocumentoTipo objeto origen
 * @param validacionCreacionDocumento Objeto destino
 * @param errorTipoList Listado que almacena errores de negocio
 * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
 */
    private void populateValidacionCreacionDocumento(ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo,
            ValidacionCreacionDocumento validacionCreacionDocumento, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(validacionCreacionDocumentoTipo.getEsAprobado())) {
            Validator.parseBooleanFromString(validacionCreacionDocumentoTipo.getEsAprobado(),
                    validacionCreacionDocumentoTipo.getClass().getSimpleName(),
                    "esAprobado", errorTipoList);
            validacionCreacionDocumento.setESAprobado(mapper.map(validacionCreacionDocumentoTipo.getEsAprobado(), Boolean.class));
        }
        if (StringUtils.isNotBlank(validacionCreacionDocumentoTipo.getFechaValidacion())) {
            validacionCreacionDocumento.setFecValidacion(DateUtil.parseStrDateTimeToCalendar(validacionCreacionDocumentoTipo.getFechaValidacion()));
        }
        if (StringUtils.isNotBlank(validacionCreacionDocumentoTipo.getValComentarios())) {
            validacionCreacionDocumento.setValComentarios(validacionCreacionDocumentoTipo.getValComentarios());
        }
    }

    /**
     * implementación de la operación buscarPorCriteriosValidacionCreacionDocumentola cula se encarga de buscar una
     * validacion de la creacion de un documento por medio de un listado de parametros envidos y ordena la consulta de
     * acuerdo a los criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a buscar una Validacion
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return listado de Validaciones encontrado que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de servicios.
     */
    @Override
    public PagerData<ValidacionCreacionDocumentoTipo> buscarPorCriteriosValidacionCreacionDocumento(
            List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ValidacionCreacionDocumento> pagerDataEntity
                = validacionCreacionDocumentoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<ValidacionCreacionDocumentoTipo> validacionCreacionDocumentoTipoList = mapper.map(pagerDataEntity.getData(),
                ValidacionCreacionDocumento.class, ValidacionCreacionDocumentoTipo.class);

        return new PagerData<ValidacionCreacionDocumentoTipo>(validacionCreacionDocumentoTipoList, pagerDataEntity.getNumPages());
    }

}
