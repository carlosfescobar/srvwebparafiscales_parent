package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface AgrupacionAfiliacionEntidadFacade extends Serializable {

    Long crearAgrupacionAfiliacionesEntidad(AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarAgrupacionAfiliacionesEntidad(AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<AgrupacionAfiliacionesPorEntidadTipo> buscarPorIdsAgrupacionAfiliacionesEntidad(List<String> idsAgrupacionAfiliacionList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
