package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionAfiliacionEntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz AgrupacionAfiliacionEntidadFacade que contiene las operaciones
 * del servicio SrvAplAgrupacionAfiliacionesEntidad
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AgrupacionAfiliacionEntidadFacadeImpl extends AbstractFacade implements AgrupacionAfiliacionEntidadFacade {

    @EJB
    private AgrupacionAfiliacionEntidadExternaDao agrupacionAfiliacionEntidadExternaDao;

    @EJB
    private Populator populator;

    @Override
    public Long crearAgrupacionAfiliacionesEntidad(AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (agrupacionAfiliacionesPorEntidadTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad = new AgrupacionAfiliacionesPorEntidad();

        agrupacionAfiliacionesPorEntidad.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAgrupacionAfiliacionEntidad(contextoTransaccionalTipo, agrupacionAfiliacionesPorEntidadTipo, agrupacionAfiliacionesPorEntidad, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idAgrupacionAfiliacion = agrupacionAfiliacionEntidadExternaDao.create(agrupacionAfiliacionesPorEntidad);
        return idAgrupacionAfiliacion;
    }

    @Override
    public void actualizarAgrupacionAfiliacionesEntidad(AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (agrupacionAfiliacionesPorEntidadTipo == null || StringUtils.isBlank(agrupacionAfiliacionesPorEntidadTipo.getIdAgrupacionAfiliacionesPorEntidad())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad
                = agrupacionAfiliacionEntidadExternaDao.find(Long.valueOf(agrupacionAfiliacionesPorEntidadTipo.getIdAgrupacionAfiliacionesPorEntidad()));

        if (agrupacionAfiliacionesPorEntidad == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "La agrupacionAfiliacionesEntidad que se desea Actualizar no existe con el ID: " + agrupacionAfiliacionesPorEntidadTipo.getIdAgrupacionAfiliacionesPorEntidad()));
            throw new AppException(errorTipoList);
        }

        agrupacionAfiliacionesPorEntidad.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAgrupacionAfiliacionEntidad(contextoTransaccionalTipo, agrupacionAfiliacionesPorEntidadTipo, agrupacionAfiliacionesPorEntidad, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        agrupacionAfiliacionEntidadExternaDao.edit(agrupacionAfiliacionesPorEntidad);
    }

    @Override
    public List<AgrupacionAfiliacionesPorEntidadTipo> buscarPorIdsAgrupacionAfiliacionesEntidad(List<String> idsAgrupacionAfiliacionList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idsAgrupacionAfiliacionList == null
                || idsAgrupacionAfiliacionList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }

        final List<Long> ids = mapper.map(idsAgrupacionAfiliacionList, String.class, Long.class);

        final List<AgrupacionAfiliacionesPorEntidad> agrupacionAfiliacionesEntidadList
                = agrupacionAfiliacionEntidadExternaDao.findByIdList(ids);

        final List<AgrupacionAfiliacionesPorEntidadTipo> agrupacionAfiliacionesEntidadTipoList
                = mapper.map(agrupacionAfiliacionesEntidadList, AgrupacionAfiliacionesPorEntidad.class, AgrupacionAfiliacionesPorEntidadTipo.class);

        return agrupacionAfiliacionesEntidadTipoList;
    }

    private void populateAgrupacionAfiliacionEntidad(final ContextoTransaccionalTipo contextoTransaccionalTipo, final AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo,
            final AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad, final List<ErrorTipo> errorTipoList) throws AppException {

        populator.populateAgrupacionAfiliacionEntidad(agrupacionAfiliacionesPorEntidadTipo, agrupacionAfiliacionesPorEntidad, contextoTransaccionalTipo, errorTipoList);
    }
}
