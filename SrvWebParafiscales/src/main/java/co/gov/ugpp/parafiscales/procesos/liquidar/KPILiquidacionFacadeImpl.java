package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.LiquidacionDao;
import co.gov.ugpp.parafiscales.persistence.entity.Liquidacion;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz LiquidacionFacade que contiene las operaciones
 * del servicio SrvAplLiquidacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class KPILiquidacionFacadeImpl extends AbstractFacade implements KPILiquidacionFacade {

    @EJB
    private LiquidacionDao liquidacionDao;

   
    @Override
    public PagerData<LiquidacionTipo> consultarLiquidacionKPI(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        
        final PagerData<Liquidacion> liquidacionPagerData = liquidacionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<LiquidacionTipo> liquidacionTipoList = new ArrayList<LiquidacionTipo>();

        for (final Liquidacion liquidacion : liquidacionPagerData.getData()) {
            liquidacion.setUseOnlySpecifiedFields(true);
            final LiquidacionTipo liquidacionTipo = mapper.map(liquidacion, LiquidacionTipo.class);
            liquidacionTipoList.add(liquidacionTipo);
        }
        return new PagerData(liquidacionTipoList, liquidacionPagerData.getNumPages());
    }

}
