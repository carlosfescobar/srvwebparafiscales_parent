package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface LiquidacionFacade extends Serializable {

    Long crearLiquidacion(LiquidacionTipo liquidacionTipofinal, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarLiquidacion(LiquidacionTipo liquidacionTipofinal, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    List<LiquidacionTipo> buscarPorIdsLiquidacion(List<String> idsLiquidacionList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<LiquidacionTipo> buscarPorCriteriosLiquidacion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
