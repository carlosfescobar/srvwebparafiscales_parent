package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AgrupacionAfiliacionEntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConstanciaEjecutoriaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ControlLiquidadorDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.LiquidacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaEjecutoria;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Liquidacion;
import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionAccion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz LiquidacionFacade que contiene las operaciones
 * del servicio SrvAplLiquidacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class LiquidacionFacadeImpl extends AbstractFacade implements LiquidacionFacade {

    @EJB
    private LiquidacionDao liquidacionDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private FiscalizacionDao fiscalizacionDao;

    @EJB
    private ExpedienteDao expedienteDao;
    
    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ControlLiquidadorDao controlLiquidadorDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private ValorDominioDao ValorDominioDao;

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private ConstanciaEjecutoriaDao constanciaEjecutoriaDao;

    @EJB
    private AgrupacionAfiliacionEntidadExternaDao agrupacionAfiliacionEntidadExternaDao;

    /**
     * implementación de la operación crearLiquidacion esta se encarga de crear
     * una nueva liquidacion en la base de datos a partir del objeto
     * liquidacionTipo enviado
     *
     * @param liquidacionTipo objeto a partir del cual se va a crear una nueva
     * liquidacion
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Nueva liquidacion creada qe se retorna junto con el contexto
     * Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearLiquidacion(LiquidacionTipo liquidacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (liquidacionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkLiquidacion(liquidacionTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Liquidacion liquidacion = new Liquidacion();

        checkBusinessLiquidacion(liquidacion, liquidacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        liquidacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateLiquidacion(liquidacionTipo, liquidacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idLiquidacion = liquidacionDao.create(liquidacion);
        return idLiquidacion;

    }

    @Override
    public void actualizarLiquidacion(LiquidacionTipo liquidacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (liquidacionTipo == null || StringUtils.isBlank(liquidacionTipo.getIdLiquidacion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Liquidacion liquidacion = liquidacionDao.find(Long.valueOf(liquidacionTipo.getIdLiquidacion()));

        if (liquidacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "La liquidación que se desea Actualizar no existe con el ID: " + liquidacionTipo.getIdLiquidacion()));
            throw new AppException(errorTipoList);
        }
        checkBusinessLiquidacion(liquidacion, liquidacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        liquidacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateLiquidacion(liquidacionTipo, liquidacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        liquidacionDao.edit(liquidacion);
    }

    @Override
    public List<LiquidacionTipo> buscarPorIdsLiquidacion(List<String> idLiquidacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idLiquidacionTipos == null || idLiquidacionTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idLiquidacionTipos, String.class, Long.class);

        final List<Liquidacion> liquidacionList = liquidacionDao.findByIdList(ids);

        final List<LiquidacionTipo> liquidacionTipoList
                = mapper.map(liquidacionList, Liquidacion.class, LiquidacionTipo.class);

        if (liquidacionTipoList != null) {
            for (final LiquidacionTipo liquidacionTipo : liquidacionTipoList) {
                if (liquidacionTipo.getLiquidador() != null
                        && StringUtils.isNotBlank(liquidacionTipo.getLiquidador().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getLiquidador(), contextoTransaccionalTipo);
                    liquidacionTipo.setLiquidador(funcionarioTipo);
                }
                if (liquidacionTipo.getSubdirectorCobranzas() != null && StringUtils.isNotBlank(liquidacionTipo.getSubdirectorCobranzas().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getSubdirectorCobranzas(), contextoTransaccionalTipo);
                    liquidacionTipo.setSubdirectorCobranzas(funcionarioTipo);
                }
            }
        }

        return liquidacionTipoList;
    }

    @Override
    public PagerData<LiquidacionTipo> buscarPorCriteriosLiquidacion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Liquidacion> pagerDataEntity = liquidacionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<LiquidacionTipo> liquidacionTipoLits = mapper.map(pagerDataEntity.getData(),
                Liquidacion.class, LiquidacionTipo.class);

        if (liquidacionTipoLits != null) {
            for (final LiquidacionTipo liquidacionTipo : liquidacionTipoLits) {
                if (liquidacionTipo.getLiquidador() != null
                        && StringUtils.isNotBlank(liquidacionTipo.getLiquidador().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getLiquidador(), contextoTransaccionalTipo);
                    liquidacionTipo.setLiquidador(funcionarioTipo);
                }
                if (liquidacionTipo.getSubdirectorCobranzas() != null && StringUtils.isNotBlank(liquidacionTipo.getSubdirectorCobranzas().getIdFuncionario())) {

                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getSubdirectorCobranzas(), contextoTransaccionalTipo);
                    liquidacionTipo.setSubdirectorCobranzas(funcionarioTipo);
                }
            }
        }
        return new PagerData<LiquidacionTipo>(liquidacionTipoLits, pagerDataEntity.getNumPages());

    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad Liquidacion
     *
     * @param liquidacionTipo Objeto origne
     * @param liquidacion Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateLiquidacion(final LiquidacionTipo liquidacionTipo,
            final Liquidacion liquidacion, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (liquidacionTipo.getExpediente() != null
                && StringUtils.isNotBlank(liquidacionTipo.getExpediente().getIdNumExpediente())) {
            final Expediente expediente = expedienteDao.find(liquidacionTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El Expediente proporcionado no existe con el ID: " + liquidacionTipo.getExpediente().getIdNumExpediente()));
            } else {
                liquidacion.setExpediente(expediente);
            }
        }
        
        if (liquidacionTipo.getPeriodoFiscalizacion() != null) {
            if (liquidacionTipo.getPeriodoFiscalizacion().getFecInicio().
                    after(liquidacionTipo.getPeriodoFiscalizacion().getFecFin())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "La Fecha Fin del periodo de la Fiscalización debe ser mayor a la Fecha de Inicio"));
            } else {
                liquidacionTipo.setPeriodoFiscalizacion(Util.aproximarPeriodo(liquidacionTipo.getPeriodoFiscalizacion().getFecInicio(), liquidacionTipo.getPeriodoFiscalizacion().getFecFin()));
                liquidacion.setFecInicio(liquidacionTipo.getPeriodoFiscalizacion().getFecInicio());
                liquidacion.setFecFin(liquidacionTipo.getPeriodoFiscalizacion().getFecFin());
            }
        }
        if (liquidacionTipo.getAportante() != null) {
            IdentificacionTipo identificacionAportante = Util.obtenerIdentificacionTipoFromChoice(liquidacionTipo.getAportante().getPersonaNatural(), liquidacionTipo.getAportante().getPersonaJuridica());
            if (identificacionAportante != null){
                final Identificacion identificacion = mapper.map(identificacionAportante, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);
                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un aportante con la identificación :" + identificacion));
                } else {
                    liquidacion.setAportante(aportante);
                }
            }
        }

        if (liquidacionTipo.getIdDocumentoConstanciaEjecutoria() != null
                && StringUtils.isNotBlank(liquidacionTipo.getIdDocumentoConstanciaEjecutoria())) {
            ConstanciaEjecutoria constanciaEjecutoria = constanciaEjecutoriaDao.find(Long.parseLong(liquidacionTipo.getIdDocumentoConstanciaEjecutoria()));
            if (constanciaEjecutoria == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una constanciaEjecutoria con el valor:" + liquidacionTipo.getIdDocumentoConstanciaEjecutoria()));
            } else {
                liquidacion.setidDocumentoConstanciaEjecutoria(constanciaEjecutoria);
            }
        }

        if (liquidacionTipo.getRequerimientoDeclararAmpliadoParcial() != null
                && StringUtils.isNotBlank(liquidacionTipo.getRequerimientoDeclararAmpliadoParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(liquidacionTipo.getRequerimientoDeclararAmpliadoParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró  un requerimientoDeclararAmpliadoParcial con el ID: " + liquidacionTipo.getRequerimientoDeclararAmpliadoParcial().getIdArchivo()));
            } else {
                liquidacion.setRequerimientoDeclararAmpliadoParcial(aprobacionDocumento);
            }
        }
        if (liquidacionTipo.getLiquidacionOficialParcial() != null
                && StringUtils.isNotBlank(liquidacionTipo.getLiquidacionOficialParcial().getIdArchivo())) {
            AprobacionDocumento liquidacionOficialParcial = aprobacionDocumentoDao.find(Long.valueOf(liquidacionTipo.getLiquidacionOficialParcial().getIdArchivo()));
            if (liquidacionOficialParcial == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un liquidacionOficialActo con el valor:" + liquidacionTipo.getLiquidacionOficialParcial().getIdArchivo()));
            } else {
                liquidacion.setLiquidacionOficialParcial(liquidacionOficialParcial);
            }
        }

        if (StringUtils.isNotBlank(liquidacionTipo.getIdActoAutoArchivo())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(liquidacionTipo.getIdActoAutoArchivo()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró actoAutoArchivo con el ID:: " + liquidacionTipo.getIdActoAutoArchivo()));
            } else {
                liquidacion.setidActoAutoArchivo(actoAdministrativo);
            }
        }

        if (StringUtils.isNotBlank(liquidacionTipo.getIdActoLiquidacionOficial())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(liquidacionTipo.getIdActoLiquidacionOficial()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró actoLiquidacionOficial con el ID: " + liquidacionTipo.getIdActoLiquidacionOficial()));
            } else {
                liquidacion.setidActoLiquidacionOficial(actoAdministrativo);
            }
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getIdFiscalizacion())) {
            final Fiscalizacion fiscalizacion
                    = fiscalizacionDao.find(Long.valueOf(liquidacionTipo.getIdFiscalizacion()));
            if (fiscalizacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró fiscalizacion con el ID: " + liquidacionTipo.getIdFiscalizacion()));
            } else {
                liquidacion.setidFiscalizacion(fiscalizacion);
            }
        }

        if (StringUtils.isNotBlank(liquidacionTipo.getIdActoRequerimientoDeclararCorregirAmpliado())) {
            final ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(String.valueOf(liquidacionTipo.getIdActoRequerimientoDeclararCorregirAmpliado()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró idActoRequerimientoDeclararCorregirAmpliado con el ID: " + liquidacionTipo.getIdActoRequerimientoDeclararCorregirAmpliado()));
            } else {
                liquidacion.setidActoRequerimientoDeclararCorregirAmpliado(actoAdministrativo);
            }
        }

        if (StringUtils.isNotBlank(liquidacionTipo.getIdLiquidadorLiquidacion())) {
            final ControlLiquidador controlLiquidador
                    = controlLiquidadorDao.find(Long.valueOf(liquidacionTipo.getIdLiquidadorLiquidacion()));
            if (controlLiquidador == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró LiquidadorLiquidacion con el ID: " + liquidacionTipo.getIdLiquidadorLiquidacion()));
            } else {
                liquidacion.setidLiquidadorLiquidacion(controlLiquidador);
            }
        }

        if (StringUtils.isNotBlank(liquidacionTipo.getIdLiquidadorLiquidacionAmpliada())) {
            final ControlLiquidador controlLiquidador
                    = controlLiquidadorDao.find(Long.valueOf(liquidacionTipo.getIdLiquidadorLiquidacionAmpliada()));
            if (controlLiquidador == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró liquidadorLiquidacionAmpliada con el ID: " + liquidacionTipo.getIdLiquidadorLiquidacionAmpliada()));
            } else {
                liquidacion.setidLiquidadorLiquidacionAmpliada(controlLiquidador);
            }
        }

        if (liquidacionTipo.getAutoArchivoParcial() != null
                && StringUtils.isNotBlank(liquidacionTipo.getAutoArchivoParcial().getIdArchivo())) {
            AprobacionDocumento autoArchivoParcial = aprobacionDocumentoDao.find(Long.valueOf(liquidacionTipo.getAutoArchivoParcial().getIdArchivo()));
            if (autoArchivoParcial == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un autoArchivoParcial con el valor:" + liquidacionTipo.getAutoArchivoParcial().getIdArchivo()));
            } else {
                liquidacion.setAutoArchivoParcial(autoArchivoParcial);
            }
        }
        
         if (StringUtils.isNotBlank(liquidacionTipo.getCodCausalCierre())) {
            ValorDominio codCausalCierre = valorDominioDao.find(liquidacionTipo.getCodCausalCierre());
            if (codCausalCierre == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codCausalCierre con el ID: " + liquidacionTipo.getCodCausalCierre()));
            } else {
                liquidacion.setCodCausalCierre(codCausalCierre);
            }
        }

        if (liquidacionTipo.getAgrupacionAfiliacionesPorEntidad() != null
                && !liquidacionTipo.getAgrupacionAfiliacionesPorEntidad().isEmpty()) {

            for (AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo : liquidacionTipo.getAgrupacionAfiliacionesPorEntidad()) {

                AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad = agrupacionAfiliacionEntidadExternaDao.find(Long.valueOf(agrupacionAfiliacionesPorEntidadTipo.getIdAgrupacionAfiliacionesPorEntidad()));
                if (agrupacionAfiliacionesPorEntidad == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un agrupacionAfiliacionesPorEntidadTipo con el ID :" + agrupacionAfiliacionesPorEntidadTipo.getIdAgrupacionAfiliacionesPorEntidad()));
                } else {
                    agrupacionAfiliacionesPorEntidad.setLiquidacion(liquidacion);
                    agrupacionAfiliacionesPorEntidad.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                    liquidacion.getAgrupacionAfiliacionesEntidad().add(agrupacionAfiliacionesPorEntidad);
                }
            }
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getCodEstadoLiquidacion())) {
            ValorDominio codEstadoLiquidacion = ValorDominioDao.find(liquidacionTipo.getCodEstadoLiquidacion());
            if (codEstadoLiquidacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstadoLiquidacion proporcionado no existe con el ID: " + liquidacionTipo.getCodEstadoLiquidacion()));
            } else {
                liquidacion.setCodEstadoLiquidacion(codEstadoLiquidacion);
            }
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getCodEstadoEnvioMemorandoCobro())) {
            ValorDominio codEstadoEnvioMemorandoCobro = ValorDominioDao.find(liquidacionTipo.getCodEstadoEnvioMemorandoCobro());
            if (codEstadoEnvioMemorandoCobro == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstadoEnvioMemorandoCobro proporcionado no existe con el ID: " + liquidacionTipo.getCodEstadoEnvioMemorandoCobro()));
            } else {
                liquidacion.setCodEstadoEnvioMemorandoCobro(codEstadoEnvioMemorandoCobro);
            }
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getValUsuarioGeneraActo())) {
            liquidacion.setValUsuarioGeneraActo(liquidacionTipo.getValUsuarioGeneraActo());
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getDescObservacionesAmpliacionRequerimiento())) {
            liquidacion.setDescObservacionesAmpliacionRequerimiento(liquidacionTipo.getDescObservacionesAmpliacionRequerimiento());
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getDescObservacionesAfiliaciones())) {
            liquidacion.setDescObservacionesAfiliaciones(liquidacionTipo.getDescObservacionesAfiliaciones());
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getEsAmpliadoRequerimiento())) {
            liquidacion.setEsAmpliadoRequerimiento(Boolean.valueOf(liquidacionTipo.getEsAmpliadoRequerimiento()));
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getEsDeterminaDeuda())) {
            liquidacion.setEsDeterminaDeuda(Boolean.valueOf(liquidacionTipo.getEsDeterminaDeuda()));
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getEsRecibidoRecursoReconsideracion())) {
            liquidacion.setEsRecibidoRecursoReconsideracion(Boolean.valueOf(liquidacionTipo.getEsRecibidoRecursoReconsideracion()));
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getEsTodosEmpleadosAfiliados())) {
            liquidacion.setEsTodosEmpleadosAfiliados(Boolean.valueOf(liquidacionTipo.getEsTodosEmpleadosAfiliados()));
        }
        if (StringUtils.isNotBlank(liquidacionTipo.getEsConfirmadaDeuda())) {
            liquidacion.setEsConfirmadaDeuda(Boolean.valueOf(liquidacionTipo.getEsConfirmadaDeuda()));
        }
        if (liquidacionTipo.getLiquidador()
                != null
                && StringUtils.isNotBlank(liquidacionTipo.getLiquidador().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getLiquidador(), contextoTransaccionalTipo);
            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró Liquidador (Funcionario) existente, con el ID: " + liquidacionTipo.getLiquidador().getIdFuncionario()));
            } else {
                liquidacion.setLiquidador(funcionarioTipo.getIdFuncionario());
            }
        }

        if (liquidacionTipo.getSubdirectorCobranzas()
                != null
                && StringUtils.isNotBlank(liquidacionTipo.getSubdirectorCobranzas().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(liquidacionTipo.getSubdirectorCobranzas(), contextoTransaccionalTipo);
            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró SubdirectorCobranzas (Funcionario) existente, con el ID: " + liquidacionTipo.getSubdirectorCobranzas().getIdFuncionario()));
            } else {
                liquidacion.setSubdirectorCobranzas(funcionarioTipo.getIdFuncionario());
            }
        }

        if (liquidacionTipo.getAcciones() != null
                && !liquidacionTipo.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : liquidacionTipo.getAcciones()) {
                final Accion accion = mapper.map(accionTipo, Accion.class);
                accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = ValorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        accion.setCodAccion(codAccion);
                    }
                    LiquidacionAccion liquidacionAccion = new LiquidacionAccion();
                    liquidacionAccion.setAccion(accion);
                    liquidacionAccion.setLiquidacion(liquidacion);
                    liquidacionAccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());

                    liquidacion.getAcciones().add(liquidacionAccion);
                }
            }
        }
    }

    private void checkBusinessLiquidacion(final Liquidacion liquidacion,
            final LiquidacionTipo liquidacionTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        //Si se va a cambiar el expediente, se valida primero que el nuevo expediente no exista en otra liquidacion
        if (liquidacionTipo.getExpediente() != null
                && StringUtils.isNotBlank(liquidacionTipo.getExpediente().getIdNumExpediente())) {
            if (liquidacion.getExpediente() != null) {
                if (!liquidacion.getExpediente().getId().equals(liquidacionTipo.getExpediente().getIdNumExpediente())) {
                    Liquidacion liquidacionOtroExpediente = liquidacionDao.findLiquidacionByExpediente(liquidacionTipo.getExpediente().getIdNumExpediente());
                    if (liquidacionOtroExpediente != null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "La liquidacion cod ID: " + liquidacionOtroExpediente.getId().toString() + ", ya está tiene asociado el expediente con ID :" + liquidacionTipo.getExpediente().getIdNumExpediente()));
                    }
                }
            } else {
                Liquidacion liquidacionOtroExpediente = liquidacionDao.findLiquidacionByExpediente(liquidacionTipo.getExpediente().getIdNumExpediente());
                if (liquidacionOtroExpediente != null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La liquidación cod ID: " + liquidacionOtroExpediente.getId().toString() + ", ya está tiene asociado el expediente con ID :" + liquidacionTipo.getExpediente().getIdNumExpediente()));
                }
            }
        }
    }
}
