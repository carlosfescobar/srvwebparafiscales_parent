package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.MsjOpActualizarAgrupacionAfiliacionesEntidadFallo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.MsjOpBuscarPorIdAgrupacionAfiliacionesEntidadFallo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.MsjOpCrearAgrupacionAfiliacionesEntidadFallo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpActualizarAgrupacionAfiliacionesEntidadRespTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpActualizarAgrupacionAfiliacionesEntidadSolTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpCrearAgrupacionAfiliacionesEntidadRespTipo;
import co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.OpCrearAgrupacionAfiliacionesEntidadSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplAgrupacionAfiliacionesEntidad",
        portName = "portSrvAplAgrupacionAfiliacionesEntidadSOAP",
        endpointInterface = "co.gov.ugpp.liquidacion.srvaplagrupacionafiliacionesentidad.v1.PortSrvAplAgrupacionAfiliacionesEntidadSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplAgrupacionAfiliacionesEntidad/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAgrupacionAfiliacionesEntidad extends AbstractSrvApl {

    @EJB
    private AgrupacionAfiliacionEntidadFacade agrupacionAfiliacionEntidadFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplAgrupacionAfiliacionesEntidad.class);

    public OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo opBuscarPorIdAgrupacionAfiliacionesEntidad(OpBuscarPorIdAgrupacionAfiliacionesEntidadSolTipo msjOpBuscarPorIdAgrupacionAfiliacionesEntidadSol) throws MsjOpBuscarPorIdAgrupacionAfiliacionesEntidadFallo {

        LOG.info("OPERACION: opBuscarPorIdConstanciaEjecutoria ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdAgrupacionAfiliacionesEntidadSol.getContextoTransaccional();
        final List<String> idAgrupacionAfiliacionEntidad = msjOpBuscarPorIdAgrupacionAfiliacionesEntidadSol.getIdsAgrupacionAfiliacionesPorEntidad();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<AgrupacionAfiliacionesPorEntidadTipo> agrupacionAfiliacionesPorEntidadTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            agrupacionAfiliacionesPorEntidadTipos = agrupacionAfiliacionEntidadFacade.buscarPorIdsAgrupacionAfiliacionesEntidad(idAgrupacionAfiliacionEntidad, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo resp = new OpBuscarPorIdAgrupacionAfiliacionesEntidadRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getAgrupacionesAfiliacionEntidad().addAll(agrupacionAfiliacionesPorEntidadTipos);

        LOG.info("OPERACION: opBuscarPorIdConstanciaEjecutoria ::: FIN");

        return resp;
    }

    public OpActualizarAgrupacionAfiliacionesEntidadRespTipo opActualizarAgrupacionAfiliacionesEntidad(OpActualizarAgrupacionAfiliacionesEntidadSolTipo msjOpActualizarAgrupacionAfiliacionesEntidadSol) throws MsjOpActualizarAgrupacionAfiliacionesEntidadFallo {

        LOG.info("OPERACION: opActualizarAgrupacionAfiliacionesEntidad ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAgrupacionAfiliacionesEntidadSol.getContextoTransaccional();
        final AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo = msjOpActualizarAgrupacionAfiliacionesEntidadSol.getAgrupacionAfiliacionesEntidad();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            agrupacionAfiliacionEntidadFacade.actualizarAgrupacionAfiliacionesEntidad(agrupacionAfiliacionesPorEntidadTipo, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarAgrupacionAfiliacionesEntidadRespTipo resp = new OpActualizarAgrupacionAfiliacionesEntidadRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opActualizarAgrupacionAfiliacionesEntidad ::: FIN");

        return resp;
    }

    public OpCrearAgrupacionAfiliacionesEntidadRespTipo opCrearAgrupacionAfiliacionesEntidad(OpCrearAgrupacionAfiliacionesEntidadSolTipo msjOpCrearAgrupacionAfiliacionesEntidadSol) throws MsjOpCrearAgrupacionAfiliacionesEntidadFallo {

        LOG.info("OPERACION: opCrearAgrupacionAfiliacionesEntidad ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAgrupacionAfiliacionesEntidadSol.getContextoTransaccional();
        final AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo = msjOpCrearAgrupacionAfiliacionesEntidadSol.getAgrupacionAfiliacionesEntidad();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idAgrupacionAfiliacionesEntidad;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            idAgrupacionAfiliacionesEntidad = agrupacionAfiliacionEntidadFacade.crearAgrupacionAfiliacionesEntidad(agrupacionAfiliacionesPorEntidadTipo, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAgrupacionAfiliacionesEntidadFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearAgrupacionAfiliacionesEntidadRespTipo resp = new OpCrearAgrupacionAfiliacionesEntidadRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdAplAgrupacionAfiliacionesEntidad(idAgrupacionAfiliacionesEntidad.toString());

        LOG.info("OPERACION: opCrearAgrupacionAfiliacionesEntidad ::: FIN");

        return resp;
    }
}
