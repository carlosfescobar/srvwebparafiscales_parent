/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1.MsjOpConsultarLiquidacionKPIFallo;
import co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1.OpConsultarLiquidacionKPIRespTipo;
import co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1.OpConsultarLiquidacionKPISolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.procesos.recursosreconsideracion.SrvAplKPIRecursoReconsideracion;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplKPILiquidacion",
        portName = "portSrvAplKPILiquidacionSOAP",
        endpointInterface = "co.gov.ugpp.liquidacion.srvaplkpiliquidacion.v1.PortSrvAplKPILiquidacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplKPILiquidacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPILiquidacion extends AbstractSrvApl {

    @EJB
    private KPILiquidacionFacade KPILiquidacionFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPIRecursoReconsideracion.class);

    public OpConsultarLiquidacionKPIRespTipo opConsultarLiquidacionKPI(OpConsultarLiquidacionKPISolTipo msjOpConsultarLiquidacionKPISol) throws MsjOpConsultarLiquidacionKPIFallo {
        LOG.info("OPERACION: opConsultarLiquidacionKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarLiquidacionKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarLiquidacionKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<LiquidacionTipo> liquidacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionTipoPagerData = KPILiquidacionFacade.consultarLiquidacionKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarLiquidacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarLiquidacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarLiquidacionKPIRespTipo resp = new OpConsultarLiquidacionKPIRespTipo();

        cr.setValCantidadPaginas(liquidacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getLiquidaciones().addAll(liquidacionTipoPagerData.getData());
        resp.setCantidadLiquidaciones(Integer.toString(liquidacionTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarLiquidacionKPI ::: FIN");

        return resp;
    }

}
