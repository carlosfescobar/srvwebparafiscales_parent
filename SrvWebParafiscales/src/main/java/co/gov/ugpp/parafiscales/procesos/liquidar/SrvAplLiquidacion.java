package co.gov.ugpp.parafiscales.procesos.liquidar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.MsjOpBuscarPorCriteriosLiquidacionFallo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.MsjOpBuscarPorIdLiquidacionFallo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpActualizarLiquidacionSolTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpBuscarPorCriteriosLiquidacionRespTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpBuscarPorCriteriosLiquidacionSolTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpBuscarPorIdLiquidacionRespTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpBuscarPorIdLiquidacionSolTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpCrearLiquidacionSolTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.MsjOpActualizarLiquidacionFallo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.MsjOpCrearLiquidacionFallo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpActualizarLiquidacionRespTipo;
import co.gov.ugpp.liquidacion.srvaplliquidacion.v1.OpCrearLiquidacionRespTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author gchavezm
 */
@WebService(serviceName = "SrvAplLiquidacion",
        portName = "portSrvAplLiquidacionSOAP",
        endpointInterface = "co.gov.ugpp.liquidacion.srvaplliquidacion.v1.PortSrvAplLiquidacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Liquidacion/SrvAplLiquidacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplLiquidacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplLiquidacion.class);
    
    
    @EJB
    private LiquidacionFacade liquidacionFacade;

    public OpCrearLiquidacionRespTipo opCrearLiquidacion(OpCrearLiquidacionSolTipo msjOpCrearLiquidacionSol) throws MsjOpCrearLiquidacionFallo {

        
        LOG.error("Op: opCrearLiquidacion ::: INIT");
        
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearLiquidacionSol.getContextoTransaccional();
        final LiquidacionTipo liquidacionTipo = msjOpCrearLiquidacionSol.getLiquidacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long liquidacionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionPK = liquidacionFacade.crearLiquidacion(liquidacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearLiquidacionRespTipo resp = new OpCrearLiquidacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdLiquidacion(liquidacionPK.toString());
        
        
        LOG.error("Op: opCrearLiquidacion ::: END");
        
        return resp;
    }

    public OpBuscarPorCriteriosLiquidacionRespTipo opBuscarPorCriteriosLiquidacion(OpBuscarPorCriteriosLiquidacionSolTipo msjOpBuscarPorCriteriosLiquidacionSol) throws MsjOpBuscarPorCriteriosLiquidacionFallo {
        
        
        LOG.error("Op: OpBuscarPorCriteriosLiquidacionRespTipo ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosLiquidacionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosLiquidacionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<LiquidacionTipo> liquidaicionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidaicionTipoPagerData = liquidacionFacade.buscarPorCriteriosLiquidacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosLiquidacionFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosLiquidacionFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);

        }
        final OpBuscarPorCriteriosLiquidacionRespTipo resp = new OpBuscarPorCriteriosLiquidacionRespTipo();
        cr.setValCantidadPaginas(liquidaicionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getLiquidaciones().addAll(liquidaicionTipoPagerData.getData());


        LOG.error("Op: OpBuscarPorCriteriosLiquidacionRespTipo ::: END");
        
        return resp;
    }

    public OpBuscarPorIdLiquidacionRespTipo opBuscarPorIdLiquidacion(OpBuscarPorIdLiquidacionSolTipo msjOpBuscarPorIdLiquidacionSol) throws MsjOpBuscarPorIdLiquidacionFallo {
        
        LOG.error("Op: opBuscarPorIdLiquidacion ::: INIT");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdLiquidacionSol.getContextoTransaccional();
        final List<String> idsLiquidacion = msjOpBuscarPorIdLiquidacionSol.getIdsLiquidacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<LiquidacionTipo> liquidacionTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionTipos = liquidacionFacade.buscarPorIdsLiquidacion(idsLiquidacion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdLiquidacionRespTipo resp = new OpBuscarPorIdLiquidacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getLiquidaciones().addAll(liquidacionTipos);

        LOG.error("Op: opBuscarPorIdLiquidacion ::: END");
        
        return resp;

    }

    public OpActualizarLiquidacionRespTipo opActualizarLiquidacion(OpActualizarLiquidacionSolTipo msjOpActualizarLiquidaicionSol) throws MsjOpActualizarLiquidacionFallo {
        
        
        LOG.error("Op: opActualizarLiquidacion ::: END");
        
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarLiquidaicionSol.getContextoTransaccional();
        final LiquidacionTipo liquidacionTipo = msjOpActualizarLiquidaicionSol.getLiquidacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            liquidacionFacade.actualizarLiquidacion(liquidacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
            //LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarLiquidacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarLiquidacionRespTipo resp = new OpActualizarLiquidacionRespTipo();
        resp.setContextoRespuesta(cr);

        
        LOG.error("Op: opActualizarLiquidacion ::: END");
        
        return resp;
    }
}


