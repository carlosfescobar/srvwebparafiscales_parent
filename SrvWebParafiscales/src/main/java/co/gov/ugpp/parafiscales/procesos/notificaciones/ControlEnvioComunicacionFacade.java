package co.gov.ugpp.parafiscales.procesos.notificaciones;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1.ControlEnvioComunicacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface ControlEnvioComunicacionFacade extends Serializable {

  Long crearControlEnvioComunicacion(ControlEnvioComunicacionTipo controlEnvioComunicacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarControlEnvioComunicacion(ControlEnvioComunicacionTipo controlEnvioComunicacionTipo,ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    List<ControlEnvioComunicacionTipo> buscarPorIdControlEnvioComunicacion(List<String> idControlEnvioComunicacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    PagerData<ControlEnvioComunicacionTipo> buscarPorCriteriosControlEnvioComunicacion(List<ParametroTipo> parametroTipos, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException;

}
