package co.gov.ugpp.parafiscales.procesos.notificaciones;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ControlEnvioComunicacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.NotificacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ControlEnvioComunicacion;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1.ControlEnvioComunicacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ControlEnvioComunicacionFacade que contiene las
 * operaciones del servicio SrvAplControlEnvioComunicacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ControlEnvioComunicacionFacadeImpl extends AbstractFacade implements ControlEnvioComunicacionFacade {

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private NotificacionDao notificacionDao;
    @EJB
    private ControlEnvioComunicacionDao controlEnvioComunicacionDao;

    /**
     * implementación de la operación crearControlEnvioComunicacion se encarga
     * de crear una nueva ControlEnvioComunicacion en la base de datos a partir
     * del dto ControlEnvioComunicacionTipo
     *
     * @param controlEnvioComunicacionTipo Objeto a partir del cual se va a
     * crear un ControlEnvioComunicacion en base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return sancion creada que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearControlEnvioComunicacion(ControlEnvioComunicacionTipo controlEnvioComunicacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlEnvioComunicacionTipo == null) {
            throw new AppException("Debe proporcionar un objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final ControlEnvioComunicacion controlEnvioComunicacion = new ControlEnvioComunicacion();
        controlEnvioComunicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateControlEnvioComunicacion(controlEnvioComunicacionTipo, controlEnvioComunicacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idControlEnvioComunicacion = controlEnvioComunicacionDao.create(controlEnvioComunicacion);

        return idControlEnvioComunicacion;
    }

    /**
     * implementación de la operación actualizarControlEnvioComunicacion se
     * encarga de actualizar un ControlEnvioComunicacion de acuerdo a el id
     * enviado
     *
     * @param controlEnvioComunicacionTipo objeto por medio del cual se va a
     * actualizar un ControlEnvioComunicacionontiene la información que se va
     * @param contextoTransaccionalTipo Contiene la información que se va
     * almacenar en la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public void actualizarControlEnvioComunicacion(ControlEnvioComunicacionTipo controlEnvioComunicacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlEnvioComunicacionTipo == null || StringUtils.isBlank(controlEnvioComunicacionTipo.getIdControlEnvioComunicacion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ControlEnvioComunicacion controlEnvioComunicacion = controlEnvioComunicacionDao.find(Long.valueOf(controlEnvioComunicacionTipo.getIdControlEnvioComunicacion()));

        if (controlEnvioComunicacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontó un controlEnvioComunicacion con el valor:" + controlEnvioComunicacionTipo.getIdControlEnvioComunicacion()));
            throw new AppException(errorTipoList);
        }
        controlEnvioComunicacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateControlEnvioComunicacion(controlEnvioComunicacionTipo, controlEnvioComunicacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        controlEnvioComunicacionDao.edit(controlEnvioComunicacion);

    }

    /**
     * implementación de la operación buscarPorIdControlEnvioComunicacion se
     * encarga de buscar un ControlEnvioComunicacion en la base de datos por
     * medio del id o listado de ids enviados
     *
     * @param idControlEnvioComunicacionTipos listado de ids para consultar el
     * ControlEnvioComunicacion
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de ControlEnvioComunicacionTipo encontrado que se retorna
     * junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public List<ControlEnvioComunicacionTipo> buscarPorIdControlEnvioComunicacion(List<String> idControlEnvioComunicacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idControlEnvioComunicacionTipos == null || idControlEnvioComunicacionTipos.isEmpty()) {
            throw new AppException("Debe proporcionar los ID del objeto a buscar");
        }

        List<Long> ids = mapper.map(idControlEnvioComunicacionTipos, String.class, Long.class);
        List<ControlEnvioComunicacion> controlEnvioComunicacionlist = controlEnvioComunicacionDao.findByIdList(ids);
        List<ControlEnvioComunicacionTipo> controlEnvioComunicacionTipos = mapper.map(controlEnvioComunicacionlist, ControlEnvioComunicacion.class, ControlEnvioComunicacionTipo.class);

        return controlEnvioComunicacionTipos;

    }

    /**
     * implementación de la operación buscarPorCriteriosControlEnvioComunicacion
     * se encarga de buscar un ControlEnvioComunicacion en la base de datos por
     * medio de los parametros enviados y ordena la consulta de acuerdo a los
     * criterios establecidos
     *
     * @param parametroTipos lista de parametros por medio de los cuales se
     * busca un ControlEnvioComunicacion en la base de datos
     * @param criterioOrdenamientoTipos tributos por los cuales se va a ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de sacion encontrado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public PagerData<ControlEnvioComunicacionTipo> buscarPorCriteriosControlEnvioComunicacion(List<ParametroTipo> parametroTipos, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ControlEnvioComunicacion> pagerDataEntity
                = controlEnvioComunicacionDao.findPorCriterios(parametroTipos, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<ControlEnvioComunicacionTipo> controlEnvioComunicacionTipos = mapper.map(pagerDataEntity.getData(), ControlEnvioComunicacion.class, ControlEnvioComunicacionTipo.class);

        return new PagerData(controlEnvioComunicacionTipos, pagerDataEntity.getNumPages());
    }

    public void populateControlEnvioComunicacion(ControlEnvioComunicacionTipo controlEnvioComunicacionTipo, ControlEnvioComunicacion controlEnvioComunicacion,
            ContextoTransaccionalTipo contextoTransaccionalTipo, List<ErrorTipo> errorTipoList) throws AppException {

        if (controlEnvioComunicacionTipo.getNotificacion() != null
                && StringUtils.isNotBlank(controlEnvioComunicacionTipo.getNotificacion().getIdNotificacion())) {
            Notificacion notificacion = notificacionDao.find(Long.valueOf(controlEnvioComunicacionTipo.getNotificacion().getIdNotificacion()));
            if (notificacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una  Notificación con el ID: " + controlEnvioComunicacionTipo.getNotificacion().getIdNotificacion()));
            } else {
                controlEnvioComunicacion.setNotificacion(notificacion);
            }
        }

        if (StringUtils.isNotBlank(controlEnvioComunicacionTipo.getCodEstado())) {
            ValorDominio codEstado = valorDominioDao.find(controlEnvioComunicacionTipo.getCodEstado());
            if (codEstado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstado proporcionado no existe con el ID:: " + controlEnvioComunicacionTipo.getCodEstado()));
            } else {
                controlEnvioComunicacion.setCodEstado(codEstado);
            }
        }

        if (StringUtils.isNotBlank(controlEnvioComunicacionTipo.getDescObservacionesRechazo())) {
            controlEnvioComunicacion.setDescObservacionesRechazo(controlEnvioComunicacionTipo.getDescObservacionesRechazo());
        }
        if (StringUtils.isNotBlank(controlEnvioComunicacionTipo.getDescObservacionesCorreccion())) {
            controlEnvioComunicacion.setDescObservacionesCorreccion(controlEnvioComunicacionTipo.getDescObservacionesCorreccion());
        }

        if (controlEnvioComunicacionTipo.getFecRechazo() != null) {
            controlEnvioComunicacion.setFecRechazo(controlEnvioComunicacionTipo.getFecRechazo());
        }
        if (controlEnvioComunicacionTipo.getFecCorreccion() != null) {
            controlEnvioComunicacion.setFecCorreccion(controlEnvioComunicacionTipo.getFecCorreccion());
        }

        if (StringUtils.isNotBlank(controlEnvioComunicacionTipo.getIdUsuarioRechaza())) {
            controlEnvioComunicacion.setIdUsuarioRechaza(controlEnvioComunicacionTipo.getIdUsuarioRechaza());
        }
        if(StringUtils.isNotBlank(controlEnvioComunicacionTipo.getIdUsuarioCorrige())){
            controlEnvioComunicacion.setIdUsuarioCorrige(controlEnvioComunicacionTipo.getIdUsuarioCorrige());
        }
    }
}
