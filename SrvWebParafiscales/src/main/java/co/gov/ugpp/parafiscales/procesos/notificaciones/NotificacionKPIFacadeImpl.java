package co.gov.ugpp.parafiscales.procesos.notificaciones;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.NotificacionDao;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz SolicitudInformacionFacade que contiene las
 * operaciones del servicio SrvAplKPINotificacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class NotificacionKPIFacadeImpl extends AbstractFacade implements NotificacionKPIFacade {

    @EJB
    private NotificacionDao notificacionDao;

    /**
     * implementación de la operación consultaNotificacionKPI esta se encarga de
     * buscar notificaciones por medio de los parametros enviado y ordenar la
     * consulta de acuerdo a unos criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar las notificaciones
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de solicitudes encontradas que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<NotificacionTipo> consultaNotificacionKPI(
            final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Notificacion> notificacionesPagerData = notificacionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<NotificacionTipo> notificacionTipoList = new ArrayList<NotificacionTipo>();

        for (final Notificacion notificacion : notificacionesPagerData.getData()) {
            notificacion.setUseOnlySpecifiedFields(true);
            final NotificacionTipo notificacionTipo = mapper.map(notificacion, NotificacionTipo.class);
            notificacionTipoList.add(notificacionTipo);
        }
        return new PagerData(notificacionTipoList, notificacionesPagerData.getNumPages());
    }

    /**
     * Método que busca notificaciones por estado
     * @param estadosNotificacion los estados de la notificacion
     * @param contextoTransaccionalTipo  Contiene la información para almacenar
     * la auditoria
     * @return el listado de notificaciones encontradas
     * @throws AppException  Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public List<NotificacionTipo> buscarPorEstadosNotificacion(List<String> estadosNotificacion, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (estadosNotificacion == null
                || estadosNotificacion.isEmpty()) {
            throw new AppException("Debe enviar el listado de estados para realizar la consulta");
        }

        final List<NotificacionTipo> notificacionTipoList = new ArrayList<NotificacionTipo>();

        List<Notificacion> notificaciones = notificacionDao.findByEstadosNotificacion(estadosNotificacion);

        for (Notificacion notificacion : notificaciones) {
            notificacion.setUseOnlySpecifiedFields(true);
            final NotificacionTipo notificacionTipo = mapper.map(notificacion, NotificacionTipo.class);
            notificacionTipoList.add(notificacionTipo);
        }

        return notificacionTipoList;

    }
}
