package co.gov.ugpp.parafiscales.procesos.notificaciones;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.MsjOpActualizarControlEnvioComunicacionFallo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.MsjOpBuscarPorCriteriosControlEnvioComunicacionFallo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.MsjOpBuscarPorIdControlEnvioComunicacionFallo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.MsjOpCrearControlEnvioComunicacionFallo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpActualizarControlEnvioComunicacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpActualizarControlEnvioComunicacionSolTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpBuscarPorCriteriosControlEnvioComunicacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpBuscarPorCriteriosControlEnvioComunicacionSolTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpBuscarPorIdControlEnvioComunicacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpBuscarPorIdControlEnvioComunicacionSolTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpCrearControlEnvioComunicacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.OpCrearControlEnvioComunicacionSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1.ControlEnvioComunicacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplControlEnvioComunicacion",
        portName = "portSrvAplControlEnvioComunicacionSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplcontrolenviocomunicacion.v1.PortSrvAplControlEnvioComunicacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplControlEnvioComunicacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplControlEnvioComunicacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplControlEnvioComunicacion.class);
   @EJB
    private ControlEnvioComunicacionFacade controlEnvioComunicacionFacade;

    public OpCrearControlEnvioComunicacionRespTipo opCrearControlEnvioComunicacion(OpCrearControlEnvioComunicacionSolTipo msjOpCrearControlEnvioComunicacionSol) throws MsjOpCrearControlEnvioComunicacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearControlEnvioComunicacionSol.getContextoTransaccional();
        final ControlEnvioComunicacionTipo controlEnvioComunicacionTipo = msjOpCrearControlEnvioComunicacionSol.getControlEnvioComunicacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long controlEnvioComunicacionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlEnvioComunicacionPK = controlEnvioComunicacionFacade.crearControlEnvioComunicacion(controlEnvioComunicacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearControlEnvioComunicacionRespTipo resp = new OpCrearControlEnvioComunicacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdControlEnvioComunicacion(controlEnvioComunicacionPK.toString());

        return resp;
    }

    public OpActualizarControlEnvioComunicacionRespTipo opActualizarControlEnvioComunicacion(OpActualizarControlEnvioComunicacionSolTipo msjOpActualizarControlEnvioComunicacionSol) throws MsjOpActualizarControlEnvioComunicacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarControlEnvioComunicacionSol.getContextoTransaccional();
        final ControlEnvioComunicacionTipo controlEnvioComunicacionTipo = msjOpActualizarControlEnvioComunicacionSol.getControlEnvioComunicacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlEnvioComunicacionFacade.actualizarControlEnvioComunicacion(controlEnvioComunicacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarControlEnvioComunicacionRespTipo resp = new OpActualizarControlEnvioComunicacionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdControlEnvioComunicacionRespTipo opBuscarPorIdControlEnvioComunicacion(OpBuscarPorIdControlEnvioComunicacionSolTipo msjOpBuscarPorIdControlEnvioComunicacionSol) throws MsjOpBuscarPorIdControlEnvioComunicacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdControlEnvioComunicacionSol.getContextoTransaccional();
        final List<String> IdControlEnvioComunicacionList = msjOpBuscarPorIdControlEnvioComunicacionSol.getIdControlEnvioComunicacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<ControlEnvioComunicacionTipo> controlEnvioComunicacionTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlEnvioComunicacionTipoList = controlEnvioComunicacionFacade.buscarPorIdControlEnvioComunicacion(IdControlEnvioComunicacionList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdControlEnvioComunicacionRespTipo resp = new OpBuscarPorIdControlEnvioComunicacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getControlEnvioComunicacion().addAll(controlEnvioComunicacionTipoList);

        return resp;
    }

    public OpBuscarPorCriteriosControlEnvioComunicacionRespTipo opBuscarPorCriteriosControlEnvioComunicacion(OpBuscarPorCriteriosControlEnvioComunicacionSolTipo msjOpBuscarPorCriteriosControlEnvioComunicacionSol) throws MsjOpBuscarPorCriteriosControlEnvioComunicacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosControlEnvioComunicacionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosControlEnvioComunicacionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<ControlEnvioComunicacionTipo> controlEnvioComunicacionTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            controlEnvioComunicacionTiposPagerData = controlEnvioComunicacionFacade.buscarPorCriteriosControlEnvioComunicacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlEnvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosControlEnvioComunicacionRespTipo resp = new OpBuscarPorCriteriosControlEnvioComunicacionRespTipo();
        cr.setValCantidadPaginas(controlEnvioComunicacionTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getControlEnvioComunicacion().addAll(controlEnvioComunicacionTiposPagerData.getData());

        return resp;
    }

}
