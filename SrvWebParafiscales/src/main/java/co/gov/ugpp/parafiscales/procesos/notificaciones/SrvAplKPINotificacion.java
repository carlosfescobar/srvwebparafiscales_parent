package co.gov.ugpp.parafiscales.procesos.notificaciones;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.MsjOpConsultarNotificacionKPIFallo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.MsjOpConsultarNotificacionKPIPorEstadosFallo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.OpConsultarNotificacionKPIPorEstadosRespTipo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.OpConsultarNotificacionKPIPorEstadosSolTipo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.OpConsultarNotificacionKPIRespTipo;
import co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.OpConsultarNotificacionKPISolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import com.jcabi.immutable.Array;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplKPINotificacion",
        portName = "portSrvAplKPINotificacionSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplkpinotificacion.v1.PortSrvAplKPINotificacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplKPINotificacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPINotificacion extends AbstractSrvApl {

    @EJB
    private NotificacionKPIFacade notificacionKPIFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPINotificacion.class);

    public OpConsultarNotificacionKPIRespTipo opConsultarNotificacionKPI(OpConsultarNotificacionKPISolTipo msjOpConsultarNotificacionKPISol) throws MsjOpConsultarNotificacionKPIFallo {

        LOG.info("OPERACION: opConsultarNotificacionKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarNotificacionKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarNotificacionKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<NotificacionTipo> notificacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            notificacionTipoPagerData = notificacionKPIFacade.consultaNotificacionKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarNotificacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarNotificacionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarNotificacionKPIRespTipo resp = new OpConsultarNotificacionKPIRespTipo();

        cr.setValCantidadPaginas(notificacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getNotificaciones().addAll(notificacionTipoPagerData.getData());
        resp.setCantidadNotificaciones(Integer.toString(notificacionTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarNotificacionKPI ::: FIN");

        return resp;
    }

    public OpConsultarNotificacionKPIPorEstadosRespTipo opConsultarNotificacionKPIPorEstados(OpConsultarNotificacionKPIPorEstadosSolTipo msjOpConsultarNotificacionKPIPorEstadosSol) throws MsjOpConsultarNotificacionKPIPorEstadosFallo {

        LOG.info("OPERACION: opConsultarNotificacionKPIPorEstados ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarNotificacionKPIPorEstadosSol.getContextoTransaccional();
        final List<String> estadosNotificacion = msjOpConsultarNotificacionKPIPorEstadosSol.getEstados();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        List<NotificacionTipo> notificaciones = new Array<NotificacionTipo>();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            notificaciones = notificacionKPIFacade.buscarPorEstadosNotificacion(estadosNotificacion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarNotificacionKPIPorEstadosFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarNotificacionKPIPorEstadosFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarNotificacionKPIPorEstadosRespTipo resp = new OpConsultarNotificacionKPIPorEstadosRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getNotificaciones().addAll(notificaciones);
        resp.setCantidadNotificaciones(Integer.toString(notificaciones.size()));

        LOG.info("OPERACION: opConsultarNotificacionKPIPorEstados ::: FIN");

        return resp;
    }

}
