package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface ENotificacionFacade {

    Notificacion persistirNotificacion(NotificacionTipo notificacionTipo,
            List<ErrorTipo> errorTipoList) throws AppException;
}
