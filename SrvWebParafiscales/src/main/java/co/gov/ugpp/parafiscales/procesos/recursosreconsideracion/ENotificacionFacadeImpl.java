package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.NotificacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ENotificacionFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
public class ENotificacionFacadeImpl extends AbstractFacade implements ENotificacionFacade {

    @EJB
    private NotificacionDao notificacionDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * Método que se encarga de crear una nueva Notificacion
     *
     * @param notificacionTipo objeto a partir del cual se va a crear una nueva notificacion
     * @param errorTipoList Listado que almacena errores de negocio
     * @return Nueva notificaion creada que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public Notificacion persistirNotificacion(final NotificacionTipo notificacionTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (notificacionTipo == null
                || StringUtils.isBlank(notificacionTipo.getIdNotificacion())) {
            throw new AppException("Debe proporcionar el ID del objeto Notificacion");
        }

        Notificacion notificacion = notificacionDao.find(Long.valueOf(notificacionTipo.getIdNotificacion()));

        if (notificacion != null) {
            return notificacion;
        }

        notificacion = mapper.map(notificacionTipo, Notificacion.class);

        final IdentificacionTipo identificacionTipo = notificacionTipo.getPersonaNaturalNotificada().getIdPersona();
        final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

        if (identificacion != null
                && StringUtils.isNotBlank(identificacion.getValNumeroIdentificacion())
                && StringUtils.isNotBlank(identificacion.getCodTipoIdentificacion())) {
            Persona persona = personaDao.findByIdentificacion(identificacion);
            if (persona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una Persona con la identificacion:" + identificacion));
                return null;
            }
            notificacion.setPersona(persona);
        }

        if (StringUtils.isNotBlank(notificacionTipo.getCodCalidadNotificado())) {
            ValorDominio valorDominio = valorDominioDao.find(notificacionTipo.getCodCalidadNotificado());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un CodCalidadNotificado con el valor:" + notificacionTipo.getCodCalidadNotificado()));
                return null;
            }
            notificacion.setCodCalidadNotificado(valorDominio);
        }

        if (StringUtils.isNotBlank(notificacionTipo.getCodTipoNotificacion())) {
            ValorDominio valorDominio = valorDominioDao.find(notificacionTipo.getCodTipoNotificacion());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codTipoNotificacion con el valor:" + notificacionTipo.getCodTipoNotificacion()));
                return null;
            }
            notificacion.setCodTipoNotificacion(valorDominio);
        }

        if (notificacionTipo.getActoAdministrativo() != null
                && StringUtils.isNotBlank(notificacionTipo.getActoAdministrativo().getIdActoAdministrativo())) {
            ActoAdministrativo actoAdministrativo
                    = actoAdministrativoDao.find(notificacionTipo.getActoAdministrativo().getIdActoAdministrativo());
            if (actoAdministrativo == null) {
                actoAdministrativo = mapper.map(notificacionTipo.getActoAdministrativo(), ActoAdministrativo.class);
            }
            notificacion.setActoAdministrativo(actoAdministrativo);
        }

        return notificacion;
    }
}
