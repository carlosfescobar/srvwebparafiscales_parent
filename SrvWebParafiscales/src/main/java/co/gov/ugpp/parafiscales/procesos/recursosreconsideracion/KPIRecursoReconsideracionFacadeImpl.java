package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoReconsideracionDao;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

/**
 * implementación de la interfaz KPIRecursoReconsideracionFacade que contiene la
 * operaciones del servicio SrvAplKPIRecursoReconsideracion
 *
 * @author everis
 * @version 1.0
 */
@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class KPIRecursoReconsideracionFacadeImpl extends AbstractFacade implements KPIRecursoReconsideracionFacade {

    @EJB
    private RecursoReconsideracionDao recursoReconsideracionDao;

    @Override
    public PagerData<RecursoReconsideracionTipo> consultarRecursoReconsideracionKPI(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<RecursoReconsideracion> recursosReconsideracionPagerData = recursoReconsideracionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<RecursoReconsideracionTipo> recursoReconsideracionTipoList = new ArrayList<RecursoReconsideracionTipo>();

        for (final RecursoReconsideracion recursoReconsideracion : recursosReconsideracionPagerData.getData()) {
            recursoReconsideracion.setUseOnlySpecifiedFields(true);
            final RecursoReconsideracionTipo recursoReconsideracionTipo = mapper.map(recursoReconsideracion, RecursoReconsideracionTipo.class);
            recursoReconsideracionTipoList.add(recursoReconsideracionTipo);
        }
        return new PagerData(recursoReconsideracionTipoList, recursosReconsideracionPagerData.getNumPages());
    }

}
