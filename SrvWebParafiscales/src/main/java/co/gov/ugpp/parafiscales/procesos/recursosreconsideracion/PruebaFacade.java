package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface PruebaFacade extends Serializable {

    Long crearPrueba(PruebaTipo pruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<PruebaTipo> buscarPorIdPrueba(List<String> idPruebaTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarPrueba(PruebaTipo pruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}