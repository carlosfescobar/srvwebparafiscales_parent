package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.PruebaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Prueba;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz PruebaFacade que contiene la operaciones del
 * servicio SrvAplPrueba
 *
 * @author everis
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class PruebaFacadeImpl extends AbstractFacade implements PruebaFacade {

    @EJB
    private PruebaDao pruebaDao;
    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;
    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;
    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @Override
    public Long crearPrueba(PruebaTipo pruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (pruebaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final Prueba prueba = new Prueba();

        //Validacion de campos obligatorios
        Validator.checkPrueba(pruebaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        prueba.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.PopulatePrueba(pruebaTipo, prueba, contextoTransaccionalTipo, errorTipoList);
        this.checkBusinesSPrueba(pruebaTipo, prueba, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idPrueba = pruebaDao.create(prueba);
        return idPrueba;
    }

    @Override
    public List<PruebaTipo> buscarPorIdPrueba(List<String> idPruebaTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idPruebaTipos == null || idPruebaTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idPruebaTipos, String.class, Long.class);

        final List<Prueba> pruebaList = pruebaDao.findByIdList(ids);

        final List<PruebaTipo> pruebaTipoList
                = mapper.map(pruebaList, Prueba.class, PruebaTipo.class);

        return pruebaTipoList;
    }

    @Override
    public void actualizarPrueba(PruebaTipo pruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (pruebaTipo == null || StringUtils.isBlank(pruebaTipo.getIdPrueba())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Prueba prueba = pruebaDao.find(Long.valueOf(pruebaTipo.getIdPrueba()));

        if (prueba == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un IdPrueba con el valor:" + pruebaTipo.getIdPrueba()));
            throw new AppException(errorTipoList);
        }
        this.checkBusinesSPrueba(pruebaTipo, prueba, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        prueba.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.PopulatePrueba(pruebaTipo, prueba, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        pruebaDao.edit(prueba);
    }

    private void PopulatePrueba(final PruebaTipo pruebaTipo, final Prueba prueba, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(pruebaTipo.getCodTipoPrueba())) {
            final ValorDominio codPrueba = valorDominioDao.find(pruebaTipo.getCodTipoPrueba());
            if (codPrueba == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codPrueba con el ID: " + pruebaTipo.getCodTipoPrueba()));
            } else {
                prueba.setCodTipoPrueba(codPrueba);
            }
        }

        if (pruebaTipo.getFecSolicitudPrueba() != null) {
            prueba.setFechaSolicitudPrueba(pruebaTipo.getFecSolicitudPrueba());

        }
        if (StringUtils.isNotBlank(pruebaTipo.getValTiempoEsperaPruebaExterna())) {
            prueba.setValTiempoEsperaPruebaExterna(pruebaTipo.getValTiempoEsperaPruebaExterna());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getDescInformacionSolicitada())) {
            prueba.setDesInformacionSolicitada(pruebaTipo.getDescInformacionSolicitada());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getCodTipoPruebaExterna())) {
            final ValorDominio codTipoPruebaExterna = valorDominioDao.find(pruebaTipo.getCodTipoPruebaExterna());
            if (codTipoPruebaExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoPruebaExterna con el ID: " + pruebaTipo.getCodTipoPruebaExterna()));
            } else {
                prueba.setCodTipoPruebaExterna(codTipoPruebaExterna);
            }
        }
        if (StringUtils.isNotBlank(pruebaTipo.getEsPruebaSolicitada())) {
            prueba.setEsPruebaSolicitud(Boolean.valueOf(pruebaTipo.getEsPruebaSolicitada()));
        }

        if (StringUtils.isNotBlank(pruebaTipo.getEsSolicitadaPruebaInterna())) {
            prueba.setEsSolicitudPruebaInterna(Boolean.valueOf(pruebaTipo.getEsSolicitadaPruebaInterna()));
        }

        if (StringUtils.isNotBlank(pruebaTipo.getEsResueltaSolicitudInterna())) {
            prueba.setEsResueltaSolicitudInterna(Boolean.valueOf(pruebaTipo.getEsResueltaSolicitudInterna()));
        }

        if (StringUtils.isNotBlank(pruebaTipo.getEsContinuadoProcesoInterno())) {
            prueba.setEsContinuadoProcesoInterno(Boolean.valueOf(pruebaTipo.getEsContinuadoProcesoInterno()));
        }

        if (pruebaTipo.getInspeccionTributariaParcial() != null
                && StringUtils.isNotBlank(pruebaTipo.getInspeccionTributariaParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(pruebaTipo.getInspeccionTributariaParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una inspeccionTributaria con el ID: " + pruebaTipo.getInspeccionTributariaParcial().getIdArchivo()));
            } else {
                prueba.setInspeccionTributariaParcial(aprobacionDocumento);
            }
        }

        if (pruebaTipo.getSolicitudPruebaExternaParcial() != null
                && StringUtils.isNotBlank(pruebaTipo.getSolicitudPruebaExternaParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(pruebaTipo.getSolicitudPruebaExternaParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una solcitudPruebaExternaParcial con el ID: " + pruebaTipo.getSolicitudPruebaExternaParcial().getIdArchivo()));
            } else {
                prueba.setSolicitudPruebaExternaParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(pruebaTipo.getIdActoSolicitudPruebaExterna())) {
            final ActoAdministrativo idActoSolicitudPruebaExterna = actoAdministrativoDao.find(String.valueOf(pruebaTipo.getIdActoSolicitudPruebaExterna()));
            if (idActoSolicitudPruebaExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoSolicitudPruebaExterna con el ID: " + pruebaTipo.getIdActoSolicitudPruebaExterna()));
            } else {
                prueba.setIdActoSolicitudPruebaExterna(idActoSolicitudPruebaExterna);
            }
        }

        if (StringUtils.isNotBlank(pruebaTipo.getIdActoInspeccionTributaria())) {
            final ActoAdministrativo idActoInspeccionTributaria = actoAdministrativoDao.find(String.valueOf(pruebaTipo.getIdActoInspeccionTributaria()));
            if (idActoInspeccionTributaria == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoInspeccionTributaria con el ID: " + pruebaTipo.getIdActoInspeccionTributaria()));
            } else {
                prueba.setIdActoInspeccionTributaria(idActoInspeccionTributaria);
            }
        }

        if (StringUtils.isNotBlank(pruebaTipo.getDescObservacionesSolicitudInterna())) {
            prueba.setDescObservacionesSolicitudInterna(pruebaTipo.getDescObservacionesSolicitudInterna());
        }

        if (pruebaTipo.getFecInspeccionTributaria() != null) {
            prueba.setFecInspeccionTributaria(pruebaTipo.getFecInspeccionTributaria());
        }
        if (StringUtils.isNotBlank(pruebaTipo.getValNombreInspector())) {
            prueba.setValNombreInspector(pruebaTipo.getValNombreInspector());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getValNombrePersonaContactadaInspeccion())) {
            prueba.setValNombrePersonaContactadaInspeccion(pruebaTipo.getValNombrePersonaContactadaInspeccion());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getValCargoPersonaContactadaInspeccion())) {
            prueba.setValCargoPersonaContactadaInspeccion(pruebaTipo.getValCargoPersonaContactadaInspeccion());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getIdDocumentoInformeInspeccion())) {
            prueba.setIdDocumentoInformeInspeccion(pruebaTipo.getIdDocumentoInformeInspeccion());
        }

        if (StringUtils.isNotBlank(pruebaTipo.getDescObservacionesInspeccion())) {
            prueba.setDescObservacionesInspeccion(pruebaTipo.getDescObservacionesInspeccion());
        }
    }

    public void checkBusinesSPrueba(PruebaTipo pruebaTipo, Prueba prueba, final List<ErrorTipo> errorTipoList) throws AppException {
        if (prueba.getAportante() != null
                && pruebaTipo.getEntidadExterna() != null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se puede actualizar la prueba " + prueba.getId() + " ,esta prueba ya tiene asociado un Aportante"));

        }
        if (prueba.getEntidadExterna() != null && pruebaTipo.getAportante() != null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se puede actualizar la prueba " + prueba.getId() + " ,esta prueba ya tiene asociada una Entidad Externa"));

        }
        if (pruebaTipo.getAportante() != null) {
            if (pruebaTipo.getAportante().getPersonaJuridica() != null
                    && pruebaTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                    && (StringUtils.isNotBlank(pruebaTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(pruebaTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = pruebaTipo.getAportante().getPersonaJuridica().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un aportante con la identificación :" + identificacion));
                } else {
                    prueba.setAportante(aportante);
                }
            } else if (pruebaTipo.getAportante().getPersonaNatural() != null
                    && pruebaTipo.getAportante().getPersonaNatural().getIdPersona() != null
                    && (StringUtils.isNotBlank(pruebaTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(pruebaTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = pruebaTipo.getAportante().getPersonaNatural().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un aportante con la identificación :" + identificacion));
                } else {
                    prueba.setAportante(aportante);
                }
            }
        }
        if (pruebaTipo.getEntidadExterna() != null
                && pruebaTipo.getEntidadExterna().getIdPersona() != null) {
            IdentificacionTipo identificacionTipo = pruebaTipo.getEntidadExterna().getIdPersona();
            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una EntidadExterna con la identificación:" + identificacion));
            } else {
                prueba.setEntidadExterna(entidadExterna);
            }
        }
    }

}
