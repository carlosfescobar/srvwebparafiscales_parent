package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;

import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface RecursoReconsideracionFacade extends Serializable {

    Long crearRecursoReconsideracion(RecursoReconsideracionTipo recursoReconsideracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<RecursoReconsideracionTipo> buscarPorIdRecursoReconsideracion(List<String> idPruebaTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarRecursoReconsideracion(RecursoReconsideracionTipo recursoReconsideracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
