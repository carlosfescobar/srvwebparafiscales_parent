package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ControlLiquidadorDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.PruebaDao;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoCausalFalloDao;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoDocAnexoReposicionDao;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoDocAnexoRequisitosDao;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoReconsideracionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Prueba;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoReposicion;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoRequisitos;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoCausalFallo;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconArchivoTemp;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz RecursoReconsideracionFacade que contiene la
 * operaciones del servicio SrvAplRecursoReconsideracion
 *
 * @author everis
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RecursoReconsideracionFacadeImpl extends AbstractFacade implements RecursoReconsideracionFacade {

    private static final Logger LOG = LoggerFactory.getLogger(RecursoReconsideracionFacadeImpl.class);

    @EJB
    private RecursoReconsideracionDao recursoReconsideracionDao;

    @EJB
    private PruebaDao pruebaDao;
    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private RecursoCausalFalloDao recursoCausalFalloDao;

    @EJB
    private RecursoDocAnexoReposicionDao recursoDocAnexoReposicionDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private RecursoDocAnexoRequisitosDao recursoDocAnexoRequisitosDao;

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private ControlLiquidadorDao controlLiquidadorDao;

    @EJB
    private ArchivoDao archivoDao;

    /**
     * implementación de la operación crearRecursoReconsideracionesta se encarga
     * de crear una reconsideracion de acuerdo al dto RecursoReconsideracionTipo
     *
     * @param recursoReconsideracionTipo Objeto a partir del cual se va a crear
     * una reconsideracion en la base de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Nueva reconsideracion creada que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearRecursoReconsideracion(RecursoReconsideracionTipo recursoReconsideracionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (recursoReconsideracionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        RecursoReconsideracion reconsideracion = new RecursoReconsideracion();
        reconsideracion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        Validator.checkRecursoReconsideracion(recursoReconsideracionTipo, errorTipoList);
        this.populateRecursoReconsideracion(recursoReconsideracionTipo, reconsideracion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return recursoReconsideracionDao.create(reconsideracion);
    }

    /**
     * implementación de la operación actualizarRecursoReconsideracion se
     * encarga de actualizar una reconsideracion de acuaerdo a un id enviado
     *
     * @param recursoReconsideracionTipo Objeto a partir del cual se Actualiza
     * una reconsideracion
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void actualizarRecursoReconsideracion(RecursoReconsideracionTipo recursoReconsideracionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(recursoReconsideracionTipo.getIdRecursoReconsideracion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        RecursoReconsideracion reconsideracion = recursoReconsideracionDao.find(Long.valueOf(recursoReconsideracionTipo.getIdRecursoReconsideracion()));

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (reconsideracion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idRecursoReconsideracion con el valor:" + recursoReconsideracionTipo.getIdRecursoReconsideracion()));
            throw new AppException(errorTipoList);
        }
        reconsideracion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        populateRecursoReconsideracion(recursoReconsideracionTipo, reconsideracion, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        recursoReconsideracionDao.edit(reconsideracion);
    }

    @Override
    public List<RecursoReconsideracionTipo> buscarPorIdRecursoReconsideracion(List<String> idPruebaTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idPruebaTipos == null || idPruebaTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idPruebaTipos, String.class, Long.class);

        final List<RecursoReconsideracion> recursoReconsideracionsList = recursoReconsideracionDao.findByIdList(ids);

        final List<RecursoReconsideracionTipo> recursoReconsideracionTipoList
                = mapper.map(recursoReconsideracionsList, RecursoReconsideracion.class, RecursoReconsideracionTipo.class);

        return recursoReconsideracionTipoList;
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad RecursoReconsideracion
     *
     * @param recursoReconsideracionTipo Objeto origen
     * @param recursoReconsideracion Objeto destino
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateRecursoReconsideracion(RecursoReconsideracionTipo recursoReconsideracionTipo,
            final RecursoReconsideracion recursoReconsideracion, final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getCodProcesoEjecutador())) {
            ValorDominio codProcesoEjecutador = valorDominioDao.find(recursoReconsideracionTipo.getCodProcesoEjecutador());
            if (codProcesoEjecutador == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un CodTipoAdmisibilidad con el valor:" + recursoReconsideracionTipo.getCodProcesoEjecutador()));

            }
            recursoReconsideracion.setCodProcesoEjecutador(codProcesoEjecutador);
        }
        if (recursoReconsideracionTipo.getExpediente() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getExpediente().getIdNumExpediente())) {
            final Expediente expediente = expedienteDao.find(String.valueOf(recursoReconsideracionTipo.getExpediente().getIdNumExpediente()));
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un expediente con el Id: " + recursoReconsideracionTipo.getExpediente().getIdNumExpediente()));
            } else {
                recursoReconsideracion.setExpediente(expediente);
            }
        }

        if (recursoReconsideracionTipo.getAportante() != null) {
            if (recursoReconsideracionTipo.getAportante().getPersonaJuridica() != null
                    && recursoReconsideracionTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                    && (StringUtils.isNotBlank(recursoReconsideracionTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(recursoReconsideracionTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = recursoReconsideracionTipo.getAportante().getPersonaJuridica().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un aportante con la identificación :" + identificacion));
                } else {
                    recursoReconsideracion.setAportante(aportante);
                }
            } else if (recursoReconsideracionTipo.getAportante().getPersonaNatural() != null
                    && recursoReconsideracionTipo.getAportante().getPersonaNatural().getIdPersona() != null
                    && (StringUtils.isNotBlank(recursoReconsideracionTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(recursoReconsideracionTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = recursoReconsideracionTipo.getAportante().getPersonaNatural().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                if (aportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un aportante con la identificación :" + identificacion));
                } else {
                    recursoReconsideracion.setAportante(aportante);
                }
            }
        }

        if (recursoReconsideracionTipo.getFecEntradaRecursoReconsideracion() != null) {
            recursoReconsideracion.setFecEntradaRecursoReconsideracion(recursoReconsideracionTipo.getFecEntradaRecursoReconsideracion());
        }

        if (recursoReconsideracionTipo.getFechaPresentacionRecursoReconsideracion() != null) {
            recursoReconsideracion.setFecPresentacionRecursoReconsideracion(recursoReconsideracionTipo.getFechaPresentacionRecursoReconsideracion());
        }

        if (recursoReconsideracionTipo.getFechaPresentacionEnDebidaForma() != null) {
            recursoReconsideracion.setFecPresentacionEnDebidaForma(recursoReconsideracionTipo.getFechaPresentacionEnDebidaForma());
        }
        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdRadicadoEntradaRecursoReconsideracion())) {
            recursoReconsideracion.setIdRadicadoEntradaRecursoReconsideracion(recursoReconsideracionTipo.getIdRadicadoEntradaRecursoReconsideracion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoRequerimientoDeclararCorregir())) {
            final ActoAdministrativo idActoRequerimientoDeclararCorrgeir = actoAdministrativoDao.find(String.valueOf(recursoReconsideracionTipo.getIdActoRequerimientoDeclararCorregir()));
            if (idActoRequerimientoDeclararCorrgeir == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoRequerimientoDeclararCorrgeir con el id  :" + recursoReconsideracionTipo.getIdActoRequerimientoDeclararCorregir()));
            } else {
                recursoReconsideracion.setIdActoRequerimientoDeclararCorregir(idActoRequerimientoDeclararCorrgeir);
            }

        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsDigitalizadoExpediente())) {
            recursoReconsideracion.setEsDigitalizadoExpediente(Boolean.valueOf(recursoReconsideracionTipo.getEsDigitalizadoExpediente()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesDigitalizacion())) {
            recursoReconsideracion.setDescObservacionesDigitalizacion(recursoReconsideracionTipo.getDescObservacionesDigitalizacion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsAdmisibleRecurso())) {
            recursoReconsideracion.setEsAdmisibleRecurso(Boolean.valueOf(recursoReconsideracionTipo.getEsAdmisibleRecurso()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsSubsanable())) {
            recursoReconsideracion.setEsSubsanable(Boolean.valueOf(recursoReconsideracionTipo.getEsSubsanable()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesRequisitos())) {
            recursoReconsideracion.setDescObservacionesRequisitos(recursoReconsideracionTipo.getDescObservacionesRequisitos());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesRequisitos())) {
            recursoReconsideracion.setDescObservacionesRequisitos(recursoReconsideracionTipo.getDescObservacionesRequisitos());
        }

        if (recursoReconsideracionTipo.getAutoAdmisorioParcial() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getAutoAdmisorioParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(recursoReconsideracionTipo.getAutoAdmisorioParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una autoAdmisorioParcial con el ID: " + recursoReconsideracionTipo.getAutoAdmisorioParcial().getIdArchivo()));
            } else {
                recursoReconsideracion.setAutoAdmisorioParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoAutoAdmisorio())) {
            ActoAdministrativo idActoAutoAdmisorio = actoAdministrativoDao.find(String.valueOf(recursoReconsideracionTipo.getIdActoAutoAdmisorio()));
            if (idActoAutoAdmisorio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una idActoAutoAdmisorio con el ID: " + recursoReconsideracionTipo.getIdActoAutoAdmisorio()));
            } else {
                recursoReconsideracion.setIdActoAutoAdmisorio(idActoAutoAdmisorio);
            }
        }

        if (recursoReconsideracionTipo.getAutoInadmisorioParcial() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getAutoInadmisorioParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(recursoReconsideracionTipo.getAutoInadmisorioParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una autoAdmisorioParcial con el ID: " + recursoReconsideracionTipo.getAutoInadmisorioParcial().getIdArchivo()));
            } else {
                recursoReconsideracion.setAutoInadmisorioParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoAutoInadmisorio())) {
            final ActoAdministrativo idActoAutoInadmisorio = actoAdministrativoDao.find(recursoReconsideracionTipo.getIdActoAutoInadmisorio());
            if (idActoAutoInadmisorio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una idActoAutoInadmisorio con el ID: " + recursoReconsideracionTipo.getIdActoAutoInadmisorio()));
            } else {
                recursoReconsideracion.setIdActoAutoInadmisorio(idActoAutoInadmisorio);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsPresentadoRecursoReposicion())) {
            recursoReconsideracion.setEsPresentadoRecursoReposicion(Boolean.valueOf(recursoReconsideracionTipo.getEsPresentadoRecursoReposicion()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsSubsanadaInadmisibilidad())) {
            recursoReconsideracion.setEsSubsanadaInadmisibilidad(Boolean.valueOf(recursoReconsideracionTipo.getEsSubsanadaInadmisibilidad()));
        }

        if (recursoReconsideracionTipo.getFecEntradaRecursoReposicion() != null) {
            recursoReconsideracion.setFecEntradaRecursoReposicion(recursoReconsideracionTipo.getFecEntradaRecursoReposicion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdRadicadoEntradaRecursoReposicion())) {
            recursoReconsideracion.setIdRadicadoEntradaRecursoReposicion(recursoReconsideracionTipo.getIdRadicadoEntradaRecursoReposicion());
        }
        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesRecursoReposicion())) {
            recursoReconsideracion.setDescObservacionesRecursoReposicion(recursoReconsideracionTipo.getDescObservacionesRecursoReposicion());
        }

        if (recursoReconsideracionTipo.getActoAprobacionInadmisionParcial() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getActoAprobacionInadmisionParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(recursoReconsideracionTipo.getActoAprobacionInadmisionParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un actoAprobacionInadmisionParcial con el ID: " + recursoReconsideracionTipo.getActoAprobacionInadmisionParcial().getIdArchivo()));
            } else {
                recursoReconsideracion.setActoAprobacionInadmisionParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoAprobacionInadmision())) {
            final ActoAdministrativo idActoAprobacionInadmision = actoAdministrativoDao.find(recursoReconsideracionTipo.getIdActoAprobacionInadmision());
            if (idActoAprobacionInadmision == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoAprobacionInadmision con el ID: " + recursoReconsideracionTipo.getIdActoAprobacionInadmision()));
            } else {
                recursoReconsideracion.setIdActoAprobacionInadmision(idActoAprobacionInadmision);
            }
        }

        if (recursoReconsideracionTipo.getActoReponeAdmiteParcial() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getActoReponeAdmiteParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(recursoReconsideracionTipo.getActoReponeAdmiteParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un actoReponeAdmiteParcial con el ID: " + recursoReconsideracionTipo.getActoReponeAdmiteParcial().getIdArchivo()));
            } else {
                recursoReconsideracion.setActoReponeAdmiteParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoReponeAdmite())) {
            final ActoAdministrativo idActoReponeAdmite = actoAdministrativoDao.find(String.valueOf(recursoReconsideracionTipo.getIdActoReponeAdmite()));
            if (idActoReponeAdmite == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoReponeAdmite con el ID: " + recursoReconsideracionTipo.getIdActoReponeAdmite()));
            } else {
                recursoReconsideracion.setIdActoReponeAdmite(idActoReponeAdmite);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsPruebasSuficientes())) {
            recursoReconsideracion.setEsPruebasSuficientes(Boolean.valueOf(recursoReconsideracionTipo.getEsPruebasSuficientes()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getEsNecesarioEjecutarLiquidador())) {
            recursoReconsideracion.setEsNecesarioEjecutarLiquidador(Boolean.valueOf(recursoReconsideracionTipo.getEsNecesarioEjecutarLiquidador()));
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getValNuevoImporteSancion())) {
            recursoReconsideracion.setValImporteSancion(recursoReconsideracionTipo.getValImporteSancion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getValNuevoImporteSancion())) {
            recursoReconsideracion.setValNuevoImporteSancion(recursoReconsideracionTipo.getValNuevoImporteSancion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesCalculoSancion())) {
            recursoReconsideracion.setDescObservacionesCalculoSancion(recursoReconsideracionTipo.getDescObservacionesCalculoSancion());
        }
        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdLiquidacion())) {
            recursoReconsideracion.setIdLiquidacion(recursoReconsideracionTipo.getIdLiquidacion());
        }
        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdSancion())) {
            recursoReconsideracion.setIdSancion(recursoReconsideracionTipo.getIdSancion());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getCodFalloFinal())) {
            ValorDominio codFalloFinal = valorDominioDao.find(recursoReconsideracionTipo.getCodFalloFinal());
            if (codFalloFinal == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codFalloFinal con el valor:" + recursoReconsideracionTipo.getCodFalloFinal()));

            }
            recursoReconsideracion.setCodFalloFinal(codFalloFinal);
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getValFinalActoAdministrativo())) {
            recursoReconsideracion.setValFinalActoAdministrativo(recursoReconsideracionTipo.getValFinalActoAdministrativo());
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getDescObservacionesFallo())) {
            recursoReconsideracion.setDescObservacionesFallo(recursoReconsideracionTipo.getDescObservacionesFallo());
        }

        if (recursoReconsideracionTipo.getActoFalloParcial() != null
                && StringUtils.isNotBlank(recursoReconsideracionTipo.getActoFalloParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(recursoReconsideracionTipo.getActoFalloParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un ActoFalloParcial con el ID: " + recursoReconsideracionTipo.getActoFalloParcial().getIdArchivo()));
            } else {
                recursoReconsideracion.setActoFalloParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdActoFallo())) {
            final ActoAdministrativo idActoFallo = actoAdministrativoDao.find(String.valueOf(recursoReconsideracionTipo.getIdActoFallo()));
            if (idActoFallo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idActoFallo con el ID: " + recursoReconsideracionTipo.getIdActoFallo()));
            } else {
                recursoReconsideracion.setIdActoFallo(idActoFallo);
            }
        }

        if (recursoReconsideracionTipo.getPruebas() != null
                && !recursoReconsideracionTipo.getPruebas().isEmpty()) {
            for (PruebaTipo pruebaTipo : recursoReconsideracionTipo.getPruebas()) {
                if ((pruebaTipo != null)) {
                    Prueba prueba = pruebaDao.find(Long.parseLong(pruebaTipo.getIdPrueba()));
                    if (prueba == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una prueba con el ID: " + prueba));
                    }
                    prueba.setRecursoReconsideracion(recursoReconsideracion);
                    prueba.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

                }
            }
        }

        if (recursoReconsideracionTipo.getCodCausalesFallo() != null
                && !recursoReconsideracionTipo.getCodCausalesFallo().isEmpty()) {
            RecursoCausalFallo recursoHasCausalFallo;
            for (String codCausalesFallo : recursoReconsideracionTipo.getCodCausalesFallo()) {
                if (StringUtils.isNotBlank(codCausalesFallo)) {
                    ValorDominio valorDominio = valorDominioDao.find(codCausalesFallo);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codCausalesFallo con el ID: " + codCausalesFallo));
                    } else if (recursoReconsideracionTipo.getIdRecursoReconsideracion() != null) {
                        recursoHasCausalFallo = recursoCausalFalloDao.findByRecursoAndCodCausaFallo(recursoReconsideracion, valorDominio);
                        if (recursoHasCausalFallo != null) {
                            LOG.info("Ya existe la relación entre codCausalesFallo: " + valorDominio.getId() + ", y el  recursoReconsideracion:" + recursoReconsideracion.getId());
//                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
//                                    "Ya existe la relación entre codCausalesFallo: " + valorDominio.getId() + ", y el  recursoReconsideracion:" + recursoReconsideracion.getId()));
                        } else {
                            recursoHasCausalFallo = crearRecursoHasCausalFallo(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                            recursoReconsideracion.getcodCausalesFallo().add(recursoHasCausalFallo);
                        }
                    } else {
                        recursoHasCausalFallo = crearRecursoHasCausalFallo(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                        recursoReconsideracion.getcodCausalesFallo().add(recursoHasCausalFallo);
                    }
                }
            }
        }

        if (recursoReconsideracionTipo.getCodDocumentosAnexosRecursoReposicion() != null
                && !recursoReconsideracionTipo.getCodDocumentosAnexosRecursoReposicion().isEmpty()) {
            RecursoAnexoReposicion recursoHasDocAnexoReposicion;
            for (String codDocumentosAnexosReposicion : recursoReconsideracionTipo.getCodDocumentosAnexosRecursoReposicion()) {
                if (StringUtils.isNotBlank(codDocumentosAnexosReposicion)) {
                    ValorDominio valorDominio = valorDominioDao.find(codDocumentosAnexosReposicion);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codDocumentosAnexosReposicion con el ID: " + codDocumentosAnexosReposicion));
                    } else if (recursoReconsideracionTipo.getIdRecursoReconsideracion() != null) {
                        recursoHasDocAnexoReposicion = recursoDocAnexoReposicionDao.findByRecursoAndCodDocAnexoReposion(recursoReconsideracion, valorDominio);
                        if (recursoHasDocAnexoReposicion != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre codDocumentosAnexosReposicion: " + valorDominio.getId() + ", y el  recursoReconsideracion:" + recursoReconsideracion.getId()));
                        } else {
                            recursoHasDocAnexoReposicion = crearRecursoHasDocAnexoReposicion(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                            recursoReconsideracion.getCodDocumentosAnexosRecursoReposicion().add(recursoHasDocAnexoReposicion);
                        }
                    } else {
                        recursoHasDocAnexoReposicion = crearRecursoHasDocAnexoReposicion(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                        recursoReconsideracion.getCodDocumentosAnexosRecursoReposicion().add(recursoHasDocAnexoReposicion);
                    }
                }
            }
        }

        if (recursoReconsideracionTipo.getCodDocumentosAnexosRequisitos() != null
                && !recursoReconsideracionTipo.getCodDocumentosAnexosRequisitos().isEmpty()) {
            RecursoAnexoRequisitos recursoHasDocAnexoRequisitos;
            for (String codDocumentosAnexosRequisitos : recursoReconsideracionTipo.getCodDocumentosAnexosRequisitos()) {
                if (StringUtils.isNotBlank(codDocumentosAnexosRequisitos)) {
                    ValorDominio valorDominio = valorDominioDao.find(codDocumentosAnexosRequisitos);
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codDocumentosAnexosRequisitos con el ID: " + codDocumentosAnexosRequisitos));
                    } else if (recursoReconsideracionTipo.getIdRecursoReconsideracion() != null) {
                        recursoHasDocAnexoRequisitos = recursoDocAnexoRequisitosDao.findByRecursoAndCodDocAnexoRequisitos(recursoReconsideracion, valorDominio);
                        if (recursoHasDocAnexoRequisitos != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre codDocumentosAnexosRequisitos: " + valorDominio.getId() + ", y el  recursoReconsideracion:" + recursoReconsideracion.getId()));
                        } else {
                            recursoHasDocAnexoRequisitos = crearRecursoHasDocAnexoRequisitos(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                            recursoReconsideracion.getCodDocumentosAnexosRequisitos().add(recursoHasDocAnexoRequisitos);
                        }
                    } else {
                        recursoHasDocAnexoRequisitos = crearRecursoHasDocAnexoRequisitos(valorDominio, recursoReconsideracion, contextoTransaccionalTipo);
                        recursoReconsideracion.getCodDocumentosAnexosRequisitos().add(recursoHasDocAnexoRequisitos);
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getIdLiquidador())) {
            ControlLiquidador controlLiquidador = controlLiquidadorDao.find(Long.valueOf(recursoReconsideracionTipo.getIdLiquidador()));
            if (controlLiquidador == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un idLiquidador con el valor" + recursoReconsideracionTipo.getIdLiquidador()));
            } else {
                recursoReconsideracion.setIdLiquidador(controlLiquidador);
            }
        }

        if (StringUtils.isNotBlank(recursoReconsideracionTipo.getCodEstado())) {
            ValorDominio codEstado = valorDominioDao.find(recursoReconsideracionTipo.getCodEstado());
            if (codEstado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstado con el valor" + recursoReconsideracionTipo.getCodEstado()));
            } else {
                recursoReconsideracion.setCodEstado(codEstado);
            }
        }

        if (recursoReconsideracionTipo.getArchivosTemporales() != null
                && !recursoReconsideracionTipo.getArchivosTemporales().isEmpty()) {
            for (ParametroValoresTipo archivoTemporal : recursoReconsideracionTipo.getArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivoTemporal.getIdLlave())) {
                    final ValorDominio codTipoAnexo = valorDominioDao.find(archivoTemporal.getIdLlave());
                    if (codTipoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoAnexo con el ID: " + archivoTemporal.getIdLlave()));
                    } else {
                        for (ParametroTipo archivo : archivoTemporal.getValValor()) {
                            if (StringUtils.isNotBlank(archivo.getIdLlave())) {
                                Archivo anexo = archivoDao.find(Long.valueOf(archivo.getIdLlave()));
                                if (anexo == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró Archivo con el ID: " + archivo.getIdLlave()));
                                } else {
                                    RecursoReconArchivoTemp recursoReconArchivoTemp = new RecursoReconArchivoTemp();
                                    recursoReconArchivoTemp.setRecursoReconsideracion(recursoReconsideracion);
                                    recursoReconArchivoTemp.setArchivo(anexo);
                                    recursoReconArchivoTemp.setCodTipoAnexo(codTipoAnexo);
                                    recursoReconArchivoTemp.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                    recursoReconsideracion.getArchivosTemporales().add(recursoReconArchivoTemp);
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoAnexo")));
                }
            }
        }
    }

    private RecursoCausalFallo crearRecursoHasCausalFallo(ValorDominio valorDominio, RecursoReconsideracion recursoReconsideracion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        RecursoCausalFallo recursoHasCausalFallo = new RecursoCausalFallo();
        recursoHasCausalFallo.setCodCausalesFallo(valorDominio);
        recursoHasCausalFallo.setRecursoReconsideracion(recursoReconsideracion);
        recursoHasCausalFallo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return recursoHasCausalFallo;
    }

    private RecursoAnexoReposicion crearRecursoHasDocAnexoReposicion(ValorDominio valorDominio, RecursoReconsideracion recursoReconsideracion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        RecursoAnexoReposicion recursoHasDocAnexoReposicion = new RecursoAnexoReposicion();
        recursoHasDocAnexoReposicion.setCodDocumentosAnexosRecursoReposicion(valorDominio);
        recursoHasDocAnexoReposicion.setRecursoReconsideracion(recursoReconsideracion);
        recursoHasDocAnexoReposicion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return recursoHasDocAnexoReposicion;
    }

    private RecursoAnexoRequisitos crearRecursoHasDocAnexoRequisitos(ValorDominio valorDominio, RecursoReconsideracion recursoReconsideracion, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        RecursoAnexoRequisitos recursoHasDocAnexoRequisitos = new RecursoAnexoRequisitos();
        recursoHasDocAnexoRequisitos.setCodDocumentosAnexosRequisitos(valorDominio);
        recursoHasDocAnexoRequisitos.setRecursoReconsideracion(recursoReconsideracion);
        recursoHasDocAnexoRequisitos.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return recursoHasDocAnexoRequisitos;
    }
}
