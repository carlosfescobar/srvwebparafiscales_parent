/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1.MsjOpConsultarRecursoReconsideracionKPIFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1.OpConsultarRecursoReconsideracionKPIRespTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplKPIRecursoReconsideracion",
        portName = "portSrvAplKPIRecursoReconsideracionSOAP",
        endpointInterface = "co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1.PortSrvAplKPIRecursoReconsideracionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPIRecursoReconsideracion extends AbstractSrvApl {

    @EJB
    private KPIRecursoReconsideracionFacade KPIrecursoReconsideracionFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPIRecursoReconsideracion.class);

    public OpConsultarRecursoReconsideracionKPIRespTipo opConsultarRecursoReconsideracionKPI(co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1.OpConsultarRecursoReconsideracionKPISolTipo msjOpConsultarRecursoReconsideracionKPISol) throws MsjOpConsultarRecursoReconsideracionKPIFallo {

        LOG.info("OPERACION: opConsultarRecursoReconsideracionKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarRecursoReconsideracionKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarRecursoReconsideracionKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<RecursoReconsideracionTipo> recursoReconsideracionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            recursoReconsideracionTipoPagerData = KPIrecursoReconsideracionFacade.consultarRecursoReconsideracionKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarRecursoReconsideracionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarRecursoReconsideracionKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarRecursoReconsideracionKPIRespTipo resp = new OpConsultarRecursoReconsideracionKPIRespTipo();

        cr.setValCantidadPaginas(recursoReconsideracionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getRecursosReconsideracion().addAll(recursoReconsideracionTipoPagerData.getData());
        resp.setCantidadRecursos(Integer.toString(recursoReconsideracionTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarRecursoReconsideracionKPIRespTipo ::: FIN");

        return resp;
    }

}
