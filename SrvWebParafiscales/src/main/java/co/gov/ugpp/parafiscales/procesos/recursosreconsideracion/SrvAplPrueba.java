/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.procesos.fiscalizacion.SrvAplFiscalizacion;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.MsjOpActualizarPruebaFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.MsjOpBuscarPorIdPruebaFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.MsjOpCrearPruebaFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpActualizarPruebaRespTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpActualizarPruebaSolTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpBuscarPorIdPruebaRespTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpCrearPruebaRespTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpCrearPruebaSolTipo;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplPrueba",
        portName = "portSrvAplPruebaSOAP",
        endpointInterface = "co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.PortSrvAplPruebaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplPrueba extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplFiscalizacion.class);
    @EJB
    private PruebaFacade pruebaFacade;

    public OpCrearPruebaRespTipo opCrearPrueba(OpCrearPruebaSolTipo msjOpCrearPruebaSol) throws MsjOpCrearPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearPruebaSol.getContextoTransaccional();
        final PruebaTipo pruebaTipo = msjOpCrearPruebaSol.getPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long pruebaPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            pruebaPK = pruebaFacade.crearPrueba(pruebaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearPruebaRespTipo resp = new OpCrearPruebaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdPrueba(pruebaPK.toString());
        return resp;
    }

    public OpActualizarPruebaRespTipo opActualizarPrueba(OpActualizarPruebaSolTipo msjOpActualizarPruebaSol) throws MsjOpActualizarPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarPruebaSol.getContextoTransaccional();
        final PruebaTipo pruebaTipo = msjOpActualizarPruebaSol.getPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            pruebaFacade.actualizarPrueba(pruebaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarPruebaRespTipo resp = new OpActualizarPruebaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpBuscarPorIdPruebaRespTipo opBuscarPorIdPrueba(co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1.OpBuscarPorIdPruebaSolTipo msjOpBuscarPorIdPruebaSol) throws MsjOpBuscarPorIdPruebaFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdPruebaSol.getContextoTransaccional();
        final List<String> idPrueba = msjOpBuscarPorIdPruebaSol.getIdPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<PruebaTipo> pruebaTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            pruebaTipos = pruebaFacade.buscarPorIdPrueba(idPrueba, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdPruebaRespTipo resp = new OpBuscarPorIdPruebaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getPrueba().addAll(pruebaTipos);

        return resp;
    }

}
