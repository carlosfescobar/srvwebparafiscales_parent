package co.gov.ugpp.parafiscales.procesos.recursosreconsideracion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.MsjOpActualizarRecursoReconsideracionFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.MsjOpBuscarPorIdRecursoReconsideracionFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.MsjOpCrearRecursoReconsideracionFallo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpActualizarRecursoReconsideracionRespTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpBuscarPorIdRecursoReconsideracionRespTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpBuscarPorIdRecursoReconsideracionSolTipo;
import co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpCrearRecursoReconsideracionRespTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplRecursoReconsideracion",
        portName = "portSrvAplRecursoReconsideracionSOAP",
        endpointInterface = "co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.PortSrvAplRecursoReconsideracionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplRecursoReconsideracion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRecursoReconsideracion.class);

    @EJB
    private RecursoReconsideracionFacade recursoReconsideracionFacade;

    public co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpCrearRecursoReconsideracionRespTipo opCrearRecursoReconsideracion(co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpCrearRecursoReconsideracionSolTipo msjOpCrearRecursoReconsideracionSol) throws MsjOpCrearRecursoReconsideracionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearRecursoReconsideracionSol.getContextoTransaccional();
        final RecursoReconsideracionTipo recursoReconsideracionTipo = msjOpCrearRecursoReconsideracionSol.getRecursoReconsideracion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long recursoReconsideracionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            recursoReconsideracionPK = recursoReconsideracionFacade.crearRecursoReconsideracion(recursoReconsideracionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearRecursoReconsideracionRespTipo resp = new OpCrearRecursoReconsideracionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdRecursoReconsideracion(recursoReconsideracionPK.toString());

        return resp;

    }

    public OpBuscarPorIdRecursoReconsideracionRespTipo opBuscarPorIdRecursoReconsideracion(OpBuscarPorIdRecursoReconsideracionSolTipo msjBuscarPorIdRecursoReconsideracionSolTipo) throws MsjOpBuscarPorIdRecursoReconsideracionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjBuscarPorIdRecursoReconsideracionSolTipo.getContextoTransaccional();
        final List<String> idRecursoReconsideracion = msjBuscarPorIdRecursoReconsideracionSolTipo.getIdRecursoReconsideracion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<RecursoReconsideracionTipo> reconsideracionTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            reconsideracionTipos = recursoReconsideracionFacade.buscarPorIdRecursoReconsideracion(idRecursoReconsideracion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdRecursoReconsideracionRespTipo resp = new OpBuscarPorIdRecursoReconsideracionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getRecursoReconsideracion().addAll(reconsideracionTipos);

        return resp;

    }

    public co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpActualizarRecursoReconsideracionRespTipo opActualizarRecursoReconsideracion(co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1.OpActualizarRecursoReconsideracionSolTipo msjOpActualizarRecursoReconsideracionSol) throws MsjOpActualizarRecursoReconsideracionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarRecursoReconsideracionSol.getContextoTransaccional();
        final RecursoReconsideracionTipo recursoReconsideracionTipo = msjOpActualizarRecursoReconsideracionSol.getRecursoReconsideracion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            recursoReconsideracionFacade.actualizarRecursoReconsideracion(recursoReconsideracionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarRecursoReconsideracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarRecursoReconsideracionRespTipo resp = new OpActualizarRecursoReconsideracionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
