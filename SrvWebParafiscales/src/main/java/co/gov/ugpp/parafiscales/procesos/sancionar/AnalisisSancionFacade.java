package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface AnalisisSancionFacade extends Serializable {

    Long crearAnalisisSancion(AnalisisSancionTipo analisisSancionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarAnalisisSancion(AnalisisSancionTipo analisisSancionTipo,  ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
  List<AnalisisSancionTipo> buscarPorIdAnalisisSancion(List <String> idAnalisisSancionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException;
    
    PagerData<AnalisisSancionTipo> buscarPorCriterioAnalisisSancion(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
     
     
}

