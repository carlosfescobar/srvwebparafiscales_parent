package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AnalisisSancionDao;
import co.gov.ugpp.parafiscales.persistence.dao.UVTDao;
import co.gov.ugpp.parafiscales.persistence.entity.AnalisisSancion;
import co.gov.ugpp.parafiscales.persistence.entity.UVT;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz AnalisisSancionFacade que contiene las
 * operaciones del servicio SrvAplAnalisisSancionFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AnalisisSancionFacadeImpl extends AbstractFacade implements AnalisisSancionFacade {

    @EJB
    private AnalisisSancionDao analisisSancionDao;

    @EJB
    private UVTDao UVTDao;

    @Override

    public Long crearAnalisisSancion(AnalisisSancionTipo analisisSancionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (analisisSancionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkAnalisisSancion(analisisSancionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        final AnalisisSancion analisisSancion = new AnalisisSancion();
        
        analisisSancion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAnalisisSancion(analisisSancionTipo, analisisSancion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        return analisisSancionDao.create(analisisSancion);
    }

    @Override
    public void actualizarAnalisisSancion(AnalisisSancionTipo analisisSancionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (analisisSancionTipo == null || StringUtils.isBlank(analisisSancionTipo.getIdAnalisisSancion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        AnalisisSancion analisisSancion = analisisSancionDao.find(Long.valueOf(analisisSancionTipo.getIdAnalisisSancion()));

        if (analisisSancion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un analisisSancion con el valor:" + analisisSancionTipo.getIdAnalisisSancion()));
            throw new AppException(errorTipoList);
        }

        analisisSancion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateAnalisisSancion(analisisSancionTipo, analisisSancion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        analisisSancionDao.edit(analisisSancion);

    }

    @Override
    public List<AnalisisSancionTipo> buscarPorIdAnalisisSancion(List<String> idAnalisisSancionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idAnalisisSancionTipo == null || idAnalisisSancionTipo.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idAnalisisSancionTipo, String.class, Long.class);

        final List<AnalisisSancion> analisisSancionList = analisisSancionDao.findByIdList(ids);

        final List<AnalisisSancionTipo> analisisSancionTipoList
                = mapper.map(analisisSancionList, AnalisisSancion.class, AnalisisSancionTipo.class);

        return analisisSancionTipoList;

    }

    @Override
    public PagerData<AnalisisSancionTipo> buscarPorCriterioAnalisisSancion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<AnalisisSancion> analisisSancionPagerData = analisisSancionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<AnalisisSancionTipo> analisisSancionTipoList
                = mapper.map(analisisSancionPagerData.getData(), AnalisisSancion.class, AnalisisSancionTipo.class);

        return new PagerData<AnalisisSancionTipo>(analisisSancionTipoList, analisisSancionPagerData.getNumPages());
    }

    private void populateAnalisisSancion(final AnalisisSancionTipo analisisSancionTipo,
            final AnalisisSancion analisisSancion, final List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if ((analisisSancionTipo.getUvt() != null
                && StringUtils.isNotBlank(analisisSancionTipo.getUvt().getIdUVT()))) {
            UVT uvt = UVTDao.find(Long.valueOf(analisisSancionTipo.getUvt().getIdUVT()));
            if (uvt == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontro una UVT con el Id:" + analisisSancionTipo.getUvt().getIdUVT()));
            } else {
                analisisSancion.setUvt(uvt);
            }
        }

        if ((analisisSancionTipo.getFecVencimientoTermino() != null)) {
            analisisSancion.setFecVencimientoTermino(analisisSancionTipo.getFecVencimientoTermino());
        }

        if ((analisisSancionTipo.getFecLlegadaExtemporanea() != null)) {
            analisisSancion.setFecLlegadaTemporanea(analisisSancionTipo.getFecLlegadaExtemporanea());
        }
        if (StringUtils.isNotBlank(analisisSancionTipo.getDiasIncumplimiento())) {
            analisisSancion.setDiasIncumplimiento(analisisSancionTipo.getDiasIncumplimiento());
        }

        if (StringUtils.isNotBlank(analisisSancionTipo.getValSancionLetras())) {
            analisisSancion.setValSancionLetras(analisisSancionTipo.getValSancionLetras());
        }

        if (StringUtils.isNotBlank(analisisSancionTipo.getValSancionNumeros())) {
            analisisSancion.setValSancionNumeros(analisisSancionTipo.getValSancionNumeros());
        }
        if (StringUtils.isNotBlank(analisisSancionTipo.getDesObservacionesImporteSancion())) {
            analisisSancion.setDesObserImporteSancion(analisisSancionTipo.getDesObservacionesImporteSancion());
        }
    }
}
