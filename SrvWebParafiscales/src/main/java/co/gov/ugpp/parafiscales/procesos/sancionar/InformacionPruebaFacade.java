package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface InformacionPruebaFacade extends Serializable {

    Long crearInformacionPrueba(InformacionPruebaTipo  informacionPruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarInformacionPrueba(InformacionPruebaTipo informacionPruebaTipo,  ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    List<InformacionPruebaTipo> buscarPorIdInformacionPrueba(List <String> idInormacionPruebasTipos, ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException;
    
    
    PagerData<InformacionPruebaTipo> buscarPorCriterioSancion(final List<ParametroTipo> parametroTipoList,final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
    final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
