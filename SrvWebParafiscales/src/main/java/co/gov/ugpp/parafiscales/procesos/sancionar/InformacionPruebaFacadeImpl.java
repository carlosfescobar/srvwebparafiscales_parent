package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionPruebaDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionPruebaDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPrueba;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPruebaDocumentos;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz InformacionPruebaFacade que contiene las
 * operaciones del servicio SrvAplInformacionPrueba
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class InformacionPruebaFacadeImpl extends AbstractFacade implements InformacionPruebaFacade {

    @EJB
    private InformacionPruebaDao informacionPruebaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;
    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ExpedienteFacade expedienteFacade;
    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private InformacionPruebaDocumentoDao informacionPruebaDocumentoDao;

    /**
     * implementación de la operación crearInformacionPrueba se encarga de crear
     * una informacionPrueba en la base de datos a partir del objeto
     * informacionPruebaTipo
     *
     * @param informacionPruebaTipo Objeto a partir del cual se va a crear una
     * informacionPrueba
     * @param contextoTransaccionalTipo Contiene la información que se va
     * almacenar en la auditoria
     * @return informacionPrueba creada que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public Long crearInformacionPrueba(InformacionPruebaTipo informacionPruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (informacionPruebaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkInformacionPrueba(informacionPruebaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final InformacionPrueba informacionPrueba = new InformacionPrueba();

        informacionPrueba.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateInformacionPrueba(informacionPruebaTipo, informacionPrueba, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return informacionPruebaDao.create(informacionPrueba);
    }

    @Override
    public void actualizarInformacionPrueba(InformacionPruebaTipo informacionPruebaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(informacionPruebaTipo.getIdInformacionPrueba())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }
        final InformacionPrueba informacionPrueba = informacionPruebaDao.find(Long.valueOf(informacionPruebaTipo.getIdInformacionPrueba()));

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (informacionPrueba == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "La informacionPrueba que desea Actualizar no existe con el ID:" + informacionPruebaTipo.getIdInformacionPrueba()));
            throw new AppException(errorTipoList);
        }
        informacionPrueba.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateInformacionPrueba(informacionPruebaTipo, informacionPrueba, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        informacionPruebaDao.edit(informacionPrueba);
    }

    @Override
    public List<InformacionPruebaTipo> buscarPorIdInformacionPrueba(List<String> idInormacionPruebasTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idInormacionPruebasTipos == null || idInormacionPruebasTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idInormacionPruebasTipos, String.class, Long.class);

        final List<InformacionPrueba> informacionPruebaList = informacionPruebaDao.findByIdList(ids);

        final List<InformacionPruebaTipo> informacionPruebaTipoList
                = mapper.map(informacionPruebaList, InformacionPrueba.class, InformacionPruebaTipo.class);

        if (informacionPruebaTipoList != null) {
            for (final InformacionPruebaTipo informacionPruebaTipo : informacionPruebaTipoList) {
                if (informacionPruebaTipo.getFuncionarioResuelveInterna() != null
                        && StringUtils.isNotBlank(informacionPruebaTipo.getFuncionarioResuelveInterna().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(informacionPruebaTipo.getFuncionarioResuelveInterna(), contextoTransaccionalTipo);
                    informacionPruebaTipo.setFuncionarioResuelveInterna(funcionarioTipo);
                }
            }
        }
        return informacionPruebaTipoList;
    }

    @Override
    public PagerData<InformacionPruebaTipo> buscarPorCriterioSancion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<InformacionPrueba> informacionPruebasPagerData = informacionPruebaDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());
        final List<InformacionPruebaTipo> informacionPruebaTipoList = new ArrayList<InformacionPruebaTipo>();

        for (final InformacionPrueba informacionPrueba : informacionPruebasPagerData.getData()) {
            final InformacionPruebaTipo informacionPruebaTipo = mapper.map(informacionPrueba, InformacionPruebaTipo.class);
            if (informacionPruebaTipo.getFuncionarioResuelveInterna() != null
                    && StringUtils.isNotBlank(informacionPruebaTipo.getFuncionarioResuelveInterna().getIdFuncionario())) {
                final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(informacionPruebaTipo.getFuncionarioResuelveInterna(), contextoTransaccionalTipo);
                informacionPruebaTipo.setFuncionarioResuelveInterna(funcionarioTipo);
            }
            informacionPruebaTipoList.add(informacionPruebaTipo);
        }
        return new PagerData(informacionPruebaTipoList, informacionPruebasPagerData.getNumPages());
    }

    private void populateInformacionPrueba(final InformacionPruebaTipo informacionPruebaTipo,
            final InformacionPrueba informacionPrueba, final List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isNotBlank(informacionPruebaTipo.getEsRequiereInformacion())) {
            Validator.parseBooleanFromString(informacionPruebaTipo.getEsRequiereInformacion(), informacionPruebaTipo.getClass().getSimpleName(), "esRequiereInformacion", errorTipoList);
            informacionPrueba.setEsRequerimientoInformacion(mapper.map(informacionPruebaTipo.getEsRequiereInformacion(), Boolean.class));
        }

        if (StringUtils.isNotBlank(informacionPruebaTipo.getCodTipoRequerimiento())) {
            final ValorDominio codTipoRequerimiento = valorDominioDao.find(informacionPruebaTipo.getCodTipoRequerimiento());
            if (codTipoRequerimiento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoRequerimiento no existe con el ID: " + informacionPruebaTipo.getCodTipoRequerimiento()));
            } else {
                informacionPrueba.setCodTipoRequerimiento(codTipoRequerimiento);
            }
        }
        if (StringUtils.isNotBlank(informacionPruebaTipo.getDesInformacionRequerida())) {
            informacionPrueba.setDesInformacionRequerida(informacionPruebaTipo.getDesInformacionRequerida());
        }
        if (StringUtils.isNotBlank(informacionPruebaTipo.getValAreaSeleccionada())) {
            informacionPrueba.setValAreaSeleccionada(informacionPruebaTipo.getValAreaSeleccionada());
        }

        if (StringUtils.isNotBlank(informacionPruebaTipo.getCodPersonaSolicitudExterna())) {
            final ValorDominio codPersonaSolicitudExterna = valorDominioDao.find(informacionPruebaTipo.getCodPersonaSolicitudExterna());
            if (codPersonaSolicitudExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codPersonaSolicitudExterna no existe con el ID: " + informacionPruebaTipo.getCodPersonaSolicitudExterna()));
            } else {
                informacionPrueba.setCodPersonaSolicitudExterna(codPersonaSolicitudExterna);
            }
        }

        if (informacionPruebaTipo.getEntidadExterna() != null
                && StringUtils.isNotBlank(informacionPruebaTipo.getEntidadExterna().getIdEntidadExterna())) {
            final EntidadExterna idEntidadExterna = entidadExternaDao.find(Long.valueOf(informacionPruebaTipo.getEntidadExterna().getIdEntidadExterna()));
            if (idEntidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El idEntidadExterna no existe con el ID: " + informacionPruebaTipo.getEntidadExterna().getIdEntidadExterna()));
            } else {
                informacionPrueba.setEntidadExterna(idEntidadExterna);
            }
        }

        if (informacionPruebaTipo.getFuncionarioResuelveInterna() != null
                && StringUtils.isNotBlank(informacionPruebaTipo.getFuncionarioResuelveInterna().getIdFuncionario())) {
            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(informacionPruebaTipo.getFuncionarioResuelveInterna(), contextoTransaccionalTipo);
            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Debe proporcionar un Funcionario existente, Funcionario ID: " + informacionPruebaTipo.getFuncionarioResuelveInterna().getIdFuncionario()));
            } else {
                informacionPrueba.setFuncionarioResuelveInterna(funcionarioTipo.getIdFuncionario());
            }
        }

        if (StringUtils.isNotBlank(informacionPruebaTipo.getDesRespuestaInformacion())) {
            informacionPrueba.setDesRespuestaInformacion(informacionPruebaTipo.getDesRespuestaInformacion());
        }

        if (StringUtils.isNotBlank(informacionPruebaTipo.getEsInformacionExternaCompleta())) {
            Validator.parseBooleanFromString(informacionPruebaTipo.getEsInformacionExternaCompleta(), informacionPruebaTipo.getClass().getSimpleName(), "esRequiereInformacion", errorTipoList);
            informacionPrueba.setEsInformacionExternaCompleta(mapper.map(informacionPruebaTipo.getEsInformacionExternaCompleta(), Boolean.class));
        }

        if (informacionPruebaTipo.getRequerimientoInformacionParcial() != null
                && StringUtils.isNotBlank(informacionPruebaTipo.getRequerimientoInformacionParcial().getIdArchivo())) {

            final AprobacionDocumento requerimientoInformacionParcial = aprobacionDocumentoDao.find(Long.valueOf(informacionPruebaTipo.getRequerimientoInformacionParcial().getIdArchivo()));
            if (requerimientoInformacionParcial == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un requerimientoInformacionParcial con el id : " + informacionPruebaTipo.getRequerimientoInformacionParcial().getIdArchivo()));
            } else {
                informacionPrueba.setRequerimientoInformacionParcial(requerimientoInformacionParcial);
            }

        }

        if (StringUtils.isNotBlank(informacionPruebaTipo.getIdActoRequerimientoInformacion())) {

            final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(informacionPruebaTipo.getIdActoRequerimientoInformacion());
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un actoAdministrativo con el id : " + informacionPruebaTipo.getIdActoRequerimientoInformacion()));
            } else {
                informacionPrueba.setIdActoRequerimientoInformacion(actoAdministrativo);
            }
        }

        if (informacionPruebaTipo.getIdDocumentosRespuestaInterna() != null
                && !informacionPruebaTipo.getIdDocumentosRespuestaInterna().isEmpty()) {
            InformacionPruebaDocumentos informacionPruebaDocumentosRespuesta;
            for (String idDocumentos : informacionPruebaTipo.getIdDocumentosRespuestaInterna()) {
                if (StringUtils.isNotBlank(idDocumentos)) {
                    DocumentoEcm documentoEcm = documentoDao.find(idDocumentos);
                    if (documentoEcm == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se contró documento con el ID: " + idDocumentos));
                    } else if (informacionPrueba.getId() != null) {
                        informacionPruebaDocumentosRespuesta = informacionPruebaDocumentoDao.findByInformacionPruebaAndDocumento(informacionPrueba, documentoEcm);
                        if (informacionPruebaDocumentosRespuesta != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre el docuemnto : " + documentoEcm.getId() + ", y la informacionprueba :" + informacionPrueba.getId()));
                        } else {
                            informacionPruebaDocumentosRespuesta = crearInformacionPruebaDocumentos(informacionPrueba, documentoEcm, contextoTransaccionalTipo);
                            informacionPrueba.getIdDocumentosRespuestaInterna().add(informacionPruebaDocumentosRespuesta);
                        }
                    } else {
                        informacionPruebaDocumentosRespuesta = crearInformacionPruebaDocumentos(informacionPrueba, documentoEcm, contextoTransaccionalTipo);
                        informacionPrueba.getIdDocumentosRespuestaInterna().add(informacionPruebaDocumentosRespuesta);
                    }
                }
            }
        }
    }

    private InformacionPruebaDocumentos crearInformacionPruebaDocumentos(InformacionPrueba informacionPrueba, DocumentoEcm documentoEcm, ContextoTransaccionalTipo contextoTransaccionalTipo) {
        InformacionPruebaDocumentos informacionPruebaDocumentosRespuesta = new InformacionPruebaDocumentos();
        informacionPruebaDocumentosRespuesta.setDocumento(documentoEcm);
        informacionPruebaDocumentosRespuesta.setInformacionPrueba(informacionPrueba);
        informacionPruebaDocumentosRespuesta.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        return informacionPruebaDocumentosRespuesta;
    }

}
