package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.sanciones.investigadotipo.v1.InvestigadoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface InvestigadoFacade extends Serializable {
    
   List<InvestigadoTipo> buscarPorIdInvestigado(List<IdentificacionTipo> identificacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}