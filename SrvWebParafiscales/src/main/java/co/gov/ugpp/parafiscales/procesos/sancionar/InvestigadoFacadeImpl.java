package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.InvestigadoDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Investigado;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.schema.sanciones.investigadotipo.v1.InvestigadoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz InvestigadoFacade que contiene las operaciones del servicio SrvAplInvestigado
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class InvestigadoFacadeImpl extends AbstractFacade implements InvestigadoFacade {

    private static final Logger LOG = LoggerFactory.getLogger(InvestigadoFacadeImpl.class);

    @EJB
    private InvestigadoDao investigadoDao;

    /**
     * implementación de la operación buscarPorIdInvestigado se encarga de consultar un listado de investigados por
     * medio de un listado de identificaciones enviadas
     *
     * @param identificacionTipoList listado de indentificaciones con las cuales se busca un listado de invetigados en
     * la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return Listado de invetigados encontrados que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public List<InvestigadoTipo> buscarPorIdInvestigado(final List<IdentificacionTipo> identificacionTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTipoList == null || identificacionTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar los datos de identificaciones de las personas");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTipoList, IdentificacionTipo.class, Identificacion.class);

        final List<Investigado> investigadoList = investigadoDao.findByIdentificacionList(identificacionList);

        final List<InvestigadoTipo> investigadoTipoList
                = mapper.map(investigadoList, Investigado.class, InvestigadoTipo.class);

        return investigadoTipoList;
    }
}
