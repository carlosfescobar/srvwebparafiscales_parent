package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface LiquidacionParcialFacade extends Serializable {

    Long crearLiquidacionParcial(LiquidacionParcialTipo liquidacionParcialTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarLiquidacionParcial(LiquidacionParcialTipo liquidacionParcialTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<LiquidacionParcialTipo> buscarPorIdLiquidacionParcial(List<String> idLiquidacionParcialTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<LiquidacionParcialTipo> buscarPorCriteriosLiquidacionParcial(final List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
