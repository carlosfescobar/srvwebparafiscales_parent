package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.LiquidacionParcialDao;
import co.gov.ugpp.parafiscales.persistence.dao.UVTDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionParcial;
import co.gov.ugpp.parafiscales.persistence.entity.UVT;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz UVTFacade que contiene las operaciones del
 * servicio SrvAplUVTFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class LiquidacionParcialFacadeImpl extends AbstractFacade implements LiquidacionParcialFacade {

    @EJB
    private UVTDao UVTDao;

    @EJB
    private LiquidacionParcialDao LiquidacionParcialDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * implementación de la operación crearLiquidacionParcial se encarga de
     * crear una nueva LiquidacionParcial en la base de datos a partir del dto
     * liquidacionParcialTipo
     *
     * @param liquidacionParcialTipo Objeto a partir del cual se va a crear una
     * sancion en la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return liquidacionParcial creada que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearLiquidacionParcial(LiquidacionParcialTipo liquidacionParcialTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (liquidacionParcialTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final LiquidacionParcial liquidacionParcial = new LiquidacionParcial();

        liquidacionParcial.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateLiquidacionparcial(liquidacionParcialTipo, liquidacionParcial, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        return LiquidacionParcialDao.create(liquidacionParcial);
    }

    /**
     * implementación de la operación actualizarLiquidacionParcial se encarga de
     * actualizar una liquidacionParcial de acuerdo a el id de la sancion
     * enviado
     *
     * @param liquidacionParcialTipo Objeto por medio del cual se va a
     * actualizar una liquidacionParcial
     * @param contextoTransaccionalTipo Contiene la información que se va
     * almacenar en la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public void actualizarLiquidacionParcial(LiquidacionParcialTipo liquidacionParcialTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (liquidacionParcialTipo == null || StringUtils.isBlank(liquidacionParcialTipo.getIdLiquidacionParcial())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final LiquidacionParcial liquidacionParcial = LiquidacionParcialDao.find(Long.valueOf(liquidacionParcialTipo.getIdLiquidacionParcial()));

        if (liquidacionParcial == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró una liquidacionParcial con el valor:" + liquidacionParcialTipo.getIdLiquidacionParcial()));
            throw new AppException(errorTipoList);
        }

        liquidacionParcial.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateLiquidacionparcial(liquidacionParcialTipo, liquidacionParcial, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        LiquidacionParcialDao.edit(liquidacionParcial);
    }

    /**
     * implementación de la operación buscarPorIdLiquidacionParcial se encarga
     * de buscar una liquidacionParcial en la base de datos p
     *
     * @param idLiquidacionParcialTipos lista de ids de busqueda
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Litado de sacion encontrado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public List<LiquidacionParcialTipo> buscarPorIdLiquidacionParcial(List<String> idLiquidacionParcialTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idLiquidacionParcialTipos == null || idLiquidacionParcialTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idLiquidacionParcialTipos, String.class, Long.class);

        final List<LiquidacionParcial> liquidacionParcialList = LiquidacionParcialDao.findByIdList(ids);

        final List<LiquidacionParcialTipo> liquidacionTipoipoList
                = mapper.map(liquidacionParcialList, LiquidacionParcial.class, LiquidacionParcialTipo.class);

        return liquidacionTipoipoList;

    }

    /**
     * implementación de la operación buscarPorCriteriosLiquidacionParcial se
     * encarga de buscar una liquidacionParcial en la base de datos por medio de
     * los parametros enviados y ordena la consulta de acuerdo a los criterios
     * establecidos
     *
     * @param parametroTipoList lista de parametros por medio de los cuales se
     * busca una liquidacionParcial en la base de datos
     * @param criterioOrdenamientoTipos Atributos por los cuales se va a ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Litado de sacion encontrado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public PagerData<LiquidacionParcialTipo> buscarPorCriteriosLiquidacionParcial(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final PagerData<LiquidacionParcial> pagerDataEntity = LiquidacionParcialDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<LiquidacionParcialTipo> liquidacionParcialTipoList = mapper.map(pagerDataEntity.getData(), LiquidacionParcial.class, LiquidacionParcialTipo.class);

        return new PagerData<LiquidacionParcialTipo>(liquidacionParcialTipoList, pagerDataEntity.getNumPages());
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad liquidacionParcial
     *
     * @param liquidacionParcialTipo Objeto origen
     * @param liquidacionParcial Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios
     */
    private void populateLiquidacionparcial(final LiquidacionParcialTipo liquidacionParcialTipo, final LiquidacionParcial liquidacionParcial,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(liquidacionParcialTipo.getCodAccionLiquidacionParcial())) {
            ValorDominio valorDominio = valorDominioDao.find(liquidacionParcialTipo.getCodAccionLiquidacionParcial());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontro un codAccionLiquidacionParcial: " + liquidacionParcialTipo.getCodAccionLiquidacionParcial()));
            } else {
                liquidacionParcial.setCodAccionLiquidacionParcial(valorDominio);
            }
        }

        if (liquidacionParcialTipo.getUvt() != null
                && StringUtils.isNotBlank(liquidacionParcialTipo.getUvt().getIdUVT())) {
            UVT uvt = UVTDao.find(Long.valueOf(liquidacionParcialTipo.getUvt().getIdUVT()));
            if (uvt == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotró un uvt con el id : " + liquidacionParcialTipo.getUvt().getIdUVT()));
            } else {
                liquidacionParcial.setUvt(uvt);
            }
        }

        if (StringUtils.isNotBlank(liquidacionParcialTipo.getDesObservaciones())) {
            liquidacionParcial.setDesObservaciones(liquidacionParcialTipo.getDesObservaciones());
        }
    }
}
