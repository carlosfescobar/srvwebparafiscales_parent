package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.sancion.sanciontipo.v1.SancionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface SancionFacade extends Serializable {

    Long crearSancion(final SancionTipo sancionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarSancion(final SancionTipo sancionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SancionTipo> buscarPorIdSancion(List<String> idSancionTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<SancionTipo> buscarPorCriterioSancion(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
