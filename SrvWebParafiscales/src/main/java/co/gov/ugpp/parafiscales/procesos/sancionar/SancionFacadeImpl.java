package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AnalisisSancionDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConstanciaEjecutoriaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionPruebaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SancionInformacionRequeridaDao;
import co.gov.ugpp.parafiscales.persistence.dao.LiquidacionParcialDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SancionPruebaRequeridaDao;
import co.gov.ugpp.parafiscales.persistence.dao.RecursoReconsideracionDao;
import co.gov.ugpp.parafiscales.persistence.dao.SancionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.AnalisisSancion;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaEjecutoria;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPrueba;
import co.gov.ugpp.parafiscales.persistence.entity.SancionInformacionRequerida;
import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionParcial;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.SancionPruebaRequerida;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.Sancion;
import co.gov.ugpp.parafiscales.persistence.entity.SancionHasAccion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import co.gov.ugpp.schema.sancion.sanciontipo.v1.SancionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz SancionFacade que contiene las operaciones del
 * servicio SrvAplSancion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SancionFacadeImpl extends AbstractFacade implements SancionFacade {

    @EJB
    private SancionDao sancionDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private InformacionPruebaDao informacionPruebaDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private AnalisisSancionDao analisisSancionDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private RecursoReconsideracionDao recursoReconsideracionDao;

    @EJB
    private ConstanciaEjecutoriaDao constanciaEjecutoriaDao;

    @EJB
    private LiquidacionParcialDao liquidacionParcialDao;

    @EJB
    private SancionPruebaRequeridaDao pruebaRequeridaDao;

    @EJB
    private SancionInformacionRequeridaDao informacionRequeridaDao;

    /**
     * implementación de la operación crearSancion se encarga de crear una nueva
     * sancion en la base de datos a partir del dto sancionTipo
     *
     * @param sancionTipo Objeto a partir del cual se va a crear una sancion en
     * la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return sancion creada que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long crearSancion(final SancionTipo sancionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (sancionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkSancion(sancionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        final Sancion sancion = new Sancion();

        sancion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSancion(sancionTipo, sancion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        this.checkBusinessSancion(sancionTipo, sancion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        return sancionDao.create(sancion);
    }

    /**
     * implementación de la operación actualizarSancion se encarga de actualizar
     * una Sancion de acuerdo a el id de la sancion enviado
     *
     * @param sancionTipo Objeto por medio del cual se va a actualizar una
     * Sancion
     * @param contextoTransaccionalTipo Contiene la información que se va
     * almacenar en la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public void actualizarSancion(final SancionTipo sancionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (sancionTipo == null || StringUtils.isBlank(sancionTipo.getIdSancion())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Sancion sancion = sancionDao.find(Long.valueOf(sancionTipo.getIdSancion()));

        if (sancion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un idSanción con el valor:" + sancionTipo.getIdSancion()));
            throw new AppException(errorTipoList);
        }
        sancion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSancion(sancionTipo, sancion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        this.checkBusinessSancion(sancionTipo, sancion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        sancionDao.edit(sancion);
    }

    /**
     * implementación de la operación buscarPorCriterioSancion se encarga de
     * buscar una Sancion en la base de datos por medio de los parametros
     * enviados y ordena la consulta de acuerdo a los criterios establecidos
     *
     * @param parametroTipoList lista de parametros por medio de los cuales se
     * busca una Sancion en la base de datos
     * @param criterioOrdenamientoTipos Atributos por los cuales se va a ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Litado de sacion encontrado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public PagerData<SancionTipo> buscarPorCriterioSancion(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Sancion> pagerDataEntity = sancionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<SancionTipo> sancionTipoList = mapper.map(pagerDataEntity.getData(), Sancion.class, SancionTipo.class);

        this.validarFuncionario(sancionTipoList, contextoTransaccionalTipo);

        return new PagerData(sancionTipoList, pagerDataEntity.getNumPages());
    }

    @Override
    public List<SancionTipo> buscarPorIdSancion(List<String> idSancionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (idSancionTipos == null || idSancionTipos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        final List<Long> ids = mapper.map(idSancionTipos, String.class, Long.class
        );

        final List<Sancion> sancionList = sancionDao.findByIdList(ids);

        final List<SancionTipo> sancionTipoList
                = mapper.map(sancionList, Sancion.class, SancionTipo.class);

        this.validarFuncionario(sancionTipoList, contextoTransaccionalTipo);

        return sancionTipoList;
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad sancion
     *
     * @param sancionTipo Objeto origen
     * @param sancion Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateSancion(final SancionTipo sancionTipo, final Sancion sancion, ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(sancionTipo.getCodProcesoPadre())) {
            final ValorDominio codProcesoPadre = valorDominioDao.find(sancionTipo.getCodProcesoPadre());
            if (codProcesoPadre == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codProcesoPadre con el valor:" + sancionTipo.getCodProcesoPadre()));
            }
            sancion.setCodProcesoPadre(codProcesoPadre);
        }
        if (StringUtils.isNotBlank(sancionTipo.getIdProcesoPadre())) {
            sancion.setIdProcesoPadre(sancionTipo.getIdProcesoPadre());
        }

        if (sancionTipo.getExpedienteCaso() != null
                && StringUtils.isNotBlank(sancionTipo.getExpedienteCaso().getIdNumExpediente())) {
            Expediente expedienteCaso = expedienteDao.find(sancionTipo.getExpedienteCaso().getIdNumExpediente());
            if (expedienteCaso == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un IdNumExpediente con el valor:" + sancionTipo.getExpedienteCaso().getIdNumExpediente()));
            }
            sancion.setExpedienteCaso(expedienteCaso);
        }

        if (sancionTipo.getExpedienteSancion() != null
                && StringUtils.isNotBlank(sancionTipo.getExpedienteSancion().getIdNumExpediente())) {
            Expediente expedienteSancion = expedienteDao.find(sancionTipo.getExpedienteSancion().getIdNumExpediente());
            if (expedienteSancion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un IdNumExpediente con el valor:" + sancionTipo.getExpedienteSancion().getIdNumExpediente()));
            }
            sancion.setExpedienteSancion(expedienteSancion);
        }

        if (StringUtils.isNotBlank(sancionTipo.getCodEstadoSancion())) {
            ValorDominio codEstadoSancion = valorDominioDao.find(sancionTipo.getCodEstadoSancion());
            if (codEstadoSancion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoSancion con el valor:" + sancionTipo.getCodEstadoSancion()));
            }
            sancion.setCodEstadoSancion(codEstadoSancion);
        }

        if (StringUtils.isNotBlank(sancionTipo.getCodTipoSancion())) {
            ValorDominio tipoSancion = valorDominioDao.find(sancionTipo.getCodTipoSancion());
            if (tipoSancion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codEstadoSancion con el valor:" + sancionTipo.getCodTipoSancion()));
            }
            sancion.setCodTipoSancion(tipoSancion);
        }

        if (sancionTipo.getInvestigado() != null) {

            final IdentificacionTipo identificacionTipoInvestigado = Util.obtenerIdentificacionTipoFromChoice(sancionTipo.getInvestigado().getPersonaNatural(),
                    sancionTipo.getInvestigado().getPersonaJuridica());

            final Identificacion identificacion = mapper.map(identificacionTipoInvestigado, Identificacion.class);
            Persona investigado = personaDao.findByIdentificacion(identificacion);

            if (investigado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un Investigado con la Identificación :" + identificacion));
            } else {
                sancion.setInvestigado(investigado);
            }
        }

        if (sancionTipo.getGestorSancion() != null
                && StringUtils.isNotBlank(sancionTipo.getGestorSancion().getIdFuncionario())) {
            final FuncionarioTipo gestorSancion = funcionarioFacade.buscarFuncionario(sancionTipo.getGestorSancion(), contextoTransaccionalTipo);
            if (gestorSancion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un gestorSancion  con el ID: " + sancionTipo.getGestorSancion().getIdFuncionario()));
            } else {
                sancion.setGestorSancion(gestorSancion.getIdFuncionario());
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getEsSancion())) {
            sancion.setEsSancion(Boolean.valueOf(sancionTipo.getEsSancion()));

        }
        if (StringUtils.isNotBlank(sancionTipo.getDesObservacionesNoProcedeSancion())) {
            sancion.setDesObservacionesNoProcedeSancion(sancionTipo.getDesObservacionesNoProcedeSancion());
        }

        if (sancionTipo.getAnalisisSancion() != null
                && !sancionTipo.getAnalisisSancion().isEmpty()) {
            for (AnalisisSancionTipo analisisSancionTipo : sancionTipo.getAnalisisSancion()) {
                if (analisisSancionTipo != null) {
                    AnalisisSancion analisisSancion = analisisSancionDao.find(Long.valueOf(analisisSancionTipo.getIdAnalisisSancion()));
                    if (analisisSancion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un analisisSancion  con el id : " + analisisSancionTipo.getIdAnalisisSancion()));
                    } else {
                        analisisSancion.setSancion(sancion);
                        analisisSancion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                    }
                }
            }
        }

        if (sancionTipo.getPliegoCargosParcial() != null
                && StringUtils.isNotBlank(sancionTipo.getPliegoCargosParcial().getIdArchivo())) {
            AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(sancionTipo.getPliegoCargosParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        " No se encotró un pliegoCargosParcial con el id : " + sancionTipo.getPliegoCargosParcial().getIdArchivo()));
            } else {
                sancion.setPliegoCargosParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getIdActoPliegoCargos())) {
            ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(sancionTipo.getIdActoPliegoCargos());
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotraró un idActoPleirgodCargo con el id : " + sancionTipo.getIdActoPliegoCargos()));
            } else {
                sancion.setIdActoPliegoCargos(actoAdministrativo);
            }
        }

        if (sancionTipo.getResolucionSancionParcial() != null
                && StringUtils.isNotBlank(sancionTipo.getResolucionSancionParcial().getIdArchivo())) {
            AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(sancionTipo.getResolucionSancionParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        " No se encotró una resolucionSancionParcial con el id : " + sancionTipo.getResolucionSancionParcial().getIdArchivo()));
            } else {
                sancion.setResolucionSancionParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getIdActoResolucionSancion())) {
            ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(sancionTipo.getIdActoResolucionSancion());
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotraró un idActoResolucionSancion con el id : " + sancionTipo.getIdActoResolucionSancion()));
            } else {
                sancion.setIdActoResolucionSancion(actoAdministrativo);
            }
        }

        if (sancionTipo.getAutoArchivoParcial() != null
                && StringUtils.isNotBlank(sancionTipo.getAutoArchivoParcial().getIdArchivo())) {
            AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(sancionTipo.getAutoArchivoParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        " No se encotró una autoArchivoParcial con el id : " + sancionTipo.getAutoArchivoParcial().getIdArchivo()));
            } else {
                sancion.setAutoArchivoParcial(aprobacionDocumento);
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getIdActoAutoArchivo())) {
            ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(sancionTipo.getIdActoAutoArchivo());
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotraró un idActoAutoArchivo con el id : " + sancionTipo.getIdActoAutoArchivo()));
            } else {
                sancion.setIdActoAutoArchivo(actoAdministrativo);
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getIdRecursoReconsideracion())) {
            RecursoReconsideracion recursoReconsideracion = recursoReconsideracionDao.find(Long.valueOf(sancionTipo.getIdRecursoReconsideracion()));
            if (recursoReconsideracion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotraró un recursoReconsideracion con el id : " + sancionTipo.getIdRecursoReconsideracion()));
            } else {
                sancion.setIdRecursoReconsideracion(recursoReconsideracion);
            }
        }

        if (StringUtils.isNotBlank(sancionTipo.getIdConstanciaEjecutoria())) {
            ConstanciaEjecutoria constanciaEjecutoria = constanciaEjecutoriaDao.find(Long.valueOf(sancionTipo.getIdConstanciaEjecutoria()));
            if (constanciaEjecutoria == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encotraró una constanciaEjecutoria con el id : " + sancionTipo.getIdConstanciaEjecutoria()));
            } else {
                sancion.setIdConstanciaEjecutoria(constanciaEjecutoria);
            }
        }

        if (sancionTipo.getLiquidacionesParciales() != null
                && !sancionTipo.getLiquidacionesParciales().isEmpty()) {

            for (LiquidacionParcialTipo liquidacionParcialTipo : sancionTipo.getLiquidacionesParciales()) {
                if (liquidacionParcialTipo != null) {
                    LiquidacionParcial liquidacionParcial = liquidacionParcialDao.find(Long.valueOf(liquidacionParcialTipo.getIdLiquidacionParcial()));
                    if (liquidacionParcial == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una liquidacionParcial : " + liquidacionParcialTipo.getIdLiquidacionParcial()));
                    } else {
                        liquidacionParcial.setSancion(sancion);
                        liquidacionParcial.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                    }
                }
            }
        }
        if (sancionTipo.getAcciones() != null
                && !sancionTipo.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : sancionTipo.getAcciones()) {
                final Accion accion = mapper.map(accionTipo, Accion.class);
                accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = valorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        accion.setFecEjecucionAccion(DateUtil.parseStrDateTimeToCalendar(accionTipo.getFecEjecucionAccion()));
                        accion.setCodAccion(codAccion);
                        accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
                    }
                    SancionHasAccion sancionHasAccion = new SancionHasAccion();
                    sancionHasAccion.setAccion(accion);
                    sancionHasAccion.setSancion(sancion);
                    sancionHasAccion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
                    sancion.getAcciones().add(sancionHasAccion);
                }
            }
        }

        if (sancionTipo.getFecLlegadaExtemporaneaFiscalizacion() != null) {
            sancion.setFecLlegadaExtemporaneaFiscalizacion(sancionTipo.getFecLlegadaExtemporaneaFiscalizacion());
        }

        if (StringUtils.isNotBlank(sancionTipo.getCodDecisionExtemporaneaLiquidacion())) {
            final ValorDominio codDecisionExtemporaneaLiquidacion = valorDominioDao.find(sancionTipo.getCodDecisionExtemporaneaLiquidacion());
            if (codDecisionExtemporaneaLiquidacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un codDecisionExtemporaneaLiquidacion con el valor:" + sancionTipo.getCodDecisionExtemporaneaLiquidacion()));
            }
            sancion.setCodDecisionExtemporaneaLiquidacion(codDecisionExtemporaneaLiquidacion);
        }

        if (StringUtils.isNotBlank(sancionTipo.getDesObservacionDecisionExtemporanea())) {
            sancion.setDesObservacionDecisionExtemporanea(sancionTipo.getDesObservacionDecisionExtemporanea());
        }
    }

    /**
     * Método que valida las restricciones únicas y restricciones de negocio de
     * la tabla sancion
     *
     * @param sancion la sancion a crear/actualizar
     * @param sancionTipo la la sancion que contiene los datos a ingresar
     * @param errorTipoList la lista de errores que se almacenan en toda la
     * transacción
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     */
    private void checkBusinessSancion(final SancionTipo sancionTipo, final Sancion sancion,
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        //Debido a que en la especificación den SancionTipo, el objeto informacionRequerida y pruebaRequerida, 
        // es una lista de máximo 2 objetos, se definió que siempre que se quiera actualizar este valor, se deben enviar los
        //dos objetos ya que la implementación eliminará los registros que encuentre en la tabla intermedia para dejar
        //los nuevos que se envíen        
        if (sancionTipo.getInformacionRequerida() != null && !sancionTipo.getInformacionRequerida().isEmpty()) {
            if (sancion.getInformacionRequerida() != null && !sancion.getInformacionRequerida().isEmpty()) {
                for (SancionInformacionRequerida informacionRequerida : sancion.getInformacionRequerida()) {
                    informacionRequeridaDao.remove(informacionRequerida);
                }
                //Se limpia la lista de informacionRequerida de sancion
                sancion.getInformacionRequerida().clear();
            }
            for (InformacionPruebaTipo informacionPruebaTipo : sancionTipo.getInformacionRequerida()) {
                if (informacionPruebaTipo != null && StringUtils.isNotBlank(informacionPruebaTipo.getIdInformacionPrueba())) {
                    InformacionPrueba informacionPrueba = informacionPruebaDao.find(Long.valueOf(informacionPruebaTipo.getIdInformacionPrueba()));
                    if (informacionPrueba == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una informacionRequerida con el ID: " + informacionPruebaTipo.getIdInformacionPrueba()));
                    } else {
                        SancionInformacionRequerida informacionRequerida = new SancionInformacionRequerida();
                        informacionRequerida.setInformacionPrueba(informacionPrueba);
                        informacionRequerida.setSancion(sancion);
                        informacionRequerida.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        sancion.getInformacionRequerida().add(informacionRequerida);

                    }
                }
            }
        }

        if (sancionTipo.getPruebaRequerida() != null && !sancionTipo.getPruebaRequerida().isEmpty()) {
            if (sancion.getPruebaRequerida() != null && !sancion.getPruebaRequerida().isEmpty()) {
                for (SancionPruebaRequerida pruebaRequerida : sancion.getPruebaRequerida()) {
                    pruebaRequeridaDao.remove(pruebaRequerida);
                }
                //Se limpia la lista de pruebaRequerida de sancion
                sancion.getPruebaRequerida().clear();
            }
            for (InformacionPruebaTipo informacionPruebaTipo : sancionTipo.getPruebaRequerida()) {
                if (informacionPruebaTipo != null && StringUtils.isNotBlank(informacionPruebaTipo.getIdInformacionPrueba())) {
                    InformacionPrueba informacionPrueba = informacionPruebaDao.find(Long.valueOf(informacionPruebaTipo.getIdInformacionPrueba()));
                    if (informacionPrueba == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró una informacionRequerida con el ID: " + informacionPruebaTipo.getIdInformacionPrueba()));
                    } else {
                        SancionPruebaRequerida pruebaRequerida = new SancionPruebaRequerida();
                        pruebaRequerida.setInformacionPrueba(informacionPrueba);
                        pruebaRequerida.setSancion(sancion);
                        pruebaRequerida.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        sancion.getPruebaRequerida().add(pruebaRequerida);
                    }
                }
            }
        }
    }

    /**
     * Método utlilizado para realizar las respectivas validaciones cuando viene
     * el funcionario de la sancion y el funcionario del listado de las
     * informacion prueba dentro de la sancion
     *
     * @param sancionTipoList listado de sanciones encontradas
     * @param contextoTransaccionalTipo contiene los datos de auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void validarFuncionario(List<SancionTipo> sancionTipoList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (sancionTipoList != null) {
            for (final SancionTipo sancionTipo : sancionTipoList) {
                if (sancionTipo.getGestorSancion() != null
                        && StringUtils.isNotBlank(sancionTipo.getGestorSancion().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(sancionTipo.getGestorSancion(), contextoTransaccionalTipo);
                    sancionTipo.setGestorSancion(funcionarioTipo);
                }
                if (sancionTipo.getInformacionRequerida() != null && !sancionTipo.getInformacionRequerida().isEmpty()) {
                    for (final InformacionPruebaTipo informacionRequeridaTipo : sancionTipo.getInformacionRequerida()) {
                        if (informacionRequeridaTipo.getFuncionarioResuelveInterna() != null
                                && StringUtils.isNotBlank(informacionRequeridaTipo.getFuncionarioResuelveInterna().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(informacionRequeridaTipo.getFuncionarioResuelveInterna(), contextoTransaccionalTipo);
                            informacionRequeridaTipo.setFuncionarioResuelveInterna(funcionarioTipo);
                        }
                    }
                }

                if (sancionTipo.getPruebaRequerida() != null && !sancionTipo.getPruebaRequerida().isEmpty()) {
                    for (final InformacionPruebaTipo pruebaRequeridaTipo : sancionTipo.getPruebaRequerida()) {
                        if (pruebaRequeridaTipo.getFuncionarioResuelveInterna() != null
                                && StringUtils.isNotBlank(pruebaRequeridaTipo.getFuncionarioResuelveInterna().getIdFuncionario())) {
                            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(pruebaRequeridaTipo.getFuncionarioResuelveInterna(), contextoTransaccionalTipo);
                            pruebaRequeridaTipo.setFuncionarioResuelveInterna(funcionarioTipo);

                        }
                    }
                }
            }
        }

    }
}
