/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.MsjOpActualizarAnalisisSancionFallo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.MsjOpBuscarPorCriteriosAnalisisSancionFallo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.MsjOpBuscarPorIdAnalisisSancionFallo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.MsjOpCrearAnalisisSancionFallo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpActualizarAnalisisSancionRespTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpActualizarAnalisisSancionSolTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpBuscarPorCriteriosAnalisisSancionRespTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpBuscarPorCriteriosAnalisisSancionSolTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpBuscarPorIdAnalisisSancionRespTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpBuscarPorIdAnalisisSancionSolTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpCrearAnalisisSancionRespTipo;
import co.gov.ugpp.sancion.srvaplanalisissancion.v1.OpCrearAnalisisSancionSolTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vgomezva
 */
@WebService(serviceName = "SrvAplAnalisisSancion",
        portName = "portSrvAplAnalisisSancionSOAP",
        endpointInterface = "co.gov.ugpp.sancion.srvaplanalisissancion.v1.PortSrvAplAnalisisSancionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAnalisisSancion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSancion.class);
    @EJB
    private AnalisisSancionFacade analisisSancionFacade;

    public OpCrearAnalisisSancionRespTipo opCrearAnalisisSancion(OpCrearAnalisisSancionSolTipo msjOpCrearAnalisisSancionSol) throws MsjOpCrearAnalisisSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAnalisisSancionSol.getContextoTransaccional();
        final AnalisisSancionTipo analisisSancionTipo = msjOpCrearAnalisisSancionSol.getAnalisisSancion();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final Long analisisPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            analisisPK = analisisSancionFacade.crearAnalisisSancion(analisisSancionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearAnalisisSancionRespTipo resp = new OpCrearAnalisisSancionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdAnalisisSancion(analisisPK.toString());
        return resp;
    }

    public OpBuscarPorCriteriosAnalisisSancionRespTipo opBuscarPorCriteriosAnalisisSancion(OpBuscarPorCriteriosAnalisisSancionSolTipo msjOpBuscarPorCriteriosAnalisisSancionSol) throws MsjOpBuscarPorCriteriosAnalisisSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAnalisisSancionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosAnalisisSancionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AnalisisSancionTipo> sancionTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            sancionTiposPagerData = analisisSancionFacade.buscarPorCriterioAnalisisSancion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosAnalisisSancionRespTipo resp = new OpBuscarPorCriteriosAnalisisSancionRespTipo();
        cr.setValCantidadPaginas(sancionTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getAnalisisSancion().addAll(sancionTiposPagerData.getData());

        return resp;
    }

    public OpBuscarPorIdAnalisisSancionRespTipo opBuscarPorIdAnalisisSancion(OpBuscarPorIdAnalisisSancionSolTipo msjOpBuscarPorIdAnalisisSancionSol) throws MsjOpBuscarPorIdAnalisisSancionFallo {
        //TODO implement this method
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdAnalisisSancionSol.getContextoTransaccional();
        final List<String> idAnalisisSancion = msjOpBuscarPorIdAnalisisSancionSol.getIdAnalisisSancion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<AnalisisSancionTipo> analsisSancionTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            analsisSancionTipos = analisisSancionFacade.buscarPorIdAnalisisSancion(idAnalisisSancion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdAnalisisSancionRespTipo resp = new OpBuscarPorIdAnalisisSancionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getAnalisisSancion().addAll(analsisSancionTipos);

        return resp;
    }

    public OpActualizarAnalisisSancionRespTipo opActualizarAnalisisSancion(OpActualizarAnalisisSancionSolTipo msjOpActualizarAnalisisSancionSol) throws MsjOpActualizarAnalisisSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAnalisisSancionSol.getContextoTransaccional();
        final AnalisisSancionTipo analisisSancionTipo = msjOpActualizarAnalisisSancionSol.getAnalisisSancion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            analisisSancionFacade.actualizarAnalisisSancion(analisisSancionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAnalisisSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarAnalisisSancionRespTipo resp = new OpActualizarAnalisisSancionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

}
