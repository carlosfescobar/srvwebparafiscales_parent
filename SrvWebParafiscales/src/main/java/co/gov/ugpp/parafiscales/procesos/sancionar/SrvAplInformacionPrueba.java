package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.MsjOpActualizarInformacionPruebaFallo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.MsjOpBuscarPorCriteriosInformacionPruebaFallo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.MsjOpBuscarPorIdInformacionPruebaFallo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.MsjOpCrearInformacionPruebaFallo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpActualizarInformacionPruebaRespTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpActualizarInformacionPruebaSolTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpBuscarPorCriteriosInformacionPruebaRespTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpBuscarPorCriteriosInformacionPruebaSolTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpBuscarPorIdInformacionPruebaRespTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpBuscarPorIdInformacionPruebaSolTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpCrearInformacionPruebaRespTipo;
import co.gov.ugpp.sancion.srvaplinformacionprueba.v1.OpCrearInformacionPruebaSolTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplInformacionPrueba",
        portName = "portSrvAplInformacionPruebaSOAP",
        endpointInterface = "co.gov.ugpp.sancion.srvaplinformacionprueba.v1.PortSrvAplInformacionPruebaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplInformacionPrueba extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplInformacionPrueba.class);

    @EJB
    private InformacionPruebaFacade informacionPruebaFacade;

    public OpCrearInformacionPruebaRespTipo opCrearInformacionPrueba(OpCrearInformacionPruebaSolTipo msjOpCrearInformacionPruebaSol) throws MsjOpCrearInformacionPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearInformacionPruebaSol.getContextoTransaccional();
        final InformacionPruebaTipo informacionPruebaTipo = msjOpCrearInformacionPruebaSol.getInformacionPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long informacionPruebaPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            informacionPruebaPK = informacionPruebaFacade.crearInformacionPrueba(informacionPruebaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearInformacionPruebaRespTipo resp = new OpCrearInformacionPruebaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdInformacionPrueba(informacionPruebaPK.toString());

        return resp;
    }

    public OpBuscarPorCriteriosInformacionPruebaRespTipo opBuscarPorCriteriosInformacionPrueba(OpBuscarPorCriteriosInformacionPruebaSolTipo msjOpBuscarPorCriteriosInformacionPruebaSol) throws MsjOpBuscarPorCriteriosInformacionPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosInformacionPruebaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosInformacionPruebaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<InformacionPruebaTipo> informacionPruebaTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            informacionPruebaTiposPagerData = informacionPruebaFacade.buscarPorCriterioSancion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosInformacionPruebaRespTipo resp = new OpBuscarPorCriteriosInformacionPruebaRespTipo();
        cr.setValCantidadPaginas(informacionPruebaTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getInformacionPruebas().addAll(informacionPruebaTiposPagerData.getData());

        return resp;
    }

    public OpActualizarInformacionPruebaRespTipo opActualizarInformacionPrueba(OpActualizarInformacionPruebaSolTipo msjOpActualizarInformacionPruebaSol) throws MsjOpActualizarInformacionPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarInformacionPruebaSol.getContextoTransaccional();
        final InformacionPruebaTipo informacionPruebaTipo = msjOpActualizarInformacionPruebaSol.getInformacionPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            informacionPruebaFacade.actualizarInformacionPrueba(informacionPruebaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarInformacionPruebaRespTipo resp = new OpActualizarInformacionPruebaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdInformacionPruebaRespTipo opBuscarPorIdInformacionPrueba(OpBuscarPorIdInformacionPruebaSolTipo msjOpBuscarPorIdInformacionPruebaSolTipo) throws MsjOpBuscarPorIdInformacionPruebaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdInformacionPruebaSolTipo.getContextoTransaccional();
        final List<String> informacionPruebaTipos = msjOpBuscarPorIdInformacionPruebaSolTipo.getIdInformacionPrueba();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<InformacionPruebaTipo> informacionPruebaTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            informacionPruebaTipoList = informacionPruebaFacade.buscarPorIdInformacionPrueba(informacionPruebaTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInformacionPruebaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdInformacionPruebaRespTipo resp = new OpBuscarPorIdInformacionPruebaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getInformacionPrueba().addAll(informacionPruebaTipoList);

        return resp;
    }

}
