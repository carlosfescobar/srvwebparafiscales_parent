package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.sanciones.srvaplinvestigado.v1.MsjOpBuscarPorIdInvestigadoFallo;
import co.gov.ugpp.sanciones.srvaplinvestigado.v1.OpBuscarPorIdInvestigadoRespTipo;
import co.gov.ugpp.schema.sanciones.investigadotipo.v1.InvestigadoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplInvestigado",
    portName = "portSrvAplInvestigadoSOAP",
    endpointInterface = "co.gov.ugpp.sanciones.srvaplinvestigado.v1.PortSrvAplInvestigadoSOAP",
    targetNamespace = "http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplInvestigado extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplInvestigado.class);

    @EJB
    private InvestigadoFacade investigadoFacade;

    public co.gov.ugpp.sanciones.srvaplinvestigado.v1.OpBuscarPorIdInvestigadoRespTipo opBuscarPorIdInvestigado(co.gov.ugpp.sanciones.srvaplinvestigado.v1.OpBuscarPorIdInvestigadoSolTipo msjOpBuscarPorIdInvestigadoSol) throws MsjOpBuscarPorIdInvestigadoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdInvestigadoSol.getContextoTransaccional();
        final List<IdentificacionTipo> identificacionTipos = msjOpBuscarPorIdInvestigadoSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        

        final List<InvestigadoTipo> investigadoTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            investigadoTipos = investigadoFacade.buscarPorIdInvestigado(identificacionTipos,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInvestigadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInvestigadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdInvestigadoRespTipo resp = new OpBuscarPorIdInvestigadoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getInvestigado().addAll(investigadoTipos);

        return resp;
    }
}
