/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.MsjOpActualizarLiquidacionParcialFallo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.MsjOpBuscarPorCriteriosLiquidacionParcialFallo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.MsjOpBuscarPorIdLiquidacionParcialFallo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.MsjOpCrearLiquidacionParcialFallo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpActualizarLiquidacionParcialRespTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpActualizarLiquidacionParcialSolTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpBuscarPorCriteriosLiquidacionParcialRespTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpBuscarPorCriteriosLiquidacionParcialSolTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpBuscarPorIdLiquidacionParcialRespTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpBuscarPorIdLiquidacionParcialSolTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpCrearLiquidacionParcialRespTipo;
import co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.OpCrearLiquidacionParcialSolTipo;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplLiquidacionParcial",
        portName = "portSrvAplLiquidacionParcialSOAP",
        endpointInterface = "co.gov.ugpp.sancion.srvaplliquidacionparcial.v1.PortSrvAplLiquidacionParcialSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplLiquidacionParcial extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSancion.class);

    @EJB
    private LiquidacionParcialFacade liquidacionParcialFacade;

    public OpCrearLiquidacionParcialRespTipo opCrearLiquidacionParcial(OpCrearLiquidacionParcialSolTipo msjOpCrearLiquidacionParcialSol) throws MsjOpCrearLiquidacionParcialFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearLiquidacionParcialSol.getContextoTransaccional();
        final LiquidacionParcialTipo liquidacionParcialTipo = msjOpCrearLiquidacionParcialSol.getLiquidacionParcial();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long liquidacionParcialPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionParcialPK = liquidacionParcialFacade.crearLiquidacionParcial(liquidacionParcialTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearLiquidacionParcialRespTipo resp = new OpCrearLiquidacionParcialRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdLiquidacionParcial(liquidacionParcialPK.toString());

        return resp;
    }

    public OpBuscarPorCriteriosLiquidacionParcialRespTipo opBuscarPorCriteriosLiquidacionParcial(OpBuscarPorCriteriosLiquidacionParcialSolTipo msjOpBuscarPorCriteriosLiquidacionParcialSol) throws MsjOpBuscarPorCriteriosLiquidacionParcialFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosLiquidacionParcialSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosLiquidacionParcialSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<LiquidacionParcialTipo> liquidacionParcialTipoTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionParcialTipoTiposPagerData = liquidacionParcialFacade.buscarPorCriteriosLiquidacionParcial(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosLiquidacionParcialRespTipo resp = new OpBuscarPorCriteriosLiquidacionParcialRespTipo();
        cr.setValCantidadPaginas(liquidacionParcialTipoTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getLiquidacionParcial().addAll(liquidacionParcialTipoTiposPagerData.getData());
        return resp;

    }

    public OpBuscarPorIdLiquidacionParcialRespTipo opBuscarPorIdLiquidacionParcial(OpBuscarPorIdLiquidacionParcialSolTipo msjOpBuscarPorIdLiquidacionParcialSol) throws MsjOpBuscarPorIdLiquidacionParcialFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdLiquidacionParcialSol.getContextoTransaccional();
        final List<String> liquidacionParcialTipos = msjOpBuscarPorIdLiquidacionParcialSol.getIdLiquidacionParcial();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<LiquidacionParcialTipo> liquidacionParcialTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionParcialTipoList = liquidacionParcialFacade.buscarPorIdLiquidacionParcial(liquidacionParcialTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdLiquidacionParcialRespTipo resp = new OpBuscarPorIdLiquidacionParcialRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getLiquidacionParcial().addAll(liquidacionParcialTipoList);

        return resp;
    }

    public OpActualizarLiquidacionParcialRespTipo opActualizarLiquidacionParcial(OpActualizarLiquidacionParcialSolTipo msjOpActualizarLiquidacionParcialSol) throws MsjOpActualizarLiquidacionParcialFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarLiquidacionParcialSol.getContextoTransaccional();
        final LiquidacionParcialTipo liquidacionParcialTipo = msjOpActualizarLiquidacionParcialSol.getLiquidacionParcial();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            liquidacionParcialFacade.actualizarLiquidacionParcial(liquidacionParcialTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarLiquidacionParcialFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarLiquidacionParcialRespTipo resp = new OpActualizarLiquidacionParcialRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

}
