package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.sancion.srvaplsancion.v1.MsjOpActualizarSancionFallo;
import co.gov.ugpp.sancion.srvaplsancion.v1.MsjOpBuscarPorCriteriosSancionFallo;
import co.gov.ugpp.sancion.srvaplsancion.v1.MsjOpBuscarPorIdSancionFallo;
import co.gov.ugpp.sancion.srvaplsancion.v1.MsjOpCrearSancionFallo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpActualizarSancionRespTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpActualizarSancionSolTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpBuscarPorCriteriosSancionRespTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpBuscarPorCriteriosSancionSolTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpBuscarPorIdSancionRespTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpBuscarPorIdSancionSolTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpCrearSancionRespTipo;
import co.gov.ugpp.sancion.srvaplsancion.v1.OpCrearSancionSolTipo;
import co.gov.ugpp.schema.sancion.sanciontipo.v1.SancionTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplSancion",
        portName = "portSrvAplSancionSOAP",
        endpointInterface = "co.gov.ugpp.sancion.srvaplsancion.v1.PortSrvAplSancionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplSancion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSancion.class);

    @EJB
    private SancionFacade sancionFacade;

    public OpCrearSancionRespTipo opCrearSancion(OpCrearSancionSolTipo msjOpCrearSancionSol) throws MsjOpCrearSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSancionSol.getContextoTransaccional();
        final SancionTipo sancionTipo = msjOpCrearSancionSol.getSancion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long sancionPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            sancionPK = sancionFacade.crearSancion(sancionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearSancionRespTipo resp = new OpCrearSancionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSancion(sancionPK.toString());

        return resp;
    }

    public OpActualizarSancionRespTipo opActualizarSancion(OpActualizarSancionSolTipo msjOpActualizarSancionSol) throws MsjOpActualizarSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSancionSol.getContextoTransaccional();
        final SancionTipo sancionTipo = msjOpActualizarSancionSol.getSancion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            sancionFacade.actualizarSancion(sancionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarSancionRespTipo resp = new OpActualizarSancionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosSancionRespTipo opBuscarPorCriteriosSancion(OpBuscarPorCriteriosSancionSolTipo msjOpBuscarPorCriteriosSancionSol) throws MsjOpBuscarPorCriteriosSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosSancionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosSancionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SancionTipo> sancionTiposPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion()
            );
            sancionTiposPagerData = sancionFacade.buscarPorCriterioSancion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosSancionRespTipo resp = new OpBuscarPorCriteriosSancionRespTipo();
        cr.setValCantidadPaginas(sancionTiposPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSancion().addAll(sancionTiposPagerData.getData());

        return resp;
    }

    public OpBuscarPorIdSancionRespTipo opBuscarPorIdSancion(OpBuscarPorIdSancionSolTipo msjOpBuscarPorIdSancionSolTipo) throws MsjOpBuscarPorIdSancionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdSancionSolTipo.getContextoTransaccional();
        final List<String> sancionTipos = msjOpBuscarPorIdSancionSolTipo.getIdSancion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<SancionTipo> sancionTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            sancionTipoList = sancionFacade.buscarPorIdSancion(sancionTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSancionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdSancionRespTipo resp = new OpBuscarPorIdSancionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getSancion().addAll(sancionTipoList);

        return resp;
    }

}
