/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.sancion.srvapluvt.v1.MsjOpBuscarPorCriteriosUVTFallo;
import co.gov.ugpp.sancion.srvapluvt.v1.MsjOpConsultarUVTActualFallo;
import co.gov.ugpp.sancion.srvapluvt.v1.OpBuscarPorCriteriosUVTRespTipo;
import co.gov.ugpp.sancion.srvapluvt.v1.OpBuscarPorCriteriosUVTSolTipo;
import co.gov.ugpp.sancion.srvapluvt.v1.OpConsultarUVTActualRespTipo;
import co.gov.ugpp.sancion.srvapluvt.v1.OpConsultarUVTActualSolTipo;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@WebService(serviceName = "SrvAplUVT",
        portName = "portSrvAplUVTSOAP",
        endpointInterface = "co.gov.ugpp.sancion.srvapluvt.v1.PortSrvAplUVTSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplUVT extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplUVT.class);
    @EJB
    private UVTFacade UVTFacade;

    public OpBuscarPorCriteriosUVTRespTipo opBuscarPorCriteriosUVT(OpBuscarPorCriteriosUVTSolTipo msjOpBuscarPorCriteriosUVTSol) throws MsjOpBuscarPorCriteriosUVTFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosUVTSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosUVTSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<UVTTipo> uvtTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            uvtTipoPagerData = UVTFacade.buscarPorCriterioUVT(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosUVTFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosUVTFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);

        }
        final OpBuscarPorCriteriosUVTRespTipo resp = new OpBuscarPorCriteriosUVTRespTipo();
        cr.setValCantidadPaginas(uvtTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getUvt().addAll(uvtTipoPagerData.getData());

        return resp;
    }

    public OpConsultarUVTActualRespTipo opConsultarUVTActual(OpConsultarUVTActualSolTipo msjOconsulActualSolTipo) throws MsjOpConsultarUVTActualFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOconsulActualSolTipo.getContextoTransaccional();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final UVTTipo uVTTipo;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            uVTTipo = UVTFacade.consultarUVTActual(contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarUVTActualFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarUVTActualFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarUVTActualRespTipo resp = new OpConsultarUVTActualRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setUVT(uVTTipo);

        return resp;
    }

}
