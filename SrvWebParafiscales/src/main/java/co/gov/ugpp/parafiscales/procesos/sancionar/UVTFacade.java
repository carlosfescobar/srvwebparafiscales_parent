package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface UVTFacade extends Serializable {

    PagerData<UVTTipo> buscarPorCriterioUVT(final List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    UVTTipo consultarUVTActual(final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
