package co.gov.ugpp.parafiscales.procesos.sancionar;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.UVTDao;
import co.gov.ugpp.parafiscales.persistence.entity.UVT;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz UVTFacade que contiene las operaciones del
 * servicio SrvAplUVTFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class UVTFacadeImpl extends AbstractFacade implements UVTFacade {

    @EJB
    private UVTDao UVTDao;

    /**
     * implementación de la operación buscarPorCriterioSancion se encarga de
     * buscar un UVT_ en la base de datos por medio de los parametros enviados y
     * ordena la consulta de acuerdo a los criterios establecidos
     *
     * @param parametroTipoList lista de parametros por medio de los cuales se
     * busca una Sancion en la base de datos
     * @param criterioOrdenamientoTipos Atributos por los cuales se va a ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Litado de sacion encontrado que se retorna junto con el contexto
     * transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public PagerData<UVTTipo> buscarPorCriterioUVT(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<UVT> pagerDataEntity = UVTDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<UVTTipo> UVTTipoList = mapper.map(pagerDataEntity.getData(), UVT.class, UVTTipo.class);

        return new PagerData<UVTTipo>(UVTTipoList, pagerDataEntity.getNumPages());
    }

    /**
     *
     * @param contextoTransaccionalTipo contiene la informacion para almacenar
     * la audotiria
     * @return retorno de objeto UVTTipo
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa del sevicio.
     */
    @Override
    public UVTTipo consultarUVTActual(ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        UVT uvt = UVTDao.findByUVTtActual();
        UVTTipo uvtTipo = mapper.map(uvt, UVTTipo.class);

        return uvtTipo;
    }

}
