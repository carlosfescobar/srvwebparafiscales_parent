package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rpadilla
 */
public interface CotizanteFacade extends Serializable {

    List<CotizanteTipo> findCotizanteByIdentificacion(List<IdentificacionTipo> identificacionTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    Long crearCotizante(CotizanteTipo cotizanteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
