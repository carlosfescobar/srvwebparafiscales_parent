package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.CotizanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Cotizante;
import co.gov.ugpp.parafiscales.persistence.entity.CotizanteInformacionBasica;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz CotizanteFacade que contiene las operaciones
 * del servicio SrvAplCotizante
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class CotizanteFacadeImpl extends AbstractFacade implements CotizanteFacade {

    private static final Logger LOG = LoggerFactory.getLogger(CotizanteFacadeImpl.class);

    @EJB
    private CotizanteDao cotizanteDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private Populator populator;

    /**
     * implementación de la operación findCotizanteByIdentificacion esta se
     * encarga de buscar un listado de cotizantes de acuerdo al listado de
     * identificaciones enviado.
     *
     * @param identificacionTipoList Listado de identificaciones para realizar
     * la consulta de cotizantes a la base de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Listado de cotizantes que se retorna junto con el contexto
     * transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public List<CotizanteTipo> findCotizanteByIdentificacion(final List<IdentificacionTipo> identificacionTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTipoList == null || identificacionTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar una lista de identificaciones");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTipoList, IdentificacionTipo.class, Identificacion.class);

        final List<Cotizante> cotizanteList = cotizanteDao.findByIdentificacionList(identificacionList, ClasificacionPersonaEnum.COTIZANTE);

        final List<CotizanteTipo> cotizanteTipoList = mapper.map(cotizanteList, Cotizante.class, CotizanteTipo.class);

        return cotizanteTipoList;
    }

    /**
     * implementación de la operación crearCotizante esta se encarga de crear un
     * nuevo cotizante en la base de datos a partir del cotizanteTipo enviado
     *
     * @param cotizanteTipo objeto a partir del cual se va a crear un cotizante
     * @param contextoTransaccionalTipo Contiene la información que se va
     * almacenar en la auditoria
     * @return Cotizante creado que retorna junto al contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios.
     */
    @Override
    public Long crearCotizante(CotizanteTipo cotizanteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (cotizanteTipo == null
                || cotizanteTipo.getIdPersona() == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Identificacion identificacion;

        identificacion = mapper.map(cotizanteTipo.getIdPersona(), Identificacion.class);

        Cotizante cotizante;

        Persona persona = personaDao.findByIdentificacion(identificacion);

        if (persona != null) {
            cotizante = cotizanteDao.find(persona.getId());
            if (cotizante != null) {
                throw new AppException("Ya existe un Cotizante con la identificación: " + identificacion,
                        ErrorEnum.ERROR_ENTIDAD_EXISTENTE);
            }
        }
        cotizante = new Cotizante();
        Validator.checkPersonaNatural(cotizanteTipo, errorTipoList);
        persona = populator.populatePersonaFromPersonaNatural(cotizanteTipo,
                contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.COTIZANTE, errorTipoList);

        if (StringUtils.isNotBlank(cotizanteTipo.getDescObservacion())
                || cotizanteTipo.getDocCotizante() != null) {

            CotizanteInformacionBasica infoBasica = new CotizanteInformacionBasica();
            infoBasica.setDescObservacion(cotizanteTipo.getDescObservacion());
            infoBasica.setArchivo(mapper.map(cotizanteTipo.getDocCotizante(), Archivo.class));
            infoBasica.setCotizante(cotizante);
            infoBasica.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            cotizante.setInfoCotizante(infoBasica);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        persona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        personaDao.create(persona);
        cotizante.setPersona(persona);

        cotizante.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        final Long cotizantePK = cotizanteDao.create(cotizante);

        return cotizantePK;
    }
}
