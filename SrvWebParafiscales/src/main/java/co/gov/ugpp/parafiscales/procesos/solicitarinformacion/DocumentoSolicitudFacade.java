package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.solicitudinformacion.agrupaciondocumentosolicitudtipo.v1.AgrupacionDocumentoSolicitudTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface DocumentoSolicitudFacade extends Serializable {

    PagerData<AgrupacionDocumentoSolicitudTipo> buscarPorCriteriosDocumentoSolicitud(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void crearDocumentoSolicitud(AgrupacionDocumentoSolicitudTipo agrupacionDocumentoSolicitudTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
