package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudExpedienteDocumentoEcmDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.solicitudinformacion.agrupaciondocumentosolicitudtipo.v1.AgrupacionDocumentoSolicitudTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz DocumentoSolicitudFacade que contiene las
 * operaciones del servicio SrvAplDocumentoSolicitud
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class DocumentoSolicitudFacadeImpl extends AbstractFacade implements DocumentoSolicitudFacade {

    @EJB
    private SolicitudExpedienteDocumentoEcmDao solicitudExpedienteDocumentoEcmDao;

    @EJB
    private SolicitudDao solicitudDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private ExpedienteDocumentoDao expedienteDocumentoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * implementación de la operación buscarPorCriteriosDocumentoSolicitud esta
     * se encarga de buscar una solicitudInformacion por medio de los parametros
     * enviado y ordenar la consulta de acuerdo a unos criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar una solicitud
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de agrupaciones encontradas que se retorna junto con el
     * contexto respuesta
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<AgrupacionDocumentoSolicitudTipo> buscarPorCriteriosDocumentoSolicitud(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<SolicitudExpedienteDocumentoEcm> solicitudesExpedienteDocumentoPagerData = solicitudExpedienteDocumentoEcmDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<AgrupacionDocumentoSolicitudTipo> agrupacionDocumentoSolicitudTipoList = new ArrayList<AgrupacionDocumentoSolicitudTipo>();

        if (solicitudesExpedienteDocumentoPagerData != null
                && !solicitudesExpedienteDocumentoPagerData.getData().isEmpty()) {

            final AgrupacionDocumentoSolicitudTipo agrupacionExpedienteDocumentoTipo = new AgrupacionDocumentoSolicitudTipo();

            for (final SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumento : solicitudesExpedienteDocumentoPagerData.getData()) {
                final DocumentoTipo documentoTipo = mapper.map(solicitudExpedienteDocumento.getExpedienteDocumentoEcm().getDocumentoEcm(), DocumentoTipo.class);
                final SolicitudInformacionTipo solicitudInformacionTipo = mapper.map(solicitudExpedienteDocumento.getSolicitud(), SolicitudInformacionTipo.class);
                if (solicitudInformacionTipo.getSolicitante() != null
                        && StringUtils.isNotBlank(solicitudInformacionTipo.getSolicitante().getIdFuncionario())) {
                    final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudInformacionTipo.getSolicitante(), contextoTransaccionalTipo);
                    solicitudInformacionTipo.setSolicitante(funcionarioTipo);
                }
                if (!existeSolicitud(agrupacionExpedienteDocumentoTipo.getSolicitudes(), solicitudInformacionTipo)) {
                    agrupacionExpedienteDocumentoTipo.getSolicitudes().add(solicitudInformacionTipo);
                }
                agrupacionExpedienteDocumentoTipo.getDocumentos().add(documentoTipo);
            }
            agrupacionDocumentoSolicitudTipoList.add(agrupacionExpedienteDocumentoTipo);
        }

        return new PagerData(agrupacionDocumentoSolicitudTipoList, solicitudesExpedienteDocumentoPagerData.getNumPages());

    }

    /**
     * Implementación de la operación crearDocumentoSolicitud esta se encarga de
     * crear una nueva agrupacion de solicitud y documentos en la base de datos
     *
     * @param agrupacionDocumentoSolicitudTipo Objeto a partir de cual se va a
     * crear una nueva solicitudInformación en la base de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void crearDocumentoSolicitud(final AgrupacionDocumentoSolicitudTipo agrupacionDocumentoSolicitudTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (agrupacionDocumentoSolicitudTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        } else if (agrupacionDocumentoSolicitudTipo.getSolicitudes() == null
                || agrupacionDocumentoSolicitudTipo.getSolicitudes().isEmpty()) {
            throw new AppException("Debe el listado de solicitudes objeto a crear");
        } else if (agrupacionDocumentoSolicitudTipo.getDocumentos() == null
                || agrupacionDocumentoSolicitudTipo.getDocumentos().isEmpty()) {
            throw new AppException("Debe el listado de documentos objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        List<SolicitudExpedienteDocumentoEcm> documentosSolicitud = new ArrayList<SolicitudExpedienteDocumentoEcm>();

        this.populateDocumentoSolicitud(contextoTransaccionalTipo, agrupacionDocumentoSolicitudTipo, documentosSolicitud, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        if (!documentosSolicitud.isEmpty()) {
            for (SolicitudExpedienteDocumentoEcm documentoSolicitud : documentosSolicitud) {
                solicitudExpedienteDocumentoEcmDao.create(documentoSolicitud);
            }
        }
    }

    private void populateDocumentoSolicitud(final ContextoTransaccionalTipo contextoTransaccionalTipo, final AgrupacionDocumentoSolicitudTipo agrupacionDocumentoSolicitudTipo,
            List<SolicitudExpedienteDocumentoEcm> documentosSolicitud, final List<ErrorTipo> errorTipoList) throws AppException {

        for (SolicitudTipo solicitudTipo : agrupacionDocumentoSolicitudTipo.getSolicitudes()) {
            if (StringUtils.isNotBlank(solicitudTipo.getIdSolicitud())) {
                Solicitud solicitud = solicitudDao.find(Long.valueOf(solicitudTipo.getIdSolicitud()));
                if (solicitud == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No existe un IdSolicitud con el valor: " + solicitudTipo.getIdSolicitud()));
                } else {
                    for (DocumentoTipo documentoTipo : agrupacionDocumentoSolicitudTipo.getDocumentos()) {
                        if (StringUtils.isNotBlank(documentoTipo.getIdDocumento())) {
                            DocumentoEcm documentoEcm = documentoDao.find(documentoTipo.getIdDocumento());
                            if (documentoEcm == null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "No existe un Documento con el identificador: " + solicitudTipo.getIdSolicitud()));
                            } else {

                                ExpedienteDocumentoEcm expedienteDocumentoEcm
                                        = expedienteDocumentoDao.findDocumentoExpedienteByExpedienteAndDocumento(solicitud.getExpediente().getId(), documentoEcm.getId());

                                if (expedienteDocumentoEcm == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No existe relación entre el Expediente con el identificador: " + solicitud.getExpediente().getId()
                                            + " y el Documento con el identificador: " + documentoEcm.getId()));
                                } else {
                                    SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm
                                            = solicitudExpedienteDocumentoEcmDao.findBySolicitudAndExpedienteDocumento(solicitud, expedienteDocumentoEcm);

                                    if (solicitudExpedienteDocumentoEcm != null) {
                                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_ENTIDAD_EXISTENTE,
                                                "Ya existe la relación entre la Solicitud con el identificador: : " + solicitudTipo.getIdSolicitud()
                                                + ", el Expediente con el identificador: " + solicitud.getExpediente().getId()
                                                + " y el Documento con el identificador: " + documentoEcm.getId()));
                                    } else {
                                        //Se deja por defecto el codOrigen como anexo
                                        ValorDominio codOrigen = valorDominioDao.find(OrigenDocumentoEnum.SOLICITUD_DOCUMENTO_ANEXO.getCode());
                                        if (codOrigen == null) {
                                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                                    "No se encontró codOrigen con el ID: " + OrigenDocumentoEnum.SOLICITUD_DOCUMENTO_ANEXO.getCode()));
                                        } else {
                                            expedienteDocumentoEcm.setCodOrigen(codOrigen);
                                            expedienteDocumentoEcm.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                                        }
                                        solicitudExpedienteDocumentoEcm = new SolicitudExpedienteDocumentoEcm();
                                        solicitudExpedienteDocumentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                        solicitudExpedienteDocumentoEcm.setExpedienteDocumentoEcm(expedienteDocumentoEcm);
                                        solicitudExpedienteDocumentoEcm.setSolicitud(solicitud);
                                        documentosSolicitud.add(solicitudExpedienteDocumentoEcm);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Metódo que valida si una solicitud existe en un listado de solicitudes
     *
     * @param solicitudes el listado de solicitudes
     * @param solicitudNueva la solicitud que se intenta agregar
     * @return true si ya existe, false si no
     */
    private boolean existeSolicitud(final List<SolicitudInformacionTipo> solicitudes, final SolicitudInformacionTipo solicitudNueva) {

        if (solicitudes != null
                && !solicitudes.isEmpty()) {
            for (SolicitudTipo solicitud : solicitudes) {
                if (solicitud.getIdSolicitud().equals(solicitudNueva.getIdSolicitud())) {
                    return true;
                }
            }
        }
        return false;
    }
}
