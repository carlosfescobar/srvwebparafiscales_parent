package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.solicitarinformacion.efectividadtipo.v1.EfectividadTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface EfectividadFacade extends Serializable {

    EfectividadTipo consultarEfectividad(List<ParametroTipo> parametroTipoList, 
            EntidadExternaTipo entidadExternaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
}
