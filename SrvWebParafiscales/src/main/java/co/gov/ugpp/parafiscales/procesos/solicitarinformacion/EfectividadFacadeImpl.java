package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.enums.EfectividadResultEnum;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.EfectividadDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Efectividad;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.enums.SolicitudEntidadEstadoEnum;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.solicitarinformacion.efectividadtipo.v1.EfectividadTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz EfectividadFacade que contiene las operaciones del servicio SrvAplEfectividad
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class EfectividadFacadeImpl extends AbstractFacade implements EfectividadFacade {

    @EJB
    private SolicitudDao solicitudDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private EfectividadDao efectividadDao;

    /**
     * Implementación de la operación consultarEfectividad esta se encarga de consultar el estado de una efectividad de
     * acuerdo a los parametros enviados
     *
     * @param parametroTipoList Listado de parametros para realizar la consulta de Efectividades a la base de datos
     * @param entidadExternaTipo Objeto para consultar la entidadExterna a través de una identificación en la base de
     * datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return Resultado de la efectividad Cosultada se que retorna junto con contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public EfectividadTipo consultarEfectividad(final List<ParametroTipo> parametroTipoList,
            final EntidadExternaTipo entidadExternaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (entidadExternaTipo == null || StringUtils.isBlank(entidadExternaTipo.getIdPersona().getValNumeroIdentificacion())
                || StringUtils.isBlank(entidadExternaTipo.getIdPersona().getCodTipoIdentificacion())) {
            throw new AppException("Debe proporcionar el ID del objeto a buscar");
        }

        if (parametroTipoList == null
                || parametroTipoList.isEmpty()) {
            throw new AppException("La lista de parámetros no puede ser nulo o vacía");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final IdentificacionTipo identificacionTipo = entidadExternaTipo.getIdPersona();
        final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

        final EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

        if (entidadExterna == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No existe una Entidad Externa: " + identificacion));
        }

        Integer cantidadSolicitudes = 0;
        Float porcentajeMinimo = 0.0F;

        for (ParametroTipo parametroTipo : parametroTipoList) {
            if (parametroTipo.getIdLlave().equals(Constants.EFECTIVIDAD_CALCULO_CANTIDAD_SOLICITUDES)) {
                if (parametroTipo.getValValor() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "La cantidad de solicitudes ingresada no es válida"));
                } else {
                    try {
                        cantidadSolicitudes = Integer.valueOf(parametroTipo.getValValor());
                    } catch (NumberFormatException nfe) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                                "La cantidad de solicitudes ingresada no es válida: " + parametroTipo.getValValor()));
                    }
                }
            } else if (parametroTipo.getIdLlave().equals(Constants.EFECTIVIDAD_CALCULO_PROCENTAJE_EFECTIVIDAD)) {
                if (parametroTipo.getValValor() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "El porcentaje de efectividad ingresado no es válida"));
                } else {
                    try {
                        porcentajeMinimo = Float.valueOf(parametroTipo.getValValor());
                    } catch (NumberFormatException nfe) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                                "El porcentaje de efectividad ingresado no es válida: " + parametroTipo.getValValor()));
                    }
                }
            }
        }

        if (cantidadSolicitudes <= 0) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "La cantidad de solicitudes debe ser mayor a 0"));
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final List<Solicitud> solicitudList
                = solicitudDao.findSolicitudByEntidadExternaId(entidadExterna, cantidadSolicitudes);

        final EfectividadTipo efectividadResult = new EfectividadTipo();

        long cantidadResueltas = 0;

        for (Solicitud solicitud : solicitudList) {
            String vCodigoSolicitud = solicitud.getCodEstadoSolicitud().getId();
            if (SolicitudEntidadEstadoEnum.RESUELTA.getCode().equals(vCodigoSolicitud)) {
                cantidadResueltas++;
            }
        }

        efectividadResult.setCantidadSolicitudes(cantidadSolicitudes.toString());
        efectividadResult.setPorcentajeEfectividad(porcentajeMinimo.toString());

        final float resultadoAnalisis = ((float) cantidadResueltas / (float) cantidadSolicitudes) * 100;

        if (resultadoAnalisis >= porcentajeMinimo) {
            efectividadResult.setCodResultadoEfectividad(EfectividadResultEnum.EFECTIVO.getCode());
        } else {
            efectividadResult.setCodResultadoEfectividad(EfectividadResultEnum.NO_EFECTIVO.getCode());
        }

        this.persistEfectividad(efectividadResult, entidadExterna, contextoTransaccionalTipo, errorTipoList);

        return efectividadResult;
    }

    /**
     * Método que se encarga de crear una nueva efectividad en la base de datos
     *
     * @param efectividadTipo Objeto a partir del cual se va a crear una Efectividad en la base de datos
     * @param entidadExterna Objeto para consultar la entidadExterna en la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    private void persistEfectividad(final EfectividadTipo efectividadTipo, final EntidadExterna entidadExterna,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        final Efectividad efectividad = new Efectividad();

        final ValorDominio valorDominio = valorDominioDao.find(efectividadTipo.getCodResultadoEfectividad());
        if (valorDominio == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No existe un codResultadoEfectividad con el Valor:" + valorDominio));
        }

        efectividad.setCodResultado(valorDominio);
        efectividad.setEntidadExterna(entidadExterna);
        efectividad.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        efectividad.setValNumeroSolicitudes(Long.valueOf(efectividadTipo.getCantidadSolicitudes()));
        efectividad.setValProcentajeEfectividad(Float.valueOf(efectividadTipo.getPorcentajeEfectividad()));

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        efectividadDao.create(efectividad);
    }
}
