package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz KPISolictudInformacionFacade que contiene las
 * operaciones del servicio SrvAplKPISolicitudInformacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class KPISolicitudInformacionFacadeImpl extends AbstractFacade implements KPISolictudInformacionFacade {

    @EJB
    private SolicitudDao solicitudDao;
    
    
    @Override
    public PagerData<SolicitudInformacionTipo> consultarSolicitudKPI(List<ParametroTipo> parametroTipoList, final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        
        final PagerData<Solicitud> solicitudesPagerData = solicitudDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<SolicitudInformacionTipo> solicitudTipoList = new ArrayList<SolicitudInformacionTipo>();

        for (final Solicitud solicitud : solicitudesPagerData.getData()) {
            solicitud.setUseOnlySpecifiedFields(true);
            final SolicitudInformacionTipo solicitudTipo = mapper.map(solicitud, SolicitudInformacionTipo.class);
            solicitudTipoList.add(solicitudTipo);
        }
        return new PagerData(solicitudTipoList, solicitudesPagerData.getNumPages());
    }

}
