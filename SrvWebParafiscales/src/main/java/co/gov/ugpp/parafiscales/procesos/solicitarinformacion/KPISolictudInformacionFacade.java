package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rpadilla
 */
public interface KPISolictudInformacionFacade extends Serializable {

    PagerData<SolicitudInformacionTipo> consultarSolicitudKPI(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
