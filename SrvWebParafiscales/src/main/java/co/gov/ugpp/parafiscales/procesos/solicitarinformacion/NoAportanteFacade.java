package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import java.io.Serializable;

/**
 *
 * @author zrodriguez
 */
public interface NoAportanteFacade extends Serializable {

    NoAportanteTipo findNoAportanteByIdentificacion(IdentificacionTipo identificacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarNoAportante(NoAportanteTipo noAportanteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
