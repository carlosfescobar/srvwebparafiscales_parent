package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.dao.DominioDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ClasificacionPersona;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Dominio;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz NoAportanteFacade que contiene las operaciones
 * del servicio SrvAplNoAportante
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class NoAportanteFacadeImpl extends AbstractFacade implements NoAportanteFacade {

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private MunicipioDao municipioDao;

    @EJB
    private Populator populator;

    @EJB
    private DominioDao dominioDao;

    /**
     * implementación de la operación findNoAportanteByIdentificacion esta se
     * encarga de consultar un objeto noAportante de acuerdo a una
     * identificacion enviada
     *
     * @param identificacionTipo Obejto para consultar un NoAportante con una
     * identificación en la base de datos
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Objeto NoAportante encontrado que se retorna junto con el
     * contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public NoAportanteTipo findNoAportanteByIdentificacion(final IdentificacionTipo identificacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTipo == null || StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion())
                || StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion())) {
            throw new AppException("Debe proporcionar los datos de identificacion de la persona");
        }

        final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

        final Persona persona = personaDao.findByIdentificacion(identificacion);

        final NoAportanteTipo noAportanteTipo = mapper.map(persona, NoAportanteTipo.class);

        return noAportanteTipo;
    }

    /**
     * implementación de la operación actualizarNoAportante esta se encarga de
     * actualizar un noAportante encontrado en la base de datos por medio de una
     * identificación enviada
     *
     * @param noAportanteTipo Objeto por medio del cual se actualiza un
     * noAportante
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void actualizarNoAportante(final NoAportanteTipo noAportanteTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (noAportanteTipo == null || noAportanteTipo.getIdPersona() == null
                || StringUtils.isBlank(noAportanteTipo.getIdPersona().getValNumeroIdentificacion())
                || StringUtils.isBlank(noAportanteTipo.getIdPersona().getCodTipoIdentificacion())) {
            throw new AppException("Debe proporcionar los datos de identificacion de la persona");
        }

        final Identificacion identificacion = mapper.map(noAportanteTipo.getIdPersona(), Identificacion.class);

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Persona persona = personaDao.findByIdentificacion(identificacion);

        if (persona == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No existe un NoAportante con la Identificacion: " + identificacion));
            throw new AppException(errorTipoList);
        }

        persona.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateNoAportante(noAportanteTipo, persona, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        personaDao.edit(persona);
    }

    private void populateNoAportante(final NoAportanteTipo noAportanteTipo, final Persona persona,
            final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        MunicipioTipo municipioTipo = noAportanteTipo.getIdPersona().getMunicipioExpedicion();

        if (municipioTipo != null
                && StringUtils.isNotBlank(municipioTipo.getCodMunicipio())) {
            Municipio municipio = municipioDao.findByCodigo(municipioTipo.getCodMunicipio());
            if (municipio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró municipio con el valor:" + municipioTipo.getCodMunicipio()));
            } else {
                persona.setMunicipio(municipio);
            }
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getValPrimerNombre())) {
            persona.setValPrimerNombre(noAportanteTipo.getValPrimerNombre());
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getValSegundoNombre())) {
            persona.setValSegundoNombre(noAportanteTipo.getValSegundoNombre());
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getValPrimerApellido())) {
            persona.setValPrimerApellido(noAportanteTipo.getValPrimerApellido());
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getValSegundoApellido())) {
            persona.setValSegundoApellido(noAportanteTipo.getValSegundoApellido());
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getCodSexo())) {
            ValorDominio valorDominio = valorDominioDao.find(noAportanteTipo.getCodSexo());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un CodSexo con el Valor :" + noAportanteTipo.getCodSexo()));
            } else {
                persona.setCodSexo(valorDominio);
            }
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getCodEstadoCivil())) {
            ValorDominio valorDominio = valorDominioDao.find(noAportanteTipo.getCodEstadoCivil());

            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un CodEstadoCivil con el Valor :" + noAportanteTipo.getCodEstadoCivil()));
            } else {
                persona.setCodEstadoCivil(valorDominio);
            }
        }

        if (StringUtils.isNotBlank(noAportanteTipo.getCodNivelEducativo())) {
            ValorDominio valorDominio = valorDominioDao.find(noAportanteTipo.getCodNivelEducativo());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un CodNivelEducativo con el Valor :" + noAportanteTipo.getCodNivelEducativo()));
            } else {
                persona.setCodNivelEducativo(valorDominio);
            }
        }

        if (noAportanteTipo.getIdAbogado() != null) {
            IdentificacionTipo identificacionAbogado = noAportanteTipo.getIdAbogado();

            if (StringUtils.isNotBlank(identificacionAbogado.getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(identificacionAbogado.getValNumeroIdentificacion())) {

                Identificacion identificacion = mapper.map(identificacionAbogado, Identificacion.class);
                Persona abogado = personaDao.findByIdentificacion(identificacion);

                if (persona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No existe un Abogado con la Identificacion :" + identificacion));
                } else {
                    persona.setAbogado(abogado);
                }
            }
        }

        if (noAportanteTipo.getIdAutorizado() != null) {
            IdentificacionTipo identificacionAutorizado = noAportanteTipo.getIdAutorizado();

            if (StringUtils.isNotBlank(identificacionAutorizado.getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(identificacionAutorizado.getValNumeroIdentificacion())) {

                Identificacion identificacion = mapper.map(identificacionAutorizado, Identificacion.class);
                Persona autorizado = personaDao.findByIdentificacion(identificacion);

                if (persona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No existe un Autorizado con la Identificacion :" + identificacion));
                } else {
                    persona.setAutorizado(autorizado);
                }
            }
        }

        if (noAportanteTipo.getContacto() != null) {
            ContactoPersona contactoPersona = populator.populateContactoPersona(noAportanteTipo.getContacto(),
                    persona.getContacto(), persona, contextoTransaccionalTipo, errorTipoList);
            persona.setContacto(contactoPersona);
        }

        //Se coloca la nueva clasificacion persona
        if (StringUtils.isNotBlank(noAportanteTipo.getCodTipoPersonaNatural())) {
            ValorDominio codTipoPersona = valorDominioDao.find(noAportanteTipo.getCodTipoPersonaNatural());

            if (codTipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoPersona con el ID: " + noAportanteTipo.getCodTipoPersonaNatural()));
            } else {

                Dominio codClasificacionPersona = dominioDao.find(ClasificacionPersonaEnum.PERSONA_NATURAL.getCode());

                if (codClasificacionPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró la clasificación de la persona con el ID: " + ClasificacionPersonaEnum.PERSONA_NATURAL.getCode()));
                } else {
                    ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                    clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                    clasificacionNuevaPersona.setPersona(persona);
                    clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                    persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                }
            }
        }
    }

}
