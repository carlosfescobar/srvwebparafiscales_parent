package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface RespuestaInformacionFacade extends Serializable {

    void validarRespuestaInformacion(FormatoEstructuraTipo formatoEstructura, IdentificacionTipo identificacion,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void almacenarRespuestaInformacion(EnvioTipo envioTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SolicitudInformacionTipo> cruzarRespuestaInformacion(RadicacionTipo radicacionTipo,
            FormatoEstructuraTipo formatoEstructuraTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
