package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.FormatoEnum;
import co.gov.ugpp.parafiscales.enums.SolicitudEntidadEstadoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.EnvioInformacionExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaDefinitivaDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaTemporalDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaPagoDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaPersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaPlanillaDao;
import co.gov.ugpp.parafiscales.persistence.dao.RespuestaSolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionFormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoArchivoPK;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaDefiniti;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPago;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPersona;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaPlanilla;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaSolicitud;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudCotizante;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPlanilla;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionFormato;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.ExcelUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementación de interfaz RespuestaInformacionFacade que contiene las
 * operaciones del servicio SrvAplRespuestaInformacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RespuestaInformacionFacadeImpl extends AbstractFacade implements RespuestaInformacionFacade {

    private static final Logger LOG = LoggerFactory.getLogger(RespuestaInformacionFacadeImpl.class);

    @EJB
    private InformacionExternaTemporalDao informacionExternaTemporalDao;

    @EJB
    private InformacionExternaDefinitivaDao informacionExternaDefinitivaDao;

    @EJB
    private EnvioInformacionExternaDao envioInformacionExternaDao;

    @EJB
    private SolicitudDao solicitudDao;

    @EJB
    private RespuestaSolicitudDao respuestaSolicitudDao;

    @EJB
    private RespuestaPersonaDao respuestaPersonaDao;

    @EJB
    private RespuestaPagoDao respuestaPagoDao;

    @EJB
    private RespuestaPlanillaDao respuestaPlanillaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FormatoDao FormatoDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private ValidacionFormatoDao validacionFormatoDao;

    //Contiene la declaración de las diferentes implementaciones para las validaciones de acuerdo con los formatos
    private ValidacionFormatoFacade validacionFormatoFacade;

    /**
     * Implementación de la operación validarRespuestaInformación, esta se
     * encarga de procesar la información enviada en el objeto formatoEstructura
     * para una entidadExterna, a traves del formato y versión se validan los
     * encabezados de los archivos y se almacena esta como información temporal.
     *
     * @param formatoEstructuraTipo contiene el formato y el archivo a validar y
     * procesar
     * @param identificacionTipo la identificación de la entidad externa
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public void validarRespuestaInformacion(FormatoEstructuraTipo formatoEstructuraTipo,
            IdentificacionTipo identificacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        List<ErrorTipo> erroresTipo = new ArrayList<ErrorTipo>();

        try {

            Workbook wb = WorkbookFactory.create(new ByteArrayInputStream(formatoEstructuraTipo.getArchivo().getValContenidoArchivo()));

            LOG.debug("Validando la estructura del archivo ", formatoEstructuraTipo.getArchivo().getValNombreArchivo());

            if (wb == null) {
                throw new AppException("No se ha procesado el archivo enviado ya que no contiene datos");
            }

            ValidacionFormato validacionFormato = null;

            if (formatoEstructuraTipo.getFormato() != null) {
                Long idFormato = formatoEstructuraTipo.getFormato().getIdFormato();
                Long idVersion = formatoEstructuraTipo.getFormato().getValVersion().longValue();
                FormatoPK formatoPK = new FormatoPK(idFormato, idVersion);
                validacionFormato = validacionFormatoDao.findByFormato(formatoPK);
                if (validacionFormato == null) {
                    throw new AppException("No se encontró una validacionFormato con el idFormato: " + idFormato.toString()
                            + " y con el idVersion: " + idVersion.toString());
                }
            }
            try {
                //A través del classLoader se obtiene la implementación para esta interfaz, el objeto validacionFormato contiene el nombre 
                //de la clase que se debe usar.
                validacionFormatoFacade = (ValidacionFormatoFacade) this.obtenerInstanciaValidacionFormato(validacionFormato).newInstance();
            } catch (AppException e) {
                throw new AppException("No se ha podido obtener la validacionFormato", e);
            } catch (IllegalAccessException e) {
                throw new AppException("No se ha podido obtener la validacionFormato", e);
            } catch (InstantiationException e) {
                throw new AppException("No se ha podido obtener la validacionFormato", e);
            }

            LOG.debug("Obteniendo información previa con el nombreArchivo y entidadExterna", formatoEstructuraTipo.getArchivo().getValNombreArchivo());
            InformacionExternaTemporal informacionExternaTemporal = informacionExternaTemporalDao.findByArchivoAndEntidadExterna(formatoEstructuraTipo.getArchivo(), identificacionTipo);
            if (informacionExternaTemporal != null) {
                throw new AppException("Ya ha sido almacenada la información para la entidadExterna : valNumeroIdentificacion:"
                        + identificacionTipo.getValNumeroIdentificacion() + " codTipoIdentificacion: " + identificacionTipo.getCodTipoIdentificacion()
                        + " y el archivo con nombre:" + formatoEstructuraTipo.getArchivo().getValNombreArchivo());
            }

            validacionFormatoFacade.validarEstructura(wb, erroresTipo);

            LOG.debug("Almacenado a informacion temporal para el archivo ", formatoEstructuraTipo.getArchivo().getValNombreArchivo());
            informacionExternaTemporal = new InformacionExternaTemporal();
            if (StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())) {
                ValorDominio valorDominio = valorDominioDao.find(identificacionTipo.getCodTipoIdentificacion());
                if (valorDominio == null) {
                    erroresTipo.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontrò un codTipoIdentificacion con el ID: " + identificacionTipo.getCodTipoIdentificacion()));
                } else {
                    informacionExternaTemporal.setCodTipoIdentificacion(valorDominio);
                }
            }
            informacionExternaTemporal.setNumeroDocumento(identificacionTipo.getValNumeroIdentificacion());
            informacionExternaTemporal.setFormatoArchivoPK(new FormatoArchivoPK(Long.valueOf(formatoEstructuraTipo.getFormato().getIdFormato().toString()), Long.valueOf(formatoEstructuraTipo.getFormato().getValVersion().toString())));
            informacionExternaTemporal.setValContenidoArchivo(formatoEstructuraTipo.getArchivo().getValContenidoArchivo());
            informacionExternaTemporal.setValNombreArchivo(formatoEstructuraTipo.getArchivo().getValNombreArchivo());
            informacionExternaTemporal.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            informacionExternaTemporal.setValContenidoFirma(formatoEstructuraTipo.getArchivo().getValContenidoFirma());
            informacionExternaTemporal.setFecColocacion(DateUtil.currentCalendar());
            informacionExternaTemporal.setIdUsuarioColocacion(contextoTransaccionalTipo.getIdUsuario());

            if (!erroresTipo.isEmpty()) {
                LOG.debug("Se encontraron errores de validación en el archivo ", formatoEstructuraTipo.getArchivo().getValNombreArchivo());
                throw new AppException(erroresTipo);
            }

            informacionExternaTemporalDao.create(informacionExternaTemporal);

        } catch (RuntimeException ex) {

            ErrorTipo errorTipo = new ErrorTipo();
            errorTipo.setCodError(ErrorEnum.ERROR_NO_ESPERADO.getCode());
            errorTipo.setValDescError("Se presentaron erroes cargando el archivo");
            errorTipo.setValDescErrorTecnico("No se ha podido procesar el archivo enviado. Asegúrese que el formato sea de tipo 'Excel' y que la estructura corresponda al formato y versión enviados.");
            erroresTipo.add(errorTipo);
            throw new AppException(erroresTipo);

        } catch (IOException ex) {
            LOG.error(ex.getMessage(), ex);
            ErrorTipo errorTipo = new ErrorTipo();
            errorTipo.setCodError(ErrorEnum.ERROR_NO_ESPERADO.getCode());
            errorTipo.setValDescError("No se ha podido procesar el archivo enviado. Asegúrese que el formato sea de tipo 'Excel' y que la estructura corresponda al formato y versión enviados.");
            errorTipo.setValDescErrorTecnico(ex.getMessage());
            erroresTipo.add(errorTipo);
            throw new AppException(erroresTipo);
        } catch (InvalidFormatException ex) {
            LOG.error(ex.getMessage(), ex);
            ErrorTipo errorTipo = new ErrorTipo();
            errorTipo.setCodError(ErrorEnum.ERROR_NO_ESPERADO.getCode());
            errorTipo.setValDescError("El formato del archivo de Excel no es correcto");
            errorTipo.setValDescErrorTecnico(ex.getMessage());
            erroresTipo.add(errorTipo);
            throw new AppException(erroresTipo);
        }
    }

    /**
     * Implementación de la operación validarRespuestaInformación, esta se
     * encarga de procesar los arhivos temporales cargados para el formato,
     * versión y entidadExterna enviados, se toma la información encontrada como
     * definitiva y se elimina de temoral.
     *
     * @param envioTipo contiene los objetos encapsulados necesarios para
     * ejecutar la operación (FormatoTipo, RadicacionTipo, IdentificacionTipo).
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public void almacenarRespuestaInformacion(EnvioTipo envioTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final RadicacionTipo radicacionTipo = envioTipo.getRadicacion();
        final IdentificacionTipo identificacionTipo = envioTipo.getIdentificacion();
        final FormatoTipo formatoTipo = envioTipo.getFormato();
        final List<String> nombreArchivos = this.obtenerNombreArchivos(envioTipo.getArchivos());
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkRadiacion(radicacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        List<InformacionExternaTemporal> informacionesExternaTemporal
                = informacionExternaTemporalDao.findPorCriterios(identificacionTipo,
                        formatoTipo, nombreArchivos);

        if (informacionesExternaTemporal == null
                || informacionesExternaTemporal.isEmpty()) {
            ErrorTipo errorTipo = new ErrorTipo();
            errorTipo.setCodError(ErrorEnum.ERROR_NO_ESPERADO.getCode());
            errorTipo.setValDescError(ErrorEnum.ERROR_NO_ESPERADO.getMessage());
            errorTipo.setValDescErrorTecnico("No se encontró información para procesar con los filtros de búsqueda ingresados");
            errorTipoList.add(errorTipo);
            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
        }

        EnvioInformacionExterna envioInformacionExterna
                = envioInformacionExternaDao.find(radicacionTipo.getIdRadicadoEntrada());

        if (envioInformacionExterna != null) {
            throw new AppException("Ya existe un envioInformacionExterna con el idRadicado: " + radicacionTipo.getIdRadicadoEntrada());
        }
        envioInformacionExterna = new EnvioInformacionExterna();
        envioInformacionExterna.setIdRadicadoEnvio(radicacionTipo.getIdRadicadoEntrada());
        envioInformacionExterna.setIdUsuarioColocacion(contextoTransaccionalTipo.getIdUsuario());
        envioInformacionExterna.setCodTipoIdentificacion(identificacionTipo.getCodTipoIdentificacion());
        envioInformacionExterna.setValNumeroIdentificacion(identificacionTipo.getValNumeroIdentificacion());
        envioInformacionExterna.setIdRadicadoRequerimiento(radicacionTipo.getIdRadicadoSalida());
        envioInformacionExterna.setFecRadicado(radicacionTipo.getFecRadicadoEntrada());
        envioInformacionExterna.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        if (StringUtils.isNotBlank(envioTipo.getCodSolicitud())) {
            ValorDominio valorDominio = valorDominioDao.find(envioTipo.getCodSolicitud());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontrò un codTipoSolicitud con el ID: " + envioTipo.getCodSolicitud()));
            } else {
                envioInformacionExterna.setCodTipoSolicitud(valorDominio);
            }
        }
        if (StringUtils.isNotBlank(envioTipo.getCodEstado())) {
            ValorDominio valorDominio = valorDominioDao.find(envioTipo.getCodEstado());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontrò un codEstado con el ID: " + envioTipo.getCodEstado()));
            } else {
                envioInformacionExterna.setCodEstado(valorDominio);
            }
        }
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        envioInformacionExternaDao.create(envioInformacionExterna);

        for (InformacionExternaTemporal informacionExternaTemporal : informacionesExternaTemporal) {
            InformacionExternaDefiniti informacionExternaDefiniti = new InformacionExternaDefiniti();
            informacionExternaDefiniti.setFecColocacion(informacionExternaTemporal.getFecColocacion());
            informacionExternaDefiniti.setValContenidoArchivo(informacionExternaTemporal.getValContenidoArchivo());
            informacionExternaDefiniti.setValContenidoFirma(informacionExternaTemporal.getValContenidoFirma());
            informacionExternaDefiniti.setValNombreArchivo(informacionExternaTemporal.getValNombreArchivo());
            informacionExternaDefiniti.setIdUsuarioColocacion(informacionExternaTemporal.getIdUsuarioColocacion());
            informacionExternaDefiniti.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            informacionExternaDefiniti.setEnvioInformacionExterna(envioInformacionExterna);

            if (formatoTipo.getIdFormato() != null
                    && formatoTipo.getValVersion() != null) {

                Formato formato = FormatoDao.findFormatoByFormatoAndVersion(formatoTipo.getIdFormato(),
                        formatoTipo.getValVersion().longValue());
                if (formato == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un Formato con el idFormato y idVersion:"
                            + formatoTipo.getIdFormato().toString() + " "
                            + formatoTipo.getValVersion().toString()));
                } else {
                    envioInformacionExterna.setFormato(formato);
                }
            }
            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            informacionExternaDefinitivaDao.create(informacionExternaDefiniti);
            informacionExternaTemporalDao.remove(informacionExternaTemporal);
        }
    }

    /**
     * Implementación de la operación cruzarRespuestaInformacion, esta se
     * encarga de validar la correspondencia de identificaciones cargadas con
     * las identificaciones que tiene la solicitud.
     *
     * @param radicacionTipo contiene el radicado de salida con el que se buscan
     * las solicitudes y el radicado de entrada que pertenece al envío
     * información
     * @param formatoEstructuraTipo Contiene el formato y la versión a cruzar.
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @return El listado de solicitudes encontradas y procesadas
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    @Override
    public List<SolicitudInformacionTipo> cruzarRespuestaInformacion(RadicacionTipo radicacionTipo, FormatoEstructuraTipo formatoEstructuraTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final Long idFormato = formatoEstructuraTipo.getFormato().getIdFormato();
        final Long idVersion = formatoEstructuraTipo.getFormato().getValVersion().longValue();
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        List<SolicitudInformacionTipo> solicitudesSolicitudInformacionTipoList = null;
        Map<String, List<String>> identificacionesPlanillas = null;

        try {
            Validator.checkRadiacion(radicacionTipo, errorTipoList);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            final List<Solicitud> solicitudesByRadicado = getSolicitudesByRadicado(radicacionTipo.getIdRadicadoSalida());

            if (solicitudesByRadicado == null
                    || solicitudesByRadicado.isEmpty()) {
                throw new AppException("No se encontraron solicitudes pendientes con el idRadicado: " + radicacionTipo.getIdRadicadoSalida());
            }

            //Se dejan ùnicamente las solicitudes que no estàn resueltas
            final List<Solicitud> solicitudesPendientes = validarSolicitudesPendientes(solicitudesByRadicado);

            if (solicitudesPendientes == null
                    || solicitudesPendientes.isEmpty()) {
                throw new AppException("No se encontraron solicitudes pendientes con el idRadicado: " + radicacionTipo.getIdRadicadoSalida());
            }

            final List<InformacionExternaDefiniti> informacionExtrernaDefinitiList = informacionExternaDefinitivaDao.findByRadicado(radicacionTipo.getIdRadicadoEntrada());
            final EnvioInformacionExterna envioInformacionExterna = envioInformacionExternaDao.find(radicacionTipo.getIdRadicadoEntrada());

            if (envioInformacionExterna == null) {
                throw new AppException("No se encontró un envioInformacionExterna con el idRadicado: " + radicacionTipo.getIdRadicadoEntrada());
            }

            final List<Identificacion> identificacionesRespondidas = new ArrayList<Identificacion>();

            RespuestaSolicitud respuestaSolicitud = new RespuestaSolicitud();
            respuestaSolicitud.setIdRadicado(envioInformacionExterna.getId());
            respuestaSolicitud.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            final Long idRespuestaSolicitud = respuestaSolicitudDao.create(respuestaSolicitud);

            for (InformacionExternaDefiniti informacionExtrernaDefiniti : informacionExtrernaDefinitiList) {
                if (informacionExtrernaDefiniti.getValContenidoArchivo() == null) {
                    throw new AppException("No se ha podido ejecutar la operación de cruces", ErrorEnum.ERROR_NO_ESPERADO);
                }
                InputStream inputStream = new ByteArrayInputStream(informacionExtrernaDefiniti.getValContenidoArchivo());
                org.apache.poi.ss.usermodel.Workbook wbDefinitivo = WorkbookFactory.create(inputStream);

                if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getFormato())
                        && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getVersion())) {
                    identificacionesPlanillas = new HashMap<String, List<String>>();
                    this.cargarRespuestaPlanillaPila(wbDefinitivo, idRespuestaSolicitud, contextoTransaccionalTipo, identificacionesRespondidas, identificacionesPlanillas);
                } else {
                    this.cargarRespuesta(wbDefinitivo, formatoEstructuraTipo, idRespuestaSolicitud, contextoTransaccionalTipo, identificacionesRespondidas);
                }
            }

            Map<Solicitud, List<Identificacion>> identificacionesSolicitud
                    = this.obtenerIdentificacionesxSolicitud(solicitudesPendientes);

            solicitudesSolicitudInformacionTipoList = this.cruzarIdentificaciones(identificacionesSolicitud,
                    identificacionesRespondidas, radicacionTipo, formatoEstructuraTipo.getFormato(),
                    envioInformacionExterna, contextoTransaccionalTipo, identificacionesPlanillas);

        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(RespuestaInformacionFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidFormatException ife) {
            java.util.logging.Logger.getLogger(RespuestaInformacionFacadeImpl.class.getName()).log(Level.SEVERE, null, ife);
        }
        return solicitudesSolicitudInformacionTipoList;
    }

    /**
     * Método que obtiene las identificaciones por cada solicitud, se itera cada
     * una de las solicitudes enviadas y se obtiene sus identificaciones
     * asociadas.
     *
     * @param solicitudList la lista de solicitudes a procesar
     * @return la correspondencia de solicitud y su listado de identificaciones
     * asociado
     */
    private Map<Solicitud, List<Identificacion>> obtenerIdentificacionesxSolicitud(final List<Solicitud> solicitudList) {

        final Map<Solicitud, List<Identificacion>> identificacionesxSolicitud = new HashMap<Solicitud, List<Identificacion>>();
        for (Solicitud solicitud : solicitudList) {

            List<Identificacion> identificacionesSolicitud = new ArrayList<Identificacion>();
            if (solicitud.getAportante() != null) {
                Identificacion identificacion = new Identificacion();
                identificacion.setCodTipoIdentificacion(solicitud.getAportante().getPersona().getCodTipoIdentificacion().getNombre());
                identificacion.setValNumeroIdentificacion(solicitud.getAportante().getPersona().getValNumeroIdentificacion());
                identificacionesSolicitud.add(identificacion);
            }
            if (solicitud.getNoAportante() != null) {
                Identificacion identificacion = new Identificacion();
                identificacion.setCodTipoIdentificacion(solicitud.getNoAportante().getCodTipoIdentificacion().getNombre());
                identificacion.setValNumeroIdentificacion(solicitud.getNoAportante().getValNumeroIdentificacion());
                identificacionesSolicitud.add(identificacion);
            }
            if (!solicitud.getSoliciutdCotizanteList().isEmpty()) {
                for (SolicitudCotizante solicitudCotizante : solicitud.getSoliciutdCotizanteList()) {
                    Identificacion identificacion = new Identificacion();
                    identificacion.setCodTipoIdentificacion(solicitudCotizante.getCotizante().getPersona().getCodTipoIdentificacion().getNombre());
                    identificacion.setValNumeroIdentificacion(solicitudCotizante.getCotizante().getPersona().getValNumeroIdentificacion());
                    identificacionesSolicitud.add(identificacion);
                }
            }
            identificacionesxSolicitud.put(solicitud, identificacionesSolicitud);
        }
        return identificacionesxSolicitud;
    }

    /**
     * Método que valida la cantidad de identificaciones que se esperan contra
     * la cantidad de identificaciones cargadas para cada solicitud
     *
     * @param identificacionesxSolicitud contiene las solicitudes y sus
     * identificaciones a validar
     * @param identificacionesEncontradas las identificaciones que se han
     * cargado desde la operación de validarRespuestaInformacion
     * @param radicacionTipo contiene el radicado de entrada con el que se
     * actualiza la solicitud
     * @param formatoTipo contiene el formato y la versión a validar
     * @param envioInformacionExterna el envioInformacionExterna con el que se
     * actualiza la solicitud
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @return el listado de solicitudesTipo actualizadas que se procesaron
     * convertidas a dto's para devolverlas al servicio y retornarlas junto con
     * el contexto respuesta
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private List<SolicitudInformacionTipo> cruzarIdentificaciones(final Map<Solicitud, List<Identificacion>> identificacionesxSolicitud,
            List<Identificacion> identificacionesEncontradas, final RadicacionTipo radicacionTipo, final FormatoTipo formatoTipo,
            final EnvioInformacionExterna envioInformacionExterna, final ContextoTransaccionalTipo contextoTransaccionalTipo, Map<String, List<String>> identificacionesPlanillas) throws AppException {

        final Long idFormato = formatoTipo.getIdFormato();
        final Long idVersion = formatoTipo.getValVersion().longValue();
        boolean sonActualizables = true;
        List<Solicitud> solicitudesActualizar = null;
        List<SolicitudInformacionTipo> solicitudInformacionTipoList = new ArrayList<SolicitudInformacionTipo>();

        for (Entry<Solicitud, List<Identificacion>> entry : identificacionesxSolicitud.entrySet()) {
            int cantidadEncontradas = 0;
            int cantidadEsperadas;

            if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getFormato())
                    && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getVersion())) {

                for (Identificacion identificacionSolicitud : entry.getValue()) {
                    String valTipoNumeroIdent = identificacionSolicitud.getCodTipoIdentificacion().concat(identificacionSolicitud.getValNumeroIdentificacion());
                    List<SolicitudPlanilla> solicitudPlanillas = entry.getKey().getSolicitudPlanillaList();
                    List<String> planillas = identificacionesPlanillas.get(valTipoNumeroIdent);
                    if (planillas != null && !planillas.isEmpty() && solicitudPlanillas != null && !solicitudPlanillas.isEmpty()) {

                        for (SolicitudPlanilla solicitudPlanilla : solicitudPlanillas) {
                            for (String planilla : planillas) {
                                if (solicitudPlanilla.getPlanilla().getId().equals(planilla)) {
                                    cantidadEncontradas++;
                                    break;
                                }
                            }
                        }
                    }
                }
            } else {
                for (Identificacion identificacionSolicitud : entry.getValue()) {
                    for (Identificacion identificacionEncontrada : identificacionesEncontradas) {
                        if (identificacionSolicitud.equals(identificacionEncontrada)) {
                            cantidadEncontradas++;
                        }
                    }
                }
            }
            cantidadEsperadas = this.obtenerCantidadIdentificacionesEsperadas(entry.getKey(), entry.getValue(), formatoTipo);
            SolicitudInformacionTipo solicitudInformacionTipo = mapper.map(entry.getKey(), SolicitudInformacionTipo.class);
            FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudInformacionTipo.getSolicitante(), contextoTransaccionalTipo);
            solicitudInformacionTipo.setSolicitante(funcionarioTipo);
            solicitudInformacionTipoList.add(solicitudInformacionTipo);

            if (cantidadEncontradas != 0 && cantidadEsperadas != 0
                    && cantidadEncontradas == cantidadEsperadas) {
                if (solicitudesActualizar == null) {
                    solicitudesActualizar = new ArrayList<Solicitud>();
                }
                solicitudesActualizar.add(entry.getKey());
            } else if (sonActualizables) {
                sonActualizables = false;
            }
            if (sonActualizables) {
                this.actualizarEstadoSolicitud(solicitudesActualizar,
                        SolicitudEntidadEstadoEnum.RESUELTA, envioInformacionExterna, contextoTransaccionalTipo);
                for (SolicitudInformacionTipo solicitudOut : solicitudInformacionTipoList) {
                    solicitudOut.setCodEstadoSolicitud(SolicitudEntidadEstadoEnum.RESUELTA.getCode());
                }
            }
        }
        return solicitudInformacionTipoList;
    }

    /**
     * Obtiene la cantidad de identificaciones que se esperan de acuerdo al
     * formato y la versión.
     *
     * @param solicitud la solicitud a validar
     * @param identificacionesSolicitud el listado de identifcaciones de la
     * solicitud
     * @param formatoTipo contiene el formato y la versión a validar
     * @return la cantidad de identificaciones que se esperan para la solicitud.
     */
    private int obtenerCantidadIdentificacionesEsperadas(final Solicitud solicitud,
            final List<Identificacion> identificacionesSolicitud, final FormatoTipo formatoTipo) {

        final Long idFormato = formatoTipo.getIdFormato();
        final Long idVersion = formatoTipo.getValVersion().longValue();
        int cantidadIdentificacionesEsperadas = 0;

        if ((idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_BANCO.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_BANCO.getVersion()))
                || (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_TELEFONIA.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_TELEFONIA.getVersion()))) {
            cantidadIdentificacionesEsperadas = identificacionesSolicitud.size();
        }

        if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_SAYP.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_SAYP.getVersion())) {
            cantidadIdentificacionesEsperadas = solicitud.getSolicitudPagoList().size();
        }

        if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_PILA.getVersion())) {
            cantidadIdentificacionesEsperadas = solicitud.getSolicitudPlanillaList().size();
        }
        return cantidadIdentificacionesEsperadas;
    }

    /**
     * Método que se encarga de validar el archivo de respuesta TELEFONÍA y
     * obtener las identificaciones que se cargaron en la operación de validar
     * respuesta información
     *
     * @param wb el archivo en el cual se buscan las identficaciones
     * @param idRespuestaSolicitud el identificador de la respuesta solicitud
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param identificaciones el listado de identificaciones que se llena a
     * medida que se encuentran ubicaciones en el archivo
     */
    private void cargarRespuestaPersonaTelefonia(final Workbook wb, final Long idRespuestaSolicitud,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<Identificacion> identificaciones) throws AppException {

        //Se obtienen las filas de la hoja del libro de excel
        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();

        if (rows.hasNext()) {
            rows.next();
            while (rows.hasNext()) {
                Row row = (Row) rows.next();

                //Se evalua si la fila contiene datos válidos
                if (ExcelUtil.evaluarFila(row, Constants.FORMATO_TELEFONIA_COLUMNA_INICIO, Constants.FORMATO_TELEFONIA_COLUMNA_FIN)) {

                    //Se asume que la fila es válida y se procede a llenar el objeto a persistir
                    RespuestaPersona respuestaPersona = new RespuestaPersona();
                    Identificacion identificacion = new Identificacion();

                    respuestaPersona.setCodTipoIdentificacion(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO)));
                    respuestaPersona.setValNumeroIdentificacion(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO)));
                    respuestaPersona.setValRazonSocial(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL)));
                    respuestaPersona.setValDireccion1(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_DIRECCION)));
                    respuestaPersona.setValDepartamento1(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO)));
                    respuestaPersona.setValCiudad1(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_CIUDAD)));
                    respuestaPersona.setValDireccion2(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS)));
                    respuestaPersona.setValDepartamento2(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS)));
                    respuestaPersona.setValCiudad2(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS)));
                    respuestaPersona.setValTelefono1(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_TELEFONO)));
                    respuestaPersona.setValTelefono2(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS)));
                    respuestaPersona.setValCelular(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_CELULAR)));
                    respuestaPersona.setValEmail(this.evaluarCeldaString(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_EMAIL)));
                    respuestaPersona.setValFax(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_FAX)));
                    respuestaPersona.setIdAnalisisInformacionSolic(idRespuestaSolicitud);
                    respuestaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    respuestaPersonaDao.create(respuestaPersona);
                    identificacion.setCodTipoIdentificacion(respuestaPersona.getCodTipoIdentificacion());
                    identificacion.setValNumeroIdentificacion(respuestaPersona.getValNumeroIdentificacion() + "");
                    identificaciones.add(identificacion);
                }
            }
        }
    }

    /**
     * Método que se encarga de validar el archivo de respuesta BANCOS y obtener
     * las identificaciones que se cargaron en la operación de validar respuesta
     * información
     *
     * @param wb el archivo en el cual se buscan las identficaciones
     * @param idRespuestaSolicitud el identificador de la respuesta solicitud
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param identificaciones el listado de identificaciones que se llena a
     * medida que se encuentran ubicaciones en el archivo
     */
    private void cargarRespuestaPersonaBancos(final Workbook wb, final Long idRespuestaSolicitud,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<Identificacion> identificaciones) throws AppException {

        //Se obtienen las filas de la hoja del libro de excel
        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();

        if (rows.hasNext()) {
            rows.next();
            while (rows.hasNext()) {
                Row row = (Row) rows.next();

                //Se evalua si la fila contiene datos válidos
                if (ExcelUtil.evaluarFila(row, Constants.FORMATO_BANCOS_COLUMNA_INICIO, Constants.FORMATO_BANCOS_COLUMNA_FIN)) {

                    //Se asume que la fila tiene datos válidos y se procede a llenar el objeto a persistir
                    RespuestaPersona respuestaPersona = new RespuestaPersona();
                    Identificacion identificacion = new Identificacion();

                    respuestaPersona.setNombreBanco(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_NOMBRE_BANCO)));
                    respuestaPersona.setCodTipoIdentificacion(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO)));
                    respuestaPersona.setValNumeroIdentificacion(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO)));
                    respuestaPersona.setValNombreCompleto(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS)));
                    respuestaPersona.setCodTipoCuenta(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_TIPO_CUENTA)));
                    respuestaPersona.setValDireccionResidencia(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA)));
                    respuestaPersona.setValDireccionTrabajo(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO)));
                    respuestaPersona.setValTelefono1(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_TELEFONO)));
                    respuestaPersona.setValCelular(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_BANCOS_CELDA_CELULAR)));
                    respuestaPersona.setDescObservaciones(this.evaluarCeldaString(row.getCell(Constants.FORMATO_BANCOS_CELDA_OBSERVACIONES)));
                    respuestaPersona.setIdAnalisisInformacionSolic(idRespuestaSolicitud);
                    respuestaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    if (row.getCell(Constants.FORMATO_BANCOS_CELDA_NUMERO_CUENTA) != null
                            && row.getCell(Constants.FORMATO_BANCOS_CELDA_NUMERO_CUENTA).getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        String vNumeroCuenta = this.stringFromDouble(row.getCell(Constants.FORMATO_BANCOS_CELDA_NUMERO_CUENTA).getNumericCellValue());
                        respuestaPersona.setValNumeroCuenta(new Double(vNumeroCuenta).longValue());
                    } else {
                        respuestaPersona.setValNumeroCuenta(new Double(row.getCell(Constants.FORMATO_BANCOS_CELDA_TIPO_CUENTA).getNumericCellValue()).longValue());
                    }

                    respuestaPersonaDao.create(respuestaPersona);
                    identificacion.setCodTipoIdentificacion(respuestaPersona.getCodTipoIdentificacion());
                    identificacion.setValNumeroIdentificacion(respuestaPersona.getValNumeroIdentificacion() + "");
                    identificaciones.add(identificacion);
                }
            }
        }
    }

    /**
     * Método que se encarga de validar el archivo de respuesta SAYP y obtener
     * las identificaciones que se cargaron en la operación de validar respuesta
     * información
     *
     * @param wb el archivo en el cual se buscan las identficaciones
     * @param idRespuestaSolicitud el identificador de la respuesta solicitud
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param identificaciones el listado de identificaciones que se llena a
     * medida que se encuentran ubicaciones en el archivo
     */
    private void cargarRespuestaPagoSAYP(Workbook wb, Long idRespuestaSolicitud,
            ContextoTransaccionalTipo contextoTransaccionalTipo, final List<Identificacion> identificaciones) throws AppException {

        //Se obtienen las filas de la hoja del libro de excel
        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();

        if (rows.hasNext()) {
            rows.next();
            while (rows.hasNext()) {
                Row row = (Row) rows.next();

                //Se evalua si la fila contiene datos válidos
                if (ExcelUtil.evaluarFila(row, Constants.FORMATO_SAYP_COLUMNA_INICIO, Constants.FORMATO_SAYP_COLUMNA_FIN)) {

                    //Se asume que la fila tiene datos válidos y se procede a llenar el objeto a persistir
                    RespuestaPago respuestaPago = new RespuestaPago();
                    Identificacion identificacion = new Identificacion();

                    respuestaPago.setTipoDocumento(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO)));
                    respuestaPago.setNumeroDocumento(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO)));
                    respuestaPago.setRazonSocial(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_RAZON_SOCIAL)));
                    respuestaPago.setFecPago(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_FECHA_PAGO)));
                    respuestaPago.setValorPagado(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_SAYP_CELDA_VALOR_PAGADO)));
                    respuestaPago.setConfirmacion(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_CONFIRMACION)));
                    respuestaPago.setFuncionarioValidador(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR)));
                    respuestaPago.setObservaciones(this.evaluarCeldaString(row.getCell(Constants.FORMATO_SAYP_CELDA_OBSERVACIONES)));
                    respuestaPago.setIdAnalisisInformacionSolic(idRespuestaSolicitud);
                    respuestaPago.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    respuestaPagoDao.create(respuestaPago);
                    identificacion.setCodTipoIdentificacion(respuestaPago.getTipoDocumento());
                    identificacion.setValNumeroIdentificacion(respuestaPago.getNumeroDocumento() + "");
                    identificaciones.add(identificacion);
                }
            }
        }
    }

    /**
     * Método que se encarga de validar el archivo de respuesta PILA y obtener
     * las identificaciones que se cargaron en la operación de validar respuesta
     * información
     *
     * @param wb el archivo en el cual se buscan las identficaciones
     * @param idRespuestaSolicitud el identificador de la respuesta solicitud
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param identificaciones el listado de identificaciones que se llena a
     * medida que se encuentran ubicaciones en el archivo
     */
    private void cargarRespuestaPlanillaPila(final Workbook wb, final Long idRespuestaSolicitud,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<Identificacion> identificaciones, Map<String, List<String>> planillasAportante) throws AppException {

        //Se obtienen las filas de la hoja del libro de excel
        Iterator<Row> rows = wb.getSheetAt(0).rowIterator();

        if (rows.hasNext()) {
            rows.next();
            while (rows.hasNext()) {
                Row row = (Row) rows.next();

                //Se evalua si la fila contiene datos válidos
                if (ExcelUtil.evaluarFila(row, Constants.FORMATO_PILA_COLUMNA_INICIO, Constants.FORMATO_PILA_COLUMNA_FIN)) {

                    //Se asume que la fila tiene datos válidos y se procede a llenar el objeto a persistir
                    RespuestaPlanilla respuestaPlanilla = new RespuestaPlanilla();
                    Identificacion identificacion = new Identificacion();

                    respuestaPlanilla.setTipoDocumento(this.evaluarCeldaString(row.getCell(Constants.FORMATO_PILA_CELDA_TIPO_DOCUMENTO)));
                    respuestaPlanilla.setNumeroDocumento(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_PILA_CELDA_NUMERO_DOCUMENTO)));
                    respuestaPlanilla.setNumeroPlanilla(this.evaluarCeldaNumeric(row.getCell(Constants.FORMATO_PILA_CELDA_NUMERO_PLANILLA)));
                    respuestaPlanilla.setPeriodoSalud(this.evaluarCeldaString(row.getCell(Constants.FORMATO_PILA_CELDA_PERIODO_SALUD)));
                    respuestaPlanilla.setFecCarguePila(this.evaluarCeldaString(row.getCell(Constants.FORMATO_PILA_CELDA_FECHA_CARGUE_PILA)));
                    respuestaPlanilla.setNombreLote(this.evaluarCeldaString(row.getCell(Constants.FORMATO_PILA_CELDA_NOMBRE_LOTE)));
                    respuestaPlanilla.setObservaciones(this.evaluarCeldaString(row.getCell(Constants.FORMATO_PILA_CELDA_OBSERVACIONES)));
                    respuestaPlanilla.setIdAnalisisInformacionSolic(idRespuestaSolicitud);
                    respuestaPlanilla.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    String valNumeroTipoIdentificacion = respuestaPlanilla.getTipoDocumento().concat(respuestaPlanilla.getNumeroDocumento().toString());
                    List<String> planillas = planillasAportante.get(valNumeroTipoIdentificacion);
                    if (planillas == null || planillas.isEmpty()) {
                        planillas = new ArrayList<String>();
                        planillas.add(respuestaPlanilla.getNumeroPlanilla().toString());
                        planillasAportante.put(valNumeroTipoIdentificacion, planillas);
                    } else {
                        planillasAportante.get(valNumeroTipoIdentificacion).add(respuestaPlanilla.getNumeroPlanilla().toString());
                    }

                    respuestaPlanillaDao.create(respuestaPlanilla);
                    identificacion.setCodTipoIdentificacion(respuestaPlanilla.getTipoDocumento());
                    identificacion.setValNumeroIdentificacion(respuestaPlanilla.getNumeroDocumento() + "");
                    identificaciones.add(identificacion);
                }
            }
        }
    }

    /**
     * Obtiene las identificaciones de acuerdo al formato y versión enviados, se
     * validan la correspondencia en los campos como en la operación de
     * validarRespuestaInformacion
     *
     * @param wb el libro de excel con la información que se ha cargado
     * @param formatoEstructuraTipo el formato y estructura que indican como se
     * debe cargar las identificaciones
     * @param idRespuestaSolicitud el identificador de la respuesta con el que
     * se realiza el cargue de información
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @param identificacionesRespondidas el listado de identificaciones
     * respondidas, el mismo objeto que se envía como parámetro y se actualiza
     * por referencia
     * @return el listado de identificaciones que se cargaron
     */
    private List<Identificacion> cargarRespuesta(final Workbook wb, final FormatoEstructuraTipo formatoEstructuraTipo,
            final Long idRespuestaSolicitud, final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<Identificacion> identificacionesRespondidas) throws AppException {

        Long idFormato = formatoEstructuraTipo.getFormato().getIdFormato();
        Long idVersion = formatoEstructuraTipo.getFormato().getValVersion().longValue();

        if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_BANCO.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_BANCO.getVersion())) {
            this.cargarRespuestaPersonaBancos(wb, idRespuestaSolicitud, contextoTransaccionalTipo, identificacionesRespondidas);
        }
        if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_SAYP.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_SAYP.getVersion())) {
            this.cargarRespuestaPagoSAYP(wb, idRespuestaSolicitud, contextoTransaccionalTipo, identificacionesRespondidas);
        }
        if (idFormato.equals(FormatoEnum.FORMATO_ESTRUCTURA_TELEFONIA.getFormato())
                && idVersion.equals(FormatoEnum.FORMATO_ESTRUCTURA_TELEFONIA.getVersion())) {
            this.cargarRespuestaPersonaTelefonia(wb, idRespuestaSolicitud,
                    contextoTransaccionalTipo, identificacionesRespondidas);
        }
        return identificacionesRespondidas;
    }

    /**
     * Obtiene las solicitudes con el radicado de salida enviado, se invoca
     * durante la operación cruzar para determinar las solicitudes a las cuales
     * se les va a realizar el cruce de información
     *
     * @param idRadicadoSalida el radicado que se toma como parámetro para
     * consultar las solicitudes a procesar
     * @return el listado de solicitudes con el radicado de salida
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private List<Solicitud> getSolicitudesByRadicado(String idRadicadoSalida) throws AppException {
        ParametroTipo parametroIdRadicadoEntrada = new ParametroTipo();
        List<ParametroTipo> parametrosTipo = new ArrayList<ParametroTipo>();

        parametroIdRadicadoEntrada.setIdLlave("idRadicadoSalida");
        parametroIdRadicadoEntrada.setValValor(idRadicadoSalida);
        parametrosTipo.add(parametroIdRadicadoEntrada);

        PagerData<Solicitud> solicitudes = solicitudDao.findPorCriterios(parametrosTipo, null, null, null);
        return solicitudes.getData();
    }

    /**
     * Obtiene la clase que se debe utilizar para realizar la validación del
     * formato, se invoca durante la operacion validarRespuestaInformacion una
     * vez se obtiene el objeto validación formato de la base de datos
     *
     * @param validacionFormato contiene el nombre de la clase a utilizar
     * @return la clase que implementa la interfaz ValidacionFormatoFacade
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private Class obtenerInstanciaValidacionFormato(ValidacionFormato validacionFormato) throws AppException {
        ClassLoader classLoader = RespuestaInformacionFacade.class.getClassLoader();
        Class loadedClass = null;
        try {
            loadedClass = classLoader.loadClass(validacionFormato.getNombreClase());
        } catch (ClassNotFoundException cnf) {
            throw new AppException(ErrorEnum.ERROR_NO_ESPERADO.getMessage(), cnf);
        }
        return loadedClass;
    }

    /**
     * Método que se encarga de actualizar el estado, el radicado de entrada y
     * el envioInformacionExterna de la solicitud, este método se invoca durante
     * el cruzarRespuestaInformación una vez se cruzan las identificaciones para
     * la solicitud a actualizar
     *
     * @param solicitudes la solicitud a actualizar
     * @param solicitudEntidadEstadoEnum el estado al que se va a actualizar la
     * solicitud
     * @param envioInformacionExterna el objeto envioInformacionExterna que
     * tiene la solicitud
     * @param radicacionTipo contiene el radicado de entrada al cual va quedar
     * asociado la solicitud
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoría.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private void actualizarEstadoSolicitud(final List<Solicitud> solicitudes,
            final SolicitudEntidadEstadoEnum solicitudEntidadEstadoEnum, final EnvioInformacionExterna envioInformacionExterna, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        ValorDominio valorDominio = valorDominioDao.find(solicitudEntidadEstadoEnum.getCode());
        if (valorDominio == null) {
            throw new AppException("No se encontró codEstadoSolicitud con el ID: " + solicitudEntidadEstadoEnum.getCode());
        }

        for (Solicitud solicitudActualizar : solicitudes) {

            if (SolicitudEntidadEstadoEnum.RESUELTA.getCode().equals(solicitudEntidadEstadoEnum.getCode())) {
                solicitudActualizar.setFecRtaSolicitud(DateUtil.currentCalendar());
            }
            solicitudActualizar.setCodEstadoSolicitud(valorDominio);
            solicitudActualizar.setRadicadoEntrada(envioInformacionExterna);
            solicitudActualizar.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
            solicitudActualizar.setFechaModificacion(DateUtil.currentCalendar());
            solicitudDao.edit(solicitudActualizar);
        }
    }

    /**
     * Método que obtiene un listado de nombres de arhivo a partir de un listado
     * de archivos completo
     *
     * @param archivoTipoList el listado de archivos a procesar
     * @return el listado de nombres de archivos encontrados
     */
    private List<String> obtenerNombreArchivos(List<ArchivoTipo> archivoTipoList) {

        List<String> nombreArchivos = null;
        if (archivoTipoList != null
                && !archivoTipoList.isEmpty()) {
            nombreArchivos = new ArrayList<String>();
            for (ArchivoTipo archivoTipo : archivoTipoList) {
                nombreArchivos.add(archivoTipo.getValNombreArchivo());
            }
        }
        return nombreArchivos;
    }

    /**
     * Mètodo que agrupa las solicitudes pendientes a procesar
     *
     * @param solicitudesPorRadicado las solicitudes agrupadas para un radicado
     * de salida
     * @return el listado de solicitudes que no estàn resueltas
     */
    private List<Solicitud> validarSolicitudesPendientes(final List<Solicitud> solicitudesPorRadicado) {

        List<Solicitud> solicitudesPendientes = null;
        if (solicitudesPorRadicado != null
                && !solicitudesPorRadicado.isEmpty()) {
            solicitudesPendientes = new ArrayList<Solicitud>();
            for (Solicitud solicitudPorRadicado : solicitudesPorRadicado) {
                if (!SolicitudEntidadEstadoEnum.RESUELTA.getCode().equals(solicitudPorRadicado.getCodEstadoSolicitud().getId())) {
                    solicitudesPendientes.add(solicitudPorRadicado);
                }
            }
        }
        return solicitudesPendientes;
    }

    /**
     * Método que evalúa el contenido de una celda y lo retorna como string
     *
     * @param celda la celda a evaluar
     * @return el valor String
     */
    private String evaluarCeldaString(final Cell celda) {

        String valorCelda = "";

        if (celda != null
                && !(celda.getCellType() == Cell.CELL_TYPE_BLANK)) {
            if (celda.getCellType() == Cell.CELL_TYPE_STRING) {
                valorCelda = celda.getStringCellValue();
            } else if (HSSFDateUtil.isCellDateFormatted(celda)) {
                valorCelda = DateUtil.parseDateToString(celda.getDateCellValue());
            } else if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                valorCelda = Double.toString(celda.getNumericCellValue());
            }
        }
        return valorCelda;
    }

    /**
     * Método que evalúa una celda y retorna su valor numérico
     *
     * @param celda la celda a evaluar
     * @return el valor numérico
     */
    private Long evaluarCeldaNumeric(final Cell celda) {

        Long valorCelda = null;

        if (celda != null
                && !(celda.getCellType() == Cell.CELL_TYPE_BLANK)) {
            if (celda.getCellType() == Cell.CELL_TYPE_STRING) {
                try {
                    valorCelda = Long.valueOf(celda.getStringCellValue());
                } catch (NumberFormatException nfe) {
                    LOG.info("Error al convertir la celda a un valor numérico", nfe);
                }
            } else if (celda.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                valorCelda = Double.valueOf(celda.getNumericCellValue()).longValue();
            }
        }
        return valorCelda;
    }

    private String stringFromDouble(final Double source) {
        return source.toString();
    }
}
