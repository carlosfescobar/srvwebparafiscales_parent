package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jsaenzar
 */
public interface SolicitudInformacionFacade extends Serializable {

    Long crearSolicitudInformacion(SolicitudInformacionTipo solicitudInformacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarSolicitudInformacion(SolicitudInformacionTipo solicitudInformacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SolicitudInformacionTipo> buscarPorIdListSolicitudInformacion(List<String> idNumSolicitudList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<SolicitudInformacionTipo> buscarPorCriteriosSolicitudInformacion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
