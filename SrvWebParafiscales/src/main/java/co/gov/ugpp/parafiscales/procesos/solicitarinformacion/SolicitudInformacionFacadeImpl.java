package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoSolicitudEnum;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudDocAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPlanilla;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz SolicitudInformacionFacade que contiene las operaciones del servicio
 * SrvAplSolicitudInformacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SolicitudInformacionFacadeImpl extends AbstractFacade implements SolicitudInformacionFacade {

    @EJB
    private SolicitudDao solicitudInformacionDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private Populator populator;

    /**
     * Implementación de la operación crearSolicitudInformacion esta se encarga de crear una nueva solicitudInformación
     * en la base de datos a partir del objeto solicitudInformacionTipo enviado.
     *
     * @param solicitudInformacionTipo Objeto a partir de cual se va a crear una nueva solicitudInformación en la base
     * de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return id del nuevo Obejto solicitudInformacion creado que se retorna junto con el contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public Long crearSolicitudInformacion(final SolicitudInformacionTipo solicitudInformacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        
        System.out.println("::INIT:: crearSolicitudInformacion ");
        
        
        if (solicitudInformacionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkSolicitarInformacion(solicitudInformacionTipo, errorTipoList);

        this.checkBusinessSolicitudInformacion(solicitudInformacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Solicitud solicitudInformacion = new Solicitud();

        solicitudInformacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        
        this.populateSolicitudInformacion(solicitudInformacionTipo, solicitudInformacion, errorTipoList, contextoTransaccionalTipo);
        
        populator.populateSolicitudInformacionOnCreate(solicitudInformacionTipo, solicitudInformacion, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        
        
        //System.out.println("::END:: crearSolicitudInformacion antes de las modifificaciones");
        //solicitudInformacionDao.getEntityManager().flush();
        //solicitudInformacionDao.getEntityManager().clear();
        //solicitudInformacionDao.getEntityManager().refresh(solicitudInformacion);
        //solicitudInformacionDao.getEntityManager().refresh(solicitudInformacion.getSolicitudPlanillaList());
        //System.out.println("::END:: crearSolicitudInformacion antes de create ");
        
        
        final Long idSolicitud = solicitudInformacionDao.create(solicitudInformacion);

        
        //solicitudInformacionDao.getEntityManager().flush();
        //solicitudInformacionDao.getEntityManager().clear();
        //solicitudInformacionDao.getEntityManager().refresh(solicitudInformacion);
        
        
        System.out.println("::END:: crearSolicitudInformacion idSolicitud: " + idSolicitud);
        
        return idSolicitud;
    }

    /**
     * Implementación de la operación buscarPorIdListSolicitudInformacion esta se encraga de buscar un listado de
     * solicitudInformacion de acuerdo la listado de ids enviados
     *
     * @param idNumSolicitudList listado de solicitudes a buscar por medio de un listado de ids en la base de datos.
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return Listado de solicitudes obtenidas que se retorna junto con el contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public List<SolicitudInformacionTipo> buscarPorIdListSolicitudInformacion(final List<String> idNumSolicitudList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idNumSolicitudList == null || idNumSolicitudList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }

        final List<Long> ids = mapper.map(idNumSolicitudList, String.class, Long.class);

        final List<Solicitud> solicitudInformacionList = solicitudInformacionDao.findByIdListAndTipo(ids, TipoSolicitudEnum.SOLICITUD_INFORMACION);

        final List<SolicitudInformacionTipo> solicitudInformacionTipoList
                = mapper.map(solicitudInformacionList, Solicitud.class, SolicitudInformacionTipo.class);

        if (solicitudInformacionTipoList != null) {
            for (final SolicitudInformacionTipo solicitudInformacionTipo : solicitudInformacionTipoList) {
                if (solicitudInformacionTipo.getSolicitante() != null
                        && StringUtils.isNotBlank(solicitudInformacionTipo.getSolicitante().getIdFuncionario())) {
                    FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudInformacionTipo.getSolicitante(), contextoTransaccionalTipo);
                    solicitudInformacionTipo.setSolicitante(funcionarioTipo);
                }
            }
        }

        return solicitudInformacionTipoList;
    }

    /**
     * Implementación de la operación actualizarSolicitudInformacion esta se encarga de actualizar una
     * solicitudInformacion encotrada en la base de datos de acuerdo a un id enviado
     *
     * @param solicitudInformacionTipo Objeto a traves del cual se actualiza la SolicitudInformacion
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public void actualizarSolicitudInformacion(final SolicitudInformacionTipo solicitudInformacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        
        System.out.println("::INIT:: actualizarSolicitudInformacion ");
        
        if (solicitudInformacionTipo == null || StringUtils.isBlank(solicitudInformacionTipo.getIdSolicitud())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Solicitud solicitudInformacion = solicitudInformacionDao.find(Long.valueOf(solicitudInformacionTipo.getIdSolicitud()));

        if (solicitudInformacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No existe un IdSolicitud con el valor: " + solicitudInformacionTipo.getIdSolicitud()));
            throw new AppException(errorTipoList);
        }

        this.checkBusinessSolicitudInformacion(solicitudInformacionTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        solicitudInformacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSolicitudInformacion(solicitudInformacionTipo, solicitudInformacion, errorTipoList, contextoTransaccionalTipo);

        if (solicitudInformacionTipo.getCotizantes() != null
                && !solicitudInformacionTipo.getCotizantes().isEmpty()
                && solicitudInformacion.getSoliciutdCotizanteList() != null
                && !solicitudInformacion.getSoliciutdCotizanteList().isEmpty()) {
            populator.populateSolicitudInformacionOnUpdate(solicitudInformacionTipo, solicitudInformacion, contextoTransaccionalTipo, errorTipoList);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        System.out.println("::END:: actualizarSolicitudInformacion solicitudInformacion: " + solicitudInformacion.getId());

        solicitudInformacionDao.edit(solicitudInformacion);
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad solicitudInformacion
     *
     * @param solicitudInformacionTipo Objeto origen
     * @param solicitudInformacion Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    private void populateSolicitudInformacion(final SolicitudInformacionTipo solicitudInformacionTipo,
            final Solicitud solicitudInformacion, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {


     
        
        //System.out.println("::ANDRES01:: solicitudInformacion: " + solicitudInformacion.getId()); //null
        //System.out.println("::ANDRES02:: populateSolicitudInformacion: " + solicitudInformacion.getSolicitudPlanillaList().size()); // 0
        
        //System.out.println("::ANDRES03:: solicitudInformacionTipo: " + solicitudInformacionTipo.getPlanillas().size()); //1
        
        
        //System.out.println("::ANDRES04:: getEstadoPlanilla: " + solicitudInformacionTipo.getPlanillas().get(0).getEstadoPlanilla()); //1870002
        //System.out.println("::ANDRES04:: getIdNumeroPlanilla: " + solicitudInformacionTipo.getPlanillas().get(0).getIdNumeroPlanilla()); // 789765432
        //System.out.println("::ANDRES04:: getFecPeriodoPago: " + solicitudInformacionTipo.getPlanillas().get(0).getFecPeriodoPago().toString()); // 1542621600000
        //System.out.println("::ANDRES04:: getDescObservaciones: " + solicitudInformacionTipo.getPlanillas().get(0).getDescObservaciones()); // bbbbbbbbb
        
        
        
        
        populator.populateSolicitud(solicitudInformacionTipo, solicitudInformacion, errorTipoList, contextoTransaccionalTipo);

        
        //System.out.println("::ANDRES05:: solicitudInformacionTipo: " + solicitudInformacionTipo.getPlanillas().size()); //1
        //System.out.println("::ANDRES06:: solicitudInformacionTipo: " + solicitudInformacionTipo.getPlanillas().get(0).getEstadoPlanilla()); //1870002
        
        if (solicitudInformacionTipo.getPlanillas() != null && solicitudInformacionTipo.getPlanillas().size() > 0)
        {
            ValorDominio valorDominio = valorDominioDao.find(solicitudInformacionTipo.getCodTipoSolicitud());
            //PlanillaTipo planillaTipo = PlanillaTipo
            //SolicitudPlanilla solicitudPlanilla
            
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró CodTipoSolicitud con el valor: " + solicitudInformacionTipo.getCodTipoSolicitud()));
            } else if (!TipoSolicitudEnum.SOLICITUD_INFORMACION.getCode().equals(valorDominio.getId())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El codTipoSolicitud enviado: (" + solicitudInformacionTipo.getCodTipoSolicitud()
                        + "), no hace referencia al tipo de solicitud de información: (" + TipoSolicitudEnum.SOLICITUD_INFORMACION.getCode() + ")"));
            } else {
                solicitudInformacion.setCodTipoSolicitud(valorDominio);
            }
        }
        
        //solicitudInformacionTipo.getPlanillas().get(0).getIdNumeroPlanilla();
        //        SolicitudPlanilla

        //solicitudInformacion.getSolicitudPlanillaList().add(solicitudInformacionTipo.getPlanillas().get(0));
        
        
        
        
        
        if (StringUtils.isNotBlank(solicitudInformacionTipo.getCodTipoSolicitud())) {
            ValorDominio valorDominio = valorDominioDao.find(solicitudInformacionTipo.getCodTipoSolicitud());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró CodTipoSolicitud con el valor: " + solicitudInformacionTipo.getCodTipoSolicitud()));
            } else if (!TipoSolicitudEnum.SOLICITUD_INFORMACION.getCode().equals(valorDominio.getId())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El codTipoSolicitud enviado: (" + solicitudInformacionTipo.getCodTipoSolicitud()
                        + "), no hace referencia al tipo de solicitud de información: (" + TipoSolicitudEnum.SOLICITUD_INFORMACION.getCode() + ")"));
            } else {
                solicitudInformacion.setCodTipoSolicitud(valorDominio);
            }
        }

        if (StringUtils.isNotBlank(solicitudInformacionTipo.getCodTipoConsulta())) {
            ValorDominio valorDominio = valorDominioDao.find(solicitudInformacionTipo.getCodTipoConsulta());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un CodTipoConsulta con el valor:" + solicitudInformacionTipo.getCodTipoConsulta()));
            } else {
                solicitudInformacion.setCodTipoConsulta(valorDominio);
            }
        }

        if (solicitudInformacionTipo.getEntidadExterna() != null
                && solicitudInformacionTipo.getEntidadExterna().getIdPersona() != null) {

            IdentificacionTipo identificacionTipo = solicitudInformacionTipo.getEntidadExterna().getIdPersona();
            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una EntidadExterna con la identificación:" + identificacion));
            } else {
                solicitudInformacion.setEntidadExterna(entidadExterna);
            }
        }

        if (StringUtils.isNotBlank(solicitudInformacionTipo.getEsConsultaNoAportante())) {
            Validator.parseBooleanFromString(solicitudInformacionTipo.getEsConsultaNoAportante(),
                    solicitudInformacionTipo.getClass().getSimpleName(),
                    "esConsultaNoAportante", errorTipoList);
            solicitudInformacion.setEsConsultaNoAportante(mapper.map(solicitudInformacionTipo.getEsConsultaNoAportante(), Boolean.class));
        }

        if (solicitudInformacionTipo.getIdDocumentoAnexo() != null
                && !solicitudInformacionTipo.getIdDocumentoAnexo().isEmpty()) {

            for (String docAnexo : solicitudInformacionTipo.getIdDocumentoAnexo()) {
                SolicitudDocAnexo solicitudDocAnexo = new SolicitudDocAnexo();
                solicitudDocAnexo.setDocAnexo(docAnexo);
                solicitudDocAnexo.setSolicitud(solicitudInformacion);
                solicitudDocAnexo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                Validator.checkSoilicitudDocAnexo(solicitudDocAnexo, errorTipoList);
                solicitudInformacion.getSolicitudDocAnexoList().add(solicitudDocAnexo);
            }
        }

        if (solicitudInformacionTipo.getDocsSolicitudAportante() != null) {
            Validator.checkArchivo(solicitudInformacionTipo.getDocsSolicitudAportante(), errorTipoList);
            Archivo archivo = mapper.map(solicitudInformacionTipo.getDocsSolicitudAportante(), Archivo.class);
            archivo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            solicitudInformacion.setDocSolicitudAportante(archivo);
        }

        if (StringUtils.isNotBlank(solicitudInformacionTipo.getEsConsultaAprobada())) {
            Validator.parseBooleanFromString(solicitudInformacionTipo.getEsConsultaAprobada(),
                    solicitudInformacionTipo.getClass().getSimpleName(),
                    "esConsultaAprobada", errorTipoList);
            solicitudInformacion.setEsConsultaAprobada(mapper.map(solicitudInformacionTipo.getEsConsultaAprobada(), Boolean.class));
        }

        if (StringUtils.isNotBlank(solicitudInformacionTipo.getDescObservacion())) {
            solicitudInformacion.setDescObservacion(solicitudInformacionTipo.getDescObservacion());
        }

        if (StringUtils.isNotBlank(solicitudInformacionTipo.getDescObservacionAprobada())) {
            solicitudInformacion.setDescObservacionAprobada(solicitudInformacionTipo.getDescObservacionAprobada());
        }
        if (StringUtils.isNotBlank(solicitudInformacionTipo.getDescObservacionNoAportante())) {
            solicitudInformacion.setDescObserNoAportante(solicitudInformacionTipo.getDescObservacionNoAportante());
        }
        
        
        //System.out.println("::ANDRES07:: getPlanillas: " + solicitudInformacionTipo.getPlanillas()); // PlanillaTipo@7ab223b5
        //System.out.println("::ANDRES07:: getPlanillas: " + !solicitudInformacionTipo.getPlanillas().isEmpty()); // true
        //System.out.println("::ANDRES07:: getSolicitudPlanillaList: " + solicitudInformacion.getSolicitudPlanillaList()); // []
        //System.out.println("::ANDRES07:: getSolicitudPlanillaList: " + !solicitudInformacion.getSolicitudPlanillaList().isEmpty()); // false
        
        

        
        
        
        if (solicitudInformacionTipo.getPlanillas() != null
            && !solicitudInformacionTipo.getPlanillas().isEmpty()
            && solicitudInformacion.getSolicitudPlanillaList() != null) {
            
             //System.out.println("::ANDRES08:: modificar getPlanillas: " + solicitudInformacionTipo.getPlanillas().get(0).getEstadoPlanilla());
            for (PlanillaTipo planillaTipo : solicitudInformacionTipo.getPlanillas()) { 
                
                //System.out.println("::ANDRES09:: getEstadoPlanilla: " + planillaTipo.getEstadoPlanilla());
                //System.out.println("::ANDRES02:: getId: " + solicitudPlanilla.getPlanilla().getId());
                
                for (SolicitudPlanilla solicitudPlanilla : solicitudInformacion.getSolicitudPlanillaList()) {
                    
                    //System.out.println("::ANDRES10:: getIdNumeroPlanilla: " + planillaTipo.getIdNumeroPlanilla());
                    //System.out.println("::ANDRES10:: getId: " + solicitudPlanilla.getPlanilla().getId());
                    //System.out.println("::ANDRES10:: getEstadoPlanilla: " + planillaTipo.getEstadoPlanilla());
                    
                    if (planillaTipo.getIdNumeroPlanilla().equals(solicitudPlanilla.getPlanilla().getId())) {
                       
                        //System.out.println("::ANDRES10:: getIdNumeroPlanilla: " + planillaTipo.getIdNumeroPlanilla());
                        //System.out.println("::ANDRES10:: getId: " + solicitudPlanilla.getPlanilla().getId());
                        if (StringUtils.isNotBlank(planillaTipo.getDescObservaciones())) {
                            ValorDominio valorDominio = valorDominioDao.find(planillaTipo.getCodTipoObservaciones());
                            solicitudPlanilla.getPlanilla().setValPlanilla(planillaTipo.getDescObservaciones());
                            solicitudPlanilla.getPlanilla().setCodTipoObservaciones(valorDominio);
                        } 
                        
                        //System.out.println("::ANDRES01:: getEstadoPlanilla: " + planillaTipo.getEstadoPlanilla());
                        //System.out.println("::ANDRES02:: getId: " + solicitudPlanilla.getPlanilla().getId());
                        if (StringUtils.isNotBlank(planillaTipo.getEstadoPlanilla())) {
                            solicitudPlanilla.getPlanilla().setEstadoPlanilla(planillaTipo.getEstadoPlanilla());
                        } 
                        
                    }
                    
                    List<SolicitudPlanilla> solicitudPlanillas = new ArrayList<SolicitudPlanilla>();
                    solicitudPlanillas.add(solicitudPlanilla);
                    solicitudInformacion.setSolicitudPlanillaList(solicitudPlanillas);
                        
                }
            }
        }
    }

    /**
     * implementación de la operación buscarPorCriteriosSolicitudInformacion esta se encarga de buscar una
     * solicitudInformacion por medio de los parametros enviado y ordenar la consulta de acuerdo a unos criterios
     * establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a buscar una solicitud
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @return Listado de solicitudes encontradas que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public PagerData<SolicitudInformacionTipo> buscarPorCriteriosSolicitudInformacion(
            final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Solicitud> solicitudesPagerData = solicitudInformacionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<SolicitudInformacionTipo> solicitudTipoList = new ArrayList<SolicitudInformacionTipo>();

        for (final Solicitud solicitud : solicitudesPagerData.getData()) {
            final SolicitudInformacionTipo solicitudTipo = mapper.map(solicitud, SolicitudInformacionTipo.class);
            if (solicitudTipo.getSolicitante() != null
                    && StringUtils.isNotBlank(solicitudTipo.getSolicitante().getIdFuncionario())) {
                final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudTipo.getSolicitante(), contextoTransaccionalTipo);
                solicitudTipo.setSolicitante(funcionarioTipo);
            }
            solicitudTipoList.add(solicitudTipo);
        }

        return new PagerData(solicitudTipoList, solicitudesPagerData.getNumPages());
    }

    /**
     * Método que valida reglas de negocio para solicitar información
     *
     * @param solicitudInformacionTipo la solicitud a validar
     * @param errorTipoList contiene los errores de la operación
     * @throws AppException
     */
    private void checkBusinessSolicitudInformacion(final SolicitudInformacionTipo solicitudInformacionTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (solicitudInformacionTipo.getEntidadExterna() != null
                && solicitudInformacionTipo.getEntidadExterna().getIdPersona() != null
                && StringUtils.isNotBlank(solicitudInformacionTipo.getEntidadExterna().getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(solicitudInformacionTipo.getEntidadExterna().getIdPersona().getValNumeroIdentificacion())) {

            final Identificacion identificacion = mapper.map(solicitudInformacionTipo.getEntidadExterna().getIdPersona(), Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);
            if (entidadExterna != null) {
                if (entidadExterna.getPersona().getContacto() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La entidad externa: " + identificacion + ", no tiene datos de contacto registrados"));
                } else if (entidadExterna.getPersona().getContacto().getUbicacionList() == null
                        || entidadExterna.getPersona().getContacto().getUbicacionList().isEmpty()) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La entidad externa: " + identificacion + ", no tiene datos de ubicación registrados"));
                }
            }
        }
    }
}
