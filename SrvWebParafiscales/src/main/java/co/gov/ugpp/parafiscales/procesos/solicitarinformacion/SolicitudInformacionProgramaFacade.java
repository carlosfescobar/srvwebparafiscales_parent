package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1.SolicitudProgramaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodriguez
 */
public interface SolicitudInformacionProgramaFacade extends Serializable {

    Long crearSolicitudInformacionPrograma(SolicitudProgramaTipo solicitudProgramaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarSolicitudInformacionPrograma(SolicitudProgramaTipo solicitudProgramaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<SolicitudProgramaTipo> buscarPorIdListSolicitudPrograma(List<String> idNumSolicitudList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
