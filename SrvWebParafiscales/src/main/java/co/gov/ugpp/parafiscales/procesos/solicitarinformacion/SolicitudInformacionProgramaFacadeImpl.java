package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoSolicitudEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudFuentesInfoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudArchivoAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudFuenteInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1.SolicitudProgramaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz SolicitudInformacionProgramaFacade que contiene las operaciones del servicio
 * SrvAplSolicitudInformacionPrograma
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class SolicitudInformacionProgramaFacadeImpl extends AbstractFacade implements SolicitudInformacionProgramaFacade {

    @EJB
    private SolicitudDao solicitudProgramaDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private Populator populator;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private SolicitudFuentesInfoDao solicitudFuentesInfoDao;

    /**
     * implementación de la operación buscarPorIdListSolicitudPrograma esta se encarga de obtener un listado de
     * solicitudPrograma de acuerdo a al listado de ids enviados
     *
     * @param idNumSolicitudList listado de solicitudes a buscar por medio de un listado de ids en la base de datos.
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return Listado de solicitudes obtenido que se retorna junto con el contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public List<SolicitudProgramaTipo> buscarPorIdListSolicitudPrograma(final List<String> idNumSolicitudList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idNumSolicitudList == null || idNumSolicitudList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID a buscar");
        }

        final List<Long> ids = mapper.map(idNumSolicitudList, String.class, Long.class);
        final List<Solicitud> solicitudProgramaList = solicitudProgramaDao.findByIdListAndTipo(ids, TipoSolicitudEnum.SOLICITUD_PROGRAMA);
        final List<SolicitudProgramaTipo> solicitudProgramaTipoList
                = mapper.map(solicitudProgramaList, Solicitud.class, SolicitudProgramaTipo.class);
        if (solicitudProgramaTipoList != null) {
            for (final SolicitudProgramaTipo solicitudProgramaTipo : solicitudProgramaTipoList) {
                if (solicitudProgramaTipo.getSolicitante() != null
                        && StringUtils.isNotBlank(solicitudProgramaTipo.getSolicitante().getIdFuncionario())) {
                    final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(solicitudProgramaTipo.getSolicitante(), contextoTransaccionalTipo);
                    solicitudProgramaTipo.setSolicitante(funcionarioTipo);
                }
            }
        }
        return solicitudProgramaTipoList;
    }

    /**
     * implementación de la operación actualizarSolicitudInformacionPrograma se encarga de actualizar una
     * solicitudInformacionPrograma encontrada en la base de datos por medio de un id solicitud enviado
     *
     * @param solicitudProgramaTipo Objeto a Actualizar en la base de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public void actualizarSolicitudInformacionPrograma(final SolicitudProgramaTipo solicitudProgramaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (solicitudProgramaTipo == null || StringUtils.isBlank(solicitudProgramaTipo.getIdSolicitud())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final Solicitud solicitudPrograma = solicitudProgramaDao.find(Long.valueOf(solicitudProgramaTipo.getIdSolicitud()));

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (solicitudPrograma == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un IdSolicitud con el valor:" + solicitudProgramaTipo.getIdSolicitud()));
            throw new AppException(errorTipoList);
        }

        solicitudPrograma.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSolicitudPrograma(solicitudProgramaTipo, solicitudPrograma, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        solicitudProgramaDao.edit(solicitudPrograma);
    }

    /**
     * implementación de la operación crearSolicitudInformacionPrograma se encarga de crear una nueva
     * SolicitudInformacionPrograma a partir del dto solicitudProgramaTipo
     *
     * @param solicitudProgramaTipo Objeto a partir del cual se va a crear una SolicitudInformacionPrograma en la base
     * de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return solicitudPrograma creado que se retorna junto con el contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public Long crearSolicitudInformacionPrograma(final SolicitudProgramaTipo solicitudProgramaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (solicitudProgramaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkSolicitarInformacionPrograma(solicitudProgramaTipo, errorTipoList);

        final Solicitud solicitudPrograma = new Solicitud();

        solicitudPrograma.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateSolicitudPrograma(solicitudProgramaTipo, solicitudPrograma, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        return solicitudProgramaDao.create(solicitudPrograma);
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad solicitudInformacion
     *
     * @param solicitudProgramaTipo Objeto Origen
     * @param solicitudPrograma Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @param contextoTransaccionalTipo Contiene la información para almacenar la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    private void populateSolicitudPrograma(final SolicitudProgramaTipo solicitudProgramaTipo,
            final Solicitud solicitudPrograma, final List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        populator.populateSolicitud(solicitudProgramaTipo, solicitudPrograma, errorTipoList, contextoTransaccionalTipo);

        if (StringUtils.isNotBlank(solicitudProgramaTipo.getCodTipoSolicitud())) {
            ValorDominio valorDominio = valorDominioDao.find(solicitudProgramaTipo.getCodTipoSolicitud());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró CodTipoSolicitud con el valor: " + solicitudProgramaTipo.getCodTipoSolicitud()));
            } else if (!TipoSolicitudEnum.SOLICITUD_PROGRAMA.getCode().equals(valorDominio.getId())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El codTipoSolicitud enviado: (" + solicitudProgramaTipo.getCodTipoSolicitud()
                        + "), no hace referencia al tipo de solicitud programa: (" + TipoSolicitudEnum.SOLICITUD_PROGRAMA.getCode() + ")"));
            } else {
                solicitudPrograma.setCodTipoSolicitud(valorDominio);
            }
        }

        if (solicitudProgramaTipo.getDocAnexos() != null
                && !solicitudProgramaTipo.getDocAnexos().isEmpty()) {
            for (ArchivoTipo archivoTipo : solicitudProgramaTipo.getDocAnexos()) {
                Validator.checkArchivo(archivoTipo, errorTipoList);
                Archivo archivo = mapper.map(archivoTipo, Archivo.class);
                archivo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                SolicitudArchivoAnexo solicitudDocAnexo = new SolicitudArchivoAnexo();
                solicitudDocAnexo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                solicitudDocAnexo.setArchivo(archivo);
                solicitudDocAnexo.setSolicitud(solicitudPrograma);
                solicitudPrograma.getSolicitudArchivoAnexoList().add(solicitudDocAnexo);
            }
        }
        if (solicitudProgramaTipo.getCodFuenteInformacion() != null
                && !solicitudProgramaTipo.getCodFuenteInformacion().isEmpty()) {
            for (String codFuenteInformacion : solicitudProgramaTipo.getCodFuenteInformacion()) {

                SolicitudFuenteInformacion solicitudFuenteInformacion;
                ValorDominio valorDominio = valorDominioDao.find(codFuenteInformacion);
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un CodFuenteInformacion con el valor:" + codFuenteInformacion));
                } else if (solicitudPrograma.getId() != null) {
                    solicitudFuenteInformacion = solicitudFuentesInfoDao.findBySolicitudAndFuenteInfo(solicitudPrograma, valorDominio);
                    if (solicitudFuenteInformacion != null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "Ya existe la relación entre la fuente de información: " + valorDominio.getId() + ", y la solicitud con el ID:" + solicitudPrograma.getId()));
                    } else {
                        this.buildFuenteInformacion(contextoTransaccionalTipo, solicitudPrograma, valorDominio);
                    }
                } else {
                    this.buildFuenteInformacion(contextoTransaccionalTipo, solicitudPrograma, valorDominio);
                }
            }
        }

        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValPorcentajeEfectividad())) {
            solicitudPrograma.setValPorcentajeEjectividad(solicitudProgramaTipo.getValPorcentajeEfectividad());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValResumen())) {
            solicitudPrograma.setValResumen(solicitudProgramaTipo.getValResumen());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValFiltros())) {
            solicitudPrograma.setDescFiltro(solicitudProgramaTipo.getValFiltros());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValCruces())) {
            solicitudPrograma.setDescCruces(solicitudProgramaTipo.getValCruces());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValResultadoEsperado())) {
            solicitudPrograma.setDescResultadoEsperado(solicitudProgramaTipo.getValResultadoEsperado());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValObservacionRequerimiento())) {
            solicitudPrograma.setVlaObservacionRequerimiento(solicitudProgramaTipo.getValObservacionRequerimiento());
        }
        if (StringUtils.isNotBlank(solicitudProgramaTipo.getValRutaArchivoRequerimiento())) {
            solicitudPrograma.setValRutaArchivoResultado(solicitudProgramaTipo.getValRutaArchivoRequerimiento());
        }
    }

    /**
     * Método que se encarga de armar la fuente informacion x solictud
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param solicitudPrograma la solicitud a la cual se asocia la fuente de información
     * @param fuenteInformacion la fuente de información que se va a asociar a la solicitud
     */
    private void buildFuenteInformacion(final ContextoTransaccionalTipo contextoTransaccionalTipo, final Solicitud solicitudPrograma,
            final ValorDominio fuenteInformacion) {

        SolicitudFuenteInformacion solicitudFuenteInformacion = new SolicitudFuenteInformacion();
        solicitudFuenteInformacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        solicitudFuenteInformacion.setCodFuenteInformacion(fuenteInformacion);
        solicitudFuenteInformacion.setSolicitud(solicitudPrograma);
        solicitudPrograma.getSolicitudFuenteInformacionList().add(solicitudFuenteInformacion);
    }
}
