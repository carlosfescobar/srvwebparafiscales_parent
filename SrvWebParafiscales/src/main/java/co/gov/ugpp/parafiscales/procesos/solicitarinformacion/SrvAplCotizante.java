package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.denuncias.srvaplcotizante.v1.MsjOpBuscarPorIdCotizanteFallo;
import co.gov.ugpp.denuncias.srvaplcotizante.v1.MsjOpCrearCotizanteFallo;
import co.gov.ugpp.denuncias.srvaplcotizante.v1.OpBuscarPorIdCotizanteRespTipo;
import co.gov.ugpp.denuncias.srvaplcotizante.v1.OpBuscarPorIdCotizanteSolTipo;
import co.gov.ugpp.denuncias.srvaplcotizante.v1.OpCrearCotizanteRespTipo;
import co.gov.ugpp.denuncias.srvaplcotizante.v1.OpCrearCotizanteSolTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplCotizante",
        portName = "portSrvAplCotizanteSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplcotizante.v1.PortSrvAplCotizanteSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplCotizante/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplCotizante extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplCotizante.class);

    @EJB
    private CotizanteFacade cotizanteFacade;

    public OpBuscarPorIdCotizanteRespTipo opBuscarPorIdCotizante(OpBuscarPorIdCotizanteSolTipo msjOpBuscarPorIdCotizanteSol) throws MsjOpBuscarPorIdCotizanteFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdCotizanteSol.getContextoTransaccional();

        final List<IdentificacionTipo> identificacionTipos = msjOpBuscarPorIdCotizanteSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<CotizanteTipo> cotizanteTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            cotizanteTipos = cotizanteFacade.findCotizanteByIdentificacion(identificacionTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdCotizanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdCotizanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdCotizanteRespTipo resp = new OpBuscarPorIdCotizanteRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getCotizante().addAll(cotizanteTipos);

        return resp;
    }

    public OpCrearCotizanteRespTipo opCrearCotizante(OpCrearCotizanteSolTipo msjOpCrearCotizanteSol) throws MsjOpCrearCotizanteFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearCotizanteSol.getContextoTransaccional();

        final CotizanteTipo cotizanteTipo = msjOpCrearCotizanteSol.getCotizante();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            cotizanteFacade.crearCotizante(cotizanteTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearCotizanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearCotizanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearCotizanteRespTipo resp = new OpCrearCotizanteRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
