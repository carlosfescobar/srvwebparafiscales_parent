package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.solicitudinformacion.agrupaciondocumentosolicitudtipo.v1.AgrupacionDocumentoSolicitudTipo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.MsjOpBuscarPorCriteriosDocumentoSolicitudFallo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.MsjOpCrearDocumentoSolicitudFallo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.OpBuscarPorCriteriosDocumentoSolicitudRespTipo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.OpBuscarPorCriteriosDocumentoSolicitudSolTipo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.OpCrearDocumentoSolicitudRespTipo;
import co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.OpCrearDocumentoSolicitudSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplDocumentoSolicitud",
        portName = "portSrvAplDocumentoSolicitudSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1.PortSrvAplDocumentoSolicitudSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplDocumentoSolicitud extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplDocumentoSolicitud.class);

    @EJB
    private DocumentoSolicitudFacade documentoSolicitudFacade;

    public OpBuscarPorCriteriosDocumentoSolicitudRespTipo opBuscarPorCriteriosDocumentoSolicitud(OpBuscarPorCriteriosDocumentoSolicitudSolTipo msjOpBuscarPorCriteriosDocumentoSolicitudSol) throws MsjOpBuscarPorCriteriosDocumentoSolicitudFallo {

        LOG.info("OPERACION: opBuscarPorCriteriosDocumentoSolicitud ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosDocumentoSolicitudSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosDocumentoSolicitudSol.getParametros();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AgrupacionDocumentoSolicitudTipo> agrupacionDocumentoSolicitudList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            agrupacionDocumentoSolicitudList = documentoSolicitudFacade.buscarPorCriteriosDocumentoSolicitud(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDocumentoSolicitudFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDocumentoSolicitudFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosDocumentoSolicitudRespTipo resp = new OpBuscarPorCriteriosDocumentoSolicitudRespTipo();

        cr.setValCantidadPaginas(agrupacionDocumentoSolicitudList.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getAgrupacionDocumentoSolicitud().addAll(agrupacionDocumentoSolicitudList.getData());

        LOG.info("OPERACION: opBuscarPorCriteriosDocumentoSolicitud ::: FIN");

        return resp;
    }

    public OpCrearDocumentoSolicitudRespTipo opCrearDocumentoSolicitud(OpCrearDocumentoSolicitudSolTipo msjOpCrearDocumentoSolicitudSol) throws MsjOpCrearDocumentoSolicitudFallo {

        LOG.info("OPERACION: opCrearDocumentoSolicitud ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearDocumentoSolicitudSol.getContextoTransaccional();
        final AgrupacionDocumentoSolicitudTipo agrupacionDocumentoSolicitudTipo = msjOpCrearDocumentoSolicitudSol.getAgrupacionDocumentoSolicitud();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            documentoSolicitudFacade.crearDocumentoSolicitud(agrupacionDocumentoSolicitudTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDocumentoSolicitudFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDocumentoSolicitudFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearDocumentoSolicitudRespTipo resp = new OpCrearDocumentoSolicitudRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opCrearDocumentoSolicitud ::: FIN");

        return resp;
    }
}
