package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.solicitarinformacion.efectividadtipo.v1.EfectividadTipo;
import co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1.MsjOpConsultarEfectividadEntidadExternaFallo;
import co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1.OpConsultarEfectividadEntidadExternaRespTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplEfectividad",
        portName = "portSrvAplEfectividadSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1.PortSrvAplEfectividadSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplEfectividad extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplEfectividad.class);

    @EJB
    private EfectividadFacade efectividadFacade;

    public co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1.OpConsultarEfectividadEntidadExternaRespTipo opConsultarEfectividadEntidadExterna(co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1.OpConsultarEfectividadEntidadExternaSolTipo msjOpConsultarEfectividadEntidadExternaSol) throws MsjOpConsultarEfectividadEntidadExternaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarEfectividadEntidadExternaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarEfectividadEntidadExternaSol.getParametros();
        final EntidadExternaTipo entidadExternaTipo = msjOpConsultarEfectividadEntidadExternaSol.getEntidadExterna();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final EfectividadTipo efectividadResult;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            efectividadResult = efectividadFacade.consultarEfectividad(parametroTipoList, entidadExternaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarEfectividadEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarEfectividadEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarEfectividadEntidadExternaRespTipo resp = new OpConsultarEfectividadEntidadExternaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setEfectividad(efectividadResult);

        return resp;

    }
}
