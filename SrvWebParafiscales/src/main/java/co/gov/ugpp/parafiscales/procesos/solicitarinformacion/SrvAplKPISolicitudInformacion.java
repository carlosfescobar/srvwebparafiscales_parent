package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1.MsjOpConsultarSolicitudKPIFallo;
import co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1.OpConsultarSolicitudKPIRespTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author lquintec
 */
@WebService(serviceName = "SrvAplKPISolicitudInformacion",
        portName = "portSrvAplKPISolicitudInformacionSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1.PortSrvAplKPISolicitudInformacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPISolicitudInformacion  extends AbstractSrvApl{

    @EJB
    private KPISolictudInformacionFacade KPISolicitudInformacionFacade;
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplKPISolicitudInformacion.class);    
    
    public co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1.OpConsultarSolicitudKPIRespTipo opConsultarSolicitudKPI(co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1.OpConsultarSolicitudKPISolTipo msjOpConsultarSolicitudKPISol) throws MsjOpConsultarSolicitudKPIFallo {
        
        LOG.info("OPERACION: opConsultarSolicitudKPI ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarSolicitudKPISol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpConsultarSolicitudKPISol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SolicitudInformacionTipo> solicitudTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudTipoPagerData = KPISolicitudInformacionFacade.consultarSolicitudKPI(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarSolicitudKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarSolicitudKPIFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpConsultarSolicitudKPIRespTipo resp = new OpConsultarSolicitudKPIRespTipo();

        cr.setValCantidadPaginas(solicitudTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSolicitudInformacion().addAll(solicitudTipoPagerData.getData());
        resp.setCantidadSolicitudes(Integer.toString(solicitudTipoPagerData.getData().size()));

        LOG.info("OPERACION: opConsultarSolicitudKPI ::: FIN");

        return resp;
    }

}
