package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.MsjOpActualizarNoAportanteFallo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.MsjOpBuscarPorIdNoAportanteFallo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.OpActualizarNoAportanteRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.OpActualizarNoAportanteSolTipo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.OpBuscarPorIdNoAportanteRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.OpBuscarPorIdNoAportanteSolTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodriguez
 */
@WebService(serviceName = "SrvAplNoAportante",
    portName = "portSrvAplNoAportanteSOAP",
    endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1.PortSrvAplNoAportanteSOAP",
    targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplNoAportante extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplNoAportante.class);

    @EJB
    private NoAportanteFacade noAportanteFacade;

    public OpBuscarPorIdNoAportanteRespTipo opBuscarPorIdNoAportante(OpBuscarPorIdNoAportanteSolTipo msjOpBuscarPorIdNoAportanteSol) throws MsjOpBuscarPorIdNoAportanteFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdNoAportanteSol.getContextoTransaccional();
        final IdentificacionTipo identificacionTipo = msjOpBuscarPorIdNoAportanteSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        

        final NoAportanteTipo noAportanteTipo;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            noAportanteTipo = noAportanteFacade.findNoAportanteByIdentificacion(identificacionTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdNoAportanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdNoAportanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdNoAportanteRespTipo resp = new OpBuscarPorIdNoAportanteRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setNoAportante(noAportanteTipo);

        return resp;
    }

    public OpActualizarNoAportanteRespTipo opActualizarNoAportante(OpActualizarNoAportanteSolTipo msjOpActualizarNoAportanteSol) throws MsjOpActualizarNoAportanteFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarNoAportanteSol.getContextoTransaccional();
        final NoAportanteTipo noAportanteTipo = msjOpActualizarNoAportanteSol.getNoAportante();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();


        try {
                    this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            noAportanteFacade.actualizarNoAportante(noAportanteTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarNoAportanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarNoAportanteFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarNoAportanteRespTipo resp = new OpActualizarNoAportanteRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
