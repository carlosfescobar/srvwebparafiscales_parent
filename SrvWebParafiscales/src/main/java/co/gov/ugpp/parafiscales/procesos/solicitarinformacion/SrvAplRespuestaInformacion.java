package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.FileStructureUtili;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.MsjOpAlmacenarRespuestaInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.MsjOpCruzarRespuestaInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.MsjOpValidarRespuestaInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpAlmacenarRespuestaInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpAlmacenarRespuestaInformacionSolTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpCruzarRespuestaInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpCruzarRespuestaInformacionSolTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpValidarRespuestaInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.OpValidarRespuestaInformacionSolTipo;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.WebServiceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author vgarcigu
 */
@WebService(serviceName = "SrvAplRespuestaInformacion",
        portName = "portSrvAplRespuestaInformacionSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1.PortSrvAplRespuestaInformacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplRespuestaInformacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRespuestaInformacion.class);

    @EJB
    private RespuestaInformacionFacade respuestaInformacionFacade;

    @Resource
    private WebServiceContext context;

    public OpValidarRespuestaInformacionRespTipo opValidarRespuestaInformacion(OpValidarRespuestaInformacionSolTipo msjOpValidarRespuestaInformacionSol) throws MsjOpValidarRespuestaInformacionFallo, IOException {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpValidarRespuestaInformacionSol.getContextoTransaccional();
        final FormatoEstructuraTipo formatoEstructuraTipo = msjOpValidarRespuestaInformacionSol.getFormatoEstructura();
        final IdentificacionTipo identificacionTipo = msjOpValidarRespuestaInformacionSol.getIdentificacion();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final OpValidarRespuestaInformacionRespTipo resp = new OpValidarRespuestaInformacionRespTipo();
        InputStream is = (InputStream) context.getMessageContext().get("archivo");
        if (is != null) {
            byte[] bytes = FileStructureUtili.getBytes(is);
            formatoEstructuraTipo.getArchivo().setValContenidoArchivo(bytes);
        }

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaInformacionFacade.validarRespuestaInformacion(formatoEstructuraTipo, identificacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } /*finally {
         auditoriaFacade.register(contextoTransaccionalTipo);
         }*/

        resp.setContextoRespuesta(cr);
        return resp;
    }

    public OpAlmacenarRespuestaInformacionRespTipo opAlmacenarRespuestaInformacion(OpAlmacenarRespuestaInformacionSolTipo msjOpAlmacenarRespuestaInformacionSol) throws MsjOpAlmacenarRespuestaInformacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpAlmacenarRespuestaInformacionSol.getContextoTransaccional();
        final EnvioTipo envioTipo = msjOpAlmacenarRespuestaInformacionSol.getEnvio();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaInformacionFacade.almacenarRespuestaInformacion(envioTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAlmacenarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAlmacenarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } /*finally {
         auditoriaFacade.register(contextoTransaccionalTipo);
         }*/

        final OpAlmacenarRespuestaInformacionRespTipo resp = new OpAlmacenarRespuestaInformacionRespTipo();

        resp.setContextoRespuesta(cr);
        return resp;
    }

    public OpCruzarRespuestaInformacionRespTipo opCruzarRespuestaInformacion(OpCruzarRespuestaInformacionSolTipo msjOpCruzarRespuestaInformacionSol) throws MsjOpCruzarRespuestaInformacionFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCruzarRespuestaInformacionSol.getContextoTransaccional();
        final RadicacionTipo radicacionTipo = msjOpCruzarRespuestaInformacionSol.getRadicacion();
        final FormatoEstructuraTipo formatoEstructuraTipo = msjOpCruzarRespuestaInformacionSol.getFormatoEstructura();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        List<SolicitudInformacionTipo> solicitudInformacionTipoReturnList = null;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudInformacionTipoReturnList
                    = respuestaInformacionFacade.cruzarRespuestaInformacion(radicacionTipo, formatoEstructuraTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarRespuestaInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }/* finally {
         auditoriaFacade.register(contextoTransaccionalTipo);
         }*/

        final OpCruzarRespuestaInformacionRespTipo resp = new OpCruzarRespuestaInformacionRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getSolicitudInformacion().addAll(solicitudInformacionTipoReturnList);
        return resp;
    }

}
