package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpActualizarSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpBuscarPorCriteriosSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpBuscarPorIdSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpCrearFormatoSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpCrearSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpActualizarSolicitudInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpActualizarSolicitudInformacionSolTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorCriteriosSolicitudInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorIdSolicitudInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpCrearFormatoSolicitudInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpCrearSolicitudInformacionRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpCrearSolicitudInformacionSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplSolicitudInformacion",
        portName = "portSrvAplSolicitudInformacionSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.PortSrvAplSolicitudInformacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplSolicitudInformacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSolicitudInformacion.class);

    @EJB
    private SolicitudInformacionFacade solicitudInformacionFacade;

    public co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorIdSolicitudInformacionRespTipo opBuscarPorIdSolicitudInformacion(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorIdSolicitudInformacionSolTipo msjOpBuscarPorIdSolicitudInformacionSol) throws MsjOpBuscarPorIdSolicitudInformacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdSolicitudInformacionSol.getContextoTransaccional();
        final List<String> idListNumSolicitudesInformacion = msjOpBuscarPorIdSolicitudInformacionSol.getIdNumSolicitud();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<SolicitudInformacionTipo> solicitudInformacionTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            solicitudInformacionTipoList
                    = solicitudInformacionFacade.buscarPorIdListSolicitudInformacion(idListNumSolicitudesInformacion,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdSolicitudInformacionRespTipo resp = new OpBuscarPorIdSolicitudInformacionRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getSolicitudInformacion().addAll(solicitudInformacionTipoList);

        return resp;
    }

    public OpCrearSolicitudInformacionRespTipo opCrearSolicitudInformacion(OpCrearSolicitudInformacionSolTipo msjOpCrearSolicitudInformacionSol) throws MsjOpCrearSolicitudInformacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSolicitudInformacionSol.getContextoTransaccional();
        final SolicitudInformacionTipo solicitudInformacionTipo = msjOpCrearSolicitudInformacionSol.getSolicitudInformacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long solicitudInformacionPk;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            solicitudInformacionPk = solicitudInformacionFacade.crearSolicitudInformacion(solicitudInformacionTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearSolicitudInformacionRespTipo resp = new OpCrearSolicitudInformacionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSolicitudInformacion(solicitudInformacionPk.toString());

        return resp;
    }

    public OpActualizarSolicitudInformacionRespTipo opActualizarSolicitudInformacion(OpActualizarSolicitudInformacionSolTipo msjOpActualizarSolicitudInformacionSol) throws MsjOpActualizarSolicitudInformacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSolicitudInformacionSol.getContextoTransaccional();
        final SolicitudInformacionTipo solicitudInformacionTipo = msjOpActualizarSolicitudInformacionSol.getSolicitudInformacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudInformacionFacade.actualizarSolicitudInformacion(solicitudInformacionTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarSolicitudInformacionRespTipo resp = new OpActualizarSolicitudInformacionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpCrearFormatoSolicitudInformacionRespTipo opCrearFormatoSolicitudInformacion(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpCrearFormatoSolicitudInformacionSolTipo msjOpCrearFormatoSolicitudInformacionSol) throws MsjOpCrearFormatoSolicitudInformacionFallo {
        throw new UnsupportedOperationException("No se ha definido el funcionamiento de esta operacion, por eso no se ha implementado aun.");
    }

    public OpBuscarPorCriteriosSolicitudInformacionRespTipo opBuscarPorCriteriosSolicitudInformacion(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorCriteriosSolicitudInformacionSolTipo msjOpBuscarPorCriteriosSolicitudInformacionSol) throws MsjOpBuscarPorCriteriosSolicitudInformacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosSolicitudInformacionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosSolicitudInformacionSol.getParametros();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<SolicitudInformacionTipo> solicitudInformacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudInformacionTipoPagerData = solicitudInformacionFacade.buscarPorCriteriosSolicitudInformacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosSolicitudInformacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosSolicitudInformacionRespTipo resp = new OpBuscarPorCriteriosSolicitudInformacionRespTipo();

        cr.setValCantidadPaginas(solicitudInformacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getSolicitudesDeInformacion().addAll(solicitudInformacionTipoPagerData.getData());

        return resp;
    }
}
