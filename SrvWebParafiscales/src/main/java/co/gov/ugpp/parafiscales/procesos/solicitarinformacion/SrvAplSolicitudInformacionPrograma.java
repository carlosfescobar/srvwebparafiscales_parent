package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1.SolicitudProgramaTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.MsjOpActualizarSolicitudInformacionProgramaFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.MsjOpBuscarPorIdSolicitudInformacionProgramaFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.MsjOpCrearFormatoSolicitudProgramaFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.MsjOpCrearSolicitudInformacionProgramaFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpActualizarSolicitudInformacionProgramaRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpActualizarSolicitudInformacionProgramaSolTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpBuscarPorIdSolicitudInformacionProgramaRespTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpCrearSolicitudInformacionProgramaRespTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplSolicitudInformacionPrograma",
        portName = "portSrvAplSolicitudInformacionProgramaSOAP",
        endpointInterface = "co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.PortSrvAplSolicitudInformacionProgramaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplSolicitudInformacionPrograma extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplSolicitudInformacionPrograma.class);

    @EJB
    private SolicitudInformacionProgramaFacade solicitudInformacionProgramaFacade;

    public co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpBuscarPorIdSolicitudInformacionProgramaRespTipo opBuscarPorIdSolicitudInformacionPrograma(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpBuscarPorIdSolicitudInformacionProgramaSolTipo msjOpBuscarPorIdSolicitudInformacionProgramaSol) throws MsjOpBuscarPorIdSolicitudInformacionProgramaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdSolicitudInformacionProgramaSol.getContextoTransaccional();
        final List<String> idListNumSolicitud = msjOpBuscarPorIdSolicitudInformacionProgramaSol.getIdNumSolicitud();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<SolicitudProgramaTipo> solicitudProgramaTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            solicitudProgramaTipoList = solicitudInformacionProgramaFacade.buscarPorIdListSolicitudPrograma(idListNumSolicitud, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdSolicitudInformacionProgramaRespTipo resp = new OpBuscarPorIdSolicitudInformacionProgramaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getSolicitudPrograma().addAll(solicitudProgramaTipoList);

        return resp;
    }

    public co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpCrearSolicitudInformacionProgramaRespTipo opCrearSolicitudInformacionPrograma(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpCrearSolicitudInformacionProgramaSolTipo msjOpCrearSolicitudInformacionProgramaSol) throws MsjOpCrearSolicitudInformacionProgramaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearSolicitudInformacionProgramaSol.getContextoTransaccional();
        final SolicitudProgramaTipo solicitudInformacionProgramaTipo = msjOpCrearSolicitudInformacionProgramaSol.getSolicitudPrograma();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        
        final Long solicitudInformacionProgramaPk;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            solicitudInformacionProgramaPk = solicitudInformacionProgramaFacade.crearSolicitudInformacionPrograma(solicitudInformacionProgramaTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearSolicitudInformacionProgramaRespTipo resp = new OpCrearSolicitudInformacionProgramaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdSolicitudInformacionPrograma(solicitudInformacionProgramaPk.toString());

        return resp;
    }

    public OpActualizarSolicitudInformacionProgramaRespTipo opActualizarSolicitudInformacionPrograma(OpActualizarSolicitudInformacionProgramaSolTipo msjOpActualizarSolicitudInformacionProgramaSol) throws MsjOpActualizarSolicitudInformacionProgramaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarSolicitudInformacionProgramaSol.getContextoTransaccional();
        final SolicitudProgramaTipo solicitudProgramaTipo = msjOpActualizarSolicitudInformacionProgramaSol.getSolicitudPrograma();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            solicitudInformacionProgramaFacade.actualizarSolicitudInformacionPrograma(solicitudProgramaTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarSolicitudInformacionProgramaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarSolicitudInformacionProgramaRespTipo resp = new OpActualizarSolicitudInformacionProgramaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpCrearFormatoSolicitudProgramaRespTipo opCrearFormatoSolicitudPrograma(co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1.OpCrearFormatoSolicitudProgramaSolTipo msjOpCrearFormatoSolicitudProgramaSol) throws MsjOpCrearFormatoSolicitudProgramaFallo {
        throw new UnsupportedOperationException("No se ha definido el funcionamiento de esta operacion, por eso no se ha implementado aun.");
    }
}
