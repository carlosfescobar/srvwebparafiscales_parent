package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.ExcelUtil;
import co.gov.ugpp.parafiscales.util.FileStructureUtili;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook; 

/**
 * implementación de la interfaz ValidacionFormatoFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
public class ValidacionEstructuraBancosImpl extends AbstractFacade implements ValidacionFormatoFacade {

    /**
     * implementación de la operación validarEstructura se encarga de validar la
     * estructura del documento enviado
     *
     * @param wb recibe el documento enviado que contiene la información
     * requerida
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void validarEstructura(final Workbook wb, final List<ErrorTipo> errorTipoList) throws AppException {

        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();
        if (!rows.hasNext()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No se ha procesado el archivo enviado ya que no contiene datos"));
        }
        while (rows.hasNext()) {
            Row row = (Row) rows.next();
            if (row.getRowNum() == 0) {
                this.validadarEstructuraEncabezadoBancos(row, errorTipoList);
                if (rows.hasNext()) {
                    row = rows.next();
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "Es necesario enviar datos en el formato"));
                    break;
                }
            }
            //Se obtienen las celdas a iterar
            Iterator cells = row.cellIterator();

            //Se valida la obligatoriedad de las celdas
            if (ExcelUtil.evaluarFila(row, Constants.FORMATO_BANCOS_COLUMNA_INICIO, Constants.FORMATO_BANCOS_COLUMNA_FIN)) {

                //Se evalúan los requeridos
                ExcelUtil.evaluarRequeridos(row, ExcelUtil.getEncabezadosBancos(), errorTipoList);

                //Se valida el tipo de dato enviado en cada celda
                while (cells.hasNext()) {
                    Cell cell = (Cell) cells.next();

                    if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * implemetación de la operación validadarEstructuraEncabezadoBancos se
     * encarga de validar cada una de las posiciones del documento enviado
     *
     * @param row recibe cada una de las posiciones a validar del documento
     * enviado
     * @param errorTipoList Listado que almacena errores de negocio
     */
    private void validadarEstructuraEncabezadoBancos(final Row row, final List<ErrorTipo> errorTipoList) {

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES.getNombreColumna()));
        }
    }
}
