package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.FileStructureEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.ExcelUtil;
import co.gov.ugpp.parafiscales.util.FileStructureUtili;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;  

/**
 * implementación de la interfaz ValidacionFormatoFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
public class ValidacionEstructuraSAYPImpl extends AbstractFacade implements ValidacionFormatoFacade {

    /**
     * implementación de la operación validarEstructura se encarga de validar la
     * estructura del documento enviado
     *
     * @param wb recibe el documento enviado que contiene la información
     * requerida
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void validarEstructura(final Workbook wb, final List<ErrorTipo> errorTipoList) throws AppException {

        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();
        if (!rows.hasNext()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No se ha procesado el archivo enviado ya que no contiene datos"));
        }

        while (rows.hasNext()) {
            Row row = (Row) rows.next();
            if (row.getRowNum() == 0) {
                this.validadarEstructuraEncabezadoSAYP(row, errorTipoList);
                if (rows.hasNext()) {
                    row = rows.next();
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "Es necesario enviar datos en el formato"));
                    break;
                }
            }
            //Se obtienen las celdas a iterar
            Iterator cells = row.cellIterator();

            //Se valida la obligatoriedad de las celdas
            if (ExcelUtil.evaluarFila(row, Constants.FORMATO_PILA_COLUMNA_INICIO, Constants.FORMATO_PILA_COLUMNA_FIN)) {

                //Se evalúan los requeridos
                ExcelUtil.evaluarRequeridos(row, ExcelUtil.getEncabezadosSAYP(), errorTipoList);

                //Se valida el tipo de dato enviado en cada celda
                while (cells.hasNext()) {
                    Cell cell = (Cell) cells.next();

                    if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getIndex()) {

                        if ((cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (!HSSFDateUtil.isCellDateFormatted(cell)) {
                                String valorNumerico = Long.toString(Math.round(cell.getNumericCellValue()));
                                if (!valorNumerico.equals(Constants.INFORMACION_NO_DISPONIBLE)) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO));
                                }
                            }
                        } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getIndex()
                                && cell.getCellType() == Cell.CELL_TYPE_STRING) {
                            String valorString = cell.getStringCellValue();

                            if (!valorString.equals(Constants.INFORMACION_NO_DISPONIBLE)) {
                                if (!DateUtil.validarFormatoFecha(valorString)) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * implemetación de la operación validadarEstructuraEncabezadoSAYP se
     * encarga de validar cada una de las posiciones del documento enviado
     *
     * @param row recibe cada una de las posiciones a validar del documento
     * enviado
     * @param errorTipoList Listado que almacena errores de negocio
     */
    private void validadarEstructuraEncabezadoSAYP(final Row row, final List<ErrorTipo> errorTipoList) {

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getIndex()).getStringCellValue().equals(FileStructureEnum.VAL_NUMERO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(FileStructureEnum.VAL_NOMBRE_RAZON_SOCIAL.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_SAYP_CELDA_RAZON_SOCIAL).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_SAYP_CELDA_VALOR_PAGADO).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES.getNombreColumna()));
        }
    }
}
