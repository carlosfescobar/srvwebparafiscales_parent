package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.FileStructureEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.ExcelUtil;
import co.gov.ugpp.parafiscales.util.FileStructureUtili;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;  
/**
 * implementación de la interfaz ValidacionFormatoFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
public class ValidacionEstructuraTelefoniaImpl extends AbstractFacade implements ValidacionFormatoFacade {

    /**
     * implementación de la operación validarEstructura se encarga de validar la
     * estructura del documento enviado
     *
     * @param wb ecibe el documento enviado que contiene la información
     * requerida
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void validarEstructura(final Workbook wb, final List<ErrorTipo> errorTipoList) throws AppException {

        Iterator<Row> rows = wb.getSheetAt(Constants.NUMERO_HOJA_EXCEL_EVALUAR).rowIterator();

        if (!rows.hasNext()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                    "No se ha procesado el archivo enviado ya que no contiene datos"));
        }
        while (rows.hasNext()) {
            Row row = (Row) rows.next();
            if (row.getRowNum() == 0) {
                this.validadarEstructuraEncabezadoTelefonia(row, errorTipoList);
                if (rows.hasNext()) {
                    row = rows.next();
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "Es necesario enviar datos en el formato"));
                    break;
                }
            }
            //Se obtienen las celdas a iterar
            Iterator cells = row.cellIterator();

            //Se valida la obligatoriedad de las celdas
            if (ExcelUtil.evaluarFila(row, Constants.FORMATO_TELEFONIA_COLUMNA_INICIO, Constants.FORMATO_TELEFONIA_COLUMNA_FIN)) {

                //Se evalúan los requeridos
                ExcelUtil.evaluarRequeridos(row, ExcelUtil.getEncabezadosTelefonia(), errorTipoList);

                //Se valida el tipo de dato enviado en cada celda
                while (cells.hasNext()) {
                    Cell cell = (Cell) cells.next();

                    if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                                try {
                                    Long.parseLong(cell.getStringCellValue());
                                } catch (NumberFormatException nfe) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                            EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX));
                                }
                            } else {
                                errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                        EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX));
                            }
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS));
                        }
                    } else if (cell.getColumnIndex() == EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getIndex()) {
                        if (!(cell.getCellType() == Cell.CELL_TYPE_BLANK) && !(cell.getCellType() == Cell.CELL_TYPE_STRING)
                                && !(cell.getCellType() == Cell.CELL_TYPE_NUMERIC)) {
                            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum() + 1,
                                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL));
                        }
                    }
                }
            }
        }
    }

    /**
     * implemetación de la operación validadarEstructuraEncabezadoTelefonia se
     * encarga de validar cada una de las posiciones del documento enviado
     *
     * @param row recibe cada una de las posiciones a validar del documento
     * enviado
     * @param errorTipoList Listado que almacena errores de negocio
     */
    public void validadarEstructuraEncabezadoTelefonia(final Row row, final List<ErrorTipo> errorTipoList) {

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO.getIndex()).getStringCellValue().equals(FileStructureEnum.VAL_DEPARTAMENTO1.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_CIUDAD).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(Constants.FORMATO_TELEFONIA_CELDA_CELULAR).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL.getNombreColumna()));
        }

        if (row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getIndex()) == null) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getNombreColumna()));
        } else if (!row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getIndex()).getStringCellValue().equals(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getNombreColumna())) {
            errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getCell(EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getIndex()).getColumnIndex() + 1,
                    EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX.getNombreColumna()));
        }
    }
}
