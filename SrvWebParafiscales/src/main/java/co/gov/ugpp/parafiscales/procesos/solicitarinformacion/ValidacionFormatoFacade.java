package co.gov.ugpp.parafiscales.procesos.solicitarinformacion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.List;
import org.apache.poi.ss.usermodel.Workbook;

/**
 *
 * @author jmuncab
 */
public interface ValidacionFormatoFacade extends Serializable {

    void validarEstructura(final Workbook wb, final List<ErrorTipo> errorTipoList) throws AppException;
}
