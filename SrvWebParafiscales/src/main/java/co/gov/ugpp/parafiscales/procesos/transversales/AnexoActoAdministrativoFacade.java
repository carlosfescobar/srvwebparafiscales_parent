package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.agrupacionactoadministrativoanexotipo.v1.AgrupacionActoAdministrativoAnexoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface AnexoActoAdministrativoFacade extends Serializable {

    void crearAnexoActoAdministrativo(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final AgrupacionActoAdministrativoAnexoTipo agrupacionActoAdministrativoAnexoTipo) throws AppException;

    void actualizarAnexoActoAdministrativo(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final AgrupacionActoAdministrativoAnexoTipo agrupacionActoAdministrativoAnexoTipo) throws AppException;

    PagerData<AgrupacionActoAdministrativoAnexoTipo> buscarPorCriteriosAnexoActoAdministrativo(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
