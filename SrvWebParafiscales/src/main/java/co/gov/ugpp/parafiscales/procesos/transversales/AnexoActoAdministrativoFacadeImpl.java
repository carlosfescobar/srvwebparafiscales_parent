package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AnexoActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativoDocumentoAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.schema.transversales.agrupacionactoadministrativoanexotipo.v1.AgrupacionActoAdministrativoAnexoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz AnexoActoAdministrativoFacade que contiene las
 * operaciones del servicio SrvAplAnexoActoAdministrativo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AnexoActoAdministrativoFacadeImpl extends AbstractFacade implements AnexoActoAdministrativoFacade {

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private AnexoActoAdministrativoDao anexoActoAdministrativoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * Método que implementa la operación OpCrearEntidadNegocio del servicio
     * SrvAplAnexoActoAdministrativo
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param agrupacionActoAdministrativoAnexoTipo los documentos asociados al
     * acto administrativo
     * @throws AppException
     */
    @Override
    public void crearAnexoActoAdministrativo(ContextoTransaccionalTipo contextoTransaccionalTipo, AgrupacionActoAdministrativoAnexoTipo agrupacionActoAdministrativoAnexoTipo) throws AppException {
        if (agrupacionActoAdministrativoAnexoTipo == null || agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo() == null
                || StringUtils.isBlank(agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo())
                || agrupacionActoAdministrativoAnexoTipo.getIdDocumentosAnexos() == null || agrupacionActoAdministrativoAnexoTipo.getIdDocumentosAnexos().isEmpty()) {
            throw new AppException("Debe proporcionar el Acto Administrativo y los Documentos a asociar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo());
        if (actoAdministrativo == null) {
            throw new AppException("No existe el Acto Administrativo con el ID: " + agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo());
        }
        populateAnexoActoAdministrativo(agrupacionActoAdministrativoAnexoTipo, actoAdministrativo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        actoAdministrativoDao.edit(actoAdministrativo);
    }

    /**
     * Método que implementa la operación OpActualizarEntidadNegocio del
     * servicio SrvAplAnexoActoAdministrativo
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param agrupacionActoAdministrativoAnexoTipo los documentos anexos al
     * acto administrativo
     * @throws AppException
     */
    @Override
    public void actualizarAnexoActoAdministrativo(ContextoTransaccionalTipo contextoTransaccionalTipo, AgrupacionActoAdministrativoAnexoTipo agrupacionActoAdministrativoAnexoTipo) throws AppException {
        if (agrupacionActoAdministrativoAnexoTipo == null || agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo() == null
                || StringUtils.isBlank(agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo())
                || agrupacionActoAdministrativoAnexoTipo.getIdDocumentosAnexos() == null || agrupacionActoAdministrativoAnexoTipo.getIdDocumentosAnexos().isEmpty()) {
            throw new AppException("Debe proporcionar el Acto Administrativo y los Documentos a asociar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo());
        if (actoAdministrativo == null) {
            throw new AppException("No existe el Acto Administrativo con el ID: " + agrupacionActoAdministrativoAnexoTipo.getActoAdministrativo().getIdActoAdministrativo());
        }
        populateAnexoActoAdministrativo(agrupacionActoAdministrativoAnexoTipo, actoAdministrativo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        actoAdministrativoDao.edit(actoAdministrativo);
    }

    /**
     * Método que implementa la operación OpBuscarPorCriterios del servicio
     * SrvAplAnexoActoAdministrativo
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param parametroTipoList los parámetros de búqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @return Los documentos anexos al acto encontrados
     * @throws AppException
     */
    @Override
    public PagerData<AgrupacionActoAdministrativoAnexoTipo> buscarPorCriteriosAnexoActoAdministrativo(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ActoAdministrativo> actoAdministrativoPagerData = actoAdministrativoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        final List<AgrupacionActoAdministrativoAnexoTipo> documentosActoAdministrativo = mapper.map(actoAdministrativoPagerData.getData(), ActoAdministrativo.class, AgrupacionActoAdministrativoAnexoTipo.class);
        return new PagerData<AgrupacionActoAdministrativoAnexoTipo>(documentosActoAdministrativo, actoAdministrativoPagerData.getNumPages());
    }

    /**
     * Métod que llena los documentos anexos al acto administrativo
     *
     * @param anexosActoAdministrativo lso documentos anexos al acto
     * administrativo
     * @param administrativo el acto administrativo a poblar
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList
     */
    private void populateAnexoActoAdministrativo(final AgrupacionActoAdministrativoAnexoTipo anexosActoAdministrativo,
            final ActoAdministrativo actoAdministrativo, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (anexosActoAdministrativo.getIdDocumentosAnexos() != null && !anexosActoAdministrativo.getIdDocumentosAnexos().isEmpty()) {
            for (ParametroValoresTipo documentosAnexos : anexosActoAdministrativo.getIdDocumentosAnexos()) {
                if (StringUtils.isNotBlank(documentosAnexos.getIdLlave())) {
                    ValorDominio codTipoDocumentoAnexo = valorDominioDao.find(documentosAnexos.getIdLlave());
                    if (codTipoDocumentoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoDocumentoAnexo con el ID: " + documentosAnexos.getIdLlave()));
                    } else {
                        if (documentosAnexos.getValValor() != null && !documentosAnexos.getValValor().isEmpty()) {
                            for (ParametroTipo documentoAnexo : documentosAnexos.getValValor()) {
                                DocumentoEcm documentoAnexoEncontrado = documentoDao.find(documentoAnexo.getIdLlave());
                                if (documentoAnexoEncontrado == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró Documento Anexo con el ID: " + documentoAnexo.getIdLlave()));
                                } else {
                                    ActoAdministrativoDocumentoAnexo documentoAnexoActo = anexoActoAdministrativoDao.findByActoAndDocumento(actoAdministrativo.getId(), documentoAnexo.getIdLlave());
                                    if (documentoAnexoActo != null) {
                                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                                "Ya existe la relación entre el Documento Anexo con el ID: " + documentoAnexo.getIdLlave() + " y el Acto Administrativo con ID: " + actoAdministrativo.getId()));
                                    } else {
                                        documentoAnexoActo = new ActoAdministrativoDocumentoAnexo();
                                        documentoAnexoActo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                        documentoAnexoActo.setActoAdministrativo(actoAdministrativo);
                                        documentoAnexoActo.setDocumentoAnexo(documentoAnexoEncontrado);
                                        documentoAnexoActo.setCodTipoAnexoActoAdministrativo(codTipoDocumentoAnexo);
                                        actoAdministrativo.getDocumentosAnexos().add(documentoAnexoActo);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoDocumentoAnexo")));
                }
            }
        }
    }
}
