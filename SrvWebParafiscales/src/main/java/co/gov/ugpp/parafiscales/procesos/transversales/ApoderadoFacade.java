package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.apoderadotipo.v1.ApoderadoTipo;
import java.io.Serializable;


/**
 *
 * @author cpatingo
 */
public interface ApoderadoFacade extends Serializable{
        ApoderadoTipo findApoderadoByIdentificacion(IdentificacionTipo identificacionTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;   
}
