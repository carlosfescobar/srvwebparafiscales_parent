/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.PersonaAssembler;
import co.gov.ugpp.parafiscales.servicios.util.SQLConection;
import co.gov.ugpp.schema.transversales.apoderadotipo.v1.ApoderadoTipo;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@Stateless
@TransactionManagement
public class ApoderadoFacadeImpl extends AbstractFacade implements ApoderadoFacade {

    private static final Logger LOG = LoggerFactory.getLogger(ApoderadoFacadeImpl.class);

    @EJB
    private PersonaDao personaDao;

    private SQLConection sqlConection = SQLConection.getInstance();

    PersonaAssembler personaAssembler = PersonaAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();

    /**
     * implementación de la operación OpBuscarPorIdAportante esta se encarga de
     * buscar un Aportante de acuerdo a la identificación enviada.
     *
     * @param id
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return Listado de cotizantes que se retorna junto con el contexto
     * transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public ApoderadoTipo findApoderadoByIdentificacion(IdentificacionTipo id, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (id != null) {
            if (StringUtils.isBlank(id.getCodTipoIdentificacion()) || StringUtils.isBlank(id.getValNumeroIdentificacion())) {
                throw new AppException("Debe proporcionar un ID para el Apoderado");
            }
        }
            ApoderadoTipo apoderadoTipo = null;
            Identificacion identificacion = mapper.map(id, Identificacion.class);
            Persona persona = personaDao.findByIdentificacion(identificacion);
            
            if (persona != null) {
                apoderadoTipo = mapper.map(persona, ApoderadoTipo.class);
            }
            return apoderadoTipo;
        }
}
