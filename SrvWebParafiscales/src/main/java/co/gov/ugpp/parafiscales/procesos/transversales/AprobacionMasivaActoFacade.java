package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */

public interface AprobacionMasivaActoFacade extends Serializable {

    Long crearAprobacionMasivaActo(AprobacionMasivaActoTipo aprobacionMasivaActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<AprobacionMasivaActoTipo> buscarPorCriteriosAprobacionMasivaActo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarAprobacionMasivaActo(AprobacionMasivaActoTipo aprobacionMasivaActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
