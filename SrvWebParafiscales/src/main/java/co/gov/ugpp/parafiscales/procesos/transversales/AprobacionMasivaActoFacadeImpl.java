package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionMasivaActoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionMasivaActo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmunocab
 */
@Stateless
@TransactionManagement
public class AprobacionMasivaActoFacadeImpl extends AbstractFacade implements AprobacionMasivaActoFacade {

    @EJB
    private AprobacionMasivaActoDao aprobacionMasivaActoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    /**
     * Método que implementa la operacion crearAprobacionMasivaActo
     *
     * @param aprobacionMasivaActoTipo el objeto a crear
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return el identificador generado en base de datos
     * @throws AppException
     */
    @Override
    public Long crearAprobacionMasivaActo(AprobacionMasivaActoTipo aprobacionMasivaActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (aprobacionMasivaActoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        AprobacionMasivaActo aprobacionMasivaActo = new AprobacionMasivaActo();
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        this.populateAprobacionMasivaActo(aprobacionMasivaActoTipo, aprobacionMasivaActo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        aprobacionMasivaActo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        Long idAprobacionMasiva = aprobacionMasivaActoDao.create(aprobacionMasivaActo);
        return idAprobacionMasiva;
    }

    /**
     * Se implementa la operacion buscar por criterios aprobacion masiva acto
     *
     * @param parametroTipoList los parametros de búsqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return los objetos encontrados en base de datos
     * @throws AppException
     */
    @Override
    public PagerData<AprobacionMasivaActoTipo> buscarPorCriteriosAprobacionMasivaActo(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final PagerData<AprobacionMasivaActo> pagerDataEntity = aprobacionMasivaActoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        final List<AprobacionMasivaActoTipo> aprobacionMasivaActoTipoList = mapper.map(pagerDataEntity.getData(),
                AprobacionMasivaActo.class, AprobacionMasivaActoTipo.class);
        if (aprobacionMasivaActoTipoList != null) {
            for (AprobacionMasivaActoTipo aprobacionMasivaActo : aprobacionMasivaActoTipoList) {
                if (aprobacionMasivaActo.getIdFuncionarioResponsable() != null
                        && StringUtils.isNotBlank(aprobacionMasivaActo.getIdFuncionarioResponsable().getIdFuncionario())) {
                    FuncionarioTipo funcionarioResponsable = funcionarioFacade.buscarFuncionario(aprobacionMasivaActo.getIdFuncionarioResponsable(), contextoTransaccionalTipo);
                    aprobacionMasivaActo.setIdFuncionarioResponsable(funcionarioResponsable);
                }
                if (aprobacionMasivaActo.getActosAdministrativosPorAprobar() != null
                        && !aprobacionMasivaActo.getActosAdministrativosPorAprobar().isEmpty()) {
                    for (ControlAprobacionActoTipo controlAprobacionActoTipo : aprobacionMasivaActo.getActosAdministrativosPorAprobar()){
                        if (controlAprobacionActoTipo.getIdFuncionarioApruebaActo() != null
                                && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioApruebaActo().getIdFuncionario())){
                            FuncionarioTipo funcionarioApruebaActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioApruebaActo(), contextoTransaccionalTipo);
                            controlAprobacionActoTipo.setIdFuncionarioApruebaActo(funcionarioApruebaActo);
                        }
                        if (controlAprobacionActoTipo.getIdFuncionarioElaboraActo() != null
                                && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioElaboraActo().getIdFuncionario())){
                            FuncionarioTipo funcionarioElaboraActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioElaboraActo(), contextoTransaccionalTipo);
                            controlAprobacionActoTipo.setIdFuncionarioElaboraActo(funcionarioElaboraActo);
                        }
                    }
                }
            }
        }
        return new PagerData<AprobacionMasivaActoTipo>(aprobacionMasivaActoTipoList, pagerDataEntity.getNumPages());
    }

    /**
     * Se implementa operacion de actualizar aprobacion masiva de actos
     *
     * @param aprobacionMasivaActoTipo el objeto a actualizar
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @throws AppException
     */
    @Override
    public void actualizarAprobacionMasivaActo(AprobacionMasivaActoTipo aprobacionMasivaActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (aprobacionMasivaActoTipo == null || StringUtils.isBlank(aprobacionMasivaActoTipo.getIdAprobacionMasiva())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        AprobacionMasivaActo aprobacionMasivaActo = aprobacionMasivaActoDao.find(Long.valueOf(aprobacionMasivaActoTipo.getIdAprobacionMasiva()));

        if (aprobacionMasivaActo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un AprobacionMasivaActo con el valor:" + aprobacionMasivaActoTipo.getIdAprobacionMasiva()));
            throw new AppException(errorTipoList);
        }
        aprobacionMasivaActo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateAprobacionMasivaActo(aprobacionMasivaActoTipo, aprobacionMasivaActo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        aprobacionMasivaActoDao.edit(aprobacionMasivaActo);
    }

    private void populateAprobacionMasivaActo(final AprobacionMasivaActoTipo aprobacionMasivaActoTipo, final AprobacionMasivaActo aprobacionMasivaActo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (aprobacionMasivaActoTipo.getFecEnvioAprobacion() != null) {
            aprobacionMasivaActo.setFecEnvioAprobacion(aprobacionMasivaActoTipo.getFecEnvioAprobacion());
        }
        if (StringUtils.isNotBlank(aprobacionMasivaActoTipo.getCodEstadoAprobacionMasiva())) {
            ValorDominio estadoAprobacion = valorDominioDao.find(aprobacionMasivaActoTipo.getCodEstadoAprobacionMasiva());
            if (estadoAprobacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstadoAprobacion proporcionado no existe con el ID: " + aprobacionMasivaActoTipo.getCodEstadoAprobacionMasiva()));
            } else {
                aprobacionMasivaActo.setCodEstadoAprobacion(estadoAprobacion);
            }
        }
        if (aprobacionMasivaActoTipo.getIdFuncionarioResponsable() != null
                && StringUtils.isNotBlank(aprobacionMasivaActoTipo.getIdFuncionarioResponsable().getIdFuncionario())) {
            final FuncionarioTipo funcionarioApruebaActo = funcionarioFacade.buscarFuncionario(aprobacionMasivaActoTipo.getIdFuncionarioResponsable(), contextoTransaccionalTipo);
            if (funcionarioApruebaActo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un funcionario existente, con el ID: " + aprobacionMasivaActoTipo.getIdFuncionarioResponsable().getIdFuncionario()));
            } else {
                aprobacionMasivaActo.setValUsuarioResponsable(funcionarioApruebaActo.getIdFuncionario());
            }
        }
    }
}
