package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;

/**
 *
 * @author rpadilla
 */
public interface AuditoriaFacade extends Serializable {

    Long registerInbound(final ContextoTransaccionalTipo contextoTransaccionalTipo, byte[] message) throws AppException;

    void registerOutbound(ContextoTransaccionalTipo audid, byte[] message) throws AppException;

}
