package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AuditoriaDaoImpl;
import co.gov.ugpp.parafiscales.persistence.entity.Auditoria;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ServiciosUtil;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 *
 * @author rpadilla
 */
@Stateless
@TransactionManagement
public class AuditoriaFacadeImpl extends AbstractFacade implements AuditoriaFacade {

    @EJB
    private AuditoriaDaoImpl auditoriaDao;

    @Override
    public Long registerInbound(final ContextoTransaccionalTipo contextoTransaccionalTipo, final byte[] message) throws AppException {
        Auditoria auditoria = ServiciosUtil.preparaContextoTransaccionalEnAuditoria(contextoTransaccionalTipo);
        auditoria.setFechaInicioTx(contextoTransaccionalTipo.getFechaInicioTx() == null ? DateUtil.currentCalendar() : contextoTransaccionalTipo.getFechaInicioTx());
        auditoria.setValContextoTransaccionalSOAP(message);
        Long pk = auditoriaDao.create(auditoria);
        return pk;
    }

    @Override
    public void registerOutbound(final ContextoTransaccionalTipo contextoTransaccionalTipo, final byte[] message) throws AppException {
        Auditoria auditoria = auditoriaDao.findByIdTrx(contextoTransaccionalTipo.getIdTx());
        auditoria.setValContextoRespuestaSOAP(message);
        auditoria.setFechaFinTx(DateUtil.currentCalendar());
        auditoriaDao.edit(auditoria);
    }
}
