package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface ConstanciaEjecutoriaFacade extends Serializable {

    Long crearConstanciaEjecutoria(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo) throws AppException;

    void actualizarConstanciaEjecutoria(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo) throws AppException;

    List<ConstanciaEjecutoriaTipo> buscarPorIdConstanciaEjecutoria(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<String> idConstanciaEjecutoriaList) throws AppException;
}
