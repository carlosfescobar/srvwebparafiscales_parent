package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ConstanciaEjecutoriaDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaDecisionesRecurso;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaEjecutoria;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaFechaRecurso;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ConstanciaEjecutoriaFacade que contiene las
 * operaciones del servicio SrvAplConstanciaEjecutoria
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ConstanciaEjecutoriaFacadeImpl extends AbstractFacade implements ConstanciaEjecutoriaFacade {

    @EJB
    private ConstanciaEjecutoriaDao constanciaEjecutoriaDao;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @Override
    public Long crearConstanciaEjecutoria(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo) throws AppException {

        if (constanciaEjecutoriaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ConstanciaEjecutoria constanciaEjecutoria = new ConstanciaEjecutoria();
        constanciaEjecutoria.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateConstanciaEjecutoria(constanciaEjecutoriaTipo, constanciaEjecutoria, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Long idConstanciaEjecutoria = constanciaEjecutoriaDao.create(constanciaEjecutoria);
        return idConstanciaEjecutoria;
    }

    @Override
    public List<ConstanciaEjecutoriaTipo> buscarPorIdConstanciaEjecutoria(ContextoTransaccionalTipo contextoTransaccionalTipo, List<String> idConstanciaEjecutoriaList) throws AppException {

        if (idConstanciaEjecutoriaList == null
                || idConstanciaEjecutoriaList.isEmpty()) {
            throw new AppException("Debe proporcionar los ID de los objetos a consultar");
        }

        final List<Long> ids = mapper.map(idConstanciaEjecutoriaList, String.class, Long.class);

        final List<ConstanciaEjecutoria> constanciaEjecutoriaList
                = constanciaEjecutoriaDao.findByIdList(ids);

        final List<ConstanciaEjecutoriaTipo> constanciaEjecutoriaTipoList
                = mapper.map(constanciaEjecutoriaList, ConstanciaEjecutoria.class, ConstanciaEjecutoriaTipo.class);

        return constanciaEjecutoriaTipoList;
    }

    @Override
    public void actualizarConstanciaEjecutoria(ContextoTransaccionalTipo contextoTransaccionalTipo, ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo) throws AppException {

        if (constanciaEjecutoriaTipo == null
                || StringUtils.isBlank(constanciaEjecutoriaTipo.getIdConstanciaEjecutoria())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        ConstanciaEjecutoria constanciaEjecutoria = constanciaEjecutoriaDao.find(Long.valueOf(constanciaEjecutoriaTipo.getIdConstanciaEjecutoria()));

        if (constanciaEjecutoria == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No existe un constanciaEjecutoria con el valor: " + constanciaEjecutoriaTipo.getIdConstanciaEjecutoria()));
            throw new AppException(errorTipoList);
        }

        this.populateConstanciaEjecutoria(constanciaEjecutoriaTipo, constanciaEjecutoria, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        constanciaEjecutoria.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        constanciaEjecutoriaDao.edit(constanciaEjecutoria);
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad ConstanciaEjecutoria
     *
     * @param constanciaEjecutoriaTipo Objeto origen
     * @param constanciaEjecutoria Objeto destino
     * @param errorTipoList Listado que almacena errores de negocio
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateConstanciaEjecutoria(final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo,
            final ConstanciaEjecutoria constanciaEjecutoria, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (constanciaEjecutoriaTipo.getFecRecursos() != null
                && !constanciaEjecutoriaTipo.getFecRecursos().isEmpty()) {
            for (ParametroTipo parametroTipo : constanciaEjecutoriaTipo.getFecRecursos()) {
                if (parametroTipo != null && StringUtils.isNotBlank(parametroTipo.getIdLlave())
                        && StringUtils.isNotBlank(parametroTipo.getValValor())) {
                    ValorDominio codTipoFecRecurso = valorDominioDao.find(parametroTipo.getIdLlave());
                    if (codTipoFecRecurso == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoFecRecurso con el ID:" + constanciaEjecutoriaTipo.getIdDocumentoConstanciaEjecutoria()));
                    } else {
                        ConstanciaFechaRecurso constanciaFechaRecurso = new ConstanciaFechaRecurso();
                        constanciaFechaRecurso.setCodTipoFecRecurso(codTipoFecRecurso);
                        constanciaFechaRecurso.setFecRecurso(DateUtil.parseStrDateTimeToCalendar(parametroTipo.getValValor()));
                        constanciaFechaRecurso.setConstanciaEjecutoria(constanciaEjecutoria);
                        constanciaFechaRecurso.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        constanciaEjecutoria.getFechasRecurso().add(constanciaFechaRecurso);
                    }
                }
            }
        }
        if (constanciaEjecutoriaTipo.getValDecisionesRecursos() != null
                && !constanciaEjecutoriaTipo.getValDecisionesRecursos().isEmpty()) {
            for (ParametroTipo parametroTipo : constanciaEjecutoriaTipo.getValDecisionesRecursos()) {
                if (parametroTipo != null && StringUtils.isNotBlank(parametroTipo.getIdLlave())
                        && StringUtils.isNotBlank(parametroTipo.getValValor())) {
                    ValorDominio codDecisionRecurso = valorDominioDao.find(parametroTipo.getIdLlave());
                    if (codDecisionRecurso == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codDecisionRecurso con el ID:" + constanciaEjecutoriaTipo.getIdDocumentoConstanciaEjecutoria()));
                    } else {
                        ConstanciaDecisionesRecurso constanciaDecisionesRecurso = new ConstanciaDecisionesRecurso();
                        constanciaDecisionesRecurso.setCodDecisionRecurso(codDecisionRecurso);
                        constanciaDecisionesRecurso.setEsDecisionRecurso(mapper.map(parametroTipo.getValValor(), Boolean.class));
                        constanciaDecisionesRecurso.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        constanciaDecisionesRecurso.setConstanciaEjecutoria(constanciaEjecutoria);
                        constanciaEjecutoria.getDecisionesRecurso().add(constanciaDecisionesRecurso);
                    }
                }
            }
        }
        if (StringUtils.isNotBlank(constanciaEjecutoriaTipo.getCodFallo())){
            ValorDominio codDecisionCaso = valorDominioDao.find(constanciaEjecutoriaTipo.getCodFallo());
            if (codDecisionCaso == null){
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codDecisionCaso con el ID:" + constanciaEjecutoriaTipo.getCodFallo()));
            }else{
                constanciaEjecutoria.setCodFallo(codDecisionCaso);
            }
        }
        if (constanciaEjecutoriaTipo.getFecConstanciaEjecutoria() != null) {
            constanciaEjecutoria.setFecConstanciaEjecutoria(constanciaEjecutoriaTipo.getFecConstanciaEjecutoria());
        }
        if (constanciaEjecutoriaTipo.getFecCreacionDocumentoConstancia() != null) {
            constanciaEjecutoria.setFecCreacionDocumentoConstancia(constanciaEjecutoriaTipo.getFecCreacionDocumentoConstancia());
        }
        if (StringUtils.isNotBlank(constanciaEjecutoriaTipo.getIdDocumentoConstanciaEjecutoria())) {
            DocumentoEcm documentoConstanciaEjecutoria = documentoDao.find(constanciaEjecutoriaTipo.getIdDocumentoConstanciaEjecutoria());

            if (documentoConstanciaEjecutoria == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró documentoConstanciaEjecutoria con el ID:" + constanciaEjecutoriaTipo.getIdDocumentoConstanciaEjecutoria()));
            } else {
                constanciaEjecutoria.setDocumentoConstanciaEjecutoria(documentoConstanciaEjecutoria);
            }
        }
    }
}
