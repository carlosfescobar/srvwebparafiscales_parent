package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author yrinconh
 */

public interface ControlAprobacionActoFacade extends Serializable {

    Long crearControlAprobacionActo(ControlAprobacionActoTipo controlAprobacionActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<ControlAprobacionActoTipo> buscarPorCriteriosControlAprobacionActo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarControlAprobacionActo(ControlAprobacionActoTipo controlAprobacionActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
