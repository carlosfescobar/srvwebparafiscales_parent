/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionMasivaActoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ControlAprobacionActoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionMasivaActo;
import co.gov.ugpp.parafiscales.persistence.entity.ControlAprobacionActo;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author yrinconh
 */
@Stateless
@TransactionManagement
public class ControlAprobacionActoFacadeImpl extends AbstractFacade implements ControlAprobacionActoFacade {

    @EJB
    private ControlAprobacionActoDao controlAprobacionActoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private AprobacionMasivaActoDao aprobacionMasivaActoDao;

    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;
    @EJB
    private FuncionarioFacade funcionarioFacade;

    @Override
    public Long crearControlAprobacionActo(ControlAprobacionActoTipo controlAprobacionActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlAprobacionActoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        ControlAprobacionActo controlAprobacionActo = new ControlAprobacionActo();

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        this.populateControlAprobacionActo(controlAprobacionActoTipo, controlAprobacionActo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        controlAprobacionActo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        Long idControlAprobacionActo = controlAprobacionActoDao.create(controlAprobacionActo);

        return idControlAprobacionActo;
    }

    @Override
    public PagerData<ControlAprobacionActoTipo> buscarPorCriteriosControlAprobacionActo(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ControlAprobacionActo> pagerDataEntity = controlAprobacionActoDao.findByCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<ControlAprobacionActoTipo> controlAprobacionActoTipoList = mapper.map(pagerDataEntity.getData(),
                ControlAprobacionActo.class, ControlAprobacionActoTipo.class);

        if (controlAprobacionActoTipoList != null) {
            for (final ControlAprobacionActoTipo controlAprobacionActoTipo : controlAprobacionActoTipoList) {
                if (controlAprobacionActoTipo.getIdFuncionarioApruebaActo() != null
                        && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioApruebaActo().getIdFuncionario())) {
                    FuncionarioTipo funcionarioApruebaActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioApruebaActo(), contextoTransaccionalTipo);
                    controlAprobacionActoTipo.setIdFuncionarioApruebaActo(funcionarioApruebaActo);
                }
                if (controlAprobacionActoTipo.getIdFuncionarioElaboraActo() != null
                        && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioElaboraActo().getIdFuncionario())) {
                    FuncionarioTipo funcionarioElaboraActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioElaboraActo(), contextoTransaccionalTipo);
                    controlAprobacionActoTipo.setIdFuncionarioElaboraActo(funcionarioElaboraActo);
                }
            }
        }

        return new PagerData<ControlAprobacionActoTipo>(controlAprobacionActoTipoList, pagerDataEntity.getNumPages());

    }

    @Override
    public void actualizarControlAprobacionActo(ControlAprobacionActoTipo controlAprobacionActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (controlAprobacionActoTipo == null || StringUtils.isBlank(controlAprobacionActoTipo.getIdControlAprobacionActo())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ControlAprobacionActo controlAprobacionActo = controlAprobacionActoDao.find(Long.valueOf(controlAprobacionActoTipo.getIdControlAprobacionActo()));

        if (controlAprobacionActo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un ControlAprobacionActo con el valor:" + controlAprobacionActoTipo.getIdControlAprobacionActo()));
            throw new AppException(errorTipoList);
        }

        controlAprobacionActo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateControlAprobacionActo(controlAprobacionActoTipo, controlAprobacionActo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        controlAprobacionActoDao.edit(controlAprobacionActo);
    }

    private void populateControlAprobacionActo(final ControlAprobacionActoTipo controlAprobacionActoTipo,
            final ControlAprobacionActo controlAprobacionActo, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(controlAprobacionActoTipo.getIdArchivo())) {
            controlAprobacionActo.setIdArchivo(controlAprobacionActoTipo.getIdArchivo());
        }

        if (controlAprobacionActoTipo.getExpediente() != null && StringUtils.isNotBlank(controlAprobacionActoTipo.getExpediente().getIdNumExpediente())) {
            final Expediente expediente = expedienteDao.find(controlAprobacionActoTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un expediente con el id: " + controlAprobacionActoTipo.getExpediente().getIdNumExpediente()));
            } else {
                controlAprobacionActo.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(controlAprobacionActoTipo.getIdAprobacionMasivaActo())) {
            AprobacionMasivaActo aprobacionMasivaActo = aprobacionMasivaActoDao.find(Long.valueOf(controlAprobacionActoTipo.getIdAprobacionMasivaActo()));
            if (aprobacionMasivaActo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe una AprobacionMasiva con el id: " + controlAprobacionActoTipo.getIdAprobacionMasivaActo()));
            } else {
                controlAprobacionActo.setAprobacionMasivaActo(aprobacionMasivaActo);
            }
        }

        if (controlAprobacionActoTipo.getAportante() != null) {
            if (controlAprobacionActoTipo.getAportante().getPersonaNatural() != null) {
                final IdentificacionTipo identificacionTipo = controlAprobacionActoTipo.getAportante().getPersonaNatural().getIdPersona();
                if (identificacionTipo != null
                        && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())
                        && StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())) {

                    final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                    Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                    if (aportante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un aportante con la identificación :" + identificacion));
                    } else {
                        controlAprobacionActo.setAportante(aportante);
                    }
                }
            } else if (controlAprobacionActoTipo.getAportante().getPersonaJuridica() != null) {
                final IdentificacionTipo identificacionTipo = controlAprobacionActoTipo.getAportante().getPersonaJuridica().getIdPersona();
                if (identificacionTipo != null
                        && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())
                        && StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())) {

                    final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                    Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                    if (aportante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un aportante con la identificación :" + identificacion));
                    } else {
                        controlAprobacionActo.setAportante(aportante);
                    }
                }
            }
        }
        if (controlAprobacionActoTipo.getIdFuncionarioApruebaActo() != null
                && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioApruebaActo().getIdFuncionario())) {
            final FuncionarioTipo funcionarioApruebaActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioApruebaActo(), contextoTransaccionalTipo);
            if (funcionarioApruebaActo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un funcionario existente, con el ID: " + controlAprobacionActoTipo.getIdFuncionarioApruebaActo().getIdFuncionario()));
            } else {
                controlAprobacionActo.setIdFuncionarioApruebaActo(funcionarioApruebaActo.getIdFuncionario());
            }
        }
        if (controlAprobacionActoTipo.getIdFuncionarioElaboraActo() != null
                && StringUtils.isNotBlank(controlAprobacionActoTipo.getIdFuncionarioElaboraActo().getIdFuncionario())) {
            final FuncionarioTipo funcionarioElaboraActo = funcionarioFacade.buscarFuncionario(controlAprobacionActoTipo.getIdFuncionarioElaboraActo(), contextoTransaccionalTipo);
            if (funcionarioElaboraActo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró funcionario  existente, con el ID: " + controlAprobacionActoTipo.getIdFuncionarioElaboraActo().getIdFuncionario()));
            } else {
                controlAprobacionActo.setIdFuncionarioElaboraActo(funcionarioElaboraActo.getIdFuncionario());
            }
        }

        if (controlAprobacionActoTipo.getFecAprobacionActo() != null) {
            controlAprobacionActo.setFecAprobacionActo(controlAprobacionActoTipo.getFecAprobacionActo());
        }

        if (controlAprobacionActoTipo.getFecTareaAprobacionEnCola() != null) {
            controlAprobacionActo.setFecTareaAprobacionEnCola(controlAprobacionActoTipo.getFecTareaAprobacionEnCola());
        }

        if (StringUtils.isNotBlank(controlAprobacionActoTipo.getIdActoAdministrativoCreado())) {
            final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(controlAprobacionActoTipo.getIdActoAdministrativoCreado());
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un actoAdministrativo con el id: " + controlAprobacionActoTipo.getIdActoAdministrativoCreado()));
            } else {
                controlAprobacionActo.setIdActoAdministrativoCreado(actoAdministrativo);
            }
        }

        if (StringUtils.isNotBlank(controlAprobacionActoTipo.getCodTipoActoAprobacion())) {
            ValorDominio codTipoActoAprobacion = valorDominioDao.find(controlAprobacionActoTipo.getCodTipoActoAprobacion());
            if (codTipoActoAprobacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoActoAprobacion proporcionado no existe con el ID: " + controlAprobacionActoTipo.getCodTipoActoAprobacion()));
            } else {
                controlAprobacionActo.setCodTipoActoAprobacion(codTipoActoAprobacion);
            }
        }

        if (StringUtils.isNotBlank(controlAprobacionActoTipo.getCodEstadoAprobacion())) {
            ValorDominio codEstadoAprobacion = valorDominioDao.find(controlAprobacionActoTipo.getCodEstadoAprobacion());
            if (codEstadoAprobacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstadoAprobacion proporcionado no existe con el ID: " + controlAprobacionActoTipo.getCodEstadoAprobacion()));
            } else {
                controlAprobacionActo.setCodEstadoAprobacion(codEstadoAprobacion);
            }
        }
        if (controlAprobacionActoTipo.getValRolEncargadoAprobar() != null) {
            controlAprobacionActo.setValRolEncargadoAprobar(controlAprobacionActoTipo.getValRolEncargadoAprobar());
        }
    }

}
