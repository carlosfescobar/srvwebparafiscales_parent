package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.controlliquidadortipo.v1.ControlLiquidadorTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface ControlLiquidadorFacade extends Serializable {

    Long crearControlLiquidador(final ContextoTransaccionalTipo contextoRespuestaTipo, 
            final ControlLiquidadorTipo controlLiquidadorTipo) throws AppException;

    void actualizarControlLiquidador(final ContextoTransaccionalTipo contextoRespuestaTipo, 
            final ControlLiquidadorTipo controlLiquidadorTipo) throws AppException;

    List<ControlLiquidadorTipo> buscarPorIdControlLiquidador(final ContextoTransaccionalTipo contextoTransaccionalTipo, 
            final List<String> idControlLiquidadorList) throws AppException;
    
    PagerData<ControlLiquidadorTipo> buscarPorCriteriosControlLiquidador(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
            

}
