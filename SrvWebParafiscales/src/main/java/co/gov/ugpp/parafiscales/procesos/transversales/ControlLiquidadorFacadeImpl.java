package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.enums.TipoDocumentoLiquidador;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConciliacionContableDao;
import co.gov.ugpp.parafiscales.persistence.dao.ControlLiquidadorArchivoTemporalDao;
import co.gov.ugpp.parafiscales.persistence.dao.ControlLiquidadorDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.HallazgoDao;
import co.gov.ugpp.parafiscales.persistence.dao.HojaCalculoLiquidacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.NominaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidadorArchivoTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacion;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.controlliquidadortipo.v1.ControlLiquidadorTipo;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz ControlLiquidadorFacade que contiene las
 * operaciones del servicio SrvAplControlLiquidador
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ControlLiquidadorFacadeImpl extends AbstractFacade implements ControlLiquidadorFacade {

    @EJB
    private ControlLiquidadorDao controlLiquidadorDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ArchivoDao archivoDao;

    @EJB
    private ConciliacionContableDao conciliacionContableDao;

    @EJB
    private FormatoDao formatoDao;

    @EJB
    private HallazgoDao hallazgoDao;

    @EJB
    private HojaCalculoLiquidacionDao hojaCalculoLiquidacionDao;

    @EJB
    private NominaDao nominaDao;

    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private ExpedienteFacade expedienteFacade;

    @EJB
    private ControlLiquidadorArchivoTemporalDao controlLiquidadorArchivoTemporalDao;

    @Override
    public Long crearControlLiquidador(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ControlLiquidadorTipo controlLiquidadorTipo) throws AppException {

        if (controlLiquidadorTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final ControlLiquidador controlLiquidador = new ControlLiquidador();
        controlLiquidador.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateControlLiquidador(contextoTransaccionalTipo, controlLiquidadorTipo, errorTipoList, controlLiquidador);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idControlLiquidador = controlLiquidadorDao.create(controlLiquidador);

        return idControlLiquidador;
    }

    @Override
    public void actualizarControlLiquidador(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ControlLiquidadorTipo controlLiquidadorTipo) throws AppException {

        if (controlLiquidadorTipo == null
                || StringUtils.isBlank(controlLiquidadorTipo.getIdControlLiquidador())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final ControlLiquidador controlLiquidador = controlLiquidadorDao.find(Long.valueOf(controlLiquidadorTipo.getIdControlLiquidador()));

        if (controlLiquidador == null) {
            throw new AppException("No se encontró controlLiquidaor con el ID: " + controlLiquidadorTipo.getIdControlLiquidador());
        }

        controlLiquidador.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateControlLiquidador(contextoTransaccionalTipo, controlLiquidadorTipo, errorTipoList, controlLiquidador);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        controlLiquidadorDao.edit(controlLiquidador);
    }

    @Override
    public List<ControlLiquidadorTipo> buscarPorIdControlLiquidador(final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<String> idControlLiquidadorList) throws AppException {

        if (idControlLiquidadorList == null
                || idControlLiquidadorList.isEmpty()) {
            throw new AppException("Debe proporcionar los ids de los objetos a buscar");
        }

        List<Long> ids = mapper.map(idControlLiquidadorList, String.class, Long.class);

        List<ControlLiquidador> controlLiquidadorList = controlLiquidadorDao.findByIdList(ids);

        final List<ControlLiquidadorTipo> controlLiquidadorTipoList
                = mapper.map(controlLiquidadorList, ControlLiquidador.class, ControlLiquidadorTipo.class);

        return controlLiquidadorTipoList;
    }

    @Override
    public PagerData<ControlLiquidadorTipo> buscarPorCriteriosControlLiquidador(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, 
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        
        final PagerData<ControlLiquidador> pagerDataEntity = controlLiquidadorDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, 
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        
        final List<ControlLiquidadorTipo> controlLiquidadorTipoList = new ArrayList<ControlLiquidadorTipo>();
              
            for(final ControlLiquidador controlLiquidador : pagerDataEntity.getData()){
                final ControlLiquidadorTipo controlLiquidadorTipo = mapper.map(controlLiquidador, ControlLiquidadorTipo.class);
                controlLiquidadorTipoList.add(controlLiquidadorTipo);                
            }
            return new PagerData<ControlLiquidadorTipo>(controlLiquidadorTipoList, pagerDataEntity.getNumPages());
        
        

        
    }

    /**
     * Método que se encarga de llenar el entity control liquidador
     *
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @param controlLiquidadorTipo objeto origen
     * @param errorTipoList istado que almacena errores de negocio
     * @param controlLiquidador objeto destino
     * @throws AppException
     */
    private void populateControlLiquidador(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ControlLiquidadorTipo controlLiquidadorTipo,
            final List<ErrorTipo> errorTipoList, final ControlLiquidador controlLiquidador) throws AppException {

        //Validacion adicional, si no se envía el expediente, puede que si se quieran cambiar los documentos asociados al expediente
        if ((controlLiquidador.getExpediente() != null && StringUtils.isNotBlank(controlLiquidador.getExpediente().getId()))
                && (controlLiquidadorTipo.getExpediente() == null || StringUtils.isBlank(controlLiquidadorTipo.getExpediente().getIdNumExpediente()))) {

            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(controlLiquidador.getExpediente().getId());
            controlLiquidadorTipo.setExpediente(expedienteTipo);
        }
        if (controlLiquidadorTipo.getExpediente() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getExpediente().getIdNumExpediente())) {

            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos
                    = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);
            //Se obtienen los documentos enviados
            if (controlLiquidadorTipo.getDocumentosFinales() != null
                    && !controlLiquidadorTipo.getDocumentosFinales().isEmpty()) {
                for (ParametroValoresTipo documentoFinal : controlLiquidadorTipo.getDocumentosFinales()) {
                    if (StringUtils.isNoneBlank(documentoFinal.getIdLlave())) {
                        if (TipoDocumentoLiquidador.DOCUMENTO_ANEXO_TH.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_ARCHIVO_ANEXO_TH, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        } else if (TipoDocumentoLiquidador.DOCUMENTO_CONCILIACION_CONTABLE.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_CONCILIACION_CONTABLE, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        } else if (TipoDocumentoLiquidador.DOCUMENTO_HOJA_TRABAJO.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_HOJA_TRABAJO, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        } else if (TipoDocumentoLiquidador.DOCUMENTO_MEMORANDO_CONTABLE.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_ARCHIVO_MEMORANDO_CONTABLE, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        } else if (TipoDocumentoLiquidador.DOCUMENTO_NOMINA.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_NOMINA, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        } else if (TipoDocumentoLiquidador.DOCUMENTO_PILA.getCodTipoDocumento().equals(documentoFinal.getIdLlave())) {
                            documentos.put(OrigenDocumentoEnum.CONTROL_LIQUIDADOR_ID_DOCUMENTO_ARCHIVO_PILA, Util.buildDocumentosFromParametroList(documentoFinal.getValValor()));
                        }
                    }
                }
            }
            Expediente expediente = expedienteFacade.persistirExpedienteDocumento(controlLiquidadorTipo.getExpediente(),
                    documentos, errorTipoList, false, contextoTransaccionalTipo);
            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                controlLiquidador.setExpediente(expediente);
            }
        }
        //Se obtienen los archivos 
        if (controlLiquidadorTipo.getArchivosTemporales() != null
                && !controlLiquidadorTipo.getArchivosTemporales().isEmpty()) {
            for (ParametroValoresTipo archivosTemporales : controlLiquidadorTipo.getArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivosTemporales.getIdLlave())) {

                    final ValorDominio codTipoArchivo = valorDominioDao.find(archivosTemporales.getIdLlave());
                    if (codTipoArchivo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoArchivo con el ID: " + archivosTemporales.getIdLlave()));
                    } else {
                        for (ParametroTipo archivoTemporal : archivosTemporales.getValValor()) {
                            if (StringUtils.isNotBlank(archivoTemporal.getIdLlave())) {
                                Archivo archivoTemporalEncontrado = archivoDao.find(Long.valueOf(archivoTemporal.getIdLlave()));
                                if (archivoTemporalEncontrado == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró Archivo con el ID: " + archivoTemporal.getIdLlave()));
                                } else {
                                    ControlLiquidadorArchivoTemporal controlLiquidadorArchivoTemporal = new ControlLiquidadorArchivoTemporal();
                                    controlLiquidadorArchivoTemporal.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                    controlLiquidadorArchivoTemporal.setCodTipoArchivo(codTipoArchivo);
                                    controlLiquidadorArchivoTemporal.setControlLiquidador(controlLiquidador);
                                    controlLiquidadorArchivoTemporal.setArchivo(archivoTemporalEncontrado);
                                    controlLiquidador.getArchivosTemporales().add(controlLiquidadorArchivoTemporal);
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoArchivo")));
                }
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getIdEnvioConciliacion())) {
            controlLiquidador.setEnvioConciliacion(Long.valueOf(controlLiquidadorTipo.getIdEnvioConciliacion()));
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getIdInformacionExternaNomina())) {
            controlLiquidador.setInformacionExterNomina(Long.valueOf(controlLiquidadorTipo.getIdInformacionExternaNomina()));
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getIdInformacionExternaConciliacion())) {
            controlLiquidador.setInfoExtConciliacion(Long.valueOf(controlLiquidadorTipo.getIdInformacionExternaConciliacion()));
        }
        if (StringUtils.isNoneBlank(controlLiquidadorTipo.getIdEnvioNomina())) {
            controlLiquidador.setEnvioNomina(Long.valueOf(controlLiquidadorTipo.getIdEnvioNomina()));
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getValPrograma())) {
            controlLiquidador.setValPrograma(controlLiquidadorTipo.getValPrograma());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getDescCargueNominaConciliacion())) {
            controlLiquidador.setDesCargueNominaConciliacion(controlLiquidadorTipo.getDescCargueNominaConciliacion());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getEsSolicitarAclaraciones())) {
            controlLiquidador.setEsSolicitarAclaraciones(Boolean.valueOf(controlLiquidadorTipo.getEsSolicitarAclaraciones()));
        }
        if (StringUtils.isNoneBlank(controlLiquidadorTipo.getIdPilaDepurada())) {
            controlLiquidador.setIdPilaDepurada(controlLiquidadorTipo.getIdPilaDepurada());
        }
        if (controlLiquidadorTipo.getFecRadicadoEntrada() != null) {
            controlLiquidador.setFecRadicadoEntrada(controlLiquidadorTipo.getFecRadicadoEntrada());
        }
        if (controlLiquidadorTipo.getFecRadicadoSalida() != null) {
            controlLiquidador.setFecRadicadoSalida(controlLiquidadorTipo.getFecRadicadoSalida());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getIdRadicadoEntrada())) {
            controlLiquidador.setIdRadicadoEntrada(controlLiquidadorTipo.getIdRadicadoEntrada());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getIdRadicadoSalida())) {
            controlLiquidador.setIdRadicadoSalida(controlLiquidadorTipo.getIdRadicadoSalida());
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodCausalCierre())) {
            ValorDominio codCausalCierre = valorDominioDao.find(controlLiquidadorTipo.getCodCausalCierre());
            if (codCausalCierre == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codCausalCierre con el ID: " + controlLiquidadorTipo.getCodCausalCierre()));
            } else {
                controlLiquidador.setCodCausalCierre(codCausalCierre);
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodLineaAccion())) {
            ValorDominio codLineaAccion = valorDominioDao.find(controlLiquidadorTipo.getCodLineaAccion());
            if (codLineaAccion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codLineaAccion con el ID: " + controlLiquidadorTipo.getCodLineaAccion()));
            } else {
                controlLiquidador.setCodLineaAccion(codLineaAccion);
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodOrigen())) {
            ValorDominio codOrigen = valorDominioDao.find(controlLiquidadorTipo.getCodOrigen());
            if (codOrigen == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codOrigen con el ID: " + controlLiquidadorTipo.getCodOrigen()));
            } else {
                controlLiquidador.setCodOrigen(codOrigen);
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodProcesoSolicitador())) {
            ValorDominio codProcesoSolicitador = valorDominioDao.find(controlLiquidadorTipo.getCodProcesoSolicitador());
            if (codProcesoSolicitador == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codProcesoSolicitador con el ID: " + controlLiquidadorTipo.getCodOrigen()));
            } else {
                controlLiquidador.setCodProcesoSolicitador(codProcesoSolicitador);
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodTipoDenuncia())) {
            ValorDominio codTipoDenuncia = valorDominioDao.find(controlLiquidadorTipo.getCodTipoDenuncia());
            if (codTipoDenuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoDenuncia con el ID: " + controlLiquidadorTipo.getCodTipoDenuncia()));
            } else {
                controlLiquidador.setCodTipoDenuncia(codTipoDenuncia);
            }
        }

        if (controlLiquidadorTipo.getFormatoConciliacion() != null
                && (controlLiquidadorTipo.getFormatoConciliacion().getIdFormato() != null
                && controlLiquidadorTipo.getFormatoConciliacion().getValVersion() != null)) {
            Formato formatoConciliacion
                    = formatoDao.findFormatoByFormatoAndVersion(controlLiquidadorTipo.getFormatoConciliacion().getIdFormato(), controlLiquidadorTipo.getFormatoConciliacion().getValVersion().longValue());

            if (formatoConciliacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró formatoConciliacion con el idFormato: " + controlLiquidadorTipo.getFormatoConciliacion().getIdFormato().toString()
                        + ", y el idVersion: " + controlLiquidadorTipo.getFormatoConciliacion().getValVersion().toString()));
            } else {
                controlLiquidador.setFormatoConciliacion(formatoConciliacion);
            }
        }

        if (controlLiquidadorTipo.getFormatoNomina() != null
                && (controlLiquidadorTipo.getFormatoNomina().getIdFormato() != null
                && controlLiquidadorTipo.getFormatoNomina().getValVersion() != null)) {
            Formato formatoNomina
                    = formatoDao.findFormatoByFormatoAndVersion(controlLiquidadorTipo.getFormatoNomina().getIdFormato(), controlLiquidadorTipo.getFormatoNomina().getValVersion().longValue());

            if (formatoNomina == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró formatoConciliacion con el idFormato: " + controlLiquidadorTipo.getFormatoNomina().getIdFormato().toString()
                        + ", y el idVersion: " + controlLiquidadorTipo.getFormatoNomina().getValVersion().toString()));
            } else {
                controlLiquidador.setFormatoNomina(formatoNomina);
            }
        }

        if (controlLiquidadorTipo.getHojaCalculo() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getHojaCalculo().getIdHojaCalculoLiquidacion())) {

            HojaCalculoLiquidacion hojaCalculo = hojaCalculoLiquidacionDao.find(new BigDecimal(controlLiquidadorTipo.getHojaCalculo().getIdHojaCalculoLiquidacion()));
            if (hojaCalculo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un hojaCalculo con el id: " + controlLiquidadorTipo.getHojaCalculo().getIdHojaCalculoLiquidacion()));
            } else {
                controlLiquidador.setHojaCalculoLiquidacion(hojaCalculo);
            }
        }

        if (controlLiquidadorTipo.getNomina() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getNomina().getIdNomina())) {
            Nomina nomina = nominaDao.find(Long.valueOf(controlLiquidadorTipo.getNomina().getIdNomina()));
            if (nomina == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró una nomina con el id: " + controlLiquidadorTipo.getNomina().getIdNomina()));
            } else {
                controlLiquidador.setNomina(nomina);
            }
        }
        if (controlLiquidadorTipo.getHallazgosNominaPila() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getHallazgosNominaPila().getIdHallazgo())) {
            Hallazgo hallazgoNominaPila = hallazgoDao.find(Long.valueOf(controlLiquidadorTipo.getHallazgosNominaPila().getIdHallazgo()));
            if (hallazgoNominaPila == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró hallazgoNominaPila con el ID: " + controlLiquidadorTipo.getHallazgosNominaPila().getIdHallazgo()));
            } else {
                controlLiquidador.setHallazgoNominaPila(hallazgoNominaPila);
            }
        }

        if (controlLiquidadorTipo.getHallazgosConciliacionNomina() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getHallazgosConciliacionNomina().getIdHallazgo())) {
            Hallazgo hallazgoConciliacionContable = hallazgoDao.find(Long.valueOf(controlLiquidadorTipo.getHallazgosConciliacionNomina().getIdHallazgo()));
            if (hallazgoConciliacionContable == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró hallazgoConciliacionNomina con el ID: " + controlLiquidadorTipo.getHallazgosConciliacionNomina().getIdHallazgo()));
            } else {
                controlLiquidador.setHallazgoConcilNomina(hallazgoConciliacionContable);
            }
        }

        if (controlLiquidadorTipo.getConciliacionContable() != null
                && StringUtils.isNotBlank(controlLiquidadorTipo.getConciliacionContable().getIdConciliacionContable())) {
            ConciliacionContable conciliacionContable = conciliacionContableDao.find(Long.valueOf(controlLiquidadorTipo.getConciliacionContable().getIdConciliacionContable()));
            if (conciliacionContable == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró una conciliacionContable con el ID: "
                        + controlLiquidadorTipo.getConciliacionContable().getIdConciliacionContable()));

            } else {
                controlLiquidador.setConciliacionContable(conciliacionContable);
            }
        }

        if (controlLiquidadorTipo.getAportante() != null) {
            if (controlLiquidadorTipo.getAportante().getPersonaNatural() != null) {
                final IdentificacionTipo identificacionTipo = controlLiquidadorTipo.getAportante().getPersonaNatural().getIdPersona();
                if (identificacionTipo != null
                        && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())
                        && StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())) {

                    final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                    Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                    if (aportante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un aportante con la identificación :" + identificacion));
                    } else {
                        controlLiquidador.setAportante(aportante);
                    }
                }
            } else if (controlLiquidadorTipo.getAportante().getPersonaJuridica() != null) {
                final IdentificacionTipo identificacionTipo = controlLiquidadorTipo.getAportante().getPersonaJuridica().getIdPersona();
                if (identificacionTipo != null
                        && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())
                        && StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())) {

                    final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                    Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

                    if (aportante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró un aportante con la identificación :" + identificacion));
                    } else {
                        controlLiquidador.setAportante(aportante);
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getCodTipoEjecucionLiquidador())) {
            ValorDominio valorDominio = valorDominioDao.find(controlLiquidadorTipo.getCodTipoEjecucionLiquidador());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró tipoEjecucionLiquidador con el ID: " + controlLiquidadorTipo.getCodTipoEjecucionLiquidador()));
            } else {
                controlLiquidador.setCodTipoEjecucionLiquidador(valorDominio);
            }
        }

        if (StringUtils.isNotBlank(controlLiquidadorTipo.getValLiquidacionAporte())) {
            controlLiquidador.setValLiquidacionAporte(controlLiquidadorTipo.getValLiquidacionAporte());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getValLiquidacionSancionOmision())) {
            controlLiquidador.setValLiquidacionSancionOmision(controlLiquidadorTipo.getValLiquidacionSancionOmision());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getValLiquidacionSancionInexactitud())) {
            controlLiquidador.setValLiquidacionSancionInexactitud(controlLiquidadorTipo.getValLiquidacionSancionInexactitud());
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getEsIncumplimiento())) {
            controlLiquidador.setEsIncumplimiento(Boolean.valueOf(controlLiquidadorTipo.getEsIncumplimiento()));
        }
        if (StringUtils.isNotBlank(controlLiquidadorTipo.getValPartidasGlobales())) {
            controlLiquidador.setValPartidasGlobales(controlLiquidadorTipo.getValPartidasGlobales());
        }
    }

}
