package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.consumer.ProcLiquidadorFacade;
import co.gov.ugpp.parafiscales.enums.ProcesoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.HallazgoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgodetalleconciliacioncontabletipo.v1.HallazgoDetalleConciliacionContableTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz CruceConciliacionContableNomina que contiene
 * las operaciones del servicio SrvAplCruceConciliacionContableNomina
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class CruceConciliacionContableNominaFacadeImpl extends AbstractFacade implements CruceConciliacionContableNominaFacade {

    private static final Logger LOG = LoggerFactory.getLogger(CruceConciliacionContableNominaFacadeImpl.class);

    @EJB
    private HallazgoDao hallazgoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ProcLiquidadorFacade procLiquidadorFacade;

    @EJB
    private HallazgoFacade hallazgoFacade;

    /**
     * Método que realiza la suma de conceptos contables y cruza la conciliación
     * contable
     *
     * @param identificacionTipo Identificacion correspondiente al Aportante
     * @param expedienteTipo Expediente que relaciona la conciliacionContable.
     * @param parametroTipoList Listado de parametros enviados para realizar la
     * consulta
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    @Asynchronous
    public void cruzarConciliacionContable(final IdentificacionTipo identificacionTipo, final ExpedienteTipo expedienteTipo,
            final List<ParametroTipo> parametroTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkNomina(identificacionTipo, expedienteTipo, parametroTipoList, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        try {
            final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo = hallazgoFacade.obtenerDatosBasicosHallazgoConciliacionContable(expedienteTipo);

            final List<Object[]> sumaNomina
                    = hallazgoDao.obtenerSumaNomina(identificacionTipo, expedienteTipo,
                            parametroTipoList, Constants.SUMAR_NOMINA);

            if (sumaNomina == null
                    || sumaNomina.isEmpty()) {
                throw new AppException("No se han encontrado datos para realizar el cálculo de la nómina");
            }
            //Se obtiene la suma de la nómina y se actualiza el detalle de la conciliación contable
            hallazgoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.ACTUALIZAR_CONCILIACION_CONTABLE), sumaNomina);
            
            final List<Object[]> cruceConciliacionContable
                    = hallazgoDao.cruzarConciliacionContable(expedienteTipo,
                            parametroTipoList, Constants.CRUZAR_CONCILIACION_CONTABLE);

            if (cruceConciliacionContable == null
                    || cruceConciliacionContable.isEmpty()) {
                throw new AppException("No se han encontrado datos para comparar la nómina y la contabilidad");
            }

            final Hallazgo hallazgo = new Hallazgo();
            hallazgo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

            final List<Object[]> hallazgosDetalle = new ArrayList<Object[]>();

            final List<HallazgoDetalleConciliacionContableTipo> hallazgoDetalleConciliacionContableTipoList = new ArrayList<HallazgoDetalleConciliacionContableTipo>();            
            Long idHallazgo = hallazgoFacade.crearHallazgoCruceConciliacionContable(contextoTransaccionalTipo, hallazgoConciliacionContableTipo);
            hallazgoConciliacionContableTipo.setIdHallazgo(idHallazgo.toString());
            final String msgDescripcion = PropsReader.getKeyParam(Constants.DESC_HALLAZGO_CONCILIACION_CONTABLE);

            this.validarHallazgo(cruceConciliacionContable, hallazgosDetalle, idHallazgo, msgDescripcion,
                    hallazgoDetalleConciliacionContableTipoList, contextoTransaccionalTipo);

            hallazgoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.INSERT_HALLAZGO_DETALLE_CONCILIACION_CONTABLE), hallazgosDetalle);
            
            hallazgoConciliacionContableTipo.getHallazgosDetalle().addAll(hallazgoDetalleConciliacionContableTipoList);

            final ValorDominio codProceso = valorDominioDao.find(ProcesoEnum.LIQUIDADOR.getCode());
            if (codProceso == null) {
                throw new AppException("No se encontrò codProceso con el ID: " + ProcesoEnum.LIQUIDADOR.getCode());
            }

            /**
             * Como el método es asíncrono, se consume el servicio de
             * integración del BPM y se pasan los hallazgos encontrados
             */
            LOG.info("Envío de hallazgos del cruce de conciliación contable ::: INICIO");
            procLiquidadorFacade.enviarHallazgosCruceConciliacionContable(hallazgoConciliacionContableTipo, contextoTransaccionalTipo, codProceso);
            LOG.info("Envío de hallazgos del cruce de conciliación contable ::: FIN");
        } catch (RuntimeException rte) {
            LOG.error(rte.getMessage());
            throw new AppException(rte.getMessage());
        } catch (SQLException sqle) {
            throw new AppException("Error enviando los hallazgos del cruce de conciliación contable"+sqle.getMessage());
        } catch (TypesMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo ex) {
            throw new AppException("Error enviando los hallazgos del cruce de conciliación contable");
        }
    }

    /**
     * Método que se encarga de determinar si hay hallazgo al comparar nómina y
     * contabilidad
     *
     * @param cruceConciliacionContable la información obtenidad de la
     * conciliación contable, la sumatoria de la nómina y la contabilidad
     * agrupada por conceptos contables
     * @param hallazgosDetalle el listado de objetos que se se van a guardar
     * @param idHallazgo el hallazgo padre
     * @param tipoHallazgoEnum el tipo de hallazgo con el que se va a guardar el
     * detalle
     * @param descHallazgo el mensaje que acompaña la descripción del hallazgo
     * @param hallazgoDetalleTipoList el listado de hallazgoDetalleTipo para
     * agregar los hallazgos
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException
     */
    private void validarHallazgo(final List<Object[]> cruceConciliacionContable, final List<Object[]> hallazgosDetalle,
            final Long idHallazgo, final String descHallazgo, final List<HallazgoDetalleConciliacionContableTipo> hallazgoDetalleTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        for (Object[] conciliacionContable : cruceConciliacionContable) {

            final String conceptoContable = conciliacionContable[Constants.CONCEPTO_CONTABLE].toString();
            final Long valorContabilidad = Long.valueOf(conciliacionContable[Constants.VALOR_CONTABILIDAD].toString());
            final Long valorNomina = Long.valueOf(conciliacionContable[Constants.VALOR_NOMINA].toString());
            final Long diferenciaPesosContabilidadNomina = (valorContabilidad - valorNomina);
            final Long diferenciaPorcentajeContabilidadNomina = (valorNomina == 0 ? 100 : ((diferenciaPesosContabilidadNomina * 100) / valorNomina));

            HallazgoDetalleConciliacionContableTipo hallazgoConciliacionContableDetalleTipo = new HallazgoDetalleConciliacionContableTipo();
            Object[] hallazgoDetalle = new Object[Constants.CAMPOS_PERSISTIR_HALL_DET_CONCI_CONTABLE];

            hallazgoDetalle[Constants.ID_HALLAZGO] = idHallazgo;
            hallazgoDetalle[Constants.DESC_HALLAZGO_CON_CONTABLE] = descHallazgo;
            hallazgoDetalle[Constants.CONCEP_CONTABLE] = conceptoContable;
            hallazgoDetalle[Constants.VAL_CONTABILIDAD] = valorContabilidad;
            hallazgoDetalle[Constants.VAL_NOMINA] = valorNomina;
            hallazgoDetalle[Constants.VAL_DIFERENCIA_PROCENTAJE] = diferenciaPorcentajeContabilidadNomina;
            hallazgoDetalle[Constants.VAL_DIFERENCIA_PESOS] = diferenciaPesosContabilidadNomina;
            hallazgoDetalle[Constants.USUARIO_CREACION_CON_CONTABLE] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.USUARIO_MODIFICACION_CON_CONTABLE] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.FECHA_CREACION_CON_CONTABLE] = new Date();
            hallazgoDetalle[Constants.FECHA_MODIFICACION_CON_CONTABLE] = new Date();

            hallazgoConciliacionContableDetalleTipo.setConceptoContable(conceptoContable);
            hallazgoConciliacionContableDetalleTipo.setValNomina(valorNomina.toString());
            hallazgoConciliacionContableDetalleTipo.setValContabilidad(valorContabilidad.toString());
            hallazgoConciliacionContableDetalleTipo.setValDiferenciaPesos(diferenciaPesosContabilidadNomina.toString());
            hallazgoConciliacionContableDetalleTipo.setValDiferenciaPorcentaje(diferenciaPorcentajeContabilidadNomina.toString());
            hallazgoConciliacionContableDetalleTipo.setDescHallazgo(descHallazgo);
            hallazgoConciliacionContableDetalleTipo.setValUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            hallazgoConciliacionContableDetalleTipo.setValFechaCreacion(new Date().toString());

            hallazgosDetalle.add(hallazgoDetalle);
            hallazgoDetalleTipoList.add(hallazgoConciliacionContableDetalleTipo);
        }
    }

    @Override
    public void actualizarHallazgosConciliacionContable(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) throws AppException {

        if (hallazgoConciliacionContableTipo.getHallazgosDetalle() == null
                || hallazgoConciliacionContableTipo.getHallazgosDetalle().isEmpty()) {
            throw new AppException("Debe enviar un listado de hallazgo detalle a actualizar");
        }

        hallazgoFacade.actualizarHallazgoConciliacionContable(contextoTransaccionalTipo, hallazgoConciliacionContableTipo);
    }
}
