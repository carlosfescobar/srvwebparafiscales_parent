package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface CruceNominaPilaFacade extends Serializable {

    void cruzarPilaConNomina(final IdentificacionTipo identificacionTipo, final ExpedienteTipo expedienteTipo,
            final List<ParametroTipo> parametroTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarHallazgosNomina(final ContextoTransaccionalTipo contextoTransaccionalTipo, 
            final HallazgoNominaTipo hallazgoNominaTipo) throws AppException;
}
