package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.consumer.ProcLiquidadorFacade;
import co.gov.ugpp.parafiscales.enums.ProcesoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.HallazgoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1.HallazgoDetalleNominaTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.TypesMsjOpRecibirHallazgosCruceNominaPILAFallo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz CruceNominaPilaFacade que contiene las
 * operaciones del servicio SrvAplNomina
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
@PermitAll
public class CruceNominaPilaFacadeImpl extends AbstractFacade implements CruceNominaPilaFacade {

    private static final Logger LOG = LoggerFactory.getLogger(CruceNominaPilaFacadeImpl.class);

    @EJB
    private HallazgoDao hallazgoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ProcLiquidadorFacade procLiquidadorFacade;

    @EJB
    private HallazgoFacade hallazgoFacade;

    /**
     * Mètodo que se encarga de realizar el cruce de pila con nòmina y nòmina
     * con vacaciones
     *
     * @param identificacionTipo la identificaciòn del aportante
     * @param expedienteTipo el expediente que relaciona la pila y la nòmina
     * @param parametroTipoList listado de parámetros con los cuales se realiza
     * la consulta de los cotizantes
     * @param contextoTransaccionalTipo
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    @Asynchronous
    public void cruzarPilaConNomina(final IdentificacionTipo identificacionTipo,
            final ExpedienteTipo expedienteTipo, final List<ParametroTipo> parametroTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkNomina(identificacionTipo, expedienteTipo, parametroTipoList, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        /**
         * Se busca la información relacionada al hallazgo
         */
        try {
            final HallazgoNominaTipo hallazgoNominaTipo = hallazgoFacade.obtenerDatosBasicosHallazgoNomina(expedienteTipo);

            /**
             * Se busca la información de los cotizantes a los que se les
             * generará hallazgos
             */
            final List<Object[]> cotizanteSinNomina
                    = hallazgoDao.buscarCotizantesConHallazgosPilaNomina(identificacionTipo, expedienteTipo,
                            parametroTipoList, Constants.CRUZAR_PILA_NOMINA);

            final List<Object[]> cotizantesSinVacaciones
                    = hallazgoDao.buscarCotizantesConHallazgosVacaciones(identificacionTipo, expedienteTipo,
                            parametroTipoList, Constants.CRUZAR_NOMINA_VACACIONES);

            if ((cotizanteSinNomina == null
                    || cotizanteSinNomina.isEmpty())
                    && (cotizantesSinVacaciones == null
                    || cotizantesSinVacaciones.isEmpty())) {
                throw new AppException("No se encontraron cotizantes para generar hallazgos con los filtros de búsqueda ingresados");
            }

            final Hallazgo hallazgo = new Hallazgo();
            hallazgo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

            final List<Object[]> hallazgosDetalle = new ArrayList<Object[]>();

            final List<HallazgoDetalleNominaTipo> hallazgoDetalleTipoList = new ArrayList<HallazgoDetalleNominaTipo>();

            Long idHallazgo = hallazgoFacade.crearHallazgoCruceNominaPila(contextoTransaccionalTipo, hallazgoNominaTipo);
            hallazgoNominaTipo.setIdHallazgo(idHallazgo.toString());

            this.buildHallazgoDetalle(cotizanteSinNomina, idHallazgo, hallazgosDetalle, hallazgoDetalleTipoList,
                    Constants.DESC_HALLAZGO_PILA_NOMINA, contextoTransaccionalTipo);

            this.buildHallazgoDetalle(cotizantesSinVacaciones, idHallazgo, hallazgosDetalle, hallazgoDetalleTipoList,
                    Constants.DESC_HALLAZGO_NOMINA_VACACIONES, contextoTransaccionalTipo);

            hallazgoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.INSERT_HALLAZGO_DETALLE_NOMINA_PILA), hallazgosDetalle);

            hallazgoNominaTipo.getHallazgosDetalle().addAll(hallazgoDetalleTipoList);

            final ValorDominio codProceso = valorDominioDao.find(ProcesoEnum.LIQUIDADOR.getCode());
            if (codProceso == null) {
                throw new AppException("No se encontrò codProceso con el ID: " + ProcesoEnum.LIQUIDADOR.getCode());
            }
            /**
             * Como el método es asíncrono, se consume el servicio de
             * integración del BPM y se pasan los hallazgos encontrados
             */
            LOG.info("Envío de hallazgos del cruce de nómina y pila ::: INICIO");
            procLiquidadorFacade.enviarHallazgosCruceNominaPila(hallazgoNominaTipo, contextoTransaccionalTipo, codProceso);
            LOG.info("Envío de hallazgos del cruce de nómina y pila ::: FIN");

        } catch (TypesMsjOpRecibirHallazgosCruceNominaPILAFallo ex) {
            throw new AppException("Error enviando los hallazgos del cruce de nomina con pila");
        } catch (SQLException sqle) {
            throw new AppException("Error enviando los hallazgos del cruce de nómina" + sqle.getMessage());
        }
    }

    /**
     * Mètodo que se encarga de armar la estructura de hallazgo detalle y
     * realizar la inserciòn en batch
     *
     * @param cotizantesConHallazgo el listado de cotizante que fueron
     * encontrados con hallazgos durante el cruce
     * @param idHallazgo el objeto padre de los hallazgo detalle
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @param hallazgosDetalle el listado de hallazgos detalle a insertar
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void buildHallazgoDetalle(final List<Object[]> cotizantesConHallazgo,
            final Long idHallazgo, final List<Object[]> hallazgosDetalle, final List<HallazgoDetalleNominaTipo> hallazgoDetalleTipoList,
            final String descHallazgo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        for (Object[] cotizanteHallazgo : cotizantesConHallazgo) {

            HallazgoDetalleNominaTipo hallazgoDetalleNominaTipo = new HallazgoDetalleNominaTipo();
            Object[] hallazgoDetalle = new Object[Constants.CAMPOS_PERSISTIR_HALLAZGO_DETALLE];

            IdentificacionTipo identificacionTipo = new IdentificacionTipo();
            identificacionTipo.setValNumeroIdentificacion(cotizanteHallazgo[Constants.NUMERO_IDENTIFICACION_CRUCE].toString());
            identificacionTipo.setCodTipoIdentificacion(cotizanteHallazgo[Constants.TIPO_DOCUMENTO_CRUCE].toString());

            final String msgDescripcion = PropsReader.getKeyParam(descHallazgo);

            hallazgoDetalle[Constants.HALLAZGO] = idHallazgo;
            hallazgoDetalle[Constants.DESC_HALLAZGO] = msgDescripcion;
            hallazgoDetalle[Constants.TIPO_IDENTIFICACION] = cotizanteHallazgo[Constants.TIPO_DOCUMENTO_CRUCE];
            hallazgoDetalle[Constants.NUMERO_IDENTIFICACION] = cotizanteHallazgo[Constants.NUMERO_IDENTIFICACION_CRUCE];
            hallazgoDetalle[Constants.NOMBRE_COTIZANTE] = cotizanteHallazgo[Constants.NOMBRE_CRUCE];
            hallazgoDetalle[Constants.PERIODO_RESTO] = cotizanteHallazgo[Constants.PERIODO_RESTO_CRUCE];
            hallazgoDetalle[Constants.TIPO_COTIZANTE] = cotizanteHallazgo[Constants.TIPO_COTIZANTE_CRUCE];
            hallazgoDetalle[Constants.USUARIO_CREACION] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.USUARIO_MODIFICACION] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.FECHA_CREACION] = new Date();
            hallazgoDetalle[Constants.FECHA_MODIFICACION] = new Date();

            hallazgoDetalleNominaTipo.setCodTipoCotizante(cotizanteHallazgo[Constants.TIPO_COTIZANTE_CRUCE].toString());
            hallazgoDetalleNominaTipo.setDescHallazgo(descHallazgo);
            hallazgoDetalleNominaTipo.setValNombreCotizante(cotizanteHallazgo[Constants.NOMBRE_CRUCE].toString());
            hallazgoDetalleNominaTipo.setIdCotizante(identificacionTipo);
            hallazgoDetalleNominaTipo.setValPeriodoResto(cotizanteHallazgo[Constants.PERIODO_RESTO_CRUCE].toString());
            hallazgoDetalleNominaTipo.setValUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            hallazgoDetalleNominaTipo.setValFechaCreacion(new Date().toString());
            hallazgoDetalleNominaTipo.setValUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
            hallazgoDetalleNominaTipo.setValUsuarioModificacion(new Date().toString());

            hallazgosDetalle.add(hallazgoDetalle);
            hallazgoDetalleTipoList.add(hallazgoDetalleNominaTipo);
        }
    }

    @Override
    public void actualizarHallazgosNomina(ContextoTransaccionalTipo contextoTransaccionalTipo, HallazgoNominaTipo hallazgoNominaTipo) throws AppException {

        if (hallazgoNominaTipo.getHallazgosDetalle() == null
                || hallazgoNominaTipo.getHallazgosDetalle().isEmpty()) {
            throw new AppException("Debe enviar un listado de hallazgo detalle a actualizar");
        }

        hallazgoFacade.actualizarHallazgoNominaPila(contextoTransaccionalTipo, hallazgoNominaTipo);
    }
}
