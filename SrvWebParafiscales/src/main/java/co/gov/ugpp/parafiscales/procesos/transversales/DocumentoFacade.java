package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rpadilla
 */
public interface DocumentoFacade extends Serializable {

    void crearDocumento(DocumentoTipo documentoTipo, ExpedienteTipo expedienteTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<Long> asociarExpedientes(DocumentoTipo documentoTipo, List<ExpedienteTipo> expedienteTipoList,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    List<DocumentoTipo> buscarPorIdDocumento(final List<String> idDocumentList) throws AppException;
    
    void actualizarDocumento(DocumentoTipo documentoTipo,ExpedienteTipo expedienteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
