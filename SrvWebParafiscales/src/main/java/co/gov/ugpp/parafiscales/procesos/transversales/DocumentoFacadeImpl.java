package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocAgrupador;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz DocumentoFacade que contiene las operaciones
 * del servicio SrvAplDocumentoCORE
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class DocumentoFacadeImpl extends AbstractFacade implements DocumentoFacade {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentoFacadeImpl.class);

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private Populator populator;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private ExpedienteDocumentoDao expedienteDocumentoDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * implementación de la operación crearDocumento se encarga de crear un
     * documento a partir del objeto documentoTipo enviado
     *
     * @param documentoTipo Objeto a partir del cual se crea un documento en la
     * base de datos
     * @param expedienteTipo Objeto que se asocia a documento a crear en la base
     * de datos
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void crearDocumento(final DocumentoTipo documentoTipo,
            final ExpedienteTipo expedienteTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (documentoTipo == null
                || StringUtils.isBlank(documentoTipo.getIdDocumento())
                || expedienteTipo == null
                || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            throw new AppException("El Documento o el Expediente enviado no puede ser null");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        DocumentoEcm documento = documentoDao.find(documentoTipo.getIdDocumento());
        if (documento != null) {
            throw new AppException("Ya existe un Documento con el ID: " + documentoTipo.getIdDocumento(),
                    ErrorEnum.ERROR_ENTIDAD_EXISTENTE);
        }

        DocumentoEcm documentoEcm = new DocumentoEcm();
        documentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        Validator.checkDocumento(documentoTipo, errorTipoList);
        this.populateDocumentoEcm(documentoTipo, documentoEcm, expedienteTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        documentoDao.create(documentoEcm);

    }

    @Override
    public void actualizarDocumento(DocumentoTipo documentoTipo, ExpedienteTipo expedienteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (documentoTipo == null || StringUtils.isBlank(documentoTipo.getIdDocumento())) {
            throw new AppException("Debe proporcionar el ID del objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        DocumentoEcm documentoEcm = documentoDao.find(documentoTipo.getIdDocumento());

        if (documentoEcm == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un documento con el id:" + documentoTipo.getIdDocumento()));
            throw new AppException(errorTipoList);
        }

        documentoEcm.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateDocumentoEcm(documentoTipo, documentoEcm, expedienteTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        documentoDao.edit(documentoEcm);
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad documentoEcm
     *
     * @param documentoTipo Objeto origen
     * @param documentoEcm Objeto destino
     * @param expedienteTipo Objeto que se asocia a la entidad
     * expedienteDocumentoEcm
     * @param expedienteDocumentoEcm entidad que contiene el objeto
     * expedienteTipo y el documentoEcm que se va asociar.
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populateDocumentoEcm(final DocumentoTipo documentoTipo, DocumentoEcm documentoEcm,
            final ExpedienteTipo expedienteTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        
        //System.out.println("Op: ::ANDRES22:: populateDocumentoEcm documentoEcm.getId(): " + documentoEcm.getId());
        
        
        if (StringUtils.isNotBlank(documentoTipo.getIdDocumento())) {
            documentoEcm.setId(documentoTipo.getIdDocumento());

        }
        if (StringUtils.isNotBlank(documentoTipo.getIdRadicadoCorrespondencia())) {
            documentoEcm.setNumeroRadicado(documentoTipo.getIdRadicadoCorrespondencia());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValNombreDocumento())) {
            documentoEcm.setValNombre(documentoTipo.getValNombreDocumento());
        }

        if (documentoTipo.getValPaginas() != null) {
            documentoEcm.setValPaginas(documentoTipo.getValPaginas());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValAutorOriginador())) {
            documentoEcm.setValAutoOriginador(documentoTipo.getValAutorOriginador());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValNaturalezaDocumento())) {
            documentoEcm.setValNaturalezaDocumento(documentoTipo.getValNaturalezaDocumento());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValOrigenDocumento())) {
            documentoEcm.setValOrigenDocumento(documentoTipo.getValOrigenDocumento());
        }

        if (StringUtils.isNotBlank(documentoTipo.getDescObservacionLegibilidad())) {
            documentoEcm.setDescObsLegibilidad(documentoTipo.getDescObservacionLegibilidad());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValNombreTipoDocumental())) {
            documentoEcm.setValNombreTipoDocumental(documentoTipo.getValNombreTipoDocumental());
        }

        if (documentoTipo.getNumFolios() != null) {
            documentoEcm.setNumFolios(documentoTipo.getNumFolios());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValLegible())) {
            documentoEcm.setValLegible(documentoTipo.getValLegible());
        }

        if (StringUtils.isNotBlank(documentoTipo.getValTipoFirma())) {
            documentoEcm.setValTipoFirma(documentoTipo.getValTipoFirma());
        }

        if (StringUtils.isNotBlank(documentoTipo.getEsMover())) {
            documentoEcm.setEsMover(Boolean.valueOf(documentoTipo.getEsMover()));
        }

        if (StringUtils.isNotBlank(documentoTipo.getFecDocumento())) {
            documentoEcm.setFecDocumento((DateUtil.parseStrDateTimeToCalendar(documentoTipo.getFecDocumento())));
        }

        if (StringUtils.isNotBlank(documentoTipo.getFecRadicacionCorrespondencia())) {
            documentoEcm.setFecRadicado((DateUtil.parseStrDateTimeToCalendar(documentoTipo.getFecRadicacionCorrespondencia())));
        }
        
        if (StringUtils.isNotBlank(documentoTipo.getCodTipoDocumento())){
            documentoEcm.setCodTipoDocumento(documentoTipo.getCodTipoDocumento());
        }

        ExpedienteDocumentoEcm expedienteDocumentoEcm;
        if (expedienteTipo != null
                && StringUtils.isNotBlank(expedienteTipo.getIdNumExpediente())) {
            Expediente expediente = expedienteDao.find(expedienteTipo.getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró el expediente con el ID: " + expedienteTipo.getIdNumExpediente()));
            } else {
                if (documentoEcm.getExpedienteDocumentoList() != null
                        && !documentoEcm.getExpedienteDocumentoList().isEmpty()) {
                    Boolean expedienteExistente = false;
                    for (ExpedienteDocumentoEcm expedienteDocumentoEcms : documentoEcm.getExpedienteDocumentoList()) {
                        if (expedienteDocumentoEcms.getIdExpediente().equals(expedienteTipo.getIdNumExpediente())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe la relación entre el expediente: " + expedienteDocumentoEcms.getIdExpediente()
                                    + ", y el documento:" + expedienteDocumentoEcms.getDocumentoEcm().getId()));
                            expedienteExistente = true;
                            break;
                        }
                    }
                    if (!expedienteExistente) {

                        expedienteDocumentoEcm = new ExpedienteDocumentoEcm();
                        expedienteDocumentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        expedienteDocumentoEcm.setDocumentoEcm(documentoEcm);
                        expedienteDocumentoEcm.setIdExpediente(expediente.getId());
                        //Por defecto queda el codOrigen SIN REGISTRAR
                        ValorDominio codOrigen = valorDominioDao.find(OrigenDocumentoEnum.SIN_REGISTRAR.getCode());
                        if (codOrigen != null) {
                            expedienteDocumentoEcm.setCodOrigen(codOrigen);
                        } else {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró codOrigen con el valor: " + OrigenDocumentoEnum.SIN_REGISTRAR.getCode()));
                        }
                        documentoEcm.getExpedienteDocumentoList().add(expedienteDocumentoEcm);
                    }
                } else {
                    expedienteDocumentoEcm = new ExpedienteDocumentoEcm();
                    expedienteDocumentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    expedienteDocumentoEcm.setDocumentoEcm(documentoEcm);
                    expedienteDocumentoEcm.setIdExpediente(expediente.getId());
                    //Por defecto queda el codOrigen SIN REGISTRAR
                    ValorDominio codOrigen = valorDominioDao.find(OrigenDocumentoEnum.SIN_REGISTRAR.getCode());
                    if (codOrigen != null) {
                        expedienteDocumentoEcm.setCodOrigen(codOrigen);
                    } else {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codOrigen con el valor: " + OrigenDocumentoEnum.SIN_REGISTRAR.getCode()));
                    }
                    documentoEcm.getExpedienteDocumentoList().add(expedienteDocumentoEcm);
                }
            }
        }
        Validator.parseBooleanFromString(documentoTipo.getEsMover(),
                documentoTipo.getClass().getSimpleName(),
                "esMover", errorTipoList);

        if (documentoTipo.getMetadataDocumento() != null) {
            MetadataDocumento metadataDocumento = mapper.map(documentoTipo.getMetadataDocumento(), MetadataDocumento.class);
            if (metadataDocumento != null) {
                List<MetadataDocAgrupador> metadataDocAgrupadorList = populator.populateAgrupador(documentoTipo.getMetadataDocumento().getValAgrupador(),
                        metadataDocumento, contextoTransaccionalTipo);
                metadataDocumento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                if (metadataDocumento.getSerieDocumental() != null) {
                    metadataDocumento.getSerieDocumental().setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                }
                if (metadataDocAgrupadorList != null
                        && !metadataDocAgrupadorList.isEmpty()) {
                    metadataDocumento.getMetadataDocAgrupadorList().addAll(metadataDocAgrupadorList);
                }
                documentoEcm.setMetadataDocumento(metadataDocumento);
            }
        }
    }

    /**
     * implementacion de la operación asociarExpedientes se encarga de asociar
     * un listado de expedientes y un documentoTipo a l entidad DocumentoEcm
     *
     * @param documentoTipo Objeto que se asocia junto con el listado de
     * expedientes
     * @param expedienteTipoList Listado de expedientes que se va asociar al
     * DocumentoEcm
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return listado de ids de ExpedienteDocumento que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public List<Long> asociarExpedientes(final DocumentoTipo documentoTipo,
            final List<ExpedienteTipo> expedienteTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        
        
        //System.out.println("Op: ::ANDRES23:: asociarExpedientes expedienteTipoList.size(): " + expedienteTipoList.size());
        
        
        if (documentoTipo == null || StringUtils.isBlank(documentoTipo.getIdDocumento())) {
            throw new AppException("Debe proporcionar el ID del Documento");
        }

        if (expedienteTipoList == null || expedienteTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar al menos un Expediente");
        }

        final List<String> expedienteTipoIdList = new ArrayList<String>();

        for (final ExpedienteTipo expedienteTipo : expedienteTipoList) {
            if (expedienteTipo == null || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
                throw new AppException("Debe proporcionar el ID de cada expediente enviado en el listado");
            }
            expedienteTipoIdList.add(expedienteTipo.getIdNumExpediente());
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final DocumentoEcm documentoEcm = documentoDao.find(documentoTipo.getIdDocumento());

        if (documentoEcm == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un documento con el ID: " + documentoTipo.getIdDocumento()));
        }

        final List<Expediente> expedienteList = expedienteDao.findByIdList(expedienteTipoIdList);

        if (expedienteList.size() != expedienteTipoIdList.size()) {
            final List<String> notFoundExpedienteIdList = new ArrayList<String>();
            for (final String expedienteTipoId : expedienteTipoIdList) {
                boolean found = false;
                for (final Expediente expediente : expedienteList) {
                    if (expediente.getId().equals(expedienteTipoId)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    notFoundExpedienteIdList.add(expedienteTipoId);
                }
            }
            if (!notFoundExpedienteIdList.isEmpty()) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontraron los expedientes con los IDs: " + notFoundExpedienteIdList));
            }
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final List<Long> createdExpedienteDocumentoIdList = new ArrayList<Long>();

        for (final Expediente expediente : expedienteList) {
            final ExpedienteDocumentoEcm preExistExpedienteDocEcm
                    = expedienteDocumentoDao.findDocumentoExpedienteByExpedienteAndDocumento(expediente.getId(), documentoEcm.getId());
            if (preExistExpedienteDocEcm != null) {
                LOG.info("El expediente con id '" + expediente.getId()
                        + "' ya tiene asociado el documento con id '" + documentoEcm.getId() + "'");
                continue;
            }
            final ExpedienteDocumentoEcm expedienteDocumentoEcm = new ExpedienteDocumentoEcm();
            expedienteDocumentoEcm.setIdExpediente(expediente.getId());
            expedienteDocumentoEcm.setDocumentoEcm(documentoEcm);
            expedienteDocumentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            //Se deja el codOrigen sin registrar
            ValorDominio valorDominio = valorDominioDao.find(OrigenDocumentoEnum.SIN_REGISTRAR.getCode());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codOrigen con el ID: " + OrigenDocumentoEnum.SIN_REGISTRAR.getCode()));
            } else {
                expedienteDocumentoEcm.setCodOrigen(valorDominio);
            }
            final Long generatedId = expedienteDocumentoDao.create(expedienteDocumentoEcm);
            createdExpedienteDocumentoIdList.add(generatedId);
        }

        return createdExpedienteDocumentoIdList;

    }

    @Override
    public List<DocumentoTipo> buscarPorIdDocumento(List<String> idDocumentList) throws AppException {
        if (idDocumentList == null
                || idDocumentList.isEmpty()) {
            throw new AppException("Debe proporcionar una lista de ID's a buscar");
        }
        final List<DocumentoEcm> documentoEcmList
                = documentoDao.findByIdList(idDocumentList);

        final List<DocumentoTipo> documentoTipoList
                = mapper.map(documentoEcmList, DocumentoEcm.class, DocumentoTipo.class);

        return documentoTipoList;
    }

}
