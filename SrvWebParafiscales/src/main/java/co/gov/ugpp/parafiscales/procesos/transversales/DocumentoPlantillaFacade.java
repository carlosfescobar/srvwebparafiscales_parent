package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.documentoplantillatipo.v1.DocumentoPlantillaTipo;
import java.io.Serializable;

/**
 *
 * @author jmuncab
 */
public interface DocumentoPlantillaFacade extends Serializable {

    DocumentoPlantillaTipo llenarTokensDocumentoPlantilla(DocumentoPlantillaTipo documentoPlantillaTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
