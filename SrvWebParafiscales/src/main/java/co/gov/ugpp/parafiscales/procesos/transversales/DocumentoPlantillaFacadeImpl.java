/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DocumentUtils;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.datosplantillatipo.v1.DatosPlantillaTipo;
import co.gov.ugpp.schema.transversales.documentoplantillatipo.v1.DocumentoPlantillaTipo;
import com.aspose.words.Document;
import com.aspose.words.License;
import com.aspose.words.SaveFormat;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.poi.POIXMLException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

/**
 * implementaciÃ³n de la interfaz DocumentoPlantillaFacade que contiene las
 * operaciones del servicio SrvAplDocumentoPlantilla
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class DocumentoPlantillaFacadeImpl implements DocumentoPlantillaFacade {

    /**
     * implementaciÃ³n de la operaciÃ³n llenarTokensDocumentoPlantilla se
     * encarga de actualizar el contenido del documento enviado
     *
     * @param documentoPlantillaTipo Objeto por medio del cual se actualiza el
     * contenido del documento
     * @param contextoTransaccionalTipo Contiene la informaciÃ³n para almacenar
     * la auditoria.
     * @return documentoPlantillaTipo enviado que se retorna junto con el
     * contexto transaccional
     * @throws AppException ExcepciÃ³n que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public DocumentoPlantillaTipo llenarTokensDocumentoPlantilla(DocumentoPlantillaTipo documentoPlantillaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (documentoPlantillaTipo == null) {
            throw new AppException("El documento plantilla es un valor requerido");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkDocumentoPlantilla(documentoPlantillaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Map<String, List<String>> mapDatosPlantilla = new HashMap<String, List<String>>();

        for (DatosPlantillaTipo datosPlantillaTipo : documentoPlantillaTipo.getDatosPlantilla()) {
            mapDatosPlantilla.put(datosPlantillaTipo.getIdLlave(), datosPlantillaTipo.getValValor());
        }
        //Documento .docx
        XWPFDocument documentoDocx;

        InputStream is = new ByteArrayInputStream(documentoPlantillaTipo.getValContenidoDocumento());

        try {
            //Se trata de convertir a .docx
            documentoDocx = new XWPFDocument(is);

            DocumentUtils.updateTokensHeaders(documentoDocx, mapDatosPlantilla);
            DocumentUtils.updateTokensParagraphs(documentoDocx, mapDatosPlantilla);
            DocumentUtils.updateTokensTables(documentoDocx, mapDatosPlantilla);
            DocumentUtils.updateTokensFooters(documentoDocx, mapDatosPlantilla);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            documentoDocx.write(out);

            documentoPlantillaTipo.setValContenidoDocumento(out.toByteArray());

        } catch (POIXMLException ie) {

            //Se trata de convertir a .doc
            try {
                License license = new License();
                license.setLicense(Constants.LICENCE_ASPOSE);
                ByteArrayInputStream isDoc = new ByteArrayInputStream(documentoPlantillaTipo.getValContenidoDocumento());
                ByteArrayOutputStream out = new ByteArrayOutputStream();

                Document doc = new Document(isDoc);
                doc.save(out, SaveFormat.DOCX);
                InputStream salidaDocx = new ByteArrayInputStream(out.toByteArray());

                documentoDocx = new XWPFDocument(salidaDocx);
                DocumentUtils.updateTokensHeaders(documentoDocx, mapDatosPlantilla);
                DocumentUtils.updateTokensParagraphs(documentoDocx, mapDatosPlantilla);
                DocumentUtils.updateTokensTables(documentoDocx, mapDatosPlantilla);
                DocumentUtils.updateTokensFooters(documentoDocx, mapDatosPlantilla);

                ByteArrayOutputStream outDocxFinal = new ByteArrayOutputStream();
                documentoDocx.write(outDocxFinal);

                InputStream salidaDoc = new ByteArrayInputStream(outDocxFinal.toByteArray());
                Document docFinal = new Document(salidaDoc);
                ByteArrayOutputStream outDocFinal = new ByteArrayOutputStream();
                docFinal.save(outDocFinal, SaveFormat.DOC);

                documentoPlantillaTipo.setValContenidoDocumento(outDocFinal.toByteArray());

            } catch (IOException io) {
                throw new AppException(io.getMessage());
            } catch (Exception ex) {
                Logger.getLogger(DocumentoPlantillaFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
                throw new AppException(ex.getMessage());
            }
        } catch (IOException ex) {
            Logger.getLogger(DocumentoPlantillaFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new AppException(ex.getMessage());
        }
        return documentoPlantillaTipo;
    }

}
