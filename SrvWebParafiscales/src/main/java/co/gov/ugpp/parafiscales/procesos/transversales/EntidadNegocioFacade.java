package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.EntidadNegocioTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface EntidadNegocioFacade extends Serializable {

    void crearEntidadNegocio(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final EntidadNegocioTipo entidadNegocioTipo) throws AppException;

    void actualizarEntidadNegocio(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final EntidadNegocioTipo entidadNegocioTipo) throws AppException;

    PagerData<EntidadNegocioTipo> buscarPorCriteriosEntidadNegocio(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
