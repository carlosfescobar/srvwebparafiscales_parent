package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadNegocioDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocio;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocioArchivoTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.EntidadNegocioTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz EntidadNegocioFacade que contiene las
 * operaciones del servicio SrvAplEntidadNegocio
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class EntidadNegocioFacadeImpl extends AbstractFacade implements EntidadNegocioFacade {

    @EJB
    private EntidadNegocioDao entidadNegocioDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ArchivoDao archivoDao;

    /**
     * Método que implementa la operación OpCrearEntidadNegocio del servicio
     * SrvAplEntidadNegocio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param entidadNegocioTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void crearEntidadNegocio(final ContextoTransaccionalTipo contextoTransaccionalTipo, final EntidadNegocioTipo entidadNegocioTipo) throws AppException {

        if (entidadNegocioTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        EntidadNegocio entidadNegocio = entidadNegocioDao.findByCodProcesoAndIdEntidad(entidadNegocioTipo.getCodProceso(), entidadNegocioTipo.getIdEntidadNegocio());
        if (entidadNegocio != null) {
            throw new AppException("Ya existe la Entidad de Negocio con el ID: " + entidadNegocioTipo.getIdEntidadNegocio()
                    + ", asociada al Proceso con ID: " + entidadNegocioTipo.getCodProceso());
        }
        entidadNegocio = new EntidadNegocio();
        entidadNegocio.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        Validator.checkEntidadNegocio(entidadNegocioTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        populateEntidadNegocio(contextoTransaccionalTipo, entidadNegocioTipo, entidadNegocio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        entidadNegocioDao.create(entidadNegocio);
    }

    /**
     * Método que implementa la operación OpActualizarEntidadNegocio del
     * servicio SrvAplEntidadNegocio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param entidadNegocioTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarEntidadNegocio(final ContextoTransaccionalTipo contextoTransaccionalTipo, final EntidadNegocioTipo entidadNegocioTipo) throws AppException {

        if (entidadNegocioTipo == null || StringUtils.isBlank(entidadNegocioTipo.getIdEntidadNegocio())
                || StringUtils.isBlank(entidadNegocioTipo.getCodProceso())) {
            throw new AppException("Debe proporcionar el objeto a actualizar: ID de Entidad de Negocio y Código del Proceso");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final EntidadNegocio entidadNegocio = entidadNegocioDao.findByCodProcesoAndIdEntidad(entidadNegocioTipo.getCodProceso(), entidadNegocioTipo.getIdEntidadNegocio());
        if (entidadNegocio == null) {
            throw new AppException("No se encontró Entidad de Negocio con el ID: " + entidadNegocioTipo.getIdEntidadNegocio());
        }
        entidadNegocio.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        populateEntidadNegocio(contextoTransaccionalTipo, entidadNegocioTipo, entidadNegocio, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        entidadNegocioDao.edit(entidadNegocio);
    }

    /**
     * Método que implementa la operación OpBuscarPorCriterios del servicio
     * SrvAplEntidadNegocio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param parametroTipoList los parámetros de búqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @return Las entidades de negocio encontradas en base de datos
     * @throws AppException
     */
    @Override
    public PagerData<EntidadNegocioTipo> buscarPorCriteriosEntidadNegocio(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<EntidadNegocio> entidadNegocioPagerData = entidadNegocioDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        final List<EntidadNegocioTipo> entidadesNegocio = mapper.map(entidadNegocioPagerData.getData(), EntidadNegocio.class, EntidadNegocioTipo.class);
        return new PagerData<EntidadNegocioTipo>(entidadesNegocio, entidadNegocioPagerData.getNumPages());
    }

    /**
     * Método que llena la entidad EntidadNegocio
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param negocioTipo el objeto origen
     * @param errorTipoList contiene el listado de errores de la operación
     */
    private void populateEntidadNegocio(final ContextoTransaccionalTipo contextoTransaccionalTipo, final EntidadNegocioTipo entidadNegocioTipo,
            final EntidadNegocio entidadNegocio, final List<ErrorTipo> errorTipoList) throws AppException {

        if (entidadNegocioTipo.getIdArchivosTemporales() != null && !entidadNegocioTipo.getIdArchivosTemporales().isEmpty()) {
            for (ParametroValoresTipo archivosTemporales : entidadNegocioTipo.getIdArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivosTemporales.getIdLlave())) {
                    ValorDominio codTipoArchivoTemporal = valorDominioDao.find(archivosTemporales.getIdLlave());
                    if (codTipoArchivoTemporal == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoArchivoTemporal con el ID: " + archivosTemporales.getIdLlave()));
                    } else {
                        for (ParametroTipo archivoTemporal : archivosTemporales.getValValor()) {
                            Archivo archivoTemporalEncontrado = archivoDao.find(Long.valueOf(archivoTemporal.getIdLlave()));
                            if (archivoTemporalEncontrado == null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "No se encontró Archivo Temporal con el ID: " + archivoTemporal.getIdLlave()));
                            } else {
                                EntidadNegocioArchivoTemporal entidadNegocioArchivoTemporal = new EntidadNegocioArchivoTemporal();
                                entidadNegocioArchivoTemporal.setCodTipoArchivoTemporal(codTipoArchivoTemporal);
                                entidadNegocioArchivoTemporal.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                entidadNegocioArchivoTemporal.setArchivoTemporal(archivoTemporalEncontrado);
                                entidadNegocioArchivoTemporal.setEntidadNegocio(entidadNegocio);
                                entidadNegocio.getArchivosTemporales().add(entidadNegocioArchivoTemporal);
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoArchivoTemporal")));
                }
            }
        }

        if (StringUtils.isNotBlank(entidadNegocioTipo.getIdEntidadNegocio())) {
            entidadNegocio.setIdEntidadNegocio(entidadNegocioTipo.getIdEntidadNegocio());
        }
        if (StringUtils.isNotBlank(entidadNegocioTipo.getCodProceso())) {
            ValorDominio codProceso = valorDominioDao.find(entidadNegocioTipo.getCodProceso());
            if (codProceso == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codProceso con el ID: " + entidadNegocioTipo.getCodProceso()));
            } else {
                entidadNegocio.setCodProceso(codProceso);
            }
        }
    }
}
