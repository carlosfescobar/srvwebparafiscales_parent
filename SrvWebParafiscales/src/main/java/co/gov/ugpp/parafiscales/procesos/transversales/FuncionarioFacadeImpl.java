package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.departamentotipo.v1.DepartamentoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.enums.LdapEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.LdapAuthentication;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;
import edu.emory.mathcs.backport.java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz FuncionarioFacade que contiene las operaciones
 * del servicio SrvAplFuncionario
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class FuncionarioFacadeImpl extends AbstractFacade implements FuncionarioFacade {

    private static final Logger LOG = LoggerFactory.getLogger(FuncionarioFacadeImpl.class);

    @EJB
    private LdapAuthentication ldapAuthentication;

    /**
     * implementación de la operación buscarFuncionario se encarga de buscar un
     * funcionario de acuerdo a un idFuncionario enviado
     *
     * @param solicitante Obtiene el valor del idfuncionario enviado
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return funcionario encontrado que se retorna junto con elcontexto
     * Transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public FuncionarioTipo buscarFuncionario(final FuncionarioTipo solicitante, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (solicitante == null || StringUtils.isBlank(solicitante.getIdFuncionario())) {
            return null;
        }

        List<ParametroTipo> parametroTipoList = new ArrayList<ParametroTipo>();

        ParametroTipo parametroTipo = new ParametroTipo();
        parametroTipo.setIdLlave("idFuncionario");
        parametroTipo.setValValor(solicitante.getIdFuncionario());
        parametroTipoList.add(parametroTipo);
        contextoTransaccionalTipo.setValNumPagina(1L);
        contextoTransaccionalTipo.setValTamPagina(1L);
        PagerData<FuncionarioTipo> funcionarioTipos
                = this.buscarPorCriteriosFuncionario(parametroTipoList, null, contextoTransaccionalTipo);

        // Se tiene como Premisa que en las lista solo exista un Funcionario
        return funcionarioTipos.getData() != null && !funcionarioTipos.getData().isEmpty()
                ? funcionarioTipos.getData().get(0) : null;
    }

    /**
     * implementación de la operación buscarPorCriteriosFuncionario se encarga
     * de buscar un listado de funcionarios por medio de una lista de parametros
     * enviados y ordena la consulta de acuerdo a los criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se busca un
     * funcionario en la base de datos
     * @param criterioOrdenamientoTipos atributos por los cuales se ordena la
     * consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return listado de funcionarios encotrados que se retorna junto con el
     * contexto transaccional.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<FuncionarioTipo> buscarPorCriteriosFuncionario(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        DirContext ldapContext = null;

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final String user = contextoTransaccionalTipo.getIdUsuarioAplicacion();
        final String password = contextoTransaccionalTipo.getValClaveUsuarioAplicacion();

        //Carga de parametros de conexión de LDAP
        final Properties props = LdapAuthentication.obtainProperties();

        final List<FuncionarioTipo> funcionarios = new ArrayList<FuncionarioTipo>();

        try {

            //Creación de conexión a LDAP
            ldapContext = ldapAuthentication.getLdapConnection(props, user, password);

            //Extracción de parametros de busqueda
            final StringBuilder builder = new StringBuilder();

            for (int i = 0, paramsSize = parametroTipoList.size(); i < paramsSize; i++) {
                final String parametro = props.getProperty("ldap." + parametroTipoList.get(i).getIdLlave());
                final String valorParametro = parametroTipoList.get(i).getValValor();
                if (StringUtils.isNotBlank(valorParametro)) {
                    builder.append("(").append(parametro).append("=").append(valorParametro).append(")");
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "El parametro ingresados para la busqueda del funcionario es invalido: " + parametroTipoList.get(i).getIdLlave()));
                }
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            // Filtro de objeto user paramétro y valor
            final String searchFilter = "(&(objectClass=user)(" + builder.toString() + "))";

            //Parametros a retornar por la consulta
            final SearchControls searchControls = new SearchControls();
            searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            searchControls.setReturningObjFlag(true);
            searchControls.setReturningAttributes(LdapEnum.ATTR_IDS_LDAP);
            //Busqueda en LDAP
            final NamingEnumeration result = ldapContext.search(props.getProperty("ldap.busqueda.usuario.base"), searchFilter, searchControls);

            while (result.hasMoreElements()) {

                final FuncionarioTipo funcionario = new FuncionarioTipo();
                final SearchResult searchResult = (SearchResult) result.next();
                final Attributes attributes = searchResult.getAttributes();
                //Datos de funcionario
                Attribute gruposAsociados = attributes.get("memberOf");
                if (gruposAsociados != null) {
                    StringBuilder sbDependencia = new StringBuilder();
                    NamingEnumeration ne = gruposAsociados.getAll();
                    while (ne.hasMoreElements()) {
                        String grupoAsociado = (String) ne.next();
                        grupoAsociado = grupoAsociado.replace(Constants.PREFIJO_LDAP_GROUP, "");
                        List<String> grupoAsociadoSplit = Arrays.asList(grupoAsociado.split(","));
                        if (grupoAsociadoSplit != null
                                && !grupoAsociadoSplit.isEmpty()) {
                            sbDependencia.append(",").append(grupoAsociadoSplit.get(0));
                        }
                    }
                    funcionario.setValDependenciaFuncionario(sbDependencia.toString());
                    funcionario.setValDependenciaFuncionario(funcionario.getValDependenciaFuncionario().replaceFirst(",", ""));
                }
                funcionario.setIdFuncionario(attributes.get("sAMAccountName") != null ? (String) attributes.get("sAMAccountName").get() : null);
                funcionario.setValCargoFuncionario(attributes.get("title") != null ? (String) attributes.get("title").get() : null);
                //Datos de persona natural
                funcionario.setValNombreCompleto(attributes.get("displayName") != null ? (String) attributes.get("displayName").get() : null);
                funcionario.setValPrimerApellido(attributes.get("sn") != null ? splitStringSp((String) attributes.get("sn").get(), "1") : null);
                funcionario.setValSegundoApellido(attributes.get("sn") != null ? splitStringSp((String) attributes.get("sn").get(), "2") : null);
                funcionario.setValPrimerNombre(attributes.get("givenname") != null ? splitStringSp((String) attributes.get("givenname").get(), "1") : null);
                funcionario.setValSegundoNombre(attributes.get("givenname") != null ? splitStringSp((String) attributes.get("givenname").get(), "2") : null);
                IdentificacionTipo identificacion = new IdentificacionTipo();
                identificacion.setValNumeroIdentificacion(attributes.get("description") != null ? (String) attributes.get("description").get() : null);
                funcionario.setIdPersona(identificacion);

                final ContactoPersonaTipo contactoPersonaTipo = new ContactoPersonaTipo();
                //Telefono
                final String telefono = attributes.get("telephoneNumber") == null ? null : (String.valueOf(attributes.get("telephoneNumber").get()));
                if (StringUtils.isNotBlank(telefono)) {
                    final TelefonoTipo telefonoTipo = new TelefonoTipo();
                    telefonoTipo.setValNumeroTelefono(telefono);
                    contactoPersonaTipo.getTelefonos().add(telefonoTipo);
                }
                final String valDireccion = attributes.get("physicalDeliveryOfficeName") != null ? (String) attributes.get("physicalDeliveryOfficeName").get() : null;
                if (StringUtils.isNotBlank(valDireccion)) {
                    //Ubicación de la persona
                    final UbicacionPersonaTipo ubicacion = new UbicacionPersonaTipo();
                    final MunicipioTipo municipio = new MunicipioTipo();
                    municipio.setValNombre(attributes.get("l") != null ? (String) attributes.get("l").get() : null);
                    final DepartamentoTipo departamento = new DepartamentoTipo();
                    departamento.setValNombre(attributes.get("st") != null ? (String) attributes.get("st").get() : null);
                    municipio.setDepartamento(departamento);
                    ubicacion.setMunicipio(municipio);
                    ubicacion.setValDireccion(valDireccion);
                    contactoPersonaTipo.getUbicaciones().add(ubicacion);
                }
                final String valEmail = attributes.get("mail") != null ? (String) attributes.get("mail").get() : null;
                if (StringUtils.isNotBlank(valEmail)) {
                    final CorreoElectronicoTipo correo = new CorreoElectronicoTipo();
                    correo.setValCorreoElectronico(valEmail);
                    contactoPersonaTipo.getCorreosElectronicos().add(correo);
                }
                funcionario.setContacto(contactoPersonaTipo);
                funcionarios.add(funcionario);
            }

        } catch (NamingException ex) {
            LOG.warn("NamingException al buscar el Funcionario por criterios", ex);
            return new PagerData<FuncionarioTipo>(Collections.EMPTY_LIST, 0);
        } finally {
            ldapAuthentication.closeLdapConnection(ldapContext);
        }

        final int rowNum = funcionarios.size();

        if (rowNum == 0) {
            return new PagerData<FuncionarioTipo>(Collections.EMPTY_LIST, 0);
        }

        final int tamPagina = (contextoTransaccionalTipo.getValTamPagina() == null || contextoTransaccionalTipo.getValTamPagina() <= 0)
                ? rowNum : contextoTransaccionalTipo.getValTamPagina().intValue();
        final int numPagina = (contextoTransaccionalTipo.getValNumPagina() == null || contextoTransaccionalTipo.getValNumPagina() <= 0)
                ? 1 : contextoTransaccionalTipo.getValNumPagina().intValue();

        final int fromIndex = (tamPagina * numPagina) - tamPagina;
        final int toIndex = tamPagina > rowNum ? rowNum : fromIndex + tamPagina;

        final List<FuncionarioTipo> funcionarioTipoList = funcionarios.subList(fromIndex, toIndex);

        return new PagerData(funcionarioTipoList, rowNum);
    }

    /**
     * Retornar el primer o segundo valor de la instrucción split por espacio
     *
     * @autor janguloh - everiss
     * @param text
     * @param val
     * @return
     */
    private String splitStringSp(String text, String val) {
        String value = "";
        String split[];
        if (val.equals("1")) {
            split = text.split(" ");
            if (split.length >= 0) {
                value = split[0];
            } else {
                value = "";
            }
        } else if (val.equals("2")) {
            split = text.split(" ");
            if (split.length == 2) {
                value = split[1];
            } else {
                value = "";
            }
        }
        return value;
    }

}
