package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;

/**
 *
 * @author jmunocab
 */
public interface HallazgoFacade {

    Long crearHallazgoCruceNominaPila(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final HallazgoNominaTipo hallazgoNominaTipo) throws AppException;

    Long crearHallazgoCruceConciliacionContable(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) throws AppException;

    HallazgoNominaTipo obtenerDatosBasicosHallazgoNomina(final ExpedienteTipo expedienteTipo) throws AppException;

    HallazgoConciliacionContableTipo obtenerDatosBasicosHallazgoConciliacionContable(final ExpedienteTipo expedienteTipo) throws AppException;
    
    void actualizarHallazgoConciliacionContable (final ContextoTransaccionalTipo contextoTransaccionalTipo, final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) throws AppException;
    
    void actualizarHallazgoNominaPila (final ContextoTransaccionalTipo contextoTransaccionalTipo, final HallazgoNominaTipo hallazgoNominaTipo) throws AppException;
}
