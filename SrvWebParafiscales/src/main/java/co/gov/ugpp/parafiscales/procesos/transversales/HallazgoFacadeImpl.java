package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoHallazgoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.HallazgoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgodetalleconciliacioncontabletipo.v1.HallazgoDetalleConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1.HallazgoDetalleNominaTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz HallazgoFacade que contiene las operaciones de
 * creación de hallazgos
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class HallazgoFacadeImpl extends AbstractFacade implements HallazgoFacade {

    @EJB
    private HallazgoDao hallazgoDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @Override
    public Long crearHallazgoCruceNominaPila(ContextoTransaccionalTipo contextoTransaccionalTipo, HallazgoNominaTipo hallazgoNominaTipo) throws AppException {
        if (hallazgoNominaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Hallazgo hallazgo = new Hallazgo();
        hallazgo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateHallazgoFromHallazgoNomina(hallazgo, hallazgoNominaTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idHallazgo = hallazgoDao.create(hallazgo);

        return idHallazgo;

    }

    @Override
    public Long crearHallazgoCruceConciliacionContable(final ContextoTransaccionalTipo contextoTransaccionalTipo, final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) throws AppException {
        if (hallazgoConciliacionContableTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Hallazgo hallazgo = new Hallazgo();
        hallazgo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateHallazgoFromHallazgoConciliacionContable(hallazgo, hallazgoConciliacionContableTipo, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idHallazgo = hallazgoDao.create(hallazgo);

        return idHallazgo;

    }

    /**
     * Método que llena el entity hallazgo desde el objeto hallazgoNomina
     *
     * @param hallazgo Objeto destino
     * @param hallazgoNominaTipo Objeto origen
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @param errorTipoList
     * @throws AppException
     */
    private void populateHallazgoFromHallazgoNomina(final Hallazgo hallazgo, final HallazgoNominaTipo hallazgoNominaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(hallazgoNominaTipo.getIdNomina())) {
            hallazgo.setIdNomina(Long.valueOf(hallazgoNominaTipo.getIdNomina()));
        }
        if (StringUtils.isNotBlank(hallazgoNominaTipo.getIdPila())) {
            hallazgo.setIdPila(Long.valueOf(hallazgoNominaTipo.getIdPila()));

        }
        if (hallazgoNominaTipo.getExpediente() != null
                && StringUtils.isNotBlank(hallazgoNominaTipo.getExpediente().getIdNumExpediente())) {
            Expediente expediente = expedienteDao.find(hallazgoNominaTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró expediente con el ID: " + hallazgoNominaTipo.getExpediente().getIdNumExpediente()));
            } else {
                hallazgo.setExpediente(expediente);
            }
        }

        ValorDominio codTipoHallazgo = valorDominioDao.find(TipoHallazgoEnum.CRUCE_PILA_NOMINA.getCode());

        if (codTipoHallazgo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se econtró tipoHallazgo con el ID: " + TipoHallazgoEnum.CRUCE_PILA_NOMINA.getCode()));
        } else {
            hallazgo.setCodTipoHallazgo(codTipoHallazgo);
        }
    }

    /**
     * Método que llena el entity hallazgo desde el objeto
     * hallazgoConciliacionContable
     *
     * @param hallazgo Objeto destino
     * @param hallazgoConciliacionContableTipo Objeto origen
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @param errorTipoList
     * @throws AppException
     */
    private void populateHallazgoFromHallazgoConciliacionContable(final Hallazgo hallazgo, final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(hallazgoConciliacionContableTipo.getIdNomina())) {
            hallazgo.setIdNomina(Long.valueOf(hallazgoConciliacionContableTipo.getIdNomina()));
        }
        if (StringUtils.isNotBlank(hallazgoConciliacionContableTipo.getIdPila())) {
            hallazgo.setIdPila(Long.valueOf(hallazgoConciliacionContableTipo.getIdPila()));

        }
        if (hallazgoConciliacionContableTipo.getExpediente() != null
                && StringUtils.isNotBlank(hallazgoConciliacionContableTipo.getExpediente().getIdNumExpediente())) {
            Expediente expediente = expedienteDao.find(hallazgoConciliacionContableTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró expediente con el ID: " + hallazgoConciliacionContableTipo.getExpediente().getIdNumExpediente()));
            } else {
                hallazgo.setExpediente(expediente);
            }
        }

        ValorDominio codTipoHallazgo = valorDominioDao.find(TipoHallazgoEnum.CRUCE_CONCILIACION_CONTABLE.getCode());

        if (codTipoHallazgo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se econtró tipoHallazgo con el ID: " + TipoHallazgoEnum.CRUCE_CONCILIACION_CONTABLE.getCode()));
        } else {
            hallazgo.setCodTipoHallazgo(codTipoHallazgo);
        }
    }

    @Override
    public HallazgoNominaTipo obtenerDatosBasicosHallazgoNomina(final ExpedienteTipo expedienteTipo) throws AppException {
        if (expedienteTipo == null
                || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            throw new AppException("Debe enviar el expediente a consultar");
        }

        final List<Object[]> infoHallazgo
                = hallazgoDao.buscarInformacionHallazgo(expedienteTipo.getIdNumExpediente(), Constants.OBTENER_INFO_HALLAZGO);

        if (infoHallazgo == null
                || infoHallazgo.isEmpty()) {
            throw new AppException("No se econtró información relacionada en conciliaciónContable  con el idExpediente: " + expedienteTipo.getIdNumExpediente());
        }

        HallazgoNominaTipo hallazgoNominaTipo = new HallazgoNominaTipo();
        this.buildHallazgoNominaTipoFromObject(infoHallazgo, hallazgoNominaTipo);

        return hallazgoNominaTipo;

    }

    @Override
    public HallazgoConciliacionContableTipo obtenerDatosBasicosHallazgoConciliacionContable(final ExpedienteTipo expedienteTipo) throws AppException {
        if (expedienteTipo == null
                || StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            throw new AppException("Debe enviar el expediente a consultar");
        }

        final List<Object[]> infoHallazgo
                = hallazgoDao.buscarInformacionHallazgo(expedienteTipo.getIdNumExpediente(), Constants.OBTENER_INFO_HALLAZGO);

        if (infoHallazgo == null
                || infoHallazgo.isEmpty()) {
            throw new AppException("No se econtró información relacionada en conciliaciónContable  con el idExpediente: " + expedienteTipo.getIdNumExpediente());
        }

        HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo = new HallazgoConciliacionContableTipo();
        this.buildHallazgoConciliacionContableFromObject(infoHallazgo, hallazgoConciliacionContableTipo);

        return hallazgoConciliacionContableTipo;
    }

    @Override
    public void actualizarHallazgoConciliacionContable(final ContextoTransaccionalTipo contextoTransaccionalTipo, final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) throws AppException {

        if (hallazgoConciliacionContableTipo.getHallazgosDetalle() == null
                || hallazgoConciliacionContableTipo.getHallazgosDetalle().isEmpty()) {
            throw new AppException("Debe enviar un listado de hallazgo detalle a actualizar");
        }

        final List<Object[]> hallazgosDetalle = new ArrayList<Object[]>();

        for (HallazgoDetalleConciliacionContableTipo hallazgoDetalleConciliacionContableTipo : hallazgoConciliacionContableTipo.getHallazgosDetalle()) {

            Object[] hallazgoDetalle = new Object[Constants.CAMPOS_ACTUALIZAR_HALLAZGO_CONCILIACION_CONTABLE];
            hallazgoDetalle[Constants.ID_HALLAZGO_CONCILIACION] = Boolean.valueOf(hallazgoDetalleConciliacionContableTipo.getEsHallazgo());
            hallazgoDetalle[Constants.OBS_HALLAZGO_CONCILIACION] = hallazgoDetalleConciliacionContableTipo.getDescObservacionHallazgo();
            hallazgoDetalle[Constants.ID_USUARIO_MODIFICACION_CONCILIACION] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.FEC_ACTUALIZACION_CONCILIACION] = new Date();
            hallazgoDetalle[Constants.ID_HALLAZGO_DETALLE_CONCILIACION] = hallazgoDetalleConciliacionContableTipo.getIdHallazgoConciliacionContableDetalle();

            hallazgosDetalle.add(hallazgoDetalle);
        }
        try {
            hallazgoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.ACTUALIZAR_HALLAZGO_CONCILIACION_CONTABLE), hallazgosDetalle);
        } catch (SQLException sqle) {
            throw new AppException("Error creando el hallazgo" + sqle.getMessage());
        }
    }

    @Override
    public void actualizarHallazgoNominaPila(ContextoTransaccionalTipo contextoTransaccionalTipo, HallazgoNominaTipo hallazgoNominaTipo) throws AppException {

        final List<Object[]> hallazgosDetalle = new ArrayList<Object[]>();

        for (HallazgoDetalleNominaTipo hallazgoDetalleNominaTipo : hallazgoNominaTipo.getHallazgosDetalle()) {

            Object[] hallazgoDetalle = new Object[Constants.CAMPOS_ACTUALIZAR_HALLAZGO_NOMINA];
            hallazgoDetalle[Constants.ID_HALLAZGO_NOMINA] = Boolean.valueOf(hallazgoDetalleNominaTipo.getEsHallazgo());
            hallazgoDetalle[Constants.OBS_HALLAZGO_NOMINA] = hallazgoDetalleNominaTipo.getDescObservacionHallazgo();
            hallazgoDetalle[Constants.ID_USUARIO_MODIFICACION_NOMINA] = contextoTransaccionalTipo.getIdUsuario();
            hallazgoDetalle[Constants.FEC_ACTUALIZACION_NOMINA] = new Date();
            hallazgoDetalle[Constants.ID_HALLAZGO_DETALLE_NOMINA] = hallazgoDetalleNominaTipo.getIdHallazgoNominaDetalle();
            hallazgosDetalle.add(hallazgoDetalle);
        }
        try {
            hallazgoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.ACTUALIZAR_HALLAZGO_NOMINA), hallazgosDetalle);
        }catch (SQLException sqle) {
            throw new AppException("Error actualizando el hallazgo de nómina" + sqle.getMessage());
        }
    }

    private void buildHallazgoNominaTipoFromObject(final List<Object[]> srcObj, final HallazgoNominaTipo hallazgoNominaTipo) {

        for (Object[] hallazgoInfo : srcObj) {
            hallazgoNominaTipo.setIdNomina(hallazgoInfo[Constants.NOMINA] == null ? null : hallazgoInfo[Constants.NOMINA].toString());
            hallazgoNominaTipo.setIdPila(hallazgoInfo[Constants.NOMINA] == null ? null : hallazgoInfo[Constants.PILA].toString());
            hallazgoNominaTipo.setIdConciliacionContable(hallazgoInfo[Constants.CONCILIACION_CONTABLE] == null ? null : hallazgoInfo[Constants.CONCILIACION_CONTABLE].toString());

            if (hallazgoInfo[Constants.EXPEDIENTE] != null) {
                ExpedienteTipo expedienteTipo = new ExpedienteTipo();
                expedienteTipo.setIdNumExpediente(hallazgoInfo[Constants.EXPEDIENTE].toString());
                hallazgoNominaTipo.setExpediente(expedienteTipo);
            }
            break;
        }
    }

    private void buildHallazgoConciliacionContableFromObject(final List<Object[]> srcObj, final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo) {

        for (Object[] hallazgoInfo : srcObj) {
            hallazgoConciliacionContableTipo.setIdNomina(hallazgoInfo[Constants.NOMINA] == null ? null : hallazgoInfo[Constants.NOMINA].toString());
            hallazgoConciliacionContableTipo.setIdPila(hallazgoInfo[Constants.NOMINA] == null ? null : hallazgoInfo[Constants.PILA].toString());
            hallazgoConciliacionContableTipo.setIdConciliacionContable(hallazgoInfo[Constants.CONCILIACION_CONTABLE] == null ? null : hallazgoInfo[Constants.CONCILIACION_CONTABLE].toString());

            if (hallazgoInfo[Constants.EXPEDIENTE] != null) {
                ExpedienteTipo expedienteTipo = new ExpedienteTipo();
                expedienteTipo.setIdNumExpediente(hallazgoInfo[Constants.EXPEDIENTE].toString());
                hallazgoConciliacionContableTipo.setExpediente(expedienteTipo);
            }
            break;
        }
    }
}
