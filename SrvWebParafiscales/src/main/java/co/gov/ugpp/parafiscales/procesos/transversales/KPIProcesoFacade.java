package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author jmunocab
 */
public interface KPIProcesoFacade extends Serializable {

    List<ParametroValoresTipo> consultarKPIProceso(final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final String codTipoConsulta, final Calendar fecInicio, final Calendar fecFin, final List<ParametroTipo> parametroTipoList) throws AppException;
}
