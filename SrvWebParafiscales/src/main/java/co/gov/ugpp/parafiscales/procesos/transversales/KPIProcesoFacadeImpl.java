package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.TipoConsultaKPIEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ParametroDao;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementaciÃ³n de la interfaz NumeroActoAdministrativoFacade que contiene las
 * operaciones del servicio SrvAplNumeroActoAdministrativo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class KPIProcesoFacadeImpl extends AbstractFacade implements KPIProcesoFacade {

    @EJB
    private ParametroDao parametroDao;

    /**
     * MÃ©todo que realiza la consulta de KPI'S para los procesos del BPM de
     * parafiscales
     *
     * @param contextoTransaccionalTipo contiene los datos de auditorÃ­a
     * @param codTipoConsulta el tipoo de consulta a realizar
     * @param fecInicio la fecha de inicio de consulta
     * @param fecFin la fecha final de la consulta
     * @param parametroTipoList los parÃ¡metros de consulta
     * @return el listado de objetos encontrados
     * @throws AppException
     */
    @Override
    public List<ParametroValoresTipo> consultarKPIProceso(ContextoTransaccionalTipo contextoTransaccionalTipo, String codTipoConsulta, Calendar fecInicio, Calendar fecFin, List<ParametroTipo> parametroTipoList) throws AppException {
        if (StringUtils.isBlank(codTipoConsulta) || fecFin == null || fecInicio == null) {
            throw new AppException("Los parametros de consulta no son validos");
        }
        //El nombre de la consulta a ejecutar
        List<String> valNombresConsulta = null;
        List<ParametroValoresTipo> objetosrResultantes = null;
        for (TipoConsultaKPIEnum tipoConsultaKPIEnum : TipoConsultaKPIEnum.values()) {
            if (tipoConsultaKPIEnum.getCodTipoKPI().equals(codTipoConsulta)) {
                valNombresConsulta = tipoConsultaKPIEnum.getValNombreConsulta();
                break;
            }
        }
        if (valNombresConsulta != null && !valNombresConsulta.isEmpty()) {
            for (String nombreConsulta : valNombresConsulta) {
                List<Object[]> objetosEncontrados = parametroDao.ejecutarConsultaKPI(fecInicio, fecFin, parametroTipoList, nombreConsulta);
                if (objetosEncontrados != null
                        && !objetosEncontrados.isEmpty()) {
                    if (objetosrResultantes == null) {
                        objetosrResultantes = new ArrayList<ParametroValoresTipo>();
                    }
                    for (Object[] atributosObjetoEncontrado : objetosEncontrados) {
                        ParametroValoresTipo objetoResultante = new ParametroValoresTipo();
                        for (Object atributoObjetoEncontrado : atributosObjetoEncontrado) {
                            ParametroTipo atributoObjetoResultante = new ParametroTipo();
                            atributoObjetoResultante.setValValor(atributoObjetoEncontrado == null ? null : atributoObjetoEncontrado.toString());
                            objetoResultante.getValValor().add(atributoObjetoResultante);
                        }
                        objetosrResultantes.add(objetoResultante);
                    }
                }
            }
        }
        if (objetosrResultantes == null){
            return Collections.EMPTY_LIST;
        }
        return objetosrResultantes;
    }
}
