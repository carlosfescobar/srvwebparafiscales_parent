package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import co.gov.ugpp.schema.transversales.migraciontipo.v1.MigracionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface MigracionFacade extends Serializable {

    Long crearMigracion(MigracionTipo migracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarMigracion(MigracionTipo migracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<MigracionTipo> buscarPorCriteriosMigracion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
     ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    void crearCasoMigracion(MigracionCasoTipo migracionCasoTipo , ContextoTransaccionalTipo contextoTransaccionalTipo )throws AppException;
    
    void actualizarCasoMigracion(MigracionCasoTipo migracionCasoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException;
    
    PagerData<MigracionCasoTipo> buscarPorCriteriosCasoMigracion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
             ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
