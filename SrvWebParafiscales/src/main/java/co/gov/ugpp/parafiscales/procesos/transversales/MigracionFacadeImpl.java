package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.MigracionCasoParafiscalesDao;
import co.gov.ugpp.parafiscales.persistence.dao.MigracionParafiscalesDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.MigracionCasoParafiscales;
import co.gov.ugpp.parafiscales.persistence.entity.MigracionParafiscales;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import co.gov.ugpp.schema.transversales.migraciontipo.v1.MigracionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

@Stateless
@TransactionManagement
public class MigracionFacadeImpl extends AbstractFacade implements MigracionFacade {

    @EJB
    private MigracionParafiscalesDao migracionParafiscalesDao;

    @EJB
    private MigracionCasoParafiscalesDao migracionCasoParafiscalesDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    /**
     * MÃ©todo que implementa la operaciÃ³n OpCrearMigracion del servicio
     * SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param migracionTipo el objeto origen
     * @return el id de negocio creado
     * @throws AppException
     */
    @Override
    public Long crearMigracion(MigracionTipo migracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (migracionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        Validator.checkMigracion(migracionTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        MigracionParafiscales migracionParafiscales = new MigracionParafiscales();
        migracionParafiscales.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateMigracionParafiscales(migracionTipo, migracionParafiscales, errorTipoList, contextoTransaccionalTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        return migracionParafiscalesDao.create(migracionParafiscales);
    }

    /**
     * MÃ©todo que implementa la operaciÃ³n OpActualizarMigracion del servicio
     * SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param migracionTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarMigracion(MigracionTipo migracionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (migracionTipo == null || StringUtils.isBlank(migracionTipo.getIdMigracion())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        MigracionParafiscales migracionParafiscales = migracionParafiscalesDao.find(Long.valueOf(migracionTipo.getIdMigracion()));
        if (migracionParafiscales == null) {
            throw new AppException("No se encontrÃ³ MigraciÃ³n con el ID: " + migracionTipo.getIdMigracion());
        }
        migracionParafiscales.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateMigracionParafiscales(migracionTipo, migracionParafiscales, errorTipoList, contextoTransaccionalTipo);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        migracionParafiscalesDao.edit(migracionParafiscales);
    }

    /**
     * MÃ©todo que implementa la operaciÃ³n OpBuscarPorCriteriosMigracion del
     * servicio SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param parametroTipoList los parÃ¡metros de bÃºqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @return Las entidades de negocio encontradas en base de datos
     * @throws AppException
     */
    @Override
    public PagerData<MigracionTipo> buscarPorCriteriosMigracion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        PagerData<MigracionParafiscales> migracionCasoPagerData = migracionParafiscalesDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        List<MigracionTipo> migracionCasoTipoList = mapper.map(migracionCasoPagerData.getData(), MigracionParafiscales.class, MigracionTipo.class);
        return new PagerData<MigracionTipo>(migracionCasoTipoList, migracionCasoPagerData.getNumPages());
    }

    /**
     * MÃ©todo que implementa la operaciÃ³n OpCrearMigracionCaso del servicio
     * SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param migracionCasoTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void crearCasoMigracion(MigracionCasoTipo migracionCasoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (migracionCasoTipo == null || StringUtils.isBlank(migracionCasoTipo.getIdFila())
                || StringUtils.isBlank(migracionCasoTipo.getIdMigracion())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        Validator.checkMigracionCaso(migracionCasoTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        MigracionCasoParafiscales migracionCasoParafiscales = migracionCasoParafiscalesDao.findByCasoAndMigracionPadre(Long.valueOf(migracionCasoTipo.getIdMigracion()), migracionCasoTipo.getIdFila());
        if (migracionCasoParafiscales != null) {
            throw new AppException("Ya existe un Caso Migrado con el ID Migracion: "
                    + migracionCasoTipo.getIdMigracion() + ", y el idFila: " + migracionCasoTipo.getIdFila());
        }
        migracionCasoParafiscales = new MigracionCasoParafiscales();
        migracionCasoParafiscales.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateMigracionCasoParafiscales(migracionCasoTipo, migracionCasoParafiscales, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        migracionCasoParafiscalesDao.create(migracionCasoParafiscales);

    }

    /**
     * MÃ©todo que implementa la operaciÃ³n OpActualizarMigracionCaso del
     * servicio SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param migracionCasoTipo el objeto origen
     * @throws AppException
     */
    @Override
    public void actualizarCasoMigracion(MigracionCasoTipo migracionCasoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (migracionCasoTipo == null || StringUtils.isBlank(migracionCasoTipo.getIdFila())
                || StringUtils.isBlank(migracionCasoTipo.getIdMigracion())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        MigracionCasoParafiscales migracionCasoParafiscales = migracionCasoParafiscalesDao.findByCasoAndMigracionPadre(Long.valueOf(migracionCasoTipo.getIdMigracion()), migracionCasoTipo.getIdFila());
        if (migracionCasoParafiscales == null) {
            throw new AppException("No se encontrÃ³ Caso Migrado con el ID MigraciÃ³n: "
                    + migracionCasoTipo.getIdMigracion() + ", y el idFila: " + migracionCasoTipo.getIdFila());
        }
        migracionCasoParafiscales.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateMigracionCasoParafiscales(migracionCasoTipo, migracionCasoParafiscales, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        migracionCasoParafiscalesDao.edit(migracionCasoParafiscales);
    }

    /**
     * MÃ©todo que implementa la operaciÃ³n OpBuscarPorCriteriosMigracion del
     * servicio SrvAplMigracion
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @param parametroTipoList los parÃ¡metros de bÃºqueda
     * @param criterioOrdenamientoTipos los criterios de ordenamiento
     * @return Las entidades de negocio encontradas en base de datos
     * @throws AppException
     */
    @Override
    public PagerData<MigracionCasoTipo> buscarPorCriteriosCasoMigracion(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        PagerData<MigracionCasoParafiscales> migracionCasoPagerData = migracionCasoParafiscalesDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos,
                contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());
        List<MigracionCasoTipo> migracionCasoTipoList = mapper.map(migracionCasoPagerData.getData(), MigracionCasoParafiscales.class, MigracionCasoTipo.class);
        return new PagerData<MigracionCasoTipo>(migracionCasoTipoList, migracionCasoPagerData.getNumPages());
    }

    /**
     * MÃ©todo que llena la entidad de negocio MigracionParaFiscales
     *
     * @param migracionTipo el objeto origen
     * @param migracionParafiscales el objeto destino
     * @param errorTipoList el listado de errores de la operaciÃ³n
     * @throws AppException
     */
    private void populateMigracionParafiscales(final MigracionTipo migracionTipo, final MigracionParafiscales migracionParafiscales,
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (migracionTipo.getFecMigracion() != null) {
            migracionParafiscales.setFecMigracion(migracionTipo.getFecMigracion());
        }
        if (StringUtils.isNotBlank(migracionTipo.getIdUsuario())) {
            migracionParafiscales.setIdUsuarioResponsable(migracionTipo.getIdUsuario());
        }
        if (StringUtils.isNotBlank(migracionTipo.getCodEscenarioMigrado())) {
            ValorDominio valorDominio = valorDominioDao.find(migracionTipo.getCodEscenarioMigrado());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontrÃ³ codEscenarioMigrado con el ID: " + migracionTipo.getCodEscenarioMigrado()));
            } else {
                migracionParafiscales.setCodCasoMigrado(valorDominio);
            }
        }
        if (migracionTipo.getCasosMigrados() != null && !migracionTipo.getCasosMigrados().isEmpty()) {
            for (MigracionCasoTipo migracionCasoTipo : migracionTipo.getCasosMigrados()) {
                if (StringUtils.isNotBlank(migracionCasoTipo.getIdFila())) {
                    if (migracionParafiscales.getId() != null) {
                        MigracionCasoParafiscales migracionCasoParafiscales = migracionCasoParafiscalesDao.findByCasoAndMigracionPadre(migracionParafiscales.getId(), migracionCasoTipo.getIdFila());
                        if (migracionCasoParafiscales != null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "Ya existe un Caso Migrado con el ID Migracion: " + migracionParafiscales.getId().toString() + ", y el idFila: " + migracionCasoTipo.getIdFila()));
                        }
                    }
                    MigracionCasoParafiscales migracionCasoParafiscales = new MigracionCasoParafiscales();
                    migracionCasoParafiscales.setMigracionParafiscales(migracionParafiscales);
                    migracionCasoParafiscales.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    this.populateMigracionCasoParafiscales(migracionCasoTipo, migracionCasoParafiscales, errorTipoList);
                    migracionParafiscales.getCasosMigrados().add(migracionCasoParafiscales);

                }
            }
        }
    }

    /**
     * MÃ¨todo que se encarga de armar la entidad migracionCasoTipo
     *
     * @param migracionCasoTipo el objeto origen
     * @param migracionCasoParafiscales el objeto destino
     * @param errorTipoList el listado de errores de la operaciÃ³n
     * @throws AppException
     */
    private void populateMigracionCasoParafiscales(final MigracionCasoTipo migracionCasoTipo, final MigracionCasoParafiscales migracionCasoParafiscales,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(migracionCasoTipo.getIdMigracion())) {
            MigracionParafiscales migracionParafiscales = migracionParafiscalesDao.find(Long.valueOf(migracionCasoTipo.getIdMigracion()));
            if (migracionParafiscales == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontro Migracion Padre con el ID: " + migracionCasoTipo.getIdMigracion()));
            } else {
                migracionCasoParafiscales.setMigracionParafiscales(migracionParafiscales);
            }
        }
        if (StringUtils.isNotBlank(migracionCasoTipo.getCodEstadoCaso())) {
            ValorDominio valorDominio = valorDominioDao.find(migracionCasoTipo.getCodEstadoCaso());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontro codEscenarioMigrado con el ID: " + migracionCasoTipo.getCodEstadoCaso()));
            } else {
                migracionCasoParafiscales.setCodEstado(valorDominio);
            }
        }
        if (StringUtils.isNotBlank(migracionCasoTipo.getIdFila())) {
            migracionCasoParafiscales.setIdFila(migracionCasoTipo.getIdFila());
        }
        if (StringUtils.isNotBlank(migracionCasoTipo.getDescErrorCaso())) {
            migracionCasoParafiscales.setDescErrorCaso(migracionCasoTipo.getDescErrorCaso());
        }
        if (StringUtils.isNotBlank(migracionCasoTipo.getIdInstanciaProceso())) {
            migracionCasoParafiscales.setIdInstanciaProceso(migracionCasoTipo.getIdInstanciaProceso());
        }
        if (StringUtils.isNotBlank(migracionCasoTipo.getIdNumExpediente())) {
            migracionCasoParafiscales.setIdExpediente(migracionCasoTipo.getIdNumExpediente());
        }
        if (migracionCasoTipo.getIdAportante() != null
                && StringUtils.isNotBlank(migracionCasoTipo.getIdAportante().getCodTipoIdentificacion())) {
            migracionCasoParafiscales.setCodTipoDocumento(migracionCasoTipo.getIdAportante().getCodTipoIdentificacion());
        }
        if (migracionCasoTipo.getIdAportante() != null
                && StringUtils.isNotBlank(migracionCasoTipo.getIdAportante().getValNumeroIdentificacion())) {
            migracionCasoParafiscales.setValNumeroDocumento(migracionCasoTipo.getIdAportante().getValNumeroIdentificacion());
        }
    }
}
