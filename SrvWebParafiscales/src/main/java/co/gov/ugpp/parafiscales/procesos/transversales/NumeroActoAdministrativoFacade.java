package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1.NumeroActoAdministrativoTipo;
import java.io.Serializable;

/**
 *
 * @author rpadilla
 */
public interface NumeroActoAdministrativoFacade extends Serializable {

    NumeroActoAdministrativoTipo generarNumeroActoAdministrativo(final NumeroActoAdministrativoTipo numeroActoAdministrativo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
