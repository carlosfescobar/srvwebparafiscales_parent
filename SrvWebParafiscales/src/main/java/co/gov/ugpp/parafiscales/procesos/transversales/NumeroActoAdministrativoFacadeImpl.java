package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.NumeroActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.entity.NumeroActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.NumeroActoAdministrativoPk;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1.NumeroActoAdministrativoTipo;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.EntityNotFoundException;
import javax.persistence.LockModeType;
import org.apache.commons.lang3.StringUtils;

/**
 * implementaciÃ³n de la interfaz NumeroActoAdministrativoFacade que contiene
 * las operaciones del servicio SrvAplNumeroActoAdministrativo
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class NumeroActoAdministrativoFacadeImpl extends AbstractFacade implements NumeroActoAdministrativoFacade {

    @EJB
    private NumeroActoAdministrativoDao numeroActoAdministrativoDao;

    /**
     * implementaciÃ³n de la operaciÃ³n generarNumeroActoAdministrativo se
     * encarga de generar un NumeroActoAdministrativo a partir del objeto
     * numeroActoAdministrativoTipo enviado.
     *
     * @param numeroActoAdministrativoTipo Objeto a partir del cual se genera un
     * nuevo NumeroActoAdministrativo
     * @param contextoTransaccionalTipo contiene la informaciÃ³n para almacenar
     * la auditoria.
     * @return numeroActoAdministrativoTipo generado que se retorna junto con el
     * contexto transaccional
     * @throws AppException ExcepciÃ³n que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public NumeroActoAdministrativoTipo generarNumeroActoAdministrativo(
            final NumeroActoAdministrativoTipo numeroActoAdministrativoTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (numeroActoAdministrativoTipo == null || StringUtils.isBlank(numeroActoAdministrativoTipo.getCodTipoActoAdministrativo())) {
            throw new AppException("Debe proporcionar el codTipoActoAdministrativo del numeroActoAdministrativoTipo");
        }

        final List<NumeroActoAdministrativo> numActAdminList
                = numeroActoAdministrativoDao.findListByCodTipoActoAdministrativo(numeroActoAdministrativoTipo.getCodTipoActoAdministrativo());

        if (numActAdminList == null || numActAdminList.isEmpty()) {
            throw new AppException("No existe un registro en la tabla NumeroActoAdministrativo con el codTipoActoAdministrativo: "
                    + numeroActoAdministrativoTipo.getCodTipoActoAdministrativo());
        }
        final int currentAnio = DateUtil.currentCalendar().get(Calendar.YEAR);
        NumeroActoAdministrativo currentNumActAdmin = null;

        for (final NumeroActoAdministrativo naa : numActAdminList) {
            if (naa.getId().getAnio() == currentAnio) {
                currentNumActAdmin = naa;
                break;
            }
        }

        final String idNumActoAdmin;

        // Si no hay un registro con el aÃ±o actual, se toma como base el registro mas reciente
        // y se le reinicia el numero, para entonces crear el nuevo registro
        if (currentNumActAdmin == null) {
            //Como no existe registro para el año actual para el tipo de acto administrativo, se valida si
            //ya existe un consecutivo para el prefijo del tipo de acto administrativo
            //Se obtiene el ultimo registro para ese tipo de acto administrativo
            final NumeroActoAdministrativo ultimoNumeroActoAdministrativo = numActAdminList.get(Constants.FIRST_POSITION);
            final NumeroActoAdministrativoPk pk = new NumeroActoAdministrativoPk(
                    ultimoNumeroActoAdministrativo.getId().getCodTipoActoAdministrativo(), currentAnio);
            final NumeroActoAdministrativo newNumActAdmin = new NumeroActoAdministrativo(pk);
            final NumeroActoAdministrativo numeroActoConsecutivo = numeroActoAdministrativoDao.findConsecutivoByTipoAndAnio(ultimoNumeroActoAdministrativo.getPrefijo(), currentAnio);
            if (numeroActoConsecutivo != null){
                ultimoNumeroActoAdministrativo.getPrefijonumeroacto().setNumero(numeroActoConsecutivo.getPrefijonumeroacto().getNumero()+1);
            } else {
                ultimoNumeroActoAdministrativo.getPrefijonumeroacto().setNumero(1L);
            }
            newNumActAdmin.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            newNumActAdmin.setPrefijonumeroacto(ultimoNumeroActoAdministrativo.getPrefijonumeroacto());
            newNumActAdmin.setPrefijo(ultimoNumeroActoAdministrativo.getPrefijo());
            numeroActoAdministrativoDao.create(newNumActAdmin);
            idNumActoAdmin = this.formatNumActAdmin(newNumActAdmin);
        } else {
            try {
                numeroActoAdministrativoDao.getEntityManager().refresh(currentNumActAdmin, LockModeType.PESSIMISTIC_READ);
            } catch (EntityNotFoundException ex) {
                throw new AppException("El NumeroActoAdministrativo ya no existe en la base de datos", ex);
            }
            currentNumActAdmin.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
            currentNumActAdmin.getPrefijonumeroacto().setNumero(currentNumActAdmin.getPrefijonumeroacto().getNumero() == null ? 1 : currentNumActAdmin.getPrefijonumeroacto().getNumero() + 1);
            numeroActoAdministrativoDao.edit(currentNumActAdmin);
            idNumActoAdmin = this.formatNumActAdmin(currentNumActAdmin);
        }

        numeroActoAdministrativoTipo.setIdNumeroActoAdministrativo(idNumActoAdmin);

        return numeroActoAdministrativoTipo;
    }

    private String formatNumActAdmin(final NumeroActoAdministrativo numeroActoAdministrativo) {
        final StringBuilder sb = new StringBuilder();
        sb.append(numeroActoAdministrativo.getPrefijo());
        sb.append(Constants.NUM_ACT_ADMIN_SEPARATOR);
        sb.append(numeroActoAdministrativo.getId().getAnio());
        sb.append(Constants.NUM_ACT_ADMIN_SEPARATOR);
        final String numero = StringUtils.leftPad(numeroActoAdministrativo.getPrefijonumeroacto().getNumero().toString(),
                Constants.NUM_ACT_ADMIN_NUMERO_LENGHT, Constants.NUM_ACT_ADMIN_FILL);
        sb.append(numero);
        return sb.toString();
    }
}
