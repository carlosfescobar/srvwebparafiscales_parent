package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface PersonaJuridicaFacade extends Serializable {

    List<PersonaJuridicaTipo> buscarPorIdPersonaJuridica(List<IdentificacionTipo> identificacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
