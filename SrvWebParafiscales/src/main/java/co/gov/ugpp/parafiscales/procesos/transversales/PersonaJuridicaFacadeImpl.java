package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz PersonaJuridicaFacade que contiene las operaciones del servicio SrvAplPersonaJuridica
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class PersonaJuridicaFacadeImpl extends AbstractFacade implements PersonaJuridicaFacade {

    @EJB
    private PersonaDao personaDao;

    /**
     * implementación de la operación buscarPorIdPersonaJuridica se encarga de buscar un listado de personas de acuerdo
     * a un listado de identificaciones enviadas.
     *
     * @param identificacionTipoList Listado de identificaciones para realizar la consulta de un listado de personas
     * @param contextoTransaccionalTipo contiene la información para almacenar la auditoria.
     * @return listado de personas encontradas que se retorna junto con el contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se retorna a la capa de los servicios.
     */
    @Override
    public List<PersonaJuridicaTipo> buscarPorIdPersonaJuridica(final List<IdentificacionTipo> identificacionTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTipoList == null || identificacionTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID a consultar");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTipoList, IdentificacionTipo.class, Identificacion.class);

        final List<Persona> personaJuridicaList = personaDao.findByIdentificacionList(identificacionList, TipoPersonaEnum.PERSONA_JURIDICA.getCode());

        final List<PersonaJuridicaTipo> personaJuridicaTipoList
                = mapper.map(personaJuridicaList, Persona.class, PersonaJuridicaTipo.class);

        return personaJuridicaTipoList;
    }
}
