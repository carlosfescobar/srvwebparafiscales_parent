package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author jmuncab
 */
public interface PersonaNaturalFacade extends Serializable {

    List<PersonaNaturalTipo> buscarPorIdPersonaNatural(List<IdentificacionTipo> identificacionTipos, ContextoTransaccionalTipo contextoTransaccionalTipo)
            throws AppException;

    Long crearPersonaNatural(PersonaNaturalTipo personaNaturalTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
