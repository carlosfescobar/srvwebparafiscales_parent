package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 * implementación de la interfaz PersonaNaturalFacade que contiene las
 * operaciones del servicio SrvAplPersonaNatural
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class PersonaNaturalFacadeImpl extends AbstractFacade implements PersonaNaturalFacade {

    @EJB
    private PersonaDao personaDao;
    @EJB
    private Populator populator;

    /**
     * implementación de la operación buscarPorIdPersonaNatural se encarga de
     * buscar un listado de personasNaturales de acuerdo al listado de
     * identificaciones enviadas
     *
     * @param identificacionTipoList Listado de identificaciones enviadas para
     * realizar la consulta en la base de datos del listado de personas.
     * @param contextoTransaccionalTipo contiene la información para almacenar
     * la auditoria.
     * @return listado de personas encontradas que se retorna junto con el
     * contexto Transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public List<PersonaNaturalTipo> buscarPorIdPersonaNatural(final List<IdentificacionTipo> identificacionTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTipoList == null || identificacionTipoList.isEmpty()) {
            throw new AppException("Debe proporcionar una lista de identificaciones");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTipoList, IdentificacionTipo.class, Identificacion.class);

        final List<Persona> personaNaturalList = personaDao.findByIdentificacionList(identificacionList, TipoPersonaEnum.PERSONA_NATURAL.getCode());

        final List<PersonaNaturalTipo> personaNaturalTipoList
                = mapper.map(personaNaturalList, Persona.class, PersonaNaturalTipo.class);

        return personaNaturalTipoList;
    }

    @Override
    public Long crearPersonaNatural(PersonaNaturalTipo personaNaturalTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (personaNaturalTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Identificacion identificacion;

        if (personaNaturalTipo.getIdPersona() != null) {
            identificacion = mapper.map(personaNaturalTipo.getIdPersona(), Identificacion.class);
        } else {
            throw new AppException("No se pudo crear la Persona Natural: Se debe enviar un  Tipo y Numero de identificacion");
        }

        Persona persona = personaDao.findByIdentificacion(identificacion);
        if (persona != null) {
            throw new AppException("Ya existe una Persona Natural  con la identificación: " + identificacion,
                    ErrorEnum.ERROR_ENTIDAD_EXISTENTE);
        }

        Validator.checkPersonaNatural(personaNaturalTipo, errorTipoList);
        persona = populator.populatePersonaFromPersonaNatural(personaNaturalTipo, contextoTransaccionalTipo, persona,
                ClasificacionPersonaEnum.PERSONA_NATURAL, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        persona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        final Long personaPk = personaDao.create(persona);
        return personaPk;

    }
}
