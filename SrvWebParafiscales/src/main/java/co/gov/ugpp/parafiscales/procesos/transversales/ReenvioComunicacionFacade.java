package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1.ReenvioComunicacionTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author yrinconh
 */

public interface ReenvioComunicacionFacade extends Serializable {

    void crearReenvioComunicacion(ReenvioComunicacionTipo reenvioComunicacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<ReenvioComunicacionTipo> buscarPorCriteriosReenvioComunicacion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarReenvioComunicacion(ReenvioComunicacionTipo reenvioComunicacionTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
