package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.FiscalizacionHasActoAdmiDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ReenvioComunicacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ReenvioComunicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1.ReenvioComunicacionTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * @author yrinconh
 */
@Stateless
@TransactionManagement
public class ReenvioComunicacionFacadeImpl extends AbstractFacade implements ReenvioComunicacionFacade {

    @EJB
    private ReenvioComunicacionDao reenvioComunicacionDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private FiscalizacionHasActoAdmiDao fiscalizacionHasActoAdmiDao;

    @Override
    public void crearReenvioComunicacion(ReenvioComunicacionTipo reenvioComunicacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (reenvioComunicacionTipo == null || StringUtils.isBlank(reenvioComunicacionTipo.getIdRadicado())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        ReenvioComunicacion reenvioComunicacion = reenvioComunicacionDao.find(reenvioComunicacionTipo.getIdRadicado());

        if (reenvioComunicacion != null) {
            throw new AppException("Ya existe un reenvioComunicacion con el ID: " + reenvioComunicacionTipo.getIdRadicado());
        } else {
            reenvioComunicacion = new ReenvioComunicacion();
            reenvioComunicacion.setId(reenvioComunicacionTipo.getIdRadicado());
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        this.populateReenvioComunicacion(reenvioComunicacionTipo, reenvioComunicacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        reenvioComunicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        reenvioComunicacionDao.create(reenvioComunicacion);
    }

    @Override
    public PagerData<ReenvioComunicacionTipo> buscarPorCriteriosReenvioComunicacion(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ReenvioComunicacion> pagerDataEntity
                = reenvioComunicacionDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValNumPagina(), contextoTransaccionalTipo.getValTamPagina());

        final List<ReenvioComunicacionTipo> reenvioComunicacionTipoList = new ArrayList<ReenvioComunicacionTipo>();

        for (final ReenvioComunicacion reenvioComunicacion : pagerDataEntity.getData()) {
            final ReenvioComunicacionTipo reenvioComunicacionTipo = mapper.map(reenvioComunicacion, ReenvioComunicacionTipo.class);

            reenvioComunicacionTipoList.add(reenvioComunicacionTipo);
        }

        return new PagerData<ReenvioComunicacionTipo>(reenvioComunicacionTipoList, pagerDataEntity.getNumPages());

    }

    @Override
    public void actualizarReenvioComunicacion(final ReenvioComunicacionTipo reenvioComunicacionTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (reenvioComunicacionTipo == null || StringUtils.isBlank(reenvioComunicacionTipo.getIdRadicado())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ReenvioComunicacion reenvioComunicacion = reenvioComunicacionDao.find(reenvioComunicacionTipo.getIdRadicado());

        if (reenvioComunicacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un ReenvioComunicacion con el valor:" + reenvioComunicacionTipo.getIdRadicado()));
            throw new AppException(errorTipoList);
        }

        reenvioComunicacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());

        this.populateReenvioComunicacion(reenvioComunicacionTipo, reenvioComunicacion, contextoTransaccionalTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        reenvioComunicacionDao.edit(reenvioComunicacion);
    }

    private void populateReenvioComunicacion(final ReenvioComunicacionTipo reenvioComunicacionTipo,
            final ReenvioComunicacion reenvioComunicacion, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getIdRadicado())) {
            reenvioComunicacion.setIdRadicado(reenvioComunicacionTipo.getIdRadicado());
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getValDireccionEnviada())) {
            reenvioComunicacion.setValDireccionEnviada(reenvioComunicacionTipo.getValDireccionEnviada());
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getCodProcesoEnviaComunicacion())) {
            ValorDominio codProcesoEnviaComunicacion = valorDominioDao.find(reenvioComunicacionTipo.getCodProcesoEnviaComunicacion());
            if (codProcesoEnviaComunicacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codProcesoEnviaComunicacion proporcionado no existe con el ID: " + reenvioComunicacionTipo.getCodProcesoEnviaComunicacion()));
            } else {
                reenvioComunicacion.setCodProcesoEnviaComunicacion(codProcesoEnviaComunicacion);
            }
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getCodTipoDireccionEnviada())) {
            ValorDominio codTipoDireccionEnviada = valorDominioDao.find(reenvioComunicacionTipo.getCodTipoDireccionEnviada());
            if (codTipoDireccionEnviada == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoDireccionEnviada proporcionado no existe con el ID: " + reenvioComunicacionTipo.getCodTipoDireccionEnviada()));
            } else {
                reenvioComunicacion.setCodTipoDireccionEnviada(codTipoDireccionEnviada);
            }
        }

        if (reenvioComunicacionTipo.getExpediente() != null
                && StringUtils.isNotBlank(reenvioComunicacionTipo.getExpediente().getIdNumExpediente())) {
            final Expediente expediente = expedienteDao.find(reenvioComunicacionTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un expediente con el id: " + reenvioComunicacionTipo.getExpediente().getIdNumExpediente()));
            } else {
                reenvioComunicacion.setExpediente(expediente);
            }
        }

        if (reenvioComunicacionTipo.getIdPersonaDestinatario() != null) {
            if (StringUtils.isNotBlank(reenvioComunicacionTipo.getIdPersonaDestinatario().getCodTipoIdentificacion())
                    && (StringUtils.isNotBlank(reenvioComunicacionTipo.getIdPersonaDestinatario().getValNumeroIdentificacion()))) {
                Identificacion identificacionDestinatario = new Identificacion(reenvioComunicacionTipo.getIdPersonaDestinatario().getCodTipoIdentificacion(),
                        reenvioComunicacionTipo.getIdPersonaDestinatario().getValNumeroIdentificacion());
                final Persona persona = personaDao.findByIdentificacion(identificacionDestinatario);
                if (persona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No existe un destinatario con  la identificación: " + identificacionDestinatario));
                } else {
                    reenvioComunicacion.setIdPersonaDestinatario(persona);
                }
            }
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getDescObservaciones())) {
            reenvioComunicacion.setDescObservaciones(reenvioComunicacionTipo.getDescObservaciones());
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getCodFuenteInformacion())) {
            ValorDominio codFuenteInformacion = valorDominioDao.find(reenvioComunicacionTipo.getCodFuenteInformacion());
            if (codFuenteInformacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codProcesoEnviaComunicacion proporcionado no existe con el ID: " + reenvioComunicacionTipo.getCodFuenteInformacion()));
            } else {
                reenvioComunicacion.setCodFuenteInformacion(codFuenteInformacion);
            }
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getEsDevolucion())) {
            reenvioComunicacion.setEsDevolucion(Boolean.valueOf(reenvioComunicacionTipo.getEsDevolucion()));
        }

        if (StringUtils.isNotBlank(reenvioComunicacionTipo.getEsReenvio())) {
            reenvioComunicacion.setEsReenvio(Boolean.valueOf(reenvioComunicacionTipo.getEsReenvio()));
        }
    }
}
