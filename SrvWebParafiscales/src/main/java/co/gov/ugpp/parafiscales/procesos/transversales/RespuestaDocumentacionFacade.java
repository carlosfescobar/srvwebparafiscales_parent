package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author rpadilla
 */
public interface RespuestaDocumentacionFacade extends Serializable {

    Long registrarDocumentacionEsperada(DocumentacionEsperadaTipo documentacionEsperadaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    void actualizarDocumentacionEsperada(DocumentacionEsperadaTipo documentacionEsperadaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    PagerData<DocumentacionEsperadaTipo> buscarPorCriteriosDocumentacionEsperada (List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
