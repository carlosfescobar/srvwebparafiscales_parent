package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentacionEsperadaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentacionEsperada;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentacionEsperadaDocumentos;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz RespuestaDocumentacionFacade que contiene las
 * operaciones del servicio SrvAplRespuestaDocumentacion
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class RespuestaDocumentacionFacadeImpl extends AbstractFacade implements RespuestaDocumentacionFacade {

    @EJB
    private DocumentacionEsperadaDao documentacionEsperadaDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private Populator populator;

    /**
     * Implementación de la operación registrarDocumentacionEsperada se encarga
     * de registrar una nueva documentacionEsperada en la base de datos de
     * acuerdo al objeto documentacionEsperadaTipo enviado
     *
     * @param documentacionEsperadaTipo Objeto a partir del cual se va a
     * registrar una nueva documentacionEsperada en la base de datos.
     * @param ctt contiene la información para almacenar la auditoria.
     * @return id de la documentacionEsperada creada que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public Long registrarDocumentacionEsperada(final DocumentacionEsperadaTipo documentacionEsperadaTipo,
            final ContextoTransaccionalTipo ctt) throws AppException {

        if (documentacionEsperadaTipo == null) {
            throw new AppException("Debe proporcionar la documentacionEsperadaTipo");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkDocumentacionEsperada(documentacionEsperadaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final DocumentacionEsperada documentacionEsperada = new DocumentacionEsperada();

        documentacionEsperada.setIdUsuarioCreacion(ctt.getIdUsuario());

        this.populate(documentacionEsperadaTipo, documentacionEsperada, ctt, errorTipoList);

        populator.populateDocumentacionEsperadaOnCreate(documentacionEsperada, documentacionEsperadaTipo, ctt, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        final Long id = documentacionEsperadaDao.create(documentacionEsperada);

        return id;
    }

    /**
     * Implementación de la operación actualizarDocumentacionEsperada se encarga
     * de actualizar la DocumentacionEsperada de acuerdo al id enviado
     *
     * @param documentacionEsperadaTipo Objeto a partir del cual se va a
     * actualizar la documentacionEsperada en la base de datos
     * @param ctt contiene la información para almacenar la auditoria.
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public void actualizarDocumentacionEsperada(final DocumentacionEsperadaTipo documentacionEsperadaTipo,
            final ContextoTransaccionalTipo ctt) throws AppException {

        if (StringUtils.isBlank(documentacionEsperadaTipo.getIdInformacionEsperada())) {
            throw new AppException("Debe proporcionar el idInformacionEsperada de la documentacionEsperadaTipo");
        }

        final DocumentacionEsperada documentacionEsperada
                = documentacionEsperadaDao.find(Long.valueOf(documentacionEsperadaTipo.getIdInformacionEsperada()));

        if (documentacionEsperada == null) {
            throw new AppException("No existe la documentacionEsperada con ID: "
                    + documentacionEsperadaTipo.getIdInformacionEsperada());
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        documentacionEsperada.setIdUsuarioModificacion(ctt.getIdUsuario());

        this.populate(documentacionEsperadaTipo, documentacionEsperada, ctt, errorTipoList);

        populator.populateDocumentacionEsperadaOnUpdate(documentacionEsperada, documentacionEsperadaTipo, ctt, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        documentacionEsperadaDao.edit(documentacionEsperada);
    }

    /**
     * implementación de la operación buscarPorCriteriosDocumentacionEsperada,
     * esta se encarga de buscar un listado de DocumentacionEsperada por medio
     * de los parametros enviado y ordenar la consulta de acuerdo a unos
     * criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar la documentación
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de solicitudes encontradas que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<DocumentacionEsperadaTipo> buscarPorCriteriosDocumentacionEsperada(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<DocumentacionEsperada> documentacionEsperadaPagerData = documentacionEsperadaDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<DocumentacionEsperadaTipo> documentacionEsperadaTipoList
                = mapper.map(documentacionEsperadaPagerData.getData(), DocumentacionEsperada.class, DocumentacionEsperadaTipo.class);

        if (documentacionEsperadaTipoList != null
                && !documentacionEsperadaTipoList.isEmpty()) {
            for (final DocumentacionEsperadaTipo documentacionEsperadaTipo : documentacionEsperadaTipoList) {
                if (documentacionEsperadaTipo.getFuncionario() != null
                        && StringUtils.isNotBlank(documentacionEsperadaTipo.getFuncionario().getIdFuncionario())) {
                    final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(documentacionEsperadaTipo.getFuncionario(), contextoTransaccionalTipo);
                    documentacionEsperadaTipo.setFuncionario(funcionarioTipo);
                }
            }
        }

        return new PagerData(documentacionEsperadaTipoList, documentacionEsperadaPagerData.getNumPages());
    }

    /**
     * Método utilizado para Crear/ Actualizar la entidad DocumentacionEsperada
     *
     * @param documentacionEsperadaTipo Objeto origen
     * @param documentacionEsperada Objeto destino
     * @param ctt contiene la información para almacenar la auditoria.
     * @param errorTipoList Listado que almacena errores de negocio
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    private void populate(final DocumentacionEsperadaTipo documentacionEsperadaTipo,
            final DocumentacionEsperada documentacionEsperada,
            final ContextoTransaccionalTipo ctt,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (documentacionEsperadaTipo.getIdDocumentos() != null && !documentacionEsperadaTipo.getIdDocumentos().isEmpty()) {
            for (final String idDocumento : documentacionEsperadaTipo.getIdDocumentos()) {
                if (StringUtils.isBlank(idDocumento)) {
                    throw new AppException("Ningun ID de los documentos asociados puede ser vacio");
                }
                DocumentacionEsperadaDocumentos documentacionEsperadaDocumentos = new DocumentacionEsperadaDocumentos();
                documentacionEsperadaDocumentos.setDocumentacionEsperada(documentacionEsperada);
                documentacionEsperadaDocumentos.setIdDocumento(idDocumento);
                documentacionEsperadaDocumentos.setIdUsuarioCreacion(ctt.getIdUsuario());
                documentacionEsperada.getIdDocumentos().add(documentacionEsperadaDocumentos);
            }
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getCodEstado())) {
            final ValorDominio valorDominio = valorDominioDao.find(documentacionEsperadaTipo.getCodEstado());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un codEstado con el codigo: " + documentacionEsperadaTipo.getCodEstado()));
            } else {
                documentacionEsperada.setCodEstado(valorDominio);
            }
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getIdExpediente())) {
            final Expediente expediente = expedienteDao.find(documentacionEsperadaTipo.getIdExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No existe un expediente con el id: " + documentacionEsperadaTipo.getIdExpediente()));
            } else {
                documentacionEsperada.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getIdInstanciaProcesoReanudacion())) {
            documentacionEsperada.setIdInstanciaProcesoReanudacion(documentacionEsperadaTipo.getIdInstanciaProcesoReanudacion());
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getIdRadicadoEntradaCorrespondencia())) {
            documentacionEsperada.setIdRadicadoEntradaCorrespondencia(documentacionEsperadaTipo.getIdRadicadoEntradaCorrespondencia());
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getIdRadicadoSalidaCorrespondencia())) {
            documentacionEsperada.setIdRadicadoSalidaCorrespond(documentacionEsperadaTipo.getIdRadicadoSalidaCorrespondencia());
        }

        if (documentacionEsperadaTipo.getFuncionario() != null
                && StringUtils.isNotBlank(documentacionEsperadaTipo.getFuncionario().getIdFuncionario())) {
            documentacionEsperada.setFuncionario(documentacionEsperadaTipo.getFuncionario().getIdFuncionario());
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getValNombreModeloProceso())) {
            documentacionEsperada.setValNombreModeloProceso(documentacionEsperadaTipo.getValNombreModeloProceso());
        }

        if (StringUtils.isNotBlank(documentacionEsperadaTipo.getValDescripcion())) {
            documentacionEsperada.setValDescripcion(documentacionEsperadaTipo.getValDescripcion());
        }
    }
}
