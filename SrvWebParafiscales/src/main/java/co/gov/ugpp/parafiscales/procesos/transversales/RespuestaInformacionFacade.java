package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import java.io.Serializable;

/**
 *
 * @author rpadilla
 */
public interface RespuestaInformacionFacade extends Serializable {

    Long registrarDocumentacionEsperada(DocumentacionEsperadaTipo documentacionEsperadaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    void actualizarDocumentacionEsperada(DocumentacionEsperadaTipo documentacionEsperadaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
