package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.agrupacionactoadministrativoanexotipo.v1.AgrupacionActoAdministrativoAnexoTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.MsjOpActualizarAnexoActoAdministrativoFallo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.MsjOpBuscarPorCriteriosAnexoActoAdministrativoFallo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.MsjOpCrearAnexoActoAdministrativoFallo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpActualizarAnexoActoAdministrativoRespTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpActualizarAnexoActoAdministrativoSolTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpCrearAnexoActoAdministrativoRespTipo;
import co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.OpCrearAnexoActoAdministrativoSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplAnexosActoAdministrativo",
        portName = "portSrvAplAnexosActoAdministrativoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1.PortSrvAplAnexosActoAdministrativoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAnexosActoAdministrativo extends AbstractSrvApl {

    @EJB
    private AnexoActoAdministrativoFacade actoAdministrativoFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAnexosActoAdministrativo.class);

    public OpCrearAnexoActoAdministrativoRespTipo opCrearAnexoActoAdministrativo(OpCrearAnexoActoAdministrativoSolTipo msjOpCrearAnexoActoAdministrativoSol) throws MsjOpCrearAnexoActoAdministrativoFallo {
        LOG.info("OPERACION: opCrearAnexoActoAdministrativo ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAnexoActoAdministrativoSol.getContextoTransaccional();
        final AgrupacionActoAdministrativoAnexoTipo anexosActoAdministrativo = msjOpCrearAnexoActoAdministrativoSol.getActoAdministrativoAnexos();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            actoAdministrativoFacade.crearAnexoActoAdministrativo(contextoTransaccionalTipo, anexosActoAdministrativo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearAnexoActoAdministrativoRespTipo resp = new OpCrearAnexoActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opCrearAnexoActoAdministrativo ::: FIN");
        return resp;
    }

    public OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo opBuscarPorCriteriosAnexoActoAdministrativo(OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo msjOpBuscarPorCriteriosAnexoActoAdministrativoSol) throws MsjOpBuscarPorCriteriosAnexoActoAdministrativoFallo {
        LOG.info("OPERACION: opBuscarPorCriteriosAnexoActoAdministrativo ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAnexoActoAdministrativoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosAnexoActoAdministrativoSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AgrupacionActoAdministrativoAnexoTipo> agrupacionAnexoActoAdministrativoPagerData;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            agrupacionAnexoActoAdministrativoPagerData = actoAdministrativoFacade.buscarPorCriteriosAnexoActoAdministrativo(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo resp = new OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo();
        cr.setValCantidadPaginas(agrupacionAnexoActoAdministrativoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getActoAdministrativoAnexos().addAll(agrupacionAnexoActoAdministrativoPagerData.getData());
        LOG.info("OPERACION: opBuscarPorCriteriosAnexoActoAdministrativo ::: FIN");
        return resp;
    }

    public OpActualizarAnexoActoAdministrativoRespTipo opActualizarAnexoActoAdministrativo(OpActualizarAnexoActoAdministrativoSolTipo msjOpActualizarAnexoActoAdministrativoSol) throws MsjOpActualizarAnexoActoAdministrativoFallo {
        LOG.info("OPERACION: opActualizarAnexoActoAdministrativo ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAnexoActoAdministrativoSol.getContextoTransaccional();
        final AgrupacionActoAdministrativoAnexoTipo anexosActoAdministrativo = msjOpActualizarAnexoActoAdministrativoSol.getActoAdministrativoAnexos();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            actoAdministrativoFacade.actualizarAnexoActoAdministrativo(contextoTransaccionalTipo, anexosActoAdministrativo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAnexoActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarAnexoActoAdministrativoRespTipo resp = new OpActualizarAnexoActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarAnexoActoAdministrativo ::: FIN");
        return resp;
    }
}
