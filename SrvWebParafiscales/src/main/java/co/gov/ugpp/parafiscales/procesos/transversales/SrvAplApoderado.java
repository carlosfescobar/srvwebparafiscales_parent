/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.apoderadotipo.v1.ApoderadoTipo;
import co.gov.ugpp.transversales.srvaplapoderado.v1.MsjOpBuscarPorIdApoderadoFallo;
import co.gov.ugpp.transversales.srvaplapoderado.v1.OpBuscarPorIdApoderadoRespTipo;
import co.gov.ugpp.transversales.srvaplapoderado.v1.OpBuscarPorIdApoderadoSolTipo;
import javax.ejb.EJB;
import javax.jws.WebService;
import static org.apache.poi.ss.formula.functions.NumericFunction.LOG;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@WebService(serviceName = "SrvAplApoderado", 
        portName = "portSrvAplApoderadoSOAP", 
        endpointInterface = "co.gov.ugpp.transversales.srvaplapoderado.v1.PortSrvAplApoderadoSOAP", 
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1")
public class SrvAplApoderado extends AbstractSrvApl{
    
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplDocumentoCORE.class);
    
    @EJB
    private ApoderadoFacade apoderadoFacade;

    public OpBuscarPorIdApoderadoRespTipo opBuscarPorIdApoderado(OpBuscarPorIdApoderadoSolTipo msjOpBuscarPorIdApoderadoSol) throws MsjOpBuscarPorIdApoderadoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdApoderadoSol.getContextoTransaccional();
        final IdentificacionTipo identificacion = msjOpBuscarPorIdApoderadoSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final ApoderadoTipo apoderadoTipo;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            apoderadoTipo = apoderadoFacade.findApoderadoByIdentificacion(identificacion, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdApoderadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdApoderadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdApoderadoRespTipo resp = new OpBuscarPorIdApoderadoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setApoderado(apoderadoTipo);
        
        return resp;
        
    }
    
}
