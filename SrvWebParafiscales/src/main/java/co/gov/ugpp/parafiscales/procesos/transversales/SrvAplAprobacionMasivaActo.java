package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.MsjOpActualizarAprobacionMasivaActoFallo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.MsjOpBuscarPorCriteriosAprobacionMasivaActoFallo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.MsjOpCrearAprobacionMasivaActoFallo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpActualizarAprobacionMasivaActoRespTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpActualizarAprobacionMasivaActoSolTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpBuscarPorCriteriosAprobacionMasivaActoRespTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpBuscarPorCriteriosAprobacionMasivaActoSolTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpCrearAprobacionMasivaActoRespTipo;
import co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.OpCrearAprobacionMasivaActoSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author acastanv
 */
@WebService(serviceName = "SrvAplAprobacionMasivaActo",
        portName = "portSrvAplAprobacionMasivaActoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1.PortSrvAplAprobacionMasivaActoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAprobacionMasivaActo extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAprobacionMasivaActo.class);

    @EJB
    private AprobacionMasivaActoFacade aprobacionMasivaActoFacade;

    public OpCrearAprobacionMasivaActoRespTipo opCrearAprobacionMasivaActo(OpCrearAprobacionMasivaActoSolTipo msjOpCrearAprobacionMasivaActoSol) throws MsjOpCrearAprobacionMasivaActoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearAprobacionMasivaActoSol.getContextoTransaccional();
        final AprobacionMasivaActoTipo aprobacionMasivaActoTipo = msjOpCrearAprobacionMasivaActoSol.getAprobacionMasivaActo();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final Long aprobacionMasivaActoPK;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            aprobacionMasivaActoPK = aprobacionMasivaActoFacade.crearAprobacionMasivaActo(aprobacionMasivaActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearAprobacionMasivaActoRespTipo resp = new OpCrearAprobacionMasivaActoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdAprobacionMasivaActo(aprobacionMasivaActoPK.toString());
        return resp;
    }

    public OpActualizarAprobacionMasivaActoRespTipo opActualizarAprobacionMasivaActo(OpActualizarAprobacionMasivaActoSolTipo msjOpActualizarAprobacionMasivaActoSol) throws MsjOpActualizarAprobacionMasivaActoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAprobacionMasivaActoSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final AprobacionMasivaActoTipo aprobacionMasivaActo = msjOpActualizarAprobacionMasivaActoSol.getAprobacionMasivaActo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            aprobacionMasivaActoFacade.actualizarAprobacionMasivaActo(aprobacionMasivaActo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarAprobacionMasivaActoRespTipo resp = new OpActualizarAprobacionMasivaActoRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;
    }

    public OpBuscarPorCriteriosAprobacionMasivaActoRespTipo opBuscarPorCriteriosAprobacionMasivaActo(OpBuscarPorCriteriosAprobacionMasivaActoSolTipo msjOpBuscarPorCriteriosAprobacionMasivaActoSol) throws MsjOpBuscarPorCriteriosAprobacionMasivaActoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAprobacionMasivaActoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosAprobacionMasivaActoSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AprobacionMasivaActoTipo> aprobacionMasivaActoTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            aprobacionMasivaActoTipoPagerData = aprobacionMasivaActoFacade.buscarPorCriteriosAprobacionMasivaActo(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAprobacionMasivaActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosAprobacionMasivaActoRespTipo resp = new OpBuscarPorCriteriosAprobacionMasivaActoRespTipo();
        cr.setValCantidadPaginas(aprobacionMasivaActoTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getAprobacionMasivaActo().addAll(aprobacionMasivaActoTipoPagerData.getData());
        return resp;
    }
}
