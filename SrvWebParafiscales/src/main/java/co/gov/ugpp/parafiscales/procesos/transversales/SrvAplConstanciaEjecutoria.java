package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.MsjOpActualizarConstanciaEjecutoriaFallo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.MsjOpBuscarPorIdConstanciaEjecutoriaFallo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.MsjOpCrearConstanciaEjecutoriaFallo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpActualizarConstanciaEjecutoriaRespTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpActualizarConstanciaEjecutoriaSolTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpBuscarPorIdConstanciaEjecutoriaRespTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpBuscarPorIdConstanciaEjecutoriaSolTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpCrearConstanciaEjecutoriaRespTipo;
import co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.OpCrearConstanciaEjecutoriaSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplConstanciaEjecutoria",
        portName = "portSrvAplConstanciaEjecutoriaSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1.PortSrvAplConstanciaEjecutoriaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplConstanciaEjecutoria extends AbstractSrvApl {

    @EJB
    private ConstanciaEjecutoriaFacade constanciaEjecutoriaFacade;

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplConstanciaEjecutoria.class);

    public OpCrearConstanciaEjecutoriaRespTipo opCrearConstanciaEjecutoria(OpCrearConstanciaEjecutoriaSolTipo msjOpCrearConstanciaEjecutoriaSol) throws MsjOpCrearConstanciaEjecutoriaFallo {

        LOG.info("OPERACION: opCrearConstanciaEjecutoria ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearConstanciaEjecutoriaSol.getContextoTransaccional();
        final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo = msjOpCrearConstanciaEjecutoriaSol.getConstanciaEjecutoria();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idConstanciaEjecutoria;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            idConstanciaEjecutoria = constanciaEjecutoriaFacade.crearConstanciaEjecutoria(contextoTransaccionalTipo, constanciaEjecutoriaTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearConstanciaEjecutoriaRespTipo resp = new OpCrearConstanciaEjecutoriaRespTipo();
        constanciaEjecutoriaTipo.setIdConstanciaEjecutoria(idConstanciaEjecutoria.toString());
        resp.setContextoRespuesta(cr);
        resp.setConstanciaEjecutoria(constanciaEjecutoriaTipo);

        LOG.info("OPERACION: opCrearConstanciaEjecutoria ::: FIN");

        return resp;
    }

    public OpBuscarPorIdConstanciaEjecutoriaRespTipo opBuscarPorIdConstanciaEjecutoria(OpBuscarPorIdConstanciaEjecutoriaSolTipo msjOpBuscarPorIdConstanciaEjecutoriaSol) throws MsjOpBuscarPorIdConstanciaEjecutoriaFallo {

        LOG.info("OPERACION: opBuscarPorIdConstanciaEjecutoria ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdConstanciaEjecutoriaSol.getContextoTransaccional();
        final List<String> idConstanciaEjecutoria = msjOpBuscarPorIdConstanciaEjecutoriaSol.getIdConstanciaEjecutoria();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<ConstanciaEjecutoriaTipo> constanciaEjecutoriaTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            constanciaEjecutoriaTipos = constanciaEjecutoriaFacade.buscarPorIdConstanciaEjecutoria(contextoTransaccionalTipo, idConstanciaEjecutoria);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdConstanciaEjecutoriaRespTipo resp = new OpBuscarPorIdConstanciaEjecutoriaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getConstanciaEjecutoriaTipo().addAll(constanciaEjecutoriaTipos);

        LOG.info("OPERACION: opBuscarPorIdConstanciaEjecutoria ::: FIN");

        return resp;

    }

    public OpActualizarConstanciaEjecutoriaRespTipo opActualizarConstanciaEjecutoria(OpActualizarConstanciaEjecutoriaSolTipo msjOpActualizaConstanciaEjecutoriaSolTipo) throws MsjOpActualizarConstanciaEjecutoriaFallo {

        LOG.info("OPERACION: opActualizarConstanciaEjecutoria ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizaConstanciaEjecutoriaSolTipo.getContextoTransaccional();
        final ConstanciaEjecutoriaTipo constanciaEjecutoriaTipo = msjOpActualizaConstanciaEjecutoriaSolTipo.getConstanciaEjecutoria();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            constanciaEjecutoriaFacade.actualizarConstanciaEjecutoria(contextoTransaccionalTipo, constanciaEjecutoriaTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarConstanciaEjecutoriaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarConstanciaEjecutoriaRespTipo resp = new OpActualizarConstanciaEjecutoriaRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opActualizarConstanciaEjecutoria ::: FIN");

        return resp;
    }
}
