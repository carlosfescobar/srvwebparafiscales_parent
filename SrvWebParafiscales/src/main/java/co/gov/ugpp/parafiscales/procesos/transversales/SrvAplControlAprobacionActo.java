/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.MsjOpActualizarControlAprobacionActoFallo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.MsjOpBuscarPorCriteriosControlAprobacionActoFallo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.MsjOpCrearControlAprobacionActoFallo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpActualizarControlAprobacionActoRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpActualizarControlAprobacionActoSolTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpBuscarPorCriteriosControlAprobacionActoRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpBuscarPorCriteriosControlAprobacionActoSolTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpCrearControlAprobacionActoRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpCrearControlAprobacionActoSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yrinconh
 */
@WebService(serviceName = "SrvAplControlAprobacionActo",
        portName = "portSrvAplControlAprobacionActoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.PortSrvAplControlAprobacionActoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplControlAprobacionActo extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplControlAprobacionActo.class);
    
    @EJB
    private ControlAprobacionActoFacade controlAprobacionActoFacade;

    public OpCrearControlAprobacionActoRespTipo opCrearControlAprobacionActo(
            OpCrearControlAprobacionActoSolTipo msjOpCrearControlAprobacionActoSol) throws MsjOpCrearControlAprobacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearControlAprobacionActoSol.getContextoTransaccional();
        final ControlAprobacionActoTipo trazabilidadRadicacionActoTipo = msjOpCrearControlAprobacionActoSol.getControlAprobacionActo();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final Long controlAprobacionActoPK;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlAprobacionActoPK = controlAprobacionActoFacade.crearControlAprobacionActo(trazabilidadRadicacionActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearControlAprobacionActoRespTipo resp = new OpCrearControlAprobacionActoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdControlAprobacionActo(controlAprobacionActoPK.toString());
        return resp;
    }

    public OpActualizarControlAprobacionActoRespTipo opActualizarControlAprobacionActo(
            OpActualizarControlAprobacionActoSolTipo msjOpActualizarControlAprobacionActoSol) throws MsjOpActualizarControlAprobacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarControlAprobacionActoSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final ControlAprobacionActoTipo controlAprobacionActoTipo = msjOpActualizarControlAprobacionActoSol.getControlAprobacionActo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlAprobacionActoFacade.actualizarControlAprobacionActo(controlAprobacionActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarControlAprobacionActoRespTipo resp = new OpActualizarControlAprobacionActoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosControlAprobacionActoRespTipo opBuscarPorCriteriosControlAprobacionActo(
            OpBuscarPorCriteriosControlAprobacionActoSolTipo msjOpBuscarPorCriteriosControlAprobacionActoSol) throws MsjOpBuscarPorCriteriosControlAprobacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosControlAprobacionActoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosControlAprobacionActoSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<ControlAprobacionActoTipo> controlAprobacionActoTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlAprobacionActoTipoPagerData = controlAprobacionActoFacade.buscarPorCriteriosControlAprobacionActo(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlAprobacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosControlAprobacionActoRespTipo resp = new OpBuscarPorCriteriosControlAprobacionActoRespTipo();
        cr.setValCantidadPaginas(controlAprobacionActoTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getControlAprobacionActo().addAll(controlAprobacionActoTipoPagerData.getData());

        return resp;
    }
}
