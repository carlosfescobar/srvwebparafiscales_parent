package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorCriteriosActoAdministrativoSolTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.controlliquidadortipo.v1.ControlLiquidadorTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.MsjOpActualizarControlLiquidadorFallo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.MsjOpBuscarPorCriteriosControlLiquidadorFallo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.MsjOpBuscarPorIdControlLiquidadorFallo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.MsjOpCrearControlLiquidadorFallo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpActualizarControlLiquidadorRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpActualizarControlLiquidadorSolTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpBuscarPorCriteriosControlLiquidadorRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpBuscarPorCriteriosControlLiquidadorSolTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpBuscarPorIdControlLiquidadorRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpBuscarPorIdControlLiquidadorSolTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpCrearControlLiquidadorRespTipo;
import co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.OpCrearControlLiquidadorSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author cpatingo
 */
@WebService(serviceName = "SrvAplControlLiquidador",
        portName = "portSrvAplControlLiquidadorSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplcontrolliquidador.v1.PortSrvAplControlLiquidadorSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplControlLiquidador extends AbstractSrvApl {

    @EJB
    private ControlLiquidadorFacade controlLiquidadorFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplControlLiquidador.class);

    public OpCrearControlLiquidadorRespTipo opCrearControlLiquidador(OpCrearControlLiquidadorSolTipo msjOpCrearControlLiquidadorSol) throws MsjOpCrearControlLiquidadorFallo {

        LOG.info("OPERACION: opCrearControlLiquidador ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearControlLiquidadorSol.getContextoTransaccional();
        final ControlLiquidadorTipo controlLiquidadorTipo = msjOpCrearControlLiquidadorSol.getControlLiquidador();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final Long controlLiquidadorPK;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            controlLiquidadorPK = controlLiquidadorFacade.crearControlLiquidador(contextoTransaccionalTipo, controlLiquidadorTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearControlLiquidadorRespTipo resp = new OpCrearControlLiquidadorRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdControlLiquidador(controlLiquidadorPK.toString());

        LOG.info("OPERACION: opCrearControlLiquidador ::: FIN");

        return resp;
    }

    public OpActualizarControlLiquidadorRespTipo opActualizarControlLiquidador(OpActualizarControlLiquidadorSolTipo msjOpActualizarControlLiquidadorSol) throws MsjOpActualizarControlLiquidadorFallo {

        LOG.info("OPERACION: opActualizarControlLiquidador ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarControlLiquidadorSol.getContextoTransaccional();
        final ControlLiquidadorTipo controlLiquidadorTipo = msjOpActualizarControlLiquidadorSol.getControlLiquidador();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            controlLiquidadorFacade.actualizarControlLiquidador(contextoTransaccionalTipo, controlLiquidadorTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarControlLiquidadorRespTipo resp = new OpActualizarControlLiquidadorRespTipo();
        resp.setContextoRespuesta(cr);

        LOG.info("OPERACION: opActualizarControlLiquidador ::: FIN");

        return resp;
    }

    public OpBuscarPorIdControlLiquidadorRespTipo opBuscarPorIdControlLiquidador(OpBuscarPorIdControlLiquidadorSolTipo msjOpBuscarPorIdControlLiquidadorSol) throws MsjOpBuscarPorIdControlLiquidadorFallo {

        LOG.info("OPERACION: opBuscarPorIdControlLiquidador ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdControlLiquidadorSol.getContextoTransaccional();
        final List<String> idControlLiquidador = msjOpBuscarPorIdControlLiquidadorSol.getIdControlLiquidador();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<ControlLiquidadorTipo> controlLiquidadorTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlLiquidadorTipoList = controlLiquidadorFacade.buscarPorIdControlLiquidador(contextoTransaccionalTipo, idControlLiquidador);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdControlLiquidadorFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdControlLiquidadorRespTipo resp = new OpBuscarPorIdControlLiquidadorRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getControlLiquidador().addAll(controlLiquidadorTipoList);

        LOG.info("OPERACION: opBuscarPorIdControlLiquidador ::: FIN");

        return resp;
    }

    public OpBuscarPorCriteriosControlLiquidadorRespTipo opBuscarPorCriteriosControlLiquidador(OpBuscarPorCriteriosControlLiquidadorSolTipo msjOpBuscarPorCriteriosControlLiquidadorSol) throws MsjOpBuscarPorCriteriosControlLiquidadorFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosControlLiquidadorSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosControlLiquidadorSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<ControlLiquidadorTipo> controlLiquidadorTipoPagerData;
        
        try{
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            controlLiquidadorTipoPagerData= controlLiquidadorFacade.buscarPorCriteriosControlLiquidador(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        }catch(AppException ex){
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlLiquidadorFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }catch(RuntimeException ex){
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosControlLiquidadorFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        
        final OpBuscarPorCriteriosControlLiquidadorRespTipo resp = new OpBuscarPorCriteriosControlLiquidadorRespTipo();
        cr.setValCantidadPaginas(controlLiquidadorTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getControlLiquidadorTipo().addAll(controlLiquidadorTipoPagerData.getData());
        return resp;        
    }

}

