package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.MsjOpActualizarHallazgosConciliacionContableNominaFallo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.MsjOpCruzarConciliacionContableFallo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.OpActualizarHallazgosConciliacionContableNominaRespTipo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.OpActualizarHallazgosConciliacionContableNominaSolTipo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.OpCruzarConciliacionContableRespTipo;
import co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.OpCruzarConciliacionContableSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author everis
 */
@WebService(serviceName = "SrvAplCruceConciliacionContableNomina",
        portName = "portSrvAplCruceConciliacionContableNominaSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1.PortSrvAplCruceConciliacionContableNominaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplCruceConciliacionContableNomina extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplCruceConciliacionContableNomina.class);

    @EJB
    private CruceConciliacionContableNominaFacade cruceConciliacionContableNominaFacade;

    public OpCruzarConciliacionContableRespTipo opCruzarConciliacionContable(OpCruzarConciliacionContableSolTipo msjOpCruzarConciliacionContableSol) throws MsjOpCruzarConciliacionContableFallo {
        
        LOG.info("OPERACION: opCruzarConciliacionContable ::: INICIO");
        
        final IdentificacionTipo identificacionTipo = msjOpCruzarConciliacionContableSol.getIdentificacion();
        final List<ParametroTipo> parametroTipoList = msjOpCruzarConciliacionContableSol.getParametro();
        final ExpedienteTipo expedienteTipo = msjOpCruzarConciliacionContableSol.getExpediente();
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCruzarConciliacionContableSol.getContextoTransaccional();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            cruceConciliacionContableNominaFacade.cruzarConciliacionContable(identificacionTipo, expedienteTipo, parametroTipoList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarConciliacionContableFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarConciliacionContableFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        LOG.info("OPERACION: opCruzarConciliacionContable ::: FIN");
        
        final OpCruzarConciliacionContableRespTipo resp = new OpCruzarConciliacionContableRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;
    }

    public OpActualizarHallazgosConciliacionContableNominaRespTipo opActualizarHallazgosConciliacionContableNomina(OpActualizarHallazgosConciliacionContableNominaSolTipo msjOpActualizarHallazgosConciliacionContableNominaSol) throws MsjOpActualizarHallazgosConciliacionContableNominaFallo {
        
        LOG.info("OPERACION: opActualizarHallazgosConciliacionContableNomina ::: INICIO");
        
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarHallazgosConciliacionContableNominaSol.getContextoTransaccional();
        final HallazgoConciliacionContableTipo hallazgoConciliacionContableTipo = msjOpActualizarHallazgosConciliacionContableNominaSol.getHallazgo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            cruceConciliacionContableNominaFacade.actualizarHallazgosConciliacionContable(contextoTransaccionalTipo, hallazgoConciliacionContableTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarHallazgosConciliacionContableNominaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarHallazgosConciliacionContableNominaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        LOG.info("OPERACION: opActualizarHallazgosConciliacionContableNomina ::: FIN");
        
        OpActualizarHallazgosConciliacionContableNominaRespTipo resp = new OpActualizarHallazgosConciliacionContableNominaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
