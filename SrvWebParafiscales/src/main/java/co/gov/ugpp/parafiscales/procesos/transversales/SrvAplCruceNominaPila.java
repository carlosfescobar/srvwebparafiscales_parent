package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.MsjOpActualizarHallazgosNominaPilaFallo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.MsjOpCruzarNominaPilaFallo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.OpActualizarHallazgosNominaPilaRespTipo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.OpActualizarHallazgosNominaPilaSolTipo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.OpCruzarNominaPilaRespTipo;
import co.gov.ugpp.transversales.srvaplcrucenominapila.v1.OpCruzarNominaPilaSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author everis
 */
@WebService(serviceName = "SrvAplCruceNominaPila",
        portName = "portSrvAplCruceNominaPilaSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplcrucenominapila.v1.PortSrvAplCruceNominaPilaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplCruceNominaPila extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplCruceNominaPila.class);

    @EJB
    private CruceNominaPilaFacade nominaFacade;

    public OpCruzarNominaPilaRespTipo opCruzarNominaPila(OpCruzarNominaPilaSolTipo msjOpCruzarNominaPilaSol) throws MsjOpCruzarNominaPilaFallo {

        LOG.info("OPERACION: opCruzarNominaPila ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCruzarNominaPilaSol.getContextoTransaccional();
        final IdentificacionTipo identificacionTipo = msjOpCruzarNominaPilaSol.getIdentificacion();
        final List<ParametroTipo> parametroTipoList = msjOpCruzarNominaPilaSol.getParametro();
        final ExpedienteTipo expedienteTipo = msjOpCruzarNominaPilaSol.getExpediente();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            nominaFacade.cruzarPilaConNomina(identificacionTipo, expedienteTipo, parametroTipoList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarNominaPilaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCruzarNominaPilaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        LOG.info("OPERACION: opCruzarNominaPila ::: FIN");

        final OpCruzarNominaPilaRespTipo resp = new OpCruzarNominaPilaRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;
    }

    public OpActualizarHallazgosNominaPilaRespTipo opActualizarHallazgosNominaPila(OpActualizarHallazgosNominaPilaSolTipo msjOpActualizarHallazgosNominaPilaSol) throws MsjOpActualizarHallazgosNominaPilaFallo {

        LOG.info("OPERACION: opActualizarHallazgosNominaPila ::: INICIO");

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarHallazgosNominaPilaSol.getContextoTransaccional();
        final HallazgoNominaTipo hallazgoNominaTipo = msjOpActualizarHallazgosNominaPilaSol.getHallazgo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            nominaFacade.actualizarHallazgosNomina(contextoTransaccionalTipo, hallazgoNominaTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarHallazgosNominaPilaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarHallazgosNominaPilaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        LOG.info("OPERACION: opActualizarHallazgosNominaPila ::: FIN");

        OpActualizarHallazgosNominaPilaRespTipo resp = new OpActualizarHallazgosNominaPilaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

}
