package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.MsjOpActualizarDocumentoCOREFallo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.MsjOpAsociarExpedientesCOREFallo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.MsjOpBuscarPorIdDocumentoCOREFallo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.MsjOpCrearDocumentoCOREFallo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpActualizarDocumentoCORERespTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpActualizarDocumentoCORESolTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpAsociarExpedientesCORERespTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpAsociarExpedientesCORESolTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpBuscarPorIdDocumentoCORERespTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpBuscarPorIdDocumentoCORESolTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpCrearDocumentoCORERespTipo;
import co.gov.ugpp.transversales.srvapldocumentocore.v1.OpCrearDocumentoCORESolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplDocumentoCORE",
        portName = "portSrvAplDocumentoCORESOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvapldocumentocore.v1.PortSrvAplDocumentoCORESOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplDocumentoCORE extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplDocumentoCORE.class);

    @EJB
    private DocumentoFacade documentoFacade;

    public OpCrearDocumentoCORERespTipo opCrearDocumentoCORE(OpCrearDocumentoCORESolTipo msjOpCrearDocumentoCORESolTipo)
            throws MsjOpCrearDocumentoCOREFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearDocumentoCORESolTipo.getContextoTransaccional();
        final DocumentoTipo documentoTipo = msjOpCrearDocumentoCORESolTipo.getDocumento();
        final ExpedienteTipo expedienteTipo = msjOpCrearDocumentoCORESolTipo.getExpediente();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            documentoFacade.crearDocumento(documentoTipo, expedienteTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearDocumentoCORERespTipo resp = new OpCrearDocumentoCORERespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpAsociarExpedientesCORERespTipo opAsociarExpedientesCORE(final OpAsociarExpedientesCORESolTipo msjOpAsociarExpedientesCORESolTipo)
            throws MsjOpAsociarExpedientesCOREFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpAsociarExpedientesCORESolTipo.getContextoTransaccional();
        final DocumentoTipo documentoTipo = msjOpAsociarExpedientesCORESolTipo.getDocumento();
        final List<ExpedienteTipo> expedienteTipoList = msjOpAsociarExpedientesCORESolTipo.getExpedientes();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            documentoFacade.asociarExpedientes(documentoTipo, expedienteTipoList, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAsociarExpedientesCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAsociarExpedientesCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpAsociarExpedientesCORERespTipo resp = new OpAsociarExpedientesCORERespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdDocumentoCORERespTipo opBuscarPorIdDocumentoCORE(OpBuscarPorIdDocumentoCORESolTipo msjOpBuscarPorIdDocumentoCORESol) throws MsjOpBuscarPorIdDocumentoCOREFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdDocumentoCORESol.getContextoTransaccional();
        final List<String> idDocumentoList = msjOpBuscarPorIdDocumentoCORESol.getIdDocumento();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final List<DocumentoTipo> documentoTipoList;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            documentoTipoList = documentoFacade.buscarPorIdDocumento(idDocumentoList);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorIdDocumentoCORERespTipo resp = new OpBuscarPorIdDocumentoCORERespTipo();
        resp.setContextoRespuesta(cr);
        resp.getDocumento().addAll(documentoTipoList);

        return resp;
    }

    public OpActualizarDocumentoCORERespTipo opActualizarDocumentoCORE(OpActualizarDocumentoCORESolTipo msjOpActualizarDocumentoCORESol) throws MsjOpActualizarDocumentoCOREFallo {
       final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarDocumentoCORESol.getContextoTransaccional();
        final DocumentoTipo documentoTipo = msjOpActualizarDocumentoCORESol.getDocumento();
        final ExpedienteTipo expedienteTipo =msjOpActualizarDocumentoCORESol.getExpediente();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            documentoFacade.actualizarDocumento(documentoTipo, expedienteTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDocumentoCOREFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarDocumentoCORERespTipo resp = new OpActualizarDocumentoCORERespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }
}
