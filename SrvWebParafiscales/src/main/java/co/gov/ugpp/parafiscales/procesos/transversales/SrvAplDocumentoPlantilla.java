package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.schema.transversales.documentoplantillatipo.v1.DocumentoPlantillaTipo;
import co.gov.ugpp.transversales.srvapldocumentoplantilla.v1.MsjOpLlenarTokensDocumentoPlantillaFallo;
import co.gov.ugpp.transversales.srvapldocumentoplantilla.v1.OpLlenarTokensDocumentoPlantillaRespTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jsaenzar
 */
@WebService(serviceName = "SrvAplDocumentoPlantilla", 
        portName = "portSrvAplDocumentoPlantillaSOAP", 
        endpointInterface = "co.gov.ugpp.transversales.srvapldocumentoplantilla.v1.PortSrvAplDocumentoPlantillaSOAP", 
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplDocumentoPlantilla extends AbstractSrvApl {
    
    private static final Logger LOG = LoggerFactory.getLogger(SrvAplDocumentoPlantilla.class);      
    
    @EJB
    private DocumentoPlantillaFacade documentoPlantillaFacade;

    public co.gov.ugpp.transversales.srvapldocumentoplantilla.v1.OpLlenarTokensDocumentoPlantillaRespTipo opLlenarTokensDocumentoPlantilla(
            co.gov.ugpp.transversales.srvapldocumentoplantilla.v1.OpLlenarTokensDocumentoPlantillaSolTipo msjOpLlenarTokensDocumentoPlantillaSol) 
            throws MsjOpLlenarTokensDocumentoPlantillaFallo {
       
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpLlenarTokensDocumentoPlantillaSol.getContextoTransaccional();
       
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        
        DocumentoPlantillaTipo documentoPlantillaTipo = msjOpLlenarTokensDocumentoPlantillaSol.getDocumentoPlantilla();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
             documentoPlantillaTipo = documentoPlantillaFacade.llenarTokensDocumentoPlantilla(documentoPlantillaTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpLlenarTokensDocumentoPlantillaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpLlenarTokensDocumentoPlantillaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
       
        final OpLlenarTokensDocumentoPlantillaRespTipo resp = new OpLlenarTokensDocumentoPlantillaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setDocumentoPlantilla(documentoPlantillaTipo);
       
        return resp;
    }
}
