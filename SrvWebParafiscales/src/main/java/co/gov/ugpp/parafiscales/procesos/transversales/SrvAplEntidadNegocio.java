package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.EntidadNegocioTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.MsjOpActualizarEntidadNegocioFallo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.MsjOpBuscarPorCriteriosEntidadNegocioFallo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.MsjOpCrearEntidadNegocioFallo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpActualizarEntidadNegocioRespTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpActualizarEntidadNegocioSolTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpBuscarPorCriteriosEntidadNegocioRespTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpBuscarPorCriteriosEntidadNegocioSolTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpCrearEntidadNegocioRespTipo;
import co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpCrearEntidadNegocioSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplEntidadNegocio",
        portName = "portSrvAplEntidadNegocioSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplentidadnegocio.v1.PortSrvAplEntidadNegocioSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplEntidadNegocio extends AbstractSrvApl {

    @EJB
    private EntidadNegocioFacade entidadNegocioFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplEntidadNegocio.class);

    public OpCrearEntidadNegocioRespTipo opCrearEntidadNegocio(OpCrearEntidadNegocioSolTipo msjOpCrearEntidadNegocioSol) throws MsjOpCrearEntidadNegocioFallo {
        LOG.info("OPERACION: opCrearEntidadNegocio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearEntidadNegocioSol.getContextoTransaccional();
        final EntidadNegocioTipo entidadNegocioTipo = msjOpCrearEntidadNegocioSol.getEntidadNegocio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            entidadNegocioFacade.crearEntidadNegocio(contextoTransaccionalTipo, entidadNegocioTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearEntidadNegocioRespTipo resp = new OpCrearEntidadNegocioRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opCrearEntidadNegocio ::: FIN");
        return resp;
    }

    public OpActualizarEntidadNegocioRespTipo opActualizarEntidadNegocio(OpActualizarEntidadNegocioSolTipo msjOpActualizarEntidadNegocioSol) throws MsjOpActualizarEntidadNegocioFallo {
        LOG.info("OPERACION: opActualizarEntidadNegocio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarEntidadNegocioSol.getContextoTransaccional();
        final EntidadNegocioTipo entidadNegocioTipo = msjOpActualizarEntidadNegocioSol.getEntidadNegocio();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            entidadNegocioFacade.actualizarEntidadNegocio(contextoTransaccionalTipo, entidadNegocioTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarEntidadNegocioRespTipo resp = new OpActualizarEntidadNegocioRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarEntidadNegocio ::: FIN");
        return resp;
    }

    public OpBuscarPorCriteriosEntidadNegocioRespTipo opBuscarPorCriteriosEntidadNegocio(OpBuscarPorCriteriosEntidadNegocioSolTipo msjOpBuscarPorCriteriosEntidadNegocioSol) throws MsjOpBuscarPorCriteriosEntidadNegocioFallo {
        LOG.info("OPERACION: opBuscarPorCriteriosEntidadNegocio ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosEntidadNegocioSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosEntidadNegocioSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<EntidadNegocioTipo> entidadesNegocioPagerData;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            entidadesNegocioPagerData = entidadNegocioFacade.buscarPorCriteriosEntidadNegocio(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosEntidadNegocioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpBuscarPorCriteriosEntidadNegocioRespTipo resp = new OpBuscarPorCriteriosEntidadNegocioRespTipo();
        cr.setValCantidadPaginas(entidadesNegocioPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getEntidadesNegocio().addAll(entidadesNegocioPagerData.getData());
        LOG.info("OPERACION: opBuscarPorCriteriosEntidadNegocio ::: FIN");
        return resp;
    }

}
