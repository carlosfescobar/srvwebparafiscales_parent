package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1.MsjOpBuscarPorCriteriosFuncionarioFallo;
import co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1.OpBuscarPorCriteriosFuncionarioRespTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplFuncionario",
        portName = "portSrvAplFuncionarioSOAP",
        endpointInterface = "co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1.PortSrvAplFuncionarioSOAP",
        targetNamespace = "http://www.ugpp.gov.co/GestionDocumental/SrvAplFuncionario/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplFuncionario extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplFuncionario.class);

    @EJB
    private FuncionarioFacade funcionarioFacade;

    public co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1.OpBuscarPorCriteriosFuncionarioRespTipo opBuscarPorCriteriosFuncionario(co.gov.ugpp.gestiondocumental.srvaplfuncionario.v1.OpBuscarPorCriteriosFuncionarioSolTipo msjOpBuscarPorCriteriosFuncionarioSol) throws MsjOpBuscarPorCriteriosFuncionarioFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosFuncionarioSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosFuncionarioSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<FuncionarioTipo> funcionarioTipos;

        try {

            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            funcionarioTipos = funcionarioFacade.buscarPorCriteriosFuncionario(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
            
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFuncionarioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosFuncionarioFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosFuncionarioRespTipo resp = new OpBuscarPorCriteriosFuncionarioRespTipo();
        cr.setValCantidadPaginas(funcionarioTipos.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getFuncionario().addAll(funcionarioTipos.getData());

        return resp;
    }
}
