package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.SrvAplArchivo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.transversales.srvaplkpiproceso.v1.MsjOpConsultarKPIProcesoFallo;
import co.gov.ugpp.transversales.srvaplkpiproceso.v1.OpConsultarKPIProcesoRespTipo;
import co.gov.ugpp.transversales.srvaplkpiproceso.v1.OpConsultarKPIProcesoSolTipo;
import java.util.Calendar;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplKPIProceso",
        portName = "portSrvAplKPIProcesoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplkpiproceso.v1.PortSrvAplKPIProcesoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplKPIProceso extends AbstractSrvApl {

    @EJB
    private KPIProcesoFacade kpiProcesoFacade;

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplArchivo.class);

    public OpConsultarKPIProcesoRespTipo opConsultarKPIProceso(OpConsultarKPIProcesoSolTipo msjOpConsultarKPIProcesoSol) throws MsjOpConsultarKPIProcesoFallo {
        LOG.info("OPERACION: opConsultarKPIProceso ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpConsultarKPIProcesoSol.getContextoTransaccional();
        final String codTipoConsulta = msjOpConsultarKPIProcesoSol.getCodTipoConsulta();
        final Calendar fecInicio = msjOpConsultarKPIProcesoSol.getFecInicio();
        final Calendar fecFin = msjOpConsultarKPIProcesoSol.getFecFin();
        final List<ParametroTipo> parametros = msjOpConsultarKPIProcesoSol.getParametros();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final List<ParametroValoresTipo> objetosConsultados;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            objetosConsultados = kpiProcesoFacade.consultarKPIProceso(contextoTransaccionalTipo, codTipoConsulta, fecInicio, fecFin, parametros);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarKPIProcesoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarKPIProcesoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpConsultarKPIProcesoRespTipo resp = new OpConsultarKPIProcesoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getResultadoConsulta().addAll(objetosConsultados);
        LOG.info("OPERACION: opConsultarKPIProceso ::: FIN");
        return resp;
    }
}
