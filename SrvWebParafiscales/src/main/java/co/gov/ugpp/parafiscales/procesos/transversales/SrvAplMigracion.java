package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import co.gov.ugpp.schema.transversales.migraciontipo.v1.MigracionTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpActualizarCasoMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpActualizarMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpBuscarPorCriteriosCasoMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpBuscarPorCriteriosMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpCrearCasoMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.MsjOpCrearMigracionFallo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpActualizarCasoMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpActualizarCasoMigracionSolTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpActualizarMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpActualizarMigracionSolTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpBuscarPorCriteriosCasoMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpBuscarPorCriteriosCasoMigracionSolTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpBuscarPorCriteriosMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpBuscarPorCriteriosMigracionSolTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpCrearCasoMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpCrearCasoMigracionSolTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpCrearMigracionRespTipo;
import co.gov.ugpp.transversales.srvaplmigracion.v1.OpCrearMigracionSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplMigracion",
        portName = "portSrvAplMigracionSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplmigracion.v1.PortSrvAplMigracionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplMigracion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplMigracion.class);

    @EJB
    private MigracionFacade migracionFacade;

    public OpCrearMigracionRespTipo opCrearMigracion(OpCrearMigracionSolTipo msjOpCrearMigracionSol) throws MsjOpCrearMigracionFallo {
        LOG.info("OPERACION: opCrearMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearMigracionSol.getContextoTransaccional();
        final MigracionTipo migracionTipo = msjOpCrearMigracionSol.getMigracion();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        Long idMigracion;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            idMigracion = migracionFacade.crearMigracion(migracionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearMigracionRespTipo resp = new OpCrearMigracionRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdMigracion(idMigracion.toString());
        LOG.info("OPERACION: opCrearMigracion ::: FIN");
        return resp;
    }

    public OpActualizarMigracionRespTipo opActualizarMigracion(OpActualizarMigracionSolTipo msjOpActualizarMigracionSol) throws MsjOpActualizarMigracionFallo {
        LOG.info("OPERACION: opActualizarMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarMigracionSol.getContextoTransaccional();
        final MigracionTipo migracionTipo = msjOpActualizarMigracionSol.getMigracion();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            migracionFacade.actualizarMigracion(migracionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarMigracionRespTipo resp = new OpActualizarMigracionRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarMigracion ::: FIN");
        return resp;
    }

    public OpBuscarPorCriteriosMigracionRespTipo opBuscarPorCriteriosMigracion(OpBuscarPorCriteriosMigracionSolTipo msjOpBuscarPorCriteriosMigracionSol) throws MsjOpBuscarPorCriteriosMigracionFallo {
        LOG.info("OPERACION: opBuscarPorCriteriosMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosMigracionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosMigracionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<MigracionTipo> migracionPagerData;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            migracionPagerData = migracionFacade.buscarPorCriteriosMigracion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosMigracionRespTipo resp = new OpBuscarPorCriteriosMigracionRespTipo();
        cr.setValCantidadPaginas(migracionPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getMigracion().addAll(migracionPagerData.getData());
        LOG.info("OPERACION: opBuscarPorCriteriosMigracion ::: FIN");
        return resp;
    }

    public OpCrearCasoMigracionRespTipo opCrearCasoMigracion(OpCrearCasoMigracionSolTipo msjOpCrearCasoMigracionSol) throws MsjOpCrearCasoMigracionFallo {
        LOG.info("OPERACION: opCrearCasoMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearCasoMigracionSol.getContextoTransaccional();
        final MigracionCasoTipo migracionCasoTipo = msjOpCrearCasoMigracionSol.getMigracionCaso();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            migracionFacade.crearCasoMigracion(migracionCasoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearCasoMigracionRespTipo resp = new OpCrearCasoMigracionRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opCrearCasoMigracion ::: FIN");
        return resp;
    }

    public OpActualizarCasoMigracionRespTipo opActualizarCasoMigracion(OpActualizarCasoMigracionSolTipo msjOpActualizarCasoMigracionSol) throws MsjOpActualizarCasoMigracionFallo {
        LOG.info("OPERACION: opActualizarCasoMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarCasoMigracionSol.getContextoTransaccional();
        final MigracionCasoTipo migracionCasoTipo = msjOpActualizarCasoMigracionSol.getMigracionCaso();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            migracionFacade.actualizarCasoMigracion(migracionCasoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarCasoMigracionRespTipo resp = new OpActualizarCasoMigracionRespTipo();
        resp.setContextoRespuesta(cr);
        LOG.info("OPERACION: opActualizarCasoMigracion ::: FIN");
        return resp;
    }

    public OpBuscarPorCriteriosCasoMigracionRespTipo opBuscarPorCriteriosCasoMigracion(OpBuscarPorCriteriosCasoMigracionSolTipo msjOpBuscarPorCriteriosCasoMigracionSol) throws MsjOpBuscarPorCriteriosCasoMigracionFallo {
        LOG.info("OPERACION: opBuscarPorCriteriosCasoMigracion ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosCasoMigracionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosCasoMigracionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<MigracionCasoTipo> migracionCasoPagerData;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            migracionCasoPagerData = migracionFacade.buscarPorCriteriosCasoMigracion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosCasoMigracionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosCasoMigracionRespTipo resp = new OpBuscarPorCriteriosCasoMigracionRespTipo();
        cr.setValCantidadPaginas(migracionCasoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getMigracionCaso().addAll(migracionCasoPagerData.getData());
        LOG.info("OPERACION: opBuscarPorCriteriosCasoMigracion ::: FIN");
        return resp;
    }
}
