package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1.NumeroActoAdministrativoTipo;
import co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.MsjOpGenerarNumeroActoAdministrativoFallo;
import co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.OpGenerarNumeroActoAdministrativoRespTipo;
import co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.OpGenerarNumeroActoAdministrativoSolTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplNumeroActoAdministrativo",
        portName = "portSrvAplNumeroActoAdministrativoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.PortSrvAplNumeroActoAdministrativoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplNumeroActoAdministrativo extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplNumeroActoAdministrativo.class);

    @EJB
    private NumeroActoAdministrativoFacade numeroActoAdministrativoFacade;

    public OpGenerarNumeroActoAdministrativoRespTipo opGenerarNumeroActoAdministrativo(final OpGenerarNumeroActoAdministrativoSolTipo msjOpGenerarNumeroActoAdministrativoSol) throws MsjOpGenerarNumeroActoAdministrativoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpGenerarNumeroActoAdministrativoSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final NumeroActoAdministrativoTipo numeroActoAdministrativoTipo = msjOpGenerarNumeroActoAdministrativoSol.getNumeroActoAdministrativo();

        final NumeroActoAdministrativoTipo numeroActoAdministrativoTipoResp;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            numeroActoAdministrativoTipoResp = numeroActoAdministrativoFacade.generarNumeroActoAdministrativo(numeroActoAdministrativoTipo,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpGenerarNumeroActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpGenerarNumeroActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpGenerarNumeroActoAdministrativoRespTipo resp = new OpGenerarNumeroActoAdministrativoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setNumeroActoAdministrativo(numeroActoAdministrativoTipoResp);

        return resp;
    }

}
