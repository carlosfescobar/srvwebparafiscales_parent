package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.transversales.srvaplpersonajuridica.v1.MsjOpBuscarPorIdPersonaJuridicaFallo;
import co.gov.ugpp.transversales.srvaplpersonajuridica.v1.OpBuscarPorIdPersonaJuridicaRespTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplPersonaJuridica",
    portName = "portSrvAplPersonaJuridicaSOAP",
    endpointInterface = "co.gov.ugpp.transversales.srvaplpersonajuridica.v1.PortSrvAplPersonaJuridicaSOAP",
    targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplPersonaJuridica extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplPersonaJuridica.class);

    @EJB
    private PersonaJuridicaFacade personaJuridicaFacade;


    public co.gov.ugpp.transversales.srvaplpersonajuridica.v1.OpBuscarPorIdPersonaJuridicaRespTipo
        opBuscarPorIdPersonaJuridica(co.gov.ugpp.transversales.srvaplpersonajuridica.v1.OpBuscarPorIdPersonaJuridicaSolTipo msjOpBuscarPorIdPersonaJuridicaSol) throws MsjOpBuscarPorIdPersonaJuridicaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdPersonaJuridicaSol.getContextoTransaccional();
        final List<IdentificacionTipo> identificacionTipos = msjOpBuscarPorIdPersonaJuridicaSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        

        final List<PersonaJuridicaTipo> personaJuridicaTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            personaJuridicaTipos = personaJuridicaFacade.buscarPorIdPersonaJuridica(identificacionTipos,contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaJuridicaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaJuridicaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdPersonaJuridicaRespTipo resp = new OpBuscarPorIdPersonaJuridicaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getPersonaJuridica().addAll(personaJuridicaTipos);

        return resp;
    }
}
