package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.transversales.srvaplpersonanatural.v1.MsjOpBuscarPorIdPersonaNaturalFallo;
import co.gov.ugpp.transversales.srvaplpersonanatural.v1.MsjOpCrearPersonaNaturalFallo;
import co.gov.ugpp.transversales.srvaplpersonanatural.v1.OpBuscarPorIdPersonaNaturalRespTipo;
import co.gov.ugpp.transversales.srvaplpersonanatural.v1.OpCrearPersonaNaturalRespTipo;
import co.gov.ugpp.transversales.srvaplpersonanatural.v1.OpCrearPersonaNaturalSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author jmunocab
 */
@WebService(serviceName = "SrvAplPersonaNatural",
        portName = "portSrvAplPersonaNaturalSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplpersonanatural.v1.PortSrvAplPersonaNaturalSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplPersonaNatural extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplPersonaNatural.class);

    @EJB
    private PersonaNaturalFacade personaNaturalFacade;

    public co.gov.ugpp.transversales.srvaplpersonanatural.v1.OpBuscarPorIdPersonaNaturalRespTipo opBuscarPorIdPersonaNatural(co.gov.ugpp.transversales.srvaplpersonanatural.v1.OpBuscarPorIdPersonaNaturalSolTipo msjOpBuscarPorIdPersonaNaturalSol) throws MsjOpBuscarPorIdPersonaNaturalFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdPersonaNaturalSol.getContextoTransaccional();
        final List<IdentificacionTipo> identificacionTipos = msjOpBuscarPorIdPersonaNaturalSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<PersonaNaturalTipo> personaNaturalTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            personaNaturalTipos = personaNaturalFacade.buscarPorIdPersonaNatural(identificacionTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaNaturalFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdPersonaNaturalFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdPersonaNaturalRespTipo resp = new OpBuscarPorIdPersonaNaturalRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getPersonaNatural().addAll(personaNaturalTipos);

        return resp;
    }

    public OpCrearPersonaNaturalRespTipo opCrearPersonaNatural(OpCrearPersonaNaturalSolTipo msjOpCrearPersonaNaturalSol) throws MsjOpCrearPersonaNaturalFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearPersonaNaturalSol.getContextoTransaccional();
        final PersonaNaturalTipo personaNaturalTipo = msjOpCrearPersonaNaturalSol.getPersonaNatural();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            personaNaturalFacade.crearPersonaNatural(personaNaturalTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearPersonaNaturalFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearPersonaNaturalFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearPersonaNaturalRespTipo resp = new OpCrearPersonaNaturalRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;

    }

}
