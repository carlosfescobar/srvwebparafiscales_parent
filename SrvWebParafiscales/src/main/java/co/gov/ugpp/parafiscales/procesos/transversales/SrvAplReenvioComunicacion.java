package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1.ReenvioComunicacionTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.MsjOpActualizarReenvioComunicacionFallo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.MsjOpBuscarPorCriteriosReenvioComunicacionFallo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.MsjOpCrearReenvioComunicacionFallo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpActualizarReenvioComunicacionRespTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpActualizarReenvioComunicacionSolTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpBuscarPorCriteriosReenvioComunicacionRespTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpBuscarPorCriteriosReenvioComunicacionSolTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpCrearReenvioComunicacionRespTipo;
import co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.OpCrearReenvioComunicacionSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author yrinconh
 */
@WebService(serviceName = "SrvAplReenvioComunicacion",
        portName = "portSrvAplReenvioComunicacionSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1.PortSrvAplReenvioComunicacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1")
public class SrvAplReenvioComunicacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplReenvioComunicacion.class);
    @EJB
    private ReenvioComunicacionFacade reenvioComunicacionFacade;

    public OpCrearReenvioComunicacionRespTipo opCrearReenvioComunicacion(
            OpCrearReenvioComunicacionSolTipo msjOpCrearReenvioComunicacionSol) throws MsjOpCrearReenvioComunicacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearReenvioComunicacionSol.getContextoTransaccional();
        final ReenvioComunicacionTipo trazabilidadRadicacionActoTipo = msjOpCrearReenvioComunicacionSol.getReenvioComunicacion();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            reenvioComunicacionFacade.crearReenvioComunicacion(trazabilidadRadicacionActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpCrearReenvioComunicacionRespTipo resp = new OpCrearReenvioComunicacionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpActualizarReenvioComunicacionRespTipo opActualizarReenvioComunicacion(
            OpActualizarReenvioComunicacionSolTipo msjOpActualizarReenvioComunicacionSol) throws MsjOpActualizarReenvioComunicacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarReenvioComunicacionSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final ReenvioComunicacionTipo reenvioComunicacionTipo = msjOpActualizarReenvioComunicacionSol.getReenvioComunicacion();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            reenvioComunicacionFacade.actualizarReenvioComunicacion(reenvioComunicacionTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarReenvioComunicacionRespTipo resp = new OpActualizarReenvioComunicacionRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorCriteriosReenvioComunicacionRespTipo opBuscarPorCriteriosReenvioComunicacion(
            OpBuscarPorCriteriosReenvioComunicacionSolTipo msjOpBuscarPorCriteriosReenvioComunicacionSol) throws MsjOpBuscarPorCriteriosReenvioComunicacionFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosReenvioComunicacionSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosReenvioComunicacionSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<ReenvioComunicacionTipo> reenvioComunicacionTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            reenvioComunicacionTipoPagerData = reenvioComunicacionFacade.buscarPorCriteriosReenvioComunicacion(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosReenvioComunicacionFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosReenvioComunicacionRespTipo resp = new OpBuscarPorCriteriosReenvioComunicacionRespTipo();
        cr.setValCantidadPaginas(reenvioComunicacionTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getReenvioComunicacion().addAll(reenvioComunicacionTipoPagerData.getData());

        return resp;

    }

}
