package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.MsjOpActualizarDocumentacionEsperadaFallo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.MsjOpBuscarPorCriteriosDocumentacionEsperadaFallo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.MsjOpRegistrarDocumentacionEsperadaFallo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpActualizarDocumentacionEsperadaRespTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpActualizarDocumentacionEsperadaSolTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpBuscarPorCriteriosDocumentacionEsperadaRespTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpBuscarPorCriteriosDocumentacionEsperadaSolTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpRegistrarDocumentacionEsperadaRespTipo;
import co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.OpRegistrarDocumentacionEsperadaSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author rpadilla
 */
@WebService(serviceName = "SrvAplRespuestaDocumentacion",
        portName = "portSrvAplRespuestaDocumentacionSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1.PortSrvAplRespuestaDocumentacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplRespuestaDocumentacion extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplRespuestaDocumentacion.class);

    @EJB
    private RespuestaDocumentacionFacade respuestaDocumentacionFacade;

    public OpActualizarDocumentacionEsperadaRespTipo opActualizarDocumentacionEsperada(
            OpActualizarDocumentacionEsperadaSolTipo msjOpActualizarDocumentacionEsperadaSol) throws MsjOpActualizarDocumentacionEsperadaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarDocumentacionEsperadaSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final DocumentacionEsperadaTipo documentacionEsperadaTipo = msjOpActualizarDocumentacionEsperadaSol.getDocumentacionEsperada();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            respuestaDocumentacionFacade.actualizarDocumentacionEsperada(documentacionEsperadaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarDocumentacionEsperadaRespTipo resp = new OpActualizarDocumentacionEsperadaRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpRegistrarDocumentacionEsperadaRespTipo opRegistrarDocumentacionEsperada(
            OpRegistrarDocumentacionEsperadaSolTipo msjOpRegistrarDocumentacionEsperadaSol) throws MsjOpRegistrarDocumentacionEsperadaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpRegistrarDocumentacionEsperadaSol.getContextoTransaccional();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final DocumentacionEsperadaTipo documentacionEsperadaTipo = msjOpRegistrarDocumentacionEsperadaSol.getDocumentacionEsperada();

        final Long generatedId;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            generatedId = respuestaDocumentacionFacade.registrarDocumentacionEsperada(documentacionEsperadaTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpRegistrarDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpRegistrarDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpRegistrarDocumentacionEsperadaRespTipo resp = new OpRegistrarDocumentacionEsperadaRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setIdInformacionEsperada(generatedId.toString());

        return resp;
    }

    public OpBuscarPorCriteriosDocumentacionEsperadaRespTipo opBuscarPorCriteriosDocumentacionEsperada(
            OpBuscarPorCriteriosDocumentacionEsperadaSolTipo msjOpBuscarPorCriteriosDocumentacionEsperadaSol) throws MsjOpBuscarPorCriteriosDocumentacionEsperadaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosDocumentacionEsperadaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosDocumentacionEsperadaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<DocumentacionEsperadaTipo> documentacionEsperadaTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            documentacionEsperadaTipoPagerData = respuestaDocumentacionFacade.buscarPorCriteriosDocumentacionEsperada(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDocumentacionEsperadaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosDocumentacionEsperadaRespTipo resp = new OpBuscarPorCriteriosDocumentacionEsperadaRespTipo();

        cr.setValCantidadPaginas(documentacionEsperadaTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getDocumentacionEsperada().addAll(documentacionEsperadaTipoPagerData.getData());

        return resp;
    }
}
