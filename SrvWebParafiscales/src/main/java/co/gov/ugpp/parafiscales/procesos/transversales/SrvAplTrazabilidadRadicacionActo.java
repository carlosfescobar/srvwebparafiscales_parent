/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1.TrazabilidadRadicacionActoTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.MsjOpActualizarTrazabilidadRadicacionActoFallo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.MsjOpBuscarPorIdTrazabilidadRadicacionActoFallo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.MsjOpCrearTrazabilidadRadicacionActoFallo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpActualizarTrazabilidadRadicacionActoRespTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpActualizarTrazabilidadRadicacionActoSolTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpBuscarPorIdTrazabilidadRadicacionActoRespTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpBuscarPorIdTrazabilidadRadicacionActoSolTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpCrearTrazabilidadRadicacionActoRespTipo;
import co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.OpCrearTrazabilidadRadicacionActoSolTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplTrazabilidadRadicacionActo",
        portName = "portSrvAplTrazabilidadRadicacionActoSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1.PortSrvAplTrazabilidadRadicacionActoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplTrazabilidadRadicacionActo extends AbstractSrvApl {

    @EJB
    private TrazabilidadRadicacionActoFacade trazabilidadRadicacionActoFacade;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplTrazabilidadRadicacionActo.class);

    public OpCrearTrazabilidadRadicacionActoRespTipo opCrearTrazabilidadRadicacionActo(OpCrearTrazabilidadRadicacionActoSolTipo msjOpCrearTrazabilidadRadicacionActoSol) throws MsjOpCrearTrazabilidadRadicacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpCrearTrazabilidadRadicacionActoSol.getContextoTransaccional();
        final TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo = msjOpCrearTrazabilidadRadicacionActoSol.getTrazabilidadRadicacionActo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            trazabilidadRadicacionActoFacade.crearTrazabilidadRadicacionActo(trazabilidadRadicacionActoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpCrearTrazabilidadRadicacionActoRespTipo resp = new OpCrearTrazabilidadRadicacionActoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpActualizarTrazabilidadRadicacionActoRespTipo opActualizarTrazabilidadRadicacionActo(OpActualizarTrazabilidadRadicacionActoSolTipo msjOpActualizarTrazabilidadRadicacionActoSol) throws MsjOpActualizarTrazabilidadRadicacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarTrazabilidadRadicacionActoSol.getContextoTransaccional();
        final TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo = msjOpActualizarTrazabilidadRadicacionActoSol.getTrazabilidadRadicacionActo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            trazabilidadRadicacionActoFacade.actualizarTrazabilidadRadicacionActo(trazabilidadRadicacionActoTipo, contextoTransaccionalTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpActualizarTrazabilidadRadicacionActoRespTipo resp = new OpActualizarTrazabilidadRadicacionActoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public OpBuscarPorIdTrazabilidadRadicacionActoRespTipo opBuscarPorIdTrazabilidadRadicacionActo(OpBuscarPorIdTrazabilidadRadicacionActoSolTipo msjOpBuscarPorIdTrazabilidadRadicacionActoSol) throws MsjOpBuscarPorIdTrazabilidadRadicacionActoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdTrazabilidadRadicacionActoSol.getContextoTransaccional();
        final List<String> idTrazabilidadRadicacionActos = msjOpBuscarPorIdTrazabilidadRadicacionActoSol.getIdActoTrazabilidadRadicacionActo();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<TrazabilidadRadicacionActoTipo> trazabilidadRadicacionActoTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            trazabilidadRadicacionActoTipos = trazabilidadRadicacionActoFacade.buscarPorIdTrazabilidadRadicacionActo(idTrazabilidadRadicacionActos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdTrazabilidadRadicacionActoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdTrazabilidadRadicacionActoRespTipo resp = new OpBuscarPorIdTrazabilidadRadicacionActoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.getTrazabilidadRadicacionActo().addAll(trazabilidadRadicacionActoTipos);

        return resp;
    }

}
