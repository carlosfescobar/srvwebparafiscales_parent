package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.trasnversales.validacionarchivotipo.v1.ValidacionArchivoTipo;
import co.gov.ugpp.transversales.srvaplvalidador.v1.MsjOpValidarFormatoArchivoFallo;
import co.gov.ugpp.transversales.srvaplvalidador.v1.OpValidarFormatoArchivoRespTipo;
import co.gov.ugpp.transversales.srvaplvalidador.v1.OpValidarFormatoArchivoSolTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplValidador",
        portName = "portSrvAplValidadorSOAP",
        endpointInterface = "co.gov.ugpp.transversales.srvaplvalidador.v1.PortSrvAplValidadorSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplValidador extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplValidador.class);

    @EJB
    private ValidadorFacade validadorFacade;

    public OpValidarFormatoArchivoRespTipo opValidarFormatoArchivo(OpValidarFormatoArchivoSolTipo msjOpValidarFormatoArchivoSol) throws MsjOpValidarFormatoArchivoFallo {
        LOG.info("OPERACION: opValidarFormatoArchivo ::: INICIO");
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpValidarFormatoArchivoSol.getContextoTransaccional();
        final ArchivoTipo archivoTipo = msjOpValidarFormatoArchivoSol.getArchivoValidar();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        ValidacionArchivoTipo validacionArchivoTipo;
        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            validacionArchivoTipo = validadorFacade.validarFormatoArchivo(contextoTransaccionalTipo, archivoTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarFormatoArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarFormatoArchivoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        final OpValidarFormatoArchivoRespTipo resp = new OpValidarFormatoArchivoRespTipo();
        resp.setContextoRespuesta(cr);
        resp.setValidacionArchivoTipo(validacionArchivoTipo);
        LOG.info("OPERACION: opValidarFormatoArchivo ::: FIN");
        return resp;
    }

}
