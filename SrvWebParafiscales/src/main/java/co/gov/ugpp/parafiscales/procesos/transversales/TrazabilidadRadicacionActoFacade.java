package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1.TrazabilidadRadicacionActoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author zrodrigu
 */
public interface TrazabilidadRadicacionActoFacade extends Serializable {

    void crearTrazabilidadRadicacionActo(TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    List<TrazabilidadRadicacionActoTipo> buscarPorIdTrazabilidadRadicacionActo(final List<String> idTrazabilidadRadicacionActos, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarTrazabilidadRadicacionActo(TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
