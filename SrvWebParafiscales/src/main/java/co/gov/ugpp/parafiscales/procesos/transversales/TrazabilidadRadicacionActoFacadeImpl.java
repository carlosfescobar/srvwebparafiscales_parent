package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.RadicadoTrazabilidadDao;
import co.gov.ugpp.parafiscales.persistence.dao.TrazabilidadActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.EventoActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.RadicadoTrazabilidad;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;
import co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1.RadicadoTipo;
import co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1.TrazabilidadRadicacionActoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 * implementación de la interfaz TrazabilidadRadicacionActoFacade que contiene
 * las operaciones del servicio SrvAplTrazabilidadRadicacionActoFacade
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class TrazabilidadRadicacionActoFacadeImpl extends AbstractFacade implements TrazabilidadRadicacionActoFacade {

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private TrazabilidadActoAdministrativoDao trazabilidadActoAdministrativoDao;

    @EJB
    private RadicadoTrazabilidadDao radicadoTrazabilidadDao;

    @EJB
    private Populator populator;

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;

    @Override
    public void crearTrazabilidadRadicacionActo(final TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (trazabilidadRadicacionActoTipo == null || StringUtils.isBlank(trazabilidadRadicacionActoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        TrazabilidadActoAdministrativo trazabilidadActoAdministrativo = trazabilidadActoAdministrativoDao.find(trazabilidadRadicacionActoTipo.getIdActoAdministrativo());
        if (trazabilidadActoAdministrativo != null) {
            throw new AppException("Ya existe una TrazabilidadRadicacionActo con el ID: " + trazabilidadRadicacionActoTipo.getIdActoAdministrativo());
        } else {
            trazabilidadActoAdministrativo = new TrazabilidadActoAdministrativo();
            trazabilidadActoAdministrativo.setId(trazabilidadRadicacionActoTipo.getIdActoAdministrativo());
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        this.populateAccionActoAdministrativo(trazabilidadRadicacionActoTipo, trazabilidadActoAdministrativo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        trazabilidadActoAdministrativo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        trazabilidadActoAdministrativoDao.create(trazabilidadActoAdministrativo);
    }

    @Override
    public void actualizarTrazabilidadRadicacionActo(final TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (trazabilidadRadicacionActoTipo == null || StringUtils.isBlank(trazabilidadRadicacionActoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        TrazabilidadActoAdministrativo trazabilidadActoAdministrativo = trazabilidadActoAdministrativoDao.find(trazabilidadRadicacionActoTipo.getIdActoAdministrativo());
        if (trazabilidadActoAdministrativo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró un trazabilidadRadicacionActo con el valor:" + trazabilidadRadicacionActoTipo.getIdActoAdministrativo()));
            throw new AppException(errorTipoList);
        }
        trazabilidadActoAdministrativo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        this.populateAccionActoAdministrativo(trazabilidadRadicacionActoTipo, trazabilidadActoAdministrativo, contextoTransaccionalTipo, errorTipoList);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        trazabilidadActoAdministrativoDao.edit(trazabilidadActoAdministrativo);
    }

    @Override
    public List<TrazabilidadRadicacionActoTipo> buscarPorIdTrazabilidadRadicacionActo(List<String> idTrazabilidadRadicacionActos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (idTrazabilidadRadicacionActos == null || idTrazabilidadRadicacionActos.isEmpty()) {
            throw new AppException("Debe proporcionar el listado de ID  a buscar");
        }
        List<TrazabilidadActoAdministrativo> trazabilidadActoAdministrativo = trazabilidadActoAdministrativoDao.findByIdList(idTrazabilidadRadicacionActos);
        final List<TrazabilidadRadicacionActoTipo> trazabilidadRadicacionActoTipoList
                = mapper.map(trazabilidadActoAdministrativo, TrazabilidadActoAdministrativo.class, TrazabilidadRadicacionActoTipo.class);
        return trazabilidadRadicacionActoTipoList;
    }

    /**
     * Método que se encarga de llenar el entity TrazabilidadRadicacionActo
     *
     * @param trazabilidadRadicacionActoTipo el dto con la información a llenar
     * el entity
     * @param trazabilidadActoAdministrativo el entity a ser llenado
     * @param contextoTransaccionalTipo contiene los datos de auditoría para
     * persistir en la base de datos
     * @param errorTipoList la lista con la pila de errores
     * @throws AppException
     */
    private void populateAccionActoAdministrativo(final TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo,
            final TrazabilidadActoAdministrativo trazabilidadActoAdministrativo, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(trazabilidadRadicacionActoTipo.getIdActoAdministrativo())) {
            final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(String.valueOf(trazabilidadRadicacionActoTipo.getIdActoAdministrativo()));
            if (actoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un actoAdministrativo con el ID: " + trazabilidadRadicacionActoTipo.getIdActoAdministrativo()));
            } else {
                trazabilidadActoAdministrativo.setActoAdministrativo(actoAdministrativo);
            }
        }
        if (StringUtils.isNotBlank(trazabilidadRadicacionActoTipo.getIdRadicadoSalidaDefinitivo())) {
            trazabilidadActoAdministrativo.setIdRadicadoSalidaDefinitivo(trazabilidadRadicacionActoTipo.getIdRadicadoSalidaDefinitivo());
        }
        if (trazabilidadRadicacionActoTipo.getFecRadicadoSalidaDefinitivo() != null) {
            trazabilidadActoAdministrativo.setFecRadicadoSalidaDefinitivo(trazabilidadRadicacionActoTipo.getFecRadicadoSalidaDefinitivo());
        }
        if (StringUtils.isNotBlank(trazabilidadRadicacionActoTipo.getValDiasProrroga())) {
            trazabilidadActoAdministrativo.setValDiasProrroga(trazabilidadRadicacionActoTipo.getValDiasProrroga());
        }
        if (trazabilidadRadicacionActoTipo.getFecNotificacionDefinitiva() != null) {
            trazabilidadActoAdministrativo.setFecNotificacionDefinitiva(trazabilidadRadicacionActoTipo.getFecNotificacionDefinitiva());
        }
        if (StringUtils.isNotBlank(trazabilidadRadicacionActoTipo.getCodTipoNotificacionDefinitiva())) {
            ValorDominio codTipoNotificacionDefinitiva = valorDominioDao.find(trazabilidadRadicacionActoTipo.getCodTipoNotificacionDefinitiva());
            if (codTipoNotificacionDefinitiva == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un codTipoNotificacionDefinitiva con el valor:" + trazabilidadRadicacionActoTipo.getCodTipoNotificacionDefinitiva()));
            } else {
                trazabilidadActoAdministrativo.setCodTipoNotificacionDefinitiva(codTipoNotificacionDefinitiva);
            }
        }
        if (StringUtils.isNotBlank(trazabilidadRadicacionActoTipo.getDescTipoNotificacionDefinitiva())) {
            trazabilidadActoAdministrativo.setDescTipoNotificacionDefinitiva(trazabilidadRadicacionActoTipo.getDescTipoNotificacionDefinitiva());
        }
        if (trazabilidadRadicacionActoTipo.getEnvioActoAdministrativo() != null
                && !trazabilidadRadicacionActoTipo.getEnvioActoAdministrativo().isEmpty()) {
            for (EnvioActoAdministrativoTipo envioActoAdministrativoTipo : trazabilidadRadicacionActoTipo.getEnvioActoAdministrativo()) {
                if (envioActoAdministrativoTipo != null) {
                    EventoActoAdministrativo eventoActoAdministrativo = mapper.map(envioActoAdministrativoTipo, EventoActoAdministrativo.class);
                    eventoActoAdministrativo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                    if (StringUtils.isNotBlank(envioActoAdministrativoTipo.getCodEstadoNotificacion())) {
                        ValorDominio codEstadoNotificacion = valorDominioDao.find(envioActoAdministrativoTipo.getCodEstadoNotificacion());
                        if (codEstadoNotificacion == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró codEstadoNotificacion con el ID: " + envioActoAdministrativoTipo.getCodEstadoNotificacion()));
                        } else {
                            eventoActoAdministrativo.setCodEstadNotificacion(codEstadoNotificacion);
                        }
                    }
                    if (envioActoAdministrativoTipo.getUbicacionNotificada() != null) {
                        Ubicacion ubicacion = new Ubicacion();
                        ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        populator.crearUbicacion(envioActoAdministrativoTipo.getUbicacionNotificada(), ubicacion, errorTipoList);
                        eventoActoAdministrativo.setUbicacionNotificada(ubicacion);
                    }
                    eventoActoAdministrativo.setTrazabilidadActoAdministrativo(trazabilidadActoAdministrativo);
                    trazabilidadActoAdministrativo.getEnvioActoAdministrativoList().add(eventoActoAdministrativo);
                }
            }
        }
        if (trazabilidadRadicacionActoTipo.getUbicacionDefinitiva() != null) {
            Ubicacion ubicacion = new Ubicacion();
            ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            populator.crearUbicacion(trazabilidadRadicacionActoTipo.getUbicacionDefinitiva(), ubicacion, errorTipoList);
            trazabilidadActoAdministrativo.setUbicacionDefinitiva(ubicacion);
        }
        if (trazabilidadRadicacionActoTipo.getRadicacionesEntrada() != null
                && !trazabilidadRadicacionActoTipo.getRadicacionesEntrada().isEmpty()) {
            RadicadoTrazabilidad radicadoTrazabilidad;
            for (RadicadoTipo radicadoTipo : trazabilidadRadicacionActoTipo.getRadicacionesEntrada()) {
                if (radicadoTipo != null) {
                    Validator.checkRadicado(radicadoTipo, errorTipoList);
                    radicadoTrazabilidad = mapper.map(radicadoTipo, RadicadoTrazabilidad.class);
                    if (StringUtils.isNotBlank(radicadoTipo.getCodRadicado())) {
                        ValorDominio codRadicado = valorDominioDao.find(radicadoTipo.getCodRadicado());
                        if (codRadicado == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "No se encontró un codRadicado con el valor :" + radicadoTipo.getCodRadicado()));
                        } else {
                            radicadoTrazabilidad.setCodRadicado(codRadicado);
                        }
                    }
                    if (StringUtils.isNotBlank(radicadoTipo.getIdRadicado())) {
                        radicadoTrazabilidad.setIdRadicado(radicadoTipo.getIdRadicado());
                    }
                    radicadoTrazabilidad.setTrazabilidadActoAdministrativo(trazabilidadActoAdministrativo);
                    radicadoTrazabilidad.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    if (radicadoTrazabilidad.getCodRadicado() != null) {
                        if (radicadoTrazabilidad.getFechaRadicado() != null && StringUtils.isNotBlank(String.valueOf(radicadoTrazabilidad.getCodRadicado()))
                                && StringUtils.isNotBlank(radicadoTrazabilidad.getIdRadicado())) {
                            if (trazabilidadActoAdministrativo.getId() != null) {
                                RadicadoTrazabilidad radicadoTrazabilidadUk = radicadoTrazabilidadDao.findByRadicadonAndTrazabilidad(trazabilidadActoAdministrativo, radicadoTrazabilidad.getIdRadicado(), radicadoTrazabilidad.getFechaRadicado(), radicadoTrazabilidad.getCodRadicado());
                                if (radicadoTrazabilidadUk != null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "Ya existe la relación entre la trazabilidad: " + trazabilidadActoAdministrativo.getId() + ", y la radicación con el ID:" + radicadoTrazabilidadUk.getId()));
                                }
                            }
                        }
                    }
                    trazabilidadActoAdministrativo.getRadicacionesEntrada().add(radicadoTrazabilidad);
                }
            }
        }
    }
}
