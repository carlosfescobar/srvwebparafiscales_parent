package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.dto.CellComponents;
import co.gov.ugpp.parafiscales.enums.ConfigExcelDataEnum;
import co.gov.ugpp.parafiscales.enums.DocTypeStructureEnum;
import co.gov.ugpp.parafiscales.enums.SheetRequiredEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ExcelUtil;
import co.gov.ugpp.parafiscales.util.FileStructureUtili;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.trasnversales.validacionarchivotipo.v1.ValidacionArchivoTipo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory; 

/**
 * implementación de la interfaz ValidadorFacade que contiene las operaciones
 * del servicio SrvAplValidador
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class ValidacionFacadeImpl extends AbstractFacade implements ValidadorFacade {

    private static final Logger LOG = LoggerFactory.getLogger(ValidacionFacadeImpl.class);

    @Override
    public ValidacionArchivoTipo validarFormatoArchivo(ContextoTransaccionalTipo contextoTransaccionalTipo, ArchivoTipo archivoTipo) throws AppException {
        if (archivoTipo == null) {
            throw new AppException("Debe enviar el archivo a validar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        final List<ParametroValoresTipo> docTypeStructure = new ArrayList<ParametroValoresTipo>();
        final List<ParametroValoresTipo> docTypeStructureMigracion = new ArrayList<ParametroValoresTipo>();
        this.validarEstructuraArchivo(archivoTipo, errorTipoList, docTypeStructure, docTypeStructureMigracion);
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        ValidacionArchivoTipo validacionArchivoTipo = new ValidacionArchivoTipo();
        validacionArchivoTipo.getEstrcuturaDocumentosMigracion().addAll(docTypeStructureMigracion);
        validacionArchivoTipo.getEstructuraDocumentos().addAll(docTypeStructure);
        return validacionArchivoTipo;
    }

    /**
     * Método que valida la estructura del archivo
     *
     * @param archivoTipo el archivo a validar
     * @throws AppException
     */
    private void validarEstructuraArchivo(final ArchivoTipo archivoTipo, final List<ErrorTipo> errorTipoList, final List<ParametroValoresTipo> docTypeStructure,
            final List<ParametroValoresTipo> docTypeStructureMigracion) throws AppException {
        try {
            Workbook libro = WorkbookFactory.create(new ByteArrayInputStream(archivoTipo.getValContenidoArchivo()));
            if (libro == null) {
                throw new AppException("No se ha procesado el archivo enviado ya que no contiene datos");
            }
            LOG.info("..:: SE INICIA A CARGAR LA CONFIGURACION DE CELDAS ::..");
            Map<Integer, CellComponents> configuracionCeldas = this.cargarConfiguracionCeldas(libro.getSheetAt(Constants.INDEX_SHEET_REQUIRED));
            LOG.info("..:: SE FINALIZA A CARGAR LA CONFIGURACION DE CELDAS ::..");
            if (configuracionCeldas == null || configuracionCeldas.isEmpty()) {
                throw new AppException("No se ha podido cargar la configuración de las celdas");
            }
            //Se carga la configuración de la estructura de los docTypes
            Map<Integer, String> configuracionEstructuraDocTypes = this.cargarConfiguracionDocTypeStructure(libro.getSheetAt(Constants.INDEX_SHEET_STRUCTURE), Constants.FIRST_INDEX_ROW_DOC_TYPE_SHEET);
            if (configuracionEstructuraDocTypes == null || configuracionEstructuraDocTypes.isEmpty()) {
                throw new AppException("No se ha cargado la configuración de la estrucutura de docTypes");
            }
            final String docTypeName = ExcelUtil.obtenerDatoHoja(libro.getSheetAt(Constants.INDEX_SHEET_STRUCTURE).getRow(Constants.ROW_INDEX_DOC_TYPE_NAME), Constants.FIRST_INDEX_COLUMN_DOC_TYPE_SHEET);
            //Se carga la configuración de la estructura de los docTypes de migración (Puede no necesitarse)
            Map<Integer, String> configuracionEstructuraDocTypesMigracion = this.cargarConfiguracionDocTypeStructure(libro.getSheetAt(Constants.INDEX_SHEET_MIGRATION_STRUCTURE), Constants.FIRST_INDEX_ROW_DOC_TYPE_MIGRACION_SHEET);
            LOG.info("..:: SE INICIA LA VALIDACION DE LOS DATOS INGRESADOS POR EL USUARIO ::..");
            Iterator<Row> rows = libro.getSheetAt(Constants.INDEX_SHEET_USER).rowIterator();
            if (!rows.hasNext()) {
                throw new AppException("No se ha procesado la hoja de datos del usuario ya que no contiene datos");
            }
            Row firstRow = (Row) rows.next();
            int lastColumnIndex = ExcelUtil.determinarIndexColumnaFinal(firstRow, Constants.FIRST_INDEX_COLUMN_USER_SHEET);
            Map<Integer, String> encabezados = ExcelUtil.obtenerNombreEncabezados(firstRow, Constants.FIRST_INDEX_COLUMN_USER_SHEET, lastColumnIndex);
            while (rows.hasNext()) {
                Row row = (Row) rows.next();
                ParametroValoresTipo parametroValoresTipoEstructuraDocumento = new ParametroValoresTipo();
                parametroValoresTipoEstructuraDocumento.setIdLlave(docTypeName);
                ParametroValoresTipo parametroValoresTipoEstructuraDocumentoMigracion = new ParametroValoresTipo();
                if (ExcelUtil.evaluarFila(row, Constants.FIRST_INDEX_COLUMN_USER_SHEET, lastColumnIndex)) {
                    boolean loadedConfig = false;
                    for (Entry<Integer, CellComponents> entry : configuracionCeldas.entrySet()) {
                        Cell cell = row.getCell(entry.getKey());
                        if (entry.getValue() != null) {
                            if (entry.getValue().isRequired()) {
                                if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
                                    if (encabezados != null && !encabezados.isEmpty()) {
                                        errorTipoList.add(FileStructureUtili.crearErrorArchivoRequerido(row.getRowNum(), encabezados.get(entry.getKey())));
                                    } else {
                                        errorTipoList.add(FileStructureUtili.crearErrorArchivo(entry.getKey()));
                                    }
                                }
                            }
                            if (cell != null && entry.getValue().getDatatType() != cell.getCellType()) {
                                if (encabezados != null && !encabezados.isEmpty()) {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivoNoDataType(row.getRowNum(), encabezados.get(entry.getKey())));
                                } else {
                                    errorTipoList.add(FileStructureUtili.crearErrorArchivo(row.getRowNum(), entry.getKey()));
                                }
                            }
                            //Se obtiene el path de la estructura del docType
                            if (cell != null) {
                                String path = configuracionEstructuraDocTypes.get(entry.getKey());
                                if (StringUtils.isNotBlank(path)) {
                                    parametroValoresTipoEstructuraDocumento.getValValor().add(this.buildDocTypeStructure(cell, path, entry.getValue()));
                                }
                                if (configuracionEstructuraDocTypesMigracion != null && !configuracionEstructuraDocTypesMigracion.isEmpty()) {
                                    path = configuracionEstructuraDocTypesMigracion.get(entry.getKey());
                                    if (StringUtils.isNotBlank(path)) {
                                        parametroValoresTipoEstructuraDocumentoMigracion.getValValor().add(this.buildDocTypeStructure(cell, path, null));
                                    }
                                }
                                if (!loadedConfig) {
                                    if (configuracionEstructuraDocTypesMigracion != null && !configuracionEstructuraDocTypesMigracion.isEmpty()) {
                                        path = configuracionEstructuraDocTypesMigracion.get(ConfigExcelDataEnum.ROW_ID.getIndex());
                                        if (StringUtils.isNotBlank(path)) {
                                            parametroValoresTipoEstructuraDocumentoMigracion.getValValor().add(this.buildConfigurationDocTypeStructure(row, cell, path, ConfigExcelDataEnum.ROW_ID));
                                        }
                                    }
                                    path = configuracionEstructuraDocTypes.get(ConfigExcelDataEnum.ROW_ID.getIndex());
                                    if (StringUtils.isNotBlank(path)) {
                                        parametroValoresTipoEstructuraDocumento.getValValor().add(this.buildConfigurationDocTypeStructure(row, cell, path, ConfigExcelDataEnum.ROW_ID));
                                    }
                                    loadedConfig = true;
                                }
                            }
                        }
                    }
                    if (parametroValoresTipoEstructuraDocumento.getValValor() != null
                            && !parametroValoresTipoEstructuraDocumento.getValValor().isEmpty()) {
                        docTypeStructure.add(parametroValoresTipoEstructuraDocumento);
                    }
                    if (parametroValoresTipoEstructuraDocumentoMigracion.getValValor() != null
                            && !parametroValoresTipoEstructuraDocumentoMigracion.getValValor().isEmpty()) {
                        docTypeStructureMigracion.add(parametroValoresTipoEstructuraDocumentoMigracion);
                    }
                }
            }
            LOG.info("..:: SE FINALIZA LA VALIDACION DE LOS DATOS INGRESADOS POR EL USUARIO ::..");

        } catch (IOException ioe) {
            LOG.error("Error validando el archivo con el error: " + ioe.getMessage());
            throw new AppException(ioe.getMessage());
        } catch (InvalidFormatException ex) {
            LOG.error("Error validando el archivo con el error: " + ex.getMessage());
            throw new AppException(ex.getMessage());
        }
    }

    /**
     * Método que arma la estructura del docType en un objeto
     * parametroValoresTipe
     *
     * @param cell la celda que contiene la información
     * @param path la ruta del atributo en el docType
     * @param docTypeName el nombre canónico del docTypé
     * @return el objeto parametroValoresTipo construido con la ruta y el valor
     * del docType
     */
    private ParametroTipo buildDocTypeStructure(final Cell cell, final String path, final CellComponents cellComponents) {
        ParametroTipo parametroTipo = new ParametroTipo();
        parametroTipo.setIdLlave(path);
        if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
            parametroTipo.setValValor(cell.getStringCellValue());
        } else if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
            if (cellComponents != null && cellComponents.isDate()) {
                parametroTipo.setValValor(DateUtil.parseDateToStringTHoraMinutoSegundo(cell.getDateCellValue()));
            } else {
                parametroTipo.setValValor(String.valueOf(Double.valueOf(cell.getNumericCellValue()).longValue()));
            }
        }
        return parametroTipo;
    }

    /**
     * Método que arma la estructura del docType en un objeto
     * parametroValoresTipe
     *
     * @param cell la celda que contiene la información
     * @param path la ruta del atributo en el docType
     * @param docTypeName el nombre canónico del docTypé
     * @param row la fila que contiene información
     * @return el objeto parametroValoresTipo construido con la ruta y el valor
     * del docType
     */
    private ParametroTipo buildConfigurationDocTypeStructure(final Row row, final Cell cell, final String path, final ConfigExcelDataEnum configExcelDataEnum) {
        ParametroTipo parametroTipo = new ParametroTipo();
        parametroTipo.setIdLlave(path);
        if (ConfigExcelDataEnum.ROW_ID.getIndex() == configExcelDataEnum.getIndex()) {
            parametroTipo.setValValor(String.valueOf(row.getRowNum() + Constants.ROW_VALUE));
        }
        return parametroTipo;
    }

    /**
     * Método que obtiene la configuración de la estructura de los docTypes
     *
     * @param hojaTrabajo la hoja de trabajo
     * @param firstIndexRow el index de la primera fila desde donde se inicia a
     * leer la configuración
     * @return la configuración de la estructura de los docTypes
     * @throws AppException
     */
    private Map<Integer, String> cargarConfiguracionDocTypeStructure(final Sheet hojaTrabajo, final int firstIndexRow) throws AppException {
        Map<Integer, String> configuracionCeldas = null;
        if (hojaTrabajo != null) {
            configuracionCeldas = new HashMap<Integer, String>();
            Iterator<Row> rows = hojaTrabajo.rowIterator();
            if (!rows.hasNext()) {
                throw new AppException("No se ha podido leer la configuración los DocTypes ya que la hoja de trabajo no tiene datos");
            }
            while (rows.hasNext()) {
                //Se obtiene la fila
                Row row = (Row) rows.next();
                if (ExcelUtil.evaluarFila(row, Constants.FIRST_INDEX_COLUMN_DOC_TYPE_SHEET, Constants.LAST_INDEX_COLUMN_DOC_TYPE_SHEET)) {
                    if (row.getRowNum() >= firstIndexRow) {
                        Iterator cells = row.cellIterator();
                        Integer columnIndex = null;
                        String path = null;
                        while (cells.hasNext()) {
                            Cell cell = (Cell) cells.next();
                            if (DocTypeStructureEnum.COLUMN_INDEX.getIndex() == cell.getColumnIndex()) {
                                columnIndex = Double.valueOf(cell.getNumericCellValue()).intValue();
                            } else if (DocTypeStructureEnum.PATH.getIndex() == cell.getColumnIndex()) {
                                path = cell.getStringCellValue();
                            }
                        }
                        configuracionCeldas.put(columnIndex, path);
                    }
                }
            }
        }

        return configuracionCeldas;
    }

    /**
     * Método que carga la configuración de las celdas requeridas
     *
     * @param hojaTrabajo la hoja de trabajo
     * @return mapa que contiene el index de la celda y su configuración
     * @throws AppException
     */
    private Map<Integer, CellComponents> cargarConfiguracionCeldas(final Sheet hojaTrabajo) throws AppException {
        Map<Integer, CellComponents> configuracionCeldas = null;
        if (hojaTrabajo != null) {
            configuracionCeldas = new HashMap<Integer, CellComponents>();
            Iterator<Row> rows = hojaTrabajo.rowIterator();
            if (!rows.hasNext()) {
                throw new AppException("No se ha podido leer la configuración de las celdas requeridas ya que la hoja de trabajo no tiene datos");
            }
            while (rows.hasNext()) {
                //Se obtiene la fila
                Row row = (Row) rows.next();
                if (ExcelUtil.evaluarFila(row, Constants.FIRST_INDEX_COLUMN_SHEET_REQUIRED, Constants.LAST_INDEX_COLUMN_SHEET_REQUIRED)) {
                    if (row.getRowNum() >= Constants.FIRST_INDEX_ROW_SHEET_REQUIRED) {
                        Iterator cells = row.cellIterator();
                        Integer columnIndex = null;
                        //Objeto que contiene la configuración de la celda
                        CellComponents cellComponents = new CellComponents();
                        while (cells.hasNext()) {
                            Cell cell = (Cell) cells.next();
                            if (SheetRequiredEnum.COLUMN_INDEX.getIndex() == cell.getColumnIndex()) {
                                columnIndex = Double.valueOf(cell.getNumericCellValue()).intValue();
                            } else if (SheetRequiredEnum.DATA_TYPE.getIndex() == cell.getColumnIndex()) {
                                cellComponents.setDatatType(cell.getCellType());
                            } else if (SheetRequiredEnum.REQUIRED.getIndex() == cell.getColumnIndex()) {
                                final String required = cell.getStringCellValue();
                                cellComponents.setRequired(Boolean.valueOf(required));
                            } else if (SheetRequiredEnum.IS_DATE.getIndex() == cell.getColumnIndex()) {
                                final String date = cell.getStringCellValue();
                                cellComponents.setDate(Boolean.valueOf(date));
                            }
                        }
                        configuracionCeldas.put(columnIndex, cellComponents);
                    }
                }
            }
        }
        return configuracionCeldas;
    }
}
