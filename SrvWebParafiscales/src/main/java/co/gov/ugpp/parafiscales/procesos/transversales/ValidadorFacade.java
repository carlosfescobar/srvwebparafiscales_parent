package co.gov.ugpp.parafiscales.procesos.transversales;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.trasnversales.validacionarchivotipo.v1.ValidacionArchivoTipo;
import java.io.Serializable;

/**
 *
 * @author jmunocab
 */
public interface ValidadorFacade extends Serializable {
    
    public ValidacionArchivoTipo validarFormatoArchivo (final ContextoTransaccionalTipo contextoTransaccionalTipo, final ArchivoTipo archivoTipo) throws AppException;

}
