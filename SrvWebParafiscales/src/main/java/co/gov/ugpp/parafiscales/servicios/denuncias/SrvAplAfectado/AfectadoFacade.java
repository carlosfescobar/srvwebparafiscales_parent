package co.gov.ugpp.parafiscales.servicios.denuncias.SrvAplAfectado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author lquintec
 */
public interface AfectadoFacade extends Serializable{
   
    public PagerData<AfectadoTipo> buscarPorCriteriosAfectado(List<ParametroTipo> parametroTipoList,
        List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    public List <AfectadoTipo> buscarPorIdAfectado(List<IdentificacionTipo> identificacionTiposList,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarAfectado(AfectadoTipo afectadoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException ;

    public void crearAfectado(AfectadoTipo afectadoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException ;
}
