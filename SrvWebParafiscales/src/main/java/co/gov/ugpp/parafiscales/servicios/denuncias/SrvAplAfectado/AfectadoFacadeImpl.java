package co.gov.ugpp.parafiscales.servicios.denuncias.SrvAplAfectado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AfectadoDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.entity.Afectado;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * implementación de la interfaz AfectadoFacade que contiene las operaciones del
 * servicio SrvAplAfectado
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class AfectadoFacadeImpl extends AbstractFacade implements AfectadoFacade {

    private static final Logger LOG = LoggerFactory.getLogger(AfectadoFacadeImpl.class);

    @EJB
    private AfectadoDao afectadoDao;
    @EJB
    private PersonaDao personaDao;
    @EJB
    private Populator populator;

    /**
     * implementación de la operación buscarPorCriteriosAfectado esta se encarga
     * de buscar un afectado por medio de los parametros enviado y ordenar la
     * consulta de acuerdo a unos criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar un Afectado
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de afectados encontrados que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<AfectadoTipo> buscarPorCriteriosAfectado(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Afectado> afectadoPagerData = afectadoDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<AfectadoTipo> afecatdoTipoList = mapper.map(afectadoPagerData.getData(), Afectado.class, AfectadoTipo.class);

        return new PagerData(afecatdoTipoList, afectadoPagerData.getNumPages());
    }

    /**
     * Impelmentacion de la operacion que busca por identificación un afectado
     *
     * @param identificacionTiposList listado de identificaciones para buscar
     * afectados
     * @param contextoTransaccionalTipo
     * @return
     * @throws AppException
     */
    @Override
    public List<AfectadoTipo> buscarPorIdAfectado(List<IdentificacionTipo> identificacionTiposList, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (identificacionTiposList == null || identificacionTiposList.isEmpty()) {
            throw new AppException("Debe proporcionar una lista de identificaciones");
        }

        final List<Identificacion> identificacionList
                = mapper.map(identificacionTiposList, IdentificacionTipo.class, Identificacion.class);

        final List<Afectado> afectadoList = afectadoDao.findByIdentificacionListLeftJoin(identificacionList);

        final List<AfectadoTipo> afectadoTipoList = mapper.map(afectadoList, Afectado.class, AfectadoTipo.class);

        return afectadoTipoList;
    }

    @Override
    public void actualizarAfectado(AfectadoTipo afectadoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (afectadoTipo == null) {
            throw new AppException("Debe proporcionar el objeto que desea actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Identificacion identificacion = null;
        if (afectadoTipo.getPersonaJuridica() != null) {
            identificacion = mapper.map(afectadoTipo.getPersonaJuridica().getIdPersona(), Identificacion.class);
        } else if (afectadoTipo.getPersonaNatural() != null) {
            identificacion = mapper.map(afectadoTipo.getPersonaNatural().getIdPersona(), Identificacion.class);
        }
        if (identificacion == null) {
            throw new AppException("Debe proporcionar la identificación del Afectado que desea actualizar");
        }
        //Se busca que el afectado esté en la tabla AFECTADO
        Afectado afectado = afectadoDao.findByIdentificacion(identificacion);
        if (afectado == null) {
            //Si no existe se busca en la tabla persona
            Persona persona = personaDao.findByIdentificacion(identificacion);
            if (persona == null) {
                throw new AppException("No se encontró un afectado con los datos enviados" + identificacion.toString());
            }
            afectado = new Afectado(persona);
            afectado.setId(persona.getId());
            afectadoDao.create(afectado);
        }

        Persona persona = afectado.getPersona();

        if (afectadoTipo.getPersonaJuridica() != null) {
            persona = populator.poupulatePersonaFromPersonaJuridica(afectadoTipo.getPersonaJuridica(), contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_JURIDICA, errorTipoList);
        } else if (afectadoTipo.getPersonaNatural() != null) {
            persona = populator.populatePersonaFromPersonaNatural(afectadoTipo.getPersonaNatural(), contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_NATURAL, errorTipoList);
        }

        this.populateAfectado(contextoTransaccionalTipo, afectadoTipo, afectado, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        afectado.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        afectado.setPersona(persona);
        afectado.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        afectadoDao.edit(afectado);
    }

    @Override
    public void crearAfectado(AfectadoTipo afectadoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (afectadoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        IdentificacionTipo identificacionTipo;

        if (afectadoTipo.getPersonaJuridica() != null) {
            identificacionTipo = afectadoTipo.getPersonaJuridica().getIdPersona();
        } else if (afectadoTipo.getPersonaNatural() != null) {
            identificacionTipo = afectadoTipo.getPersonaNatural().getIdPersona();
        } else {
            throw new AppException("Debe proporcionar la identificación del afectado a crear");
        }

        Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

        Afectado afectado = afectadoDao.findByIdentificacion(identificacion);

        if (afectado != null) {
            LOG.info("Ya existe el afectado con la identificación: " + identificacion);
            return;
        }

        Persona persona = personaDao.findByIdentificacion(identificacion);

        if (persona == null) {
            persona = new Persona();
            if (afectadoTipo.getPersonaNatural() != null) {
                Validator.checkPersonaNatural(afectadoTipo.getPersonaNatural(), errorTipoList);
                persona = populator.populatePersonaFromPersonaNatural(afectadoTipo.getPersonaNatural(), contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_NATURAL, errorTipoList);
            } else if (afectadoTipo.getPersonaJuridica() != null) {
                Validator.checkPersonaJuridica(afectadoTipo.getPersonaJuridica(), errorTipoList);
                persona = populator.poupulatePersonaFromPersonaJuridica(afectadoTipo.getPersonaJuridica(), contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_JURIDICA, errorTipoList);
            }

            persona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            Long idPersona = personaDao.create(persona);
            persona = personaDao.find(idPersona);
        }

        afectado = new Afectado(persona);
        afectado.setId(persona.getId());

        this.populateAfectado(contextoTransaccionalTipo, afectadoTipo, afectado, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        afectado.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        afectado.setFechaCreacion(DateUtil.currentCalendar());

        afectadoDao.create(afectado);
    }

    /**
     * Método que llena al afectado a partir del afectadoTipo
     *
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param afectadoTipo el objeto origen
     * @param afectado el objeto destino
     * @param errorTipoList listado de errores de la operación
     * @throws AppException
     */
    private void populateAfectado(final ContextoTransaccionalTipo contextoTransaccionalTipo, final AfectadoTipo afectadoTipo,
            final Afectado afectado, final List<ErrorTipo> errorTipoList) throws AppException {

        if (afectadoTipo.getFecIngresoEmpresa() != null) {
            afectado.setFecIngresoEmpresa(afectadoTipo.getFecIngresoEmpresa());
        }
        if (afectadoTipo.getValIBC() != null) {
            afectado.setvalIBC(afectadoTipo.getValIBC());
        }
    }
}
