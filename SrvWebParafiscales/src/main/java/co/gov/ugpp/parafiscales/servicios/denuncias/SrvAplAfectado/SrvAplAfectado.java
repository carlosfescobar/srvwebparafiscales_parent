/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.servicios.denuncias.SrvAplAfectado;

import co.gov.ugpp.denuncias.srvaplafectado.v1.MsjOpActualizarAfectadoFallo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.MsjOpBuscarPorCriteriosAfectadoFallo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.MsjOpBuscarPorIdAfectadoFallo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.MsjOpCrearAfectadoFallo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.OpActualizarAfectadoRespTipo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorCriteriosAfectadoRespTipo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorIdAfectadoRespTipo;
import co.gov.ugpp.denuncias.srvaplafectado.v1.OpCrearAfectadoRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author zrodrigu
 */
@WebService(serviceName = "SrvAplAfectado",
        portName = "portSrvAplAfectadoSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplafectado.v1.PortSrvAplAfectadoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplAfectado/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAfectado extends AbstractSrvApl {

    private static final Logger LOG = LoggerFactory.getLogger(SrvAplAfectado.class);
    @EJB
    private AfectadoFacade afectadoFacade;

    public co.gov.ugpp.denuncias.srvaplafectado.v1.OpCrearAfectadoRespTipo opCrearAfectado(co.gov.ugpp.denuncias.srvaplafectado.v1.OpCrearAfectadoSolTipo msjOpCrearAfectadoSol) throws MsjOpCrearAfectadoFallo {

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearAfectadoSol.getContextoTransaccional();

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            afectadoFacade.crearAfectado(msjOpCrearAfectadoSol.getAfectado(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpCrearAfectadoRespTipo crearAfectadoRespTipo = new OpCrearAfectadoRespTipo();
        crearAfectadoRespTipo.setContextoRespuesta(contextoRespuesta);
        return crearAfectadoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplafectado.v1.OpActualizarAfectadoRespTipo opActualizarAfectado(co.gov.ugpp.denuncias.srvaplafectado.v1.OpActualizarAfectadoSolTipo msjOpActualizarAfectadoSol) throws MsjOpActualizarAfectadoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarAfectadoSol.getContextoTransaccional();
        final AfectadoTipo afectadoTipo = msjOpActualizarAfectadoSol.getAfectado();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            afectadoFacade.actualizarAfectado(afectadoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarAfectadoRespTipo resp = new OpActualizarAfectadoRespTipo();
        resp.setContextoRespuesta(cr);

        return resp;
    }

    public co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorIdAfectadoRespTipo opBuscarPorIdAfectado(co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorIdAfectadoSolTipo msjOpBuscarPorIdAfectadoSol) throws MsjOpBuscarPorIdAfectadoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorIdAfectadoSol.getContextoTransaccional();

        final List<IdentificacionTipo> identificacionTipos = msjOpBuscarPorIdAfectadoSol.getIdentificacion();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        final List<AfectadoTipo> afectadoTipos;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            afectadoTipos = afectadoFacade.buscarPorIdAfectado(identificacionTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorIdAfectadoRespTipo resp = new OpBuscarPorIdAfectadoRespTipo();

        resp.setContextoRespuesta(cr);
        resp.getAfectado().addAll(afectadoTipos);
        return resp;
    }

    public co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorCriteriosAfectadoRespTipo opBuscarPorCriteriosAfectado(co.gov.ugpp.denuncias.srvaplafectado.v1.OpBuscarPorCriteriosAfectadoSolTipo msjOpBuscarPorCriteriosAfectadoSol) throws MsjOpBuscarPorCriteriosAfectadoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosAfectadoSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipo = msjOpBuscarPorCriteriosAfectadoSol.getParametro();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<AfectadoTipo> pagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            pagerData = afectadoFacade.buscarPorCriteriosAfectado(parametroTipo,
                    contextoTransaccionalTipo.getCriteriosOrdenamiento(), contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosAfectadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }
        OpBuscarPorCriteriosAfectadoRespTipo buscarPorCriteriosAfectadoRespTipo = new OpBuscarPorCriteriosAfectadoRespTipo();
        cr.setValCantidadPaginas(pagerData.getNumPages());
        buscarPorCriteriosAfectadoRespTipo.setContextoRespuesta(cr);
        buscarPorCriteriosAfectadoRespTipo.getAfectado().addAll(pagerData.getData());

        return buscarPorCriteriosAfectadoRespTipo;
    }

}
