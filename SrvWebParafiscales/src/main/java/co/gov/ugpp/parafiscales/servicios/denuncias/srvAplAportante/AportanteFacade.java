package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplAportante;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface AportanteFacade extends Serializable{
    
    /**
     * Operacion que a partir de unos parametros de busqueda obtiene los
     * datos registrados del Aportante en las BD de la UGPP. Al enviar el tipo y
     * numero de identificacion se debera retornar un solo Aportante y al no
     * enviar parametros debera retornar todos los Aportantes.
     * 
     * @return
     * @throws AppException 
     */
    public List<AportanteTipo> buscarPorIdAportante(IdentificacionTipo id) throws AppException;
    
    public void actualizarAportante(AportanteTipo aportante, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public Boolean analizarPagosPersuasivo(AportanteTipo aportante, CotizanteTipo cotizante, String idExpediente, RangoFechaTipo periodo)throws AppException ;

    public byte[] analizarPagosAportante(AportanteTipo aportante, DenuncianteTipo denunciante, String idExpediente, RangoFechaTipo periodo)throws AppException ;
    
}
