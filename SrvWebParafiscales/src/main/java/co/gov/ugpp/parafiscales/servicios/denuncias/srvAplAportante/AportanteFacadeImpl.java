package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplAportante;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.EquivalenciasCodigosDominioEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ClaseCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.CorreoElectronicoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DivisionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.DominioDao;
import co.gov.ugpp.parafiscales.persistence.dao.GrupoCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SeccionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.TelefonoDao;
import co.gov.ugpp.parafiscales.persistence.dao.UbicacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.ClaseCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.ClasificacionPersona;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.DivisionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Dominio;
import co.gov.ugpp.parafiscales.persistence.entity.GrupoCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.SeccionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.AportanteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.CorreoElectronicoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.MunicipioAssembler;
import co.gov.ugpp.parafiscales.servicios.util.CrearExcelAportante;
import co.gov.ugpp.parafiscales.servicios.util.SQLConection;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class AportanteFacadeImpl extends AbstractFacade implements AportanteFacade {

    @EJB
    private AportanteDao aportanteDao;
    @EJB
    private PersonaDao personaDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private MunicipioDao municipioDao;
    @EJB
    private ClaseCiiuDao claseCiiuDao;
    @EJB
    private DivisionCiiuDao divisionCiiuDao;
    @EJB
    private GrupoCiiuDao grupoCiiuDao;
    @EJB
    private SeccionCiiuDao seccionCiiuDao;
    @EJB
    private DominioDao dominioDao;
    @EJB
    private UbicacionDao ubicacionDao;
    @EJB
    private CorreoElectronicoDao correoElectronicoDao;
    @EJB
    private TelefonoDao telefonoDao;
    
    

    private SQLConection sqlConection = SQLConection.getInstance();

    AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();

    @Override
    public List<AportanteTipo> buscarPorIdAportante(IdentificacionTipo id) throws AppException {
        List<AportanteTipo> aportantes = new ArrayList<AportanteTipo>();

        if (id != null) {
            if (StringUtils.isBlank(id.getCodTipoIdentificacion()) || StringUtils.isBlank(id.getValNumeroIdentificacion())) {
                throw new AppException("Debe proporcionar los ID de los objetos a consultar");
            }
            Identificacion identificacion = mapper.map(id, Identificacion.class);
            Aportante aportante = aportanteDao.findByIdentificacion(identificacion);
            if (aportante != null) {
                aportante.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.APORTANTE);
                AportanteTipo aportanteTipo = aportanteAssembler.assembleServicio(aportante);
                aportantes.add(aportanteTipo);
            }
            /**
             * Se comenta ya que la operacion no puede crear ningun aportante
             */
//            else {
//                final Persona persona = personaDao.findByIdentificacion(identificacion);
//                if (persona == null) {
//                    //TODO Buscar en RUA RUE y PILA
//                    throw new AppException("No existe la persona a referenciar con el aportante con la identificacion enviada " + identificacion.toString());
//                }
//                aportante = new Aportante(persona);
//                aportanteDao.create(aportante);
//
//                AportanteTipo aportanteTipo = aportanteAssembler.assembleServicio(aportante);
//                aportantes.add(aportanteTipo);
//            }

        } else {
            List<Aportante> aportantesEntidad = aportanteDao.findAll();
            aportantes.addAll(aportanteAssembler.assembleCollectionServicio(aportantesEntidad));
        }
        return aportantes;

    }

    @Override
    public void actualizarAportante(AportanteTipo aportanteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

     
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        
        if (aportanteTipo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar el objeto a crear"));
            throw new AppException(errorTipoList);
        }
        
        if (aportanteTipo.getPersonaJuridica() != null){   
            if (aportanteTipo.getPersonaJuridica().getCodFuente() == null){ 
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El codigo fuente no puede ser nulo."));
                throw new AppException(errorTipoList);
            }    
        }
        else if (aportanteTipo.getPersonaNatural() != null){
            if (aportanteTipo.getPersonaNatural().getCodFuente() == null){
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El codigo fuente no puede ser nulo."));
                throw new AppException(errorTipoList);
            }        
        }
        
        
        Identificacion identificacion = null;
        String codFuenteServicio = null;

        
        if (aportanteTipo.getPersonaJuridica() != null) {
            identificacion = mapper.map(aportanteTipo.getPersonaJuridica().getIdPersona(), Identificacion.class);
            codFuenteServicio = aportanteTipo.getPersonaJuridica().getCodFuente();
        } else if (aportanteTipo.getPersonaNatural() != null) {
            identificacion = mapper.map(aportanteTipo.getPersonaNatural().getIdPersona(), Identificacion.class);
            codFuenteServicio = aportanteTipo.getPersonaNatural().getCodFuente();
        }
        if (identificacion == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Los campos estan vacios para la busqueda del aportante a actualizar"));
            throw new AppException(errorTipoList);  
        }

        Aportante aportante = aportanteDao.findByIdentificacion(identificacion);
        if (aportante == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontro una aportante a actualizar con el valor:" + identificacion.toString()));
            throw new AppException(errorTipoList);
        }
        
        
        ValorDominio codFuente = valorDominioDao.find(codFuenteServicio);
        if (codFuente == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El codigo de fuente es invalido: " + codFuenteServicio));
            throw new AppException(errorTipoList);
        }
        
        
        Persona persona = aportante.getPersona();
        
        

        assembleEntidad(persona, aportanteTipo, errorTipoList, contextoTransaccionalTipo);

 
        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        aportante.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        aportante.setFechaModificacion(DateUtil.currentCalendar());
        

        aportanteDao.edit(aportante);
    }

    
    
    
    public void assembleEntidad(final Persona persona, AportanteTipo servicio, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (servicio.getPersonaNatural() != null) {
            
            PersonaNaturalTipo personaNatural = servicio.getPersonaNatural();

            //Se coloca la nueva clasificacion persona
            if (StringUtils.isNotBlank(personaNatural.getCodTipoPersonaNatural())) {
                ValorDominio codTipoPersona = valorDominioDao.find(personaNatural.getCodTipoPersonaNatural());

                if (codTipoPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoPersona con el ID: " + personaNatural.getCodTipoPersonaNatural()));
                } else {

                    Dominio codClasificacionPersona = dominioDao.find(ClasificacionPersonaEnum.APORTANTE.getCode());

                    if (codClasificacionPersona == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró la clasificación de la persona con el ID: " + ClasificacionPersonaEnum.APORTANTE.getCode()));
                    } else {
                        ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                        clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                        clasificacionNuevaPersona.setPersona(persona);
                        clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                        persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                    }
                }
            }

            ValorDominio tipoPersona = valorDominioDao.find(TipoPersonaEnum.PERSONA_NATURAL.getCode());
            if (tipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo Persona proporcionado no existe con el ID: " + TipoPersonaEnum.PERSONA_NATURAL.getCode()));
            } else {
                persona.setCodNaturalezaPersona(tipoPersona);
            }
            if (StringUtils.isNotBlank(personaNatural.getCodFuente())) {
                ValorDominio codFuente = valorDominioDao.find(personaNatural.getCodFuente());
                if (codFuente == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codFuente proporcionado no existe con el ID: " + personaNatural.getCodFuente()));
                } else {
                    persona.setCodFuente(codFuente);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodEstadoCivil())) {
                ValorDominio codEstadoCivil = valorDominioDao.find(personaNatural.getCodEstadoCivil());
                if (codEstadoCivil == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codEstadoCivil proporcionado no existe con el ID: " + personaNatural.getCodNivelEducativo()));
                } else {
                    persona.setCodEstadoCivil(codEstadoCivil);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodNivelEducativo())) {
                ValorDominio nivelEducativo = valorDominioDao.find(personaNatural.getCodNivelEducativo());
                if (nivelEducativo == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El nivelEducativo proporcionado no existe con el ID: " + personaNatural.getCodNivelEducativo()));
                } else {
                    persona.setCodNivelEducativo(nivelEducativo);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodSexo())) {
                ValorDominio sexo = valorDominioDao.find(personaNatural.getCodSexo());
                if (sexo == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codSexo proporcionado no existe con el ID: " + personaNatural.getCodSexo()));
                } else {
                    persona.setCodSexo(sexo);
                }
            }
            
            

            if (personaNatural.getIdPersona().getMunicipioExpedicion() != null) {
                    if (personaNatural.getIdPersona().getMunicipioExpedicion().getDepartamento() != null) {
                        
                        String codMunicipio = personaNatural.getIdPersona().getMunicipioExpedicion().getCodMunicipio();
                        Municipio municipio = municipioDao.findByCodigo(codMunicipio);

                        if (municipio == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "El Municipio proporcionado para persona natural no existe con el ID: " + codMunicipio));
                        } else {
                            persona.setMunicipio(municipio);
                        }
                    }

            }



            if (personaNatural.getIdAbogado() != null) {
                if (StringUtils.isBlank(personaNatural.getIdAbogado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdAbogado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del abogado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaNatural.getIdAbogado().getCodTipoIdentificacion(),
                            personaNatural.getIdAbogado().getValNumeroIdentificacion());
                    Persona abogado = personaDao.findByIdentificacion(identificacion);
                    if (abogado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El abogado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAbogado(abogado);
                    }
                }
            }
            if (personaNatural.getIdAutorizado() != null) {
                if (StringUtils.isBlank(personaNatural.getIdAutorizado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdAutorizado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaNatural.getIdAutorizado().getCodTipoIdentificacion(),
                            personaNatural.getIdAutorizado().getValNumeroIdentificacion());
                    Persona autorizado = personaDao.findByIdentificacion(identificacion);
                    if (autorizado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El autorizado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAutorizado(autorizado);
                    }
                }
            }
            if (personaNatural.getIdPersona() != null) {
                if (StringUtils.isBlank(personaNatural.getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdPersona().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos de la persona estan vacios"));
                } else {
                    persona.setValNumeroIdentificacion(personaNatural.getIdPersona().getValNumeroIdentificacion());
                    ValorDominio tipoIdentificacion = valorDominioDao.find(personaNatural.getIdPersona().getCodTipoIdentificacion());
                    if (tipoIdentificacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El tipoIdentificacion proporcionado no existe con el ID: " + personaNatural.getIdPersona().getCodTipoIdentificacion()));
                    } else {
                        persona.setCodTipoIdentificacion(tipoIdentificacion);
                    }

                }
            }
            

            
            if (StringUtils.isNotBlank(personaNatural.getValPrimerNombre())) {
                persona.setValPrimerNombre(personaNatural.getValPrimerNombre());
            }

            if (StringUtils.isNotBlank(personaNatural.getValSegundoNombre())) {
                persona.setValSegundoNombre(personaNatural.getValSegundoNombre());
            }

            if (StringUtils.isNotBlank(personaNatural.getValPrimerApellido())) {
                persona.setValPrimerApellido(personaNatural.getValPrimerApellido());
            }

            if (StringUtils.isNotBlank(personaNatural.getValSegundoApellido())) {
                persona.setValSegundoApellido(personaNatural.getValSegundoApellido());
            }
            if (StringUtils.isNotBlank(personaNatural.getValNombreCompleto())) {
                persona.setValNombreRazonSocial(personaNatural.getValNombreCompleto());
            }
            
            
            
            
                        
            //si la persona ya tiene un contacto
            if (personaNatural.getContacto() != null) {
                persona.setContacto(assembleEntidadContactoPersona(personaNatural.getContacto(), persona.getContacto(), personaNatural.getCodFuente(), contextoTransaccionalTipo, errorTipoList));
                persona.getContacto().setPersona(persona);
            }
            
            
            

        } else if (servicio.getPersonaJuridica() != null) {

            PersonaJuridicaTipo personaJuridica = servicio.getPersonaJuridica();
            

            //Se coloca la nueva clasificacion persona
            if (StringUtils.isNotBlank(personaJuridica.getCodTipoPersonaJuridica())) {
                ValorDominio codTipoPersona = valorDominioDao.find(personaJuridica.getCodTipoPersonaJuridica());

                if (codTipoPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoPersona con el ID: " + personaJuridica.getCodTipoPersonaJuridica()));
                } else {

                    Dominio codClasificacionPersona = dominioDao.find(ClasificacionPersonaEnum.APORTANTE.getCode());

                    if (codClasificacionPersona == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró la clasificación de la persona con el ID: " + ClasificacionPersonaEnum.APORTANTE.getCode()));
                    } else {
                        ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                        clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                        clasificacionNuevaPersona.setPersona(persona);
                        clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                        persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                    }
                }
            }

            ValorDominio tipoPersona = valorDominioDao.find(TipoPersonaEnum.PERSONA_JURIDICA.getCode());
            if (tipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo Persona proporcionado no existe con el ID: " + TipoPersonaEnum.PERSONA_JURIDICA.getCode()));
            } else {
                persona.setCodNaturalezaPersona(tipoPersona);
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodFuente())) {
                ValorDominio codFuente = valorDominioDao.find(personaJuridica.getCodFuente());
                if (codFuente == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codFuente proporcionado no existe con el ID: " + personaJuridica.getCodFuente()));
                } else {
                    persona.setCodFuente(codFuente);
                }
            }
            

            if (personaJuridica.getIdAbogado() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdAbogado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdAbogado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del abogado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdAbogado().getCodTipoIdentificacion(),
                            personaJuridica.getIdAbogado().getValNumeroIdentificacion());
                    Persona abogado = personaDao.findByIdentificacion(identificacion);
                    if (abogado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El abogado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAbogado(abogado);
                    }
                }
            }
            if (personaJuridica.getIdAutorizado() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdAutorizado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdAutorizado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdAutorizado().getCodTipoIdentificacion(),
                            personaJuridica.getIdAutorizado().getValNumeroIdentificacion());
                    Persona autorizado = personaDao.findByIdentificacion(identificacion);
                    if (autorizado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El autorizado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAutorizado(autorizado);
                    }
                }
            }
            if (personaJuridica.getIdPersona() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdPersona().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos de la persona estan vacios"));
                } else {
                    persona.setValNumeroIdentificacion(personaJuridica.getIdPersona().getValNumeroIdentificacion());
                    ValorDominio tipoIdentificacion = valorDominioDao.find(personaJuridica.getIdPersona().getCodTipoIdentificacion());
                    if (tipoIdentificacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El tipoIdentificacion proporcionado no existe con el ID: " + personaJuridica.getIdPersona().getCodTipoIdentificacion()));
                    } else {
                        persona.setCodTipoIdentificacion(tipoIdentificacion);
                    }
                }
            }
            try {
                if (personaJuridica.getIdPersona().getMunicipioExpedicion() != null) {
                    if (personaJuridica.getIdPersona().getMunicipioExpedicion().getDepartamento() != null) {
                        String codMunicipio = personaJuridica.getIdPersona().getMunicipioExpedicion().getCodMunicipio();
                        Municipio municipio = municipioDao.findByCodigo(codMunicipio);
                        if (municipio == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "El Municipio proporcionado no existe con el ID: " + codMunicipio));
                        } else {
                            persona.setMunicipio(municipio);
                        }
                    }

                }

            } catch (Exception e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al parsear los codigos de departamento y municipio o al consultarlos por favor verificar los datos " + e.getMessage()));
            }
            if (personaJuridica.getIdRepresentanteLegal() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdRepresentanteLegal().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdRepresentanteLegal().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdRepresentanteLegal().getCodTipoIdentificacion(),
                            personaJuridica.getIdRepresentanteLegal().getValNumeroIdentificacion());
                    Persona representante = personaDao.findByIdentificacion(identificacion);
                    if (representante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El representante proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setRepresentanteLegal(representante);
                    }
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getValNombreRazonSocial())) {
                persona.setValNombreRazonSocial(personaJuridica.getValNombreRazonSocial());
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodClaseActividadEconomica())) {
                ClaseCiiu codClaseAE = claseCiiuDao.find(Long.parseLong(personaJuridica.getCodClaseActividadEconomica()));
                if (codClaseAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodClaseActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodClaseActividadEconomica()));
                } else {
                    persona.setCodClaseActividadEconom(codClaseAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodDivisionActividadEconomica())) {
                DivisionCiiu codDivisionAE = divisionCiiuDao.find(Long.parseLong(personaJuridica.getCodDivisionActividadEconomica()));
                if (codDivisionAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodDivisionActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodDivisionActividadEconomica()));
                } else {
                    persona.setCodDivisionActividadEco(codDivisionAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodGrupoActividadEconomica())) {
                GrupoCiiu codGrupoAE = grupoCiiuDao.find(Long.parseLong(personaJuridica.getCodGrupoActividadEconomica()));
                if (codGrupoAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodGrupoActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodGrupoActividadEconomica()));
                } else {
                    persona.setCodGrupoActividadEconom(codGrupoAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodSeccionActividadEconomica())) {
                SeccionCiiu codSeccionAE = seccionCiiuDao.find(personaJuridica.getCodSeccionActividadEconomica());
                if (codSeccionAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodSeccionActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodSeccionActividadEconomica()));
                } else {
                    persona.setCodSeccionActividadEcon(codSeccionAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getValNumTrabajadores())) {
                persona.setValNumeroTrabajadores(Long.parseLong(personaJuridica.getValNumTrabajadores()));
            }
            
            
            if (personaJuridica.getContacto() != null) {
                persona.setContacto(assembleEntidadContactoPersona(personaJuridica.getContacto(), persona.getContacto(), personaJuridica.getCodFuente(), contextoTransaccionalTipo , errorTipoList)); 
                persona.getContacto().setPersona(persona);
                
            }
            
            
            

        }

    }

    private CorreoElectronicoAssembler correoElectronicoAssembler = CorreoElectronicoAssembler.getInstance();

    
    
    
    
    public ContactoPersona assembleEntidadContactoPersona(ContactoPersonaTipo servicio, ContactoPersona contactoPersona, String codFuenteServicio, ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        
        ContactoPersona contacto = new ContactoPersona();
        ValorDominio codFuente = valorDominioDao.find(codFuenteServicio);
       
        
        if(contactoPersona != null){
           contacto.setId(contactoPersona.getId());           
           contacto.setFechaModificacion(DateUtil.currentCalendar());
           contacto.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        }
        else
        {
            contacto.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            contacto.setFechaCreacion(DateUtil.currentCalendar());
        }

        
        
        if (servicio.getEsAutorizaNotificacionElectronica() != null) {
            contacto.setEsAutorizaNotificacionElectronica(Boolean.valueOf(servicio.getEsAutorizaNotificacionElectronica()));
        }
        
        
        if (servicio.getTelefonos() != null && codFuente != null) {
            List<Telefono> listTelefono = new ArrayList<Telefono>();
            for (TelefonoTipo telefonoTipo : servicio.getTelefonos()) {
                if (telefonoTipo != null) {
                    
                    Long contador = telefonoDao.findTelefono(telefonoTipo,contactoPersona.getId(),codFuente);
                    
                    if (contador.equals(0L))
                    {
                        Telefono telefono = assembleEntidadTelefono(telefonoTipo, errorTipoList);
                        telefono.setContactoPersona(contactoPersona);
                        telefono.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        telefono.setCodFuente(codFuente);
                        listTelefono.add(telefono);
                    } 
                }
            }
            contacto.getTelefonoList().addAll(listTelefono);
        }
        
        
        if (servicio.getUbicaciones() != null) {
            contacto.getUbicacionList().addAll(assembleCollectionEntidadUbicacion(servicio.getUbicaciones(), contactoPersona, contextoTransaccionalTipo, errorTipoList)); 
        }
        
        
        
        if (servicio.getCorreosElectronicos()!= null && codFuente != null) {
            
            List<CorreoElectronicoTipo> correosDiferentes = new ArrayList<CorreoElectronicoTipo>();
            
            for (CorreoElectronicoTipo correo : servicio.getCorreosElectronicos()){

                    Long contador = correoElectronicoDao.findCorreoElectronico(String.valueOf(correo),contactoPersona.getId(),codFuente);

                    if (contador.equals(0L))
                        correosDiferentes.add(correo);
            }
            
                
            if(correosDiferentes.size()>0)
                  contacto.getCorreoElectronicoList().addAll(correoElectronicoAssembler.assembleCollectionEntidad(correosDiferentes));
            
            
             
            for (CorreoElectronico correoElectronico : contacto.getCorreoElectronicoList()) {
                    if (correoElectronico != null) {
                        correoElectronico.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        correoElectronico.setContactoPersona(contactoPersona);
                        correoElectronico.setCodFuente(codFuente);
                    }
            }
         
            
        }

        return contacto;
    }

    
    
    public Telefono assembleEntidadTelefono(TelefonoTipo servicio, final List<ErrorTipo> errorTipoList) {

        Telefono telefono = new Telefono();

        if (StringUtils.isNotBlank(servicio.getCodTipoTelefono())) {
            ValorDominio codTipoTelefono = null;
            try {
                codTipoTelefono = valorDominioDao.find(servicio.getCodTipoTelefono());
            } catch (AppException ex) {
                Logger.getLogger(AportanteFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al consultar el codTipoTelefono, ver log para mas informacion: " + servicio.getCodTipoTelefono()));
            }
            if (codTipoTelefono == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoTelefono proporcionado no existe con el ID: " + servicio.getCodTipoTelefono()));
            } else {
                telefono.setCodTipoTelefono(codTipoTelefono);
            }

        }
        if (servicio.getValNumeroTelefono() != null) {
            telefono.setValNumero(String.valueOf(servicio.getValNumeroTelefono()));
        }
        return telefono;

    }

    
    
    
    
    
    public List<Ubicacion> assembleCollectionEntidadUbicacion(Collection<UbicacionPersonaTipo> arrayL, final ContactoPersona contactoPersona, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        List<Ubicacion> collection = new ArrayList<Ubicacion>();
        for (UbicacionPersonaTipo parcial : arrayL) {

            Long contador = ubicacionDao.findUbicacion(parcial,contactoPersona.getId());
            
            if (contador.equals(0L)) 
                collection.add(assembleEntidadUbicacion(parcial,contactoPersona,contextoTransaccionalTipo,errorTipoList));     
        }
        
        return collection;
    }
    
    
    
    
    

    public Ubicacion assembleEntidadUbicacion(UbicacionPersonaTipo servicio, final ContactoPersona contactoPersona, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) {

        Ubicacion ubicacion = new Ubicacion();

        if (StringUtils.isNotBlank(servicio.getCodTipoDireccion())) {
            ValorDominio codTipoDireccion = null;
            try {
                codTipoDireccion = valorDominioDao.find(servicio.getCodTipoDireccion());
            } catch (AppException ex) {
                Logger.getLogger(AportanteFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al consultar el codTipoDireccion, ver log para mas informacion: " + servicio.getCodTipoDireccion()));
            }
            if (codTipoDireccion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoDireccion proporcionado no existe con el ID: " + servicio.getCodTipoDireccion()));
            } else {
                ubicacion.setCodTipoDireccion(codTipoDireccion);
            }
        }
        if (servicio.getMunicipio() != null) {
            ubicacion.setMunicipio(assembleEntidadMunicipio(servicio.getMunicipio(), errorTipoList));
        }
        if (StringUtils.isNotBlank(servicio.getValDireccion())) {
            ubicacion.setValDireccion(servicio.getValDireccion());
        }

        
        ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        ubicacion.setContactoPersona(contactoPersona);
        
        return ubicacion;

    }

    public Municipio assembleEntidadMunicipio(MunicipioTipo servicio, final List<ErrorTipo> errorTipoList) {

        Municipio municipio = null;
        try {
            if (servicio != null && servicio.getDepartamento() != null) {
                if (StringUtils.isNotBlank(servicio.getCodMunicipio())) {
                    municipio = municipioDao.findByCodigo(servicio.getCodMunicipio());
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Los datos del municipio son nulos idMunicipio y idDepartamento: "));
            }

            if (municipio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El municipio no existe con los datos suministrados: " + servicio.getCodMunicipio() + " " + servicio.getDepartamento().getCodDepartamento()));

            }

        } catch (Exception ex) {
            Logger.getLogger(MunicipioAssembler.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        return municipio;
    }

    @Override
    public Boolean analizarPagosPersuasivo(AportanteTipo aportante, CotizanteTipo cotizante, String idExpediente, RangoFechaTipo periodo) throws AppException {
     //   System.out.println("YESID: analizar pagos persuasivo INICIO");
        String fechaDesde = "";
        String fechaHasta = "";
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        
        
        if (aportante == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar el aportante a revizar"));
            throw new AppException(errorTipoList);
        }
        Identificacion identificacion = null;
     // System.out.println("YESID: Inicia 1 en analizarPagosPersuasivo");      
        if (aportante.getPersonaNatural() != null) {
            identificacion = mapper.map(aportante.getPersonaNatural().getIdPersona(), Identificacion.class);
        } else if (aportante.getPersonaJuridica() != null) {
            identificacion = mapper.map(aportante.getPersonaJuridica().getIdPersona(), Identificacion.class);
        }
   //  System.out.println("YESID: Inicia 2 en analizarPagosPersuasivo");
        /*
        Aportante verificarAportante = aportanteDao.findByIdentificacion(identificacion);
        if (verificarAportante == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontro un aportante con el valor:" + identificacion));
            throw new AppException(errorTipoList);
        }
        */
 
    String Nidentificacion ="";
    String Tidentificacion="";          
    String tipoIdentificacionCOT = null;
   //System.out.println("YESID: Inicia try  en analizarPagosPersuasivo");
    try 
    {
       
        
        if (cotizante.getIdPersona().getValNumeroIdentificacion() != null && cotizante.getIdPersona().getCodTipoIdentificacion()  != null) 
        {
                Nidentificacion = cotizante.getIdPersona().getValNumeroIdentificacion().trim();
                Tidentificacion = cotizante.getIdPersona().getCodTipoIdentificacion().trim();
                
                
 //System.out.println("YESID: entre cotizante");

                /*Toco quemar el codigo de los tipos de documentos porque no hay una tabla
               especifica donde este la equivalencia, o una columna en dominio que identifique*/
               for(EquivalenciasCodigosDominioEnum equivalencia: EquivalenciasCodigosDominioEnum.values()){ 
                   if(equivalencia.getCodigoDominio().equals(Tidentificacion))
                       tipoIdentificacionCOT = equivalencia.getEquivalencia();
               }

         /*     if(tipoIdentificacionCOT == null){
                       errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                           "Tipo de documento Cotizante invalido"));
                           throw new AppException(errorTipoList);
               }
*/
               if( Nidentificacion.length()>1 && Tidentificacion.length()>1 )
               {
                   
               }
               else
               {
                    Nidentificacion = "NO";
                    Tidentificacion = "NO";
                    tipoIdentificacionCOT = "NO";
               }
        }
        else
        {
            Nidentificacion="NO";
            tipoIdentificacionCOT="NO";
            
            //System.out.println("YESID: El aportante esta nulo ");    
        }
            
   
      } catch (Exception e) {
          /*  errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                    "Hubo un error verifique cotizante."));
            throw new AppException(errorTipoList);
            */   
            
            Nidentificacion="NO";
            tipoIdentificacionCOT="NO";
            
            System.out.println("ERROR: " + e.toString());
            
        }   
   // System.out.println("YESID: Inicia try  1 en analizarPagosPersuasivo");
    // 2012-12-01
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
     try {
            
    
            Date fecha1= periodo.getFecInicio().getTime();
            fechaDesde =formatter.format(fecha1);             

            Date fecha2= periodo.getFecFin().getTime();
            fechaHasta =formatter.format(fecha2); 
     
        } catch (Exception e) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                    "Hubo un error al intentar parsear las fechas verifique que este incluyendo un fecha."));
            throw new AppException(errorTipoList);
            
        }
       // System.out.println("YESID: Inicia 2 en analizarPagosPersuasivo");
        if (StringUtils.isBlank(fechaDesde) || StringUtils.isBlank(fechaHasta)){
            throw new AppException("Las fechas son parametros obligatorios para realizar la consulta y vienen nulos o estan vacios");
        }
        String tipoIdentificacion = null;

        //   System.out.println("YESID: Inicia 3  en analizarPagosPersuasivo");
        for(EquivalenciasCodigosDominioEnum equivalencia: EquivalenciasCodigosDominioEnum.values()){ 
            if(equivalencia.getCodigoDominio().equals(identificacion.getCodTipoIdentificacion()))
                tipoIdentificacion = equivalencia.getEquivalencia();
        }
        
        if(tipoIdentificacion == null){
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                    "Tipo de documento invalido"));
                    throw new AppException(errorTipoList);
        }
   
 
        Boolean esPago = false;
       
        
        try {
            /*
          System.out.println("YESID: analizar pagos persuasivo DATOS tipoidentificacion = "+tipoIdentificacion);
          System.out.println("YESID: analizar pagos persuasivo DATOS getValNumeroIdentificacion = "+identificacion.getValNumeroIdentificacion());
          System.out.println("YESID: analizar pagos persuasivo DATOS tipoIdentificacionCOT = "+tipoIdentificacionCOT);
          System.out.println("YESID: analizar pagos persuasivo DATOS Nidentificacion = "+Nidentificacion);
          System.out.println("YESID: analizar pagos persuasivo DATOS fechaDesde = "+fechaDesde);
          System.out.println("YESID: analizar pagos persuasivo DATOS fechaHasta = "+fechaHasta);
          */
              esPago = sqlConection.analisisPagosAportanteNV(tipoIdentificacion, identificacion.getValNumeroIdentificacion(),  tipoIdentificacionCOT, Nidentificacion ,fechaDesde, fechaHasta);
          
            
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }
        //System.out.println("YESID: analizar pagos persuasivo FINAL espago = "+esPago);
        return esPago;
    }

    @Override
    public byte[] analizarPagosAportante(AportanteTipo aportante, DenuncianteTipo denunciante, String idExpediente, RangoFechaTipo periodo) throws AppException {
        //CODIGO OP004
        String tipoIdentificacion = "";
        String numIdentificacion = "";
        String fechaDesde = "";
        String fechaHasta = "";

        
        
        
        
        if (aportante.getPersonaNatural() == null) {
            tipoIdentificacion = aportante.getPersonaJuridica().getIdPersona().getCodTipoIdentificacion();
            numIdentificacion = aportante.getPersonaJuridica().getIdPersona().getValNumeroIdentificacion();
        } else if (aportante.getPersonaJuridica() == null) {
            tipoIdentificacion = aportante.getPersonaNatural().getIdPersona().getCodTipoIdentificacion();
            numIdentificacion = aportante.getPersonaNatural().getIdPersona().getValNumeroIdentificacion();
        }

         Identificacion identificacionDEN = null;

        identificacionDEN = mapper.map(denunciante.getPersonaNatural().getIdPersona(), Identificacion.class);
          
        if(identificacionDEN.getCodTipoIdentificacion() == null ||  identificacionDEN.getValNumeroIdentificacion() ==null){
                //errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                 //   "Tipo de documento o numero documento denunciante invalido"));
                    throw new AppException("Tipo de documento o numero documento denunciante invalido");
        }
        
        
        fechaDesde = periodo.getFecInicio().toString();
        fechaHasta = periodo.getFecFin().toString();
/*
          System.out.println("YESID: aportantetipo cot tipo= "+ tipoIdentificacion);
        System.out.println("YESID: numero identificacion = "+ numIdentificacion);
        System.out.println("YESID: tipo identificaion  DEN = "+ identificacionDEN.getCodTipoIdentificacion());
        System.out.println("YESID: numero identificaion  DEN = "+ identificacionDEN.getValNumeroIdentificacion());
        System.out.println("YESID: fecha desde = "+ fechaDesde);
        System.out.println("YESID: fecha hasta = "+ fechaHasta);
        
        */
        if (tipoIdentificacion == null || tipoIdentificacion.isEmpty()
                || numIdentificacion == null || numIdentificacion.isEmpty()
                || fechaDesde == null || fechaDesde.isEmpty()
                || fechaHasta == null || fechaHasta.isEmpty()) {
            throw new AppException("Los parametros obligatorios para realizar la consulta vienen nulos o estan vacios");
        }

        
        
        ResultSet rs;
        byte[] xls = null;
        try {
            rs = sqlConection.analisisPagosAportante(tipoIdentificacion, numIdentificacion,fechaDesde, fechaHasta);
            if (rs != null) {
                xls = CrearExcelAportante.crearExcelAportante(rs);
            }
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }

        return xls;

    }
}
