package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplAportante;

import co.gov.ugpp.denuncias.srvaplaportante.v1.MsjOpActualizarAportanteFallo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.MsjOpAnalizarPagosAportanteFallo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.MsjOpAnalizarPagosPersuasivoFallo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.MsjOpBuscarPorIdAportanteFallo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.OpActualizarAportanteRespTipo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosAportanteRespTipo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosPersuasivoRespTipo;
import co.gov.ugpp.denuncias.srvaplaportante.v1.OpBuscarPorIdAportanteRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplAportante",
        portName = "portSrvAplAportanteSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplaportante.v1.PortSrvAplAportanteSOAP")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplAportante extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplAportante.class);

    @EJB
    private AportanteFacade aportanteFacade;

    /**
     * Operacion que a partir de unos parometros de bosqueda obtiene los datos registrados del Aportante en las BD de la
     * UGPP. Al enviar el tipo y nomero de identificacion se debera retornar un solo Aportante y al no enviar parometros
     * debera retornar todos los Aportantes.
     *
     * @param msjOpBuscarPorIdAportanteSol
     * @return
     * @throws MsjOpBuscarPorIdAportanteFallo
     */
    public co.gov.ugpp.denuncias.srvaplaportante.v1.OpBuscarPorIdAportanteRespTipo opBuscarPorIdAportante(co.gov.ugpp.denuncias.srvaplaportante.v1.OpBuscarPorIdAportanteSolTipo msjOpBuscarPorIdAportanteSol) throws MsjOpBuscarPorIdAportanteFallo {

        LOG.info("Op: opBuscarPorIdAportante ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdAportanteSol.getContextoTransaccional();

        final List<AportanteTipo> listAportante;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            listAportante = aportanteFacade.buscarPorIdAportante(msjOpBuscarPorIdAportanteSol.getIdentificacion());

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdAportante ::: END");

        OpBuscarPorIdAportanteRespTipo buscarPorIdAportanteRes = new OpBuscarPorIdAportanteRespTipo();
        buscarPorIdAportanteRes.getAportante().addAll(listAportante);
        buscarPorIdAportanteRes.setContextoRespuesta(contextoRespuesta);

        return buscarPorIdAportanteRes;
    }

    public co.gov.ugpp.denuncias.srvaplaportante.v1.OpActualizarAportanteRespTipo opActualizarAportante(co.gov.ugpp.denuncias.srvaplaportante.v1.OpActualizarAportanteSolTipo msjOpActualizarAportanteSol) throws MsjOpActualizarAportanteFallo {
        LOG.info("Op: opActualizarAportante ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarAportanteSol.getContextoTransaccional();

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            aportanteFacade.actualizarAportante(msjOpActualizarAportanteSol.getAportante(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarAportante ::: END");

        OpActualizarAportanteRespTipo actualizarAportanteRespTipo = new OpActualizarAportanteRespTipo();
        actualizarAportanteRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarAportanteRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosPersuasivoRespTipo opAnalizarPagosPersuasivo(co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosPersuasivoSolTipo msjOpAnalizarPagosPersuasivoSol) throws MsjOpAnalizarPagosPersuasivoFallo {

        LOG.info("Op: opAnalizarPagosPersuasivo ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpAnalizarPagosPersuasivoSol.getContextoTransaccional();

        Boolean esPago;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            String expediente = String.valueOf(msjOpAnalizarPagosPersuasivoSol.getIdExpediente());

            esPago = aportanteFacade.analizarPagosPersuasivo(msjOpAnalizarPagosPersuasivoSol.getAportante(),
                    msjOpAnalizarPagosPersuasivoSol.getCotizante(),
                    expediente,
                    msjOpAnalizarPagosPersuasivoSol.getPeriodo());

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAnalizarPagosPersuasivoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAnalizarPagosPersuasivoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opAnalizarPagosPersuasivo ::: END");

        OpAnalizarPagosPersuasivoRespTipo analizarPagosPersuasivoRespTipo = new OpAnalizarPagosPersuasivoRespTipo();
        analizarPagosPersuasivoRespTipo.setContextoRespuesta(contextoRespuesta);
        analizarPagosPersuasivoRespTipo.setEsPago(String.valueOf(esPago));

        return analizarPagosPersuasivoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosAportanteRespTipo opAnalizarPagosAportante(co.gov.ugpp.denuncias.srvaplaportante.v1.OpAnalizarPagosAportanteSolTipo msjOpAnalizarPagosAportanteSol) throws MsjOpAnalizarPagosAportanteFallo {

        LOG.info("Op: opAnalizarPagosAportante ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpAnalizarPagosAportanteSol.getContextoTransaccional();

        byte[] documento;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            documento = aportanteFacade.analizarPagosAportante(msjOpAnalizarPagosAportanteSol.getAportante(),
                    msjOpAnalizarPagosAportanteSol.getDenunciante(),
                    msjOpAnalizarPagosAportanteSol.getIdExpediente(),
                    msjOpAnalizarPagosAportanteSol.getPeriodo());

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAnalizarPagosAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpAnalizarPagosAportanteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opAnalizarPagosAportante ::: END");

        OpAnalizarPagosAportanteRespTipo analizarPagosAportanteRespTipo = new OpAnalizarPagosAportanteRespTipo();
        analizarPagosAportanteRespTipo.setContextoRespuesta(contextoRespuesta);
        analizarPagosAportanteRespTipo.setDocumento(documento);

        return analizarPagosAportanteRespTipo;
    }

}
