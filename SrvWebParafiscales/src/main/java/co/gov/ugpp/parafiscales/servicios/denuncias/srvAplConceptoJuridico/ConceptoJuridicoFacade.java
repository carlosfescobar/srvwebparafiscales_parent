package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplConceptoJuridico;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;
import java.io.Serializable;

/**
 *
 * @author franzjr
 */
public interface ConceptoJuridicoFacade extends Serializable{
    
    public ConceptoJuridicoTipo buscarPorIdConcepto(String idConceptoJuridico,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    public void actualizarConcepto(ConceptoJuridicoTipo conceptoJuridicoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    public Long crearConcepto(ConceptoJuridicoTipo conceptoJuridicoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
