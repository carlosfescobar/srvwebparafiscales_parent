package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplConceptoJuridico;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ConceptoJuridicoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridico;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.AportanteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.ConceptoJuridicoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class ConceptoJuridicoFacadeImpl extends AbstractFacade implements ConceptoJuridicoFacade {

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private ConceptoJuridicoDao conceptoJuridicoDao;
    @EJB
    private ExpedienteDao expedienteDao;
    @EJB
    private DocumentoDao documentoEcmDao;

    AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();
    ConceptoJuridicoAssembler conceptoJuridicoAssembler = ConceptoJuridicoAssembler.getInstance();

    @Override
    public ConceptoJuridicoTipo buscarPorIdConcepto(String idConceptoJuridico,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(idConceptoJuridico)) {
            throw new AppException("Debe proporcionar los parametros de busqueda de los objetos a consultar");
        }

        ConceptoJuridico conceptoJuridico = conceptoJuridicoDao.find(Long.parseLong(idConceptoJuridico));
        ConceptoJuridicoTipo conceptoJuridicoTipo = new ConceptoJuridicoTipo();

        if (conceptoJuridico != null) {
            conceptoJuridicoTipo = conceptoJuridicoAssembler.assembleServicio(conceptoJuridico);
        }

        return conceptoJuridicoTipo;
    }

    @Override
    public void actualizarConcepto(ConceptoJuridicoTipo conceptoJuridicoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (conceptoJuridicoTipo == null || StringUtils.isBlank(conceptoJuridicoTipo.getIdConceptoJuridico())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ConceptoJuridico concepto = conceptoJuridicoDao.find(Long.valueOf(conceptoJuridicoTipo.getIdConceptoJuridico()));

        if (concepto == null) {
            throw new AppException("No se encontro el objeto a actualizar");
        }

        assembleEntidad(concepto, conceptoJuridicoTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        concepto.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        concepto.setFechaModificacion(DateUtil.currentCalendar());
        conceptoJuridicoDao.edit(concepto);
    }

    @Override
    public Long crearConcepto(ConceptoJuridicoTipo conceptoJuridicoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (conceptoJuridicoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ConceptoJuridico conceptoJuridico = new ConceptoJuridico();
        
        conceptoJuridico.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        conceptoJuridico.setFechaCreacion(DateUtil.currentCalendar());

        assembleEntidad(conceptoJuridico, conceptoJuridicoTipo, errorTipoList);

        if (StringUtils.isBlank(conceptoJuridicoTipo.getCodPrioridad())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El codigoPrioridad no puede ser nulo"));
        }

        if (StringUtils.isBlank(conceptoJuridicoTipo.getIdNumeroExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El idNumeroExpediente no puede ser nulo"));
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idConceptoJuridico = conceptoJuridicoDao.create(conceptoJuridico);

        return idConceptoJuridico;
    }

    private void assembleEntidad(final ConceptoJuridico conceptoJuridico, ConceptoJuridicoTipo servicio, final List<ErrorTipo> errorTipoList) throws AppException {

        if (servicio.getIdConceptoJuridico() != null) {
            conceptoJuridico.setIdConceptoJuridico(Long.valueOf(servicio.getIdConceptoJuridico()));
        }
        if (StringUtils.isNotBlank(servicio.getCodPrioridad())) {
            ValorDominio codProDominio = valorDominioDao.find(servicio.getCodPrioridad());
            if (codProDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codigoPrioridad proporcionado no existe con el ID: " + servicio.getCodPrioridad()));
            } else {
                conceptoJuridico.setCodPrioridad(codProDominio);
            }
        }
        if (StringUtils.isNotBlank(servicio.getDescAsuntoConceptoJuridico())) {
            conceptoJuridico.setDescAsuntoConceptoJuridico(servicio.getDescAsuntoConceptoJuridico());
        }
        if (StringUtils.isNotBlank(servicio.getDescMotivoPrioridad())) {
            conceptoJuridico.setDescMotivoPrioridad(servicio.getDescMotivoPrioridad());
        }
        if (servicio.getDocumento() != null) {
            DocumentoEcm documento = documentoEcmDao.find(servicio.getDocumento().getIdDocumento());
            if (documento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El documento proporcionado no existe con el ID: " + servicio.getDocumento().getIdDocumento()));
            } else {
                conceptoJuridico.setDocumentoEcm(documento);
            }
        }
        if (StringUtils.isNotBlank(servicio.getIdNumeroExpediente())) {
            Expediente expediente = expedienteDao.find(servicio.getIdNumeroExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente proporcionado no existe con el ID: " + servicio.getIdNumeroExpediente()));
            } else {
                conceptoJuridico.setExpedienteId(expediente);
            }
        }
    }

}
