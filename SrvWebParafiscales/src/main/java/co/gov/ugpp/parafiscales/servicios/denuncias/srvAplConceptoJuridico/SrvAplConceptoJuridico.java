package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplConceptoJuridico;

import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpActualizarConceptoJuridicoFallo;
import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpBuscarPorIdConceptoJuridicoFallo;
import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpCrearConceptoJuridicoFallo;
import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpActualizarConceptoJuridicoRespTipo;
import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpBuscarPorIdConceptoJuridicoRespTipo;
import co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpCrearConceptoJuridicoRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplConceptoJuridico",
        portName = "portSrvAplConceptoJuridicoSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.PortSrvAplConceptoJuridicoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplConceptoJuridico/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplConceptoJuridico extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplConceptoJuridico.class);

    @EJB
    private ConceptoJuridicoFacade conceptoJuridicoFacade;

    public co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpBuscarPorIdConceptoJuridicoRespTipo opBuscarPorIdConceptoJuridico(co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpBuscarPorIdConceptoJuridicoSolTipo msjOpBuscarPorIdConceptoJuridicoSol) throws co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpBuscarPorIdConceptoJuridicoFallo {
        LOG.info("Op: opBuscarPorIdConceptoJuridico ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdConceptoJuridicoSol.getContextoTransaccional();

        final ConceptoJuridicoTipo concepto;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            concepto = conceptoJuridicoFacade.buscarPorIdConcepto(
                    msjOpBuscarPorIdConceptoJuridicoSol.getIdConceptoJuridico(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdConceptoJuridico ::: END");

        OpBuscarPorIdConceptoJuridicoRespTipo buscarPorIdConceptoJuridicoRespTipo = new OpBuscarPorIdConceptoJuridicoRespTipo();
        buscarPorIdConceptoJuridicoRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdConceptoJuridicoRespTipo.setConceptoJuridico(concepto);

        return buscarPorIdConceptoJuridicoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpActualizarConceptoJuridicoRespTipo opActualizarConceptoJuridico(co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpActualizarConceptoJuridicoSolTipo msjOpActualizarConceptoJuridicoSol) throws co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpActualizarConceptoJuridicoFallo {
        LOG.info("Op: opActualizarConceptoJuridico ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarConceptoJuridicoSol.getContextoTransaccional();

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            conceptoJuridicoFacade.actualizarConcepto(msjOpActualizarConceptoJuridicoSol.getConceptoJuridico(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarConceptoJuridico ::: END");

        OpActualizarConceptoJuridicoRespTipo actualizarConceptoJuridicoRespTipo = new OpActualizarConceptoJuridicoRespTipo();
        actualizarConceptoJuridicoRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarConceptoJuridicoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpCrearConceptoJuridicoRespTipo opCrearConceptoJuridico(co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.OpCrearConceptoJuridicoSolTipo msjOpCrearConceptoJuridicoSol) throws co.gov.ugpp.denuncias.srvaplconceptojuridico.v1.MsjOpCrearConceptoJuridicoFallo {
        LOG.info("Op: opCrearConceptoJuridico ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearConceptoJuridicoSol.getContextoTransaccional();
        Long idConceptoJuridico;
        
        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            idConceptoJuridico = conceptoJuridicoFacade.crearConcepto(msjOpCrearConceptoJuridicoSol.getConceptoJuridico(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearConceptoJuridicoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opCrearConceptoJuridico ::: END");

        OpCrearConceptoJuridicoRespTipo crearConceptoJuridicoRespTipo = new OpCrearConceptoJuridicoRespTipo();
        crearConceptoJuridicoRespTipo.setContextoRespuesta(contextoRespuesta);
        crearConceptoJuridicoRespTipo.setIdConceptoJuridico(String.valueOf(idConceptoJuridico));

        return crearConceptoJuridicoRespTipo;
    }

}
