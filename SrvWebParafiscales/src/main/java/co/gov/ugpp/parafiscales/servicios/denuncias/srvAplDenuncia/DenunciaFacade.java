package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenuncia;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface DenunciaFacade extends Serializable {

    public String crearDenuncia(DenunciaTipo denuncia,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarDenuncia(DenunciaTipo denuncia,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public DenunciaTipo buscarPorIdDenuncia(String idDenuncia,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public String calificarDenuncia(String idExpediente,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<DenunciaTipo> buscarPorCriteriosDenuncia(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
