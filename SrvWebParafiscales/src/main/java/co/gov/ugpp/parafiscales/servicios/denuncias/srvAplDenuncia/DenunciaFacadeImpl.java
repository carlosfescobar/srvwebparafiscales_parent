package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenuncia;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.EstadoDenunciaEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.AfectadoDao;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaCausalAnalisisDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaCausalInicialDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenuncianteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.persistence.entity.Afectado;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalAnalisis;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalInicial;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaAccion;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaHasCodSubsistema;
import co.gov.ugpp.parafiscales.persistence.entity.Denunciante;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.servicios.helpers.DenunciaAssembler;
import co.gov.ugpp.parafiscales.servicios.util.SQLConection;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.EnumMap;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class DenunciaFacadeImpl extends AbstractFacade implements DenunciaFacade {

    @EJB
    private DenunciaDao denunciaDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private AportanteDao aportanteDao;
    @EJB
    private DenuncianteDao denuncianteDao;
    @EJB
    private ExpedienteFacade expedienteFacade;
    @EJB
    private AfectadoDao afectadoDao;
    @EJB
    private DenunciaCausalAnalisisDao denunciaCausalAnalisisDao;
    @EJB
    private DenunciaCausalInicialDao denunciaCausalInicialDao;

    private DenunciaAssembler denunciaAssembler = DenunciaAssembler.getInstance();
    private SQLConection sqlConection = SQLConection.getInstance();

    @Override
    public String crearDenuncia(DenunciaTipo denunciaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (denunciaTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }
        if (StringUtils.isNotBlank(denunciaTipo.getIdDenuncia())) {
            throw new AppException("Para la creacion de la denuncia no es necesario el id");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Validator.checkDenuncia(denunciaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        Denuncia denuncia = new Denuncia();

        checkBusinessDenuncia(denuncia, denunciaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        denuncia.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        denuncia.setFechaCreacion(DateUtil.currentCalendar());
        assembleEntidad(denuncia, denunciaTipo, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idDenuncia = denunciaDao.create(denuncia);
        Denuncia denun = denunciaDao.find(idDenuncia);

        if (denunciaTipo.getCodSubsistema() != null) {
            List<DenunciaHasCodSubsistema> list = new ArrayList<DenunciaHasCodSubsistema>();
            for (String codSubsitemasTipo : denunciaTipo.getCodSubsistema()) {
                ValorDominio codSub = valorDominioDao.find(codSubsitemasTipo);
                if (codSub == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El valor dominio codSubsitema proporcionado no existe con el ID: " + codSubsitemasTipo));
                } else {
                    DenunciaHasCodSubsistema denunciaHasCodSubsistema = new DenunciaHasCodSubsistema();
                    denunciaHasCodSubsistema.setIdDenuncia(denun);
                    denunciaHasCodSubsistema.setCodSubsistema(codSubsitemasTipo);
                    list.add(denunciaHasCodSubsistema);
                }
            }
            denun.setDenunciaHasCodSubsistemaList(list);
        }

        denunciaDao.edit(denun);

        return String.valueOf(idDenuncia);
    }

    @Override
    public void actualizarDenuncia(DenunciaTipo denunciaTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (denunciaTipo == null || denunciaTipo.getIdDenuncia() == null) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Denuncia denuncia = denunciaDao.find(Long.parseLong(denunciaTipo.getIdDenuncia()));
        
        //System.out.println("Op: ::ANDRES1:: actualizarDenuncia denuncia.getId(): " + denuncia.getId());

        
        if (denuncia == null) {
            throw new AppException("No se encontro una denuncia con el valor:" + denunciaTipo.getIdDenuncia());
        }
        
        checkBusinessDenuncia(denuncia, denunciaTipo, errorTipoList);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        assembleEntidad(denuncia, denunciaTipo, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        if (denunciaTipo.getCodSubsistema() != null) {
            List<DenunciaHasCodSubsistema> list = new ArrayList<DenunciaHasCodSubsistema>();
            for (String codSubsitemasTipo : denunciaTipo.getCodSubsistema()) {
                ValorDominio codSub = valorDominioDao.find(codSubsitemasTipo);
                if (codSub == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El valor dominio codSubsitemasTipo proporcionado no existe con el ID: " + codSubsitemasTipo));
                } else {
                    DenunciaHasCodSubsistema denunciaHasCodSubsistema = new DenunciaHasCodSubsistema();
                    denunciaHasCodSubsistema.setIdDenuncia(denuncia);
                    denunciaHasCodSubsistema.setCodSubsistema(codSubsitemasTipo);
                    
                    
                    //System.out.println("Op: ::ANDRES33:: actualizarDenuncia list.size(): " + denunciaHasCodSubsistema.getId());
                    
                    list.add(denunciaHasCodSubsistema);
                }
            }
            
            
            //System.out.println("Op: ::ANDRES34:: actualizarDenuncia list.size(): " + list.size());
            
            denuncia.setDenunciaHasCodSubsistemaList(list);
        }

        denuncia.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        denuncia.setFechaModificacion(DateUtil.currentCalendar());
        denunciaDao.edit(denuncia);
    }

    @Override
    public DenunciaTipo buscarPorIdDenuncia(String idDenuncia,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isBlank(idDenuncia)) {
            throw new AppException("Debe proporcionar los parametros de busqueda de los objetos a consultar");
        }

        Denuncia denuncia = denunciaDao.find(Long.parseLong(idDenuncia));

        return denuncia == null ? null : denunciaAssembler.assembleServicio(denuncia);

    }

    @Override
    public String calificarDenuncia(String numExpediente,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        List<Denuncia> denuncias;
        try {
            denuncias = denunciaDao.findByExpediente(numExpediente);

            Integer scoring = null;

            if (denuncias != null) {
                if (denuncias.size() > 0) {
                    scoring = sqlConection.fuctionStoredCalculaScoring(denuncias.get(0));
                }
            }

            if (scoring != null) {
                return String.valueOf(scoring);
            } else {
                return "";
            }

        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }
    }

    private void assembleEntidad(final Denuncia denuncia, DenunciaTipo servicio, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        
        //System.out.println("Op: ::ANDRES56:: assembleEntidad denuncia.getId(): " + denuncia.getId());
        
        
        if (StringUtils.isNotBlank(servicio.getIdDenuncia())) {
            try {
                denuncia.setIdDenuncia(Long.parseLong(servicio.getIdDenuncia()));
            } catch (NumberFormatException e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Error en el formato del id denuncia: " + e.getMessage()));
            }
        }
        if (StringUtils.isNotBlank(servicio.getIdRadicadoDenuncia())) {
            denuncia.setIdRadicadoDenuncia(servicio.getIdRadicadoDenuncia());
        }
        if (servicio.getAportante() != null) {
            Identificacion identificacion = null;
            if (servicio.getAportante().getPersonaJuridica() != null) {
                identificacion = mapper.map(servicio.getAportante().getPersonaJuridica().getIdPersona(), Identificacion.class);
            } else if (servicio.getAportante().getPersonaNatural() != null) {
                identificacion = mapper.map(servicio.getAportante().getPersonaNatural().getIdPersona(), Identificacion.class);
            }
            if (identificacion != null) {
                Aportante aportante = aportanteDao.findByIdentificacion(identificacion);
                if (aportante != null) {
                    denuncia.setAportante(aportante);
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El aportante asociado a la denuncia, no existe con el id: " + identificacion.toString()));
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Por favor enviar el id del aportante ya que viene nulo"));
            }
        }
        if (StringUtils.isNotBlank(servicio.getCodCanalDenuncia())) {
            ValorDominio codCanalDenuncia = valorDominioDao.find(servicio.getCodCanalDenuncia());
            if (codCanalDenuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codCanalDenuncia proporcionado no existe con el ID: " + servicio.getCodCanalDenuncia()));
            } else {
                denuncia.setCodCanaDenuncial(codCanalDenuncia);
            }
        }
        if (StringUtils.isNotBlank(servicio.getCodCategoriaDenuncia())) {
            ValorDominio codCategoriaDenuncia = valorDominioDao.find(servicio.getCodCategoriaDenuncia());
            if (codCategoriaDenuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codCategoriaDenuncia proporcionado no existe con el ID: " + servicio.getCodCategoriaDenuncia()));
            } else {
                denuncia.setCodCategoriaDenuncia(codCategoriaDenuncia);
            }
        }
        if (StringUtils.isNotBlank(servicio.getCodDecisionAnalisisPagos())) {
            ValorDominio codDecisionAnalisisPagos = valorDominioDao.find(servicio.getCodDecisionAnalisisPagos());
            if (codDecisionAnalisisPagos == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codDecisionAnalisisPagos proporcionado no existe con el ID: " + servicio.getCodDecisionAnalisisPagos()));
            } else {
                denuncia.setCodDAPagos(codDecisionAnalisisPagos);
            }
        }

        if (servicio.getCodSubsistema() != null && servicio.getCodSubsistema().size() > 0) {
            for (String codSubsitemasTipo : servicio.getCodSubsistema()) {
                ValorDominio codSub = valorDominioDao.find(codSubsitemasTipo);
                if (codSub == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El valor dominio codSubsistema proporcionado no existe con el ID: " + codSubsitemasTipo));
                }
            }
        }

        if (servicio.getAfectado() != null) {
            if (servicio.getAfectado().getPersonaJuridica() != null
                    && servicio.getAfectado().getPersonaJuridica().getIdPersona() != null
                    && (StringUtils.isNotBlank(servicio.getAfectado().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(servicio.getAfectado().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = servicio.getAfectado().getPersonaJuridica().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Afectado afectado = afectadoDao.findByIdentificacion(identificacion);

                if (afectado == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un Afectado con la identificación :" + identificacion));
                } else {
                    denuncia.setAfectado(afectado);
                }
            } else if (servicio.getAfectado().getPersonaNatural() != null
                    && servicio.getAfectado().getPersonaNatural().getIdPersona() != null
                    && (StringUtils.isNotBlank(servicio.getAfectado().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(servicio.getAfectado().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                final IdentificacionTipo identificacionTipo = servicio.getAfectado().getPersonaNatural().getIdPersona();

                final Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Afectado afectado = afectadoDao.findByIdentificacion(identificacion);

                if (afectado == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un Afectado con la identificación :" + identificacion));
                } else {
                    denuncia.setAfectado(afectado);
                }
            }
        }

        if (servicio.getDenunciante() != null) {
            Identificacion identificacion = null;
            if (servicio.getDenunciante().getPersonaJuridica() != null) {
                identificacion = mapper.map(servicio.getDenunciante().getPersonaJuridica().getIdPersona(), Identificacion.class);
            } else if (servicio.getDenunciante().getPersonaNatural() != null) {
                identificacion = mapper.map(servicio.getDenunciante().getPersonaNatural().getIdPersona(), Identificacion.class);
            }
            if (identificacion != null) {
                Denunciante denunciante = denuncianteDao.findByIdentificacion(identificacion);
                if (denunciante != null) {
                    denuncia.setIdDenunciante(denunciante);
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El denunciante asociado a la denuncia, no existe con el id: " + identificacion.toString()));
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Por favor enviar el id del denunciante ya que viene nulo"));
            }
        }
        if (StringUtils.isNotBlank(servicio.getDescObservaciones())) {
            denuncia.setDescObservaciones(servicio.getDescObservaciones());
        }
        if (StringUtils.isNotBlank(servicio.getEsCasoCerrado())) {
            denuncia.setEsCasoCerrado(Boolean.valueOf(servicio.getEsCasoCerrado()));
        }
        if (servicio.getEsCompetenciaUGPP() != null) {
            denuncia.setEsCompetenciaUgPp(Boolean.valueOf(servicio.getEsCompetenciaUGPP()));
        }
        if (servicio.getEsCompetenciaParcial()!= null) {
            denuncia.setEsCompetenciaParcial(Boolean.valueOf(servicio.getEsCompetenciaParcial()));
        }
        if (servicio.getEsPago() != null) {
            denuncia.setEsPago(Boolean.valueOf(servicio.getEsPago()));
        }
        if (servicio.getEsEnFiscalizacion() != null) {
            denuncia.setEsEnFiscalizacion(Boolean.valueOf(servicio.getEsEnFiscalizacion()));
        }
        if (servicio.getEsInformacionCompleta() != null) {
            denuncia.setEsInformacionCompleta(Boolean.valueOf(servicio.getEsInformacionCompleta()));
        }
        if (StringUtils.isNotBlank(servicio.getEsSuspenderTerminos())) {
            denuncia.setEsSuspenderTermino(Boolean.valueOf(servicio.getEsSuspenderTerminos()));
        }
        if (StringUtils.isNotBlank(servicio.getEsTieneDerechoPeticion())) {
            denuncia.setEsTieneDerechoPeticion(Boolean.valueOf(servicio.getEsTieneDerechoPeticion()));
        }
        if (StringUtils.isNotBlank(servicio.getEsTieneAnexos())) {
            denuncia.setEsTieneAnexos(Boolean.valueOf(servicio.getEsTieneAnexos()));
        }
        if (StringUtils.isNotBlank(servicio.getEsPagosValidados())) {
            denuncia.setEsPagosValidados(Boolean.valueOf(servicio.getEsPagosValidados()));
        }

        if (StringUtils.isNotBlank(servicio.getCodTipoDenuncia())) {
            ValorDominio codTipoDenuncia = valorDominioDao.find(servicio.getCodTipoDenuncia());
            if (codTipoDenuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoDenuncia proporcionado no existe con el ID: " + servicio.getCodTipoDenuncia()));
            } else {
                denuncia.setCodTipoDenuncia(codTipoDenuncia);
            }
        }

        if (StringUtils.isNotBlank(servicio.getDescInformacionFaltante())) {
            denuncia.setDescInformacionFaltante(servicio.getDescInformacionFaltante());
        }

        final Calendar calendar = new GregorianCalendar();
        if (StringUtils.isNotBlank(servicio.getFecRadicadoDenuncia())) {
            denuncia.setFecRadicadoDenuncia(DateUtil.parseStrDateTimeToCalendar(servicio.getFecRadicadoDenuncia()));
        }
        if (calendar.before(denuncia.getFecRadicadoDenuncia())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "La Fecha del Radicado de la denuncia no puede ser mayor a la fecha actual"));
        }

        //Validacion adicional, si no se envía el expediente, puede que si se quieran cambiar los documentos asociados al expediente
        if ((denuncia.getExpediente() != null && StringUtils.isNotBlank(denuncia.getExpediente().getId()))
                && (servicio.getExpediente() == null || StringUtils.isBlank(servicio.getExpediente().getIdNumExpediente()))) {

            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(denuncia.getExpediente().getId());
            servicio.setExpediente(expedienteTipo);
        }

        if (servicio.getExpediente() != null
                && StringUtils.isNotBlank(servicio.getExpediente().getIdNumExpediente())) {

            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos
                    = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);

            List<DocumentoTipo> documentosxOrigen;

            if (StringUtils.isNotBlank(servicio.getIdDocumentoCierreCaso())) {
                
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoCierreCaso = new DocumentoTipo();
                documentoCierreCaso.setIdDocumento(servicio.getIdDocumentoCierreCaso());
                documentosxOrigen.add(documentoCierreCaso);
                documentos.put(OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_CIERRE_CASO, documentosxOrigen);
                
                //System.out.println("Op: ::ANDRES9:: assembleEntidad getIdDocumentoCierreCaso documentos.size(): " + documentos.size());
                
            }
            if (StringUtils.isNotBlank(servicio.getIdDocumentoCompletitudInformacion())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoCompletitudInformacion = new DocumentoTipo();
                documentoCompletitudInformacion.setIdDocumento(servicio.getIdDocumentoCompletitudInformacion());
                documentosxOrigen.add(documentoCompletitudInformacion);
                documentos.put(OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_COMPLETITUD_INFO, documentosxOrigen);
                
                //System.out.println("Op: ::ANDRES10:: assembleEntidad getIdDocumentoCompletitudInformacion documentos.size(): " + documentos.size());
            }
            if (StringUtils.isNotBlank(servicio.getIdDocumentoPersuasivo())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoPersuasivo = new DocumentoTipo();
                documentoPersuasivo.setIdDocumento(servicio.getIdDocumentoPersuasivo());
                documentosxOrigen.add(documentoPersuasivo);
                documentos.put(OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_PERSUASIVO, documentosxOrigen);
                
                //System.out.println("Op: ::ANDRES11:: assembleEntidad getIdDocumentoPersuasivo documentos.size(): " + documentos.size());
                //System.out.println("Op: ::ANDRES11:: assembleEntidad getIdDocumentoPersuasivo documentos.size(): " + servicio.getIdDocumentoPersuasivo());
                
            }
            if (StringUtils.isNotBlank(servicio.getIdDocumentoSuspensionTerminos())) {
                documentosxOrigen = new ArrayList<DocumentoTipo>();
                DocumentoTipo documentoSuspensionTerminos = new DocumentoTipo();
                documentoSuspensionTerminos.setIdDocumento(servicio.getIdDocumentoSuspensionTerminos());
                documentosxOrigen.add(documentoSuspensionTerminos);
                documentos.put(OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_SUSP_TERMINOS, documentosxOrigen);
                
                //System.out.println("Op: ::ANDRES12:: assembleEntidad getIdDocumentoSuspensionTerminos documentos.size(): " + documentos.size());
                
            }

            Expediente expediente = expedienteFacade.persistirExpedienteDocumento(servicio.getExpediente(),
                    documentos, errorTipoList, true, contextoTransaccionalTipo);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                denuncia.setExpediente(expediente);
            }
        }

        if (StringUtils.isNotBlank(servicio.getCodEstadoDenuncia())) {
            ValorDominio codEstadoDenuncia = valorDominioDao.find(servicio.getCodEstadoDenuncia());
            if (codEstadoDenuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el codEstadoDenuncia con el ID: " + servicio.getCodEstadoDenuncia()));
            } else {
                denuncia.setCodEstadoDenuncia(codEstadoDenuncia);
            }
        }

        if (StringUtils.isNotBlank(servicio.getCodTipoEnvioComite())) {
            ValorDominio codTipoEnvioComite = valorDominioDao.find(servicio.getCodTipoEnvioComite());
            if (codTipoEnvioComite == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el codTipoEnvioComite con el ID: " + servicio.getCodTipoEnvioComite()));
            } else {
                denuncia.setCodTipoEnvioComite(codTipoEnvioComite);
            }
        }

        if (servicio.getEsAprobadoCierre() != null) {
            Validator.parseBooleanFromString(servicio.getEsAprobadoCierre(),
                    servicio.getClass().getSimpleName(),
                    "esAprobadoCierre", errorTipoList);
            denuncia.setEsAprobadoCierre(Boolean.valueOf(servicio.getEsAprobadoCierre()));
        }

        if (servicio.getFecInicioPeriodo() != null) {
            denuncia.setFecInicioPeriodo(servicio.getFecInicioPeriodo());
        }
        if (servicio.getFecFinPeriodo() != null) {
            denuncia.setFecFinPeriodo(servicio.getFecFinPeriodo());
        }
        if (denuncia.getFecFinPeriodo() != null) {
            if (denuncia.getFecInicioPeriodo() != null) {
                if (denuncia.getFecInicioPeriodo().after(denuncia.getFecFinPeriodo())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La fecha de fin periodo debe ser superior a la fecha de inicio periodo"));
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La fecha de fin periodo  no se puede registar ya que la fecha de inicio periodo es nula"));
            }
        }

        if (servicio.getCodCausalAnalisisDenuncia() != null
                && !servicio.getCodCausalAnalisisDenuncia().isEmpty()) {
            for (String codAnalisisDenuncia : servicio.getCodCausalAnalisisDenuncia()) {
                if (StringUtils.isNotBlank(codAnalisisDenuncia)) {
                    ValorDominio codAnalisiDenunciaEntity = valorDominioDao.find(codAnalisisDenuncia);
                    if (codAnalisiDenunciaEntity == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se ha encontrado el codCausalAnalisisDenuncia con el ID: " + codAnalisisDenuncia));
                    } else {
                        DenunciaCausalAnalisis denunciaCausalAnalisis = this.buildDenunciaCausalAnalisis(denuncia, codAnalisiDenunciaEntity, contextoTransaccionalTipo);
                        denuncia.getCausalAnalisis().add(denunciaCausalAnalisis);
                    }
                }
            }
        }

        
        
        //modificacion andres        
        if (StringUtils.isNotBlank(servicio.getObservaciones())) 
        {
            for (DenunciaCausalAnalisis denunciaCausalAnalisis : denuncia.getCausalAnalisis()) 
            {
                denunciaCausalAnalisis.setObservaciones(servicio.getObservaciones());
            }
        }
       
                
                
                
                
                
        
        if (servicio.getCodCausalInicialDenuncia() != null
                && !servicio.getCodCausalInicialDenuncia().isEmpty()) {
            for (String codInicialDenuncia : servicio.getCodCausalInicialDenuncia()) {
                if (StringUtils.isNotBlank(codInicialDenuncia)) {
                    ValorDominio codInicialDenunciaEntity = valorDominioDao.find(codInicialDenuncia);
                    if (codInicialDenunciaEntity == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se ha encontrado el codCausalInicialDenuncia con el ID: " + codInicialDenuncia));
                    } else {
                        DenunciaCausalInicial denunciaCausalInicial = this.buildDenunciaCausalInicial(denuncia, codInicialDenunciaEntity, contextoTransaccionalTipo);
                        denuncia.getCausalInicial().add(denunciaCausalInicial);
                    }
                }
            }
        }

        if (servicio.getAcciones() != null
                && !servicio.getAcciones().isEmpty()) {
            for (AccionTipo accionTipo : servicio.getAcciones()) {
                Accion accion = new Accion();
                accion.setFecEjecucionAccion(DateUtil.parseStrDateTimeToCalendar(accionTipo.getFecEjecucionAccion()));
                 accion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
                if (StringUtils.isNotBlank(accionTipo.getCodAccion())) {
                    ValorDominio codAccion = valorDominioDao.find(accionTipo.getCodAccion());
                    if (codAccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codAccion con el ID: " + accionTipo.getCodAccion()));
                    } else {
                        accion.setCodAccion(codAccion);
                    }
                }
                DenunciaAccion accionDenuncia = new DenunciaAccion();
                accionDenuncia.setAccion(accion);
                accionDenuncia.setDenuncia(denuncia);
                accionDenuncia.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());

                denuncia.getDenunciaAccionesList().add(accionDenuncia);
            }
        }

    }

    /**
     * Método que crea una causal inicial para la denuncia
     *
     * @param denuncia la denuncia
     * @param causalInicial la causal inicial
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return la denuncia asociada a la causal inicial
     */
    private DenunciaCausalInicial buildDenunciaCausalInicial(final Denuncia denuncia, final ValorDominio causalInicial, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        DenunciaCausalInicial denunciaCausalInicial = new DenunciaCausalInicial();
        denunciaCausalInicial.setDenuncia(denuncia);
        denunciaCausalInicial.setCodCausalInicial(causalInicial);
        denunciaCausalInicial.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        denunciaCausalInicialDao.create(denunciaCausalInicial);
        return denunciaCausalInicial;
    }

    /**
     * Método que crea una causal de analisis para la denuncia
     *
     * @param denuncia la denuncia
     * @param causalAnalisis el causal de analisis
     * @param contextoTransaccionalTipo contiene los datos de auditoria
     * @return la denuncia asociada al causal de analisis
     */
    private DenunciaCausalAnalisis buildDenunciaCausalAnalisis(final Denuncia denuncia, final ValorDominio causalAnalisis, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        DenunciaCausalAnalisis denunciaCausalAnalisis = new DenunciaCausalAnalisis();
        denunciaCausalAnalisis.setDenuncia(denuncia);
        denunciaCausalAnalisis.setCodCausalAnalisis(causalAnalisis);
        denunciaCausalAnalisis.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        denunciaCausalAnalisisDao.create(denunciaCausalAnalisis);
        return denunciaCausalAnalisis;
    }

    /**
     * implementación de la operación buscarPorCriteriosDenuncia esta se encarga
     * de buscar una denuncia por medio de los parametros enviado y ordenar la
     * consulta de acuerdo a unos criterios establecidos
     *
     * @param parametroTipoList Listado de parametros por los cuales se va a
     * buscar una solicitud
     * @param criterioOrdenamientoTipos Atributos por los cuales se va ordenar
     * la consulta
     * @param contextoTransaccionalTipo Contiene la información para almacenar
     * la auditoria
     * @return Listado de denuncias encontradas que se retorna junto con el
     * contexto transaccional
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de los servicios.
     */
    @Override
    public PagerData<DenunciaTipo> buscarPorCriteriosDenuncia(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Denuncia> denunciaPagerData = denunciaDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(),
                contextoTransaccionalTipo.getValNumPagina());

        final List<DenunciaTipo> denunciaTipoListo = new ArrayList<DenunciaTipo>();

        for (Denuncia denuncia : denunciaPagerData.getData()) {
            DenunciaTipo denunciaTipo = denunciaAssembler.assembleServicio(denuncia);
            denunciaTipoListo.add(denunciaTipo);
        }
        return new PagerData(denunciaTipoListo, denunciaPagerData.getNumPages());
    }


     private void checkBusinessDenuncia(final Denuncia denuncia, final DenunciaTipo denunciaTipo, final List<ErrorTipo> errorTipoList) throws AppException {

         //System.out.println("Op: ::ANDRES55:: checkBusinessDenuncia denuncia.getId(): " + denuncia.getId());
         
         
        if (denunciaTipo.getExpediente() != null && StringUtils.isNotBlank(denunciaTipo.getExpediente().getIdNumExpediente())) 
        {
            Boolean todasDenunciasSuspendidas = true;
            ValorDominio valorDominioDenunciaSuspendido = valorDominioDao.find(EstadoDenunciaEnum.SUSPENDIDO.getCode());
            Long idDenuncia = null;

            //System.out.println("Op: ::ANDRES2:: checkBusinessDenuncia valorDominioDenunciaSuspendido: " + valorDominioDenunciaSuspendido);
            
            
            if (denuncia.getExpediente() != null) 
            {
                if (!denuncia.getExpediente().getId().equals(denunciaTipo.getExpediente().getIdNumExpediente())) 
                {
                    List<Denuncia> denunciaNuevoExpediente = denunciaDao.findByExpediente(denunciaTipo.getExpediente().getIdNumExpediente());
                    
                    
                    //System.out.println("Op: ::ANDRES3:: checkBusinessDenuncia if denunciaNuevoExpediente.size(): " + denunciaNuevoExpediente.size());
                    
                    
                    for (Denuncia itemDenuncia : denunciaNuevoExpediente) {
                       
                        if(itemDenuncia.getCodEstadoDenuncia() != null)
                        {    
                            if(!itemDenuncia.getCodEstadoDenuncia().equals(valorDominioDenunciaSuspendido))
                            {    
                                todasDenunciasSuspendidas = false;
                                idDenuncia = itemDenuncia.getId();
                                
                                //System.out.println("Op: ::ANDRES4:: checkBusinessDenuncia denunciaNuevoExpediente.size(): " + idDenuncia);
                            }
                        }
                        else
                        {
                            todasDenunciasSuspendidas = false;
                            idDenuncia = itemDenuncia.getId();
                            
                            //System.out.println("Op: ::ANDRES5:: checkBusinessDenuncia denunciaNuevoExpediente.size(): " + idDenuncia);
                        }
                    }
                            
                    if (!todasDenunciasSuspendidas) 
                    {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El expediente con ID " + denunciaTipo.getExpediente().getIdNumExpediente() + " ya tiene asociada una denuncia NO suspendida ID " + idDenuncia));
                    }
                }
            } 
            else 
            {
               	List<Denuncia> denunciaNuevoExpediente = denunciaDao.findByExpediente(denunciaTipo.getExpediente().getIdNumExpediente());

                
                //System.out.println("Op: ::ANDRES6:: checkBusinessDenuncia else denunciaNuevoExpediente.size(): " + denunciaNuevoExpediente.size());
                
                for (Denuncia itemDenuncia : denunciaNuevoExpediente) 
                { 
                        if(itemDenuncia.getCodEstadoDenuncia() != null)
                        {    
                            if(!itemDenuncia.getCodEstadoDenuncia().equals(valorDominioDenunciaSuspendido))
                            {    
                                todasDenunciasSuspendidas = false;
                                idDenuncia = itemDenuncia.getId();
                                
                                //System.out.println("Op: ::ANDRES7:: checkBusinessDenuncia denunciaNuevoExpediente.size(): " + idDenuncia);
                            }
                        }
                        else
                        {
                            todasDenunciasSuspendidas = false;
                            idDenuncia = itemDenuncia.getId();
                            
                            //System.out.println("Op: ::ANDRES8:: checkBusinessDenuncia denunciaNuevoExpediente.size(): " + idDenuncia);
                        }
                }
                            
                if (!todasDenunciasSuspendidas) 
                {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El expediente con ID " + denunciaTipo.getExpediente().getIdNumExpediente() + " ya tiene asociada una denuncia NO suspendida ID " + idDenuncia));
                }
            }  
        }
    }
     
}     
