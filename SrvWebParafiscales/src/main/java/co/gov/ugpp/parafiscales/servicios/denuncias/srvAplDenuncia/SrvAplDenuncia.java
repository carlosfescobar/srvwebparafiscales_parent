package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenuncia;

import co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpActualizarDenunciaFallo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpBuscarPorCriteriosDenunciaFallo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpBuscarPorIdDenunciaFallo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpCalificarDenunciaFallo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpCrearDenunciaFallo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpActualizarDenunciaRespTipo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpBuscarPorCriteriosDenunciaRespTipo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpBuscarPorCriteriosDenunciaSolTipo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpBuscarPorIdDenunciaRespTipo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCalificarDenunciaRespTipo;
import co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCrearDenunciaRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.MsjOpBuscarPorCriteriosSolicitudInformacionFallo;
import co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1.OpBuscarPorCriteriosSolicitudInformacionRespTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplDenuncia",
        portName = "portSrvAplDenunciaSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvapldenuncia.v1.PortSrvAplDenunciaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenuncia/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplDenuncia extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplDenuncia.class);

    @EJB
    private DenunciaFacade denunciaFacade;

    public co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCrearDenunciaRespTipo opCrearDenuncia(co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCrearDenunciaSolTipo msjOpCrearDenunciaSol) throws co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpCrearDenunciaFallo {
        LOG.info("Op: opCrearDenuncia ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearDenunciaSol.getContextoTransaccional();

        String idDenuncia;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            idDenuncia = denunciaFacade.crearDenuncia(msjOpCrearDenunciaSol.getDenuncia(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opCrearDenuncia ::: END");

        OpCrearDenunciaRespTipo crearDenunciaRespTipo = new OpCrearDenunciaRespTipo();
        crearDenunciaRespTipo.setContextoRespuesta(contextoRespuesta);
        crearDenunciaRespTipo.setIdDenuncia(String.valueOf(idDenuncia));

        return crearDenunciaRespTipo;
    }

    public co.gov.ugpp.denuncias.srvapldenuncia.v1.OpActualizarDenunciaRespTipo opActualizarDenuncia(co.gov.ugpp.denuncias.srvapldenuncia.v1.OpActualizarDenunciaSolTipo msjOpActualizarDenunciaSol) throws co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpActualizarDenunciaFallo {
        LOG.info("Op: opActualizarDenuncia ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarDenunciaSol.getContextoTransaccional();

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            denunciaFacade.actualizarDenuncia(msjOpActualizarDenunciaSol.getDenuncia(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarDenuncia ::: END");

        OpActualizarDenunciaRespTipo actualizarDenunciaRespTipo = new OpActualizarDenunciaRespTipo();
        actualizarDenunciaRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarDenunciaRespTipo;
    }

    public co.gov.ugpp.denuncias.srvapldenuncia.v1.OpBuscarPorIdDenunciaRespTipo opBuscarPorIdDenuncia(co.gov.ugpp.denuncias.srvapldenuncia.v1.OpBuscarPorIdDenunciaSolTipo msjOpBuscarPorIdDenunciaSol) throws co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpBuscarPorIdDenunciaFallo {
        LOG.info("Op: opBuscarPorIdDenuncia ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdDenunciaSol.getContextoTransaccional();

        DenunciaTipo denunciaTipo;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            denunciaTipo = denunciaFacade.buscarPorIdDenuncia(msjOpBuscarPorIdDenunciaSol.getIdDenuncia(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdDenuncia ::: END");

        OpBuscarPorIdDenunciaRespTipo buscarPorIdDenunciaRespTipo = new OpBuscarPorIdDenunciaRespTipo();
        buscarPorIdDenunciaRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdDenunciaRespTipo.setDenuncia(denunciaTipo);

        return buscarPorIdDenunciaRespTipo;
    }

    public co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCalificarDenunciaRespTipo opCalificarDenuncia(co.gov.ugpp.denuncias.srvapldenuncia.v1.OpCalificarDenunciaSolTipo msjOpCalificarDenunciaSol) throws co.gov.ugpp.denuncias.srvapldenuncia.v1.MsjOpCalificarDenunciaFallo {
        LOG.info("Op: opCalificarDenuncia ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCalificarDenunciaSol.getContextoTransaccional();

        String valCalificacion;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            valCalificacion = denunciaFacade.calificarDenuncia(msjOpCalificarDenunciaSol.getValNumeroExpediente(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCalificarDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCalificarDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opCalificarDenuncia ::: END");

        OpCalificarDenunciaRespTipo calificarDenunciaRespTipo = new OpCalificarDenunciaRespTipo();
        calificarDenunciaRespTipo.setContextoRespuesta(contextoRespuesta);
        calificarDenunciaRespTipo.setValCalificacion(valCalificacion);

        return calificarDenunciaRespTipo;
    }

    public OpBuscarPorCriteriosDenunciaRespTipo opBuscarPorCriteriosDenuncia(OpBuscarPorCriteriosDenunciaSolTipo msjOpBuscarPorCriteriosDenunciaSol) throws MsjOpBuscarPorCriteriosDenunciaFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosDenunciaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosDenunciaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<DenunciaTipo> denunciaPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);

            ldapAuthentication.validateLdapAuthentication(
                    contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            denunciaPagerData = denunciaFacade.buscarPorCriteriosDenuncia(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosDenunciaFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosDenunciaRespTipo resp = new OpBuscarPorCriteriosDenunciaRespTipo();

        cr.setValCantidadPaginas(denunciaPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getDenuncias().addAll(denunciaPagerData.getData());

        return resp;
    }
}
