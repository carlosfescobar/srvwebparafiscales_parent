package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenunciante;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface DenuncianteFacade extends Serializable{
    
    public List<DenuncianteTipo> buscarPorIdDenunciante(IdentificacionTipo identificacion,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarDenunciante(DenuncianteTipo denunciante,
            ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException ;

    public Long crearDenunciante(DenuncianteTipo denunciante,
            ContextoTransaccionalTipo contextoTransaccionalTipo)throws AppException ;
    
    
}
