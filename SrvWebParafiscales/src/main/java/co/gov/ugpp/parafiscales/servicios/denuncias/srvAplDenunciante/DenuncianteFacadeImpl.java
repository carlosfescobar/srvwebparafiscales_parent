package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenunciante;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ClaseCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.DenuncianteDao;
import co.gov.ugpp.parafiscales.persistence.dao.DivisionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.DominioDao;
import co.gov.ugpp.parafiscales.persistence.dao.GrupoCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SeccionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ClaseCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.ClasificacionPersona;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Denunciante;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.DenuncianteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.persistence.entity.DivisionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Dominio;
import co.gov.ugpp.parafiscales.persistence.entity.GrupoCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.SeccionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.servicios.helpers.CorreoElectronicoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.MunicipioAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class DenuncianteFacadeImpl extends AbstractFacade implements DenuncianteFacade {

    @EJB
    private DenuncianteDao denuncianteDao;
    @EJB
    private PersonaDao personaDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private MunicipioDao municipioDao;
    @EJB
    private ClaseCiiuDao claseCiiuDao;
    @EJB
    private DivisionCiiuDao divisionCiiuDao;
    @EJB
    private GrupoCiiuDao grupoCiiuDao;
    @EJB
    private SeccionCiiuDao seccionCiiuDao;
    @EJB
    private DominioDao dominioDao;

    private IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();
    private DenuncianteAssembler denuncianteAssembler = DenuncianteAssembler.getInstance();

    @Override
    public List<DenuncianteTipo> buscarPorIdDenunciante(IdentificacionTipo id,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        List<DenuncianteTipo> denunciantes = new ArrayList<DenuncianteTipo>();

        if (id != null) {
            if (StringUtils.isBlank(id.getCodTipoIdentificacion()) || StringUtils.isBlank(id.getValNumeroIdentificacion())) {
                throw new AppException("Debe proporcionar los ID de los objetos a consultar");
            }

            Identificacion identificacion = mapper.map(id, Identificacion.class);

            Denunciante denunciante = denuncianteDao.findByIdentificacion(identificacion);

            if (denunciante != null) {
                Persona persona = personaDao.find(denunciante.getId());
                denunciante.setPersona(persona);
                DenuncianteTipo denuncianteTipo = denuncianteAssembler.assembleServicio(denunciante);
                denunciantes.add(denuncianteTipo);
            }

        } else {
            denunciantes.addAll(denuncianteAssembler.assembleCollectionServicio(denuncianteDao.findAll()));
        }
        return denunciantes;
    }

    @Override
    public void actualizarDenunciante(DenuncianteTipo denuncianteTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (denuncianteTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Identificacion identificacion = null;
        if (denuncianteTipo.getPersonaJuridica() != null) {
            identificacion = mapper.map(denuncianteTipo.getPersonaJuridica().getIdPersona(), Identificacion.class);
        } else if (denuncianteTipo.getPersonaNatural() != null) {
            identificacion = mapper.map(denuncianteTipo.getPersonaNatural().getIdPersona(), Identificacion.class);
        }
        if (identificacion == null) {
            throw new AppException("Los campos estan vacios para la busqueda del denunciante a actualizar");
        }

        Denunciante denunciante = denuncianteDao.findByIdentificacion(identificacion);
        if (denunciante == null) {
            throw new AppException("No hay un denunciante con los datos buscados" + identificacion.toString());
        }

        if (StringUtils.isNotBlank(denuncianteTipo.getCodTipoDenunciante())) {
            ValorDominio codDenunciante = valorDominioDao.find(denuncianteTipo.getCodTipoDenunciante());
            if (codDenunciante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo denunciante proporcionado no existe con el ID: " + denuncianteTipo.getCodTipoDenunciante()));
            } else {
                denunciante.setCodTipoDenunciante(codDenunciante);
            }
        }

        Persona persona = denunciante.getPersona();

        assembleEntidad(persona, denuncianteTipo, errorTipoList, contextoTransaccionalTipo);
        if (persona.getContacto() != null) {
            persona.getContacto().setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
            persona.getContacto().setPersona(persona);

        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        denunciante.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        denunciante.setFechaModificacion(DateUtil.currentCalendar());
        denuncianteDao.edit(denunciante);
    }

    @Override
    public Long crearDenunciante(DenuncianteTipo denuncianteTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (denuncianteTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        IdentificacionTipo id;

        if (denuncianteTipo.getPersonaJuridica() != null) {
            id = denuncianteTipo.getPersonaJuridica().getIdPersona();

        } else if (denuncianteTipo.getPersonaNatural() != null) {
            id = denuncianteTipo.getPersonaNatural().getIdPersona();

        } else {
            throw new AppException("Debe proporcionar el objeto a crear, personas vacias del denunciante");
        }

        Identificacion identificacion = mapper.map(id, Identificacion.class);

        Denunciante denunciante = denuncianteDao.findByIdentificacion(identificacion);

        if (denunciante != null) {
            throw new AppException("El denunciante ya existe");
        }

        Persona persona = personaDao.findByIdentificacion(identificacion);

        if (persona == null) {
            persona = new Persona();
            assembleEntidad(persona, denuncianteTipo, errorTipoList, contextoTransaccionalTipo);

            if (StringUtils.isNotBlank(denuncianteTipo.getCodTipoDenunciante())) {
                ValorDominio codDenunciante = valorDominioDao.find(denuncianteTipo.getCodTipoDenunciante());
                if (codDenunciante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El tipo denunciante proporcionado no existe con el ID: " + denuncianteTipo.getCodTipoDenunciante()));
                }
            }

            if (persona.getCodFuente() == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                        "El codFuente no puede ser nulo: "));
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            if (persona.getContacto() != null) {
                persona.getContacto().setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                persona.getContacto().setPersona(persona);
            }

            persona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

            Long idPersona = personaDao.create(persona);
            persona = personaDao.find(idPersona);

        }

        denunciante = new Denunciante(persona);
        denunciante.setIddenunciante(persona.getId());

        if (StringUtils.isNotBlank(denuncianteTipo.getCodTipoDenunciante())) {
            ValorDominio codDenunciante = valorDominioDao.find(denuncianteTipo.getCodTipoDenunciante());
            if (codDenunciante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo denunciante proporcionado no existe con el ID: " + denuncianteTipo.getCodTipoDenunciante()));
            } else {
                denunciante.setCodTipoDenunciante(codDenunciante);
            }
        }

        denunciante.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        denunciante.setFechaCreacion(DateUtil.currentCalendar());

        Long idDenunciante = denuncianteDao.create(denunciante);

        return idDenunciante;

    }

    public void assembleEntidad(final Persona persona, DenuncianteTipo servicio, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (servicio.getPersonaNatural() != null) {

            PersonaNaturalTipo personaNatural = servicio.getPersonaNatural();

            //Se coloca la nueva clasificacion persona
            if (StringUtils.isNotBlank(personaNatural.getCodTipoPersonaNatural())) {
                ValorDominio codTipoPersona = valorDominioDao.find(personaNatural.getCodTipoPersonaNatural());

                if (codTipoPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoPersona con el ID: " + personaNatural.getCodTipoPersonaNatural()));
                } else {

                    Dominio codClasificacionPersona = dominioDao.find(ClasificacionPersonaEnum.DENUNCIANTE.getCode());

                    if (codClasificacionPersona == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró la clasificación de la persona con el ID: " + ClasificacionPersonaEnum.DENUNCIANTE.getCode()));
                    } else {
                        ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                        clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                        clasificacionNuevaPersona.setPersona(persona);
                        clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                        persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                    }
                }
            }

            ValorDominio tipoPersona = valorDominioDao.find(TipoPersonaEnum.PERSONA_NATURAL.getCode());
            if (tipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo Persona proporcionado no existe con el ID: " + TipoPersonaEnum.PERSONA_NATURAL.getCode()));
            } else {
                persona.setCodNaturalezaPersona(tipoPersona);
            }
            if (StringUtils.isNotBlank(personaNatural.getCodFuente())) {
                ValorDominio codFuente = valorDominioDao.find(personaNatural.getCodFuente());
                if (codFuente == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codFuente proporcionado no existe con el ID: " + personaNatural.getCodFuente()));
                } else {
                    persona.setCodFuente(codFuente);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodEstadoCivil())) {
                ValorDominio codEstadoCivil = valorDominioDao.find(personaNatural.getCodEstadoCivil());
                if (codEstadoCivil == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codEstadoCivil proporcionado no existe con el ID: " + personaNatural.getCodNivelEducativo()));
                } else {
                    persona.setCodEstadoCivil(codEstadoCivil);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodNivelEducativo())) {
                ValorDominio nivelEducativo = valorDominioDao.find(personaNatural.getCodNivelEducativo());
                if (nivelEducativo == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El nivelEducativo proporcionado no existe con el ID: " + personaNatural.getCodNivelEducativo()));
                } else {
                    persona.setCodNivelEducativo(nivelEducativo);
                }
            }
            if (StringUtils.isNotBlank(personaNatural.getCodSexo())) {
                ValorDominio sexo = valorDominioDao.find(personaNatural.getCodSexo());
                if (sexo == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codSexo proporcionado no existe con el ID: " + personaNatural.getCodSexo()));
                } else {
                    persona.setCodSexo(sexo);
                }
            }
            if (personaNatural.getContacto() != null) {
                persona.setContacto(assembleEntidadContactoPersona(personaNatural.getContacto(), errorTipoList));
            }
            if (personaNatural.getIdAbogado() != null) {
                if (StringUtils.isBlank(personaNatural.getIdAbogado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdAbogado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del abogado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaNatural.getIdAbogado().getCodTipoIdentificacion(),
                            personaNatural.getIdAbogado().getValNumeroIdentificacion());
                    Persona abogado = personaDao.findByIdentificacion(identificacion);
                    if (abogado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El abogado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAbogado(abogado);
                    }
                }
            }
            if (personaNatural.getIdAutorizado() != null) {
                if (StringUtils.isBlank(personaNatural.getIdAutorizado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdAutorizado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaNatural.getIdAutorizado().getCodTipoIdentificacion(),
                            personaNatural.getIdAutorizado().getValNumeroIdentificacion());
                    Persona autorizado = personaDao.findByIdentificacion(identificacion);
                    if (autorizado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El autorizado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAutorizado(autorizado);
                    }
                }
            }
            if (personaNatural.getIdPersona() != null) {
                if (StringUtils.isBlank(personaNatural.getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaNatural.getIdPersona().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos de la persona estan vacios"));
                } else {
                    persona.setValNumeroIdentificacion(personaNatural.getIdPersona().getValNumeroIdentificacion());
                    ValorDominio tipoIdentificacion = valorDominioDao.find(personaNatural.getIdPersona().getCodTipoIdentificacion());
                    if (tipoIdentificacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El tipoIdentificacion proporcionado no existe con el ID: " + personaNatural.getIdPersona().getCodTipoIdentificacion()));
                    } else {
                        persona.setCodTipoIdentificacion(tipoIdentificacion);
                    }

                }
            }
            try {
                if (personaNatural.getIdPersona().getMunicipioExpedicion() != null) {
                    if (personaNatural.getIdPersona().getMunicipioExpedicion().getDepartamento() != null) {
                        String codMunicipio = personaNatural.getIdPersona().getMunicipioExpedicion().getCodMunicipio();
                        Municipio municipio = municipioDao.findByCodigo(codMunicipio);
                        if (municipio == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "El Municipio proporcionado no existe con el ID: " + codMunicipio));
                        } else {
                            persona.setMunicipio(municipio);
                        }
                    }

                }

            } catch (Exception e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al parsear los codigos de departamento y municipio o al consultarlos por favor verificar los datos " + e.getMessage()));
            }
            if (StringUtils.isNotBlank(personaNatural.getValPrimerNombre())) {
                persona.setValPrimerNombre(personaNatural.getValPrimerNombre());
            }
            if (StringUtils.isNotBlank(personaNatural.getValSegundoNombre())) {
                persona.setValSegundoNombre(personaNatural.getValSegundoNombre());
            }
            if (StringUtils.isNotBlank(personaNatural.getValPrimerApellido())) {
                persona.setValPrimerApellido(personaNatural.getValPrimerApellido());
            }
            if (StringUtils.isNotBlank(personaNatural.getValSegundoApellido())) {
                persona.setValSegundoApellido(personaNatural.getValSegundoApellido());
            }
            if (StringUtils.isNotBlank(personaNatural.getValNombreCompleto())) {
                persona.setValNombreRazonSocial(personaNatural.getValNombreCompleto());
            }

        } else if (servicio.getPersonaJuridica() != null) {

            PersonaJuridicaTipo personaJuridica = servicio.getPersonaJuridica();

            //Se coloca la nueva clasificacion persona
            if (StringUtils.isNotBlank(personaJuridica.getCodTipoPersonaJuridica())) {
                ValorDominio codTipoPersona = valorDominioDao.find(personaJuridica.getCodTipoPersonaJuridica());

                if (codTipoPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoPersona con el ID: " + personaJuridica.getCodTipoPersonaJuridica()));
                } else {

                    Dominio codClasificacionPersona = dominioDao.find(ClasificacionPersonaEnum.DENUNCIANTE.getCode());

                    if (codClasificacionPersona == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró la clasificación de la persona con el ID: " + ClasificacionPersonaEnum.DENUNCIANTE.getCode()));
                    } else {
                        ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                        clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                        clasificacionNuevaPersona.setPersona(persona);
                        clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                        persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                    }
                }
            }

            ValorDominio tipoPersona = valorDominioDao.find(TipoPersonaEnum.PERSONA_JURIDICA.getCode());
            if (tipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo Persona proporcionado no existe con el ID: " + TipoPersonaEnum.PERSONA_JURIDICA.getCode()));
            } else {
                persona.setCodNaturalezaPersona(tipoPersona);
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodFuente())) {
                ValorDominio codFuente = valorDominioDao.find(personaJuridica.getCodFuente());
                if (codFuente == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codFuente proporcionado no existe con el ID: " + personaJuridica.getCodFuente()));
                } else {
                    persona.setCodFuente(codFuente);
                }
            }
            if (personaJuridica.getContacto() != null) {
                persona.setContacto(assembleEntidadContactoPersona(personaJuridica.getContacto(), errorTipoList));
            }
            if (personaJuridica.getIdAbogado() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdAbogado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdAbogado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del abogado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdAbogado().getCodTipoIdentificacion(),
                            personaJuridica.getIdAbogado().getValNumeroIdentificacion());
                    Persona abogado = personaDao.findByIdentificacion(identificacion);
                    if (abogado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El abogado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAbogado(abogado);
                    }
                }
            }
            if (personaJuridica.getIdAutorizado() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdAutorizado().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdAutorizado().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdAutorizado().getCodTipoIdentificacion(),
                            personaJuridica.getIdAutorizado().getValNumeroIdentificacion());
                    Persona autorizado = personaDao.findByIdentificacion(identificacion);
                    if (autorizado == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El autorizado proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setAutorizado(autorizado);
                    }
                }
            }
            if (personaJuridica.getIdPersona() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdPersona().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdPersona().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos de la persona estan vacios"));
                } else {
                    persona.setValNumeroIdentificacion(personaJuridica.getIdPersona().getValNumeroIdentificacion());
                    ValorDominio tipoIdentificacion = valorDominioDao.find(personaJuridica.getIdPersona().getCodTipoIdentificacion());
                    if (tipoIdentificacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El tipoIdentificacion proporcionado no existe con el ID: " + personaJuridica.getIdPersona().getCodTipoIdentificacion()));
                    } else {
                        persona.setCodTipoIdentificacion(tipoIdentificacion);
                    }
                }
            }
            try {
                if (personaJuridica.getIdPersona().getMunicipioExpedicion() != null) {
                    if (personaJuridica.getIdPersona().getMunicipioExpedicion().getDepartamento() != null) {
                        String codMunicipio = personaJuridica.getIdPersona().getMunicipioExpedicion().getCodMunicipio();
                        Municipio municipio = municipioDao.findByCodigo(codMunicipio);
                        if (municipio == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "El Municipio proporcionado no existe con el ID: " + codMunicipio));
                        } else {
                            persona.setMunicipio(municipio);
                        }
                    }

                }

            } catch (Exception e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al parsear los codigos de departamento y municipio o al consultarlos por favor verificar los datos " + e.getMessage()));
            }
            if (personaJuridica.getIdRepresentanteLegal() != null) {
                if (StringUtils.isBlank(personaJuridica.getIdRepresentanteLegal().getCodTipoIdentificacion())
                        && StringUtils.isBlank(personaJuridica.getIdRepresentanteLegal().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los datos del autorizado estan vacios"));
                } else {
                    Identificacion identificacion = new Identificacion(personaJuridica.getIdRepresentanteLegal().getCodTipoIdentificacion(),
                            personaJuridica.getIdRepresentanteLegal().getValNumeroIdentificacion());
                    Persona representante = personaDao.findByIdentificacion(identificacion);
                    if (representante == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El representante proporcionado no existe con el ID: " + identificacion.toString()));
                    } else {
                        persona.setRepresentanteLegal(representante);
                    }
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getValNombreRazonSocial())) {
                persona.setValNombreRazonSocial(personaJuridica.getValNombreRazonSocial());
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodClaseActividadEconomica())) {
                ClaseCiiu codClaseAE = claseCiiuDao.find(Long.parseLong(personaJuridica.getCodClaseActividadEconomica()));
                if (codClaseAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodClaseActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodClaseActividadEconomica()));
                } else {
                    persona.setCodClaseActividadEconom(codClaseAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodDivisionActividadEconomica())) {
                DivisionCiiu codDivisionAE = divisionCiiuDao.find(Long.parseLong(personaJuridica.getCodDivisionActividadEconomica()));
                if (codDivisionAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodDivisionActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodDivisionActividadEconomica()));
                } else {
                    persona.setCodDivisionActividadEco(codDivisionAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodGrupoActividadEconomica())) {
                GrupoCiiu codGrupoAE = grupoCiiuDao.find(Long.parseLong(personaJuridica.getCodGrupoActividadEconomica()));
                if (codGrupoAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodGrupoActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodGrupoActividadEconomica()));
                } else {
                    persona.setCodGrupoActividadEconom(codGrupoAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getCodSeccionActividadEconomica())) {
                SeccionCiiu codSeccionAE = seccionCiiuDao.find(personaJuridica.getCodSeccionActividadEconomica());
                if (codSeccionAE == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El CodSeccionActividadEconomica proporcionado no existe con el ID: " + personaJuridica.getCodSeccionActividadEconomica()));
                } else {
                    persona.setCodSeccionActividadEcon(codSeccionAE);
                }
            }
            if (StringUtils.isNotBlank(personaJuridica.getValNumTrabajadores())) {
                persona.setValNumeroTrabajadores(Long.parseLong(personaJuridica.getValNumTrabajadores()));
            }

        }

    }

    private CorreoElectronicoAssembler correoElectronicoAssembler = CorreoElectronicoAssembler.getInstance();
    private MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    public ContactoPersona assembleEntidadContactoPersona(ContactoPersonaTipo servicio, final List<ErrorTipo> errorTipoList) throws AppException {

        ContactoPersona contacto = new ContactoPersona();

        if (servicio.getEsAutorizaNotificacionElectronica() != null) {
            contacto.setEsAutorizaNotificacionElectronica(Boolean.valueOf(servicio.getEsAutorizaNotificacionElectronica()));
        }
        if (servicio.getTelefonos() != null) {
            List<Telefono> telefonos = new ArrayList<Telefono>();
            for (TelefonoTipo teleTipo : servicio.getTelefonos()) {
                Telefono telefono = new Telefono();

                if (StringUtils.isNotBlank(teleTipo.getCodTipoTelefono())) {
                    ValorDominio codTipoTelefono = valorDominioDao.find(teleTipo.getCodTipoTelefono());
                    if (codTipoTelefono == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El codTipoTelefono proporcionado no existe con el ID: " + teleTipo.getCodTipoTelefono()));
                    } else {
                        telefono.setCodTipoTelefono(codTipoTelefono);
                    }
                }
                if (teleTipo.getValNumeroTelefono() != null) {
                    telefono.setValNumero(String.valueOf(teleTipo.getValNumeroTelefono()));
                }
                telefonos.add(telefono);
            }
            contacto.getTelefonoList().addAll(telefonos);
        }
        if (servicio.getUbicaciones() != null) {
            List<Ubicacion> ubicaciones = new ArrayList<Ubicacion>();
            for (UbicacionPersonaTipo ubicacionTipo : servicio.getUbicaciones()) {
                Ubicacion ubicacion = new Ubicacion();

                if (StringUtils.isNotBlank(ubicacionTipo.getCodTipoDireccion())) {
                    ValorDominio codTipoDireccion = valorDominioDao.find(ubicacionTipo.getCodTipoDireccion());
                    if (codTipoDireccion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El codTipoDireccion proporcionado no existe con el ID: " + ubicacionTipo.getCodTipoDireccion()));
                    } else {
                        ubicacion.setCodTipoDireccion(codTipoDireccion);
                    }
                }
                if (ubicacionTipo.getValDireccion() != null) {
                    ubicacion.setValDireccion(String.valueOf(ubicacionTipo.getValDireccion()));
                }
                if (ubicacionTipo.getMunicipio() != null) {
                    ubicacion.setMunicipio(municipioAssembler.assembleEntidad(ubicacionTipo.getMunicipio()));
                }
                ubicaciones.add(ubicacion);
            }
            contacto.getUbicacionList().addAll(ubicaciones);
        }
        if (servicio.getCorreosElectronicos() != null) {
            contacto.getCorreoElectronicoList().addAll(correoElectronicoAssembler.assembleCollectionEntidad(servicio.getCorreosElectronicos()));
        }

        return contacto;

    }

    public List<Ubicacion> assembleCollectionEntidadUbicacion(Collection<UbicacionPersonaTipo> arrayL, final List<ErrorTipo> errorTipoList) {

        List<Ubicacion> collection = new ArrayList<Ubicacion>();
        for (UbicacionPersonaTipo parcial : arrayL) {

            collection.add(assembleEntidadUbicacion(parcial, errorTipoList));
        }
        return collection;

    }

    public Ubicacion assembleEntidadUbicacion(UbicacionPersonaTipo servicio, final List<ErrorTipo> errorTipoList) {

        Ubicacion ubicacion = new Ubicacion();

        if (StringUtils.isNotBlank(servicio.getCodTipoDireccion())) {
            ValorDominio codTipoDireccion = null;
            try {
                codTipoDireccion = valorDominioDao.find(servicio.getCodTipoDireccion());
            } catch (AppException ex) {
                Logger.getLogger(DenuncianteFacadeImpl.class.getName()).log(Level.SEVERE, null, ex);
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Hubo un error al consultar el codTipoDireccion, ver log para mas informacion: " + servicio.getCodTipoDireccion()));
            }
            if (codTipoDireccion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoDireccion proporcionado no existe con el ID: " + servicio.getCodTipoDireccion()));
            } else {
                ubicacion.setCodTipoDireccion(codTipoDireccion);
            }
        }
        if (servicio.getMunicipio() != null) {
            ubicacion.setMunicipio(assembleEntidadMunicipio(servicio.getMunicipio(), errorTipoList));
        }
        if (StringUtils.isNotBlank(servicio.getValDireccion())) {
            ubicacion.setValDireccion(servicio.getValDireccion());
        }

        return ubicacion;

    }

    public Municipio assembleEntidadMunicipio(MunicipioTipo servicio, final List<ErrorTipo> errorTipoList) {

        Municipio municipio = null;
        try {
            if (servicio != null && servicio.getDepartamento() != null) {
                if (StringUtils.isNotBlank(servicio.getCodMunicipio())) {
                    municipio = municipioDao.findByCodigo(servicio.getCodMunicipio());
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Los datos del municipio son nulos idMunicipio: "));
            }

            if (municipio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El municipio no existe con los datos suministrados: " + servicio.getCodMunicipio() + " " + servicio.getDepartamento().getCodDepartamento()));
            }

        } catch (Exception ex) {
            Logger.getLogger(MunicipioAssembler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return municipio;
    }

}
