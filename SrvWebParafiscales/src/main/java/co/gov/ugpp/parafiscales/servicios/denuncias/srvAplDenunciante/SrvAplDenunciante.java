package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplDenunciante;

import co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpActualizarDenuncianteFallo;
import co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpBuscarPorIdDenuncianteFallo;
import co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpCrearDenuncianteFallo;
import co.gov.ugpp.denuncias.srvapldenunciante.v1.OpActualizarDenuncianteRespTipo;
import co.gov.ugpp.denuncias.srvapldenunciante.v1.OpBuscarPorIdDenuncianteRespTipo;
import co.gov.ugpp.denuncias.srvapldenunciante.v1.OpCrearDenuncianteRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplDenunciante",
        portName = "portSrvAplDenuncianteSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvapldenunciante.v1.PortSrvAplDenuncianteSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplDenunciante/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplDenunciante extends AbstractSrvApl {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplDenunciante.class);
    
    @EJB
    private DenuncianteFacade denuncianteFacade;
    
    public co.gov.ugpp.denuncias.srvapldenunciante.v1.OpBuscarPorIdDenuncianteRespTipo opBuscarPorIdDenunciante(co.gov.ugpp.denuncias.srvapldenunciante.v1.OpBuscarPorIdDenuncianteSolTipo msjOpBuscarPorIdDenuncianteSol) throws co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpBuscarPorIdDenuncianteFallo {
        LOG.info("Op: opBuscarPorIdDenunciante ::: INIT");
        
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdDenuncianteSol.getContextoTransaccional();
        
        List<DenuncianteTipo> denunciantes;
        
        try {
            
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            
            denunciantes = denuncianteFacade.buscarPorIdDenunciante(
                    msjOpBuscarPorIdDenuncianteSol.getIdentificacion(), contextoSolicitud);
            
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdDenunciante ::: END");
        
        OpBuscarPorIdDenuncianteRespTipo buscarPorIdDenuncianteRespTipo = new OpBuscarPorIdDenuncianteRespTipo();
        buscarPorIdDenuncianteRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdDenuncianteRespTipo.getDenunciante().addAll(denunciantes);
        
        return buscarPorIdDenuncianteRespTipo;
    }
    
    public co.gov.ugpp.denuncias.srvapldenunciante.v1.OpActualizarDenuncianteRespTipo opActualizarDenunciante(co.gov.ugpp.denuncias.srvapldenunciante.v1.OpActualizarDenuncianteSolTipo msjOpActualizarDenuncianteSol) throws co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpActualizarDenuncianteFallo {
        LOG.info("Op: opActualizarDenunciante ::: INIT");
        
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarDenuncianteSol.getContextoTransaccional();
        
        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            
            denuncianteFacade.actualizarDenunciante(msjOpActualizarDenuncianteSol.getDenunciante(),
                    contextoSolicitud);
            
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarDenunciante ::: END");
        
        OpActualizarDenuncianteRespTipo actualizarDenuncianteRespTipo = new OpActualizarDenuncianteRespTipo();
        actualizarDenuncianteRespTipo.setContextoRespuesta(contextoRespuesta);
        
        return actualizarDenuncianteRespTipo;
    }
    
    public co.gov.ugpp.denuncias.srvapldenunciante.v1.OpCrearDenuncianteRespTipo opCrearDenunciante(co.gov.ugpp.denuncias.srvapldenunciante.v1.OpCrearDenuncianteSolTipo msjOpCrearDenuncianteSol) throws co.gov.ugpp.denuncias.srvapldenunciante.v1.MsjOpCrearDenuncianteFallo {
        LOG.info("Op: opCrearDenunciante ::: INIT");
        
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearDenuncianteSol.getContextoTransaccional();
        Long idDenunciante;
        try {
            
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            
            idDenunciante = denuncianteFacade.crearDenunciante(msjOpCrearDenuncianteSol.getDenunciante(),
                    contextoSolicitud);
            
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearDenuncianteFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opCrearDenunciante ::: END");
        
        OpCrearDenuncianteRespTipo crearDenuncianteRespTipo = new OpCrearDenuncianteRespTipo();
        crearDenuncianteRespTipo.setContextoRespuesta(contextoRespuesta);
        crearDenuncianteRespTipo.setIdDenunciante(String.valueOf(idDenunciante));
        return crearDenuncianteRespTipo;
    }
    
}
