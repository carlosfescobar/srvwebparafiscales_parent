package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplEntidadExterna;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface EntidadExternaFacade extends Serializable{
    
    public EntidadExternaTipo buscarPorIdEntidadExterna(final IdentificacionTipo identificacionTipo )throws AppException;

    public PagerData<EntidadExternaTipo> buscarPorCriteriosEntidadExterna(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina)throws AppException;
    
    
}
