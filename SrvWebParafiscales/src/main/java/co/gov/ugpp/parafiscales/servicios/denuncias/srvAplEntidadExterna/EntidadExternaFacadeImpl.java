package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplEntidadExterna;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class EntidadExternaFacadeImpl extends AbstractFacade implements EntidadExternaFacade {


    @EJB
    private EntidadExternaDao entidadExternaDao;

    @Override
    public EntidadExternaTipo buscarPorIdEntidadExterna(final IdentificacionTipo identificacionTipo) throws AppException{

        if(identificacionTipo == null ||
                StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion()) ||
                StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion())){
            throw new AppException("Debe proporcionar los parametros de busqueda de los objetos a consultar");
        }
        
        Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
        
        EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

        return mapper.map(entidadExterna, EntidadExternaTipo.class);
    }

    @Override
    public PagerData<EntidadExternaTipo> buscarPorCriteriosEntidadExterna(List<ParametroTipo> parametro, List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina) throws AppException {
        
        final PagerData<EntidadExterna> pagerDataEntity = 
                entidadExternaDao.findPorCriterios(parametro, criteriosOrdenamiento, valTamPagina, valNumPagina);

        final List<EntidadExternaTipo> entidadeTipoList = mapper.map(pagerDataEntity.getData(), EntidadExterna.class,
                EntidadExternaTipo.class);

        return new PagerData<EntidadExternaTipo>(entidadeTipoList, pagerDataEntity.getNumPages());
    }


}
