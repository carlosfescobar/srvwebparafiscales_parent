package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplEntidadExterna;

import co.gov.ugpp.denuncias.srvaplentidadexterna.v1.MsjOpBuscarPorCriteriosEntidadExternaFallo;
import co.gov.ugpp.denuncias.srvaplentidadexterna.v1.MsjOpBuscarPorIdEntidadExternaFallo;
import co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorCriteriosEntidadExternaRespTipo;
import co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorIdEntidadExternaRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplEntidadExterna",
        portName = "portSrvAplEntidadExternaSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvaplentidadexterna.v1.PortSrvAplEntidadExternaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplEntidadExterna/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplEntidadExterna extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplEntidadExterna.class);

    @EJB
    private EntidadExternaFacade entidadExternaFacade;

    public co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorIdEntidadExternaRespTipo opBuscarPorIdEntidadExterna(co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorIdEntidadExternaSolTipo msjOpBuscarPorIdEntidadExternaSol) throws MsjOpBuscarPorIdEntidadExternaFallo {
        LOG.info("Op: opBuscarPorIdEntidadExterna ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdEntidadExternaSol.getContextoTransaccional();
        final IdentificacionTipo identificacionTipo = msjOpBuscarPorIdEntidadExternaSol.getIdentificacion();

        EntidadExternaTipo entidadExternaTipo;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            entidadExternaTipo = entidadExternaFacade.buscarPorIdEntidadExterna(identificacionTipo);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdEntidadExterna ::: END");

        OpBuscarPorIdEntidadExternaRespTipo buscarPorIdEntidadExternaRespTipo = new OpBuscarPorIdEntidadExternaRespTipo();
        buscarPorIdEntidadExternaRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdEntidadExternaRespTipo.setEntidadExterna(entidadExternaTipo);

        return buscarPorIdEntidadExternaRespTipo;
    }

    public co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorCriteriosEntidadExternaRespTipo opBuscarPorCriteriosEntidadExterna(co.gov.ugpp.denuncias.srvaplentidadexterna.v1.OpBuscarPorCriteriosEntidadExternaSolTipo msjOpBuscarPorCriteriosEntidadExternaSol) throws MsjOpBuscarPorCriteriosEntidadExternaFallo {

        LOG.info("Op: opBuscarPorCriteriosEntidadExterna ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorCriteriosEntidadExternaSol.getContextoTransaccional();

        final PagerData<EntidadExternaTipo> entidadesExternas;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            entidadesExternas = entidadExternaFacade.buscarPorCriteriosEntidadExterna(msjOpBuscarPorCriteriosEntidadExternaSol.getParametro(),
                    contextoSolicitud.getCriteriosOrdenamiento(),
                    contextoSolicitud.getValTamPagina(),
                    contextoSolicitud.getValNumPagina());

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosEntidadExternaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        LOG.info("Op: opBuscarPorCriteriosEntidadExterna ::: END");

        OpBuscarPorCriteriosEntidadExternaRespTipo buscarPorCriteriosEntidadExternaRespTipo = new OpBuscarPorCriteriosEntidadExternaRespTipo();
        contextoRespuesta.setValCantidadPaginas(entidadesExternas.getNumPages());
        buscarPorCriteriosEntidadExternaRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosEntidadExternaRespTipo.getEntidadExterna().addAll(entidadesExternas.getData());

        return buscarPorCriteriosEntidadExternaRespTipo;
    }

}
