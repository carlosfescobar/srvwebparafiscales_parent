package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplTraslado;

import co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpActualizarTrasladoFallo;
import co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpBuscarPorCriteriosTrasladoFallo;
import co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpCrearTrasladoFallo;
import co.gov.ugpp.denuncias.srvapltraslado.v1.OpActualizarTrasladoRespTipo;
import co.gov.ugpp.denuncias.srvapltraslado.v1.OpBuscarPorCriteriosTrasladoRespTipo;
import co.gov.ugpp.denuncias.srvapltraslado.v1.OpCrearTrasladoRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplTraslado",
        portName = "portSrvAplTrasladoSOAP",
        endpointInterface = "co.gov.ugpp.denuncias.srvapltraslado.v1.PortSrvAplTrasladoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Denuncias/SrvAplTraslado/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplTraslado extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplTraslado.class);

    @EJB
    private TrasladoFacade trasladoFacade;

    public co.gov.ugpp.denuncias.srvapltraslado.v1.OpActualizarTrasladoRespTipo opActualizarTraslado(co.gov.ugpp.denuncias.srvapltraslado.v1.OpActualizarTrasladoSolTipo msjOpActualizarTrasladoSol) throws co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpActualizarTrasladoFallo {
        LOG.info("Op: opActualizarTraslado ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarTrasladoSol.getContextoTransaccional();

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            trasladoFacade.actualizarTraslado(msjOpActualizarTrasladoSol.getTraslado(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrasladoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarTrasladoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarTraslado ::: END");

        OpActualizarTrasladoRespTipo actualizarTrasladoRespTipo = new OpActualizarTrasladoRespTipo();
        actualizarTrasladoRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarTrasladoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvapltraslado.v1.OpCrearTrasladoRespTipo opCrearTraslado(co.gov.ugpp.denuncias.srvapltraslado.v1.OpCrearTrasladoSolTipo msjOpCrearTrasladoSol) throws co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpCrearTrasladoFallo {
        LOG.info("Op: opActualizarTraslado ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearTrasladoSol.getContextoTransaccional();
        Long idTraslado;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            idTraslado = trasladoFacade.crearTraslado(msjOpCrearTrasladoSol.getTraslado(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrasladoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearTrasladoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opActualizarTraslado ::: END");

        OpCrearTrasladoRespTipo crearTrasladoRespTipo = new OpCrearTrasladoRespTipo();
        crearTrasladoRespTipo.setContextoRespuesta(contextoRespuesta);
        crearTrasladoRespTipo.setIdTraslado(String.valueOf(idTraslado));

        return crearTrasladoRespTipo;
    }

    public co.gov.ugpp.denuncias.srvapltraslado.v1.OpBuscarPorCriteriosTrasladoRespTipo opBuscarPorCriteriosTraslado(co.gov.ugpp.denuncias.srvapltraslado.v1.OpBuscarPorCriteriosTrasladoSolTipo msjOpBuscarPorCriteriosTrasladoSolTipo) throws co.gov.ugpp.denuncias.srvapltraslado.v1.MsjOpBuscarPorCriteriosTrasladoFallo {
        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpBuscarPorCriteriosTrasladoSolTipo.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosTrasladoSolTipo.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = contextoTransaccionalTipo.getCriteriosOrdenamiento();
        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();
        final PagerData<TrasladoTipo> TrasladoTipoPagerData;

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());
            TrasladoTipoPagerData = trasladoFacade.buscarPorCriteriosTraslado(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosTrasladoFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosTrasladoFallo("Error", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpBuscarPorCriteriosTrasladoRespTipo resp = new OpBuscarPorCriteriosTrasladoRespTipo();
        cr.setValCantidadPaginas(TrasladoTipoPagerData.getNumPages());
        resp.setContextoRespuesta(cr);
        resp.getTraslado().addAll(TrasladoTipoPagerData.getData());

        return resp;
    }
}
