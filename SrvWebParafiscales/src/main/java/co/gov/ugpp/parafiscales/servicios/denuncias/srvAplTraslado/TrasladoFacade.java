package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplTraslado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface TrasladoFacade extends Serializable {

    public void actualizarTraslado(TrasladoTipo traslado, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public Long crearTraslado(TrasladoTipo traslado, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<TrasladoTipo> buscarPorCriteriosTraslado(final List<ParametroTipo> parametroTipoList,
            final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
}
