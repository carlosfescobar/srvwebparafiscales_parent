package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplTraslado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.DenunciaDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.TrasladoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Traslado;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.servicios.helpers.AportanteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class TrasladoFacadeImpl extends AbstractFacade implements TrasladoFacade {

    @EJB
    private TrasladoDao trasladoDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private DenunciaDao denunciaDao;

    AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();

    @Override
    public void actualizarTraslado(TrasladoTipo trasladoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (trasladoTipo == null || StringUtils.isBlank(trasladoTipo.getIdTraslado())) {
            throw new AppException("Debe proporcionar el objeto a actualizar, es probable el id este vacio");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Traslado traslado = trasladoDao.find(Long.parseLong(trasladoTipo.getIdTraslado()));

        if (traslado == null) {
            throw new AppException("No se encontró un traslado con el valor:" + trasladoTipo.getIdTraslado());
        }

        assembleEntidadUpdate(traslado, trasladoTipo, errorTipoList, contextoTransaccionalTipo);
        traslado.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        traslado.setFechaModificacion(DateUtil.currentCalendar());

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        trasladoDao.edit(traslado);
    }

    @Override
    public Long crearTraslado(TrasladoTipo trasladoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (trasladoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Traslado traslado = new Traslado();
        traslado.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        traslado.setFechaCreacion(DateUtil.currentCalendar());
        assembleEntidad(traslado, trasladoTipo, errorTipoList, contextoTransaccionalTipo);

        if (trasladoTipo.getExpediente() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El valor de Expediente no puede ser nulo: " + trasladoTipo.getExpediente()));
            if (StringUtils.isBlank(trasladoTipo.getExpediente().getIdNumExpediente())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor de Expediente no puede ser vacio: " + trasladoTipo.getExpediente().getIdNumExpediente()));
            }
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idTraslado = trasladoDao.create(traslado);

        return idTraslado;
    }

    private void assembleEntidadUpdate(final Traslado traslado, TrasladoTipo trasladoTipo, List<ErrorTipo> errorTipoList,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (!StringUtils.isBlank(trasladoTipo.getDescMotivoTraslado())) {
            traslado.setDescMotivoTraslado(trasladoTipo.getDescMotivoTraslado());
        }
        if (!StringUtils.isBlank(trasladoTipo.getIdRadicadoDocumentoEntidadExterna())) {
            traslado.setIdRadicadoDocumentoEntidadExterna(trasladoTipo.getIdRadicadoDocumentoEntidadExterna());
        }
        if (trasladoTipo.getFechaRadicadoDocumentoEntidadExterna() != null) {
            traslado.setFechaRadicadoDocumentoEntidadExterna(trasladoTipo.getFechaRadicadoDocumentoEntidadExterna());
        }

        if (!StringUtils.isBlank(trasladoTipo.getCodEstadoTraslado())) {
            ValorDominio estadoTraslado = valorDominioDao.find(trasladoTipo.getCodEstadoTraslado());
            if (estadoTraslado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio estadoTraslado proporcionado no existe con el ID: " + trasladoTipo.getCodEstadoTraslado()));
            } else {
                traslado.setEstadoTraslado(trasladoTipo.getCodEstadoTraslado());
            }
        }
        if (trasladoTipo.getFecTraslado() != null) {
              traslado.setFecTraslado(trasladoTipo.getFecTraslado()); 
            /*
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().toString());
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.YEAR));
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.MONTH));
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.DATE));
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.HOUR));
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.MINUTE));
            System.out.println("::ANDRES1:: " + trasladoTipo.getFecTraslado().get(Calendar.ZONE_OFFSET));
            */
            
            //Calendar horaEspecifica = trasladoTipo.getFecTraslado();
            //horaEspecifica.set(Calendar.HOUR, 1);
            //horaEspecifica.setTimeZone(TimeZone.getTimeZone("-5"));
            
            /*
            System.out.println("::ANDRES2:: " + horaEspecifica.toString());
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.YEAR));
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.MONTH));
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.DATE));
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.HOUR));
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.MINUTE));
            System.out.println("::ANDRES2:: " + horaEspecifica.get(Calendar.ZONE_OFFSET));
            */
            
            //traslado.setFecTraslado(horaEspecifica);   
         
        }
        if (StringUtils.isNotBlank(trasladoTipo.getIdTraslado())) {
            traslado.setId(Long.parseLong(trasladoTipo.getIdTraslado()));
        }
        if (trasladoTipo.getExpediente() != null) {
            Expediente expediente = expedienteDao.find(trasladoTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente proporcionado no existe con el ID: " + trasladoTipo.getExpediente().getIdNumExpediente()));
            } else {
                traslado.setIdNumeroExpediente(expediente.getId());
            }
        }
        if (trasladoTipo.getFuncionario() != null) {
            final FuncionarioTipo funcionarioTipo
                    = funcionarioFacade.buscarFuncionario(trasladoTipo.getFuncionario(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        "El (Funcionario) proporcinado no existe en el LDAP: "
                        + (StringUtils.isNotBlank(trasladoTipo.getFuncionario().getIdFuncionario()) ? trasladoTipo.getFuncionario().getIdFuncionario()
                        : "No se adjunta el id para buscar el funcionario")));
            } else {
                traslado.setFuncionarioId(funcionarioTipo.getIdFuncionario());
            }
        }
        if (trasladoTipo.getEntidadExterna() != null
                && trasladoTipo.getEntidadExterna().getIdPersona() != null) {

            IdentificacionTipo identificacionTipo = trasladoTipo.getEntidadExterna().getIdPersona();
            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una EntidadExterna con la identificación:" + identificacion));
            } else {
                traslado.setEntidadExterna(entidadExterna);
            }
        }
        if (StringUtils.isNotBlank(trasladoTipo.getIdDocumentoTrasladoDenunciante())) {
            DocumentoEcm documentoTrasladoDenunciante = documentoDao.find(trasladoTipo.getIdDocumentoTrasladoDenunciante());
            if (documentoTrasladoDenunciante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el documentoTrasladoDenunciate con el ID: " + trasladoTipo.getIdDocumentoTrasladoDenunciante()));
            } else {
                traslado.setDocumentoTrasladoDenunciante(documentoTrasladoDenunciante);
            }
        }

        if (StringUtils.isNotBlank(trasladoTipo.getIdDocumentoTrasladoEntidadExterna())) {
            DocumentoEcm documentoTrasladoEntidadExterna = documentoDao.find(trasladoTipo.getIdDocumentoTrasladoEntidadExterna());
            if (documentoTrasladoEntidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el documentoTrasladoEntidadExterna con el ID: " + trasladoTipo.getIdDocumentoTrasladoEntidadExterna()));
            } else {
                traslado.setDocumentoTrasladoEntidadExterna(documentoTrasladoEntidadExterna);
            }
        }
        if (trasladoTipo.getDenuncia() != null
                && StringUtils.isNotBlank(trasladoTipo.getDenuncia().getIdDenuncia())) {
            Denuncia denuncia = denunciaDao.find(Long.valueOf(trasladoTipo.getDenuncia().getIdDenuncia()));
            if (denuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado la denuncia con el ID: " + trasladoTipo.getDenuncia().getIdDenuncia()));
            } else {
                traslado.setDenuncia(denuncia);
            }
        }
    }

    private void assembleEntidad(final Traslado traslado, TrasladoTipo trasladoTipo, List<ErrorTipo> errorTipoList,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (!StringUtils.isBlank(trasladoTipo.getDescMotivoTraslado())) {
            traslado.setDescMotivoTraslado(trasladoTipo.getDescMotivoTraslado());
        }
        if (!StringUtils.isBlank(trasladoTipo.getIdRadicadoDocumentoEntidadExterna())) {
            traslado.setIdRadicadoDocumentoEntidadExterna(trasladoTipo.getIdRadicadoDocumentoEntidadExterna());
        }
        if (trasladoTipo.getFechaRadicadoDocumentoEntidadExterna() != null) {
            traslado.setFechaRadicadoDocumentoEntidadExterna(trasladoTipo.getFechaRadicadoDocumentoEntidadExterna());
        }
        if (!StringUtils.isBlank(trasladoTipo.getCodEstadoTraslado())) {
            ValorDominio estadoTraslado = valorDominioDao.find(trasladoTipo.getCodEstadoTraslado());
            if (estadoTraslado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio estadoTraslado proporcionado no existe con el ID: " + trasladoTipo.getCodEstadoTraslado()));
            } else {
                traslado.setEstadoTraslado(trasladoTipo.getCodEstadoTraslado());
            }
        } else {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El valor codestadoTraslado proporcionado no puede ser vacio o nulo: " + trasladoTipo.getCodEstadoTraslado()));
        }
        if (trasladoTipo.getFecTraslado() != null) {
            traslado.setFecTraslado(trasladoTipo.getFecTraslado());
        } else {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El valor de la fecha proporcionado no puede ser vacio o nulo: " + trasladoTipo.getFecTraslado()));
        }
        if (StringUtils.isNotBlank(trasladoTipo.getIdTraslado())) {
            traslado.setId(Long.parseLong(trasladoTipo.getIdTraslado()));
        }
        if (trasladoTipo.getExpediente() != null) {
            Expediente expediente = expedienteDao.find(trasladoTipo.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente proporcionado no existe con el ID: " + trasladoTipo.getExpediente().getIdNumExpediente()));
            } else {
                traslado.setIdNumeroExpediente(expediente.getId());
            }
        }
        if (trasladoTipo.getFuncionario() != null) {
            final FuncionarioTipo funcionarioTipo
                    = funcionarioFacade.buscarFuncionario(trasladoTipo.getFuncionario(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        "El (Funcionario) proporcinado no existe en el LDAP: "
                        + (StringUtils.isNotBlank(trasladoTipo.getFuncionario().getIdFuncionario()) ? trasladoTipo.getFuncionario().getIdFuncionario()
                        : "No se adjunta el id para buscar el funcionario")));
            } else {
                traslado.setFuncionarioId(funcionarioTipo.getIdFuncionario());
            }
        } else {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El id funcionario no puede ser vacio o nulo: " + trasladoTipo.getFuncionario()));
        }
        if (trasladoTipo.getEntidadExterna() != null
                && trasladoTipo.getEntidadExterna().getIdPersona() != null) {

            IdentificacionTipo identificacionTipo = trasladoTipo.getEntidadExterna().getIdPersona();
            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacion);

            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una EntidadExterna con la identificación:" + identificacion));
            } else {
                traslado.setEntidadExterna(entidadExterna);
            }
        }
        if (StringUtils.isNotBlank(trasladoTipo.getIdDocumentoTrasladoDenunciante())) {
            DocumentoEcm documentoTrasladoDenunciante = documentoDao.find(trasladoTipo.getIdDocumentoTrasladoDenunciante());
            if (documentoTrasladoDenunciante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el documentoTrasladoDenunciate con el ID: " + trasladoTipo.getIdDocumentoTrasladoDenunciante()));
            } else {
                traslado.setDocumentoTrasladoDenunciante(documentoTrasladoDenunciante);
            }
        }

        if (StringUtils.isNotBlank(trasladoTipo.getIdDocumentoTrasladoEntidadExterna())) {
            DocumentoEcm documentoTrasladoEntidadExterna = documentoDao.find(trasladoTipo.getIdDocumentoTrasladoEntidadExterna());
            if (documentoTrasladoEntidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado el documentoTrasladoEntidadExterna con el ID: " + trasladoTipo.getIdDocumentoTrasladoEntidadExterna()));
            } else {
                traslado.setDocumentoTrasladoEntidadExterna(documentoTrasladoEntidadExterna);
            }
        }
        if (trasladoTipo.getDenuncia() != null
                && StringUtils.isNotBlank(trasladoTipo.getDenuncia().getIdDenuncia())) {
            Denuncia denuncia = denunciaDao.find(Long.valueOf(trasladoTipo.getDenuncia().getIdDenuncia()));
            if (denuncia == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se ha encontrado la denuncia con el ID: " + trasladoTipo.getDenuncia().getIdDenuncia()));
            } else {
                traslado.setDenuncia(denuncia);
            }
        }
    }

    @Override
    public PagerData<TrasladoTipo> buscarPorCriteriosTraslado(List<ParametroTipo> parametroTipoList,
            List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<Traslado> pagerDataEntity
                = trasladoDao.findPorCriterios(parametroTipoList, criterioOrdenamientoTipos, contextoTransaccionalTipo.getValTamPagina(), contextoTransaccionalTipo.getValNumPagina());

        final List<TrasladoTipo> trasladoTipoList = new ArrayList<TrasladoTipo>();

        for (final Traslado traslado : pagerDataEntity.getData()) {
            
            TrasladoTipo trasladoTipo = mapper.map(traslado, TrasladoTipo.class);
            
            if (trasladoTipo != null && trasladoTipo.getFecTraslado() != null)
            {        
                Calendar cal = Calendar.getInstance();
                cal.set(trasladoTipo.getFecTraslado().get(Calendar.YEAR), trasladoTipo.getFecTraslado().get(Calendar.MONTH),  trasladoTipo.getFecTraslado().get(Calendar.DAY_OF_MONTH), 0, 0, 0);
                trasladoTipo.setFecTraslado(cal);
            }
            
            if (trasladoTipo != null && trasladoTipo.getFuncionario() != null && StringUtils.isNotBlank(trasladoTipo.getFuncionario().getIdFuncionario()))
            {
                FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(trasladoTipo.getFuncionario(), contextoTransaccionalTipo);
                trasladoTipo.setFuncionario(funcionarioTipo);
            }
            
            trasladoTipoList.add(trasladoTipo);
        }
        return new PagerData(trasladoTipoList, pagerDataEntity.getNumPages());
    }
}
