package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplUbicacionPersona;

import co.gov.ugpp.comun.srvaplubicacionpersona.v1.MsjOpBuscarPorCriteriosUbicacionPersonaFallo;
import co.gov.ugpp.comun.srvaplubicacionpersona.v1.OpBuscarPorCriteriosUbicacionPersonaRespTipo;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplUbicacionPersona",
        portName = "portSrvAplUbicacionPersonaSOAP",
        endpointInterface = "co.gov.ugpp.comun.srvaplubicacionpersona.v1.PortSrvAplUbicacionPersonaSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Comun/SrvAplUbicacionPersona/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplUbicacionPersona extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplUbicacionPersona.class);

    @EJB
    private UbicacionPersonaFacade ubicacionPersonaFacade;

    public co.gov.ugpp.comun.srvaplubicacionpersona.v1.OpBuscarPorCriteriosUbicacionPersonaRespTipo opBuscarPorCriteriosUbicacionPersona(co.gov.ugpp.comun.srvaplubicacionpersona.v1.OpBuscarPorCriteriosUbicacionPersonaSolTipo msjOpBuscarPorCriteriosUbicacionPersonaSol) throws MsjOpBuscarPorCriteriosUbicacionPersonaFallo {
        LOG.info("Op: opBuscarPorCriteriosUbicacionPersona ::: INIT");

        final ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorCriteriosUbicacionPersonaSol.getContextoTransaccional();
        final List<ParametroTipo> parametroTipoList = msjOpBuscarPorCriteriosUbicacionPersonaSol.getParametro();
        final List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos = msjOpBuscarPorCriteriosUbicacionPersonaSol.getContextoTransaccional().getCriteriosOrdenamiento();

        PagerData<UbicacionPersonaTipo> ubicaciones;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            ubicaciones = ubicacionPersonaFacade.buscarPorCriteriosUbicacionPersona(parametroTipoList, criterioOrdenamientoTipos, contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosUbicacionPersonaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosUbicacionPersonaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpBuscarPorCriteriosUbicacionPersonaRespTipo buscarPorCriteriosUbicacionPersonaRespTipo = new OpBuscarPorCriteriosUbicacionPersonaRespTipo();

        contextoRespuesta.setValCantidadPaginas(ubicaciones.getNumPages());
        buscarPorCriteriosUbicacionPersonaRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosUbicacionPersonaRespTipo.getUbicacionPersona().addAll(ubicaciones.getData());

        LOG.info("Op: opBuscarPorCriteriosUbicacionPersona ::: END");

        return buscarPorCriteriosUbicacionPersonaRespTipo;
    }

}
