package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplUbicacionPersona;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface UbicacionPersonaFacade extends Serializable {

    /**
     *
     * @param parametro
     * @param criteriosOrdenamiento
     * @param valNumPagina
     * @param valTamPagina
     * @param contextoRespuesta
     * @return
     * @throws AppException
     */
    public List<UbicacionPersonaTipo> buscarPorCriteriosUbicacionPersona(List<ParametroTipo> parametro, List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valNumPagina, Long valTamPagina, ContextoRespuestaTipo contextoRespuesta) throws AppException;

    public PagerData<UbicacionPersonaTipo> buscarPorCriteriosUbicacionPersona(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoSolicitud) throws AppException;

}
