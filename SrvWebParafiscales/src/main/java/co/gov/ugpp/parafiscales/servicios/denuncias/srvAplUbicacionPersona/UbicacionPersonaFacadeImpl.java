package co.gov.ugpp.parafiscales.servicios.denuncias.srvAplUbicacionPersona;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.departamentotipo.v1.DepartamentoTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.TipoDireccionEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ContactoDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.dao.UbicacionDao;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.servicios.helpers.MunicipioAssembler;
import co.gov.ugpp.parafiscales.servicios.util.SQLConection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class UbicacionPersonaFacadeImpl extends AbstractFacade implements UbicacionPersonaFacade {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplUbicacionPersona.class);

    private SQLConection sqlConection = SQLConection.getInstance();

    @EJB
    private UbicacionDao ubicacionDao;

    @EJB
    private ContactoDao contactoDao;

    @EJB
    private MunicipioDao municipioDao;

    private MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    @Override
    public List<UbicacionPersonaTipo> buscarPorCriteriosUbicacionPersona(List<ParametroTipo> parametro, List<CriterioOrdenamientoTipo> criteriosOrdenamiento,
            Long valNumPagina, Long valTamPagina, ContextoRespuestaTipo contextoRespuesta) throws AppException {

        List<UbicacionPersonaTipo> listaUbicacionPersona = new ArrayList<UbicacionPersonaTipo>();

        ResultSet result;

        Integer tamPagina = null;
        Integer numPagina = null;

        List<ParametroTipo> parametros = new ArrayList<ParametroTipo>();

        try {
            if (valTamPagina != null) {
                tamPagina = valTamPagina.intValue();
            }
            if (valNumPagina != null) {
                numPagina = valNumPagina.intValue();
            }

            String filtroTipoDireccion = "";

            for (ParametroTipo param : parametro) {

                if (param.getIdLlave().contains("cod_tipo_direccion")) {
                    if (param.getValValor().contains(TipoDireccionEnum.RUT.getCode())) {
                        filtroTipoDireccion = TipoDireccionEnum.RUT.getCode();

                    } else if (param.getValValor().contains(TipoDireccionEnum.PILA.getCode())) {
                        filtroTipoDireccion = TipoDireccionEnum.PILA.getCode();

                    } else if (param.getValValor().contains(TipoDireccionEnum.RUE.getCode())) {
                        filtroTipoDireccion = TipoDireccionEnum.RUE.getCode();

                    } else if (param.getValValor().contains(TipoDireccionEnum.PROCESAL.getCode())) {
                        filtroTipoDireccion = TipoDireccionEnum.PROCESAL.getCode();

                    } else {
                        throw new AppException("El cod_tipo_direccion solo esta parametrizado para los siguientes valores: " + TipoDireccionEnum.RUT.getCode() + " ó " + TipoDireccionEnum.PILA.getCode() + " ó " + TipoDireccionEnum.RUE.getCode());
                    }

                } else {
                    parametros.add(param);
                }

            }

            result = sqlConection
                    .ubicacionPersona(parametros, criteriosOrdenamiento, tamPagina, numPagina);

            while (result.next()) {

                String codDepartamento = "";// C��digo del departamento SI
                // OBLIGATORIO
                String nombreDepartamento = "";// Nombre del departamento NO
                // OBLIGATORIO

                String codMunicipio = "";// C��digo del municipio NO OBLIGATORIO
                String nombreMunicipio = "";// Nombre del municipio NO OBLIGATORIO

                String direccion = "";// Descripci��n de la direcci��n NO OBLIGATORIO
                String codTipoDireccion = "";// Tipo de direcci��n (Procesal, RUT,
                // otra) NO OBLIGATORIO

                if (StringUtils.isNoneBlank(filtroTipoDireccion)) {

                    if (filtroTipoDireccion.equals(TipoDireccionEnum.RUT.getCode())) {

                        if (result.getString("direccion_rut_dian") != null && result.getString("municipio_rut_dian") != null
                                && result.getString("departamento_rut_dian") != null) {
                            codDepartamento = result.getString("departamento_rut_dian");

                            codMunicipio = result.getString("municipio_rut_dian");

                            direccion = result.getString("direccion_rut_dian");

                            codTipoDireccion = TipoDireccionEnum.RUT.getCode();

                        }
                    } else if (filtroTipoDireccion.equals(TipoDireccionEnum.PILA.getCode())) {

                        if (result.getString("direccion_pila") != null && result.getString("codigo_ciudad_pila") != null
                                && result.getString("codigo_depto_pila") != null) {

                            codDepartamento = result.getString("codigo_depto_pila");

                            codMunicipio = result.getString("codigo_ciudad_pila");

                            direccion = result.getString("direccion_pila");

                            codTipoDireccion = TipoDireccionEnum.PILA.getCode();

                        }

                    } else if (filtroTipoDireccion.equals(TipoDireccionEnum.RUE.getCode())) {

                        if ((result.getString("direccion_fiscal_rue") != null || result.getString("direccion_comercial_rue") != null)
                                && result.getString("codigo_municipio_rue") != null) {

                            codDepartamento = result.getString("codigo_depto_rue");

                            codMunicipio = result.getString("codigo_municipio_rue");

                            direccion = result.getString("direccion_fiscal_rue").equals("") ? result.getString("direccion_comercial_rue")
                                    : result.getString("direccion_fiscal_rue");
                            codTipoDireccion = TipoDireccionEnum.RUE.getCode();
                        }
                    }

                } else {

                    System.err.println("SIN FILTRO");

                    if (result.getString("direccion_rut_dian") != null && result.getString("municipio_rut_dian") != null
                            && result.getString("departamento_rut_dian") != null) {

                        codDepartamento = result.getString("departamento_rut_dian");

                        codMunicipio = result.getString("municipio_rut_dian");

                        direccion = result.getString("direccion_rut_dian");
                        codTipoDireccion = TipoDireccionEnum.RUT.getCode();

                    } else if (result.getString("direccion_pila") != null && result.getString("codigo_ciudad_pila") != null
                            && result.getString("codigo_depto_pila") != null) {

                        codDepartamento = result.getString("codigo_depto_pila");

                        codMunicipio = result.getString("codigo_ciudad_pila");

                        direccion = result.getString("direccion_pila");
                        codTipoDireccion = TipoDireccionEnum.PILA.getCode();

                    } else if ((result.getString("direccion_fiscal_rue") != null || result.getString("direccion_comercial_rue") != null)
                            && result.getString("codigo_municipio_rue") != null) {

                        codDepartamento = result.getString("codigo_depto_rue");

                        codMunicipio = result.getString("codigo_municipio_rue");

                        direccion = result.getString("direccion_fiscal_rue").equals("") ? result.getString("direccion_comercial_rue")
                                : result.getString("direccion_fiscal_rue");
                        codTipoDireccion = TipoDireccionEnum.RUE.getCode();
                    }

                }

                LOG.info("Op: opBuscarPorCriteriosUbicacionPersona ::: codDepartamento: " + codDepartamento + " nombreDepartamento: " + nombreDepartamento + " codMunicipio: " + codMunicipio + " nombreMunicipio: " + nombreMunicipio + " direccion: " + direccion);

                try {
                    if (StringUtils.isNotBlank(codMunicipio)) {
                        Municipio municipio = municipioDao.findByCodigo(codMunicipio);
                        if (municipio != null) {
                            nombreMunicipio = municipio.getValNombre();
                            nombreDepartamento = municipio.getDepartamento().getValNombre();
                        } else {
                            LOG.error("No se encontro informacion de  municipio con los id: codMunicipio " + codMunicipio);
                        }
                    }
                    if (StringUtils.isBlank(nombreMunicipio) || StringUtils.isBlank(nombreDepartamento)) {
                        LOG.error("No se encontro informacion de departamento o municipio con los id: codMunicipio " + codMunicipio + " codDepartamento " + codDepartamento);
                    }
                } catch (AppException e) {
                    LOG.error("Hubo un error al obtener el departamento y municipio (AppException) " + e.getMessage());
                } catch (NumberFormatException e) {
                    LOG.error("Hubo un error al obtener el departamento y municipio (NumberFormatException) " + e.getMessage());
                } catch (Exception e) {
                    LOG.error("Hubo un error al obtener el departamento y municipio (Exception) " + e.getMessage());
                }

                LOG.info("Op: opBuscarPorCriteriosUbicacionPersona ::: codDepartamento: " + codDepartamento + " nombreDepartamento: " + nombreDepartamento + " codMunicipio: " + codMunicipio + " nombreMunicipio: " + nombreMunicipio + " direccion: " + direccion);

                if (StringUtils.isNotBlank(codDepartamento) && StringUtils.isNotBlank(codMunicipio)
                        && StringUtils.isNotBlank(nombreMunicipio) && StringUtils.isNotBlank(nombreDepartamento)
                        && StringUtils.isNotBlank(direccion) && StringUtils.isNotBlank(codTipoDireccion)) {

                    UbicacionPersonaTipo ubicacionPersona = new UbicacionPersonaTipo();
                    MunicipioTipo municipioTipo = new MunicipioTipo();
                    DepartamentoTipo departamentoTipo = new DepartamentoTipo();

                    departamentoTipo.setCodDepartamento(codDepartamento);
                    municipioTipo.setCodMunicipio(codMunicipio);

                    municipioTipo.setValNombre(nombreMunicipio);
                    departamentoTipo.setValNombre(nombreDepartamento);

                    municipioTipo.setDepartamento(departamentoTipo);

                    ubicacionPersona.setMunicipio(municipioTipo);

                    ubicacionPersona.setValDireccion(direccion);
                    ubicacionPersona.setCodTipoDireccion(codTipoDireccion);
                    

                    listaUbicacionPersona.add(ubicacionPersona);

                }

            }

            return listaUbicacionPersona;

        } catch (AppException e) {
            throw e;
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        } finally {
            try {
                sqlConection.closeConnection();
            } catch (SQLException e) {
                LOG.error("Hubo un error al intentar cerrar la conexion" + e.getMessage());
            }
            try {
                Integer totalRegistros = sqlConection.ubicacionPersonaCount(parametros, criteriosOrdenamiento, tamPagina, numPagina);
                Long totalPaginas = Long.valueOf(totalRegistros) / Long.valueOf(tamPagina);
                LOG.info("totalRegistros:  " + totalRegistros + " tampagina " + tamPagina + " totalPaginas: " + totalPaginas);
                contextoRespuesta.setValCantidadPaginas(totalPaginas);
                sqlConection.closeConnection();
            } catch (Exception e) {
                LOG.error("Hubo un error al intentar contar los registros: " + e.getMessage());
            }

        }

    }

    @Override
    public PagerData<UbicacionPersonaTipo> buscarPorCriteriosUbicacionPersona(List<ParametroTipo> parametroTipoList, List<CriterioOrdenamientoTipo> criterioOrdenamientoTipos, ContextoTransaccionalTipo contextoSolicitud) throws AppException {

        final PagerData<Ubicacion> ubicaciones = ubicacionDao.findPorCriterios(parametroTipoList,
                criterioOrdenamientoTipos, contextoSolicitud.getValTamPagina(),
                contextoSolicitud.getValNumPagina());

        final List<UbicacionPersonaTipo> ubicacionPersonaTipoList = new ArrayList<UbicacionPersonaTipo>();

        for (Ubicacion ubicacion : ubicaciones.getData()) {

            UbicacionPersonaTipo ubicacionPersonaTipo = new UbicacionPersonaTipo();

            if (StringUtils.isNotBlank(ubicacion.getValDireccion())) {
                ubicacionPersonaTipo.setValDireccion(ubicacion.getValDireccion());
            }
            if (ubicacion.getCodTipoDireccion() != null && StringUtils.isNotBlank(ubicacion.getCodTipoDireccion().getId())) {
                ubicacionPersonaTipo.setCodTipoDireccion(ubicacion.getCodTipoDireccion().getId());
                ubicacionPersonaTipo.setDescTipoDireccion(ubicacion.getCodTipoDireccion().getNombre());
            }
            if (ubicacion.getMunicipio() != null) {
                ubicacionPersonaTipo.setMunicipio(municipioAssembler.assembleServicio(ubicacion.getMunicipio()));
            }

            ubicacionPersonaTipoList.add(ubicacionPersonaTipo);

        }

        return new PagerData(ubicacionPersonaTipoList, ubicaciones.getNumPages());
    }

}
