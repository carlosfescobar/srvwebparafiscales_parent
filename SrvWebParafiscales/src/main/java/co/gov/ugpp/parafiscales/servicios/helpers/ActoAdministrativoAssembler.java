package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionActoAdministrativo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.notificaciones.validacionactoadministrativotipo.v1.ValidacionActoAdministrativoTipo;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class ActoAdministrativoAssembler extends AssemblerGeneric<ActoAdministrativo, ActoAdministrativoTipo> {

    private static ActoAdministrativoAssembler actoSingleton = new ActoAdministrativoAssembler();

    private ActoAdministrativoAssembler() {
    }

    public static ActoAdministrativoAssembler getInstance() {
        return actoSingleton;
    }

    DocumentoAssembler documentoAssembler = DocumentoAssembler.getInstance();
    ExpedienteAssembler expedienteAssembler = ExpedienteAssembler.getInstance();

    @Override
    public ActoAdministrativo assembleEntidad(ActoAdministrativoTipo servicio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ActoAdministrativoTipo assembleServicio(ActoAdministrativo entidad) {

        ActoAdministrativoTipo actoAdministrativoTipo = new ActoAdministrativoTipo();

        if (entidad.getId() != null) {
            actoAdministrativoTipo.setIdActoAdministrativo(entidad.getId());
        }
        if (entidad.getCodTipoActoAdministrativo() != null) {
            actoAdministrativoTipo.setCodTipoActoAdministrativo(entidad.getCodTipoActoAdministrativo().getId());
        }
         if (entidad.getCodAreaOrigen()!= null) {
            actoAdministrativoTipo.setCodAreaOrigen(entidad.getCodAreaOrigen().getId());
            actoAdministrativoTipo.setDescAreaOrigen(entidad.getCodAreaOrigen().getNombre());
        }
        if (StringUtils.isNotBlank(entidad.getDescParteResolActoAdm())) {
            actoAdministrativoTipo.setDescParteResolutivaActoAdministrativo(entidad.getDescParteResolActoAdm());
        }
        if (entidad.getDocumentoEcm() != null) {
            actoAdministrativoTipo.setDocumento(documentoAssembler.assembleServicio(entidad.getDocumentoEcm()));
        }
        if (entidad.getExpediente() != null) {
            actoAdministrativoTipo.setExpediente(expedienteAssembler.assembleServicio(entidad.getExpediente()));
        }
        if (entidad.getFecActoAdministrativo() != null) {
            actoAdministrativoTipo.setFecActoAdministrativo(entidad.getFecActoAdministrativo());
        }
        if (StringUtils.isNotBlank(entidad.getValVigencia())) {
            actoAdministrativoTipo.setValVigencia(entidad.getValVigencia());
        }

        if (entidad.getValidaciones() != null && entidad.getValidaciones().size() > 0) {
            List<ValidacionActoAdministrativoTipo> list = new ArrayList<ValidacionActoAdministrativoTipo>();
            for (ValidacionActoAdministrativo validacion : entidad.getValidaciones()) {
                ValidacionActoAdministrativoTipo validacionActoAdministrativoTipo = new ValidacionActoAdministrativoTipo();
                if (validacion.getCodEstadoAceptacionEtapa() != null) {
                    validacionActoAdministrativoTipo.setCodEstadoAceptacionEtapa(validacion.getCodEstadoAceptacionEtapa().getId());
                }
                if (validacion.getCodEtapaValidacion() != null) {
                    validacionActoAdministrativoTipo.setCodEtapaValidacion(validacion.getCodEtapaValidacion().getId());
                }
                validacionActoAdministrativoTipo.setDesObservacion(validacion.getDesObservacion());
                list.add(validacionActoAdministrativoTipo);
            }
            actoAdministrativoTipo.getValidaciones().addAll(list);
        }

        return actoAdministrativoTipo;

    }

}
