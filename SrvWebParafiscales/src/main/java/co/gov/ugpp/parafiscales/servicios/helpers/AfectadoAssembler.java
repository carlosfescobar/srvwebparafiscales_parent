package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Afectado;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;

/**
 *
 * @author jmunocab
 */
public class AfectadoAssembler extends AssemblerGeneric<Afectado, AfectadoTipo> {

    private static final AfectadoAssembler afectadoSingleton = new AfectadoAssembler();

    private AfectadoAssembler() {
    }

    public static AfectadoAssembler getInstance() {
        return afectadoSingleton;
    }

    private final PersonaAssembler personaAssembler = PersonaAssembler.getInstance();

    @Override
    public AfectadoTipo assembleServicio(Afectado entidad) {

        AfectadoTipo afectadoTipo = new AfectadoTipo();

        entidad.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.DENUNCIANTE);
        Object persona = personaAssembler.assembleServicio(entidad.getPersona());

        if (persona instanceof PersonaJuridicaTipo) {
            afectadoTipo.setPersonaJuridica((PersonaJuridicaTipo) persona);
        } else if (persona instanceof PersonaNaturalTipo) {
            afectadoTipo.setPersonaNatural((PersonaNaturalTipo) persona);
        }
        afectadoTipo.setValIBC(entidad.getvalIBC());
        afectadoTipo.setFecIngresoEmpresa(entidad.getFecIngresoEmpresa());

        return afectadoTipo;
    }

    @Override
    public Afectado assembleEntidad(AfectadoTipo servicio) {
        //Debido a que no tenemos que mapear el objeto de dto a entity, esta parte no se implementa
        return null;
    }
}
