package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class AportanteAssembler extends AssemblerGeneric<Aportante, AportanteTipo> {

    private static AportanteAssembler aportanteSingleton = new AportanteAssembler();

    private AportanteAssembler() {
    }

    public static AportanteAssembler getInstance() {
        return aportanteSingleton;
    }

    private PersonaAssembler personaAssembler = PersonaAssembler.getInstance();

    public Aportante assembleEntidad(AportanteTipo servicio) {

        return new Aportante();
    }


    public AportanteTipo assembleServicio(Aportante entidad) {

        AportanteTipo aportanteTipo = new AportanteTipo();

        Object persona = personaAssembler.assembleServicio(entidad.getPersona());

        if (persona instanceof PersonaJuridicaTipo) {
            aportanteTipo.setPersonaJuridica((PersonaJuridicaTipo) persona);
        } else if (persona instanceof PersonaNaturalTipo) {
            aportanteTipo.setPersonaNatural((PersonaNaturalTipo) persona);
        }

        return aportanteTipo;
    }

}
