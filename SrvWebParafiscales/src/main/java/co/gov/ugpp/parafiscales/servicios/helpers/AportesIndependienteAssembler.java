package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.AportesIndependienteHclDetalle;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.liquidaciones.aportesindependientetipo.v1.AportesIndependienteTipo;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Mauricio Guerrero
 */
public class AportesIndependienteAssembler extends AssemblerGeneric<AportesIndependienteHclDetalle, AportesIndependienteTipo> {

    private static AportesIndependienteAssembler aportesIndependienteSingleton = new AportesIndependienteAssembler();

    public AportesIndependienteAssembler() {
    }

    public static AportesIndependienteAssembler getInstance() {
        return aportesIndependienteSingleton;
    }

    @Override
    public AportesIndependienteHclDetalle assembleEntidad(AportesIndependienteTipo servicio) {
        AportesIndependienteHclDetalle aportesIndependiente = new AportesIndependienteHclDetalle();

//        aportesIndependiente.setAceptacionCosto(servicio.getAceptacionCosto().toString());
//        aportesIndependiente.setContratoFfinal(servicio.getContratoFFinal());
//        aportesIndependiente.setContratoFinicio(servicio.getContratoFInicio());
//        aportesIndependiente.setDescripcionCosto(servicio.getDescripcionCosto().toString());
//        aportesIndependiente.setDescripcionIngreso(servicio.getDescripcionIngreso().toString());
//        aportesIndependiente.setEstado(servicio.getEstado().toString());
//        aportesIndependiente.setFechaCosto(servicio.getFechaCosto());
//        aportesIndependiente.setHojatradAnoFa(servicio.getHOJATRADAnoFA());
//        aportesIndependiente.setHojatradDuracionIngMesesEz(servicio.getHOJATRADDuracionIngMesesEZ());
//        aportesIndependiente.setHojatradIngresoDepuradoFd(servicio.getHOJATRADIngresoDepuradoFD());
//        aportesIndependiente.setHojatradMesFb(servicio.getHOJATRADMesFB());
//        aportesIndependiente.setHojatradValorMensualizadoFc(servicio.getHOJATRADValorMensualizadoFC());
//        aportesIndependiente.setIdhojacalculoliquidaciondet(servicio.getIdHojaCalculoLiquidacionDetalle());
//        aportesIndependiente.setId(servicio.getIdAportesIndependiente());
//        aportesIndependiente.setNumeroContrato(servicio.getNumeroContrato().toString());
//        aportesIndependiente.setNumeroFactura(servicio.getNumeroFactura());
//        aportesIndependiente.setObservacionesArl(servicio.getObservacionesArl().toString());
//        aportesIndependiente.setObservacionesCcf(servicio.getObservacionesCcf().toString());
//        aportesIndependiente.setObservacionesCosto(servicio.getObservacionesCosto().toString());
//        aportesIndependiente.setObservacionesEsap(servicio.getObservacionesEsap().toString());
//        aportesIndependiente.setObservacionesIcbf(servicio.getObservacionesIcbf().toString());
//        aportesIndependiente.setObservacionesIngresos(servicio.getObservacionesIngresos().toString());
//        aportesIndependiente.setObservacionesMen(servicio.getObservacionesMen().toString());
//        aportesIndependiente.setObservacionesNovedades(servicio.getObservacionesNovedades().toString());
//        aportesIndependiente.setObservacionesPension(servicio.getObservacionesPension().toString());
//        aportesIndependiente.setObservacionesSalud(servicio.getObservacionesSalud().toString());
//        aportesIndependiente.setObservacionesSena(servicio.getObservacionesSena().toString());
        return aportesIndependiente;
    }

    @Override
    public AportesIndependienteTipo assembleServicio(AportesIndependienteHclDetalle entidad) {
        AportesIndependienteTipo aportesIndependiente = new AportesIndependienteTipo();

        aportesIndependiente.setAceptacionCosto(entidad.getAceptacionCosto());
        if (entidad.getContratoFfinal() != null) {
            aportesIndependiente.setContratoFFinal(DateUtil.parseStrDateToCalendar(entidad.getContratoFfinal().toString()));
        }
        if (entidad.getContratoFinicio() != null) {
            aportesIndependiente.setContratoFInicio(DateUtil.parseStrDateToCalendar(entidad.getContratoFinicio().toString()));
        }
        aportesIndependiente.setDescripcionCosto(entidad.getDescripcionCosto());
        aportesIndependiente.setDescripcionIngreso(entidad.getDescripcionIngreso().toString());
        aportesIndependiente.setEstado(entidad.getEstado().toString());
        if (entidad.getFechaCosto() != null) {
            aportesIndependiente.setFechaCosto(DateUtil.parseStrDateToCalendar(entidad.getFechaCosto().toString()));
        }
        aportesIndependiente.setHOJATRADAnoFA(entidad.getHojatradAnoFa().longValue());
        aportesIndependiente.setHOJATRADDuracionIngMesesEZ(entidad.getHojatradDuracionIngMesesEz().longValue());
        aportesIndependiente.setHOJATRADIngresoDepuradoFD(entidad.getHojatradIngresoDepuradoFd());
        aportesIndependiente.setHOJATRADMesFB(entidad.getHojatradMesFb().longValue());
        aportesIndependiente.setHOJATRADValorMensualizadoFC(entidad.getHojatradValorMensualizadoFc());
        if (entidad.getIdhojacalculoliquidaciondet() != null) {
            aportesIndependiente.setIdHojaCalculoLiquidacionDetalle(entidad.getIdhojacalculoliquidaciondet().getId().toString());
        }
        if (entidad.getId() != null) {
            aportesIndependiente.setIdAportesIndependiente(entidad.getId().toString());
        }
        aportesIndependiente.setNumeroContrato(entidad.getNumeroContrato());
        aportesIndependiente.setNumeroFactura(entidad.getNumeroFactura().toString());
        aportesIndependiente.setObservacionesArl(entidad.getObservacionesArl());
        aportesIndependiente.setObservacionesCcf(entidad.getObservacionesCcf());
        aportesIndependiente.setObservacionesCosto(entidad.getObservacionesCosto());
        aportesIndependiente.setObservacionesEsap(entidad.getObservacionesEsap());
        aportesIndependiente.setObservacionesIcbf(entidad.getObservacionesIcbf());
        aportesIndependiente.setObservacionesIngresos(entidad.getObservacionesIngresos());
        aportesIndependiente.setObservacionesMen(entidad.getObservacionesMen());
        aportesIndependiente.setObservacionesNovedades(entidad.getObservacionesNovedades());
        aportesIndependiente.setObservacionesPension(entidad.getObservacionesPension());
        aportesIndependiente.setObservacionesSalud(entidad.getObservacionesSalud());
        aportesIndependiente.setObservacionesSena(entidad.getObservacionesSena());

        return aportesIndependiente;
    }

    private XMLGregorianCalendar getXMLGregorianCalendar(Date fecha) {
        XMLGregorianCalendar xmlDate = null;
        try {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(fecha);
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(DetalleHojaCalculoAssembler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlDate;
    }
}
