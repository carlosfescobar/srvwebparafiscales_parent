package co.gov.ugpp.parafiscales.servicios.helpers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Clase generica para el paso entre el servicio y la entidad de persistencia.
 * @author franzjr cpalacios 
 * @param <T>
 * @param <X> 
 */

public abstract class AssemblerGeneric<T,X> {

    public abstract T assembleEntidad(X servicio);
    public abstract X assembleServicio(T entidad);
    
    public  List<T> assembleCollectionEntidad(Collection<X> arrayL) {
	
	List<T> collection = new ArrayList<T>();
	for (X parcial : arrayL) {
	    
	    collection.add(assembleEntidad(parcial));
	}
	return collection;
	
    }
    
    public List<X> assembleCollectionServicio(Collection<T> collection) {
	
	List<X> arrayL = new ArrayList<X>();
		
	for (T parcial : collection) {
	    
	    arrayL.add(assembleServicio(parcial));
	}
	return arrayL;
	
    }

}