package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.DenunciHasCodCausalDenunci;

/**
 * Clase intermedio entre el servicio y la persistencia.
 * @author franzjr
 */
public class CausalDenunciaAssembler extends AssemblerGeneric<DenunciHasCodCausalDenunci, String>{
    
    private static CausalDenunciaAssembler causalDenunciaSingleton = new CausalDenunciaAssembler();

    private CausalDenunciaAssembler() {
    }

    public static CausalDenunciaAssembler getInstance() {
	return causalDenunciaSingleton;
    }


    public DenunciHasCodCausalDenunci assembleEntidad(String servicio) {
        
        DenunciHasCodCausalDenunci denunciaEntidad = new DenunciHasCodCausalDenunci();
        denunciaEntidad.setCodCausalDenuncia(servicio);
        
        return denunciaEntidad;
        
    }

    public String assembleServicio(DenunciHasCodCausalDenunci entidad) {
        
        return String.valueOf(entidad.getCodCausalDenuncia());
    }
    
}
