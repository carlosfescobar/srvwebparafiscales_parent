package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContableHclDetalle;
import co.gov.ugpp.schema.liquidaciones.conceptocontabletipo.v1.ConceptoContableTipo;
import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Mauricio Guerrero
 */
public class ConceptoContableAssembler extends AssemblerGeneric<ConceptoContableHclDetalle, ConceptoContableTipo> {

    private static ConceptoContableAssembler conceptoContableSingleton = new ConceptoContableAssembler();

    public ConceptoContableAssembler() {
    }

    public static ConceptoContableAssembler getInstance() {
        return conceptoContableSingleton;
    }

    @Override
    public ConceptoContableHclDetalle assembleEntidad(ConceptoContableTipo servicio) {
        ConceptoContableHclDetalle conceptoContable = new ConceptoContableHclDetalle();
//
//        conceptoContable.setCodigo(servicio.getCodigo().toString());
//        conceptoContable.setCuenta(servicio.getCuenta().toString());
//        conceptoContable.setEstado(servicio.getEstado().toString());
//        if (servicio.getIdConceptoContable() != null) {
//            conceptoContable.setId(new BigDecimal(servicio.getIdConceptoContable()));
//        }
//        if (servicio.getIdHojaCalculoLiquidacionDetalle() != null) {
//            conceptoContable.setIdhojacalculoliquidaciondet(servicio.getIdHojaCalculoLiquidacionDetalle());
//        }
//        conceptoContable.setNombre(servicio.getNombre().toString());
//        conceptoContable.setNombreConcepto(servicio.getNombreConcepto().toString());
//        conceptoContable.setSubsistemas(servicio.getSubsistemas().toString());
//        conceptoContable.setValor(servicio.getValor());

        return conceptoContable;
    }

    @Override
    public ConceptoContableTipo assembleServicio(ConceptoContableHclDetalle entidad) {
        ConceptoContableTipo conceptoContable = new ConceptoContableTipo();

        conceptoContable.setCodigo(StringUtils.isBlank(entidad.getCodigo()) ? "" : entidad.getCodigo().toString());
        conceptoContable.setCuenta(StringUtils.isBlank(entidad.getCuenta()) ? "" : entidad.getCuenta().toString());
        conceptoContable.setEstado(StringUtils.isBlank(entidad.getEstado()) ? "" : entidad.getEstado().toString());
        conceptoContable.setIdConceptoContable(entidad.getId() == null ? "" : entidad.getId().toString());
        if (entidad.getIdhojacalculoliquidaciondet() != null) {
            conceptoContable.setIdHojaCalculoLiquidacionDetalle(entidad.getIdhojacalculoliquidaciondet().getId().toString());
        }
        conceptoContable.setNombre(StringUtils.isBlank(entidad.getNombre()) ? "" : entidad.getNombre().toString());
        conceptoContable.setNombreConcepto(StringUtils.isBlank(entidad.getNombreConcepto()) ? "" : entidad.getNombreConcepto().toString());
        conceptoContable.setSubsistemas(StringUtils.isBlank(entidad.getSubsistemas()) ? "" : entidad.getSubsistemas().toString());
        conceptoContable.setValor(entidad.getValor() == null ? BigDecimal.ZERO : entidad.getValor());

        return conceptoContable;
    }
}
