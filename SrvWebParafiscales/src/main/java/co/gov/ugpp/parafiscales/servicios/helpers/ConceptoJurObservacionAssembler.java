package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJurHasDescObservac;


/**
 *
 * @author franzjr
 */
public class ConceptoJurObservacionAssembler extends AssemblerGeneric<ConceptoJurHasDescObservac, String>{
    
    private static ConceptoJurObservacionAssembler conceptoJuridicoSingleton = new ConceptoJurObservacionAssembler();

    private ConceptoJurObservacionAssembler() {
    }

    public static ConceptoJurObservacionAssembler getInstance() {
        return conceptoJuridicoSingleton;
    }
    
    @Override
    public ConceptoJurHasDescObservac assembleEntidad(String servicio) {
        
        ConceptoJurHasDescObservac conceptoPregunta = new ConceptoJurHasDescObservac();
        conceptoPregunta.setDescObservaciones(servicio);
        
        return conceptoPregunta;
    }

    @Override
    public String assembleServicio(ConceptoJurHasDescObservac entidad) {
        
        return entidad.getDescObservaciones();
    }
    
}
