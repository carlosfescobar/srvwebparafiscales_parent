package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridHasDescPregun;


/**
 *
 * @author franzjr
 */
public class ConceptoJurPreguntaAssembler extends AssemblerGeneric<ConceptoJuridHasDescPregun, String>{
    
    private static ConceptoJurPreguntaAssembler conceptoJuridicoSingleton = new ConceptoJurPreguntaAssembler();

    private ConceptoJurPreguntaAssembler() {
    }

    public static ConceptoJurPreguntaAssembler getInstance() {
        return conceptoJuridicoSingleton;
    }
    
    @Override
    public ConceptoJuridHasDescPregun assembleEntidad(String servicio) {
        
        ConceptoJuridHasDescPregun conceptoPregunta = new ConceptoJuridHasDescPregun();
        conceptoPregunta.setDescPreguntas(servicio);
        
        return conceptoPregunta;
    }

    @Override
    public String assembleServicio(ConceptoJuridHasDescPregun entidad) {
        
        return entidad.getDescPreguntas();
    }
    
}
