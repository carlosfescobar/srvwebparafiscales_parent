package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.ConceptoJuridico;
import co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1.ConceptoJuridicoTipo;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class ConceptoJuridicoAssembler extends AssemblerGeneric<ConceptoJuridico, ConceptoJuridicoTipo> {

    private static ConceptoJuridicoAssembler conceptoJuridicoSingleton = new ConceptoJuridicoAssembler();

    private ConceptoJuridicoAssembler() {
    }

    public static ConceptoJuridicoAssembler getInstance() {
        return conceptoJuridicoSingleton;
    }

    ExpedienteAssembler expedienteAssembler = ExpedienteAssembler.getInstance();
    ConceptoJurPreguntaAssembler conceptoPreguntaAssembler = ConceptoJurPreguntaAssembler.getInstance();
    DocumentoAssembler documentoAssembler = DocumentoAssembler.getInstance();
    ConceptoJurObservacionAssembler conceptoJurObservacionAssembler = ConceptoJurObservacionAssembler.getInstance();

    public ConceptoJuridico assembleEntidad(ConceptoJuridicoTipo servicio) {

        ConceptoJuridico conceptoJuridico = new ConceptoJuridico();
        
        return conceptoJuridico;
    }

    public ConceptoJuridicoTipo assembleServicio(ConceptoJuridico entidad) {

        ConceptoJuridicoTipo conceptoJuridicoTipo = new ConceptoJuridicoTipo();
        
        if (entidad.getIdConceptoJuridico() != null) {
            conceptoJuridicoTipo.setIdConceptoJuridico(String.valueOf(entidad.getIdConceptoJuridico()));
        }
        if (entidad.getCodPrioridad() != null) {
            conceptoJuridicoTipo.setCodPrioridad(entidad.getCodPrioridad().getId());
        }
        if (StringUtils.isNotBlank(entidad.getDescAsuntoConceptoJuridico())) {
            conceptoJuridicoTipo.setDescAsuntoConceptoJuridico(entidad.getDescAsuntoConceptoJuridico());
        }
        if (StringUtils.isNotBlank(entidad.getDescMotivoPrioridad())) {
            conceptoJuridicoTipo.setDescMotivoPrioridad(entidad.getDescMotivoPrioridad());
        }
        if (entidad.getDocumentoEcm() != null) {
            conceptoJuridicoTipo.setDocumento(documentoAssembler.assembleServicio(entidad.getDocumentoEcm()));
        }
        if (entidad.getExpedienteId() != null) {
            conceptoJuridicoTipo.setIdNumeroExpediente(entidad.getExpedienteId().getId());
        }

        return conceptoJuridicoTipo;
    }

}
