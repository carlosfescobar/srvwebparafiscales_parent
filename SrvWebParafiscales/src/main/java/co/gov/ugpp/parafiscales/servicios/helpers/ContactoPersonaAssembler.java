package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class ContactoPersonaAssembler extends AssemblerGeneric<ContactoPersona, ContactoPersonaTipo> {

    private static ContactoPersonaAssembler contactoAssembler = new ContactoPersonaAssembler();

    private ContactoPersonaAssembler() {
    }

    public static ContactoPersonaAssembler getInstance() {
        return contactoAssembler;
    }

    TelefonoAssembler telefonoAssembler = TelefonoAssembler.getInstance();
    UbicacionAssembler ubicacionAssembler = UbicacionAssembler.getInstance();
    CorreoElectronicoAssembler correoElectronicoAssembler = CorreoElectronicoAssembler.getInstance();

    public ContactoPersona assembleEntidad(ContactoPersonaTipo servicio) {

        ContactoPersona contacto = new ContactoPersona();

        if (servicio.getEsAutorizaNotificacionElectronica() != null) {
            contacto.setEsAutorizaNotificacionElectronica(Boolean.valueOf(servicio.getEsAutorizaNotificacionElectronica()));
        }
        if (servicio.getTelefonos() != null) {
            contacto.getTelefonoList().addAll(telefonoAssembler.assembleCollectionEntidad(servicio.getTelefonos()));
        }
        if (servicio.getUbicaciones() != null) {
            contacto.getUbicacionList().addAll(ubicacionAssembler.assembleCollectionEntidad(servicio.getUbicaciones()));
        }
        if (servicio.getCorreosElectronicos() != null) {
            contacto.getCorreoElectronicoList().addAll(correoElectronicoAssembler.assembleCollectionEntidad(servicio.getCorreosElectronicos()));
        }

        return contacto;

    }

    public ContactoPersonaTipo assembleServicio(ContactoPersona entidad) {

        ContactoPersonaTipo contacto = new ContactoPersonaTipo();

        if (entidad.getEsAutorizaNotificacionElectronica() != null) {
            contacto.setEsAutorizaNotificacionElectronica(entidad.getEsAutorizaNotificacionElectronica().toString());
        }
        if (entidad.getTelefonoList() != null) {
            contacto.getTelefonos().addAll(telefonoAssembler.assembleCollectionServicio(entidad.getTelefonoList()));
        }
        if (entidad.getUbicacionList() != null) {
            contacto.getUbicaciones().addAll(ubicacionAssembler.assembleCollectionServicio(entidad.getUbicacionList()));
        }
        if (entidad.getCorreoElectronicoList() != null && !entidad.getCorreoElectronicoList().isEmpty()) {
            List<CorreoElectronico> correosElectronicos = new ArrayList<CorreoElectronico>();
            for (CorreoElectronico correo : entidad.getCorreoElectronicoList()) {
                if (correo.getCodFuente() != null || StringUtils.isNotBlank(correo.getValCorreoElectronico())) {
                    correosElectronicos.add(correo);
                }
            }
            contacto.getCorreosElectronicos().addAll(correoElectronicoAssembler.assembleCollectionServicio(correosElectronicos));
        }

        return contacto;

    }

}
