package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;
import org.apache.commons.lang3.StringUtils;


/**
 * Clase intermedio entre el servicio y la persistencia.
 * @author franzjr
 */
public class CorreoElectronicoAssembler extends AssemblerGeneric<CorreoElectronico, CorreoElectronicoTipo>{
    
    private static CorreoElectronicoAssembler correoAssembler = new CorreoElectronicoAssembler();

    private CorreoElectronicoAssembler() {
    }

    public static CorreoElectronicoAssembler getInstance() {
	return correoAssembler;
    }


    public CorreoElectronico assembleEntidad(CorreoElectronicoTipo servicio) {
        
        CorreoElectronico correo = new CorreoElectronico();
        
        if (servicio.getValCorreoElectronico()!=null && StringUtils.isNotBlank(servicio.getValCorreoElectronico())) {
            correo.setValCorreoElectronico(servicio.getValCorreoElectronico());
        }
                 
        
        return correo;
        
    }

    public CorreoElectronicoTipo assembleServicio(CorreoElectronico entidad) {
        
        CorreoElectronicoTipo correo = new CorreoElectronicoTipo();
        
        if (StringUtils.isNotBlank(entidad.getValCorreoElectronico())) {
            correo.setValCorreoElectronico(entidad.getValCorreoElectronico());
        }
        if(entidad.getCodFuente()!=null){
            correo.setCodFuente(entidad.getCodFuente().getId());
            correo.setDescFuenteCorreo(entidad.getCodFuente().getNombre());
        }
        
        return correo;
    }
        
    
}
