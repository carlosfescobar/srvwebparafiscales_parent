package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalAnalisis;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaCausalInicial;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaAccion;
import co.gov.ugpp.parafiscales.persistence.entity.DenunciaHasCodSubsistema;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class DenunciaAssembler extends AssemblerGeneric<Denuncia, DenunciaTipo> {

    private static DenunciaAssembler denunciaSingleton = new DenunciaAssembler();

    private DenunciaAssembler() {
    }

    public static DenunciaAssembler getInstance() {
        return denunciaSingleton;
    }

    private DenuncianteAssembler denuncianteAssembler = DenuncianteAssembler.getInstance();
    private AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    private ExpedienteAssembler expedienteAssembler = ExpedienteAssembler.getInstance();
    private static final AfectadoAssembler afectadoAssembler = AfectadoAssembler.getInstance();

    @Override
    public Denuncia assembleEntidad(DenunciaTipo servicio) {

        Denuncia denuncia = new Denuncia();

        return denuncia;

    }

    @Override
    public DenunciaTipo assembleServicio(Denuncia entidad) {

        DenunciaTipo denunciaServicio = new DenunciaTipo();

        if (entidad.getIdDenuncia() != null) {
            denunciaServicio.setIdDenuncia(String.valueOf(entidad.getIdDenuncia()));
        }
        if (StringUtils.isNotBlank(entidad.getIdRadicadoDenuncia())) {
            denunciaServicio.setIdRadicadoDenuncia(entidad.getIdRadicadoDenuncia());
        }
        if (entidad.getAportante() != null) {
            denunciaServicio.setAportante(aportanteAssembler.assembleServicio(entidad.getAportante()));
        }
        if (entidad.getCodCanaDenuncial() != null) {
            denunciaServicio.setCodCanalDenuncia(entidad.getCodCanaDenuncial().getId());
        }
        if (entidad.getCodCategoriaDenuncia() != null) {
            denunciaServicio.setCodCategoriaDenuncia(entidad.getCodCategoriaDenuncia().getId());
        }
        if (entidad.getCodDAPagos() != null) {
            denunciaServicio.setCodDecisionAnalisisPagos(entidad.getCodDAPagos().getId());
        }
        if (entidad.getIdDenunciante() != null) {
            denunciaServicio.setDenunciante(denuncianteAssembler.assembleServicio(entidad.getIdDenunciante()));
        }
        if (StringUtils.isNotBlank(entidad.getDescObservaciones())) {
            denunciaServicio.setDescObservaciones(entidad.getDescObservaciones());
        }
        if (entidad.getEsCasoCerrado() != null) {
            denunciaServicio.setEsCasoCerrado(String.valueOf(entidad.getEsCasoCerrado()));
        }
        if (entidad.getEsPago() != null) {
            denunciaServicio.setEsPago(String.valueOf(entidad.getEsPago()));
        }
        if (entidad.getEsCompetenciaUgPp() != null) {
            denunciaServicio.setEsCompetenciaUGPP(String.valueOf(entidad.getEsCompetenciaUgPp()));
        }
        if (entidad.getEsEnFiscalizacion() != null) {
            denunciaServicio.setEsEnFiscalizacion(String.valueOf(entidad.getEsEnFiscalizacion()));
        }
        if (entidad.getEsEnFiscalizacion() != null) {
            denunciaServicio.setEsEnFiscalizacion(String.valueOf(entidad.getEsEnFiscalizacion()));
        }
        if (entidad.getEsEnFiscalizacion() != null) {
            denunciaServicio.setEsInformacionCompleta(String.valueOf(entidad.getEsEnFiscalizacion()));
        }
        if (entidad.getEsInformacionCompleta() != null) {
            denunciaServicio.setEsInformacionCompleta(String.valueOf(entidad.getEsInformacionCompleta()));
        }

        //INICIO CAMBIOS REALIZADOS EVERIS 20/06/2014
        if (entidad.getEsSuspenderTermino() != null) {
            denunciaServicio.setEsSuspenderTerminos(String.valueOf(entidad.getEsSuspenderTermino()));
        }
        if (entidad.getEsTieneDerechoPeticion() != null) {
            denunciaServicio.setEsTieneDerechoPeticion(String.valueOf(entidad.getEsTieneDerechoPeticion()));
        }
        if (entidad.getEsTieneAnexos() != null) {
            denunciaServicio.setEsTieneAnexos(String.valueOf(entidad.getEsTieneAnexos()));
        }
        if (entidad.getEsPagosValidados() != null) {
            denunciaServicio.setEsPagosValidados(String.valueOf(entidad.getEsPagosValidados()));
        }
        if (entidad.getCodTipoDenuncia() != null) {
            denunciaServicio.setCodTipoDenuncia(entidad.getCodTipoDenuncia().getId());
        }
        //FIN CAMBIOS RELALIZADOS POR EVERIS 20/06/2014

        //INICIO CAMBIOS REALIZADOS POR EVERIS 28/07/2014
        if (entidad.getEsAprobadoCierre() != null) {
            denunciaServicio.setEsAprobadoCierre(String.valueOf(entidad.getEsAprobadoCierre()));
        }

        if (entidad.getCodTipoEnvioComite() != null) {
            denunciaServicio.setCodTipoEnvioComite(entidad.getCodTipoEnvioComite().getId());
        }

        if (entidad.getCodEstadoDenuncia() != null) {
            denunciaServicio.setCodEstadoDenuncia(entidad.getCodEstadoDenuncia().getId());
        }

        //FIN CAMBIOS RELALIZADOS POR EVERIS 28/07/2014
        //INICIO CAMBIOS REALIZADOS POR EVERIS 12/08/2014
        if (entidad.getFecRadicadoDenuncia() != null) {
            denunciaServicio.setFecRadicadoDenuncia(DateUtil.parseCalendarToStrDateNoHour(entidad.getFecRadicadoDenuncia()));
        }
        //FIN CAMBIOS RELALIZADOS POR EVERIS 12/08/2014

        if (entidad.getAfectado() != null) {
            denunciaServicio.setAfectado(afectadoAssembler.assembleServicio(entidad.getAfectado()));
        }

        if (entidad.getExpediente() != null) {
            denunciaServicio.setExpediente(expedienteAssembler.assembleServicio(entidad.getExpediente()));
        }
        if (entidad.getFecFinPeriodo() != null) {
            denunciaServicio.setFecFinPeriodo(entidad.getFecFinPeriodo());
        }
        if (entidad.getFecInicioPeriodo() != null) {
            denunciaServicio.setFecInicioPeriodo(entidad.getFecInicioPeriodo());
        }

        if (entidad.getDenunciaHasCodSubsistemaList() != null && entidad.getDenunciaHasCodSubsistemaList().size() > 0) {
            List<String> list = new ArrayList<String>();
            for (DenunciaHasCodSubsistema subsistema : entidad.getDenunciaHasCodSubsistemaList()) {
                list.add(subsistema.getCodSubsistema());
            }
            denunciaServicio.getCodSubsistema().addAll(list);
        }
        if (StringUtils.isNotBlank(entidad.getDescInformacionFaltante())) {
            denunciaServicio.setDescInformacionFaltante(entidad.getDescInformacionFaltante());
        }

        if (entidad.getCausalAnalisis() != null
                && entidad.getCausalAnalisis().isEmpty()) {
            for (DenunciaCausalAnalisis denunciaCausalAnalisis : entidad.getCausalAnalisis()) {
                denunciaServicio.getCodCausalAnalisisDenuncia().add(denunciaCausalAnalisis.getCodCausalAnalisis().getId());
            }
        }

        if (entidad.getCausalInicial() != null
                && entidad.getCausalInicial().isEmpty()) {
            for (DenunciaCausalInicial denunciaCausalInicial : entidad.getCausalInicial()) {
                denunciaServicio.getCodCausalInicialDenuncia().add(denunciaCausalInicial.getCodCausalInicial().getId());
            }
        }
        if (entidad.getEsCompetenciaParcial()!= null) {
            denunciaServicio.setEsCompetenciaParcial(String.valueOf(entidad.getEsCompetenciaParcial()));
        }
        //Se obtienen los documentos del expediente
        if (entidad.getExpediente().getExpedienteDocumentoList() != null
                && !entidad.getExpediente().getExpedienteDocumentoList().isEmpty()) {

            int cantidadDocumentosEncontrados = 0;

            for (ExpedienteDocumentoEcm expedienteDocumentoEcm : entidad.getExpediente().getExpedienteDocumentoList()) {
                if (expedienteDocumentoEcm.getCodOrigen() != null) {
                    if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_CIERRE_CASO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        denunciaServicio.setIdDocumentoCierreCaso(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_COMPLETITUD_INFO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        denunciaServicio.setIdDocumentoCompletitudInformacion(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_PERSUASIVO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        denunciaServicio.setIdDocumentoPersuasivo(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_SUSP_TERMINOS.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        denunciaServicio.setIdDocumentoSuspensionTerminos(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    }
                }
                if (Constants.CANTIDAD_DOCUMENTOS_DENUNCIA == cantidadDocumentosEncontrados) {
                    break;
                }
            }
        }

        if (entidad.getCausalAnalisis() != null
                && !entidad.getCausalAnalisis().isEmpty()) {
            for (DenunciaCausalAnalisis denunciaCausalAnalisis : entidad.getCausalAnalisis()) {
                denunciaServicio.getCodCausalAnalisisDenuncia().add(denunciaCausalAnalisis.getCodCausalAnalisis().getId());
            }
        }

        if (entidad.getCausalInicial() != null
                && !entidad.getCausalInicial().isEmpty()) {
            for (DenunciaCausalInicial denunciaCausalInicial : entidad.getCausalInicial()) {
                denunciaServicio.getCodCausalInicialDenuncia().add(denunciaCausalInicial.getCodCausalInicial().getId());
            }
        }

        if (entidad.getDenunciaAccionesList() != null
                && !entidad.getDenunciaAccionesList().isEmpty()) {
            for (DenunciaAccion accionDenuncia : entidad.getDenunciaAccionesList()) {
                if (accionDenuncia.getAccion() != null) {
                    AccionTipo accionTipo = new AccionTipo();
                    accionTipo.setCodAccion(accionDenuncia.getAccion().getCodAccion() == null ? null : accionDenuncia.getAccion().getCodAccion().getId());
                    accionTipo.setDescAccion(accionDenuncia.getAccion().getCodAccion() == null ? null : accionDenuncia.getAccion().getCodAccion().getNombre());
                    accionTipo.setFecEjecucionAccion(DateUtil.parseCalendarToStrDate(accionDenuncia.getAccion().getFecEjecucionAccion()));
                    denunciaServicio.getAcciones().add(accionTipo);
                }
            }
        }

        return denunciaServicio;

    }
}
