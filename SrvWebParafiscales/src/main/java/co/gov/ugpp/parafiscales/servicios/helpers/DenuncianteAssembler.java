package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Denunciante;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class DenuncianteAssembler extends AssemblerGeneric<Denunciante, DenuncianteTipo> {

    private static DenuncianteAssembler denuncianteSingleton = new DenuncianteAssembler();

    private DenuncianteAssembler() {
    }

    public static DenuncianteAssembler getInstance() {
        return denuncianteSingleton;
    }

    private ContactoPersonaAssembler contactoPersonaAssembler = ContactoPersonaAssembler.getInstance();
    private PersonaAssembler personaAssembler = PersonaAssembler.getInstance();

    public Denunciante assembleEntidad(DenuncianteTipo servicio) {

        Persona persona = new Persona();

        if (servicio.getPersonaNatural() != null) {

            PersonaNaturalTipo personaNatural = servicio.getPersonaNatural();

            persona.setCodNaturalezaPersona(new ValorDominio(TipoPersonaEnum.PERSONA_NATURAL.getCode()));

            if (personaNatural.getCodEstadoCivil() != null) {
                persona.setCodEstadoCivil(new ValorDominio(personaNatural.getCodEstadoCivil()));
            }
            if (personaNatural.getCodNivelEducativo() != null) {
                persona.setCodNivelEducativo(new ValorDominio(personaNatural.getCodNivelEducativo()));
            }
            if (personaNatural.getCodSexo() != null) {
                persona.setCodSexo(new ValorDominio(personaNatural.getCodSexo()));
            }
            if (personaNatural.getContacto() != null) {
                persona.setContacto(contactoPersonaAssembler.assembleEntidad(personaNatural.getContacto()));
            }
            if (personaNatural.getIdAbogado() != null) {
                Persona abogado = new Persona();
                abogado.setValNumeroIdentificacion(personaNatural.getIdAbogado().getValNumeroIdentificacion());
                abogado.setCodTipoIdentificacion(new ValorDominio(personaNatural.getIdAbogado().getCodTipoIdentificacion()));

                persona.setAbogado(abogado);
            }
            if (personaNatural.getIdAutorizado() != null) {
                Persona autorizado = new Persona();
                autorizado.setValNumeroIdentificacion(personaNatural.getIdAbogado().getValNumeroIdentificacion());
                autorizado.setCodTipoIdentificacion(new ValorDominio(personaNatural.getIdAbogado().getCodTipoIdentificacion()));

                persona.setAbogado(autorizado);
            }
            if (personaNatural.getIdPersona() != null) {
                persona.setValNumeroIdentificacion(personaNatural.getIdPersona().getValNumeroIdentificacion());
                persona.setCodTipoIdentificacion(new ValorDominio(personaNatural.getIdPersona().getCodTipoIdentificacion()));
            }
            if (personaNatural.getValPrimerNombre() != null) {
                persona.setValPrimerNombre(personaNatural.getValPrimerNombre());
            }
            if (personaNatural.getValSegundoNombre() != null) {
                persona.setValSegundoNombre(personaNatural.getValSegundoNombre());
            }
            if (personaNatural.getValPrimerApellido() != null) {
                persona.setValPrimerApellido(personaNatural.getValPrimerApellido());
            }
            if (personaNatural.getValSegundoApellido() != null) {
                persona.setValSegundoApellido(personaNatural.getValSegundoApellido());
            }

        } else if (servicio.getPersonaJuridica() != null) {

            PersonaJuridicaTipo personaJuridica = servicio.getPersonaJuridica();

            persona.setCodNaturalezaPersona(new ValorDominio(TipoPersonaEnum.PERSONA_JURIDICA.getCode()));

            if (personaJuridica.getContacto() != null) {
                persona.setContacto(contactoPersonaAssembler.assembleEntidad(personaJuridica.getContacto()));
            }
            if (personaJuridica.getIdAbogado() != null) {
                Persona abogado = new Persona();
                abogado.setValNumeroIdentificacion(personaJuridica.getIdAbogado().getValNumeroIdentificacion());
                abogado.setCodTipoIdentificacion(new ValorDominio(personaJuridica.getIdAbogado().getCodTipoIdentificacion()));

                persona.setAbogado(abogado);
            }
            if (personaJuridica.getIdAutorizado() != null) {
                Persona autorizado = new Persona();
                autorizado.setValNumeroIdentificacion(personaJuridica.getIdAbogado().getValNumeroIdentificacion());
                autorizado.setCodTipoIdentificacion(new ValorDominio(personaJuridica.getIdAbogado().getCodTipoIdentificacion()));

                persona.setAbogado(autorizado);
            }
            if (personaJuridica.getIdPersona() != null) {
                persona.setValNumeroIdentificacion(personaJuridica.getIdPersona().getValNumeroIdentificacion());
                persona.setCodTipoIdentificacion(new ValorDominio(personaJuridica.getIdPersona().getCodTipoIdentificacion()));
            }
            if (personaJuridica.getIdRepresentanteLegal() != null) {
                Persona representante = new Persona();
                representante.setValNumeroIdentificacion(personaJuridica.getIdAbogado().getValNumeroIdentificacion());
                representante.setCodTipoIdentificacion(new ValorDominio(personaJuridica.getIdAbogado().getCodTipoIdentificacion()));

                persona.setAbogado(representante);
            }
            if (personaJuridica.getValNombreRazonSocial() != null) {
                persona.setValNombreRazonSocial(personaJuridica.getValNombreRazonSocial());
            }

        }

        Denunciante denunciante = new Denunciante();
        denunciante.setPersona(persona);

        return denunciante;

    }

    public DenuncianteTipo assembleServicio(Denunciante entidad) {

        DenuncianteTipo denuncianteTipo = new DenuncianteTipo();

        entidad.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.DENUNCIANTE);
        Object persona = personaAssembler.assembleServicio(entidad.getPersona());

        if (persona instanceof PersonaJuridicaTipo) {
            denuncianteTipo.setPersonaJuridica((PersonaJuridicaTipo) persona);
        } else if (persona instanceof PersonaNaturalTipo) {
            denuncianteTipo.setPersonaNatural((PersonaNaturalTipo) persona);
        }
        denuncianteTipo.setCodTipoDenunciante(entidad.getCodTipoDenunciante().getId());

        return denuncianteTipo;
    }

}
