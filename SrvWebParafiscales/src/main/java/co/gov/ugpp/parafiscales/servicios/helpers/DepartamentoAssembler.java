package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.departamentotipo.v1.DepartamentoTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Departamento;

/**
 * Clase intermedio entre el servicio y la persistencia.
 * @author franzjr
 */
public class DepartamentoAssembler extends AssemblerGeneric<Departamento, DepartamentoTipo>{
    
    private static DepartamentoAssembler departamentoAssembler = new DepartamentoAssembler();

    private DepartamentoAssembler() {
    }

    public static DepartamentoAssembler getInstance() {
	return departamentoAssembler;
    }


    public Departamento assembleEntidad(DepartamentoTipo servicio) {
        
        Departamento departamento = new Departamento();
        
        if (servicio.getCodDepartamento() != null) {
            departamento.setCodigo(servicio.getCodDepartamento());
        }
        if (servicio.getValNombre() != null) {
            departamento.setValNombre(servicio.getValNombre());
        }
        
        return departamento;
    }

    public DepartamentoTipo assembleServicio(Departamento entidad) {
        
        DepartamentoTipo departamento = new DepartamentoTipo();
        
         if (entidad.getCodigo() != null) {
            departamento.setCodDepartamento(String.valueOf(entidad.getCodigo()));
        }
        if (entidad.getValNombre() != null) {
            departamento.setValNombre(entidad.getValNombre());
        }
        
        return departamento;
    }
        
    
}
