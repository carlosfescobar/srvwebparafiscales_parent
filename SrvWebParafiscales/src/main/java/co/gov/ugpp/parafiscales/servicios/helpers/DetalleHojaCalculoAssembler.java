package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.liquidador.srvaplliquidador.SrvAplLiquidador;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.entity.AportesIndependienteHclDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContableHclDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacionDetalle;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.liquidaciones.aportesindependientetipo.v1.AportesIndependienteTipo;
import co.gov.ugpp.schema.liquidaciones.conceptocontabletipo.v1.ConceptoContableTipo;
import co.gov.ugpp.schema.liquidaciones.detallehojacalculotipo.v1.DetalleHojaCalculoTipo;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

/**
 *
 * @author Mauricio Guerrero
 */
public class DetalleHojaCalculoAssembler extends AssemblerGeneric<HojaCalculoLiquidacionDetalle, DetalleHojaCalculoTipo> {

    private static DetalleHojaCalculoAssembler DetalleHojaCalculoSingleton = new DetalleHojaCalculoAssembler();
    private ContextoRespuestaTipo ctxRespuesta;

    private DetalleHojaCalculoAssembler() {
    }

    public static DetalleHojaCalculoAssembler getInstance() {
        return DetalleHojaCalculoSingleton;
    }

    private ConceptoContableAssembler conceptoContableAssembler = ConceptoContableAssembler.getInstance();
    private AportesIndependienteAssembler aportesIndependienteAssembler = AportesIndependienteAssembler.getInstance();

    @Override
    public HojaCalculoLiquidacionDetalle assembleEntidad(DetalleHojaCalculoTipo servicio) {
        HojaCalculoLiquidacionDetalle hojaCalculoDetalle = new HojaCalculoLiquidacionDetalle();

        return hojaCalculoDetalle;
    }

    @Override
    public DetalleHojaCalculoTipo assembleServicio(HojaCalculoLiquidacionDetalle entidad) {
        DetalleHojaCalculoTipo detalleHojaCalculo = new DetalleHojaCalculoTipo();

        ctxRespuesta = new ContextoRespuestaTipo();
        ctxRespuesta.setIdTx(entidad.getId() != null ? entidad.getId().toString() : " ");
        ctxRespuesta.setFechaTx(DateUtil.currentCalendar());
        try {

            if (entidad.getIdhojacalculoliquidacion() != null) {
                detalleHojaCalculo.setIdHojaCalculoLiquidacionDetalle(entidad.getIdhojacalculoliquidacion().getId().toString());
            }
            detalleHojaCalculo.setIdNominaDetalle(entidad.getIdnominadetalle().toString());
            detalleHojaCalculo.setIdPilaDetalle(entidad.getIdpiladetalle().toString());
            detalleHojaCalculo.setIdConciliacionContableDetalle(entidad.getIdconciliacioncontabledetalle().toString());
            // Definicion de los nuevos 106 campos
            
            // Finalizacion - se agregan los 106 campos nuevos
            if (entidad.getAportaId() != null) {
                detalleHojaCalculo.setAPORTAId(entidad.getAportaId().longValue());
            }
            if (entidad.getAportaTipoIdentificacion() != null) {
                detalleHojaCalculo.setAPORTATipoIdentificacion(entidad.getAportaTipoIdentificacion().longValue());
            }
            detalleHojaCalculo.setAPORTANumeroIdentificacion(entidad.getAportaNumeroIdentificacion());
            detalleHojaCalculo.setAPORTAPrimerNombre(entidad.getAportaPrimerNombre());
            detalleHojaCalculo.setAPORTAClase(entidad.getAportaClase());
            detalleHojaCalculo.setAPORTAAporteEsapYMen(entidad.getAportaAporteEsapYMen());
            detalleHojaCalculo.setAPORTAExcepcionLey12332008(entidad.getAportaExcepcionLey12332008());
            if (entidad.getCotizId() != null) {
                detalleHojaCalculo.setCOTIZId(entidad.getCotizId().longValue());
            }
            if (entidad.getCotizTipoDocumento() != null) {
                detalleHojaCalculo.setCOTIZTipoDocumento(entidad.getCotizTipoDocumento().longValue());
            }
            detalleHojaCalculo.setCOTIZNumeroIdentificacion(entidad.getCotizNumeroIdentificacion());
            detalleHojaCalculo.setCOTIZNombre(entidad.getCotizNombre());
            if (entidad.getCotizTipoCotizante() != null) {
                detalleHojaCalculo.setCOTIZTipoCotizante(entidad.getCotizTipoCotizante().longValue());
            }
            if (entidad.getCotizSubtipoCotizante() != null) {
                detalleHojaCalculo.setCOTIZSubtipoCotizante(entidad.getCotizSubtipoCotizante().longValue());
            }
            detalleHojaCalculo.setCOTIZExtranjeroNoCotizar(entidad.getCotizExtranjeroNoCotizar());
            detalleHojaCalculo.setCOTIZColombianoEnElExt(entidad.getCotizColombianoEnElExt());
            detalleHojaCalculo.setCOTIZActividadAltoRiesgoPe(entidad.getCotizActividadAltoRiesgoPe());
            if (entidad.getCotizdAno() != null) {
                detalleHojaCalculo.setCOTIZDAno(entidad.getCotizdAno().longValue());
            }
            if (entidad.getCotizdMes() != null) {
                detalleHojaCalculo.setCOTIZDMes(entidad.getCotizdMes().longValue());
            }
            detalleHojaCalculo.setCOTIZDIng(entidad.getCotizdIng());

            if (entidad.getCotizdIngFecha() != null) {
                detalleHojaCalculo.setCOTIZDIngFecha(DateUtil.parseStrDateToCalendar(entidad.getCotizdIngFecha().toString()));
            }

            detalleHojaCalculo.setCOTIZDRet(entidad.getCotizdRet());
            if (entidad.getCotizdRetFecha() != null) {
                detalleHojaCalculo.setCOTIZDRetFecha(DateUtil.parseStrDateToCalendar(entidad.getCotizdRetFecha().toString()));
            }
            detalleHojaCalculo.setCOTIZDSln(entidad.getCotizdSln());
            detalleHojaCalculo.setCOTIZDCausalSuspension(entidad.getCotizdCausalSuspension());
            if (entidad.getCotizdCausalSuspFinicial() != null) {
                detalleHojaCalculo.setCOTIZDCausalSuspFInicial(DateUtil.parseStrDateToCalendar(entidad.getCotizdCausalSuspFinicial().toString()));
            }
            if (entidad.getCotizdCausalSuspFfinal() != null) {
                detalleHojaCalculo.setCOTIZDCausalSuspFFinal(DateUtil.parseStrDateToCalendar(entidad.getCotizdCausalSuspFfinal().toString()));
            }
            detalleHojaCalculo.setCOTIZDIge(entidad.getCotizdIge());
            if (entidad.getCotizdIgeFinicial() != null) {
                detalleHojaCalculo.setCOTIZDIgeFInicial(DateUtil.parseStrDateToCalendar(entidad.getCotizdIgeFinicial().toString()));
            }
            if (entidad.getCotizdIgeFfinal() != null) {
                detalleHojaCalculo.setCOTIZDIgeFFinal(DateUtil.parseStrDateToCalendar(entidad.getCotizdIgeFfinal().toString()));
            }
            if (entidad.getDiasIncapacidadGeneral() != null) {
                detalleHojaCalculo.setDiasIncapacidadGeneral(entidad.getDiasIncapacidadGeneral().longValue());
            }
            detalleHojaCalculo.setCOTIZDLma(entidad.getCotizdLma());
            if (entidad.getCotizdLmaFinicial() != null) {
                detalleHojaCalculo.setCOTIZDLmaFInicial(DateUtil.parseStrDateToCalendar(entidad.getCotizdLmaFinicial().toString()));
            }
            if (entidad.getCotizdLmaFfinal() != null) {
                detalleHojaCalculo.setCOTIZDLmaFFinal(DateUtil.parseStrDateToCalendar(entidad.getCotizdLmaFfinal().toString()));
            }
            if (entidad.getDiasLicenciaMaternidad() != null) {
                detalleHojaCalculo.setDiasLicenciaMaternidad(entidad.getDiasLicenciaMaternidad().longValue());
            }
            detalleHojaCalculo.setCOTIZDVac(entidad.getCotizdVac());
//        if (entidad.getCotizdVacFinicial() != null) {
//            detalleHojaCalculo.setcotizdv COTIZDVacFInicial(getXMLGregorianCalendar(entidad.getCotizdVacFinicial()));
//        }
//        if (entidad.getCotizdVacFfinal() != null) {
//            detalleHojaCalculo.setCOTIZDVacFfinal(getXMLGregorianCalendar(entidad.getCotizdVacFfinal()));
//        }
            if (entidad.getDiasVacaciones() != null) {
                detalleHojaCalculo.setDiasVacaciones(entidad.getDiasVacaciones().longValue());
            }
            detalleHojaCalculo.setCOTIZDIrp(entidad.getCotizdIrp());
            if (entidad.getCotizdIrpFinicial() != null) {
                detalleHojaCalculo.setCOTIZDIrpFInicial(DateUtil.parseStrDateToCalendar(entidad.getCotizdIrpFinicial().toString()));
            }
            if (entidad.getCotizdIrpFfinal() != null) {
                detalleHojaCalculo.setCOTIZDIrpFFinal(DateUtil.parseStrDateToCalendar(entidad.getCotizdIrpFfinal().toString()));
            }
            if (entidad.getDiasIncapacidadRp() != null) {
                detalleHojaCalculo.setDiasIncapacidadRp(entidad.getDiasIncapacidadRp().longValue());
            }
            detalleHojaCalculo.setCOTIZDSalarioIntegral(entidad.getCotizdSalarioIntegral());
            if (entidad.getDiasLaborados() != null) {
                detalleHojaCalculo.setDiasLaborados(entidad.getDiasLaborados().longValue());
            }
            detalleHojaCalculo.setCOTIZDUltimoIbcSinNovedad(entidad.getCotizdUltimoIbcSinNovedad());
            detalleHojaCalculo.setIngPilaAT(entidad.getIngPilaAt());
            detalleHojaCalculo.setRetPilaAU(entidad.getRetPilaAu());
            detalleHojaCalculo.setSlnPilaAV(entidad.getSlnPilaAv());
            detalleHojaCalculo.setIgePilaAU(entidad.getIgePilaAw());
            detalleHojaCalculo.setLmaPilaAU(entidad.getLmaPilaAx());
            detalleHojaCalculo.setVacPilaAU(entidad.getVacPilaAy());
            detalleHojaCalculo.setIrpPilaAU(entidad.getIrpPilaAz());
            if (entidad.getPorcentajePagosNoSalariaBb() != null) {
                detalleHojaCalculo.setPorcentajePagosNoSalariaBB(entidad.getPorcentajePagosNoSalariaBb());
            }
            if (entidad.getExcedeLimitePagoNoSalarBc() != null) {
                detalleHojaCalculo.setExcedeLimitePagoNoSalarBC(entidad.getExcedeLimitePagoNoSalarBc());
            }
            detalleHojaCalculo.setCodigoAdministradoraBD(entidad.getCodigoAdministradoraBd());
            detalleHojaCalculo.setNombreCortoAdministradoraBE(entidad.getNombreCortoAdministradoraBe());
            if (entidad.getIbcCoreBf() != null) {
                detalleHojaCalculo.setIbcCoreBF(entidad.getIbcCoreBf());
            }
            if (entidad.getTarifaCoreBg() != null) {
                detalleHojaCalculo.setTarifaCoreBG(entidad.getTarifaCoreBg());
            }
            if (entidad.getCotizacionObligatoriaBh() != null) {
                detalleHojaCalculo.setCotizacionObligatoriaBH(entidad.getCotizacionObligatoriaBh());
            }
            if (entidad.getDiasCotizadosPilaBi() != null) {
                detalleHojaCalculo.setDiasCotizadosPilaBI(entidad.getDiasCotizadosPilaBi().longValue());
            }
            if (entidad.getIbcPilaBj() != null) {
                detalleHojaCalculo.setIbcPilaBJ(entidad.getIbcPilaBj());
            }
            if (entidad.getCotizacionPagadaPilaBl() != null) {
                detalleHojaCalculo.setCotizacionPagadaPilaBL(entidad.getCotizacionPagadaPilaBl());
            }
            if (entidad.getAjusteBm() != null) {
                detalleHojaCalculo.setAjusteBM(entidad.getAjusteBm());
            }
            detalleHojaCalculo.setConceptoAjusteBN(entidad.getConceptoAjusteBn());
            detalleHojaCalculo.setTipoIncumplimientoBO(entidad.getTipoIncumplimientoBo());
            detalleHojaCalculo.setCodigoAdministradoraBP(entidad.getCodigoAdministradoraBp());
            detalleHojaCalculo.setNombreCortoAdministradoraBQ(entidad.getNombreCortoAdministradoraBq());
            if (entidad.getIbcCoreBr() != null) {
                detalleHojaCalculo.setIbcCoreBR(entidad.getIbcCoreBr());
            }
            if (entidad.getTarifaCorePensionBs() != null) {
                detalleHojaCalculo.setTarifaCorePensionBS(entidad.getTarifaCorePensionBs());
            }
            if (entidad.getCotizacionObligatoAPensiBt() != null) {
                detalleHojaCalculo.setCotizacionObligatoAPensiBT(entidad.getCotizacionObligatoAPensiBt());
            }
            if (entidad.getTarifaCoreFspBu() != null) {
                detalleHojaCalculo.setTarifaCoreFspBU(entidad.getTarifaCoreFspBu());
            }
            if (entidad.getCotizacionObligatoriaFspBv() != null) {
                detalleHojaCalculo.setCotizacionObligatoriaFspBV(entidad.getCotizacionObligatoriaFspBv());
            }
            if (entidad.getTarifaCorePensionAdicArBw() != null) {
                detalleHojaCalculo.setTarifaCorePensionAdicArBW(entidad.getTarifaCorePensionAdicArBw());
            }
            if (entidad.getCotizacionObliPenAdicArBx() != null) {
                detalleHojaCalculo.setCotizacionObliPenAdicArBX(entidad.getCotizacionObliPenAdicArBx());
            }
            if (entidad.getDiasCotizadosPilaBy() != null) {
                detalleHojaCalculo.setDiasCotizadosPilaBY(entidad.getDiasCotizadosPilaBy().longValue());
            }
            if (entidad.getIbcPilaBz() != null) {
                detalleHojaCalculo.setIbcPilaBZ(entidad.getIbcPilaBz());
            }
            if (entidad.getTarifaPensionCa() != null) {
                detalleHojaCalculo.setTarifaPensionCA(entidad.getTarifaPensionCa());
            }
            if (entidad.getCotizacionPagadaPensionCb() != null) {
                detalleHojaCalculo.setCotizacionPagadaPensionCB(entidad.getCotizacionPagadaPensionCb());
            }
            if (entidad.getAjustePensionCc() != null) {
                detalleHojaCalculo.setAjustePensionCC(entidad.getAjustePensionCc());
            }
            detalleHojaCalculo.setConceptoAjustePensionCD(entidad.getConceptoAjustePensionCd());
            if (entidad.getTarifaPilaFspCe() != null) {
                detalleHojaCalculo.setTarifaPilaFspCE(entidad.getTarifaPilaFspCe());
            }
            if (entidad.getCotizacionPagadaFspCf() != null) {
                detalleHojaCalculo.setCotizacionPagadaFspCF(entidad.getCotizacionPagadaFspCf());
            }
            if (entidad.getAjusteFspCg() != null) {
                detalleHojaCalculo.setAjusteFspCG(entidad.getAjusteFspCg());
            }
            detalleHojaCalculo.setConceptoAjusteFspCH(entidad.getConceptoAjusteFspCh());
            if (entidad.getTarifaPilaPensiAdicioArCi() != null) {
                detalleHojaCalculo.setTarifaPilaPensiAdicioArCI(entidad.getTarifaPilaPensiAdicioArCi());
            }
            if (entidad.getCotizacionPagPensAdicArCj() != null) {
                detalleHojaCalculo.setCotizacionPagPensAdicArCJ(entidad.getCotizacionPagPensAdicArCj());
            }
            if (entidad.getAjustePensionAdicionalArCk() != null) {
                detalleHojaCalculo.setAjustePensionAdicionalArCK(entidad.getAjustePensionAdicionalArCk());
            }
            detalleHojaCalculo.setConceptoAjustePensionArCL(entidad.getConceptoAjustePensionArCl());
            detalleHojaCalculo.setTipoIncumplimientoCM(entidad.getTipoIncumplimientoCm());
            if (entidad.getCalculoActuarialCn() != null) {
                detalleHojaCalculo.setCalculoActuarialCN(entidad.getCalculoActuarialCn());
            }
            detalleHojaCalculo.setCodigoAdministradoraCO(entidad.getCodigoAdministradoraCo());
            detalleHojaCalculo.setNombreCortoAdministradoraCP(entidad.getNombreCortoAdministradoraCp());
            if (entidad.getIbcCoreCq() != null) {
                detalleHojaCalculo.setIbcCoreCQ(entidad.getIbcCoreCq());
            }
            if (entidad.getCotizacionObligatoriaCr() != null) {
                detalleHojaCalculo.setCotizacionObligatoriaCR(entidad.getCotizacionObligatoriaCr());
            }
            if (entidad.getDiasCotizadosPilaCs() != null) {
                detalleHojaCalculo.setDiasCotizadosPilaCS(entidad.getDiasCotizadosPilaCs().longValue());
            }
            if (entidad.getIbcPilaCt() != null) {
                detalleHojaCalculo.setIbcPilaCT(entidad.getIbcPilaCt());
            }
            if (entidad.getTarifaPilaCu() != null) {
                detalleHojaCalculo.setTarifaPilaCU(entidad.getTarifaPilaCu());
            }
            if (entidad.getCotizacionPagadaPilaCv() != null) {
                detalleHojaCalculo.setCotizacionPagadaPilaCV(entidad.getCotizacionPagadaPilaCv());
            }
            if (entidad.getAjusteCw() != null) {
                detalleHojaCalculo.setAjusteCW(entidad.getAjusteCw());
            }
            detalleHojaCalculo.setConceptoAjusteCX(entidad.getConceptoAjusteCx());
            detalleHojaCalculo.setTipoIncumplimientoCY(entidad.getTipoIncumplimientoCy());
            detalleHojaCalculo.setNombreCortoAdministradoraCZ(entidad.getNombreCortoAdministradoraCz());
            if (entidad.getIbcCoreDa() != null) {
                detalleHojaCalculo.setIbcCoreDA(entidad.getIbcCoreDa());
            }
            if (entidad.getTarifaCoreDb() != null) {
                detalleHojaCalculo.setTarifaCoreDB(entidad.getTarifaCoreDb());
            }
            if (entidad.getCotizacionObligatoriaDc() != null) {
                detalleHojaCalculo.setCotizacionObligatoriaDC(entidad.getCotizacionObligatoriaDc());
            }
            if (entidad.getDiasCotizadosPilaDd() != null) {
                detalleHojaCalculo.setDiasCotizadosPilaDD(entidad.getDiasCotizadosPilaDd().longValue());
            }
            if (entidad.getIbcPilaDe() != null) {
                detalleHojaCalculo.setIbcPilaDE(entidad.getIbcPilaDe());
            }
            if (entidad.getTarifaPilaDf() != null) {
                detalleHojaCalculo.setTarifaPilaDF(entidad.getTarifaPilaDf());
            }
            detalleHojaCalculo.setCotizacionPagadaPilaDG(entidad.getCotizacionPagadaPilaDg());
            if (entidad.getAjusteDh() != null) {
                detalleHojaCalculo.setAjusteDH(entidad.getAjusteDh());
            }
            detalleHojaCalculo.setConceptoAjusteDI(entidad.getConceptoAjusteDi());
            detalleHojaCalculo.setTipoIncumplimientoDJ(entidad.getTipoIncumplimientoDj());
            if (entidad.getIbcCoreDk() != null) {
                detalleHojaCalculo.setIbcCoreDK(entidad.getIbcCoreDk());
            }
            if (entidad.getTarifaCoreDl() != null) {
                detalleHojaCalculo.setTarifaCoreDL(entidad.getTarifaCoreDl());
            }
            if (entidad.getAporteDm() != null) {
                detalleHojaCalculo.setAporteDM(entidad.getAporteDm());
            }
            if (entidad.getTarifaPilaDn() != null) {
                detalleHojaCalculo.setTarifaPilaDN(entidad.getTarifaPilaDn());
            }
            if (entidad.getAportePilaDo() != null) {
                detalleHojaCalculo.setAportePilaDO(entidad.getAportePilaDo());
            }
            if (entidad.getAjusteDp() != null) {
                detalleHojaCalculo.setAjusteDP(entidad.getAjusteDp());
            }
            detalleHojaCalculo.setConceptoAjusteDQ(entidad.getConceptoAjusteDq());
            detalleHojaCalculo.setTipoIncumplimientoDR(entidad.getTipoIncumplimientoDr());
            if (entidad.getIbcCoreDs() != null) {
                detalleHojaCalculo.setIbcCoreDS(entidad.getIbcCoreDs());
            }
            if (entidad.getIbcCoreDs() != null) {
                detalleHojaCalculo.setTarifaCoreDT(entidad.getTarifaCoreDt());
            }
            if (entidad.getAporteDu() != null) {
                detalleHojaCalculo.setAporteDU(entidad.getAporteDu());
            }
            if (entidad.getTarifaPilaDv() != null) {
                detalleHojaCalculo.setTarifaPilaDV(entidad.getTarifaPilaDv());
            }
            if (entidad.getAportePilaDw() != null) {
                detalleHojaCalculo.setAportePilaDW(entidad.getAportePilaDw());
            }
            if (entidad.getAjusteDx() != null) {
                detalleHojaCalculo.setAjusteDX(entidad.getAjusteDx());
            }
            detalleHojaCalculo.setConceptoAjusteDY(entidad.getConceptoAjusteDy());
            detalleHojaCalculo.setTipoIncumplimientoDZ(entidad.getTipoIncumplimientoDz());
            if (entidad.getIbcEa() != null) {
                detalleHojaCalculo.setIbcEA(entidad.getIbcEa());
            }
            if (entidad.getTarifaCoreEb() != null) {
                detalleHojaCalculo.setTarifaCoreEB(entidad.getTarifaCoreEb());
            }
            if (entidad.getAporteEc() != null) {
                detalleHojaCalculo.setAporteEC(entidad.getAporteEc());
            }
            if (entidad.getTarifaPilaEd() != null) {
                detalleHojaCalculo.setTarifaPilaED(entidad.getTarifaPilaEd());
            }
            if (entidad.getAportePilaEe() != null) {
                detalleHojaCalculo.setAportePilaEE(entidad.getAportePilaEe());
            }
            if (entidad.getAjusteEf() != null) {
                detalleHojaCalculo.setAjusteEF(entidad.getAjusteEf());
            }
            detalleHojaCalculo.setConceptoAjusteEG(entidad.getConceptoAjusteEg());
            detalleHojaCalculo.setTipoIncumplimientoEH(entidad.getTipoIncumplimientoEh());
            if (entidad.getIbcCoreEi() != null) {
                detalleHojaCalculo.setIbcCoreEI(entidad.getIbcCoreEi());
            }
            if (entidad.getTarifaCoreEj() != null) {
                detalleHojaCalculo.setTarifaCoreEJ(entidad.getTarifaCoreEj());
            }
            if (entidad.getAporteEk() != null) {
                detalleHojaCalculo.setAporteEK(entidad.getAporteEk());
            }
            if (entidad.getTarifaPilaEl() != null) {
                detalleHojaCalculo.setTarifaPilaEL(entidad.getTarifaPilaEl());
            }
            if (entidad.getAporteEm() != null) {
                detalleHojaCalculo.setAporteEM(entidad.getAporteEm());
            }
            if (entidad.getAjusteEn() != null) {
                detalleHojaCalculo.setAjusteEN(entidad.getAjusteEn());
            }
            detalleHojaCalculo.setConceptoAjusteEO(entidad.getConceptoAjusteEo());
            detalleHojaCalculo.setTipoIncumplimientoEP(entidad.getTipoIncumplimientoEp());

            for (ConceptoContableHclDetalle conceptoContable : entidad.getConceptoContableHclDetalleCollection()) {
                ConceptoContableTipo conceptoContableTipo = conceptoContableAssembler.assembleServicio(conceptoContable);
                detalleHojaCalculo.getConceptoContableTipo().add(conceptoContableTipo);
            }

            for (AportesIndependienteHclDetalle aporteIndependiente : entidad.getAportesIndependienteHclDetaCollection()) {
                AportesIndependienteTipo aportesIndependienteTipo = aportesIndependienteAssembler.assembleServicio(aporteIndependiente);
                detalleHojaCalculo.getAportesIndependienteTipo().add(aportesIndependienteTipo);
            }

            ctxRespuesta.setCodEstadoTx(ErrorEnum.FALLO.getCode());

        } catch (Exception ex) {
            ctxRespuesta.setCodEstadoTx(ErrorEnum.FALLO.getCode());
            Logger.getLogger(SrvAplLiquidador.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }

        detalleHojaCalculo.setContextoRespuestaTipo(ctxRespuesta);

        return detalleHojaCalculo;
    }

    private XMLGregorianCalendar getXMLGregorianCalendar(Date fecha) {
        XMLGregorianCalendar xmlDate = null;
        try {
            GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTime(fecha);
            xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (DatatypeConfigurationException ex) {
            Logger.getLogger(DetalleHojaCalculoAssembler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return xmlDate;
    }
}
