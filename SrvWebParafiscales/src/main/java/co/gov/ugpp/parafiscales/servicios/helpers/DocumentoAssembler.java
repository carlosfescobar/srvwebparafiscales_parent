package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;

public class DocumentoAssembler extends AssemblerGeneric<DocumentoEcm, DocumentoTipo> {

    private static DocumentoAssembler documentoSingleton = new DocumentoAssembler();

    private DocumentoAssembler() {
    }

    public static DocumentoAssembler getInstance() {
        return documentoSingleton;
    }

    private MetadataDocumentoAssembler metadataDocumentoAssembler = MetadataDocumentoAssembler.getInstance();

    public DocumentoEcm assembleEntidad(DocumentoTipo servicio) {

        DocumentoEcm documentoEntidad = new DocumentoEcm();

        return documentoEntidad;

    }

    public DocumentoTipo assembleServicio(DocumentoEcm entidad) {

        DocumentoTipo servicio = new DocumentoTipo();

        //        TO-DO -> FALTA DE DEFINICION SI USAR ARCHIVOTIPO O DOCUMENTOTIPO
        servicio.setIdDocumento(entidad.getId());
        servicio.setValNombreDocumento(entidad.getValNombre());
        servicio.setFecDocumento(DateUtil.parseCalendarToStrDateTime(entidad.getFecDocumento()));
        servicio.setCodTipoDocumento(entidad.getCodTipoDocumento());
        servicio.setIdRadicadoCorrespondencia(entidad.getNumeroRadicado());
        servicio.setFecRadicacionCorrespondencia(DateUtil.parseCalendarToStrDateTime(entidad.getFecRadicado()));
        servicio.setValPaginas(entidad.getValPaginas() != null ? entidad.getValPaginas(): Long.valueOf("0") );
        servicio.setValAutorOriginador(entidad.getValAutoOriginador());
        servicio.setValNaturalezaDocumento(entidad.getValNaturalezaDocumento());
        servicio.setValOrigenDocumento(entidad.getValOrigenDocumento());
        servicio.setDescObservacionLegibilidad(entidad.getDescObsLegibilidad());
        servicio.setEsMover(String.valueOf(entidad.getEsMover()));
        servicio.setValNombreTipoDocumental(entidad.getValNombreTipoDocumental());
        servicio.setNumFolios(entidad.getNumFolios() != null ? entidad.getNumFolios() :  Long.valueOf("0"));
        servicio.setValLegible(entidad.getValLegible());
        servicio.setValTipoFirma(entidad.getValTipoFirma());
        servicio.setMetadataDocumento(metadataDocumentoAssembler.assembleServicio(entidad.getMetadataDocumento()));

        return servicio;

    }

}
