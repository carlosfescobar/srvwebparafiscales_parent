package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;
import java.util.Calendar;

public class ExpedienteAssembler extends AssemblerGeneric<Expediente, ExpedienteTipo> {

    private static ExpedienteAssembler expedienteSingleton = new ExpedienteAssembler();

    private ExpedienteAssembler() {
    }

    public static ExpedienteAssembler getInstance() {
        return expedienteSingleton;
    }

    private MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    public Expediente assembleEntidad(ExpedienteTipo servicio) {

        Expediente expedienteEntidad = new Expediente();

        return expedienteEntidad;
    }

    @Override
    public ExpedienteTipo assembleServicio(Expediente entidad) {

        ExpedienteTipo servicio = new ExpedienteTipo();

        servicio.setIdNumExpediente(entidad.getId());

        IdentificacionTipo identificacionTipo = new IdentificacionTipo();

        if (entidad != null && entidad.getPersona() != null) {

            identificacionTipo.setCodTipoIdentificacion(entidad.getPersona().getCodTipoIdentificacion() != null ? entidad.getPersona().getCodTipoIdentificacion().getId() : null);
            identificacionTipo.setValNumeroIdentificacion(entidad.getPersona().getValNumeroIdentificacion() != null ? entidad.getPersona().getValNumeroIdentificacion() : null);
            identificacionTipo.setMunicipioExpedicion(entidad.getPersona().getMunicipio() != null ? municipioAssembler.assembleServicio(entidad.getPersona().getMunicipio()) : null);

        }
        servicio.setIdentificacion(identificacionTipo);

        servicio.setCodSeccion(entidad.getIdSeccion() == null ? null : entidad.getIdSeccion().toString());
        servicio.setCodSubSeccion(entidad.getIdSubseccion() == null ? null : entidad.getIdSubseccion().toString());
        
        SerieDocumentalTipo serieDocumentalTipo = new SerieDocumentalTipo();
        serieDocumentalTipo.setCodSerie(entidad.getIdSerieDocuemental() == null ? null : entidad.getIdSerieDocuemental().toString());
        serieDocumentalTipo.setCodSubserie(entidad.getIdSubserieDocumental() == null ? null : entidad.getIdSubserieDocumental().toString());
        servicio.setSerieDocumental(serieDocumentalTipo);
        
        servicio.setValFondo(entidad.getIdFondo() == null ? null : entidad.getIdFondo().toString());
        servicio.setValEntidadPredecesora(entidad.getIdEntidadPredecesora() == null ? null : entidad.getIdEntidadPredecesora().toString());

        return servicio;
    }

    public Expediente assembleEntidad(String idNumExpediente) {

        Expediente expedienteEntidad = new Expediente();
        expedienteEntidad.setId(idNumExpediente);

        return expedienteEntidad;
    }

}
