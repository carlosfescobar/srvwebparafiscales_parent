package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;

public class IdentificacionAssembler extends AssemblerGeneric<Identificacion, IdentificacionTipo> {

    private static IdentificacionAssembler identificacionSingleton = new IdentificacionAssembler();
    private MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    private IdentificacionAssembler() {
    }

    public static IdentificacionAssembler getInstance() {
        return identificacionSingleton;
    }

    public Identificacion assembleEntidad(IdentificacionTipo servicio) {

        Identificacion entidad = new Identificacion();

        entidad.setCodTipoIdentificacion(servicio.getCodTipoIdentificacion());
        entidad.setValNumeroIdentificacion(servicio.getValNumeroIdentificacion());

        return entidad;
    }

    public IdentificacionTipo assembleServicio(Identificacion entidad) {

        IdentificacionTipo servicio = new IdentificacionTipo();

        servicio.setCodTipoIdentificacion(entidad.getCodTipoIdentificacion());
        servicio.setValNumeroIdentificacion(entidad.getValNumeroIdentificacion());
        if (entidad.getMunicipio() != null) {
            servicio.setMunicipioExpedicion(municipioAssembler.assembleServicio(entidad.getMunicipio()));
        }

        return servicio;
    }

}
