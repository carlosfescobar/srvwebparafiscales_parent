package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class InteresadoAssembler extends AssemblerGeneric<Persona, InteresadoTipo> {

    private static InteresadoAssembler interesadoSingleton = new InteresadoAssembler();

    private InteresadoAssembler() {
    }

    public static InteresadoAssembler getInstance() {
        return interesadoSingleton;
    }

    private PersonaAssembler personaAssembler = PersonaAssembler.getInstance();

    public Persona assembleEntidad(InteresadoTipo servicio) {

        return new Persona();
    }

    public InteresadoTipo assembleServicio(Persona entidad) {

        InteresadoTipo interesadoTipo = new InteresadoTipo();

        Object persona = personaAssembler.assembleServicio(entidad);
        
        if (persona instanceof PersonaJuridicaTipo) {
            interesadoTipo.setPersonaJuridica((PersonaJuridicaTipo)persona);
        }else if (persona instanceof PersonaNaturalTipo) {
            interesadoTipo.setPersonaNatural((PersonaNaturalTipo)persona);
        }

        return interesadoTipo;
    }

}
