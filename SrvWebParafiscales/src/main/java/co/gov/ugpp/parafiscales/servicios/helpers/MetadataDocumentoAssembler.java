package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocAgrupador;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocumento;
import java.util.ArrayList;
import java.util.Collection;

import co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.MetadataDocumentoTipo;
import java.util.List;

public class MetadataDocumentoAssembler extends AssemblerGeneric<MetadataDocumento, MetadataDocumentoTipo> {

    private static MetadataDocumentoAssembler metadataDocumentoSingleton = new MetadataDocumentoAssembler();

    private MetadataDocumentoAssembler() {
    }

    public static MetadataDocumentoAssembler getInstance() {
        return metadataDocumentoSingleton;
    }

    private SerieDocumentalAssembler serieDocumentalAssembler = SerieDocumentalAssembler.getInstance();

    public MetadataDocumento assembleEntidad(MetadataDocumentoTipo servicio) {

        MetadataDocumento metadataDocumentoEntidad = new MetadataDocumento();

        if (servicio.getSerieDocumental() != null) {
            metadataDocumentoEntidad.setSerieDocumental(serieDocumentalAssembler.assembleEntidad(servicio.getSerieDocumental()));
        }
        if (servicio.getValAgrupador() != null) {

            Collection<MetadataDocAgrupador> valoresAgrupador = new ArrayList<MetadataDocAgrupador>();
            for (String string : servicio.getValAgrupador()) {
                MetadataDocAgrupador agrupador = new MetadataDocAgrupador();
                agrupador.setValAgrupador(string);

            }

            metadataDocumentoEntidad.getMetadataDocAgrupadorList().addAll(valoresAgrupador);
        }

        return metadataDocumentoEntidad;
    }

    public MetadataDocumentoTipo assembleServicio(MetadataDocumento entidad) {

        MetadataDocumentoTipo metadataDocumentoServicio = new MetadataDocumentoTipo();

        if (entidad != null) {
            if (entidad.getMetadataDocAgrupadorList() != null) {

                List<String> valAgru = new ArrayList<String>();
                for (MetadataDocAgrupador metaDoc : entidad.getMetadataDocAgrupadorList()) {
                    valAgru.add(metaDoc.getValAgrupador());
                }
                metadataDocumentoServicio.getValAgrupador().addAll(valAgru);
            }

            if (entidad.getSerieDocumental() != null) {
                metadataDocumentoServicio.setSerieDocumental(serieDocumentalAssembler.assembleServicio(entidad.getSerieDocumental()));
            }

        }

        return metadataDocumentoServicio;
    }

}
