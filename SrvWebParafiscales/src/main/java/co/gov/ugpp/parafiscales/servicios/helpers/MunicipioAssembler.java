package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.parafiscales.persistence.dao.DepartamentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Departamento;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class MunicipioAssembler extends AssemblerGeneric<Municipio, MunicipioTipo> {
    
    private static MunicipioAssembler municipioAssembler = new MunicipioAssembler();
    
    private MunicipioAssembler() {
    }
    
    public static MunicipioAssembler getInstance() {
        return municipioAssembler;
    }
    
    @EJB
    MunicipioDao municipioDao;
    
    @EJB
    DepartamentoDao departamentoDao;
    
    DepartamentoAssembler departamentoAssembler = DepartamentoAssembler.getInstance();
    
    public Municipio assembleEntidad(MunicipioTipo servicio) {
        
        Municipio municipio = null;
//        Departamento departamento = null;
        try {
            if (servicio != null && servicio.getDepartamento() != null) {
                if (StringUtils.isNotBlank(servicio.getCodMunicipio()) ) {
                    municipio = municipioDao.findByCodigo(servicio.getCodMunicipio());
//                    departamento = departamentoDao.find(Long.parseLong(servicio.getDepartamento().getCodDepartamento()));
                }
            }else{
                return null;
            }
            
            if (municipio == null) {
                return null;
            }
//            if (servicio.getCodMunicipio() != null) {
//                municipio.setCodigo(Long.valueOf(servicio.getCodMunicipio()));
//            }
//            if (servicio.getDepartamento() != null) {
//                municipio.setDepartamento(departamento);
//            }
//            if (servicio.getValNombre() != null) {
//                municipio.setValNombre(servicio.getValNombre());
//            }
            
        } catch (Exception ex) {
            Logger.getLogger(MunicipioAssembler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return municipio;
    }
    
    public MunicipioTipo assembleServicio(Municipio entidad) {
        
        MunicipioTipo municipio = new MunicipioTipo();
        if (entidad != null) {
            if (entidad.getCodigo() != null) {
                municipio.setCodMunicipio(String.valueOf(entidad.getCodigo()));
            }
            if (entidad.getDepartamento() != null) {
                municipio.setDepartamento(departamentoAssembler.assembleServicio(entidad.getDepartamento()));
            }
            if (entidad.getValNombre() != null) {
                municipio.setValNombre(entidad.getValNombre());
            }
        }
        
        return municipio;
    }
    
}
