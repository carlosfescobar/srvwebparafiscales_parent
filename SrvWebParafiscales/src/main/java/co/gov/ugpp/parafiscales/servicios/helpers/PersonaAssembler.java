package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Util;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class PersonaAssembler extends AssemblerGeneric<Persona, Object> {

    private static PersonaAssembler interesadoSingleton = new PersonaAssembler();

    private PersonaAssembler() {
    }

    public static PersonaAssembler getInstance() {
        return interesadoSingleton;
    }

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AportanteAssembler.class);

    private ContactoPersonaAssembler contactoPersonaAssembler = ContactoPersonaAssembler.getInstance();
    private MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    public Persona assembleEntidad(Object servicio) {

        return new Persona();
    }

    public Object assembleServicio(Persona entidad) {

        final String tipoPersona = entidad.getCodNaturalezaPersona() == null ? null : entidad.getCodNaturalezaPersona().getId();

        if (TipoPersonaEnum.PERSONA_NATURAL.getCode().equals(tipoPersona)) {

            if (entidad.getClasificacionPersona() == null) {
                entidad.setClasificacionPersona(ClasificacionPersonaEnum.PERSONA_NATURAL);
            }

            final ValorDominio tipoPersonaActual = Util.obtenerUltimaClasificacionPersona(entidad);

            Persona personaEntidad = entidad;
            PersonaNaturalTipo personaNaturalTipo = new PersonaNaturalTipo();

            try {

                personaNaturalTipo.setCodTipoPersonaNatural(tipoPersonaActual == null ? null : tipoPersonaActual.getId());
                personaNaturalTipo.setDescTipoPersonaNatural(tipoPersonaActual == null ? null : tipoPersonaActual.getNombre());

            } catch (Exception e) {
                LOG.info("No existe el codigo natural de la persona");

            }

            if (personaEntidad.getCodEstadoCivil() != null) {
                personaNaturalTipo.setCodEstadoCivil(personaEntidad.getCodEstadoCivil().getId());
            }
            if (personaEntidad.getCodNivelEducativo() != null) {
                personaNaturalTipo.setCodNivelEducativo(personaEntidad.getCodNivelEducativo().getId());
            }
            if (personaEntidad.getCodSexo() != null) {
                personaNaturalTipo.setCodSexo(personaEntidad.getCodSexo().getId());
            }
            if (personaEntidad.getContacto() != null) {
                personaNaturalTipo.setContacto(contactoPersonaAssembler.assembleServicio(personaEntidad.getContacto()));
            }
            if (personaEntidad.getCodEstadoCivil() != null) {
                personaNaturalTipo.setDescEstadoCivil(personaEntidad.getCodEstadoCivil().getNombre());
            }
            if (personaEntidad.getCodNivelEducativo() != null) {
                personaNaturalTipo.setDescNivelEducativo(personaEntidad.getCodNivelEducativo().getNombre());
            }
            if (personaEntidad.getCodSexo() != null) {
                personaNaturalTipo.setDescSexo(personaEntidad.getCodSexo().getNombre());
            }
            if (personaEntidad.getAbogado() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getAbogado().getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getAbogado().getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));
                personaNaturalTipo.setIdAbogado(identificacionTipo);
            }
            if (personaEntidad.getAutorizado() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getAutorizado().getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getAutorizado().getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));
                personaNaturalTipo.setIdAutorizado(identificacionTipo);
            }
            if (personaEntidad.getCodTipoIdentificacion() != null && personaEntidad.getValNumeroIdentificacion() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));
                personaNaturalTipo.setIdPersona(identificacionTipo);
            }
            if (StringUtils.isNotBlank(personaEntidad.getValPrimerNombre())) {
                personaNaturalTipo.setValPrimerNombre(personaEntidad.getValPrimerNombre());
            }
            if (StringUtils.isNotBlank(personaEntidad.getValSegundoNombre())) {
                personaNaturalTipo.setValSegundoNombre(personaEntidad.getValSegundoNombre());
            }
            if (StringUtils.isNotBlank(personaEntidad.getValPrimerApellido())) {
                personaNaturalTipo.setValPrimerApellido(personaEntidad.getValPrimerApellido());
            }
            if (StringUtils.isNotBlank(personaEntidad.getValSegundoApellido())) {
                personaNaturalTipo.setValSegundoApellido(personaEntidad.getValSegundoApellido());
            }
            if (personaEntidad.getCodFuente() != null) {
                personaNaturalTipo.setCodFuente(personaEntidad.getCodFuente().getId());
            }
            if (StringUtils.isNotBlank(personaEntidad.getValNombreRazonSocial())) {
                personaNaturalTipo.setValNombreCompleto(personaEntidad.getValNombreRazonSocial());
            }

            return personaNaturalTipo;

        } else if (TipoPersonaEnum.PERSONA_JURIDICA.getCode()
                .equals(tipoPersona)) {

            if (entidad.getClasificacionPersona() == null) {
                entidad.setClasificacionPersona(ClasificacionPersonaEnum.PERSONA_JURIDICA);
            }

            final ValorDominio tipoPersonaActual = Util.obtenerUltimaClasificacionPersona(entidad);

            PersonaJuridicaTipo personaJuridicaTipo = new PersonaJuridicaTipo();

            Persona personaEntidad = entidad;

            try {
                personaJuridicaTipo.setCodTipoPersonaJuridica(tipoPersonaActual == null ? null : tipoPersonaActual.getId());
                personaJuridicaTipo.setDescTipoPersonaJuridica(tipoPersonaActual == null ? null : tipoPersonaActual.getNombre());

            } catch (Exception e) {
                LOG.info("No existe el codigo natural de la persona");

            }

            if (personaEntidad.getContacto() != null) {
                personaJuridicaTipo.setContacto(contactoPersonaAssembler.assembleServicio(personaEntidad.getContacto()));
            }
            personaJuridicaTipo.setCodSeccionActividadEconomica(personaEntidad.getCodSeccionActividadEcon() == null ? null : personaEntidad.getCodSeccionActividadEcon().getId());
            personaJuridicaTipo.setDescSeccionActividadEconomica(personaEntidad.getCodSeccionActividadEcon() == null ? null : personaEntidad.getCodSeccionActividadEcon().getDescDescSeccion());
            personaJuridicaTipo.setCodDivisionActividadEconomica(personaEntidad.getCodDivisionActividadEco() == null ? null : personaEntidad.getCodDivisionActividadEco().getId().toString());
            personaJuridicaTipo.setDescDivisionActividadEconomica(personaEntidad.getCodDivisionActividadEco() == null ? null : personaEntidad.getCodDivisionActividadEco().getDescDivision());
            personaJuridicaTipo.setCodGrupoActividadEconomica(personaEntidad.getCodGrupoActividadEconom() == null ? null : personaEntidad.getCodGrupoActividadEconom().getId().toString());
            personaJuridicaTipo.setDescGrupoActividadEconomica(personaEntidad.getCodGrupoActividadEconom() == null ? null : personaEntidad.getCodGrupoActividadEconom().getDescGrupo());
            personaJuridicaTipo.setCodClaseActividadEconomica(personaEntidad.getCodClaseActividadEconom() == null ? null : personaEntidad.getCodClaseActividadEconom().getId().toString());
            personaJuridicaTipo.setDescClaseActividadEconomica(personaEntidad.getCodClaseActividadEconom() == null ? null : personaEntidad.getCodClaseActividadEconom().getDescClase());
            if (personaEntidad.getAbogado() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getAbogado().getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getAbogado().getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));

                personaJuridicaTipo.setIdAbogado(identificacionTipo);
            }
            if (personaEntidad.getAutorizado() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getAutorizado().getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getAutorizado().getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));

                personaJuridicaTipo.setIdAutorizado(identificacionTipo);
            }
            if (personaEntidad.getRepresentanteLegal() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getRepresentanteLegal().getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getRepresentanteLegal().getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));

                personaJuridicaTipo.setIdRepresentanteLegal(identificacionTipo);
            }
            if (personaEntidad.getCodTipoIdentificacion() != null && personaEntidad.getValNumeroIdentificacion() != null) {
                IdentificacionTipo identificacionTipo = new IdentificacionTipo();
                identificacionTipo.setCodTipoIdentificacion(personaEntidad.getCodTipoIdentificacion().getId());
                identificacionTipo.setValNumeroIdentificacion(personaEntidad.getValNumeroIdentificacion());
                identificacionTipo.setMunicipioExpedicion(municipioAssembler.assembleServicio(personaEntidad.getMunicipio()));

                personaJuridicaTipo.setIdPersona(identificacionTipo);
            }
            if (StringUtils.isNotBlank(personaEntidad.getValNombreRazonSocial())) {
                personaJuridicaTipo.setValNombreRazonSocial(personaEntidad.getValNombreRazonSocial());
            }
            if (personaEntidad.getValNumeroTrabajadores() != null) {
                personaJuridicaTipo.setValNumTrabajadores(String.valueOf(personaEntidad.getValNumeroTrabajadores()));
            }
            if (personaEntidad.getCodFuente() != null) {
                personaJuridicaTipo.setCodFuente(personaEntidad.getCodFuente().getId());
            }

            return personaJuridicaTipo;

        }

        return null;

    }

}
