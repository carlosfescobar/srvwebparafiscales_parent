package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.SerieDocumental;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;

public class SerieDocumentalAssembler extends AssemblerGeneric<SerieDocumental, SerieDocumentalTipo> {

    private static SerieDocumentalAssembler serieDocumentalSingleton = new SerieDocumentalAssembler();

    private SerieDocumentalAssembler() {
    }

    public static SerieDocumentalAssembler getInstance() {
        return serieDocumentalSingleton;
    }

    public SerieDocumental assembleEntidad(SerieDocumentalTipo servicio) {

        SerieDocumental serieDocumentalEntidad = new SerieDocumental();

        serieDocumentalEntidad.setCodSerie(servicio.getCodSerie());
        serieDocumentalEntidad.setCodSubSerie(servicio.getCodSubserie());
        serieDocumentalEntidad.setCodTipoDocumental(servicio.getCodTipoDocumental());
        serieDocumentalEntidad.setNombreSerie(servicio.getValNombreSerie());
        serieDocumentalEntidad.setNombreSubSerie(servicio.getValNombreSubserie());

        return serieDocumentalEntidad;
    }

    public SerieDocumentalTipo assembleServicio(SerieDocumental entidad) {

        SerieDocumentalTipo serieDocumentalServicio = new SerieDocumentalTipo();

        if (entidad != null) {
            serieDocumentalServicio.setCodSerie(entidad.getCodSerie());
            serieDocumentalServicio.setCodSubserie(entidad.getCodSubSerie());
            serieDocumentalServicio.setCodTipoDocumental(entidad.getCodTipoDocumental());
            serieDocumentalServicio.setValNombreSerie(entidad.getNombreSerie());
            serieDocumentalServicio.setValNombreSubserie(entidad.getNombreSubSerie());
        }

        return serieDocumentalServicio;
    }

}
