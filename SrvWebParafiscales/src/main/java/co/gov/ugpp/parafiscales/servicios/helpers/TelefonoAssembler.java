package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class TelefonoAssembler extends AssemblerGeneric<Telefono, TelefonoTipo> {

    private static TelefonoAssembler telefonoAssembler = new TelefonoAssembler();

    private TelefonoAssembler() {
    }

    public static TelefonoAssembler getInstance() {
        return telefonoAssembler;
    }

    /**
     * No esta en uso, sin embargo, se mantiene para revision de limpieza
     * jenkins 1802
     *
     * @param servicio
     * @return
     */
    public Telefono assembleEntidad(TelefonoTipo servicio) {

        Telefono telefono = new Telefono();

        return telefono;

    }

    public TelefonoTipo assembleServicio(Telefono entidad) {

        TelefonoTipo telefono = new TelefonoTipo();

        if (entidad.getCodTipoTelefono() != null) {
            telefono.setCodTipoTelefono(entidad.getCodTipoTelefono().getId());
        }
        if (StringUtils.isNotBlank(entidad.getValNumero())) {
            telefono.setValNumeroTelefono((entidad.getValNumero()));
        }
        return telefono;
    }

}
