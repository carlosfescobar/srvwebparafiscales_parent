package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.parafiscales.persistence.entity.Traslado;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 *
 * @author franzjr
 */
public class TrasladoAssembler extends AssemblerGeneric<Traslado, TrasladoTipo> {

    private static final TrasladoAssembler trasladoAssembler = new TrasladoAssembler();

    private TrasladoAssembler() {
    }

    public static TrasladoAssembler getInstance() {
        return trasladoAssembler;
    }

    @Override
    public Traslado assembleEntidad(TrasladoTipo servicio) {

        Traslado traslado = new Traslado();

        traslado.setDescMotivoTraslado(servicio.getDescMotivoTraslado());
        traslado.setEstadoTraslado(servicio.getCodEstadoTraslado());
        traslado.setFecTraslado(servicio.getFecTraslado());
        traslado.setIdNumeroExpediente(servicio.getExpediente().getIdNumExpediente());

        return traslado;
    }

    @Override
    public TrasladoTipo assembleServicio(Traslado entidad) {

        TrasladoTipo trasladoTipo = new TrasladoTipo();

        trasladoTipo.setIdTraslado(String.valueOf(entidad.getId()));
        trasladoTipo.setCodEstadoTraslado(entidad.getEstadoTraslado());
        trasladoTipo.setDescMotivoTraslado(entidad.getDescMotivoTraslado());
        trasladoTipo.setFecTraslado(entidad.getFecTraslado());

        ExpedienteTipo expedienteTipo = null;
        
        if (StringUtils.isNotBlank(entidad.getIdNumeroExpediente())) {
            expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(entidad.getIdNumeroExpediente());
        }

        trasladoTipo.setExpediente(expedienteTipo);

        return trasladoTipo;
    }

}
