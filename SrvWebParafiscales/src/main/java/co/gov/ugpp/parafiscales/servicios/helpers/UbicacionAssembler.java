package co.gov.ugpp.parafiscales.servicios.helpers;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase intermedio entre el servicio y la persistencia.
 * @author franzjr
 */
public class UbicacionAssembler extends AssemblerGeneric<Ubicacion, UbicacionPersonaTipo>{
    
    private static UbicacionAssembler ubicacionAssembler = new UbicacionAssembler();

    private UbicacionAssembler() {
    }

    public static UbicacionAssembler getInstance() {
	return ubicacionAssembler;
    }
    
    MunicipioAssembler municipioAssembler = MunicipioAssembler.getInstance();

    public Ubicacion assembleEntidad(UbicacionPersonaTipo servicio) {
        
        Ubicacion ubicacion = new Ubicacion();
        
        if (StringUtils.isNotBlank(servicio.getCodTipoDireccion())) {
            ubicacion.setCodTipoDireccion(new ValorDominio(servicio.getCodTipoDireccion()));
        }
        if (servicio.getMunicipio() != null) {
            ubicacion.setMunicipio(municipioAssembler.assembleEntidad(servicio.getMunicipio()));
        }
        if (StringUtils.isNotBlank(servicio.getValDireccion())) {
            ubicacion.setValDireccion(servicio.getValDireccion());
        }
        
        return ubicacion;
        
    }

    public UbicacionPersonaTipo assembleServicio(Ubicacion entidad) {
        
        UbicacionPersonaTipo ubicacion = new UbicacionPersonaTipo();
        
        if (entidad.getCodTipoDireccion() != null) {
            ubicacion.setCodTipoDireccion(entidad.getCodTipoDireccion().getId());
            ubicacion.setDescTipoDireccion(entidad.getCodTipoDireccion().getNombre());
        }
        if (entidad.getMunicipio() != null) {
            ubicacion.setMunicipio(municipioAssembler.assembleServicio(entidad.getMunicipio()));
        }
        if (StringUtils.isNotBlank(entidad.getValDireccion())) {
            ubicacion.setValDireccion(entidad.getValDireccion());
        }
        
        return ubicacion;
    }
        
    
}
