package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplConciliacionContable;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplconciliacioncontable.ConciliacionContableTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import java.io.Serializable;

/**
 *
 * @author franzjr
 */
public interface ConciliacionContableFacade extends Serializable {

    /**
     * Lee de las tablas "definitivas" los archivos de contabilidad y los traduce a un modelo relacional
     * @param idExpediente
     * @param envio
     * @param contextoSolicitud
     * @return 
     */
    public ConciliacionContableTipo crearConciliacionContable(String idExpediente, EnvioTipo envio, ContextoTransaccionalTipo contextoSolicitud) throws AppException;

}
