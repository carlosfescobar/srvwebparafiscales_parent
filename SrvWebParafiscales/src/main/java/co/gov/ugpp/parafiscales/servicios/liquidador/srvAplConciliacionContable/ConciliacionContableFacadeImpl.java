package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplConciliacionContable;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.liquidador.srvaplconciliacioncontable.ConciliacionContableTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AplicacionFormulaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConceptoContableDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConciliacionContableDao;
import co.gov.ugpp.parafiscales.persistence.dao.ConciliacionContableDetalleDao;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContable;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormulaDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaDefinitivaDao;
import co.gov.ugpp.parafiscales.persistence.dao.NominaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorFormulaDao;
import co.gov.ugpp.parafiscales.persistence.entity.AplicacionFormula;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContableDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaDefiniti;
import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import co.gov.ugpp.parafiscales.persistence.entity.ValorFormula;
import co.gov.ugpp.parafiscales.servicios.util.liquidador.Contabilidad;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class ConciliacionContableFacadeImpl extends AbstractFacade
        implements
        ConciliacionContableFacade {

    @EJB
    private InformacionExternaDefinitivaDao informacionExternaDefinitivaDao;

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private NominaDao nominaDao;

    @EJB
    private ConciliacionContableDao conciliacionContableDao;

    @EJB
    private ConciliacionContableDetalleDao conciliacionContableDetalleDao;

    @EJB
    private ConceptoContableDao conceptoContableDao;

    @EJB
    private AplicacionFormulaDao aplicacionFormulaDao;

    @EJB
    private FormulaDao formulaDao;

    @EJB
    private ValorFormulaDao valorFormulaDao;

    @Override

    public ConciliacionContableTipo crearConciliacionContable(String idExpediente, EnvioTipo envio,
            ContextoTransaccionalTipo contextoSolicitud) throws AppException {

        if (org.apache.commons.lang3.StringUtils.isBlank(idExpediente) || envio == null || envio.getRadicacion() == null || org.apache.commons.lang3.StringUtils.isBlank(envio.getRadicacion().getIdRadicadoEntrada())) {
            throw new AppException("Los parametros de entrada no pueden ser nulos");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        String idRadicadoEnvio = envio.getRadicacion().getIdRadicadoEntrada();

        ConciliacionContable conciliacionContable = new ConciliacionContable();

        EnvioInformacionExterna envioInformacionExterna = new EnvioInformacionExterna();
        envioInformacionExterna.setIdRadicadoEnvio(idRadicadoEnvio);

        List<InformacionExternaDefiniti> informacionExternaDefinitivaList = null;

        try {
            informacionExternaDefinitivaList = informacionExternaDefinitivaDao.findByRadicado(idRadicadoEnvio);
        } catch (AppException exception) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                    "Error en la consulta de resultados por id radicado: " + exception.getMessage()));
        }

        if (informacionExternaDefinitivaList == null || informacionExternaDefinitivaList.isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El objeto 'Envio' no tiene archivos asociados"));
        }

        Expediente expediente = expedienteDao.find(idExpediente);

        if (expediente == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El expediente proporcionado no existe con el id: " + idExpediente));
        }

        List<Nomina> listNomina = nominaDao.findByExpediente(idExpediente);
        Nomina nomina = null;

        if (listNomina == null || listNomina.isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No existe nomina relacionada con el id expediente proporcionado: " + idExpediente));
        } else {
            nomina = listNomina.get(0);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        String idConciliacionContable = null;

        for (InformacionExternaDefiniti informacionExternaDefiniti : informacionExternaDefinitivaList) {

            File file_temporal = new File("TEMP- ");

            try {
                FileOutputStream output_stream = new FileOutputStream(file_temporal);
                output_stream.write(informacionExternaDefiniti.getValContenidoArchivo());
                output_stream.close();

                InputStream input_stream = new ByteArrayInputStream(informacionExternaDefiniti.getValContenidoArchivo());

                GregorianCalendar gc = new GregorianCalendar();

                conciliacionContable.setEstado("1");
                conciliacionContable.setFechacreacion(DateUtil.currentCalendar());
                conciliacionContable.setIdexpediente(expediente);
                conciliacionContable.setIdinformacionexternadefinitiva(informacionExternaDefiniti.getId());
                conciliacionContable.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());

                idConciliacionContable = conciliacionContableDao.create(conciliacionContable).toString();

                Contabilidad contabilidad = new Contabilidad();

                List<ConciliacionContableDetalle> listConciliacionContableDetalles = new ArrayList<ConciliacionContableDetalle>();

                try {
                    listConciliacionContableDetalles = contabilidad.registrarArchivo(input_stream, conciliacionContable, errorTipoList);

                } catch (Exception e) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "Hubo un problema general al validar el archivo: " + e.getMessage()));
                    if (!errorTipoList.isEmpty()) {
                        throw new AppException(errorTipoList);
                    }
                }

                Hashtable<String, Object> params = new Hashtable<String, Object>();

                for (ConciliacionContableDetalle conciliacionContableDetalleItem : listConciliacionContableDetalles) {

                    BigDecimal nunom = BigDecimal.valueOf(0);
                    params.clear();
                    if (nomina != null && nomina.getId() != null) {

                        List<ConceptoContable> ndl = conceptoContableDao.findByCAI("%" + conciliacionContableDetalleItem.getNumerocuentacontable() + "%", nomina.getId(), conciliacionContableDetalleItem.getAno());
                        for (ConceptoContable nd : ndl) {
                            nunom = nunom.add(nd.getValor());
                        }
                    } else {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No existe nomina relacionada con el id expediente proporcionado: " + idExpediente));
                    }
                    conciliacionContableDetalleItem.setId(Long.valueOf(System.currentTimeMillis()));
                    conciliacionContableDetalleItem.setValornomina(BigDecimal.valueOf(nunom.longValue()));
                    conciliacionContableDetalleItem.setDiferencia(BigDecimal.valueOf(conciliacionContableDetalleItem.getValornetomovimiento().doubleValue()
                            - conciliacionContableDetalleItem.getValornomina().doubleValue()));
                    conciliacionContableDetalleItem.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());

                    conciliacionContableDetalleDao.create(conciliacionContableDetalleItem);

                    List<AplicacionFormula> listAplicacionFormulas = aplicacionFormulaDao.findByActivaVigente("Validaciòn de Archivo de Contabilidad", new Date());

                    for (AplicacionFormula aplicacionFormulaItem : listAplicacionFormulas) {
                        try {
                            if (aplicacionFormulaItem.getCampoDestino().split("\\.")[0].equals("ConciliacionContableDetalle")) {
                                List<Object> listado = formulaDao.ejecutarFormula(aplicacionFormulaItem.getFormula(), "ConciliacionContableDetalle",
                                        conciliacionContableDetalleItem.getId());
                                for (Object ob : listado) {
                                    String query = "UPDATE ConciliacionContableDetalle SET "
                                            + aplicacionFormulaItem.getCampoDestino().split("\\.")[1] + " = " + ob.toString() + " "
                                            + " WHERE id = :id ";
                                    try {
                                        conciliacionContableDetalleDao.ejecutarUpdate(query, conciliacionContableDetalleItem.getId());
                                    } catch (AppException exception) {
                                        throw new AppException(ErrorEnum.ERROR_NO_ESPERADO.getMessage(), exception);
                                    }
                                }
                            } else {
                                if (aplicacionFormulaItem.getCampoDestino().equals("ValorFormula.valor")) {
                                    List<Object> listado = formulaDao.ejecutarFormula(aplicacionFormulaItem.getFormula(),
                                            "ConciliacionContableDetalle", conciliacionContableDetalleItem.getId());
                                    Long id = System.currentTimeMillis();
                                    for (Object ob : listado) {
                                        ValorFormula valorFormula = new ValorFormula();
                                        valorFormula.setId(Long.valueOf(id));
                                        valorFormula.setFecha(gc.getTime());
                                        valorFormula.setIdTabla(BigInteger.valueOf(conciliacionContableDetalleItem.getId().longValue()));
                                        valorFormula.setAplicacionFormula(aplicacionFormulaItem);
                                        valorFormula.setValor(BigInteger.valueOf(Long.valueOf(ob.toString())));
                                        
                                        valorFormula.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());
                                        
                                        valorFormulaDao.create(valorFormula);
                                        id++;
                                    }
                                }
                            }
                        } catch (Exception exception) {
                            throw new AppException(ErrorEnum.ERROR_NO_ESPERADO.getMessage(), exception);
                        }
                    }
                }

            } catch (IOException exception) {
                throw new AppException(ErrorEnum.ERROR_NO_ESPERADO.getMessage(), exception);

            } catch (AppException exception) {
                throw new AppException(exception.getMessage());
            }
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        ConciliacionContableTipo conciliacionContableTipo = new ConciliacionContableTipo();

        conciliacionContableTipo.setIdConciliacionContable(idConciliacionContable);

        return conciliacionContableTipo;
    }

}
