package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplConciliacionContable;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplconciliacioncontable.ConciliacionContableTipo;
import co.gov.ugpp.liquidador.srvaplconciliacioncontable.MsjOpCrearFallo;
import co.gov.ugpp.liquidador.srvaplconciliacioncontable.OpCrearResTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplConciliacionContable",
        portName = "portSrvAplConciliacionContable",
        endpointInterface = "co.gov.ugpp.liquidador.srvaplconciliacioncontable.PortSrvAplConciliacionContable")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplConciliacionContable extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplConciliacionContable.class);
    
    @EJB
    private ConciliacionContableFacade conciliacionContableFacade;

    public co.gov.ugpp.liquidador.srvaplconciliacioncontable.OpCrearResTipo opCrear(co.gov.ugpp.liquidador.srvaplconciliacioncontable.OpCrearSolTipo msjOpCrearSol) throws MsjOpCrearFallo {

        LOG.info("Op: OpCrearConciliacionContable ::: INIT");
        
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearSol.getContextoTransaccional();

        ConciliacionContableTipo conciliacion;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            conciliacion = conciliacionContableFacade.crearConciliacionContable(msjOpCrearSol.getIdExpediente(), msjOpCrearSol.getEnvio(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpCrearResTipo crearResTipo = new OpCrearResTipo();
        crearResTipo.setConciliacionContable(conciliacion);
        crearResTipo.setContextoRespuesta(contextoRespuesta);

        LOG.info("Op: OpCrearConciliacionContable ::: END");
        
        return crearResTipo;

    }

}
