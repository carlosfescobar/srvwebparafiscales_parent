package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplEnvioInformacionExterna;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface EnvioInformacionExternaFacade extends Serializable {

    /**
     * Mecanismo para realizar un envio de uno o varios archivos y asociarlos con un consecutivo "idRadicadoEnvio"
     * @param idsInformacionExternaTemporal
     * @param envio
     * @param contextoSolicitud
     * @return 
     */
    public String crearEnvioInformacionExterna(List<String> idsInformacionExternaTemporal, EnvioTipo envio, ContextoTransaccionalTipo contextoSolicitud) throws AppException ;

}
