package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplEnvioInformacionExterna;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.EnvioInformacionExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaDefinitivaDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaTemporalDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaDefiniti;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaTemporal;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class EnvioInformacionExternaFacadeImpl extends AbstractFacade implements EnvioInformacionExternaFacade {

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private FormatoDao formatoDao;

    @EJB
    private EnvioInformacionExternaDao envioInformacionExternaDao;

    @EJB
    private InformacionExternaDefinitivaDao informacionExternaDefinitivaDao;

    @EJB
    private InformacionExternaTemporalDao informacionExternaTemporalDao;

    @EJB
    private PersonaDao personaDao;

    @Override
    public String crearEnvioInformacionExterna(List<String> idsInformacionExternaTemporal, EnvioTipo envioTipo, ContextoTransaccionalTipo contextoSolicitud) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        String idEnvioInformacionExterna = "";

        try {

            if (envioTipo == null) {
                throw new AppException("El envioTipo no puede ser nulo");
            }

            final RadicacionTipo radicacionTipo = envioTipo.getRadicacion();
            final IdentificacionTipo identificacionTipo = envioTipo.getIdentificacion();
            final FormatoTipo formatoTipo = envioTipo.getFormato();

            if (radicacionTipo == null || radicacionTipo.getIdRadicadoSalida() == null || radicacionTipo.getFecRadicadoSalida()== null) {
                throw new AppException("El radicadoTipo no puede ser nulo, tampoco los campos {idRadicadoSalida, fecRadicadoSalida}");
            }

            Validator.checkRadiacion(radicacionTipo, errorTipoList);

            EnvioInformacionExterna envioInformacionExterna
                    = envioInformacionExternaDao.find(radicacionTipo.getIdRadicadoSalida());

            if (envioInformacionExterna == null) {
                envioInformacionExterna = new EnvioInformacionExterna();
            }

            if (StringUtils.isBlank(radicacionTipo.getIdRadicadoSalida())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El idRadicadoSalida es nulo:"
                ));
            } else {
                envioInformacionExterna.setIdRadicadoEnvio(radicacionTipo.getIdRadicadoSalida());
            }

            if (identificacionTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El identificacionTipo no puede ser nulo"
                ));
            }

            if (identificacionTipo != null && (StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion()) || StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion()))) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El codTipoIdentificacion y/o valNumeroIdentificacion no pueden ser nulos"
                ));
            }

            if (formatoTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El formatoTipo no puede ser nulo"
                ));
            }

            if (formatoTipo != null && (formatoTipo.getIdFormato() == null || formatoTipo.getValVersion() == null || StringUtils.isBlank(formatoTipo.getIdFormato().toString()) || StringUtils.isBlank(formatoTipo.getValVersion().toString()))) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El idFormato y/o valVersion no pueden ser nulos"
                ));
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            List<ArchivoTipo> archivosTipo = new ArrayList<ArchivoTipo>();

            for (String idInformacionExternaTemporal : idsInformacionExternaTemporal) {
                try {
                    InformacionExternaTemporal informacionExternaTemporal = informacionExternaTemporalDao.find(Long.parseLong(idInformacionExternaTemporal));

                    if (informacionExternaTemporal != null) {

                        if (!informacionExternaTemporal.getNumeroDocumento().equals(identificacionTipo.getValNumeroIdentificacion())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                    "La informacionExternaTemporal con id: " + idInformacionExternaTemporal + " con número de documento: " + informacionExternaTemporal.getNumeroDocumento() +" no coincide el numero de documento con el enviado en el envioTipo: "+identificacionTipo.getValNumeroIdentificacion()));
                        }
                        if (!informacionExternaTemporal.getCodTipoIdentificacion().getId().equals(identificacionTipo.getCodTipoIdentificacion())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                    "La informacionExternaTemporal con id: " + idInformacionExternaTemporal + " con el código de documento: "+informacionExternaTemporal.getCodTipoIdentificacion().getId()+" no coincide el codigo del tipo de documento con el enviado en el envioTipo: "+identificacionTipo.getCodTipoIdentificacion()));
                        }
                        if (!informacionExternaTemporal.getFormatoArchivoPK().getIdFormato().equals(formatoTipo.getIdFormato().longValue())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                    "La informacionExternaTemporal con id: " + idInformacionExternaTemporal + "con idFormato:"+informacionExternaTemporal.getFormatoArchivoPK().getIdFormato()+" no coincide el idFormato del formato con el enviado en el envioTipo: "+formatoTipo.getIdFormato().longValue()));
                        }
                        if (!informacionExternaTemporal.getFormatoArchivoPK().getIdVersion().equals(formatoTipo.getValVersion().longValue())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                    "La informacionExternaTemporal con id: " + idInformacionExternaTemporal + " con idVersion: "+informacionExternaTemporal.getFormatoArchivoPK().getIdVersion()+" no coincide la idVersion del formato con el enviado en el envioTipo:"+formatoTipo.getValVersion().longValue()));
                        }

                        ArchivoTipo archivo = new ArchivoTipo();
                        archivo.setValContenidoArchivo(informacionExternaTemporal.getValContenidoArchivo() == null ? null : informacionExternaTemporal.getValContenidoArchivo());
                        archivo.setValContenidoFirma(informacionExternaTemporal.getValContenidoFirma() == null ? null : informacionExternaTemporal.getValContenidoFirma());
                        archivo.setValNombreArchivo(informacionExternaTemporal.getValNombreArchivo() == null ? null : informacionExternaTemporal.getValNombreArchivo());

                        archivosTipo.add(archivo);

                    } else {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "No se encontro la informacion externa temporal con el id: "
                                + idInformacionExternaTemporal));
                    }

                } catch (Exception e) {
                    Logger.getLogger(EnvioInformacionExternaFacadeImpl.class.getCanonicalName()).log(Level.SEVERE, e.getMessage(), e);
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "Hubo un error al adjuntar el archivo de la informacion externa temporal con el id: "
                            + idInformacionExternaTemporal));
                }

            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            if (archivosTipo.size() <= 0) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "No se encontraron archivos para las informaciones externas temporales con los id's :"
                        + idsInformacionExternaTemporal.toString()));
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            try {
                envioInformacionExterna.setIdUsuarioColocacion(contextoSolicitud.getIdUsuario());
                envioInformacionExterna.setCodTipoIdentificacion(identificacionTipo == null ? null : identificacionTipo.getCodTipoIdentificacion());
                envioInformacionExterna.setValNumeroIdentificacion(identificacionTipo == null ? null : identificacionTipo.getValNumeroIdentificacion());
                envioInformacionExterna.setIdRadicadoRequerimiento(radicacionTipo.getIdRadicadoSalida());
                envioInformacionExterna.setFecRadicado(radicacionTipo.getFecRadicadoSalida());
                envioInformacionExterna.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());

                idEnvioInformacionExterna = envioInformacionExternaDao.create(envioInformacionExterna);

            } catch (AppException e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                        "Hubo un error al crear el envioInformacionExterna "));
                throw new AppException(errorTipoList);
            }

            for (ArchivoTipo archivoTipo : archivosTipo) {

                Calendar currentCalendar = DateUtil.currentCalendar();

                InformacionExternaDefiniti informacionExternaDefinitiva = new InformacionExternaDefiniti();
                informacionExternaDefinitiva.setIdUsuarioColocacion(contextoSolicitud.getIdUsuario());
                informacionExternaDefinitiva.setFecColocacion(currentCalendar);
                informacionExternaDefinitiva.setEnvioInformacionExterna(envioInformacionExterna);
                informacionExternaDefinitiva.setFechaCreacion(currentCalendar);
                informacionExternaDefinitiva.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());

                InputStream inputStream = new ByteArrayInputStream(archivoTipo.getValContenidoArchivo());
                byte[] byteStream = IOUtils.toByteArray(inputStream);

                informacionExternaDefinitiva.setValContenidoArchivo(byteStream);
                informacionExternaDefinitiva.setValNombreArchivo(archivoTipo.getValNombreArchivo());
                informacionExternaDefinitiva.setValContenidoFirma(archivoTipo.getValContenidoFirma());

                try {
                    informacionExternaDefinitivaDao.create(informacionExternaDefinitiva);

                } catch (AppException e) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                            "Hubo un error al crear la informacionExternaDefinitiva "));

                }
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            for (String idInformacionExternaTemporal : idsInformacionExternaTemporal) {
                try {
                    InformacionExternaTemporal informacionExternaTemporal = informacionExternaTemporalDao.find(Long.parseLong(idInformacionExternaTemporal));
                    informacionExternaTemporalDao.remove(informacionExternaTemporal);

                } catch (AppException e) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                            "Hubo un error al intentar borrar la información externa temporal con id: " + idInformacionExternaTemporal));

                } catch (NumberFormatException e) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                            "Hubo un error al intentar borrar la información externa temporal con id: " + idInformacionExternaTemporal + " : Al parecer el formato del id es incorrecto"));
                }
            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            return idEnvioInformacionExterna;

        } catch (AppException exception) {

            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.FALLO, exception.getMessage()));
            throw new AppException(errorTipoList);

        } catch (IOException exception) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.FALLO, exception.getMessage()));
            throw new AppException(errorTipoList);
        }

    }

    /**
     * Método que obtiene un listado de nombres de arhivo a partir de un listado
     * de archivos completo
     *
     * @param archivoTipoList el listado de archivos a procesar
     * @return el listado de nombres de archivos encontrados
     */
    private List<String> obtenerNombreArchivos(List<ArchivoTipo> archivoTipoList) {

        List<String> nombreArchivos = null;
        if (archivoTipoList != null
                && !archivoTipoList.isEmpty()) {
            nombreArchivos = new ArrayList<String>();
            for (ArchivoTipo archivoTipo : archivoTipoList) {
                nombreArchivos.add(archivoTipo.getValNombreArchivo());
            }
        }
        return nombreArchivos;
    }

}
