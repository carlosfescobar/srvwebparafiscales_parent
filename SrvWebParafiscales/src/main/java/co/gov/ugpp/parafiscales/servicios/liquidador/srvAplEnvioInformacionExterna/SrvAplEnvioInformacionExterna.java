package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplEnvioInformacionExterna;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.MsjOpCrearFallo;
import co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.OpCrearResTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplEnvioInformacionExterna", 
        portName = "portSrvAplEnvioInformacionExterna", 
        endpointInterface = "co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.PortSrvAplEnvioInformacionExterna")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplEnvioInformacionExterna extends AbstractSrvApl{
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplEnvioInformacionExterna.class);
    
    @EJB
    private EnvioInformacionExternaFacade envioInformacionExternaFacade;

    public co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.OpCrearResTipo opCrear(co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.OpCrearSolTipo msjOpCrearSol) throws co.gov.ugpp.liquidador.srvaplenvioinformacionexterna.MsjOpCrearFallo {
        LOG.info("Op: OpCrearEnvioInformacionExterna ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearSol.getContextoTransaccional();
        
        String id;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            
            id = envioInformacionExternaFacade.crearEnvioInformacionExterna(msjOpCrearSol.getIdsInformacionExternaTemporal(), msjOpCrearSol.getEnvio(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpCrearResTipo crearResTipo = new OpCrearResTipo();
        crearResTipo.setIdEnvioInformacionExterna(id);
        crearResTipo.setContextoRespuesta(contextoRespuesta);

        LOG.info("Op: OpCrearEnvioInformacionExterna ::: END");

        return crearResTipo;
    }
    
}
