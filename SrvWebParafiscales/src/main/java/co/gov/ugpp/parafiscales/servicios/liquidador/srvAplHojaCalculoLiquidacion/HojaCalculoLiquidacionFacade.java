package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplHojaCalculoLiquidacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;
import java.io.Serializable;

/**
 *
 * @author franzjr
 */
public interface HojaCalculoLiquidacionFacade extends Serializable {

    public HojaCalculoTipo consultarPorId(String idHojaCalculoLiquidacion, ContextoTransaccionalTipo contextoSolicitud) throws AppException ;

    public ArchivoTipo generarExcel(String idHojaCalculoLiquidacion, ContextoTransaccionalTipo contextoSolicitud) throws AppException ;

    

}
