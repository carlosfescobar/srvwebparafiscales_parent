package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplHojaCalculoLiquidacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.MsjOpConsultarPorIdFallo;
import co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.MsjOpGenerarExcelFallo;
import co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpConsultarPorIdResTipo;
import co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpGenerarExcelResTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.SrvAplNomina;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplHojaCalculoLiquidacion",
        portName = "portSrvAplHojaCalculoLiquidacion",
        endpointInterface = "co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.PortSrvAplHojaCalculoLiquidacion")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplHojaCalculoLiquidacion extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplNomina.class);

    @EJB
    private HojaCalculoLiquidacionFacade hojaCalculoLiquidacionFacade;

    public co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpConsultarPorIdResTipo opConsultarPorId(co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpConsultarPorIdSolTipo msjOpConsultarPorIdSol) throws MsjOpConsultarPorIdFallo {
        LOG.info("Op: OpConsultarPorId ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpConsultarPorIdSol.getContextoTransaccional();

        HojaCalculoTipo hojaCalculoTipo;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            hojaCalculoTipo = hojaCalculoLiquidacionFacade.consultarPorId(msjOpConsultarPorIdSol.getIdHojaCalculoLiquidacion(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarPorIdFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpConsultarPorIdFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpConsultarPorIdResTipo consultarPorIdResTipo = new OpConsultarPorIdResTipo();
        consultarPorIdResTipo.setContextoRespuesta(contextoRespuesta);
        consultarPorIdResTipo.setHojaCalculoLiquidacion(hojaCalculoTipo);

        LOG.info("Op: OpConsultarPorId ::: END");

        return consultarPorIdResTipo;
    }

    public co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpGenerarExcelResTipo opGenerarExcel(co.gov.ugpp.liquidador.srvaplhojacalculoliquidacion.OpGenerarExcelSolTipo msjOpGenerarExcelSol) throws MsjOpGenerarExcelFallo {
        LOG.info("Op: OpGenerarExcel ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpGenerarExcelSol.getContextoTransaccional();

        ArchivoTipo hojaCalculoLiquidacionEXCEL;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            //Double rand = Math.floor(Math.random() * (Double.valueOf(100))) + 1;

            hojaCalculoLiquidacionEXCEL = hojaCalculoLiquidacionFacade.generarExcel(msjOpGenerarExcelSol.getIdHojaCalculoLiquidacion(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error("ERROR: " + ex.getMessage(), ex);
            throw new MsjOpGenerarExcelFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error("ERROR: " + ex.getMessage(), ex);
            throw new MsjOpGenerarExcelFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpGenerarExcelResTipo generarExcelResTipo = new OpGenerarExcelResTipo();
        generarExcelResTipo.setContextoRespuesta(contextoRespuesta);
        generarExcelResTipo.setHojaCalculoLiquidacionEXCEL(hojaCalculoLiquidacionEXCEL);

        LOG.info("Op: OpGenerarExcel ::: END");

        return generarExcelResTipo;
    }

}
