package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplInformacionExterna;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.liquidador.validacioninformacionexternatemporaltipo.v1.ValidacionInformacionExternaTemporalTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface InformacionExternaFacade extends Serializable {

    /**
     * Mecanismo para validar y almacenar archivos de excel sujetos a un formato y versión en una tabla temporal.
     * @param archivo
     * @param formato
     * @param identificacion
     * @param contextoSolicitud
     * @return
     * @throws AppException 
     */
    
    public Integer colocarArchivosTemporal(ArchivoTipo archivo, FormatoTipo formato,
            IdentificacionTipo identificacion, ContextoTransaccionalTipo contextoSolicitud) throws AppException;
    
    
    public List<ValidacionInformacionExternaTemporalTipo> validarArchivosTemporal(List<ArchivoTipo> archivos, ContextoTransaccionalTipo contextoSolicitud) throws AppException;
    
    
    

}
