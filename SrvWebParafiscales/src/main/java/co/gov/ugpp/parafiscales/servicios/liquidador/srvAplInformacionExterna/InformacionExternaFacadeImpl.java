package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplInformacionExterna;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;

import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.EstructuraDao;
import co.gov.ugpp.parafiscales.persistence.dao.FormatoDao;
import co.gov.ugpp.parafiscales.persistence.dao.InformacionExternaTemporalDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoArchivoPK;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.util.files.CampoCarga;
import co.gov.ugpp.parafiscales.servicios.util.files.Estructura;
import co.gov.ugpp.parafiscales.servicios.util.files.MiLog;
import co.gov.ugpp.parafiscales.servicios.util.files.ValidarArchivoXlsxBase;
import co.gov.ugpp.parafiscales.servicios.util.files.ValidarArchivoXlsxCont;
import co.gov.ugpp.parafiscales.servicios.util.files.ValidarArchivoXlsxNomina;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.validadorinformacion.ErrorValidacionArchivoTipo;
import co.gov.ugpp.parafiscales.validadorinformacion.ValidadorArchivos;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.liquidador.validacioninformacionexternatemporaltipo.v1.ValidacionInformacionExternaTemporalTipo;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.IOUtils;


/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class InformacionExternaFacadeImpl extends AbstractFacade implements InformacionExternaFacade {

    private ValidarArchivoXlsxBase validador;

    private List<MiLog> logs;

    @EJB
    private FormatoDao formatoDao;
    @EJB
    private EstructuraDao estructuraDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private InformacionExternaTemporalDao informacionExternaTemporalDao;
    @EJB
    private ValidadorArchivos validadorArchivos;
    
    
    @Override
    public Integer colocarArchivosTemporal(ArchivoTipo archivoTipo, FormatoTipo formatoTipo, IdentificacionTipo identificacionTipo,
            ContextoTransaccionalTipo contextoSolicitud) throws AppException {
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        InformacionExternaTemporal informacionExternaTemporal = new InformacionExternaTemporal();

        if (formatoTipo == null || formatoTipo.getIdFormato() == null || formatoTipo.getValVersion() == null
                || StringUtils.isBlank(formatoTipo.getIdFormato().toString()) || StringUtils.isBlank(formatoTipo.getValVersion().toString())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar un formato valido, los campos {version, id formato} no pueden ser vacios o nulos."));
        }
        if (archivoTipo == null || StringUtils.isBlank(archivoTipo.getValNombreArchivo())
                || archivoTipo.getValContenidoArchivo() == null || archivoTipo.getValContenidoArchivo().length <= 0) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar un archivo valido, los campos {nombre, contenido archivo} no pueden ser vacios o nulos."));
        }

        if (identificacionTipo == null || StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion()) || StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar una identificaciÃ³n valida, los campos {CodTipoIdentificacion, ValNumeroIdentificacion} no pueden ser vacios o nulos."));
        } else {
            ValorDominio codTipoIdentificacion = valorDominioDao.find(identificacionTipo.getCodTipoIdentificacion());
            if (codTipoIdentificacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El tipo de codigo identificacion proporcionado no existe con el ID: " + identificacionTipo.getCodTipoIdentificacion()));
            } else {
                informacionExternaTemporal.setCodTipoIdentificacion(codTipoIdentificacion);
            }
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        Long idinformacionExternaTemporal = null;

        Calendar currentCalendar = DateUtil.currentCalendar();

        byte[] fileByteDecoded;
        InputStream inputStream;
        byte[] byteStream;

        try {
            inputStream = new ByteArrayInputStream(archivoTipo.getValContenidoArchivo());
            byteStream = IOUtils.toByteArray(inputStream);

            fileByteDecoded = byteStream;

        } catch (IOException exception) {

            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.FALLO,
                    "Ocurrio un error tratando de leer el archivo:" + archivoTipo.getValNombreArchivo() + " : " + exception.getMessage()));
            throw new AppException(errorTipoList);
        }

        if (formatoTipo == null || formatoTipo.getIdFormato() == null || formatoTipo.getValVersion() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Debe proporcionar un valor de formato valido."));
        }

        //FormatoPK formatoPkEnviado = new FormatoPK(formatoTipo.getIdFormato(), formatoTipo.getValVersion().longValue());
        FormatoArchivoPK formatoArchivoPkEnviado = new FormatoArchivoPK(formatoTipo.getIdFormato(), formatoTipo.getValVersion().longValue());

        Formato formatoEnviado = formatoDao.findFormatoByFormatoAndVersion(formatoTipo.getIdFormato(), formatoTipo.getValVersion().longValue());

        if (formatoEnviado == null) {
            throw new AppException("El codigo de formato y/o codigo de versiÃ³n no son valores aceptados.");
        }

        /*
        InformacionExternaTemporal validarArchivo = informacionExternaTemporalDao.findByArchivoAndEntidadExterna(archivoTipo, identificacionTipo);

        if (validarArchivo != null) {
            throw new AppException("Ya existe un registro con los valores ingresados. (Nombre de archivo, tipo y nÃºmero de identificaciÃ³n).");
        }
        */

        Estructura estructuraArchivo;

        estructuraArchivo = estructuraDao.findByFormato(formatoTipo.getIdFormato(), formatoTipo.getValVersion().longValue());

        if (estructuraArchivo != null) {

            if (estructuraArchivo.getEstructura() != null && !estructuraArchivo.getEstructura().equals("")) {

                if (estructuraArchivo.getCampos() != null && !estructuraArchivo.getCampos().isEmpty()) {

                    List<CampoCarga> estructura = estructuraArchivo.getCampos();

                    if (estructuraArchivo.getEstructura().equals("NOMINA")) {
                        validador = new ValidarArchivoXlsxNomina(estructura);

                    } else if (estructuraArchivo.getEstructura().equals("CONCILIACION_CONTABLE")) {
                        validador = new ValidarArchivoXlsxCont(estructura);

                    }

                    validador.validar(new ByteArrayInputStream(fileByteDecoded));

                    
                    if (getValidador() != null) {
                        if (!getValidador().getLogs().isEmpty()) {

                            setLogs(validador.getLogs());
                            for (MiLog validationLog : getLogs()) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO, validationLog.toString()));
                            }
                            if (!errorTipoList.isEmpty()) {
                                throw new AppException(errorTipoList);
                            }
                        }
                    } else {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO, "Error - Fallo al validar el archivo Excel : Archivo Excel VacÃ­o"));
                        throw new AppException(errorTipoList);
                    }

                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "Parece que la versiÃ³n y el formato ingresados tienen una estructura sin embargo no hay campos a validar"));
                }

            }

        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        try {
            informacionExternaTemporal.setFormatoArchivoPK(formatoArchivoPkEnviado);
            informacionExternaTemporal.setNumeroDocumento(identificacionTipo.getValNumeroIdentificacion());
            informacionExternaTemporal.setValNombreArchivo(archivoTipo.getValNombreArchivo());
            informacionExternaTemporal.setValContenidoArchivo(fileByteDecoded);
            informacionExternaTemporal.setValContenidoFirma(archivoTipo.getValContenidoFirma());
            informacionExternaTemporal.setFecColocacion(currentCalendar);
            informacionExternaTemporal.setFechaCreacion(currentCalendar);
            informacionExternaTemporal.setIdUsuarioColocacion(contextoSolicitud.getIdUsuario());
            informacionExternaTemporal.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());
            idinformacionExternaTemporal = informacionExternaTemporalDao.create(informacionExternaTemporal);

        } catch (AppException e) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "Error al intentar guardar la informacion Externa Temporal: " + e.getMessage()));
            throw new AppException(errorTipoList);
        }

        return (idinformacionExternaTemporal != null ? idinformacionExternaTemporal.intValue() : null);

    }

    /**
     * @return the validador
     */
    private ValidarArchivoXlsxBase getValidador() {
        return validador;
    }


    /**
     * @return the logs
     */
    private List<MiLog> getLogs() {
        return logs;
    }

    /**
     * @param logs the logs to set
     */
    private void setLogs(List<MiLog> logs) {
        this.logs = logs;
    }

    
    
    @Override
    public List<ValidacionInformacionExternaTemporalTipo> validarArchivosTemporal(List<ArchivoTipo> archivos, ContextoTransaccionalTipo contextoSolicitud) throws AppException {
        
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        List<ValidacionInformacionExternaTemporalTipo> validaciones = new ArrayList<ValidacionInformacionExternaTemporalTipo>();

        if(archivos.size() <= 0)
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,"Debe enviar al menos un archivo para validar."));

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        
        //ACTIVAR ESTE BLOQUE DE CODIGO CUANDO NECESITE QUE NO SE VALIDE EL ARCHIVO
        /*
        ValidacionInformacionExternaTemporalTipo validacion = new ValidacionInformacionExternaTemporalTipo();
        validacion.setIdArchivo("1");
        validacion.setIdEjecucion("1");
        validacion.setInfoNegocio("{PROCESO_VALIDACION=COMPLETO}");
        validacion.setEstadoValidacion("COMPLETO");
        validaciones.add(validacion);
        return validaciones;
        */
        // INACTIVAR EL FOR DE ABAJO CUANDO ACTIVEN ESTA
        
        
        for (ArchivoTipo archivo : archivos) 
        {
                
                ValidacionInformacionExternaTemporalTipo validacion = new ValidacionInformacionExternaTemporalTipo();
                
                try 
                {
                    if(!StringUtils.isBlank(archivo.getIdArchivo()))
                    {
                        InformacionExternaTemporal informacionExternaTemporal = informacionExternaTemporalDao.find(Long.parseLong(archivo.getIdArchivo()));
                        InputStream inputStream = null;
                        
                        if (informacionExternaTemporal != null) {

                            inputStream = new ByteArrayInputStream(informacionExternaTemporal.getValContenidoArchivo());

                            Map infoNegocio = new HashMap();
                            ArrayList<ErrorValidacionArchivoTipo> listaErrores;
                            Long idEjecucion;
                            ErrorTipo errorTipo;
                            String resultadoValidacion = null;

                            Map objReturn = validadorArchivos.validarArchivo(inputStream, archivo.getValNombreArchivo(), informacionExternaTemporal.getFormatoArchivoPK().getIdFormato(), informacionExternaTemporal.getFormatoArchivoPK().getIdVersion(), null, contextoSolicitud.getIdUsuario(), infoNegocio);
                            
                            
                            //System.out.println("::ANDRES::" + objReturn);
                            infoNegocio = (Map)objReturn.get("infoNegocio");
                            listaErrores = (ArrayList<ErrorValidacionArchivoTipo>)objReturn.get("listaErrores");
                            idEjecucion = (Long)objReturn.get("idEjecucion");
                            

                            if(infoNegocio != null)
                            {
                               resultadoValidacion = (String)infoNegocio.get("PROCESO_VALIDACION");
                               validacion.setInfoNegocio(infoNegocio.toString());
                            }
                               
                            validacion.setIdEjecucion(idEjecucion.toString());
                            validacion.setIdArchivo(archivo.getIdArchivo());
                            validacion.setEstadoValidacion(resultadoValidacion);
                            
                            
                            if(listaErrores != null){
                                
                                for(ErrorValidacionArchivoTipo errorValidacionArchivoTipo: listaErrores){
                                        errorTipo = new ErrorTipo(); 
                                        errorTipo.setCodError(errorValidacionArchivoTipo.getCodError());
                                        errorTipo.setValDescError(errorValidacionArchivoTipo.getValDescError());
                                        errorTipo.setValDescErrorTecnico(errorValidacionArchivoTipo.getValDescErrorTecnico());                                       
                                        validacion.getErrorTipo().add(errorTipo);      
                                }
                                
                            }
                            else
                                validacion.getErrorTipo().add(null);


                        } else {
                            
                            ErrorTipo errorTipo = new ErrorTipo();
                            errorTipo.setCodError("1");
                            errorTipo.setValDescError("No se encontro informacion Externa Temporal con el id: " + archivo.getIdArchivo());
                            errorTipo.setValDescErrorTecnico("Error Validador");
                            validacion.getErrorTipo().add(errorTipo);
                            validacion.setIdArchivo(archivo.getIdArchivo());
                            validacion.setInfoNegocio(null);
                            validacion.setEstadoValidacion("Error"); 
                        }
                    } 
                    else 
                    {
                        
                       ErrorTipo errorTipo = new ErrorTipo();
                       errorTipo.setCodError("2");
                       errorTipo.setValDescError("Id de archivo invalido");
                       errorTipo.setValDescErrorTecnico("Error Validador");
                       validacion.getErrorTipo().add(errorTipo);
                       validacion.setIdArchivo(archivo.getIdArchivo());
                       validacion.setInfoNegocio(null);
                       validacion.setEstadoValidacion("Error"); 
                    }
                    
                    validaciones.add(validacion);
                    
                        
                }
                catch (NumberFormatException e) 
                {
                    ErrorTipo errorTipo = new ErrorTipo();
                    errorTipo.setCodError("1");
                    errorTipo.setValDescError("Hubo un error NumberFormatException al procesar el archivo con id: " + archivo.getIdArchivo());
                    errorTipo.setValDescErrorTecnico("Error Validador");
                    validacion.getErrorTipo().add(errorTipo);
                    validacion.setInfoNegocio(null);
                    validacion.setIdArchivo(archivo.getIdArchivo());
                    validacion.setEstadoValidacion("NumberFormatException");    
                    Logger.getLogger(InformacionExternaFacadeImpl.class.getCanonicalName()).log(Level.SEVERE, e.getMessage(), e);
                } catch (AppException e) {
                  
                    ErrorTipo errorTipo = new ErrorTipo();
                    errorTipo.setCodError("1");
                    errorTipo.setValDescError("Hubo un error AppException al procesar el archivo con id: " + archivo.getIdArchivo());
                    errorTipo.setValDescErrorTecnico("Error Validador");
                    validacion.getErrorTipo().add(errorTipo);
                    validacion.setInfoNegocio(null);
                    validacion.setIdArchivo(archivo.getIdArchivo());
                    validacion.setEstadoValidacion("AppException");
                    Logger.getLogger(InformacionExternaFacadeImpl.class.getCanonicalName()).log(Level.SEVERE, e.getMessage(), e);
                }  

            }
         
        return validaciones;

        
    }
    
    
    
    

    public static byte[] getBytes(InputStream is) throws IOException {
        
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int len;
        byte[] data = new byte[100000];
        while ((len = is.read(data, 0, data.length)) != -1) {
        buffer.write(data, 0, len);
        }

        buffer.flush();
        return buffer.toByteArray();
    }
    
    
    

}
