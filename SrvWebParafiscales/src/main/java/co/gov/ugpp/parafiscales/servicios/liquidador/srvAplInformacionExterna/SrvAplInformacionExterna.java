package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplInformacionExterna;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplinformacionexterna.MsjOpColocarArchivosTemporalFallo;
import co.gov.ugpp.liquidador.srvaplinformacionexterna.MsjOpValidarArchivosTemporalFallo;
import co.gov.ugpp.liquidador.srvaplinformacionexterna.OpColocarArchivosTemporalResponse;
import co.gov.ugpp.liquidador.srvaplinformacionexterna.OpValidarArchivosTemporalResponse;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.liquidador.validacioninformacionexternatemporaltipo.v1.ValidacionInformacionExternaTemporalTipo;
import java.util.List;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplInformacionExterna",
        portName = "portSrvAplInformacionExterna",
        endpointInterface = "co.gov.ugpp.liquidador.srvaplinformacionexterna.PortSrvAplInformacionExterna")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplInformacionExterna extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplInformacionExterna.class);

    @EJB
    private InformacionExternaFacade informacionExternaFacade;

    
    
    public co.gov.ugpp.liquidador.srvaplinformacionexterna.OpColocarArchivosTemporalResponse opColocarArchivosTemporal(co.gov.ugpp.liquidador.srvaplinformacionexterna.OpColocarArchivosTemporalSolTipo msjOpColocarArchivosTemporalSol) throws MsjOpColocarArchivosTemporalFallo 
    {

        LOG.info("Op: opColocarArchivosTemporal ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpColocarArchivosTemporalSol.getContextoTransaccional();

        Integer id;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());


            id = informacionExternaFacade.colocarArchivosTemporal(msjOpColocarArchivosTemporalSol.getArchivo(), msjOpColocarArchivosTemporalSol.getFormato(), msjOpColocarArchivosTemporalSol.getIdentificacion(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpColocarArchivosTemporalFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpColocarArchivosTemporalFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpColocarArchivosTemporalResponse colocarArchivosTemporalResponse = new OpColocarArchivosTemporalResponse();
        colocarArchivosTemporalResponse.setId(String.valueOf(id));
        colocarArchivosTemporalResponse.setContextoRespuesta(contextoRespuesta);

        LOG.info("Op: opColocarArchivosTemporal ::: END");

        return colocarArchivosTemporalResponse;
    }
    
    
    
    
    public co.gov.ugpp.liquidador.srvaplinformacionexterna.OpValidarArchivosTemporalResponse opValidarArchivosTemporal(co.gov.ugpp.liquidador.srvaplinformacionexterna.OpValidarArchivosTemporalSolTipo msjOpValidarArchivosTemporalSol) throws MsjOpValidarArchivosTemporalFallo 
    {

        LOG.info("Op: opValidarArchivosTemporal ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpValidarArchivosTemporalSol.getContextoTransaccional();

        List<ValidacionInformacionExternaTemporalTipo> resultadoValidacion;
        
     
        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());


            resultadoValidacion = informacionExternaFacade.validarArchivosTemporal(msjOpValidarArchivosTemporalSol.getArchivo(), contextoSolicitud);
          
            
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarArchivosTemporalFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpValidarArchivosTemporalFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpValidarArchivosTemporalResponse validarArchivosTemporalResponse = new OpValidarArchivosTemporalResponse();
        
        for (ValidacionInformacionExternaTemporalTipo validacionInformacionExternaTemporalTipo : resultadoValidacion) {
            validarArchivosTemporalResponse.getValidacionInformacionExternaTemporal().add(validacionInformacionExternaTemporalTipo);
        }
        
        //validarArchivosTemporalResponse.setValidacionInformacionExternaTemporal(resultadoValidacion);
        validarArchivosTemporalResponse.setContextoRespuesta(contextoRespuesta);
        
        
        LOG.info("Op: opValidarArchivosTemporal ::: END");

        return validarArchivosTemporalResponse;
    }
    
    
    
    
    

}
