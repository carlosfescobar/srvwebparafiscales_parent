
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import java.util.Calendar;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter1
    extends XmlAdapter<String, Calendar>
{


    public Calendar unmarshal(String value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseStrDateTimeToCalendar(value));
    }

    public String marshal(Calendar value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseCalendarToStrDateTime(value));
    }

}
