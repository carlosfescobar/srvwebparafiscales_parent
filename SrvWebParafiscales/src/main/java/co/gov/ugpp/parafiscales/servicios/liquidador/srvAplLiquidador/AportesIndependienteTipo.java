
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para AportesIndependienteTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AportesIndependienteTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAportesIndependiente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="idHojaCalculoLiquidacionDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="tipo_ingreso" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/>
 *         &lt;element name="descripcion_ingreso" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}descripcion_ingreso" minOccurs="0" form="qualified"/>
 *         &lt;element name="numero_contrato" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}numero_contrato" minOccurs="0" form="qualified"/>
 *         &lt;element name="contrato_fInicio" type="{http://www.w3.org/2001/XMLSchema}dateTime" form="qualified"/>
 *         &lt;element name="contrato_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime" form="qualified"/>
 *         &lt;element name="valor_contrato" type="{http://www.w3.org/2001/XMLSchema}decimal" form="qualified"/>
 *         &lt;element name="HOJATRAD_duracion_ing_meses_EZ" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="HOJATRAD_ano_FA" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="HOJATRAD_mes_FB" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/>
 *         &lt;element name="HOJATRAD_valor_mensualizado_FC" type="{http://www.w3.org/2001/XMLSchema}decimal" form="qualified"/>
 *         &lt;element name="HOJATRAD_ingreso_depurado_FD" type="{http://www.w3.org/2001/XMLSchema}decimal" form="qualified"/>
 *         &lt;element name="observaciones_ingresos" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_ingresos" minOccurs="0" form="qualified"/>
 *         &lt;element name="fecha_costo" type="{http://www.w3.org/2001/XMLSchema}dateTime" form="qualified"/>
 *         &lt;element name="descripcion_costo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}descripcion_costo" minOccurs="0" form="qualified"/>
 *         &lt;element name="proveedor_costo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}proveedor_costo" minOccurs="0" form="qualified"/>
 *         &lt;element name="numero_factura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/>
 *         &lt;element name="valor_costo" type="{http://www.w3.org/2001/XMLSchema}decimal" form="qualified"/>
 *         &lt;element name="aceptacion_costo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}aceptacion_costo" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_costo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_costo" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_novedades" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_novedades" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_salud" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_salud" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_pension" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_pension" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_arl" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_arl" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_sena" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_sena" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_icbf" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_icbf" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_men" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_men" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_ccf" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_ccf" minOccurs="0" form="qualified"/>
 *         &lt;element name="observaciones_esap" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}observaciones_esap" minOccurs="0" form="qualified"/>
 *         &lt;element name="estado" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}estado" minOccurs="0" form="qualified"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AportesIndependienteTipo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", propOrder = {
    "idAportesIndependiente",
    "idHojaCalculoLiquidacionDetalle",
    "tipoIngreso",
    "descripcionIngreso",
    "numeroContrato",
    "contratoFInicio",
    "contratoFFinal",
    "valorContrato",
    "hojatradDuracionIngMesesEZ",
    "hojatradAnoFA",
    "hojatradMesFB",
    "hojatradValorMensualizadoFC",
    "hojatradIngresoDepuradoFD",
    "observacionesIngresos",
    "fechaCosto",
    "descripcionCosto",
    "proveedorCosto",
    "numeroFactura",
    "valorCosto",
    "aceptacionCosto",
    "observacionesCosto",
    "observacionesNovedades",
    "observacionesSalud",
    "observacionesPension",
    "observacionesArl",
    "observacionesSena",
    "observacionesIcbf",
    "observacionesMen",
    "observacionesCcf",
    "observacionesEsap",
    "estado"
})
public class AportesIndependienteTipo {

    @XmlElement(namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String idAportesIndependiente;
    @XmlElement(namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String idHojaCalculoLiquidacionDetalle;
    @XmlElement(name = "tipo_ingreso", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long tipoIngreso;
    @XmlElement(name = "descripcion_ingreso", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String descripcionIngreso;
    @XmlElement(name = "numero_contrato", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String numeroContrato;
    @XmlElement(name = "contrato_fInicio", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar contratoFInicio;
    @XmlElement(name = "contrato_fFinal", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar contratoFFinal;
    @XmlElement(name = "valor_contrato", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, nillable = true)
    protected BigDecimal valorContrato;
    @XmlElement(name = "HOJATRAD_duracion_ing_meses_EZ", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long hojatradDuracionIngMesesEZ;
    @XmlElement(name = "HOJATRAD_ano_FA", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long hojatradAnoFA;
    @XmlElement(name = "HOJATRAD_mes_FB", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long hojatradMesFB;
    @XmlElement(name = "HOJATRAD_valor_mensualizado_FC", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, nillable = true)
    protected BigDecimal hojatradValorMensualizadoFC;
    @XmlElement(name = "HOJATRAD_ingreso_depurado_FD", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, nillable = true)
    protected BigDecimal hojatradIngresoDepuradoFD;
    @XmlElement(name = "observaciones_ingresos", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesIngresos;
    @XmlElement(name = "fecha_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaCosto;
    @XmlElement(name = "descripcion_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String descripcionCosto;
    @XmlElement(name = "proveedor_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String proveedorCosto;
    @XmlElement(name = "numero_factura", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String numeroFactura;
    @XmlElement(name = "valor_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", required = true, nillable = true)
    protected BigDecimal valorCosto;
    @XmlElement(name = "aceptacion_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String aceptacionCosto;
    @XmlElement(name = "observaciones_costo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesCosto;
    @XmlElement(name = "observaciones_novedades", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesNovedades;
    @XmlElement(name = "observaciones_salud", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesSalud;
    @XmlElement(name = "observaciones_pension", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesPension;
    @XmlElement(name = "observaciones_arl", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesArl;
    @XmlElement(name = "observaciones_sena", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesSena;
    @XmlElement(name = "observaciones_icbf", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesIcbf;
    @XmlElement(name = "observaciones_men", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesMen;
    @XmlElement(name = "observaciones_ccf", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesCcf;
    @XmlElement(name = "observaciones_esap", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String observacionesEsap;
    @XmlElement(namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1", nillable = true)
    protected String estado;

    /**
     * Obtiene el valor de la propiedad idAportesIndependiente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAportesIndependiente() {
        return idAportesIndependiente;
    }

    /**
     * Define el valor de la propiedad idAportesIndependiente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAportesIndependiente(String value) {
        this.idAportesIndependiente = value;
    }

    /**
     * Obtiene el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculoLiquidacionDetalle() {
        return idHojaCalculoLiquidacionDetalle;
    }

    /**
     * Define el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculoLiquidacionDetalle(String value) {
        this.idHojaCalculoLiquidacionDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIngreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getTipoIngreso() {
        return tipoIngreso;
    }

    /**
     * Define el valor de la propiedad tipoIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIngreso(Long value) {
        this.tipoIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionIngreso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionIngreso() {
        return descripcionIngreso;
    }

    /**
     * Define el valor de la propiedad descripcionIngreso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionIngreso(String value) {
        this.descripcionIngreso = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroContrato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroContrato() {
        return numeroContrato;
    }

    /**
     * Define el valor de la propiedad numeroContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroContrato(String value) {
        this.numeroContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad contratoFInicio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getContratoFInicio() {
        return contratoFInicio;
    }

    /**
     * Define el valor de la propiedad contratoFInicio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContratoFInicio(Calendar value) {
        this.contratoFInicio = value;
    }

    /**
     * Obtiene el valor de la propiedad contratoFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getContratoFFinal() {
        return contratoFFinal;
    }

    /**
     * Define el valor de la propiedad contratoFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContratoFFinal(Calendar value) {
        this.contratoFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad valorContrato.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorContrato() {
        return valorContrato;
    }

    /**
     * Define el valor de la propiedad valorContrato.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorContrato(BigDecimal value) {
        this.valorContrato = value;
    }

    /**
     * Obtiene el valor de la propiedad hojatradDuracionIngMesesEZ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getHOJATRADDuracionIngMesesEZ() {
        return hojatradDuracionIngMesesEZ;
    }

    /**
     * Define el valor de la propiedad hojatradDuracionIngMesesEZ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOJATRADDuracionIngMesesEZ(Long value) {
        this.hojatradDuracionIngMesesEZ = value;
    }

    /**
     * Obtiene el valor de la propiedad hojatradAnoFA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getHOJATRADAnoFA() {
        return hojatradAnoFA;
    }

    /**
     * Define el valor de la propiedad hojatradAnoFA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOJATRADAnoFA(Long value) {
        this.hojatradAnoFA = value;
    }

    /**
     * Obtiene el valor de la propiedad hojatradMesFB.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getHOJATRADMesFB() {
        return hojatradMesFB;
    }

    /**
     * Define el valor de la propiedad hojatradMesFB.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHOJATRADMesFB(Long value) {
        this.hojatradMesFB = value;
    }

    /**
     * Obtiene el valor de la propiedad hojatradValorMensualizadoFC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHOJATRADValorMensualizadoFC() {
        return hojatradValorMensualizadoFC;
    }

    /**
     * Define el valor de la propiedad hojatradValorMensualizadoFC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHOJATRADValorMensualizadoFC(BigDecimal value) {
        this.hojatradValorMensualizadoFC = value;
    }

    /**
     * Obtiene el valor de la propiedad hojatradIngresoDepuradoFD.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getHOJATRADIngresoDepuradoFD() {
        return hojatradIngresoDepuradoFD;
    }

    /**
     * Define el valor de la propiedad hojatradIngresoDepuradoFD.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setHOJATRADIngresoDepuradoFD(BigDecimal value) {
        this.hojatradIngresoDepuradoFD = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesIngresos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesIngresos() {
        return observacionesIngresos;
    }

    /**
     * Define el valor de la propiedad observacionesIngresos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesIngresos(String value) {
        this.observacionesIngresos = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaCosto() {
        return fechaCosto;
    }

    /**
     * Define el valor de la propiedad fechaCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaCosto(Calendar value) {
        this.fechaCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad descripcionCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionCosto() {
        return descripcionCosto;
    }

    /**
     * Define el valor de la propiedad descripcionCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionCosto(String value) {
        this.descripcionCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad proveedorCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProveedorCosto() {
        return proveedorCosto;
    }

    /**
     * Define el valor de la propiedad proveedorCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProveedorCosto(String value) {
        this.proveedorCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroFactura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroFactura() {
        return numeroFactura;
    }

    /**
     * Define el valor de la propiedad numeroFactura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroFactura(String value) {
        this.numeroFactura = value;
    }

    /**
     * Obtiene el valor de la propiedad valorCosto.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValorCosto() {
        return valorCosto;
    }

    /**
     * Define el valor de la propiedad valorCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValorCosto(BigDecimal value) {
        this.valorCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad aceptacionCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAceptacionCosto() {
        return aceptacionCosto;
    }

    /**
     * Define el valor de la propiedad aceptacionCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAceptacionCosto(String value) {
        this.aceptacionCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesCosto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesCosto() {
        return observacionesCosto;
    }

    /**
     * Define el valor de la propiedad observacionesCosto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesCosto(String value) {
        this.observacionesCosto = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesNovedades.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesNovedades() {
        return observacionesNovedades;
    }

    /**
     * Define el valor de la propiedad observacionesNovedades.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesNovedades(String value) {
        this.observacionesNovedades = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesSalud() {
        return observacionesSalud;
    }

    /**
     * Define el valor de la propiedad observacionesSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesSalud(String value) {
        this.observacionesSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesPension() {
        return observacionesPension;
    }

    /**
     * Define el valor de la propiedad observacionesPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesPension(String value) {
        this.observacionesPension = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesArl() {
        return observacionesArl;
    }

    /**
     * Define el valor de la propiedad observacionesArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesArl(String value) {
        this.observacionesArl = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesSena() {
        return observacionesSena;
    }

    /**
     * Define el valor de la propiedad observacionesSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesSena(String value) {
        this.observacionesSena = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesIcbf() {
        return observacionesIcbf;
    }

    /**
     * Define el valor de la propiedad observacionesIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesIcbf(String value) {
        this.observacionesIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesMen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesMen() {
        return observacionesMen;
    }

    /**
     * Define el valor de la propiedad observacionesMen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesMen(String value) {
        this.observacionesMen = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesCcf() {
        return observacionesCcf;
    }

    /**
     * Define el valor de la propiedad observacionesCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesCcf(String value) {
        this.observacionesCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad observacionesEsap.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getObservacionesEsap() {
        return observacionesEsap;
    }

    /**
     * Define el valor de la propiedad observacionesEsap.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setObservacionesEsap(String value) {
        this.observacionesEsap = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

}
