
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para ContextoRespuestaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ContextoRespuestaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTx" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}idTx2"/>
 *         &lt;element name="codEstadoTx" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fechaTx" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="idInstanciaProceso" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}idInstanciaProceso2" minOccurs="0"/>
 *         &lt;element name="idInstanciaActividad" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}idInstanciaActividad2" minOccurs="0"/>
 *         &lt;element name="valCantidadPaginas" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="valNumPagina" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContextoRespuestaTipo", namespace = "http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1", propOrder = {
    "idTx",
    "codEstadoTx",
    "fechaTx",
    "idInstanciaProceso",
    "idInstanciaActividad",
    "valCantidadPaginas",
    "valNumPagina"
})
public class ContextoRespuestaTipo {

    @XmlElement(required = true)
    protected String idTx;
    @XmlElement(required = true)
    protected String codEstadoTx;
    @XmlElement(required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaTx;
    @XmlElement(nillable = true)
    protected String idInstanciaProceso;
    @XmlElement(nillable = true)
    protected String idInstanciaActividad;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valCantidadPaginas;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valNumPagina;

    /**
     * Obtiene el valor de la propiedad idTx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTx() {
        return idTx;
    }

    /**
     * Define el valor de la propiedad idTx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTx(String value) {
        this.idTx = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoTx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoTx() {
        return codEstadoTx;
    }

    /**
     * Define el valor de la propiedad codEstadoTx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoTx(String value) {
        this.codEstadoTx = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaTx.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaTx() {
        return fechaTx;
    }

    /**
     * Define el valor de la propiedad fechaTx.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaTx(Calendar value) {
        this.fechaTx = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    /**
     * Define el valor de la propiedad idInstanciaProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaProceso(String value) {
        this.idInstanciaProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaActividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaActividad() {
        return idInstanciaActividad;
    }

    /**
     * Define el valor de la propiedad idInstanciaActividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaActividad(String value) {
        this.idInstanciaActividad = value;
    }

    /**
     * Obtiene el valor de la propiedad valCantidadPaginas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValCantidadPaginas() {
        return valCantidadPaginas;
    }

    /**
     * Define el valor de la propiedad valCantidadPaginas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCantidadPaginas(Long value) {
        this.valCantidadPaginas = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumPagina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValNumPagina() {
        return valNumPagina;
    }

    /**
     * Define el valor de la propiedad valNumPagina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumPagina(Long value) {
        this.valNumPagina = value;
    }

}
