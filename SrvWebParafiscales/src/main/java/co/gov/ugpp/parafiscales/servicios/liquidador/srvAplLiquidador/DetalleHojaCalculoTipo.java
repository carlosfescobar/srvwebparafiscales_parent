
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Clase Java para DetalleHojaCalculoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DetalleHojaCalculoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHojaCalculoLiquidacionDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNominaDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPilaDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idConciliacionContableDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="APORTA_id" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="APORTA_Tipo_Identificacion" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="APORTA_Numero_Identificacion" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}APORTA_Numero_Identificacion" minOccurs="0"/>
 *         &lt;element name="APORTA_Primer_Nombre" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}APORTA_Primer_Nombre" minOccurs="0"/>
 *         &lt;element name="APORTA_Clase" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}APORTA_Clase" minOccurs="0"/>
 *         &lt;element name="APORTA_Aporte_Esap_y_Men" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}APORTA_Aporte_Esap_y_Men" minOccurs="0"/>
 *         &lt;element name="APORTA_Excepcion_Ley_1233_2008" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}APORTA_Excepcion_Ley_1233_2008" minOccurs="0"/>
 *         &lt;element name="COTIZ_id" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Tipo_Documento" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Numero_Identificacion" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZ_Numero_Identificacion" minOccurs="0"/>
 *         &lt;element name="COTIZ_Nombre" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZ_Nombre" minOccurs="0"/>
 *         &lt;element name="COTIZ_Tipo_Cotizante" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Subtipo_Cotizante" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Extranjero_No_Cotizar" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZ_Extranjero_No_Cotizar" minOccurs="0"/>
 *         &lt;element name="COTIZ_Colombiano_En_El_Ext" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZ_Colombiano_En_El_Ext" minOccurs="0"/>
 *         &lt;element name="COTIZ_Actividad_Alto_Riesgo_Pe" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZ_Actividad_Alto_Riesgo_Pe" minOccurs="0"/>
 *         &lt;element name="COTIZD_ano" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_mes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ing" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_ing" minOccurs="0"/>
 *         &lt;element name="COTIZD_ing_fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_ret" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_ret" minOccurs="0"/>
 *         &lt;element name="COTIZD_ret_fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_sln" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_sln" minOccurs="0"/>
 *         &lt;element name="COTIZD_causal_suspension" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_causal_suspension" minOccurs="0"/>
 *         &lt;element name="COTIZD_causal_susp_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_causal_susp_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_suspension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ige" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_ige" minOccurs="0"/>
 *         &lt;element name="COTIZD_ige_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_ige_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_incapacidad_general" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_lma" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_lma" minOccurs="0"/>
 *         &lt;element name="COTIZD_lma_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_lma_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_licencia_maternidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_vac" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_vac" minOccurs="0"/>
 *         &lt;element name="COTIZD_vac_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime" maxOccurs="unbounded"/>
 *         &lt;element name="dias_vacaciones" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_irp" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_irp" minOccurs="0"/>
 *         &lt;element name="COTIZD_irp_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_irp_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_incapacidad_rp" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_salario_integral" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_salario_integral" minOccurs="0"/>
 *         &lt;element name="dias_laborados" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ultimo_ibc_sin_novedad" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}COTIZD_ultimo_ibc_sin_novedad" minOccurs="0"/>
 *         &lt;element name="ing_pila_AT" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}ing_pila_AT" minOccurs="0"/>
 *         &lt;element name="ret_pila_AU" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}ret_pila_AU" minOccurs="0"/>
 *         &lt;element name="sln_pila_AV" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}sln_pila_AV" minOccurs="0"/>
 *         &lt;element name="ige_pila_AU" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}ige_pila_AU" minOccurs="0"/>
 *         &lt;element name="lma_pila_AU" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}lma_pila_AU" minOccurs="0"/>
 *         &lt;element name="vac_pila_AU" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}vac_pila_AU" minOccurs="0"/>
 *         &lt;element name="irp_pila_AU" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}irp_pila_AU" minOccurs="0"/>
 *         &lt;element name="porcentaje_pagos_no_salaria_BB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="excede_limite_pago_no_salar_BC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo_administradora_BD" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}codigo_administradora_BD" minOccurs="0"/>
 *         &lt;element name="nombre_corto_administradora_BE" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre_corto_administradora_BE" minOccurs="0"/>
 *         &lt;element name="ibc_core_BF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_BG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_BH" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_BI" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_BJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_BK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_BL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_BM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_BN" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_BN" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_BO" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_BO" minOccurs="0"/>
 *         &lt;element name="codigo_administradora_BP" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}codigo_administradora_BP" minOccurs="0"/>
 *         &lt;element name="nombre_corto_administradora_BQ" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre_corto_administradora_BQ" minOccurs="0"/>
 *         &lt;element name="ibc_core_BR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_pension_BS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligato_a_pensi_BT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_fsp_BU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_fsp_BV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_pension_adic_ar_BW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obli_pen_adic_ar_BX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_BY" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_BZ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pension_CA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pension_CB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_pension_CC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_pension_CD" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_pension_CD" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_fsp_CE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_fsp_CF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_fsp_CG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_fsp_CH" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_fsp_CH" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_pensi_adicio_ar_CI" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pag_pens_adic_ar_CJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_pension_adicional_ar_CK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_pension_ar_CL" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_pension_ar_CL" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_CM" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_CM" minOccurs="0"/>
 *         &lt;element name="calculo_actuarial_CN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo_administradora_CO" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}codigo_administradora_CO" minOccurs="0"/>
 *         &lt;element name="nombre_corto_administradora_CP" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre_corto_administradora_CP" minOccurs="0"/>
 *         &lt;element name="ibc_core_CQ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_CR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_CS" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_CT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_CU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_CV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_CW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_CX" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_CX" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_CY" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_CY" minOccurs="0"/>
 *         &lt;element name="nombre_corto_administradora_CZ" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre_corto_administradora_CZ" minOccurs="0"/>
 *         &lt;element name="ibc_core_DA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_DC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_DD" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_DE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_DG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DH" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DI" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_DI" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_DJ" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_DJ" minOccurs="0"/>
 *         &lt;element name="ibc_core_DK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_DM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_DO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DP" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DQ" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_DQ" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_DR" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_DR" minOccurs="0"/>
 *         &lt;element name="ibc_core_DS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_DU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_DW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DY" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_DY" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_DZ" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_DZ" minOccurs="0"/>
 *         &lt;element name="ibc_EA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_EB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_ED" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_EE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_EF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_EG" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_EG" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_EH" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_EH" minOccurs="0"/>
 *         &lt;element name="ibc_core_EI" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_EJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_EL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_EN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_EO" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}concepto_ajuste_EO" minOccurs="0"/>
 *         &lt;element name="tipo_incumplimiento_EP" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}tipo_incumplimiento_EP" minOccurs="0"/>
 *         &lt;element name="ConceptoContableTipo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/ConceptoContableTipo/v1}ConceptoContableTipo" maxOccurs="unbounded"/>
 *         &lt;element name="AportesIndependienteTipo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}AportesIndependienteTipo" maxOccurs="unbounded"/>
 *         &lt;element name="ContextoRespuestaTipo" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetalleHojaCalculoTipo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/DetalleHojaCalculoTipo/v1", propOrder = {
    "idHojaCalculoLiquidacionDetalle",
    "idNominaDetalle",
    "idPilaDetalle",
    "idConciliacionContableDetalle",
    "aportaId",
    "aportaTipoIdentificacion",
    "aportaNumeroIdentificacion",
    "aportaPrimerNombre",
    "aportaClase",
    "aportaAporteEsapYMen",
    "aportaExcepcionLey12332008",
    "cotizId",
    "cotizTipoDocumento",
    "cotizNumeroIdentificacion",
    "cotizNombre",
    "cotizTipoCotizante",
    "cotizSubtipoCotizante",
    "cotizExtranjeroNoCotizar",
    "cotizColombianoEnElExt",
    "cotizActividadAltoRiesgoPe",
    "cotizdAno",
    "cotizdMes",
    "cotizdIng",
    "cotizdIngFecha",
    "cotizdRet",
    "cotizdRetFecha",
    "cotizdSln",
    "cotizdCausalSuspension",
    "cotizdCausalSuspFInicial",
    "cotizdCausalSuspFFinal",
    "diasSuspension",
    "cotizdIge",
    "cotizdIgeFInicial",
    "cotizdIgeFFinal",
    "diasIncapacidadGeneral",
    "cotizdLma",
    "cotizdLmaFInicial",
    "cotizdLmaFFinal",
    "diasLicenciaMaternidad",
    "cotizdVac",
    "cotizdVacFInicial",
    "diasVacaciones",
    "cotizdIrp",
    "cotizdIrpFInicial",
    "cotizdIrpFFinal",
    "diasIncapacidadRp",
    "cotizdSalarioIntegral",
    "diasLaborados",
    "cotizdUltimoIbcSinNovedad",
    "ingPilaAT",
    "retPilaAU",
    "slnPilaAV",
    "igePilaAU",
    "lmaPilaAU",
    "vacPilaAU",
    "irpPilaAU",
    "porcentajePagosNoSalariaBB",
    "excedeLimitePagoNoSalarBC",
    "codigoAdministradoraBD",
    "nombreCortoAdministradoraBE",
    "ibcCoreBF",
    "tarifaCoreBG",
    "cotizacionObligatoriaBH",
    "diasCotizadosPilaBI",
    "ibcPilaBJ",
    "tarifaPilaBK",
    "cotizacionPagadaPilaBL",
    "ajusteBM",
    "conceptoAjusteBN",
    "tipoIncumplimientoBO",
    "codigoAdministradoraBP",
    "nombreCortoAdministradoraBQ",
    "ibcCoreBR",
    "tarifaCorePensionBS",
    "cotizacionObligatoAPensiBT",
    "tarifaCoreFspBU",
    "cotizacionObligatoriaFspBV",
    "tarifaCorePensionAdicArBW",
    "cotizacionObliPenAdicArBX",
    "diasCotizadosPilaBY",
    "ibcPilaBZ",
    "tarifaPensionCA",
    "cotizacionPagadaPensionCB",
    "ajustePensionCC",
    "conceptoAjustePensionCD",
    "tarifaPilaFspCE",
    "cotizacionPagadaFspCF",
    "ajusteFspCG",
    "conceptoAjusteFspCH",
    "tarifaPilaPensiAdicioArCI",
    "cotizacionPagPensAdicArCJ",
    "ajustePensionAdicionalArCK",
    "conceptoAjustePensionArCL",
    "tipoIncumplimientoCM",
    "calculoActuarialCN",
    "codigoAdministradoraCO",
    "nombreCortoAdministradoraCP",
    "ibcCoreCQ",
    "cotizacionObligatoriaCR",
    "diasCotizadosPilaCS",
    "ibcPilaCT",
    "tarifaPilaCU",
    "cotizacionPagadaPilaCV",
    "ajusteCW",
    "conceptoAjusteCX",
    "tipoIncumplimientoCY",
    "nombreCortoAdministradoraCZ",
    "ibcCoreDA",
    "tarifaCoreDB",
    "cotizacionObligatoriaDC",
    "diasCotizadosPilaDD",
    "ibcPilaDE",
    "tarifaPilaDF",
    "cotizacionPagadaPilaDG",
    "ajusteDH",
    "conceptoAjusteDI",
    "tipoIncumplimientoDJ",
    "ibcCoreDK",
    "tarifaCoreDL",
    "aporteDM",
    "tarifaPilaDN",
    "aportePilaDO",
    "ajusteDP",
    "conceptoAjusteDQ",
    "tipoIncumplimientoDR",
    "ibcCoreDS",
    "tarifaCoreDT",
    "aporteDU",
    "tarifaPilaDV",
    "aportePilaDW",
    "ajusteDX",
    "conceptoAjusteDY",
    "tipoIncumplimientoDZ",
    "ibcEA",
    "tarifaCoreEB",
    "aporteEC",
    "tarifaPilaED",
    "aportePilaEE",
    "ajusteEF",
    "conceptoAjusteEG",
    "tipoIncumplimientoEH",
    "ibcCoreEI",
    "tarifaCoreEJ",
    "aporteEK",
    "tarifaPilaEL",
    "aporteEM",
    "ajusteEN",
    "conceptoAjusteEO",
    "tipoIncumplimientoEP",
    "conceptoContableTipo",
    "aportesIndependienteTipo",
    "contextoRespuestaTipo"
})
public class DetalleHojaCalculoTipo {

    @XmlElement(nillable = true)
    protected String idHojaCalculoLiquidacionDetalle;
    @XmlElement(nillable = true)
    protected String idNominaDetalle;
    @XmlElement(nillable = true)
    protected String idPilaDetalle;
    @XmlElement(nillable = true)
    protected String idConciliacionContableDetalle;
    @XmlElement(name = "APORTA_id", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long aportaId;
    @XmlElement(name = "APORTA_Tipo_Identificacion", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long aportaTipoIdentificacion;
    @XmlElement(name = "APORTA_Numero_Identificacion", nillable = true)
    protected String aportaNumeroIdentificacion;
    @XmlElement(name = "APORTA_Primer_Nombre", nillable = true)
    protected String aportaPrimerNombre;
    @XmlElement(name = "APORTA_Clase", nillable = true)
    protected String aportaClase;
    @XmlElement(name = "APORTA_Aporte_Esap_y_Men", nillable = true)
    protected String aportaAporteEsapYMen;
    @XmlElement(name = "APORTA_Excepcion_Ley_1233_2008", nillable = true)
    protected String aportaExcepcionLey12332008;
    @XmlElement(name = "COTIZ_id", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizId;
    @XmlElement(name = "COTIZ_Tipo_Documento", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizTipoDocumento;
    @XmlElement(name = "COTIZ_Numero_Identificacion", nillable = true)
    protected String cotizNumeroIdentificacion;
    @XmlElement(name = "COTIZ_Nombre", nillable = true)
    protected String cotizNombre;
    @XmlElement(name = "COTIZ_Tipo_Cotizante", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizTipoCotizante;
    @XmlElement(name = "COTIZ_Subtipo_Cotizante", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizSubtipoCotizante;
    @XmlElement(name = "COTIZ_Extranjero_No_Cotizar", nillable = true)
    protected String cotizExtranjeroNoCotizar;
    @XmlElement(name = "COTIZ_Colombiano_En_El_Ext", nillable = true)
    protected String cotizColombianoEnElExt;
    @XmlElement(name = "COTIZ_Actividad_Alto_Riesgo_Pe", nillable = true)
    protected String cotizActividadAltoRiesgoPe;
    @XmlElement(name = "COTIZD_ano", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizdAno;
    @XmlElement(name = "COTIZD_mes", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizdMes;
    @XmlElement(name = "COTIZD_ing", nillable = true)
    protected String cotizdIng;
    @XmlElement(name = "COTIZD_ing_fecha", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIngFecha;
    @XmlElement(name = "COTIZD_ret", nillable = true)
    protected String cotizdRet;
    @XmlElement(name = "COTIZD_ret_fecha", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdRetFecha;
    @XmlElement(name = "COTIZD_sln", nillable = true)
    protected String cotizdSln;
    @XmlElement(name = "COTIZD_causal_suspension", nillable = true)
    protected String cotizdCausalSuspension;
    @XmlElement(name = "COTIZD_causal_susp_fInicial", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdCausalSuspFInicial;
    @XmlElement(name = "COTIZD_causal_susp_fFinal", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdCausalSuspFFinal;
    @XmlElement(name = "dias_suspension", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasSuspension;
    @XmlElement(name = "COTIZD_ige", nillable = true)
    protected String cotizdIge;
    @XmlElement(name = "COTIZD_ige_fInicial", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIgeFInicial;
    @XmlElement(name = "COTIZD_ige_fFinal", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIgeFFinal;
    @XmlElement(name = "dias_incapacidad_general", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasIncapacidadGeneral;
    @XmlElement(name = "COTIZD_lma", nillable = true)
    protected String cotizdLma;
    @XmlElement(name = "COTIZD_lma_fInicial", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdLmaFInicial;
    @XmlElement(name = "COTIZD_lma_fFinal", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdLmaFFinal;
    @XmlElement(name = "dias_licencia_maternidad", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasLicenciaMaternidad;
    @XmlElement(name = "COTIZD_vac", nillable = true)
    protected String cotizdVac;
    @XmlElement(name = "COTIZD_vac_fInicial", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected List<Calendar> cotizdVacFInicial;
    @XmlElement(name = "dias_vacaciones", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasVacaciones;
    @XmlElement(name = "COTIZD_irp", nillable = true)
    protected String cotizdIrp;
    @XmlElement(name = "COTIZD_irp_fInicial", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIrpFInicial;
    @XmlElement(name = "COTIZD_irp_fFinal", required = true, type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIrpFFinal;
    @XmlElement(name = "dias_incapacidad_rp", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasIncapacidadRp;
    @XmlElement(name = "COTIZD_salario_integral", nillable = true)
    protected String cotizdSalarioIntegral;
    @XmlElement(name = "dias_laborados", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasLaborados;
    @XmlElement(name = "COTIZD_ultimo_ibc_sin_novedad", nillable = true)
    protected String cotizdUltimoIbcSinNovedad;
    @XmlElement(name = "ing_pila_AT", nillable = true)
    protected String ingPilaAT;
    @XmlElement(name = "ret_pila_AU", nillable = true)
    protected String retPilaAU;
    @XmlElement(name = "sln_pila_AV", nillable = true)
    protected String slnPilaAV;
    @XmlElement(name = "ige_pila_AU", nillable = true)
    protected String igePilaAU;
    @XmlElement(name = "lma_pila_AU", nillable = true)
    protected String lmaPilaAU;
    @XmlElement(name = "vac_pila_AU", nillable = true)
    protected String vacPilaAU;
    @XmlElement(name = "irp_pila_AU", nillable = true)
    protected String irpPilaAU;
    @XmlElement(name = "porcentaje_pagos_no_salaria_BB", nillable = true)
    protected BigDecimal porcentajePagosNoSalariaBB;
    @XmlElement(name = "excede_limite_pago_no_salar_BC", nillable = true)
    protected BigDecimal excedeLimitePagoNoSalarBC;
    @XmlElement(name = "codigo_administradora_BD", nillable = true)
    protected String codigoAdministradoraBD;
    @XmlElement(name = "nombre_corto_administradora_BE", nillable = true)
    protected String nombreCortoAdministradoraBE;
    @XmlElement(name = "ibc_core_BF", nillable = true)
    protected BigDecimal ibcCoreBF;
    @XmlElement(name = "tarifa_core_BG", nillable = true)
    protected BigDecimal tarifaCoreBG;
    @XmlElement(name = "cotizacion_obligatoria_BH", nillable = true)
    protected BigDecimal cotizacionObligatoriaBH;
    @XmlElement(name = "dias_cotizados_pila_BI", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaBI;
    @XmlElement(name = "ibc_pila_BJ", nillable = true)
    protected BigDecimal ibcPilaBJ;
    @XmlElement(name = "tarifa_pila_BK", nillable = true)
    protected BigDecimal tarifaPilaBK;
    @XmlElement(name = "cotizacion_pagada_pila_BL", nillable = true)
    protected BigDecimal cotizacionPagadaPilaBL;
    @XmlElement(name = "ajuste_BM", nillable = true)
    protected BigDecimal ajusteBM;
    @XmlElement(name = "concepto_ajuste_BN", nillable = true)
    protected String conceptoAjusteBN;
    @XmlElement(name = "tipo_incumplimiento_BO", nillable = true)
    protected String tipoIncumplimientoBO;
    @XmlElement(name = "codigo_administradora_BP", nillable = true)
    protected String codigoAdministradoraBP;
    @XmlElement(name = "nombre_corto_administradora_BQ", nillable = true)
    protected String nombreCortoAdministradoraBQ;
    @XmlElement(name = "ibc_core_BR", nillable = true)
    protected BigDecimal ibcCoreBR;
    @XmlElement(name = "tarifa_core_pension_BS", nillable = true)
    protected BigDecimal tarifaCorePensionBS;
    @XmlElement(name = "cotizacion_obligato_a_pensi_BT", nillable = true)
    protected BigDecimal cotizacionObligatoAPensiBT;
    @XmlElement(name = "tarifa_core_fsp_BU", nillable = true)
    protected BigDecimal tarifaCoreFspBU;
    @XmlElement(name = "cotizacion_obligatoria_fsp_BV", nillable = true)
    protected BigDecimal cotizacionObligatoriaFspBV;
    @XmlElement(name = "tarifa_core_pension_adic_ar_BW", nillable = true)
    protected BigDecimal tarifaCorePensionAdicArBW;
    @XmlElement(name = "cotizacion_obli_pen_adic_ar_BX", nillable = true)
    protected BigDecimal cotizacionObliPenAdicArBX;
    @XmlElement(name = "dias_cotizados_pila_BY", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaBY;
    @XmlElement(name = "ibc_pila_BZ", nillable = true)
    protected BigDecimal ibcPilaBZ;
    @XmlElement(name = "tarifa_pension_CA", nillable = true)
    protected BigDecimal tarifaPensionCA;
    @XmlElement(name = "cotizacion_pagada_pension_CB", nillable = true)
    protected BigDecimal cotizacionPagadaPensionCB;
    @XmlElement(name = "ajuste_pension_CC", nillable = true)
    protected BigDecimal ajustePensionCC;
    @XmlElement(name = "concepto_ajuste_pension_CD", nillable = true)
    protected String conceptoAjustePensionCD;
    @XmlElement(name = "tarifa_pila_fsp_CE", nillable = true)
    protected BigDecimal tarifaPilaFspCE;
    @XmlElement(name = "cotizacion_pagada_fsp_CF", nillable = true)
    protected BigDecimal cotizacionPagadaFspCF;
    @XmlElement(name = "ajuste_fsp_CG", nillable = true)
    protected BigDecimal ajusteFspCG;
    @XmlElement(name = "concepto_ajuste_fsp_CH", nillable = true)
    protected String conceptoAjusteFspCH;
    @XmlElement(name = "tarifa_pila_pensi_adicio_ar_CI", nillable = true)
    protected BigDecimal tarifaPilaPensiAdicioArCI;
    @XmlElement(name = "cotizacion_pag_pens_adic_ar_CJ", nillable = true)
    protected BigDecimal cotizacionPagPensAdicArCJ;
    @XmlElement(name = "ajuste_pension_adicional_ar_CK", nillable = true)
    protected BigDecimal ajustePensionAdicionalArCK;
    @XmlElement(name = "concepto_ajuste_pension_ar_CL", nillable = true)
    protected String conceptoAjustePensionArCL;
    @XmlElement(name = "tipo_incumplimiento_CM", nillable = true)
    protected String tipoIncumplimientoCM;
    @XmlElement(name = "calculo_actuarial_CN", nillable = true)
    protected BigDecimal calculoActuarialCN;
    @XmlElement(name = "codigo_administradora_CO", nillable = true)
    protected String codigoAdministradoraCO;
    @XmlElement(name = "nombre_corto_administradora_CP", nillable = true)
    protected String nombreCortoAdministradoraCP;
    @XmlElement(name = "ibc_core_CQ", nillable = true)
    protected BigDecimal ibcCoreCQ;
    @XmlElement(name = "cotizacion_obligatoria_CR", nillable = true)
    protected BigDecimal cotizacionObligatoriaCR;
    @XmlElement(name = "dias_cotizados_pila_CS", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaCS;
    @XmlElement(name = "ibc_pila_CT", nillable = true)
    protected BigDecimal ibcPilaCT;
    @XmlElement(name = "tarifa_pila_CU", nillable = true)
    protected BigDecimal tarifaPilaCU;
    @XmlElement(name = "cotizacion_pagada_pila_CV", nillable = true)
    protected BigDecimal cotizacionPagadaPilaCV;
    @XmlElement(name = "ajuste_CW", nillable = true)
    protected BigDecimal ajusteCW;
    @XmlElement(name = "concepto_ajuste_CX", nillable = true)
    protected String conceptoAjusteCX;
    @XmlElement(name = "tipo_incumplimiento_CY", nillable = true)
    protected String tipoIncumplimientoCY;
    @XmlElement(name = "nombre_corto_administradora_CZ", nillable = true)
    protected String nombreCortoAdministradoraCZ;
    @XmlElement(name = "ibc_core_DA", nillable = true)
    protected BigDecimal ibcCoreDA;
    @XmlElement(name = "tarifa_core_DB", nillable = true)
    protected BigDecimal tarifaCoreDB;
    @XmlElement(name = "cotizacion_obligatoria_DC", nillable = true)
    protected BigDecimal cotizacionObligatoriaDC;
    @XmlElement(name = "dias_cotizados_pila_DD", type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaDD;
    @XmlElement(name = "ibc_pila_DE", nillable = true)
    protected BigDecimal ibcPilaDE;
    @XmlElement(name = "tarifa_pila_DF", nillable = true)
    protected BigDecimal tarifaPilaDF;
    @XmlElement(name = "cotizacion_pagada_pila_DG", nillable = true)
    protected BigDecimal cotizacionPagadaPilaDG;
    @XmlElement(name = "ajuste_DH", nillable = true)
    protected BigDecimal ajusteDH;
    @XmlElement(name = "concepto_ajuste_DI", nillable = true)
    protected String conceptoAjusteDI;
    @XmlElement(name = "tipo_incumplimiento_DJ", nillable = true)
    protected String tipoIncumplimientoDJ;
    @XmlElement(name = "ibc_core_DK", nillable = true)
    protected BigDecimal ibcCoreDK;
    @XmlElement(name = "tarifa_core_DL", nillable = true)
    protected BigDecimal tarifaCoreDL;
    @XmlElement(name = "aporte_DM", nillable = true)
    protected BigDecimal aporteDM;
    @XmlElement(name = "tarifa_pila_DN", nillable = true)
    protected BigDecimal tarifaPilaDN;
    @XmlElement(name = "aporte_pila_DO", nillable = true)
    protected BigDecimal aportePilaDO;
    @XmlElement(name = "ajuste_DP", nillable = true)
    protected BigDecimal ajusteDP;
    @XmlElement(name = "concepto_ajuste_DQ", nillable = true)
    protected String conceptoAjusteDQ;
    @XmlElement(name = "tipo_incumplimiento_DR", nillable = true)
    protected String tipoIncumplimientoDR;
    @XmlElement(name = "ibc_core_DS", nillable = true)
    protected BigDecimal ibcCoreDS;
    @XmlElement(name = "tarifa_core_DT", nillable = true)
    protected BigDecimal tarifaCoreDT;
    @XmlElement(name = "aporte_DU", nillable = true)
    protected BigDecimal aporteDU;
    @XmlElement(name = "tarifa_pila_DV", nillable = true)
    protected BigDecimal tarifaPilaDV;
    @XmlElement(name = "aporte_pila_DW", nillable = true)
    protected BigDecimal aportePilaDW;
    @XmlElement(name = "ajuste_DX", nillable = true)
    protected BigDecimal ajusteDX;
    @XmlElement(name = "concepto_ajuste_DY", nillable = true)
    protected String conceptoAjusteDY;
    @XmlElement(name = "tipo_incumplimiento_DZ", nillable = true)
    protected String tipoIncumplimientoDZ;
    @XmlElement(name = "ibc_EA", nillable = true)
    protected BigDecimal ibcEA;
    @XmlElement(name = "tarifa_core_EB", nillable = true)
    protected BigDecimal tarifaCoreEB;
    @XmlElement(name = "aporte_EC", nillable = true)
    protected BigDecimal aporteEC;
    @XmlElement(name = "tarifa_pila_ED", nillable = true)
    protected BigDecimal tarifaPilaED;
    @XmlElement(name = "aporte_pila_EE", nillable = true)
    protected BigDecimal aportePilaEE;
    @XmlElement(name = "ajuste_EF", nillable = true)
    protected BigDecimal ajusteEF;
    @XmlElement(name = "concepto_ajuste_EG", nillable = true)
    protected String conceptoAjusteEG;
    @XmlElement(name = "tipo_incumplimiento_EH", nillable = true)
    protected String tipoIncumplimientoEH;
    @XmlElement(name = "ibc_core_EI", nillable = true)
    protected BigDecimal ibcCoreEI;
    @XmlElement(name = "tarifa_core_EJ", nillable = true)
    protected BigDecimal tarifaCoreEJ;
    @XmlElement(name = "aporte_EK", nillable = true)
    protected BigDecimal aporteEK;
    @XmlElement(name = "tarifa_pila_EL", nillable = true)
    protected BigDecimal tarifaPilaEL;
    @XmlElement(name = "aporte_EM", nillable = true)
    protected BigDecimal aporteEM;
    @XmlElement(name = "ajuste_EN", nillable = true)
    protected BigDecimal ajusteEN;
    @XmlElement(name = "concepto_ajuste_EO", nillable = true)
    protected String conceptoAjusteEO;
    @XmlElement(name = "tipo_incumplimiento_EP", nillable = true)
    protected String tipoIncumplimientoEP;
    @XmlElement(name = "ConceptoContableTipo", required = true, nillable = true)
    protected List<ConceptoContableTipo> conceptoContableTipo;
    @XmlElement(name = "AportesIndependienteTipo", required = true, nillable = true)
    protected List<AportesIndependienteTipo> aportesIndependienteTipo;
    @XmlElement(name = "ContextoRespuestaTipo", required = true, nillable = true)
    protected ContextoRespuestaTipo contextoRespuestaTipo;

    /**
     * Obtiene el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculoLiquidacionDetalle() {
        return idHojaCalculoLiquidacionDetalle;
    }

    /**
     * Define el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculoLiquidacionDetalle(String value) {
        this.idHojaCalculoLiquidacionDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idNominaDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNominaDetalle() {
        return idNominaDetalle;
    }

    /**
     * Define el valor de la propiedad idNominaDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNominaDetalle(String value) {
        this.idNominaDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idPilaDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPilaDetalle() {
        return idPilaDetalle;
    }

    /**
     * Define el valor de la propiedad idPilaDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPilaDetalle(String value) {
        this.idPilaDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idConciliacionContableDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConciliacionContableDetalle() {
        return idConciliacionContableDetalle;
    }

    /**
     * Define el valor de la propiedad idConciliacionContableDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConciliacionContableDetalle(String value) {
        this.idConciliacionContableDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAPORTAId() {
        return aportaId;
    }

    /**
     * Define el valor de la propiedad aportaId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAId(Long value) {
        this.aportaId = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaTipoIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAPORTATipoIdentificacion() {
        return aportaTipoIdentificacion;
    }

    /**
     * Define el valor de la propiedad aportaTipoIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTATipoIdentificacion(Long value) {
        this.aportaTipoIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaNumeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTANumeroIdentificacion() {
        return aportaNumeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad aportaNumeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTANumeroIdentificacion(String value) {
        this.aportaNumeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaPrimerNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAPrimerNombre() {
        return aportaPrimerNombre;
    }

    /**
     * Define el valor de la propiedad aportaPrimerNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAPrimerNombre(String value) {
        this.aportaPrimerNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaClase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAClase() {
        return aportaClase;
    }

    /**
     * Define el valor de la propiedad aportaClase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAClase(String value) {
        this.aportaClase = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaAporteEsapYMen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAAporteEsapYMen() {
        return aportaAporteEsapYMen;
    }

    /**
     * Define el valor de la propiedad aportaAporteEsapYMen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAAporteEsapYMen(String value) {
        this.aportaAporteEsapYMen = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaExcepcionLey12332008.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAExcepcionLey12332008() {
        return aportaExcepcionLey12332008;
    }

    /**
     * Define el valor de la propiedad aportaExcepcionLey12332008.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAExcepcionLey12332008(String value) {
        this.aportaExcepcionLey12332008 = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZId() {
        return cotizId;
    }

    /**
     * Define el valor de la propiedad cotizId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZId(Long value) {
        this.cotizId = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizTipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZTipoDocumento() {
        return cotizTipoDocumento;
    }

    /**
     * Define el valor de la propiedad cotizTipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZTipoDocumento(Long value) {
        this.cotizTipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizNumeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZNumeroIdentificacion() {
        return cotizNumeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad cotizNumeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZNumeroIdentificacion(String value) {
        this.cotizNumeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZNombre() {
        return cotizNombre;
    }

    /**
     * Define el valor de la propiedad cotizNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZNombre(String value) {
        this.cotizNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizTipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZTipoCotizante() {
        return cotizTipoCotizante;
    }

    /**
     * Define el valor de la propiedad cotizTipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZTipoCotizante(Long value) {
        this.cotizTipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizSubtipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZSubtipoCotizante() {
        return cotizSubtipoCotizante;
    }

    /**
     * Define el valor de la propiedad cotizSubtipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZSubtipoCotizante(Long value) {
        this.cotizSubtipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizExtranjeroNoCotizar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZExtranjeroNoCotizar() {
        return cotizExtranjeroNoCotizar;
    }

    /**
     * Define el valor de la propiedad cotizExtranjeroNoCotizar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZExtranjeroNoCotizar(String value) {
        this.cotizExtranjeroNoCotizar = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizColombianoEnElExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZColombianoEnElExt() {
        return cotizColombianoEnElExt;
    }

    /**
     * Define el valor de la propiedad cotizColombianoEnElExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZColombianoEnElExt(String value) {
        this.cotizColombianoEnElExt = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizActividadAltoRiesgoPe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZActividadAltoRiesgoPe() {
        return cotizActividadAltoRiesgoPe;
    }

    /**
     * Define el valor de la propiedad cotizActividadAltoRiesgoPe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZActividadAltoRiesgoPe(String value) {
        this.cotizActividadAltoRiesgoPe = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdAno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZDAno() {
        return cotizdAno;
    }

    /**
     * Define el valor de la propiedad cotizdAno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDAno(Long value) {
        this.cotizdAno = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdMes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZDMes() {
        return cotizdMes;
    }

    /**
     * Define el valor de la propiedad cotizdMes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDMes(Long value) {
        this.cotizdMes = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIng.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIng() {
        return cotizdIng;
    }

    /**
     * Define el valor de la propiedad cotizdIng.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIng(String value) {
        this.cotizdIng = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIngFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIngFecha() {
        return cotizdIngFecha;
    }

    /**
     * Define el valor de la propiedad cotizdIngFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIngFecha(Calendar value) {
        this.cotizdIngFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdRet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDRet() {
        return cotizdRet;
    }

    /**
     * Define el valor de la propiedad cotizdRet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDRet(String value) {
        this.cotizdRet = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdRetFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDRetFecha() {
        return cotizdRetFecha;
    }

    /**
     * Define el valor de la propiedad cotizdRetFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDRetFecha(Calendar value) {
        this.cotizdRetFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdSln.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDSln() {
        return cotizdSln;
    }

    /**
     * Define el valor de la propiedad cotizdSln.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDSln(String value) {
        this.cotizdSln = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDCausalSuspension() {
        return cotizdCausalSuspension;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspension(String value) {
        this.cotizdCausalSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDCausalSuspFInicial() {
        return cotizdCausalSuspFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspFInicial(Calendar value) {
        this.cotizdCausalSuspFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDCausalSuspFFinal() {
        return cotizdCausalSuspFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspFFinal(Calendar value) {
        this.cotizdCausalSuspFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasSuspension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasSuspension() {
        return diasSuspension;
    }

    /**
     * Define el valor de la propiedad diasSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasSuspension(Long value) {
        this.diasSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIge() {
        return cotizdIge;
    }

    /**
     * Define el valor de la propiedad cotizdIge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIge(String value) {
        this.cotizdIge = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIgeFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIgeFInicial() {
        return cotizdIgeFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdIgeFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIgeFInicial(Calendar value) {
        this.cotizdIgeFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIgeFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIgeFFinal() {
        return cotizdIgeFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdIgeFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIgeFFinal(Calendar value) {
        this.cotizdIgeFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasIncapacidadGeneral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasIncapacidadGeneral() {
        return diasIncapacidadGeneral;
    }

    /**
     * Define el valor de la propiedad diasIncapacidadGeneral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasIncapacidadGeneral(Long value) {
        this.diasIncapacidadGeneral = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDLma() {
        return cotizdLma;
    }

    /**
     * Define el valor de la propiedad cotizdLma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLma(String value) {
        this.cotizdLma = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLmaFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDLmaFInicial() {
        return cotizdLmaFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdLmaFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLmaFInicial(Calendar value) {
        this.cotizdLmaFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLmaFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDLmaFFinal() {
        return cotizdLmaFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdLmaFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLmaFFinal(Calendar value) {
        this.cotizdLmaFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasLicenciaMaternidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasLicenciaMaternidad() {
        return diasLicenciaMaternidad;
    }

    /**
     * Define el valor de la propiedad diasLicenciaMaternidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasLicenciaMaternidad(Long value) {
        this.diasLicenciaMaternidad = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdVac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDVac() {
        return cotizdVac;
    }

    /**
     * Define el valor de la propiedad cotizdVac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDVac(String value) {
        this.cotizdVac = value;
    }

    /**
     * Gets the value of the cotizdVacFInicial property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cotizdVacFInicial property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCOTIZDVacFInicial().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<Calendar> getCOTIZDVacFInicial() {
        if (cotizdVacFInicial == null) {
            cotizdVacFInicial = new ArrayList<Calendar>();
        }
        return this.cotizdVacFInicial;
    }

    /**
     * Obtiene el valor de la propiedad diasVacaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasVacaciones() {
        return diasVacaciones;
    }

    /**
     * Define el valor de la propiedad diasVacaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasVacaciones(Long value) {
        this.diasVacaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIrp() {
        return cotizdIrp;
    }

    /**
     * Define el valor de la propiedad cotizdIrp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrp(String value) {
        this.cotizdIrp = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrpFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIrpFInicial() {
        return cotizdIrpFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdIrpFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrpFInicial(Calendar value) {
        this.cotizdIrpFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrpFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIrpFFinal() {
        return cotizdIrpFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdIrpFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrpFFinal(Calendar value) {
        this.cotizdIrpFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasIncapacidadRp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasIncapacidadRp() {
        return diasIncapacidadRp;
    }

    /**
     * Define el valor de la propiedad diasIncapacidadRp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasIncapacidadRp(Long value) {
        this.diasIncapacidadRp = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdSalarioIntegral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDSalarioIntegral() {
        return cotizdSalarioIntegral;
    }

    /**
     * Define el valor de la propiedad cotizdSalarioIntegral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDSalarioIntegral(String value) {
        this.cotizdSalarioIntegral = value;
    }

    /**
     * Obtiene el valor de la propiedad diasLaborados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasLaborados() {
        return diasLaborados;
    }

    /**
     * Define el valor de la propiedad diasLaborados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasLaborados(Long value) {
        this.diasLaborados = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdUltimoIbcSinNovedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDUltimoIbcSinNovedad() {
        return cotizdUltimoIbcSinNovedad;
    }

    /**
     * Define el valor de la propiedad cotizdUltimoIbcSinNovedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDUltimoIbcSinNovedad(String value) {
        this.cotizdUltimoIbcSinNovedad = value;
    }

    /**
     * Obtiene el valor de la propiedad ingPilaAT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIngPilaAT() {
        return ingPilaAT;
    }

    /**
     * Define el valor de la propiedad ingPilaAT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIngPilaAT(String value) {
        this.ingPilaAT = value;
    }

    /**
     * Obtiene el valor de la propiedad retPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetPilaAU() {
        return retPilaAU;
    }

    /**
     * Define el valor de la propiedad retPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetPilaAU(String value) {
        this.retPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad slnPilaAV.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlnPilaAV() {
        return slnPilaAV;
    }

    /**
     * Define el valor de la propiedad slnPilaAV.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlnPilaAV(String value) {
        this.slnPilaAV = value;
    }

    /**
     * Obtiene el valor de la propiedad igePilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIgePilaAU() {
        return igePilaAU;
    }

    /**
     * Define el valor de la propiedad igePilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIgePilaAU(String value) {
        this.igePilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad lmaPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLmaPilaAU() {
        return lmaPilaAU;
    }

    /**
     * Define el valor de la propiedad lmaPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLmaPilaAU(String value) {
        this.lmaPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad vacPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVacPilaAU() {
        return vacPilaAU;
    }

    /**
     * Define el valor de la propiedad vacPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVacPilaAU(String value) {
        this.vacPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad irpPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIrpPilaAU() {
        return irpPilaAU;
    }

    /**
     * Define el valor de la propiedad irpPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIrpPilaAU(String value) {
        this.irpPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajePagosNoSalariaBB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPorcentajePagosNoSalariaBB() {
        return porcentajePagosNoSalariaBB;
    }

    /**
     * Define el valor de la propiedad porcentajePagosNoSalariaBB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPorcentajePagosNoSalariaBB(BigDecimal value) {
        this.porcentajePagosNoSalariaBB = value;
    }

    /**
     * Obtiene el valor de la propiedad excedeLimitePagoNoSalarBC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExcedeLimitePagoNoSalarBC() {
        return excedeLimitePagoNoSalarBC;
    }

    /**
     * Define el valor de la propiedad excedeLimitePagoNoSalarBC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExcedeLimitePagoNoSalarBC(BigDecimal value) {
        this.excedeLimitePagoNoSalarBC = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraBD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraBD() {
        return codigoAdministradoraBD;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraBD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraBD(String value) {
        this.codigoAdministradoraBD = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraBE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraBE() {
        return nombreCortoAdministradoraBE;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraBE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraBE(String value) {
        this.nombreCortoAdministradoraBE = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreBF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreBF() {
        return ibcCoreBF;
    }

    /**
     * Define el valor de la propiedad ibcCoreBF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreBF(BigDecimal value) {
        this.ibcCoreBF = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreBG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreBG() {
        return tarifaCoreBG;
    }

    /**
     * Define el valor de la propiedad tarifaCoreBG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreBG(BigDecimal value) {
        this.tarifaCoreBG = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaBH.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaBH() {
        return cotizacionObligatoriaBH;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaBH.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaBH(BigDecimal value) {
        this.cotizacionObligatoriaBH = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaBI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaBI() {
        return diasCotizadosPilaBI;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaBI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaBI(Long value) {
        this.diasCotizadosPilaBI = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaBJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaBJ() {
        return ibcPilaBJ;
    }

    /**
     * Define el valor de la propiedad ibcPilaBJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaBJ(BigDecimal value) {
        this.ibcPilaBJ = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaBK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaBK() {
        return tarifaPilaBK;
    }

    /**
     * Define el valor de la propiedad tarifaPilaBK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaBK(BigDecimal value) {
        this.tarifaPilaBK = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaBL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaBL() {
        return cotizacionPagadaPilaBL;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaBL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaBL(BigDecimal value) {
        this.cotizacionPagadaPilaBL = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteBM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteBM() {
        return ajusteBM;
    }

    /**
     * Define el valor de la propiedad ajusteBM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteBM(BigDecimal value) {
        this.ajusteBM = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteBN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteBN() {
        return conceptoAjusteBN;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteBN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteBN(String value) {
        this.conceptoAjusteBN = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoBO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoBO() {
        return tipoIncumplimientoBO;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoBO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoBO(String value) {
        this.tipoIncumplimientoBO = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraBP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraBP() {
        return codigoAdministradoraBP;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraBP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraBP(String value) {
        this.codigoAdministradoraBP = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraBQ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraBQ() {
        return nombreCortoAdministradoraBQ;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraBQ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraBQ(String value) {
        this.nombreCortoAdministradoraBQ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreBR.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreBR() {
        return ibcCoreBR;
    }

    /**
     * Define el valor de la propiedad ibcCoreBR.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreBR(BigDecimal value) {
        this.ibcCoreBR = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCorePensionBS.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCorePensionBS() {
        return tarifaCorePensionBS;
    }

    /**
     * Define el valor de la propiedad tarifaCorePensionBS.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCorePensionBS(BigDecimal value) {
        this.tarifaCorePensionBS = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoAPensiBT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoAPensiBT() {
        return cotizacionObligatoAPensiBT;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoAPensiBT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoAPensiBT(BigDecimal value) {
        this.cotizacionObligatoAPensiBT = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreFspBU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreFspBU() {
        return tarifaCoreFspBU;
    }

    /**
     * Define el valor de la propiedad tarifaCoreFspBU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreFspBU(BigDecimal value) {
        this.tarifaCoreFspBU = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaFspBV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaFspBV() {
        return cotizacionObligatoriaFspBV;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaFspBV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaFspBV(BigDecimal value) {
        this.cotizacionObligatoriaFspBV = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCorePensionAdicArBW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCorePensionAdicArBW() {
        return tarifaCorePensionAdicArBW;
    }

    /**
     * Define el valor de la propiedad tarifaCorePensionAdicArBW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCorePensionAdicArBW(BigDecimal value) {
        this.tarifaCorePensionAdicArBW = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObliPenAdicArBX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObliPenAdicArBX() {
        return cotizacionObliPenAdicArBX;
    }

    /**
     * Define el valor de la propiedad cotizacionObliPenAdicArBX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObliPenAdicArBX(BigDecimal value) {
        this.cotizacionObliPenAdicArBX = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaBY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaBY() {
        return diasCotizadosPilaBY;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaBY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaBY(Long value) {
        this.diasCotizadosPilaBY = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaBZ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaBZ() {
        return ibcPilaBZ;
    }

    /**
     * Define el valor de la propiedad ibcPilaBZ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaBZ(BigDecimal value) {
        this.ibcPilaBZ = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPensionCA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPensionCA() {
        return tarifaPensionCA;
    }

    /**
     * Define el valor de la propiedad tarifaPensionCA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPensionCA(BigDecimal value) {
        this.tarifaPensionCA = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPensionCB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPensionCB() {
        return cotizacionPagadaPensionCB;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPensionCB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPensionCB(BigDecimal value) {
        this.cotizacionPagadaPensionCB = value;
    }

    /**
     * Obtiene el valor de la propiedad ajustePensionCC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjustePensionCC() {
        return ajustePensionCC;
    }

    /**
     * Define el valor de la propiedad ajustePensionCC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjustePensionCC(BigDecimal value) {
        this.ajustePensionCC = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjustePensionCD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjustePensionCD() {
        return conceptoAjustePensionCD;
    }

    /**
     * Define el valor de la propiedad conceptoAjustePensionCD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjustePensionCD(String value) {
        this.conceptoAjustePensionCD = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaFspCE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaFspCE() {
        return tarifaPilaFspCE;
    }

    /**
     * Define el valor de la propiedad tarifaPilaFspCE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaFspCE(BigDecimal value) {
        this.tarifaPilaFspCE = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaFspCF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaFspCF() {
        return cotizacionPagadaFspCF;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaFspCF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaFspCF(BigDecimal value) {
        this.cotizacionPagadaFspCF = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteFspCG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteFspCG() {
        return ajusteFspCG;
    }

    /**
     * Define el valor de la propiedad ajusteFspCG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteFspCG(BigDecimal value) {
        this.ajusteFspCG = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteFspCH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteFspCH() {
        return conceptoAjusteFspCH;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteFspCH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteFspCH(String value) {
        this.conceptoAjusteFspCH = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaPensiAdicioArCI.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaPensiAdicioArCI() {
        return tarifaPilaPensiAdicioArCI;
    }

    /**
     * Define el valor de la propiedad tarifaPilaPensiAdicioArCI.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaPensiAdicioArCI(BigDecimal value) {
        this.tarifaPilaPensiAdicioArCI = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagPensAdicArCJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagPensAdicArCJ() {
        return cotizacionPagPensAdicArCJ;
    }

    /**
     * Define el valor de la propiedad cotizacionPagPensAdicArCJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagPensAdicArCJ(BigDecimal value) {
        this.cotizacionPagPensAdicArCJ = value;
    }

    /**
     * Obtiene el valor de la propiedad ajustePensionAdicionalArCK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjustePensionAdicionalArCK() {
        return ajustePensionAdicionalArCK;
    }

    /**
     * Define el valor de la propiedad ajustePensionAdicionalArCK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjustePensionAdicionalArCK(BigDecimal value) {
        this.ajustePensionAdicionalArCK = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjustePensionArCL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjustePensionArCL() {
        return conceptoAjustePensionArCL;
    }

    /**
     * Define el valor de la propiedad conceptoAjustePensionArCL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjustePensionArCL(String value) {
        this.conceptoAjustePensionArCL = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoCM.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoCM() {
        return tipoIncumplimientoCM;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoCM.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoCM(String value) {
        this.tipoIncumplimientoCM = value;
    }

    /**
     * Obtiene el valor de la propiedad calculoActuarialCN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalculoActuarialCN() {
        return calculoActuarialCN;
    }

    /**
     * Define el valor de la propiedad calculoActuarialCN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalculoActuarialCN(BigDecimal value) {
        this.calculoActuarialCN = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraCO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraCO() {
        return codigoAdministradoraCO;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraCO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraCO(String value) {
        this.codigoAdministradoraCO = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraCP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraCP() {
        return nombreCortoAdministradoraCP;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraCP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraCP(String value) {
        this.nombreCortoAdministradoraCP = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreCQ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreCQ() {
        return ibcCoreCQ;
    }

    /**
     * Define el valor de la propiedad ibcCoreCQ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreCQ(BigDecimal value) {
        this.ibcCoreCQ = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaCR.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaCR() {
        return cotizacionObligatoriaCR;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaCR.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaCR(BigDecimal value) {
        this.cotizacionObligatoriaCR = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaCS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaCS() {
        return diasCotizadosPilaCS;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaCS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaCS(Long value) {
        this.diasCotizadosPilaCS = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaCT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaCT() {
        return ibcPilaCT;
    }

    /**
     * Define el valor de la propiedad ibcPilaCT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaCT(BigDecimal value) {
        this.ibcPilaCT = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaCU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaCU() {
        return tarifaPilaCU;
    }

    /**
     * Define el valor de la propiedad tarifaPilaCU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaCU(BigDecimal value) {
        this.tarifaPilaCU = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaCV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaCV() {
        return cotizacionPagadaPilaCV;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaCV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaCV(BigDecimal value) {
        this.cotizacionPagadaPilaCV = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteCW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteCW() {
        return ajusteCW;
    }

    /**
     * Define el valor de la propiedad ajusteCW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteCW(BigDecimal value) {
        this.ajusteCW = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteCX.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteCX() {
        return conceptoAjusteCX;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteCX.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteCX(String value) {
        this.conceptoAjusteCX = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoCY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoCY() {
        return tipoIncumplimientoCY;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoCY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoCY(String value) {
        this.tipoIncumplimientoCY = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraCZ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraCZ() {
        return nombreCortoAdministradoraCZ;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraCZ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraCZ(String value) {
        this.nombreCortoAdministradoraCZ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDA() {
        return ibcCoreDA;
    }

    /**
     * Define el valor de la propiedad ibcCoreDA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDA(BigDecimal value) {
        this.ibcCoreDA = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDB() {
        return tarifaCoreDB;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDB(BigDecimal value) {
        this.tarifaCoreDB = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaDC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaDC() {
        return cotizacionObligatoriaDC;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaDC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaDC(BigDecimal value) {
        this.cotizacionObligatoriaDC = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaDD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaDD() {
        return diasCotizadosPilaDD;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaDD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaDD(Long value) {
        this.diasCotizadosPilaDD = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaDE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaDE() {
        return ibcPilaDE;
    }

    /**
     * Define el valor de la propiedad ibcPilaDE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaDE(BigDecimal value) {
        this.ibcPilaDE = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDF() {
        return tarifaPilaDF;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDF(BigDecimal value) {
        this.tarifaPilaDF = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaDG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaDG() {
        return cotizacionPagadaPilaDG;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaDG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaDG(BigDecimal value) {
        this.cotizacionPagadaPilaDG = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDH.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDH() {
        return ajusteDH;
    }

    /**
     * Define el valor de la propiedad ajusteDH.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDH(BigDecimal value) {
        this.ajusteDH = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDI() {
        return conceptoAjusteDI;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDI(String value) {
        this.conceptoAjusteDI = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDJ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDJ() {
        return tipoIncumplimientoDJ;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDJ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDJ(String value) {
        this.tipoIncumplimientoDJ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDK() {
        return ibcCoreDK;
    }

    /**
     * Define el valor de la propiedad ibcCoreDK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDK(BigDecimal value) {
        this.ibcCoreDK = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDL() {
        return tarifaCoreDL;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDL(BigDecimal value) {
        this.tarifaCoreDL = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteDM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteDM() {
        return aporteDM;
    }

    /**
     * Define el valor de la propiedad aporteDM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteDM(BigDecimal value) {
        this.aporteDM = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDN() {
        return tarifaPilaDN;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDN(BigDecimal value) {
        this.tarifaPilaDN = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaDO.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaDO() {
        return aportePilaDO;
    }

    /**
     * Define el valor de la propiedad aportePilaDO.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaDO(BigDecimal value) {
        this.aportePilaDO = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDP.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDP() {
        return ajusteDP;
    }

    /**
     * Define el valor de la propiedad ajusteDP.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDP(BigDecimal value) {
        this.ajusteDP = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDQ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDQ() {
        return conceptoAjusteDQ;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDQ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDQ(String value) {
        this.conceptoAjusteDQ = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDR() {
        return tipoIncumplimientoDR;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDR(String value) {
        this.tipoIncumplimientoDR = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDS.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDS() {
        return ibcCoreDS;
    }

    /**
     * Define el valor de la propiedad ibcCoreDS.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDS(BigDecimal value) {
        this.ibcCoreDS = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDT() {
        return tarifaCoreDT;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDT(BigDecimal value) {
        this.tarifaCoreDT = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteDU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteDU() {
        return aporteDU;
    }

    /**
     * Define el valor de la propiedad aporteDU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteDU(BigDecimal value) {
        this.aporteDU = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDV() {
        return tarifaPilaDV;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDV(BigDecimal value) {
        this.tarifaPilaDV = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaDW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaDW() {
        return aportePilaDW;
    }

    /**
     * Define el valor de la propiedad aportePilaDW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaDW(BigDecimal value) {
        this.aportePilaDW = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDX() {
        return ajusteDX;
    }

    /**
     * Define el valor de la propiedad ajusteDX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDX(BigDecimal value) {
        this.ajusteDX = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDY() {
        return conceptoAjusteDY;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDY(String value) {
        this.conceptoAjusteDY = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDZ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDZ() {
        return tipoIncumplimientoDZ;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDZ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDZ(String value) {
        this.tipoIncumplimientoDZ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcEA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcEA() {
        return ibcEA;
    }

    /**
     * Define el valor de la propiedad ibcEA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcEA(BigDecimal value) {
        this.ibcEA = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreEB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreEB() {
        return tarifaCoreEB;
    }

    /**
     * Define el valor de la propiedad tarifaCoreEB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreEB(BigDecimal value) {
        this.tarifaCoreEB = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEC() {
        return aporteEC;
    }

    /**
     * Define el valor de la propiedad aporteEC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEC(BigDecimal value) {
        this.aporteEC = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaED.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaED() {
        return tarifaPilaED;
    }

    /**
     * Define el valor de la propiedad tarifaPilaED.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaED(BigDecimal value) {
        this.tarifaPilaED = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaEE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaEE() {
        return aportePilaEE;
    }

    /**
     * Define el valor de la propiedad aportePilaEE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaEE(BigDecimal value) {
        this.aportePilaEE = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteEF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteEF() {
        return ajusteEF;
    }

    /**
     * Define el valor de la propiedad ajusteEF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteEF(BigDecimal value) {
        this.ajusteEF = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteEG.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteEG() {
        return conceptoAjusteEG;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteEG.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteEG(String value) {
        this.conceptoAjusteEG = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoEH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoEH() {
        return tipoIncumplimientoEH;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoEH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoEH(String value) {
        this.tipoIncumplimientoEH = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreEI.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreEI() {
        return ibcCoreEI;
    }

    /**
     * Define el valor de la propiedad ibcCoreEI.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreEI(BigDecimal value) {
        this.ibcCoreEI = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreEJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreEJ() {
        return tarifaCoreEJ;
    }

    /**
     * Define el valor de la propiedad tarifaCoreEJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreEJ(BigDecimal value) {
        this.tarifaCoreEJ = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEK() {
        return aporteEK;
    }

    /**
     * Define el valor de la propiedad aporteEK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEK(BigDecimal value) {
        this.aporteEK = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaEL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaEL() {
        return tarifaPilaEL;
    }

    /**
     * Define el valor de la propiedad tarifaPilaEL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaEL(BigDecimal value) {
        this.tarifaPilaEL = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEM() {
        return aporteEM;
    }

    /**
     * Define el valor de la propiedad aporteEM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEM(BigDecimal value) {
        this.aporteEM = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteEN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteEN() {
        return ajusteEN;
    }

    /**
     * Define el valor de la propiedad ajusteEN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteEN(BigDecimal value) {
        this.ajusteEN = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteEO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteEO() {
        return conceptoAjusteEO;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteEO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteEO(String value) {
        this.conceptoAjusteEO = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoEP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoEP() {
        return tipoIncumplimientoEP;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoEP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoEP(String value) {
        this.tipoIncumplimientoEP = value;
    }

    /**
     * Gets the value of the conceptoContableTipo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptoContableTipo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptoContableTipo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptoContableTipo }
     * 
     * 
     */
    public List<ConceptoContableTipo> getConceptoContableTipo() {
        if (conceptoContableTipo == null) {
            conceptoContableTipo = new ArrayList<ConceptoContableTipo>();
        }
        return this.conceptoContableTipo;
    }

    /**
     * Gets the value of the aportesIndependienteTipo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aportesIndependienteTipo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAportesIndependienteTipo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AportesIndependienteTipo }
     * 
     * 
     */
    public List<AportesIndependienteTipo> getAportesIndependienteTipo() {
        if (aportesIndependienteTipo == null) {
            aportesIndependienteTipo = new ArrayList<AportesIndependienteTipo>();
        }
        return this.aportesIndependienteTipo;
    }

    /**
     * Obtiene el valor de la propiedad contextoRespuestaTipo.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuestaTipo() {
        return contextoRespuestaTipo;
    }

    /**
     * Define el valor de la propiedad contextoRespuestaTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuestaTipo(ContextoRespuestaTipo value) {
        this.contextoRespuestaTipo = value;
    }

}
