
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ExpedienteTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ExpedienteTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idNumExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSubSeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSubSeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serieDocumental" type="{http://www.ugpp.gov.co/schema/GestionDocumental/SerieDocumentalTipo/v1}SerieDocumentalTipo" minOccurs="0"/>
 *         &lt;element name="valAnoApertura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFondo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valEntidadPredecesora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificacion" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="descDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExpedienteTipo", namespace = "http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1", propOrder = {
    "idNumExpediente",
    "codSeccion",
    "valSeccion",
    "codSubSeccion",
    "valSubSeccion",
    "serieDocumental",
    "valAnoApertura",
    "valFondo",
    "valEntidadPredecesora",
    "identificacion",
    "descDescripcion"
})
public class ExpedienteTipo {

    @XmlElement(nillable = true)
    protected String idNumExpediente;
    @XmlElement(nillable = true)
    protected String codSeccion;
    @XmlElement(nillable = true)
    protected String valSeccion;
    @XmlElement(nillable = true)
    protected String codSubSeccion;
    @XmlElement(nillable = true)
    protected String valSubSeccion;
    @XmlElement(nillable = true)
    protected SerieDocumentalTipo serieDocumental;
    @XmlElement(nillable = true)
    protected String valAnoApertura;
    @XmlElement(nillable = true)
    protected String valFondo;
    @XmlElement(nillable = true)
    protected String valEntidadPredecesora;
    @XmlElement(nillable = true)
    protected IdentificacionTipo identificacion;
    @XmlElement(nillable = true)
    protected String descDescripcion;

    /**
     * Obtiene el valor de la propiedad idNumExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumExpediente() {
        return idNumExpediente;
    }

    /**
     * Define el valor de la propiedad idNumExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumExpediente(String value) {
        this.idNumExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codSeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSeccion() {
        return codSeccion;
    }

    /**
     * Define el valor de la propiedad codSeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSeccion(String value) {
        this.codSeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad valSeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSeccion() {
        return valSeccion;
    }

    /**
     * Define el valor de la propiedad valSeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSeccion(String value) {
        this.valSeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codSubSeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSubSeccion() {
        return codSubSeccion;
    }

    /**
     * Define el valor de la propiedad codSubSeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSubSeccion(String value) {
        this.codSubSeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad valSubSeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSubSeccion() {
        return valSubSeccion;
    }

    /**
     * Define el valor de la propiedad valSubSeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSubSeccion(String value) {
        this.valSubSeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad serieDocumental.
     * 
     * @return
     *     possible object is
     *     {@link SerieDocumentalTipo }
     *     
     */
    public SerieDocumentalTipo getSerieDocumental() {
        return serieDocumental;
    }

    /**
     * Define el valor de la propiedad serieDocumental.
     * 
     * @param value
     *     allowed object is
     *     {@link SerieDocumentalTipo }
     *     
     */
    public void setSerieDocumental(SerieDocumentalTipo value) {
        this.serieDocumental = value;
    }

    /**
     * Obtiene el valor de la propiedad valAnoApertura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValAnoApertura() {
        return valAnoApertura;
    }

    /**
     * Define el valor de la propiedad valAnoApertura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValAnoApertura(String value) {
        this.valAnoApertura = value;
    }

    /**
     * Obtiene el valor de la propiedad valFondo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFondo() {
        return valFondo;
    }

    /**
     * Define el valor de la propiedad valFondo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFondo(String value) {
        this.valFondo = value;
    }

    /**
     * Obtiene el valor de la propiedad valEntidadPredecesora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValEntidadPredecesora() {
        return valEntidadPredecesora;
    }

    /**
     * Define el valor de la propiedad valEntidadPredecesora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValEntidadPredecesora(String value) {
        this.valEntidadPredecesora = value;
    }

    /**
     * Obtiene el valor de la propiedad identificacion.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdentificacion() {
        return identificacion;
    }

    /**
     * Define el valor de la propiedad identificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdentificacion(IdentificacionTipo value) {
        this.identificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDescripcion() {
        return descDescripcion;
    }

    /**
     * Define el valor de la propiedad descDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDescripcion(String value) {
        this.descDescripcion = value;
    }

}
