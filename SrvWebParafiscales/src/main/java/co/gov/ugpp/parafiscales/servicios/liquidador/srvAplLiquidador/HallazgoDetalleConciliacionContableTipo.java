
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HallazgoDetalleConciliacionContableTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HallazgoDetalleConciliacionContableTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHallazgoConciliacionContableDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conceptoContable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valContabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDiferenciaPorcentaje" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDiferenciaPesos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFechaCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFechaModificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HallazgoDetalleConciliacionContableTipo", namespace = "http://www.ugpp.gov.co/schema/Transversales/HallazgoDetalleConciliacionContableTipo/v1", propOrder = {
    "idHallazgoConciliacionContableDetalle",
    "conceptoContable",
    "valNomina",
    "valContabilidad",
    "valDiferenciaPorcentaje",
    "valDiferenciaPesos",
    "esHallazgo",
    "descObservacionHallazgo",
    "descHallazgo",
    "valUsuarioCreacion",
    "valUsuarioModificacion",
    "valFechaCreacion",
    "valFechaModificacion"
})
public class HallazgoDetalleConciliacionContableTipo {

    protected String idHallazgoConciliacionContableDetalle;
    protected String conceptoContable;
    protected String valNomina;
    protected String valContabilidad;
    protected String valDiferenciaPorcentaje;
    protected String valDiferenciaPesos;
    protected String esHallazgo;
    protected String descObservacionHallazgo;
    protected String descHallazgo;
    protected String valUsuarioCreacion;
    protected String valUsuarioModificacion;
    protected String valFechaCreacion;
    protected String valFechaModificacion;

    /**
     * Obtiene el valor de la propiedad idHallazgoConciliacionContableDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHallazgoConciliacionContableDetalle() {
        return idHallazgoConciliacionContableDetalle;
    }

    /**
     * Define el valor de la propiedad idHallazgoConciliacionContableDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHallazgoConciliacionContableDetalle(String value) {
        this.idHallazgoConciliacionContableDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoContable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoContable() {
        return conceptoContable;
    }

    /**
     * Define el valor de la propiedad conceptoContable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoContable(String value) {
        this.conceptoContable = value;
    }

    /**
     * Obtiene el valor de la propiedad valNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNomina() {
        return valNomina;
    }

    /**
     * Define el valor de la propiedad valNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNomina(String value) {
        this.valNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad valContabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValContabilidad() {
        return valContabilidad;
    }

    /**
     * Define el valor de la propiedad valContabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValContabilidad(String value) {
        this.valContabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad valDiferenciaPorcentaje.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiferenciaPorcentaje() {
        return valDiferenciaPorcentaje;
    }

    /**
     * Define el valor de la propiedad valDiferenciaPorcentaje.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiferenciaPorcentaje(String value) {
        this.valDiferenciaPorcentaje = value;
    }

    /**
     * Obtiene el valor de la propiedad valDiferenciaPesos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiferenciaPesos() {
        return valDiferenciaPesos;
    }

    /**
     * Define el valor de la propiedad valDiferenciaPesos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiferenciaPesos(String value) {
        this.valDiferenciaPesos = value;
    }

    /**
     * Obtiene el valor de la propiedad esHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsHallazgo() {
        return esHallazgo;
    }

    /**
     * Define el valor de la propiedad esHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsHallazgo(String value) {
        this.esHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionHallazgo() {
        return descObservacionHallazgo;
    }

    /**
     * Define el valor de la propiedad descObservacionHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionHallazgo(String value) {
        this.descObservacionHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad descHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescHallazgo() {
        return descHallazgo;
    }

    /**
     * Define el valor de la propiedad descHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescHallazgo(String value) {
        this.descHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuarioCreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuarioCreacion() {
        return valUsuarioCreacion;
    }

    /**
     * Define el valor de la propiedad valUsuarioCreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuarioCreacion(String value) {
        this.valUsuarioCreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuarioModificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuarioModificacion() {
        return valUsuarioModificacion;
    }

    /**
     * Define el valor de la propiedad valUsuarioModificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuarioModificacion(String value) {
        this.valUsuarioModificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valFechaCreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFechaCreacion() {
        return valFechaCreacion;
    }

    /**
     * Define el valor de la propiedad valFechaCreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFechaCreacion(String value) {
        this.valFechaCreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valFechaModificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFechaModificacion() {
        return valFechaModificacion;
    }

    /**
     * Define el valor de la propiedad valFechaModificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFechaModificacion(String value) {
        this.valFechaModificacion = value;
    }

}
