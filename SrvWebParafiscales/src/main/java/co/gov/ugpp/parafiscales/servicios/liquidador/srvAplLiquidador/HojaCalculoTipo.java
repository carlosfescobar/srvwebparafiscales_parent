
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para HojaCalculoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HojaCalculoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHojaCalculoLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esIncumplimiento" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="detalleHojaCalculo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/DetalleHojaCalculoTipo/v1}DetalleHojaCalculoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HojaCalculoTipo", namespace = "http://www.ugpp.gov.co/schema/Liquidaciones/HojaCalculoTipo/v1", propOrder = {
    "idHojaCalculoLiquidacion",
    "codEstado",
    "desEstado",
    "esIncumplimiento",
    "detalleHojaCalculo"
})
public class HojaCalculoTipo {

    @XmlElement(required = true)
    protected String idHojaCalculoLiquidacion;
    @XmlElement(nillable = true)
    protected String codEstado;
    @XmlElement(nillable = true)
    protected String desEstado;
    protected Boolean esIncumplimiento;
    @XmlElement(nillable = true)
    protected List<DetalleHojaCalculoTipo> detalleHojaCalculo;

    /**
     * Obtiene el valor de la propiedad idHojaCalculoLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculoLiquidacion() {
        return idHojaCalculoLiquidacion;
    }

    /**
     * Define el valor de la propiedad idHojaCalculoLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculoLiquidacion(String value) {
        this.idHojaCalculoLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad desEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesEstado() {
        return desEstado;
    }

    /**
     * Define el valor de la propiedad desEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesEstado(String value) {
        this.desEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad esIncumplimiento.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isEsIncumplimiento() {
        return esIncumplimiento;
    }

    /**
     * Define el valor de la propiedad esIncumplimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setEsIncumplimiento(Boolean value) {
        this.esIncumplimiento = value;
    }

    /**
     * Gets the value of the detalleHojaCalculo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detalleHojaCalculo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetalleHojaCalculo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DetalleHojaCalculoTipo }
     * 
     * 
     */
    public List<DetalleHojaCalculoTipo> getDetalleHojaCalculo() {
        if (detalleHojaCalculo == null) {
            detalleHojaCalculo = new ArrayList<DetalleHojaCalculoTipo>();
        }
        return this.detalleHojaCalculo;
    }

}
