package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarSolTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import java.io.Serializable;

/**
 *
 * @author franzjr
 */
public interface LiquidadorFacade extends Serializable {

    public void liquidar(OpLiquidarSolTipo msjOpLiquidarSol)  throws AppException;

}
