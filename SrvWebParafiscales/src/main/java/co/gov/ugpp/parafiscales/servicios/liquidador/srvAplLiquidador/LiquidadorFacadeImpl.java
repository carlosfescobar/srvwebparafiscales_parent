package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarSolTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class LiquidadorFacadeImpl extends AbstractFacade implements LiquidadorFacade {

    @EJB
    private ExpedienteDao expedienteDao;
    
    //@EJB
    //private AportanteLIQDao aportanteDao;
    
    
    //@EJB
    //private PersonaDao personaDao;

    
    @Override
    public void liquidar(OpLiquidarSolTipo msjOpLiquidarSol) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<>();

        if (msjOpLiquidarSol.getExpediente() == null
                || StringUtils.isBlank(msjOpLiquidarSol.getExpediente().getIdNumExpediente())
                || msjOpLiquidarSol.getIdentificacion() == null
                || StringUtils.isBlank(msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion())
                || StringUtils.isBlank(msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion())) {

            
            //System.out.println("ERROR EXCEPTION opLiquidar: Los campos idExpediente, tipoIdentificacion, numero identificacion son obligatorios");
            
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "Los campos idExpediente, tipoIdentificacion, numero identificacion son obligatorios"));
            throw new AppException(errorTipoList);
        } 
        else 
        {
            Expediente expediente = expedienteDao.find(msjOpLiquidarSol.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                
              //System.out.println("ERROR EXCEPTION opLiquidar: El expediente proporcionado no existe con el ID:" + msjOpLiquidarSol.getExpediente().getIdNumExpediente());
              errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,"El expediente proporcionado no existe con el ID: " + msjOpLiquidarSol.getExpediente().getIdNumExpediente()));
              throw new AppException(errorTipoList);
            }

            
                
            /*  ANDRES -    pendiente por organizar
                System.out.println("::ANDRES:: getValNumeroIdentificacion: " + msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion());
                System.out.println("::ANDRES:: getCodTipoIdentificacion: " + Long.parseLong(msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion()));

                Long numeroAportantes = aportanteDao.findByIdentificacionAndTipo(msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion(),Long.parseLong(msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion()));
  
                
                System.out.println("::ANDRES:: numeroAportantes: " + numeroAportantes);

                
                
                if (numeroAportantes == 0) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encuentra el aportante liquidador con los valores enviados: " + msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion() + " " + msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion()));
                }
                
                System.exit(1);
                
            */    
                

            
                // POR PROBAR LAS PERSONAS INDEPENDIENTES SE VERIFICABA LA TABLA-PERSONA, PEOR EN NOMINA SE SUBE ES EN LIQ-APORTANTE
                // SE DECIDE QUITAR ESTA COMPROBACION PARA PODER SUBIR LAS NOMINAS DE INDEPENDIENTES 
                /*
                Identificacion identificacion = new Identificacion(msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion(), msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion());
                Persona persona = personaDao.findByIdentificacion(identificacion);

                if (persona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La persona con los valores enviados no se encuentra: " + msjOpLiquidarSol.getIdentificacion().getCodTipoIdentificacion() + " " + msjOpLiquidarSol.getIdentificacion().getValNumeroIdentificacion()));
                }
                */
                
            /*
            if (!errorTipoList.isEmpty()) {

                System.out.println("ERROR EXCEPTION opLiquidar : " + errorTipoList.toString());

                throw new AppException(errorTipoList);
            }
             */
        }

    }

}
