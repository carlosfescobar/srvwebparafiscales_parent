
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para msjOpRecibirHallazgosCruceConciliacionContableNominaFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpRecibirHallazgosCruceConciliacionContableNominaFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceConciliacionContableNominaFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpRecibirHallazgosCruceConciliacionContableNominaFallo", propOrder = {
    "opRecibirHallazgosCruceConciliacionContableNominaFallo"
})
public class MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo {

    @XmlElement(name = "OpRecibirHallazgosCruceConciliacionContableNominaFallo", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected FalloTipo opRecibirHallazgosCruceConciliacionContableNominaFallo;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpRecibirHallazgosCruceConciliacionContableNominaFallo() {
        return opRecibirHallazgosCruceConciliacionContableNominaFallo;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceConciliacionContableNominaFallo(FalloTipo value) {
        this.opRecibirHallazgosCruceConciliacionContableNominaFallo = value;
    }

}
