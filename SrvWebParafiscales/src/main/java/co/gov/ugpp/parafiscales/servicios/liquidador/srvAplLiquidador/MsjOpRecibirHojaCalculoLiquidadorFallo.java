
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para msjOpRecibirHojaCalculoLiquidadorFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpRecibirHojaCalculoLiquidadorFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHojaCalculoLiquidadorFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpRecibirHojaCalculoLiquidadorFallo", propOrder = {
    "opRecibirHojaCalculoLiquidadorFallo"
})
public class MsjOpRecibirHojaCalculoLiquidadorFallo {

    @XmlElement(name = "OpRecibirHojaCalculoLiquidadorFallo", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected FalloTipo opRecibirHojaCalculoLiquidadorFallo;

    /**
     * Obtiene el valor de la propiedad opRecibirHojaCalculoLiquidadorFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpRecibirHojaCalculoLiquidadorFallo() {
        return opRecibirHojaCalculoLiquidadorFallo;
    }

    /**
     * Define el valor de la propiedad opRecibirHojaCalculoLiquidadorFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpRecibirHojaCalculoLiquidadorFallo(FalloTipo value) {
        this.opRecibirHojaCalculoLiquidadorFallo = value;
    }

}
