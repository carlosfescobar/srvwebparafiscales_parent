
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para MunicipioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MunicipioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codMunicipio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="departamento" type="{http://www.ugpp.gov.co/esb/schema/DepartamentoTipo/v1}DepartamentoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MunicipioTipo", namespace = "http://www.ugpp.gov.co/esb/schema/MunicipioTipo/v1", propOrder = {
    "codMunicipio",
    "valNombre",
    "departamento"
})
public class MunicipioTipo {

    @XmlElement(nillable = true)
    protected String codMunicipio;
    @XmlElement(nillable = true)
    protected String valNombre;
    @XmlElement(nillable = true)
    protected DepartamentoTipo departamento;

    /**
     * Obtiene el valor de la propiedad codMunicipio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMunicipio() {
        return codMunicipio;
    }

    /**
     * Define el valor de la propiedad codMunicipio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMunicipio(String value) {
        this.codMunicipio = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombre() {
        return valNombre;
    }

    /**
     * Define el valor de la propiedad valNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombre(String value) {
        this.valNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad departamento.
     * 
     * @return
     *     possible object is
     *     {@link DepartamentoTipo }
     *     
     */
    public DepartamentoTipo getDepartamento() {
        return departamento;
    }

    /**
     * Define el valor de la propiedad departamento.
     * 
     * @param value
     *     allowed object is
     *     {@link DepartamentoTipo }
     *     
     */
    public void setDepartamento(DepartamentoTipo value) {
        this.departamento = value;
    }

}
