
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Output");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Input");
    private final static QName _OpRecibirNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirNominaFallo");
    private final static QName _OpRecibirNominaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirNominaSol");
    private final static QName _OpRecibirHallazgosCruceNominaPILASol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceNominaPILASol");
    private final static QName _OpRecibirHallazgosCruceConciliacionContableNominaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceConciliacionContableNominaResp");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Output");
    private final static QName _MsjOpRecibirHallazgosCruceNominaPILAFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "msjOpRecibirHallazgosCruceNominaPILAFallo");
    private final static QName _OpRecibirHallazgosCruceNominaPILAResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceNominaPILAResp");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Output");
    private final static QName _MsjOpRecibirNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "msjOpRecibirNominaFallo");
    private final static QName _MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "msjOpRecibirHallazgosCruceConciliacionContableNominaFallo");
    private final static QName _OpRecibirHojaCalculoLiquidadorSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHojaCalculoLiquidadorSol");
    private final static QName _OpRecibirHallazgosCruceNominaPILAFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceNominaPILAFallo");
    private final static QName _OpRecibirHallazgosCruceConciliacionContableNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceConciliacionContableNominaFallo");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Input");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Input");
    private final static QName _OpRecibirHojaCalculoLiquidadorResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHojaCalculoLiquidadorResp");
    private final static QName _MsjOpRecibirHojaCalculoLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "msjOpRecibirHojaCalculoLiquidadorFallo");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Output");
    private final static QName _PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Input");
    private final static QName _OpRecibirNominaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirNominaResp");
    private final static QName _OpRecibirHojaCalculoLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHojaCalculoLiquidadorFallo");
    private final static QName _OpRecibirHallazgosCruceConciliacionContableNominaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", "OpRecibirHallazgosCruceConciliacionContableNominaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExpedienteTipo }
     * 
     */
    public ExpedienteTipo createExpedienteTipo() {
        return new ExpedienteTipo();
    }

    /**
     * Create an instance of {@link HojaCalculoTipo }
     * 
     */
    public HojaCalculoTipo createHojaCalculoTipo() {
        return new HojaCalculoTipo();
    }

    /**
     * Create an instance of {@link IdentificacionTipo }
     * 
     */
    public IdentificacionTipo createIdentificacionTipo() {
        return new IdentificacionTipo();
    }

    /**
     * Create an instance of {@link HallazgoNominaTipo }
     * 
     */
    public HallazgoNominaTipo createHallazgoNominaTipo() {
        return new HallazgoNominaTipo();
    }

    /**
     * Create an instance of {@link NominaTipo }
     * 
     */
    public NominaTipo createNominaTipo() {
        return new NominaTipo();
    }

    /**
     * Create an instance of {@link ContextoRespuestaTipo }
     * 
     */
    public ContextoRespuestaTipo createContextoRespuestaTipo() {
        return new ContextoRespuestaTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput createPortSrvIntProcLiquidadorSOAPOpRecibirNominaInput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput();
    }

    /**
     * Create an instance of {@link OpRecibirHojaCalculoLiquidadorRespTipo }
     * 
     */
    public OpRecibirHojaCalculoLiquidadorRespTipo createOpRecibirHojaCalculoLiquidadorRespTipo() {
        return new OpRecibirHojaCalculoLiquidadorRespTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput();
    }

    /**
     * Create an instance of {@link OpRecibirNominaRespTipo }
     * 
     */
    public OpRecibirNominaRespTipo createOpRecibirNominaRespTipo() {
        return new OpRecibirNominaRespTipo();
    }

    /**
     * Create an instance of {@link FalloTipo }
     * 
     */
    public FalloTipo createFalloTipo() {
        return new FalloTipo();
    }

    /**
     * Create an instance of {@link OpRecibirHallazgosCruceConciliacionContableNominaSolTipo }
     * 
     */
    public OpRecibirHallazgosCruceConciliacionContableNominaSolTipo createOpRecibirHallazgosCruceConciliacionContableNominaSolTipo() {
        return new OpRecibirHallazgosCruceConciliacionContableNominaSolTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput createPortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput();
    }

    /**
     * Create an instance of {@link MsjOpRecibirHojaCalculoLiquidadorFallo }
     * 
     */
    public MsjOpRecibirHojaCalculoLiquidadorFallo createMsjOpRecibirHojaCalculoLiquidadorFallo() {
        return new MsjOpRecibirHojaCalculoLiquidadorFallo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput();
    }

    /**
     * Create an instance of {@link OpRecibirHallazgosCruceNominaPILARespTipo }
     * 
     */
    public OpRecibirHallazgosCruceNominaPILARespTipo createOpRecibirHallazgosCruceNominaPILARespTipo() {
        return new OpRecibirHallazgosCruceNominaPILARespTipo();
    }

    /**
     * Create an instance of {@link MsjOpRecibirHallazgosCruceNominaPILAFallo }
     * 
     */
    public MsjOpRecibirHallazgosCruceNominaPILAFallo createMsjOpRecibirHallazgosCruceNominaPILAFallo() {
        return new MsjOpRecibirHallazgosCruceNominaPILAFallo();
    }

    /**
     * Create an instance of {@link OpRecibirHojaCalculoLiquidadorSolTipo }
     * 
     */
    public OpRecibirHojaCalculoLiquidadorSolTipo createOpRecibirHojaCalculoLiquidadorSolTipo() {
        return new OpRecibirHojaCalculoLiquidadorSolTipo();
    }

    /**
     * Create an instance of {@link MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo }
     * 
     */
    public MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo createMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo() {
        return new MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo();
    }

    /**
     * Create an instance of {@link MsjOpRecibirNominaFallo }
     * 
     */
    public MsjOpRecibirNominaFallo createMsjOpRecibirNominaFallo() {
        return new MsjOpRecibirNominaFallo();
    }

    /**
     * Create an instance of {@link OpRecibirHallazgosCruceNominaPILASolTipo }
     * 
     */
    public OpRecibirHallazgosCruceNominaPILASolTipo createOpRecibirHallazgosCruceNominaPILASolTipo() {
        return new OpRecibirHallazgosCruceNominaPILASolTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput createPortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput();
    }

    /**
     * Create an instance of {@link OpRecibirHallazgosCruceConciliacionContableNominaRespTipo }
     * 
     */
    public OpRecibirHallazgosCruceConciliacionContableNominaRespTipo createOpRecibirHallazgosCruceConciliacionContableNominaRespTipo() {
        return new OpRecibirHallazgosCruceConciliacionContableNominaRespTipo();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput();
    }

    /**
     * Create an instance of {@link PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput }
     * 
     */
    public PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput createPortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput() {
        return new PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput();
    }

    /**
     * Create an instance of {@link OpRecibirNominaSolTipo }
     * 
     */
    public OpRecibirNominaSolTipo createOpRecibirNominaSolTipo() {
        return new OpRecibirNominaSolTipo();
    }

    /**
     * Create an instance of {@link CriterioOrdenamientoTipo }
     * 
     */
    public CriterioOrdenamientoTipo createCriterioOrdenamientoTipo() {
        return new CriterioOrdenamientoTipo();
    }

    /**
     * Create an instance of {@link HallazgoDetalleNominaTipo }
     * 
     */
    public HallazgoDetalleNominaTipo createHallazgoDetalleNominaTipo() {
        return new HallazgoDetalleNominaTipo();
    }

    /**
     * Create an instance of {@link ErrorTipo }
     * 
     */
    public ErrorTipo createErrorTipo() {
        return new ErrorTipo();
    }

    /**
     * Create an instance of {@link MunicipioTipo }
     * 
     */
    public MunicipioTipo createMunicipioTipo() {
        return new MunicipioTipo();
    }

    /**
     * Create an instance of {@link DepartamentoTipo }
     * 
     */
    public DepartamentoTipo createDepartamentoTipo() {
        return new DepartamentoTipo();
    }

    /**
     * Create an instance of {@link AportesIndependienteTipo }
     * 
     */
    public AportesIndependienteTipo createAportesIndependienteTipo() {
        return new AportesIndependienteTipo();
    }

    /**
     * Create an instance of {@link ContextoTransaccionalTipo }
     * 
     */
    public ContextoTransaccionalTipo createContextoTransaccionalTipo() {
        return new ContextoTransaccionalTipo();
    }

    /**
     * Create an instance of {@link HallazgoDetalleConciliacionContableTipo }
     * 
     */
    public HallazgoDetalleConciliacionContableTipo createHallazgoDetalleConciliacionContableTipo() {
        return new HallazgoDetalleConciliacionContableTipo();
    }

    /**
     * Create an instance of {@link DetalleHojaCalculoTipo }
     * 
     */
    public DetalleHojaCalculoTipo createDetalleHojaCalculoTipo() {
        return new DetalleHojaCalculoTipo();
    }

    /**
     * Create an instance of {@link ConceptoContableTipo }
     * 
     */
    public ConceptoContableTipo createConceptoContableTipo() {
        return new ConceptoContableTipo();
    }

    /**
     * Create an instance of {@link HallazgoConciliacionContableTipo }
     * 
     */
    public HallazgoConciliacionContableTipo createHallazgoConciliacionContableTipo() {
        return new HallazgoConciliacionContableTipo();
    }

    /**
     * Create an instance of {@link SerieDocumentalTipo }
     * 
     */
    public SerieDocumentalTipo createSerieDocumentalTipo() {
        return new SerieDocumentalTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Output")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput> createPortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput(PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput>(_PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Input")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput> createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput(PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirNominaFallo")
    public JAXBElement<FalloTipo> createOpRecibirNominaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRecibirNominaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirNominaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirNominaSol")
    public JAXBElement<OpRecibirNominaSolTipo> createOpRecibirNominaSol(OpRecibirNominaSolTipo value) {
        return new JAXBElement<OpRecibirNominaSolTipo>(_OpRecibirNominaSol_QNAME, OpRecibirNominaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHallazgosCruceNominaPILASolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceNominaPILASol")
    public JAXBElement<OpRecibirHallazgosCruceNominaPILASolTipo> createOpRecibirHallazgosCruceNominaPILASol(OpRecibirHallazgosCruceNominaPILASolTipo value) {
        return new JAXBElement<OpRecibirHallazgosCruceNominaPILASolTipo>(_OpRecibirHallazgosCruceNominaPILASol_QNAME, OpRecibirHallazgosCruceNominaPILASolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHallazgosCruceConciliacionContableNominaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceConciliacionContableNominaResp")
    public JAXBElement<OpRecibirHallazgosCruceConciliacionContableNominaRespTipo> createOpRecibirHallazgosCruceConciliacionContableNominaResp(OpRecibirHallazgosCruceConciliacionContableNominaRespTipo value) {
        return new JAXBElement<OpRecibirHallazgosCruceConciliacionContableNominaRespTipo>(_OpRecibirHallazgosCruceConciliacionContableNominaResp_QNAME, OpRecibirHallazgosCruceConciliacionContableNominaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Output")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput> createPortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput(PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpRecibirHallazgosCruceNominaPILAFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "msjOpRecibirHallazgosCruceNominaPILAFallo")
    public JAXBElement<MsjOpRecibirHallazgosCruceNominaPILAFallo> createMsjOpRecibirHallazgosCruceNominaPILAFallo(MsjOpRecibirHallazgosCruceNominaPILAFallo value) {
        return new JAXBElement<MsjOpRecibirHallazgosCruceNominaPILAFallo>(_MsjOpRecibirHallazgosCruceNominaPILAFallo_QNAME, MsjOpRecibirHallazgosCruceNominaPILAFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHallazgosCruceNominaPILARespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceNominaPILAResp")
    public JAXBElement<OpRecibirHallazgosCruceNominaPILARespTipo> createOpRecibirHallazgosCruceNominaPILAResp(OpRecibirHallazgosCruceNominaPILARespTipo value) {
        return new JAXBElement<OpRecibirHallazgosCruceNominaPILARespTipo>(_OpRecibirHallazgosCruceNominaPILAResp_QNAME, OpRecibirHallazgosCruceNominaPILARespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Output")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput> createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput(PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpRecibirNominaFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "msjOpRecibirNominaFallo")
    public JAXBElement<MsjOpRecibirNominaFallo> createMsjOpRecibirNominaFallo(MsjOpRecibirNominaFallo value) {
        return new JAXBElement<MsjOpRecibirNominaFallo>(_MsjOpRecibirNominaFallo_QNAME, MsjOpRecibirNominaFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "msjOpRecibirHallazgosCruceConciliacionContableNominaFallo")
    public JAXBElement<MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo> createMsjOpRecibirHallazgosCruceConciliacionContableNominaFallo(MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo value) {
        return new JAXBElement<MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo>(_MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo_QNAME, MsjOpRecibirHallazgosCruceConciliacionContableNominaFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHojaCalculoLiquidadorSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHojaCalculoLiquidadorSol")
    public JAXBElement<OpRecibirHojaCalculoLiquidadorSolTipo> createOpRecibirHojaCalculoLiquidadorSol(OpRecibirHojaCalculoLiquidadorSolTipo value) {
        return new JAXBElement<OpRecibirHojaCalculoLiquidadorSolTipo>(_OpRecibirHojaCalculoLiquidadorSol_QNAME, OpRecibirHojaCalculoLiquidadorSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceNominaPILAFallo")
    public JAXBElement<FalloTipo> createOpRecibirHallazgosCruceNominaPILAFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRecibirHallazgosCruceNominaPILAFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceConciliacionContableNominaFallo")
    public JAXBElement<FalloTipo> createOpRecibirHallazgosCruceConciliacionContableNominaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRecibirHallazgosCruceConciliacionContableNominaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Input")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput> createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput(PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Input")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput> createPortSrvIntProcLiquidadorSOAPOpRecibirNominaInput(PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput>(_PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHojaCalculoLiquidadorRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHojaCalculoLiquidadorResp")
    public JAXBElement<OpRecibirHojaCalculoLiquidadorRespTipo> createOpRecibirHojaCalculoLiquidadorResp(OpRecibirHojaCalculoLiquidadorRespTipo value) {
        return new JAXBElement<OpRecibirHojaCalculoLiquidadorRespTipo>(_OpRecibirHojaCalculoLiquidadorResp_QNAME, OpRecibirHojaCalculoLiquidadorRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MsjOpRecibirHojaCalculoLiquidadorFallo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "msjOpRecibirHojaCalculoLiquidadorFallo")
    public JAXBElement<MsjOpRecibirHojaCalculoLiquidadorFallo> createMsjOpRecibirHojaCalculoLiquidadorFallo(MsjOpRecibirHojaCalculoLiquidadorFallo value) {
        return new JAXBElement<MsjOpRecibirHojaCalculoLiquidadorFallo>(_MsjOpRecibirHojaCalculoLiquidadorFallo_QNAME, MsjOpRecibirHojaCalculoLiquidadorFallo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Output")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput> createPortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput(PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Input")
    public JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput> createPortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput(PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput value) {
        return new JAXBElement<PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput>(_PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput_QNAME, PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirNominaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirNominaResp")
    public JAXBElement<OpRecibirNominaRespTipo> createOpRecibirNominaResp(OpRecibirNominaRespTipo value) {
        return new JAXBElement<OpRecibirNominaRespTipo>(_OpRecibirNominaResp_QNAME, OpRecibirNominaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHojaCalculoLiquidadorFallo")
    public JAXBElement<FalloTipo> createOpRecibirHojaCalculoLiquidadorFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRecibirHojaCalculoLiquidadorFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRecibirHallazgosCruceConciliacionContableNominaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", name = "OpRecibirHallazgosCruceConciliacionContableNominaSol")
    public JAXBElement<OpRecibirHallazgosCruceConciliacionContableNominaSolTipo> createOpRecibirHallazgosCruceConciliacionContableNominaSol(OpRecibirHallazgosCruceConciliacionContableNominaSolTipo value) {
        return new JAXBElement<OpRecibirHallazgosCruceConciliacionContableNominaSolTipo>(_OpRecibirHallazgosCruceConciliacionContableNominaSol_QNAME, OpRecibirHallazgosCruceConciliacionContableNominaSolTipo.class, null, value);
    }

}
