
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para OpRecibirHojaCalculoLiquidadorSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpRecibirHojaCalculoLiquidadorSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="hojaCalculoLiquidador" type="{http://www.ugpp.gov.co/schema/Liquidaciones/HojaCalculoTipo/v1}HojaCalculoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpRecibirHojaCalculoLiquidadorSolTipo", propOrder = {
    "contextoTransaccional",
    "hojaCalculoLiquidador"
})
public class OpRecibirHojaCalculoLiquidadorSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected HojaCalculoTipo hojaCalculoLiquidador;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad hojaCalculoLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link HojaCalculoTipo }
     *     
     */
    public HojaCalculoTipo getHojaCalculoLiquidador() {
        return hojaCalculoLiquidador;
    }

    /**
     * Define el valor de la propiedad hojaCalculoLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link HojaCalculoTipo }
     *     
     */
    public void setHojaCalculoLiquidador(HojaCalculoTipo value) {
        this.hojaCalculoLiquidador = value;
    }

}
