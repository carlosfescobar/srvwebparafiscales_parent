
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceConciliacionContableNominaSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Input", propOrder = {
    "opRecibirHallazgosCruceConciliacionContableNominaSol"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaInput {

    @XmlElement(name = "OpRecibirHallazgosCruceConciliacionContableNominaSol", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected OpRecibirHallazgosCruceConciliacionContableNominaSolTipo opRecibirHallazgosCruceConciliacionContableNominaSol;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaSol.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHallazgosCruceConciliacionContableNominaSolTipo }
     *     
     */
    public OpRecibirHallazgosCruceConciliacionContableNominaSolTipo getOpRecibirHallazgosCruceConciliacionContableNominaSol() {
        return opRecibirHallazgosCruceConciliacionContableNominaSol;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHallazgosCruceConciliacionContableNominaSolTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceConciliacionContableNominaSol(OpRecibirHallazgosCruceConciliacionContableNominaSolTipo value) {
        this.opRecibirHallazgosCruceConciliacionContableNominaSol = value;
    }

}
