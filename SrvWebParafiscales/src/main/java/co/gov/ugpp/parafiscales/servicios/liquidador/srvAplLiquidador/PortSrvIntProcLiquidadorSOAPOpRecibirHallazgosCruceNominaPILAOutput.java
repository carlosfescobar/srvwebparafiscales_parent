
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceNominaPILAResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Output", propOrder = {
    "opRecibirHallazgosCruceNominaPILAResp"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAOutput {

    @XmlElement(name = "OpRecibirHallazgosCruceNominaPILAResp", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected OpRecibirHallazgosCruceNominaPILARespTipo opRecibirHallazgosCruceNominaPILAResp;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceNominaPILAResp.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHallazgosCruceNominaPILARespTipo }
     *     
     */
    public OpRecibirHallazgosCruceNominaPILARespTipo getOpRecibirHallazgosCruceNominaPILAResp() {
        return opRecibirHallazgosCruceNominaPILAResp;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceNominaPILAResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHallazgosCruceNominaPILARespTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceNominaPILAResp(OpRecibirHallazgosCruceNominaPILARespTipo value) {
        this.opRecibirHallazgosCruceNominaPILAResp = value;
    }

}
