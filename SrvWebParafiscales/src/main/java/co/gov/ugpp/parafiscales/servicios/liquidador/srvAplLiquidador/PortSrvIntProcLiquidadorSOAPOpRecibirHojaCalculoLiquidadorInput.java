
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHojaCalculoLiquidadorSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Input", propOrder = {
    "opRecibirHojaCalculoLiquidadorSol"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput {

    @XmlElement(name = "OpRecibirHojaCalculoLiquidadorSol", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected OpRecibirHojaCalculoLiquidadorSolTipo opRecibirHojaCalculoLiquidadorSol;

    /**
     * Obtiene el valor de la propiedad opRecibirHojaCalculoLiquidadorSol.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHojaCalculoLiquidadorSolTipo }
     *     
     */
    public OpRecibirHojaCalculoLiquidadorSolTipo getOpRecibirHojaCalculoLiquidadorSol() {
        return opRecibirHojaCalculoLiquidadorSol;
    }

    /**
     * Define el valor de la propiedad opRecibirHojaCalculoLiquidadorSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHojaCalculoLiquidadorSolTipo }
     *     
     */
    public void setOpRecibirHojaCalculoLiquidadorSol(OpRecibirHojaCalculoLiquidadorSolTipo value) {
        this.opRecibirHojaCalculoLiquidadorSol = value;
    }

}
