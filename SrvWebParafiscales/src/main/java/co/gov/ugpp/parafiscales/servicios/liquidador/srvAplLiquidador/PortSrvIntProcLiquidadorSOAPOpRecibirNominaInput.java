
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirNominaSol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Input", propOrder = {
    "opRecibirNominaSol"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput {

    @XmlElement(name = "OpRecibirNominaSol", namespace = "http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1", nillable = true)
    protected OpRecibirNominaSolTipo opRecibirNominaSol;

    /**
     * Obtiene el valor de la propiedad opRecibirNominaSol.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirNominaSolTipo }
     *     
     */
    public OpRecibirNominaSolTipo getOpRecibirNominaSol() {
        return opRecibirNominaSol;
    }

    /**
     * Define el valor de la propiedad opRecibirNominaSol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirNominaSolTipo }
     *     
     */
    public void setOpRecibirNominaSol(OpRecibirNominaSolTipo value) {
        this.opRecibirNominaSol = value;
    }

}
