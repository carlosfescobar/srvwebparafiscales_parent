
package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SerieDocumentalTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SerieDocumentalTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valNombreSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSerie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombreSubserie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSubserie" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDocumental" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SerieDocumentalTipo", namespace = "http://www.ugpp.gov.co/schema/GestionDocumental/SerieDocumentalTipo/v1", propOrder = {
    "valNombreSerie",
    "codSerie",
    "valNombreSubserie",
    "codSubserie",
    "codTipoDocumental"
})
public class SerieDocumentalTipo {

    @XmlElement(nillable = true)
    protected String valNombreSerie;
    @XmlElement(nillable = true)
    protected String codSerie;
    @XmlElement(nillable = true)
    protected String valNombreSubserie;
    @XmlElement(nillable = true)
    protected String codSubserie;
    @XmlElement(nillable = true)
    protected String codTipoDocumental;

    /**
     * Obtiene el valor de la propiedad valNombreSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreSerie() {
        return valNombreSerie;
    }

    /**
     * Define el valor de la propiedad valNombreSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreSerie(String value) {
        this.valNombreSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad codSerie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSerie() {
        return codSerie;
    }

    /**
     * Define el valor de la propiedad codSerie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSerie(String value) {
        this.codSerie = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreSubserie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreSubserie() {
        return valNombreSubserie;
    }

    /**
     * Define el valor de la propiedad valNombreSubserie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreSubserie(String value) {
        this.valNombreSubserie = value;
    }

    /**
     * Obtiene el valor de la propiedad codSubserie.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSubserie() {
        return codSubserie;
    }

    /**
     * Define el valor de la propiedad codSubserie.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSubserie(String value) {
        this.codSubserie = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDocumental.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDocumental() {
        return codTipoDocumental;
    }

    /**
     * Define el valor de la propiedad codTipoDocumental.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDocumental(String value) {
        this.codTipoDocumental = value;
    }

}
