package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplliquidador.MsjOpLiquidarFallo;
import co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarResTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma.AdministrarPersistencia;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.LoggerFactory;

import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma.GestorProgramaService;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma.thread.ParentThread;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplLiquidador",
        portName = "portSrvAplLiquidador",
        endpointInterface = "co.gov.ugpp.liquidador.srvaplliquidador.PortSrvAplLiquidador")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplLiquidador extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplLiquidador.class);

    @EJB
    private LiquidadorFacade liquidadorFacade;

    @PersistenceContext
    private EntityManager entityManager;

    @EJB
    private AdministrarPersistencia administrarPersistencia;

    //@EJB
    //private HojaCalculoLiquidacionDao hojaCalculoLiquidacionDao;
    //
    //@EJB
    //private HojaCalculoLiquidacionDetalleDao hojaCalculoLiquidacionDetalleDao;
    public co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarResTipo opLiquidar(co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarSolTipo msjOpLiquidarSol) throws MsjOpLiquidarFallo {

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpLiquidarSol.getContextoTransaccional();
        
        try {
            ldapAuthentication.validateLdapAuthentication(contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());
            initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            
            if (msjOpLiquidarSol.getExpediente().getIdNumExpediente().equalsIgnoreCase("Reset")) {
                String result = ParentThread.resetLiquidadorPool();
                OpLiquidarResTipo liquidarResTipo = new OpLiquidarResTipo();
                contextoRespuesta.setCodEstadoTx(result);                
                liquidarResTipo.setContextoRespuesta(contextoRespuesta);
                liquidarResTipo.setEsIncumplimiento(Boolean.FALSE);
                
                LOG.info(result);
                LOG.info("Op: OpLiquidar ::: Se reinicia enviado Reset");
                
                return liquidarResTipo;
                
            } else {
                LOG.info("Op: OpLiquidar ::: INIT");
                liquidadorFacade.liquidar(msjOpLiquidarSol);
            }

        } catch (AppException ex) {
            LOG.error("ERRROR EXCEPTION opLiquidar:" + ex.getMessage(), ex);
            throw new MsjOpLiquidarFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error("ERRROR EXCEPTION opLiquidar:" + ex.getMessage(), ex);
            throw new MsjOpLiquidarFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpLiquidarResTipo liquidarResTipo = new OpLiquidarResTipo();
        // Se envia uno (1) para notificar que fue recibida la peticion
        contextoRespuesta.setCodEstadoTx("1");
        // WMRR
        liquidarResTipo.setContextoRespuesta(contextoRespuesta);
        liquidarResTipo.setEsIncumplimiento(Boolean.FALSE);

        //SrvAplLiquidadorHilo liquidadorHilo = new SrvAplLiquidadorHilo(msjOpLiquidarSol, liquidarResTipo, hojaCalculoLiquidacionDao, hojaCalculoLiquidacionDetalleDao);
        //liquidadorHilo.start();
        try {

            String stringdate = "2015-03-27 15:56:49";
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = format.parse(stringdate);

            String modoEjecucion = "NOM";

            GestorProgramaService gestorProgramaService = new GestorProgramaService(administrarPersistencia,
                    entityManager, "1", modoEjecucion, date, msjOpLiquidarSol);
            gestorProgramaService.start();

        } catch (ParseException e) {
            LOG.error(e.getMessage(), e);
        }
        LOG.info("Op: OpLiquidar ::: END");
        return liquidarResTipo;

    }

}
