package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador;

import co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarResTipo;
import co.gov.ugpp.liquidador.srvaplliquidador.OpLiquidarSolTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.dao.HojaCalculoLiquidacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.HojaCalculoLiquidacionDetalleDao;
import co.gov.ugpp.parafiscales.persistence.entity.AportesIndependienteHclDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContableHclDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacion;
import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacionDetalle;
import co.gov.ugpp.parafiscales.servicios.helpers.DetalleHojaCalculoAssembler;
import co.gov.ugpp.parafiscales.servicios.util.SQLConection;
import co.gov.ugpp.schema.liquidaciones.detallehojacalculotipo.v1.DetalleHojaCalculoTipo;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.OpRecibirHojaCalculoLiquidadorSolTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;

/**
 *
 * @author Mauricio Guerrero
 */
public class SrvAplLiquidadorHilo extends Thread {

    @EJB
    private HojaCalculoLiquidacionDao hojaCalculoLiquidacionDao;
    
    @EJB
    private HojaCalculoLiquidacionDetalleDao hojaCalculoLiquidacionDetalleDao;

    private OpLiquidarSolTipo msjOpLiquidar;
    private OpLiquidarResTipo OpLiquidarRespTipo;
    private HojaCalculoLiquidacion hojaCalculoLiquidacion;
    private HojaCalculoTipo hojaCalculoTipo;
    private SQLConection sqlConection = SQLConection.getInstance();
    private DetalleHojaCalculoAssembler detalleHojaCalculoAssembler = DetalleHojaCalculoAssembler.getInstance();

    public SrvAplLiquidadorHilo(OpLiquidarSolTipo msjOpLiquidar, OpLiquidarResTipo OpLiquidarRespTipo, HojaCalculoLiquidacionDao hojaCalculoLiquidacionDAO, HojaCalculoLiquidacionDetalleDao hojaCalculoLiquidacionDetalleDao) {
        this.hojaCalculoLiquidacionDao = hojaCalculoLiquidacionDAO;
        this.msjOpLiquidar = msjOpLiquidar;
        this.OpLiquidarRespTipo = OpLiquidarRespTipo;
        this.hojaCalculoLiquidacionDetalleDao = hojaCalculoLiquidacionDetalleDao;
    }

    public void run() {
        try {

            System.out.println("ENTRO AL HILO");

            ResultSet hojaCalculoResultSet = sqlConection.consultaLiquidador(1, msjOpLiquidar.getExpediente().getIdNumExpediente(), msjOpLiquidar.getIdentificacion().getCodTipoIdentificacion(), msjOpLiquidar.getIdentificacion().getValNumeroIdentificacion(), "");
            System.out.println("CONECTO INFORMACION");

            hojaCalculoLiquidacion = new HojaCalculoLiquidacion();

            hojaCalculoLiquidacion.setIdexpediente(msjOpLiquidar.getExpediente().getIdNumExpediente());
            hojaCalculoLiquidacion.setFechacreacion(new Date());
            hojaCalculoLiquidacion.setIdUsuarioCreacion(msjOpLiquidar.getContextoTransaccional().getIdUsuario());

            BigDecimal id = hojaCalculoLiquidacionDao.create(hojaCalculoLiquidacion);
            hojaCalculoLiquidacion = hojaCalculoLiquidacionDao.find(id);

            List<HojaCalculoLiquidacionDetalle> listHojaCalculoLiquidacion = new ArrayList<HojaCalculoLiquidacionDetalle>();

            while (hojaCalculoResultSet.next()) {
                hojaCalculoLiquidacion.setCodesstado(hojaCalculoResultSet.getString("COD_ESTADO"));
                hojaCalculoLiquidacion.setDesestado(hojaCalculoResultSet.getString("DES_ESTADO"));

                HojaCalculoLiquidacionDetalle hojaCalculoLiquidacionDetalle = new HojaCalculoLiquidacionDetalle();

                if (hojaCalculoResultSet.getBigDecimal("IDNOMINADETALLE") != null) {
                    hojaCalculoLiquidacionDetalle.setIdnominadetalle(hojaCalculoResultSet.getBigDecimal("IDNOMINADETALLE").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("IDPILADETALLE") != null) {
                    hojaCalculoLiquidacionDetalle.setIdpiladetalle(hojaCalculoResultSet.getBigDecimal("IDPILADETALLE").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("IDCONCILIACIONCONTABLE") != null) {
                    hojaCalculoLiquidacionDetalle.setIdconciliacioncontabledetalle(hojaCalculoResultSet.getBigDecimal("IDCONCILIACIONCONTABLE").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setAportaNumeroIdentificacion(hojaCalculoResultSet.getString("APORTA_NUMERO_IDENTIFICACION"));
                if (hojaCalculoResultSet.getBigDecimal("APORTA_ID") != null) {
                    hojaCalculoLiquidacionDetalle.setAportaId(hojaCalculoResultSet.getBigDecimal("APORTA_ID").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("APORTA_TIPO_IDENTIFICACION") != null) {
                    hojaCalculoLiquidacionDetalle.setAportaTipoIdentificacion(hojaCalculoResultSet.getBigDecimal("APORTA_TIPO_IDENTIFICACION").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setAportaNumeroIdentificacion(hojaCalculoResultSet.getString("APORTA_NUMERO_IDENTIFICACION"));
                hojaCalculoLiquidacionDetalle.setAportaPrimerNombre(hojaCalculoResultSet.getString("APORTA_PRIMER_NOMBRE"));
                hojaCalculoLiquidacionDetalle.setAportaClase(hojaCalculoResultSet.getString("APORTA_CLASE"));
                hojaCalculoLiquidacionDetalle.setAportaAporteEsapYMen(hojaCalculoResultSet.getString("APORTA_APORTE_ESAP_Y_MEN"));
                hojaCalculoLiquidacionDetalle.setAportaExcepcionLey12332008(hojaCalculoResultSet.getString("APORTA_EXCEPCION_LEY_1233_2008"));
                if (hojaCalculoResultSet.getBigDecimal("COTIZ_ID") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizId(hojaCalculoResultSet.getBigDecimal("COTIZ_ID").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("COTIZ_TIPO_DOCUMENTO") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizTipoDocumento(hojaCalculoResultSet.getBigDecimal("COTIZ_TIPO_DOCUMENTO").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizNumeroIdentificacion(hojaCalculoResultSet.getString("COTIZ_NUMERO_IDENTIFICACION"));
                hojaCalculoLiquidacionDetalle.setCotizNombre(hojaCalculoResultSet.getString("COTIZ_NOMBRE"));
                if (hojaCalculoResultSet.getBigDecimal("COTIZ_TIPO_COTIZANTE") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizTipoCotizante(hojaCalculoResultSet.getBigDecimal("COTIZ_TIPO_COTIZANTE").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("COTIZ_SUBTIPO_COTIZANTE") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizSubtipoCotizante(hojaCalculoResultSet.getBigDecimal("COTIZ_SUBTIPO_COTIZANTE").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizExtranjeroNoCotizar(hojaCalculoResultSet.getString("COTIZ_EXTRANJERO_NO_COTIZAR"));
                hojaCalculoLiquidacionDetalle.setCotizColombianoEnElExt(hojaCalculoResultSet.getString("COTIZ_COLOMBIANO_EN_EL_EXT"));
                hojaCalculoLiquidacionDetalle.setCotizActividadAltoRiesgoPe(hojaCalculoResultSet.getString("COTIZ_ACTIVIDAD_ALTO_RIESGO_PE"));
                if (hojaCalculoResultSet.getBigDecimal("COTIZD_ANO") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizdAno(hojaCalculoResultSet.getBigDecimal("COTIZD_ANO").toBigInteger());
                }
                if (hojaCalculoResultSet.getBigDecimal("COTIZD_MES") != null) {
                    hojaCalculoLiquidacionDetalle.setCotizdMes(hojaCalculoResultSet.getBigDecimal("COTIZD_MES").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdIng(hojaCalculoResultSet.getString("COTIZD_ING"));
                hojaCalculoLiquidacionDetalle.setCotizdIngFecha(hojaCalculoResultSet.getDate("COTIZD_ING_FECHA"));
                hojaCalculoLiquidacionDetalle.setCotizdRet(hojaCalculoResultSet.getString("COTIZD_RET"));
                hojaCalculoLiquidacionDetalle.setCotizdRetFecha(hojaCalculoResultSet.getDate("COTIZD_RET_FECHA"));
                hojaCalculoLiquidacionDetalle.setCotizdSln(hojaCalculoResultSet.getString("COTIZD_SLN"));
                hojaCalculoLiquidacionDetalle.setCotizdCausalSuspension(hojaCalculoResultSet.getString("COTIZD_CAUSAL_SUSPENSION"));
                hojaCalculoLiquidacionDetalle.setCotizdCausalSuspFinicial(hojaCalculoResultSet.getDate("COTIZD_CAUSAL_SUSP_FINICIAL"));
                hojaCalculoLiquidacionDetalle.setCotizdCausalSuspFfinal(hojaCalculoResultSet.getDate("COTIZD_CAUSAL_SUSP_FFINAL"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_SUSPENSION") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasSuspension(hojaCalculoResultSet.getBigDecimal("DIAS_SUSPENSION").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdIge(hojaCalculoResultSet.getString("COTIZD_IGE"));
                hojaCalculoLiquidacionDetalle.setCotizdIgeFinicial(hojaCalculoResultSet.getDate("COTIZD_IGE_FINICIAL"));
                hojaCalculoLiquidacionDetalle.setCotizdIgeFfinal(hojaCalculoResultSet.getDate("COTIZD_IGE_FFINAL"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_INCAPACIDAD_GENERAL") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasIncapacidadGeneral(hojaCalculoResultSet.getBigDecimal("DIAS_INCAPACIDAD_GENERAL").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdLma(hojaCalculoResultSet.getString("COTIZD_LMA"));
                hojaCalculoLiquidacionDetalle.setCotizdLmaFinicial(hojaCalculoResultSet.getDate("COTIZD_LMA_FINICIAL"));
                hojaCalculoLiquidacionDetalle.setCotizdLmaFfinal(hojaCalculoResultSet.getDate("COTIZD_LMA_FFINAL"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_LICENCIA_MATERNIDAD") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasLicenciaMaternidad(hojaCalculoResultSet.getBigDecimal("DIAS_LICENCIA_MATERNIDAD").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdVac(hojaCalculoResultSet.getString("COTIZD_VAC"));
                hojaCalculoLiquidacionDetalle.setCotizdVacFinicial(hojaCalculoResultSet.getDate("COTIZD_VAC_FINICIAL"));
                hojaCalculoLiquidacionDetalle.setCotizdVacFfinal(hojaCalculoResultSet.getDate("COTIZD_VAC_FFINAL"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_VACACIONES") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasVacaciones(hojaCalculoResultSet.getBigDecimal("DIAS_VACACIONES").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdIrp(hojaCalculoResultSet.getString("COTIZD_IRP"));
                hojaCalculoLiquidacionDetalle.setCotizdIrpFinicial(hojaCalculoResultSet.getDate("COTIZD_IRP_FINICIAL"));
                hojaCalculoLiquidacionDetalle.setCotizdIrpFfinal(hojaCalculoResultSet.getDate("COTIZD_IRP_FFINAL"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_INCAPACIDAD_RP") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasIncapacidadRp(hojaCalculoResultSet.getBigDecimal("DIAS_INCAPACIDAD_RP").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setCotizdSalarioIntegral(hojaCalculoResultSet.getString("COTIZD_SALARIO_INTEGRAL"));
                hojaCalculoLiquidacionDetalle.setIngPilaAt(hojaCalculoResultSet.getString("ING_PILA_AT"));
                hojaCalculoLiquidacionDetalle.setRetPilaAu(hojaCalculoResultSet.getString("RET_PILA_AU"));
                hojaCalculoLiquidacionDetalle.setSlnPilaAv(hojaCalculoResultSet.getString("SLN_PILA_AV"));
                hojaCalculoLiquidacionDetalle.setIgePilaAw(hojaCalculoResultSet.getString("IGE_PILA_AU"));
                hojaCalculoLiquidacionDetalle.setLmaPilaAx(hojaCalculoResultSet.getString("LMA_PILA_AU"));
                hojaCalculoLiquidacionDetalle.setVacPilaAy(hojaCalculoResultSet.getString("VAC_PILA_AU"));
                hojaCalculoLiquidacionDetalle.setIrpPilaAz(hojaCalculoResultSet.getString("IRP_PILA_AU"));

                hojaCalculoLiquidacionDetalle.setPorcentajePagosNoSalariaBb(hojaCalculoResultSet.getBigDecimal("PORCENTAJE_PAGOS_NO_SALARIA_BB"));
                hojaCalculoLiquidacionDetalle.setExcedeLimitePagoNoSalarBc(hojaCalculoResultSet.getBigDecimal("EXCEDE_LIMITE_PAGO_NO_SALAR_BC"));
                hojaCalculoLiquidacionDetalle.setCodigoAdministradoraBd(hojaCalculoResultSet.getString("CODIGO_ADMINISTRADORA_BD"));
                hojaCalculoLiquidacionDetalle.setNombreCortoAdministradoraBe(hojaCalculoResultSet.getString("NOMBRE_CORTO_ADMINISTRADORA_BE"));
                hojaCalculoLiquidacionDetalle.setIbcCoreBf(hojaCalculoResultSet.getBigDecimal("IBC_CORE_BF"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreBg(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_BG"));
                hojaCalculoLiquidacionDetalle.setCotizacionObligatoriaBh(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLIGATORIA_BH"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_BI") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasCotizadosPilaBi(hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_BI").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setIbcPilaBj(hojaCalculoResultSet.getBigDecimal("IBC_PILA_BJ"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaBk(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_BK"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagadaPilaBl(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAGADA_PILA_BL"));
                hojaCalculoLiquidacionDetalle.setAjusteBm(hojaCalculoResultSet.getBigDecimal("AJUSTE_BM"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteBn(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_BN"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoBo(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_BO"));
                hojaCalculoLiquidacionDetalle.setCodigoAdministradoraBp(hojaCalculoResultSet.getString("CODIGO_ADMINISTRADORA_BP"));
                hojaCalculoLiquidacionDetalle.setNombreCortoAdministradoraBq(hojaCalculoResultSet.getString("NOMBRE_CORTO_ADMINISTRADORA_BQ"));
                hojaCalculoLiquidacionDetalle.setIbcCoreBr(hojaCalculoResultSet.getBigDecimal("IBC_CORE_BR"));
                hojaCalculoLiquidacionDetalle.setTarifaCorePensionBs(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_PENSION_BS"));
                hojaCalculoLiquidacionDetalle.setCotizacionObligatoAPensiBt(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLIGATO_A_PENSI_BT"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreFspBu(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_FSP_BU"));
                hojaCalculoLiquidacionDetalle.setCotizacionObligatoriaFspBv(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLIGATORIA_FSP_BV"));
                hojaCalculoLiquidacionDetalle.setTarifaCorePensionAdicArBw(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_PENSION_ADIC_AR_BW"));
                hojaCalculoLiquidacionDetalle.setCotizacionObliPenAdicArBx(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLI_PEN_ADIC_AR_BX"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_BY") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasCotizadosPilaBy(hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_BY").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setIbcPilaBz(hojaCalculoResultSet.getBigDecimal("IBC_PILA_BZ"));
                hojaCalculoLiquidacionDetalle.setTarifaPensionCa(hojaCalculoResultSet.getBigDecimal("TARIFA_PENSION_CA"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagadaPensionCb(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAGADA_PENSION_CB"));
                hojaCalculoLiquidacionDetalle.setAjustePensionCc(hojaCalculoResultSet.getBigDecimal("AJUSTE_PENSION_CC"));
                hojaCalculoLiquidacionDetalle.setConceptoAjustePensionCd(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_PENSION_CD"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaFspCe(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_FSP_CE"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagadaFspCf(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAGADA_FSP_CF"));
                hojaCalculoLiquidacionDetalle.setAjusteFspCg(hojaCalculoResultSet.getBigDecimal("AJUSTE_FSP_CG"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteFspCh(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_FSP_CH"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaPensiAdicioArCi(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_PENSI_ADICIO_AR_CI"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagPensAdicArCj(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAG_PENS_ADIC_AR_CJ"));
                hojaCalculoLiquidacionDetalle.setAjustePensionAdicionalArCk(hojaCalculoResultSet.getBigDecimal("AJUSTE_PENSION_ADICIONAL_AR_CK"));
                hojaCalculoLiquidacionDetalle.setConceptoAjustePensionArCl(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_PENSION_AR_CL"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoCm(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_CM"));
                hojaCalculoLiquidacionDetalle.setCalculoActuarialCn(hojaCalculoResultSet.getBigDecimal("CALCULO_ACTUARIAL_CN"));
                hojaCalculoLiquidacionDetalle.setCodigoAdministradoraCo(hojaCalculoResultSet.getString("CODIGO_ADMINISTRADORA_CO"));
                hojaCalculoLiquidacionDetalle.setNombreCortoAdministradoraCp(hojaCalculoResultSet.getString("NOMBRE_CORTO_ADMINISTRADORA_CP"));
                hojaCalculoLiquidacionDetalle.setIbcCoreCq(hojaCalculoResultSet.getBigDecimal("IBC_CORE_CQ"));
                hojaCalculoLiquidacionDetalle.setCotizacionObligatoriaCr(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLIGATORIA_CR"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_CS") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasCotizadosPilaCs(hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_CS").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setIbcPilaCt(hojaCalculoResultSet.getBigDecimal("IBC_PILA_CT"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaCu(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_CU"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagadaPilaCv(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAGADA_PILA_CV"));
                hojaCalculoLiquidacionDetalle.setAjusteCw(hojaCalculoResultSet.getBigDecimal("AJUSTE_CW"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteCx(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_CX"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoCy(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_CY"));
                hojaCalculoLiquidacionDetalle.setNombreCortoAdministradoraCz(hojaCalculoResultSet.getString("NOMBRE_CORTO_ADMINISTRADORA_CZ"));
                hojaCalculoLiquidacionDetalle.setIbcCoreDa(hojaCalculoResultSet.getBigDecimal("IBC_CORE_DA"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreDb(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_DB"));
                hojaCalculoLiquidacionDetalle.setCotizacionObligatoriaDc(hojaCalculoResultSet.getBigDecimal("COTIZACION_OBLIGATORIA_DC"));
                if (hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_DD") != null) {
                    hojaCalculoLiquidacionDetalle.setDiasCotizadosPilaDd(hojaCalculoResultSet.getBigDecimal("DIAS_COTIZADOS_PILA_DD").toBigInteger());
                }
                hojaCalculoLiquidacionDetalle.setIbcPilaDe(hojaCalculoResultSet.getBigDecimal("IBC_PILA_DE"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaDf(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_DF"));
                hojaCalculoLiquidacionDetalle.setCotizacionPagadaPilaDg(hojaCalculoResultSet.getBigDecimal("COTIZACION_PAGADA_PILA_DG"));
                hojaCalculoLiquidacionDetalle.setAjusteDh(hojaCalculoResultSet.getBigDecimal("AJUSTE_DH"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteDi(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_DI"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoDj(hojaCalculoResultSet.getString("TIPO_INCUMPLIMINETO_DJ"));
                hojaCalculoLiquidacionDetalle.setIbcCoreDk(hojaCalculoResultSet.getBigDecimal("IBC_CORE_DK"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreDl(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_DL"));
                hojaCalculoLiquidacionDetalle.setAporteDm(hojaCalculoResultSet.getBigDecimal("APORTE_DM"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaDn(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_DN"));
                hojaCalculoLiquidacionDetalle.setAportePilaDo(hojaCalculoResultSet.getBigDecimal("APORTE_PILA_DO"));
                hojaCalculoLiquidacionDetalle.setAjusteDp(hojaCalculoResultSet.getBigDecimal("AJUSTE_DP"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteDq(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_DQ"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoDr(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_DR"));
                hojaCalculoLiquidacionDetalle.setIbcCoreDs(hojaCalculoResultSet.getBigDecimal("IBC_CORE_DS"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreDt(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_DT"));
                hojaCalculoLiquidacionDetalle.setAporteDu(hojaCalculoResultSet.getBigDecimal("APORTE_DU"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaDv(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_DV"));
                hojaCalculoLiquidacionDetalle.setAportePilaDw(hojaCalculoResultSet.getBigDecimal("APORTE_PILA_DW"));
                hojaCalculoLiquidacionDetalle.setAjusteDx(hojaCalculoResultSet.getBigDecimal("AJUSTE_DX"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteDy(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_DY"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoDz(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_DZ"));
                hojaCalculoLiquidacionDetalle.setIbcEa(hojaCalculoResultSet.getBigDecimal("IBC_EA"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreEb(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_EB"));
                hojaCalculoLiquidacionDetalle.setAporteEc(hojaCalculoResultSet.getBigDecimal("APORTE_EC"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaEd(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_ED"));
                hojaCalculoLiquidacionDetalle.setAportePilaEe(hojaCalculoResultSet.getBigDecimal("APORTE_PILA_EE"));
                hojaCalculoLiquidacionDetalle.setAjusteEf(hojaCalculoResultSet.getBigDecimal("AJUSTE_EF"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteEg(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_EG"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoEh(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_EH"));
                hojaCalculoLiquidacionDetalle.setIbcCoreEi(hojaCalculoResultSet.getBigDecimal("IBC_CORE_EI"));
                hojaCalculoLiquidacionDetalle.setTarifaCoreEj(hojaCalculoResultSet.getBigDecimal("TARIFA_CORE_EJ"));
                hojaCalculoLiquidacionDetalle.setAporteEk(hojaCalculoResultSet.getBigDecimal("APORTE_EK"));
                hojaCalculoLiquidacionDetalle.setTarifaPilaEl(hojaCalculoResultSet.getBigDecimal("TARIFA_PILA_EL"));
                hojaCalculoLiquidacionDetalle.setAporteEm(hojaCalculoResultSet.getBigDecimal("APORTE_EM"));
                hojaCalculoLiquidacionDetalle.setAjusteEn(hojaCalculoResultSet.getBigDecimal("AJUSTE_EN"));
                hojaCalculoLiquidacionDetalle.setConceptoAjusteEo(hojaCalculoResultSet.getString("CONCEPTO_AJUSTE_EO"));
                hojaCalculoLiquidacionDetalle.setTipoIncumplimientoEp(hojaCalculoResultSet.getString("TIPO_INCUMPLIMIENTO_EP"));
//                hojaCalculoLiquidacionDetalle.setIdhojacalculoliquidacion(hojaCalculoLiquidacion);
                hojaCalculoLiquidacionDetalle.setIdUsuarioCreacion(msjOpLiquidar.getContextoTransaccional().getIdUsuario());
                
                BigDecimal idhcldetalle = hojaCalculoLiquidacionDetalleDao.create(hojaCalculoLiquidacionDetalle);
                hojaCalculoLiquidacionDetalle = hojaCalculoLiquidacionDetalleDao.find(idhcldetalle);

                ResultSet conceptoContableResultSet = sqlConection.consultaLiquidador(2, msjOpLiquidar.getExpediente().getIdNumExpediente(), msjOpLiquidar.getIdentificacion().getCodTipoIdentificacion(), msjOpLiquidar.getIdentificacion().getValNumeroIdentificacion(), hojaCalculoLiquidacionDetalle.getIdnominadetalle().toString());

                List<ConceptoContableHclDetalle> conceptosContableList = new ArrayList<ConceptoContableHclDetalle>();

                while (conceptoContableResultSet.next()) {
                    ConceptoContableHclDetalle conceptoContable = new ConceptoContableHclDetalle();

                    conceptoContable.setCodigo(conceptoContableResultSet.getString("CODIGO CONCEPTO DE PAGO"));
                    conceptoContable.setCuenta(conceptoContableResultSet.getString("CUENTA CONTABLE"));
                    conceptoContable.setNombreConcepto(conceptoContableResultSet.getString("NOMBRE CONCEPTO"));
                    conceptoContable.setEstado(conceptoContableResultSet.getString("ESTADO"));
                    conceptoContable.setNombre(conceptoContableResultSet.getString("NOMBRE CONCEPTO APORTANTE"));
                    conceptoContable.setSubsistemas(conceptoContableResultSet.getString("SUBSISTEMAS"));
                    conceptoContable.setValor(conceptoContableResultSet.getBigDecimal("VALOR"));
                    conceptoContable.setIdhojacalculoliquidaciondet(hojaCalculoLiquidacionDetalle);

                    conceptosContableList.add(conceptoContable);
                }

                ResultSet aportesIndependienteResultSet = sqlConection.consultaLiquidador(3, msjOpLiquidar.getExpediente().getIdNumExpediente(), msjOpLiquidar.getIdentificacion().getCodTipoIdentificacion(), msjOpLiquidar.getIdentificacion().getValNumeroIdentificacion(), hojaCalculoLiquidacionDetalle.getIdnominadetalle().toString());

                List<AportesIndependienteHclDetalle> AportesIntependienteList = new ArrayList<AportesIndependienteHclDetalle>();

                while (aportesIndependienteResultSet.next()) {
                    AportesIndependienteHclDetalle aportesIndependiente = new AportesIndependienteHclDetalle();

                    if (aportesIndependienteResultSet.getString("TIPO INGRESO") != null) {
                        aportesIndependiente.setTipoIngreso(new BigInteger(aportesIndependienteResultSet.getString("TIPO INGRESO")));
                    }
                    aportesIndependiente.setDescripcionIngreso(aportesIndependienteResultSet.getString("DESCRIPCION INGRESO"));
                    aportesIndependiente.setNumeroContrato(aportesIndependienteResultSet.getString("NUMERO CONTRATO"));
                    aportesIndependiente.setContratoFinicio(aportesIndependienteResultSet.getDate("FECHA INI INGRESO O CONTRATO"));
                    aportesIndependiente.setContratoFfinal(aportesIndependienteResultSet.getDate("FECHA FIN INGRESO O CONTRATO"));
                    aportesIndependiente.setValorContrato(aportesIndependienteResultSet.getBigDecimal("VALOR INGRESO O CONTRATO"));
                    if (aportesIndependienteResultSet.getString("DURACION INGRESO MESES") != null) {
                        aportesIndependiente.setHojatradDuracionIngMesesEz(new BigInteger(aportesIndependienteResultSet.getString("DURACION INGRESO MESES")));
                    }
                    if (aportesIndependienteResultSet.getString("ANIO") != null) {
                        aportesIndependiente.setHojatradAnoFa(new BigInteger(aportesIndependienteResultSet.getString("ANIO")));
                    }
                    if (aportesIndependienteResultSet.getString("MES") != null) {
                        aportesIndependiente.setHojatradMesFb(new BigInteger(aportesIndependienteResultSet.getString("MES")));
                    }
                    aportesIndependiente.setHojatradValorMensualizadoFc(aportesIndependienteResultSet.getBigDecimal("VAL PAGO MENSUALIZADO ING"));
                    aportesIndependiente.setHojatradIngresoDepuradoFd(aportesIndependienteResultSet.getBigDecimal("INGRESO DEPURADO"));
                    aportesIndependiente.setObservacionesIngresos(aportesIndependienteResultSet.getString("OBSERVACIONES"));
                    aportesIndependiente.setFechaCosto(aportesIndependienteResultSet.getDate("FECHA COSTO"));
                    aportesIndependiente.setDescripcionCosto(aportesIndependienteResultSet.getString("DESCRIPCION COSTO"));
                    aportesIndependiente.setProveedorCosto(aportesIndependienteResultSet.getString("PROVEEDOR COSTO"));
                    aportesIndependiente.setNumeroFactura(aportesIndependienteResultSet.getLong("NUMERO FACTURA"));
                    aportesIndependiente.setValorCosto(aportesIndependienteResultSet.getBigDecimal("VALOR COSTO"));
                    aportesIndependiente.setAceptacionCosto(aportesIndependienteResultSet.getString("ACEPTACION COSTO"));
                    aportesIndependiente.setObservacionesCosto(aportesIndependienteResultSet.getString("OBSERVACIONES COSTO"));
                    aportesIndependiente.setObservacionesNovedades(aportesIndependienteResultSet.getString("OBSERVACIONES NOVEDADES"));
                    aportesIndependiente.setObservacionesSalud(aportesIndependienteResultSet.getString("OBSERVACIONES SALUD"));
                    aportesIndependiente.setObservacionesPension(aportesIndependienteResultSet.getString("OBSERVACIONES PENSION"));
                    aportesIndependiente.setObservacionesArl(aportesIndependienteResultSet.getString("OBSERVACIONES ARL"));
                    aportesIndependiente.setObservacionesSena(aportesIndependienteResultSet.getString("OBSERVACIONES SENA"));
                    aportesIndependiente.setObservacionesIcbf(aportesIndependienteResultSet.getString("OBSERVACIONES ICBF"));
                    aportesIndependiente.setObservacionesMen(aportesIndependienteResultSet.getString("OBSERVACIONES MEN"));
                    aportesIndependiente.setObservacionesCcf(aportesIndependienteResultSet.getString("OBSERVACIONES CCF"));
                    aportesIndependiente.setObservacionesEsap(aportesIndependienteResultSet.getString("OBSERVACIONES ESAP"));
                    aportesIndependiente.setEstado(aportesIndependienteResultSet.getString("ESTADO"));
                    aportesIndependiente.setIdhojacalculoliquidaciondet(hojaCalculoLiquidacionDetalle);

                    AportesIntependienteList.add(aportesIndependiente);
                }

//                hojaCalculoLiquidacionDetalle.setConceptoContableHclDetalleCollection(conceptosContableList);

//                hojaCalculoLiquidacionDetalle.setAportesIndependienteHclDetaCollection(AportesIntependienteList);
                
                listHojaCalculoLiquidacion.add(hojaCalculoLiquidacionDetalle);
            }
            
            

            hojaCalculoLiquidacion.setHojaCalculoLiquidacionDetalCollection(listHojaCalculoLiquidacion);

            hojaCalculoLiquidacionDao.edit(hojaCalculoLiquidacion);

            System.out.println("CREO hojaCalculoLiquidacionDao: " + id);

            hojaCalculoLiquidacion.setId(id);

            hojaCalculoTipo = new HojaCalculoTipo();
            hojaCalculoTipo.setCodEstado(hojaCalculoLiquidacion.getCodesstado());
            hojaCalculoTipo.setDesEstado(hojaCalculoLiquidacion.getDesestado());
            if (hojaCalculoLiquidacion.getEsincumplimiento() != null) {
                hojaCalculoTipo.setEsIncumplimiento(hojaCalculoLiquidacion.getEsincumplimiento() == '1' ? true : false);
            }
            if (hojaCalculoLiquidacion.getId() != null) {
                hojaCalculoTipo.setIdHojaCalculoLiquidacion(hojaCalculoLiquidacion.getId().toString());
            }

            for (HojaCalculoLiquidacionDetalle detalleEnt : hojaCalculoLiquidacion.getHojaCalculoLiquidacionDetalCollection()) {
                DetalleHojaCalculoTipo detalleSrv = detalleHojaCalculoAssembler.assembleServicio(detalleEnt);
                hojaCalculoTipo.getDetalleHojaCalculo().add(detalleSrv);
            }

        } catch (javax.ejb.EJBException ex2) {
            ex2.printStackTrace();
            OpLiquidarRespTipo.getContextoRespuesta().setCodEstadoTx(ErrorEnum.FALLO.getCode());
        } catch (javax.validation.ConstraintViolationException ex1) {
            ex1.printStackTrace();
            OpLiquidarRespTipo.getContextoRespuesta().setCodEstadoTx(ErrorEnum.FALLO.getCode());
        } catch (Exception ex) {
            ex.printStackTrace();
            OpLiquidarRespTipo.getContextoRespuesta().setCodEstadoTx(ErrorEnum.FALLO.getCode());
        }finally{
            try {
                sqlConection.closeConnection();
            } catch (SQLException ex) {
                Logger.getLogger(SrvAplLiquidadorHilo.class.getName()).log(Level.SEVERE, null, "Hubo un error al intentar cerrar la conexion");
            }
        }

        try { // Call Web Service Operation
            System.out.println("LLAMA SERVICIO");

            co.gov.ugpp.transversales.srvintprocliquidador.v1.VSSrvIntProcLiquidador service = new co.gov.ugpp.transversales.srvintprocliquidador.v1.VSSrvIntProcLiquidador();
            co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAP port = service.getVSSrvIntProcLiquidadorsoap12Http();

            OpRecibirHojaCalculoLiquidadorSolTipo request = new OpRecibirHojaCalculoLiquidadorSolTipo();
            request.setContextoTransaccional(msjOpLiquidar.getContextoTransaccional());
            request.setHojaCalculoLiquidador(hojaCalculoTipo);

            co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput parameters = new PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorInput();
            parameters.setOpRecibirHojaCalculoLiquidadorSol(request);

            co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput result = port.opRecibirHojaCalculoLiquidador(parameters);
            System.out.println("Result Estado = " + result.getOpRecibirHojaCalculoLiquidadorResp().getContextoRespuesta().getCodEstadoTx().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("ERROR AL LLAMAR SERVICIO");

        }
    }
}
