package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma;

import java.math.BigDecimal;

import groovy.lang.GroovyShell;

public class GroovyRuntime implements RuntimeScript
{
   private GroovyShell groovyShell;

   public GroovyRuntime()
   {
      this.groovyShell = new GroovyShell();

   }

   @Override
   public Double ejecutarScript(String script) throws Exception
   {
      Object convertedValue = groovyShell.evaluate(script);

      if (convertedValue instanceof Double)
         return (Double) convertedValue;
      else if (convertedValue instanceof BigDecimal)
         return ((BigDecimal) convertedValue).doubleValue();
      if (convertedValue instanceof Double)
         return (Double) convertedValue;
      else if (convertedValue instanceof Integer)
         return new Double(convertedValue.toString());
      else if (convertedValue instanceof Long)
         return new Double(convertedValue.toString());

      return null;

   }

}
