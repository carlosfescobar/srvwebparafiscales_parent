package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma;

public interface RuntimeScript
{
   public Double ejecutarScript(String script) throws Exception;
}
