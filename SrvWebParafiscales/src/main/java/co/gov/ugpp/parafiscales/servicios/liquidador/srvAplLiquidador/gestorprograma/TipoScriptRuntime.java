package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplLiquidador.gestorprograma;

public class TipoScriptRuntime
{
   public static final String JAVASCRIPT = "JAVASCRIPT";
   public static final String GROOVY = "GROOVY";
}
