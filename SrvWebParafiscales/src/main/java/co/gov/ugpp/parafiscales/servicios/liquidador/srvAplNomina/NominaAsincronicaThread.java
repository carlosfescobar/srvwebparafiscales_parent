package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AportanteLIQ;
import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionExternaDefiniti;
import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import co.gov.ugpp.parafiscales.persistence.entity.TipoIdentificacion;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaLastThread;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaMultiThread;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread;
import static co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread.cargarListaTiposDocumentos;
import static co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread.convertToString;
import static co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread.getTipoDOcumentoBySigla;
import static co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread.isDaemonNominaPool;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1.EnvioTipo;
import co.gov.ugpp.schema.transversales.nominatipo.v1.NominaTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.OpRecibirNominaSolTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAP;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.VSSrvIntProcLiquidador;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.ejb.TransactionManagement;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import javax.xml.ws.BindingProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.LoggerFactory;

/**
 *
 * @author --
 */
@TransactionManagement
public class NominaAsincronicaThread extends Thread implements java.io.Serializable {

    private static final long serialVersionUID = 7038902816875760113L;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NominaAsincronicaThread.class);
    protected static final Object SYNCHRONIZED_OBJECT = new Object();

    private static final String NOMBRE = "NominaThread";
    private Nomina nomina;
    private ContextoTransaccionalTipo contextoSolicitud;

    private String idExpediente = null;
    private EnvioTipo envio;
    private EntityManager em;

    /**
     * Constructor por defecto
     */
    public NominaAsincronicaThread() {
    }

    /**
     * Constructor con parametros
     *
     * @param idExpediente
     * @param envio
     * @param contextoSolicitud
     * @param entityManager
     * @param error
     */
    public NominaAsincronicaThread(String idExpediente, EnvioTipo envio,
            ContextoTransaccionalTipo contextoSolicitud, EntityManager entityManager, boolean error) {
        this.contextoSolicitud = contextoSolicitud;
        this.idExpediente = idExpediente;
        this.envio = envio;
        this.em = entityManager;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(Thread.currentThread().getName().replace("pool", NOMBRE));
        synchronized (SYNCHRONIZED_OBJECT) {
            ejecutarPrograma();
        }
    }

    /**
     * Metodo de ejecucion principal
     */
    @SuppressWarnings("SleepWhileInLoop")
    public void ejecutarPrograma() {
        if (isDaemonNominaPool()) {
            LOG.error("#ERROR.NOMINA.DEMONIO# No se puede ejecutar porque existe un proceso bloqueando la ejecución.");
            try {
                Thread.currentThread().interrupt();
            } catch (SecurityException se) {
                LOG.error("SecurityException " + Thread.currentThread().getName(), se);
                invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Error tipo SecurityException procesando la nomina");
                return;
            }
        }
        while (NominaParentThread.isOcupado()) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                LOG.error("InterruptedException - " + Thread.currentThread().getName(), ex);
            }
        }
        
        
        NominaParentThread.ocuparHilo();
        UserTransaction transaction = null;
        ExecutorService executorService = null;
        boolean lastThreadRun = false;
        
        try 
        {

            try 
            {
                transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            } 
            catch (NamingException ex) 
            {
                LOG.error("ERROR ERROR NOMINA: error de transaccion: " + ex);
                //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "error de transaccion");
                throw new Exception(ex);
            }

            if (StringUtils.isBlank(idExpediente) || envio == null || envio.getRadicacion() == null
                    || StringUtils.isBlank(envio.getRadicacion().getIdRadicadoSalida())) {
                LOG.error("ERROR ERROR NOMINA: Los parametros de entrada: expediente y/o idRadicadoSalida no pueden ser nulos");
                //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Los parametros de entrada: expediente y/o idRadicadoSalida no pueden ser nulos");
                throw new Exception();
            }

            String idRadicadoEnvio = envio.getRadicacion().getIdRadicadoSalida();
            List<InformacionExternaDefiniti> listInformacionExternaDefinitiva;

            Query query = em.createNamedQuery("infoExternaDef.findByRadicadoFirst");
            query.setMaxResults(1);
            query.setParameter("idRadicado", idRadicadoEnvio);
            listInformacionExternaDefinitiva = query.getResultList();

            EnvioInformacionExterna envioInformacionExterna = new EnvioInformacionExterna();
            envioInformacionExterna.setIdRadicadoEnvio(idRadicadoEnvio);

            Expediente expedienteLiquidador;
            expedienteLiquidador = em.find(Expediente.class, idExpediente);

            if (expedienteLiquidador == null) {
                LOG.error("EXCEPCION ERROR NOMINA: No se encuentra el expediente con el id: " + idExpediente);
                //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "No se encuentra el expediente con el id: " + idExpediente);
                throw new Exception();
            }

            if (listInformacionExternaDefinitiva == null || listInformacionExternaDefinitiva.isEmpty()) {
                LOG.error("EXCEPCION ERROR NOMINA: El objeto 'Envio' no tiene archivos asociados");
                //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "El objeto 'Envio' no tiene archivos asociados");
                throw new Exception();
            }

            Date fechaTemporal = new Date();
            Timestamp tiempoTemporal = new Timestamp(fechaTemporal.getTime());

            nomina = new Nomina();
            nomina.setFechacreacion(DateUtil.currentCalendar());
            nomina.setIdexpediente(expedienteLiquidador);

            if (expedienteLiquidador.getNominaLiquidadorList() == null) {
                expedienteLiquidador.setNominaLiquidadorList(new ArrayList<Nomina>());
            }

            expedienteLiquidador.getNominaLiquidadorList().add(nomina);

            // Se consulta solo el ultimo registro
            InformacionExternaDefiniti informacionExternaDefinitiva = listInformacionExternaDefinitiva.get(0);
            if (informacionExternaDefinitiva != null)
            {
                Sheet sheet = null;
                try {
                    LOG.info("::CORE NOMINA:: Procesando informacionExternaDefinitiva:"
                            + informacionExternaDefinitiva.getId() + " - " + informacionExternaDefinitiva.getValNombreArchivo());

                    nomina.setArchivo(informacionExternaDefinitiva.getValContenidoArchivo());
                    nomina.setNombreArchivo("TEMP-" + Long.toString(tiempoTemporal.getTime()) + "-" + informacionExternaDefinitiva.getId().toString() + "-" + informacionExternaDefinitiva.getValNombreArchivo());
                    nomina.setIdinformacionexternadefinitiva(BigInteger.valueOf(informacionExternaDefinitiva.getId()));
                    nomina.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());

                    try ( // DETERMINAR ULTIMA FILA CON DATOS VERIFICANDO
                            // 5 CELDAS QUE SON OBLIGATORIAS
                            InputStream inputStream = new ByteArrayInputStream(informacionExternaDefinitiva.getValContenidoArchivo())) {
                        Workbook wb1 = WorkbookFactory.create(inputStream);
                        sheet = wb1.getSheetAt(0);
                        inputStream.close();
                    }

                    int numeroUltimaFila1 = 5;
                    int ultimaPilaSegunPoi = sheet.getLastRowNum();

                    String celdaObligatoriaH;
                    String celdaObligatoriaI;
                    String celdaObligatoriaJ;
                    String celdaObligatoriaK;
                    String celdaObligatoriaL;

                    do {
                        numeroUltimaFila1++;
                        Row fila = sheet.getRow(numeroUltimaFila1);
                        if (fila != null) {
                            celdaObligatoriaH = convertToString(fila.getCell(8, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaI = convertToString(fila.getCell(9, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaJ = convertToString(fila.getCell(10, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaK = convertToString(fila.getCell(11, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaL = convertToString(fila.getCell(12, Row.CREATE_NULL_AS_BLANK));
                        } else {
                            celdaObligatoriaH = null;
                            celdaObligatoriaI = null;
                            celdaObligatoriaJ = null;
                            celdaObligatoriaK = null;
                            celdaObligatoriaL = null;
                        }
                    } while (!StringUtils.isBlank(celdaObligatoriaH) && !StringUtils.isBlank(celdaObligatoriaI)
                            && !StringUtils.isBlank(celdaObligatoriaJ) && !StringUtils.isBlank(celdaObligatoriaK)
                            && !StringUtils.isBlank(celdaObligatoriaL) && numeroUltimaFila1 < ultimaPilaSegunPoi);

                    nomina.setnumEmpleado((numeroUltimaFila1 - 5));

                } catch (IOException | InvalidFormatException e) {
                    LOG.error("EXCEPCION ERROR NOMINA: " + e);
                    //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Error procesando la nomina ver el log");
                    throw new Exception(e);
                }

                try 
                {

                    int cantidadColumnasDinamicas = -1;
                    int filasCabecera = 5;
                    int cantidadColumnas = 64;
                    String contenidoCelda = "";

                    //INICIO: Codigo para determinar el numero de columnas dinámicas
                    Row fila = sheet.getRow(filasCabecera);

                    do {
                        cantidadColumnasDinamicas++;
                        contenidoCelda = convertToString(fila.getCell(cantidadColumnas + cantidadColumnasDinamicas, Row.CREATE_NULL_AS_BLANK));
                    } while (!contenidoCelda.isEmpty());

                    //*********************DATOS APORTANTE*****************************//
                    fila = sheet.getRow(3);
                    TipoIdentificacion tipoIdentificacion = null;
                    cargarListaTiposDocumentos(em);
                    try {
                        tipoIdentificacion = getTipoDOcumentoBySigla(convertToString(fila.getCell(1, Row.CREATE_NULL_AS_BLANK)));
                    } catch (NoResultException ex) {
                        //LOG.warn("EXCEPCION PERSISTIR TIPOIDENTIFICACION: " + ex);
                    }

                    if (tipoIdentificacion == null) {
                        LOG.error("EXCEPCION ERROR NOMINA: El tipoIdentificacion del aportante es incorrecto.");
                        //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "El tipoIdentificacion del aportante es incorrecto.");
                        throw new Exception();
                    }

                    AportanteLIQ aportante = null;
                    query = em.createNamedQuery("Aportante.findByIdentificacionAndIdTipoIdentificacion");
                    query.setMaxResults(1);
                    query.setParameter("numeroIdentificacion", convertToString(fila.getCell(2, Row.CREATE_NULL_AS_BLANK)));
                    query.setParameter("idTipoIdentificacion", tipoIdentificacion.getId());

                    try {
                        aportante = (AportanteLIQ) query.getSingleResult();
                    } catch (NoResultException ex) {
                        //LOG.warn("EXCEPCION PERSISTIR APORTANTE: " + ex);
                        aportante = null;
                    }

                    if (aportante == null) {
                        aportante = nuevoAportante(fila, tipoIdentificacion);
                    }

                    aportante.setNaturalezaJuridica(convertToString(fila.getCell(4, Row.CREATE_NULL_AS_BLANK)));
                    aportante.setTipoAportante(convertToString(fila.getCell(5, Row.CREATE_NULL_AS_BLANK)));
                    aportante.setAportaEsapYMen(convertToString(fila.getCell(6, Row.CREATE_NULL_AS_BLANK)).toLowerCase());
                    aportante.setSujetoPasivoImpuestoCree(convertToString(fila.getCell(7, Row.CREATE_NULL_AS_BLANK)).toUpperCase());

                    try {
                        transaction.begin();
                        if (aportante.getId() == null || aportante.getId() <= 0L) {
                            em.persist(aportante);
                        } else {
                            em.merge(aportante);
                        }
                        transaction.commit();
                    } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                        LOG.error("EXCEPCION PERSISTIR APORTANTE: " + ex);
                        //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Al persistir el aportante");
                        throw new Exception(ex);
                    }

                    //*********************FIN DATOS APORTANTE*****************************//
                    //EL EXPEDIENTE YA SE GUARDO EN LA NOMINA ARRIBA, NO SE CARGA DE ESTA CELDA SINO QUE VIENE
                    //EN LA TRAMA, DE HAY SE COJE.
                    //IdExpediente = fila.getCell(8, Row.CREATE_NULL_AS_BLANK).getStringCellValue().toLowerCase());
                    nomina.setTipoActo(convertToString(fila.getCell(9, Row.CREATE_NULL_AS_BLANK)).toLowerCase());
                    nomina.setNit(new BigInteger(aportante.getNumeroIdentificacion()));

                    try {
                        transaction.begin();
                        em.persist(nomina);
                        transaction.commit();
                    } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                        LOG.error("EXCEPCION PERSISTIR NOMINA: " + ex);
                        //invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Al persistir la nomina");
                        throw new Exception(ex);
                    }

                    int numeroUltimaFila = 5;
                    int ultimaPilaSegunPoi = sheet.getLastRowNum();

                    String celdaObligatoriaH;
                    String celdaObligatoriaI;
                    String celdaObligatoriaJ;
                    String celdaObligatoriaK;
                    String celdaObligatoriaL;

                    do {
                        numeroUltimaFila++;
                        fila = sheet.getRow(numeroUltimaFila);

                        if (fila != null) {
                            celdaObligatoriaH = convertToString(fila.getCell(8, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaI = convertToString(fila.getCell(9, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaJ = convertToString(fila.getCell(10, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaK = convertToString(fila.getCell(11, Row.CREATE_NULL_AS_BLANK));
                            celdaObligatoriaL = convertToString(fila.getCell(12, Row.CREATE_NULL_AS_BLANK));
                        } else {
                            celdaObligatoriaH = null;
                            celdaObligatoriaI = null;
                            celdaObligatoriaJ = null;
                            celdaObligatoriaK = null;
                            celdaObligatoriaL = null;
                        }

                    } while (!StringUtils.isBlank(celdaObligatoriaH) && !StringUtils.isBlank(celdaObligatoriaI)
                            && !StringUtils.isBlank(celdaObligatoriaJ) && !StringUtils.isBlank(celdaObligatoriaK)
                            && !StringUtils.isBlank(celdaObligatoriaL) && numeroUltimaFila < ultimaPilaSegunPoi);

                    //*********************FIN DETERMINAR ULTIMA FILA VALIDA*****************************//
                    //FORMATO A UTILIZAR EN LAS FECHAS SUMINISTRADAS POR EL EXCEL
                    List<String> lstDocumentos = new ArrayList<>();

                    for (int j = filasCabecera + 1; j <= numeroUltimaFila; j++) {
                        fila = sheet.getRow(j);
                        String numeroDoc = convertToString(fila.getCell(9, Row.CREATE_NULL_AS_BLANK));
                        String tipoDoc = convertToString(fila.getCell(10, Row.CREATE_NULL_AS_BLANK));
                        String cadena = numeroDoc + ";" + tipoDoc;
                        if (lstDocumentos.isEmpty() || !lstDocumentos.contains(cadena)) {
                            lstDocumentos.add(cadena);
                        }
                    }
                    int sizeLstDoc = lstDocumentos.size();
                    NominaMultiThread.setParameters(sheet, aportante,
                            cantidadColumnasDinamicas, contextoSolicitud, nomina,
                            sizeLstDoc, Thread.activeCount());
                    executorService = Executors.newFixedThreadPool(
                            Integer.parseInt(PropsReader.getKeyParam("parametroPoolSize")));
                    for (String auxiliar : lstDocumentos) {
                        List<Row> lstFilas = new ArrayList<>();
                        for (int j = filasCabecera + 1; j <= numeroUltimaFila; j++) {
                            fila = sheet.getRow(j);
                            String numeroDoc = convertToString(fila.getCell(9, Row.CREATE_NULL_AS_BLANK));
                            String tipoDoc = convertToString(fila.getCell(10, Row.CREATE_NULL_AS_BLANK));
                            String cadena = numeroDoc + ";" + tipoDoc;
                            if (auxiliar.equals(cadena) && !cadena.equals(";")) {
                                lstFilas.add(fila);
                            }
                        }
                        
                        if(lstFilas.size() > 0)
                        {
                            
                            
                            /*
                            if(lstFilas.get(0).getCell(9, Row.CREATE_NULL_AS_BLANK).getNumericCellValue() == 43035891)
                            { 
                                System.out.println("::ANDRES54:: size: " + lstFilas.size());
                                System.out.println("::ANDRES54:: documento: " + lstFilas.get(0).getCell(9, Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
                                System.out.println("::ANDRES54:: documento: " + lstFilas.get(2).getCell(9, Row.CREATE_NULL_AS_BLANK).getNumericCellValue());
                                System.out.println("::ANDRES54:: getAno: " + lstFilas.get(2).getCell(14, Row.CREATE_NULL_AS_BLANK));
                                System.out.println("::ANDRES54:: getMes: " + lstFilas.get(2).getCell(15, Row.CREATE_NULL_AS_BLANK));
                                System.out.println("::ANDRES54:: identificacion: " + lstFilas.get(2).getCell(9, Row.CREATE_NULL_AS_BLANK));
                                System.out.println("::ANDRES54:: vacaciones: " + lstFilas.get(2).getCell(27, Row.CREATE_NULL_AS_BLANK));
                                System.out.println("::ANDRES54:: novedad: " + lstFilas.get(2).getCell(21, Row.CREATE_NULL_AS_BLANK));
                            }
                            */
                            
                            
                            
                            
                            NominaMultiThread multiThread = new NominaMultiThread(lstFilas);
                            multiThread.setPriority(Thread.MAX_PRIORITY);
                            executorService.submit(multiThread);
                        }
                    }

                    
                    NominaLastThread nlt = new NominaLastThread(nomina.getId().toString(), "1", "Correcto");
                    nlt.setPriority(Thread.MIN_PRIORITY);
                    executorService.submit(nlt);
                    lastThreadRun = true;

                } catch (IOException | InvalidFormatException e) {
                    LOG.error("Error NOMINA: " + e);
                    throw new Exception(e);
                } finally {
                    // LOG.info("Asincrona.em.getFlushMode(): " + em.getFlushMode());
                }

            }

        } 
        catch (Exception e) 
        {
            LOG.error("ERROR EXCEPTION NOMINA" + e);
            invocarServicioIntegracion("0", "ERROR EXCEPCION NOMINA", "Error procesando la nomina");
            
            if (executorService != null) { // El pool de hilos ya se ha iniciado
                if (lastThreadRun) { // el lastThread libera al pool de hilos
                    NominaParentThread.unError();
                } else { // hay que deterner inmediatamente el pool de hilos y liberarlo
                    executorService.shutdownNow();
                    executorService = null;
                    NominaParentThread.liberarHilo();
                }
            } else {
                // en el caso que se presente un error y no se haya iniciado 
                // el pool de hilos hay que liberar el pool de hilos para que se 
                // puedan ejecutar los demas
                NominaParentThread.liberarHilo();
            }
        } 
        finally 
        {
            if (executorService != null) {
                executorService.shutdown();
            }
        }
    }

    /**
     * Metodo que retorna un aportante nuevo
     *
     * @param fila
     * @param tipoIdentificacion
     * @return
     */
    private AportanteLIQ nuevoAportante(Row fila, TipoIdentificacion tipoIdentificacion) throws Exception {
        AportanteLIQ aportante = new AportanteLIQ();
        aportante.setNumeroIdentificacion(convertToString(fila.getCell(2, Row.CREATE_NULL_AS_BLANK)));
        aportante.setTipoIdentificacion(tipoIdentificacion);
        aportante.setPrimerNombre(convertToString(fila.getCell(3, Row.CREATE_NULL_AS_BLANK)));
        aportante.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());
        return aportante;
    }
    
    
    
    
    private void invocarServicioIntegracion(String id, String codigo, String mensaje) {
        try {
            NominaTipo nominaTipo = new NominaTipo();
            nominaTipo.setIdNomina(id);
            nominaTipo.setCodigoEstado(codigo);
            nominaTipo.setEstado(mensaje);
   
            LOG.info("opCrearNomina llamar servicio de Integracion BPM con IDNomina: " + id + " Mensaje: " + mensaje);
            
            VSSrvIntProcLiquidador vssipl = new VSSrvIntProcLiquidador();
            PortSrvIntProcLiquidadorSOAP port = vssipl.getVSSrvIntProcLiquidadorsoap12Http();
            BindingProvider bindingProvider = (BindingProvider) port;
            bindingProvider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "s-bpmpar01");
            bindingProvider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "RptSSo1Ug");

            PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput input = new PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput();
            OpRecibirNominaSolTipo request = new OpRecibirNominaSolTipo();

            request.setContextoTransaccional(contextoSolicitud);
            request.setNomina(nominaTipo);

            input.setOpRecibirNominaSol(request);
            PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput output = port.opRecibirNomina(input);
            LOG.info("opCrearNomina Resultado = " + output.getOpRecibirNominaResp().getContextoRespuesta().getCodEstadoTx());

        } catch (Exception ex) {
            LOG.error("opCrearNomina llamar servicio de Integracion BPM ERROR: " + ex);
        }

    }
    
    

}
