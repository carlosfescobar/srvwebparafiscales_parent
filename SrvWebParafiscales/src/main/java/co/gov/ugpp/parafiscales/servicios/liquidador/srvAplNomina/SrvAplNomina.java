package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.liquidador.srvaplnomina.NominaTipo;
import co.gov.ugpp.liquidador.srvaplnomina.MsjOpCrearFallo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplNomina",
        portName = "portSrvAplNomina",
        endpointInterface = "co.gov.ugpp.liquidador.srvaplnomina.PortSrvAplNomina")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplNomina extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplNomina.class);

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Metodo principal WS
     *
     * @param msjOpCrearSol
     * @return
     * @throws MsjOpCrearFallo
     */
    public co.gov.ugpp.liquidador.srvaplnomina.OpCrearResTipo opCrear(co.gov.ugpp.liquidador.srvaplnomina.OpCrearSolTipo msjOpCrearSol) throws co.gov.ugpp.liquidador.srvaplnomina.MsjOpCrearFallo {
        LOG.info("Op: OpCrearNomina ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearSol.getContextoTransaccional();
        co.gov.ugpp.liquidador.srvaplnomina.OpCrearResTipo crearResTipo = new co.gov.ugpp.liquidador.srvaplnomina.OpCrearResTipo();
        NominaTipo nomina;

        try {
            initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            nomina = new NominaTipo();
            nomina.setIdNomina("0");
            crearResTipo.setNomina(nomina);
            crearResTipo.setContextoRespuesta(contextoRespuesta);
            if (msjOpCrearSol.getIdExpediente().equalsIgnoreCase("Reset")) {
                String result = NominaParentThread.resetNominaPool();
                LOG.info(result);
                crearResTipo.getContextoRespuesta().setCodEstadoTx(result);
            } else {
                ldapAuthentication.validateLdapAuthentication(contextoSolicitud.getIdUsuarioAplicacion(),
                        contextoSolicitud.getValClaveUsuarioAplicacion());
                //ASINCRONICO
                NominaAsincronicaThread nominaAsincronica = new NominaAsincronicaThread(msjOpCrearSol.getIdExpediente(),
                        msjOpCrearSol.getEnvio(), contextoSolicitud, entityManager, false);
                nominaAsincronica.start();
            }

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        LOG.info("Op: OpCrearNomina ::: END");

        return crearResTipo;
    }

}
