package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread;

import co.gov.ugpp.schema.transversales.nominatipo.v1.NominaTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.OpRecibirNominaSolTipo;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAP;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput;
import co.gov.ugpp.transversales.srvintprocliquidador.v1.VSSrvIntProcLiquidador;
import javax.xml.ws.BindingProvider;
import org.slf4j.LoggerFactory;
import static co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread.NominaParentThread.CONTEXTO_SOLICITUD;

/**
 * @since 04/Ago/2017
 * @author UT_TECNOLOGI
 */
public class NominaLastThread extends NominaParentThread {

    private static final long serialVersionUID = 1L;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NominaLastThread.class);
    String id;
    String codigo;
    String mensaje;

    /**
     * Constructor de la clase NominaLastThread Este debe ser siempre el ultimo
     * hilo en ejecutarse
     *
     * @param id
     * @param codigo
     * @param mensaje
     */
    public NominaLastThread(String id, String codigo, String mensaje) {
        this.id = id;
        this.codigo = codigo;
        this.mensaje = mensaje;
    }

    /**
     * Metodo principal de ejecucion del hilo
     */
    @Override
    public void run() {
        // LOG.info("Get in NominaLastThread() - " + Thread.currentThread().getName());
        if (!ERROR) {
            iniciarTarea();
            // LOG.info("Start NominaLastThread() - " + Thread.currentThread().getName());
            invocarServicioIntegracion(id, codigo, mensaje);
        }
        LOG.info("End NominaLastThread() - " + Thread.currentThread().getName());
        liberarHilo();
    }

    /**
     * Metodo encargado de llamar al servicio de integracion
     *
     * @param id
     * @param codigo
     * @param mensaje
     */
    private void invocarServicioIntegracion(String id, String codigo, String mensaje) {
        try {
            NominaTipo nominaTipo = new NominaTipo();
            nominaTipo.setIdNomina(id);
            nominaTipo.setCodigoEstado(codigo);
            nominaTipo.setEstado(mensaje);
   
            LOG.info("opCrearNomina llamar servicio de Integracion BPM con IDNomina: " + id);
            
            VSSrvIntProcLiquidador vssipl = new VSSrvIntProcLiquidador();
            PortSrvIntProcLiquidadorSOAP port = vssipl.getVSSrvIntProcLiquidadorsoap12Http();
            BindingProvider bindingProvider = (BindingProvider) port;
            bindingProvider.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, "s-bpmpar01");
            bindingProvider.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, "RptSSo1Ug");

            PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput input = new PortSrvIntProcLiquidadorSOAPOpRecibirNominaInput();
            OpRecibirNominaSolTipo request = new OpRecibirNominaSolTipo();

            request.setContextoTransaccional(CONTEXTO_SOLICITUD);
            request.setNomina(nominaTipo);

            input.setOpRecibirNominaSol(request);
            PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput output = port.opRecibirNomina(input);
            LOG.info("opCrearNomina Resultado = " + output.getOpRecibirNominaResp().getContextoRespuesta().getCodEstadoTx());

        } catch (Exception ex) {
            LOG.error("opCrearNomina llamar servicio de Integracion BPM ERROR: " + ex);
        }

    }

}
