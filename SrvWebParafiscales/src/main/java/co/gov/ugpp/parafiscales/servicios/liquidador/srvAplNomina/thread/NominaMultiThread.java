package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplNomina.thread;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AportanteLIQ;
import co.gov.ugpp.parafiscales.persistence.entity.AportesIndependiente;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContable;
import co.gov.ugpp.parafiscales.persistence.entity.ConceptoContableDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.CotizanteLIQ;
import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import co.gov.ugpp.parafiscales.persistence.entity.NominaDetalle;
import co.gov.ugpp.parafiscales.persistence.entity.TipoIdentificacion;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.ejb.TransactionManagement;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.LoggerFactory;

/**
 * @since 03/Ago/2017
 * @author UT_TECNOLOGI
 */
@TransactionManagement
public class NominaMultiThread extends NominaParentThread {

    private static final long serialVersionUID = 1L;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(NominaMultiThread.class);
    private static int cantidadColumnasDinamicas;
    private static Sheet sheet;
    private static AportanteLIQ aportante;

    private UserTransaction transaction;

    private List<Row> lstFilas;

    /**
     * Constructor por defecto
     */
    public NominaMultiThread() {
    }

    /**
     * Constructor con parametros
     *
     * @param lstFilas
     */
    public NominaMultiThread(List<Row> lstFilas) {
        this.lstFilas = lstFilas;
    }

    /**
     * metodo encargado de setear los valores de los atributos estaticos de la
     * clase
     *
     * @param sheet
     * @param aportante
     * @param contenidoCelda
     * @param cantidadColumnasDinamicas
     * @param contextoSolicitud
     * @param nomina
     * @param totalThread
     * @param iniThread
     */
    public static void setParameters(Sheet sheet, AportanteLIQ aportante,
            int cantidadColumnasDinamicas, ContextoTransaccionalTipo contextoSolicitud,
            Nomina nomina, Integer totalThread, Integer iniThread) {
        NominaMultiThread.sheet = sheet;
        NominaMultiThread.aportante = aportante;
        NominaMultiThread.cantidadColumnasDinamicas = cantidadColumnasDinamicas;
        NominaParentThread.setParameters(contextoSolicitud, nomina, totalThread, iniThread);
    }

    /**
     * Metodo principal de ejecucion de los hilos
     */
    @Override
    public void run() {
        setNewName();
        stopByError();
        asignarEntityManager();
        ejecutarThread();
        endingThread();
        em.clear();
    }

    /**
     * Metodo encargado de la ejecucion del proceso principal del hilo
     */
    void ejecutarThread() {
        TipoIdentificacion tipoIdentificacion = null;
        //LOG.info("em.getFlushMode(Start): " + em.getFlushMode());            
        int columna = 0;
        try {
            try {
                transaction = (UserTransaction) new InitialContext().lookup("java:comp/UserTransaction");
            } catch (NamingException ex) {
                LOG.error("ERROR NOMINA Columna: " + (columna+1) + ". Eerror de transaccion. Ex: " + ex);
                throw new Exception(ex);
            }
            Row fila = lstFilas.get(0);
            columna = 10;
            //*********************DATOS COTIZANTE*****************************//
            try {
                
                tipoIdentificacion = getTipoDOcumentoBySigla(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)).toUpperCase());
      
            } catch (NoResultException ex) {
                LOG.error("EXCEPCION Columna: " + (columna+1) + " PERSISTIR TIPOIDENTIFICACION. Ex: " + ex);
            }

            if (tipoIdentificacion == null) {
                LOG.error("EXCEPCION Columna: " + (columna+1) + " El tipoIdentificacion del cotizante es incorrecto.");
                throw new Exception();
            }

            columna = 9;
            CotizanteLIQ cotizante = null;
            stopByError(); // parada por ERROR
            Query query = em.createNamedQuery("Cotizante.findByIdentificacionAndIdTipoIdentificacion");
            query.setMaxResults(1);
            query.setParameter("numeroIdentificacion", convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            query.setParameter("idTipoIdentificacion", tipoIdentificacion.getId());
            
            //System.out.println("::ANDRES2:: numeroIdentificacion: " + convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            //System.out.println("::ANDRES2:: idTipoIdentificacion: " + tipoIdentificacion.getId());

            try {
                cotizante = (CotizanteLIQ) query.getSingleResult();
                //System.out.println("::ANDRES3:: cotizante: " + cotizante);
   
                
            } catch (NoResultException ex) {
                //LOG.warn("EXCEPCION Columna: " + (columna+1) + " PERSISTIR COTIZANTELIQ. Ex: " + ex);
            }

            if (cotizante == null) {
                //System.out.println("::ANDRES4:: se crea un cotizante fila: " + fila + " tipoIdentificacion: " + tipoIdentificacion);
                cotizante = nuevoCotizante(fila, tipoIdentificacion);
            }

            columna = 3;
            cotizante.setCondicionEspecialEmpresa(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            
            columna = 7;
            cotizante.setActividad_alto_riesgo_pension(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            
            columna = 10;
            cotizante.setTipoNumeroIdentificacionRealizoAportes(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            
            columna = 11;
            cotizante.setNumeroIdentificacionRealizoAportes(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
            
            columna = 13;
            cotizante.setCargoTrabajador(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));

            
            
            /*
            if(cotizante.getNumeroIdentificacion().equals("43035891"))
            {
                Row fila1 = lstFilas.get(1);
                
                System.out.println("::ANDRES56:: getAno: " + fila1.getCell(14, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: getMes: " + fila1.getCell(15, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: identificacion: " + fila1.getCell(9, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: vacaciones: " + fila1.getCell(27, Row.CREATE_NULL_AS_BLANK));
                
                
                Row fila2 = lstFilas.get(2);
                
                System.out.println("::ANDRES56:: getAno: " + fila2.getCell(14, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: getMes: " + fila2.getCell(15, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: identificacion: " + fila2.getCell(9, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: vacaciones: " + fila2.getCell(27, Row.CREATE_NULL_AS_BLANK));
            
                                
                Row fila3 = lstFilas.get(3);
                
                System.out.println("::ANDRES56:: getAno: " + fila3.getCell(14, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: getMes: " + fila3.getCell(15, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: identificacion: " + fila3.getCell(9, Row.CREATE_NULL_AS_BLANK));
                System.out.println("::ANDRES56:: vacaciones: " + fila3.getCell(27, Row.CREATE_NULL_AS_BLANK));
            
            }
            */
            
            
            
            try {
                stopByError(); // parada por ERROR
                transaction.begin();
                if (cotizante.getId() == null || cotizante.getId() <= 0L) {
                    em.persist(cotizante);
                    Long idCotizante = cotizante.getId();
                    cotizante = em.find(CotizanteLIQ.class, idCotizante);
                } else {
                    em.merge(cotizante);
                }
                em.flush();
                transaction.commit();
                //LOG.info("Set COTIZANTE: " + cotizante.getId());
            } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                LOG.error("EXCEPCION Columna: " + (columna+1) + " PERSISTIR COTIZANTE. Ex: " + ex);
                throw new Exception(ex);
            } finally {
                //LOG.info("cotizante: " + cotizante + " - Estado: " + transaction.getStatus());
            }
            //*********************FIN DATOS COTIZANTE*****************************//

            SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

            
            //System.out.println("::ANDRES2:: lstFilas: " + lstFilas.size());
            
            //*********************INICIO NOMINA DETALLE *****************************//
            //LOG.info(Thread.currentThread().getName() + "(" + (getCOUNT_THREAD() + 1)
            //        + ") Error: " + isError() + " - lstFilas.size(): " + lstFilas.size());
            // Calendar start = Calendar.getInstance();
            for (Row tmp : lstFilas) {
                NominaDetalle nominaDetalle = new NominaDetalle();
                nominaDetalle.setIdaportante(aportante);
                nominaDetalle.setIdcotizante(cotizante);

                
                columna = 1;
                nominaDetalle.setTipoCotizante(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 2;
                nominaDetalle.setSubTipoCotizante(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 3;
                nominaDetalle.setCondEspEmp(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 5;
                nominaDetalle.setExtranjero_no_obligado_a_cotizar_pension(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 6;
                nominaDetalle.setColombiano_en_el_exterior(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 14;
                nominaDetalle.setAno(new BigInteger(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 15;
                nominaDetalle.setMes(new BigInteger(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 16;
                nominaDetalle.setSalarioIntegral(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 17;
                nominaDetalle.setNovedadIncapacidad(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 18;
                nominaDetalle.setNovedadLicenciaMaternidadPaternidad(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 19;
                nominaDetalle.setNovedadPermisoLicenciaRemunerada(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 20;
                nominaDetalle.setNovedadSuspension(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 21;
                nominaDetalle.setNovedadVacaciones(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 22;
                nominaDetalle.setDiasTrabajadosMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 23;
                nominaDetalle.setDiasIncapacidadesMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 24;
                nominaDetalle.setDiasLicenciaMaternidadPaternidadMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 25;
                nominaDetalle.setDiasLicenciaRemuneradasMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 26;
                nominaDetalle.setDiasSuspensionMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));

                
                
                
                /*
                if(nominaDetalle.getIdcotizante().getId().intValue()==5170 && nominaDetalle.getAno().intValue()==2013 && nominaDetalle.getMes().intValue()==3)
                {
                    System.out.println("::ANDRES59:: getAno: " + nominaDetalle.getAno());
                    System.out.println("::ANDRES59:: getMes: " + nominaDetalle.getMes());
                    System.out.println("::ANDRES59:: getId: " + nominaDetalle.getIdcotizante().getId());
                    System.out.println("::ANDRES59:: convertToString: " + convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                }
                */
                
                
               
                
                columna = 27;
                nominaDetalle.setDiasVacacionesMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                
                //if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("43035891") && nominaDetalle.getAno().equals("2013") && nominaDetalle.getMes().equals("3"))
                //System.out.println("::ANDRES56:: getDiasVacacionesMes: " + nominaDetalle.getDiasVacacionesMes());
                
        
                /*
                if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("43035891"))
                {
                    System.out.println("::ANDRES57:: getAno: " + nominaDetalle.getAno());
                    System.out.println("::ANDRES57:: getMes: " + nominaDetalle.getMes());
                    System.out.println("::ANDRES57:: convertToString: " + convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                    System.out.println("::ANDRES57:: getId: " + nominaDetalle.getIdcotizante().getId());
                    System.out.println("::ANDRES57:: getDiasVacacionesMes: " + nominaDetalle.getDiasVacacionesMes());
                    System.out.println("::ANDRES57:: getNumeroIdentificacion: " + nominaDetalle.getIdcotizante().getNumeroIdentificacion());
                }
                */
                
                
                columna = 28;
                nominaDetalle.setDiasHuelgaLegalMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 29;
                nominaDetalle.setTotalDiasReportadosMes(new Integer(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 30;
                nominaDetalle.setIng(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                
                
                
                columna = 31;
                
                //System.out.println("::ANDRES9:: columna: " + columna + " getCellType: " + tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK).getCellType());
                //System.out.println("::ANDRES9:: columna: " + columna + " convertToString: " + convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));

                if (HSSFDateUtil.isCellDateFormatted(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK))) {
                    if (!StringUtils.isBlank(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))) {
                        formatoFecha.format(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK).getDateCellValue());
                        nominaDetalle.setIngFecha(formatoFecha.getCalendar().getTime());
                    }
                }

                columna = 32;
                nominaDetalle.setRet(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));

                columna = 33;
                if (HSSFDateUtil.isCellDateFormatted(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK))) {
                    if (!StringUtils.isBlank(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))) {
                        formatoFecha.format(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK).getDateCellValue());
                        nominaDetalle.setRetFecha(formatoFecha.getCalendar().getTime());
                    }
                }

                columna = 34;
                nominaDetalle.setCalculoActuarial(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 35;
                nominaDetalle.setValorCalculoActuarial(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 36;
                nominaDetalle.setSalBasico(new BigInteger(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 37;
                nominaDetalle.setIbcConcurrenciaIngresosOtrosAportantes(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));

                //nuevos campos 03-02-2016
                columna = 38;
                nominaDetalle.setIbcPenConIngrOtrApo(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 39;
                nominaDetalle.setIbcArlConIngrOtrApo(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                nominaDetalle.setNomina(NOMINA);
                nominaDetalle.setIdUsuarioCreacion(CONTEXTO_SOLICITUD.getIdUsuario());
                
                
                
                //COLUMNAS NUEVAS EN TOTAL 9 PARA PROCESAR PAGOS POR FUERA DE PILA MANUALES PLANILLA TIPO M 
                columna = 50;
                nominaDetalle.setCargueManualPilaSalud(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 51;
                nominaDetalle.setCargueManualPilaPension(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 52;
                nominaDetalle.setCargueManualPilaFspSolid(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 53;
                nominaDetalle.setCargueManualPilaFspSubsis(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 54;
                nominaDetalle.setCargueManualPilaAltoRiPe(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 55;
                nominaDetalle.setCargueManualPilaArl(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 56;
                nominaDetalle.setCargueManualPilaCcf(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 57;
                nominaDetalle.setCargueManualPilaSena(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                columna = 58;
                nominaDetalle.setCargueManualPilaIcbf(new BigDecimal(convertToZero(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)))));
                

                columna = 59;
                nominaDetalle.setOmisionSalud(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 60;
                nominaDetalle.setOmisionPension(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 61;
                nominaDetalle.setOmisionArl(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                columna = 62;
                nominaDetalle.setOmisionCcf(convertToString(tmp.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
                
                

                try {
                    stopByError(); // parada por ERROR
                    transaction.begin();
                    em.persist(nominaDetalle);
                    em.flush();
                    transaction.commit();
                    //LOG.info("Set NOMINADETALLE: " + nominaDetalle.getId());
                } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                    LOG.error("EXCEPCION Columna: " + (columna+1) + " PERSISTIR NOMINADETALLE Ex: " + ex);
                    throw new Exception(ex);
                } finally {
                    //LOG.info("nominaDetalle: " + nominaDetalle  + " - Estado: " + transaction.getStatus());
                }
                //*********************FIN NOMINA DETALLE *****************************//
                nuevoAportanteIndependiente(tmp, tipoIdentificacion, transaction, nominaDetalle);

                //********************* INICIO CONCEPTO CONTABLES **************//
                int contConceptosContables = 0;
                int valorTotalConcepto = 0;

                String tipoPagoUgpp;
                String contenidoCeldaLocal = null;

                if (cantidadColumnasDinamicas > 1) {
                    int cantidadColumnas = 64;
                    //RECORRE DE IZQIERDA A DERECHA LAS COLUMNAS DINAMICAS
                    while (contConceptosContables < cantidadColumnasDinamicas) {

                        try {
                            //TOMA LA PRIMERA COLUMNA DINAMICA DE LA FILA 6
                            contenidoCeldaLocal = convertToString(tmp.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK));
                            
                            
                            contenidoCeldaLocal = contenidoCeldaLocal.replace(',', '.');
                            if (!contenidoCeldaLocal.equals("")) {

                                ConceptoContable conceptoContable = new ConceptoContable();
                                
                                Row filaConceptoContableColumnaDinamica = sheet.getRow(0);
                                
                                conceptoContable.setJustificacionCambioUgpp(convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)));
                                filaConceptoContableColumnaDinamica = sheet.getRow(1);
                                
                                tipoPagoUgpp = convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK));
                                conceptoContable.setTipoPagoUgpp(tipoPagoUgpp);
                                
                                
                                
                                
                                //if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                                //    System.out.println("::ANDRES11:: contenidoCelda: " + contenidoCelda + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId() + " TIPO: " + tipoPagoUgpp);
                            

                                
                                // SUMATORIA PARA TP NO SALARIAL
                                if (tipoPagoUgpp.equals("TP NO SALARIAL") && Integer.parseInt(contenidoCeldaLocal) > 0) {
                                    try {
                                        valorTotalConcepto += Integer.parseInt(contenidoCeldaLocal);
                                           
                                    } catch (NumberFormatException ex) {
                                        LOG.error("EXCEPCION Columna: " + (columna+1) + " Valor total concepto. Ex: " + ex);
                                        throw new Exception(ex);
                                    }
                                }
                                
                                
                                
                                
                                

                                filaConceptoContableColumnaDinamica = sheet.getRow(2);
                                conceptoContable.setNombreConceptoUgpp(convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)));

                                filaConceptoContableColumnaDinamica = sheet.getRow(3);
                                conceptoContable.setSubsistemas(convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)));
                                String[] subsistemas = convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)).split(";");

                                if (subsistemas.length > 0 && !contenidoCeldaLocal.equals("0")) {
                                    for (String valor : subsistemas) {
                                        nuevoConceptoContableDetalle(valor, nominaDetalle,
                                                contenidoCeldaLocal, tipoPagoUgpp, transaction);
                                    }
                                }
                                
                                
                                
                                //colocar contenido celda como local, no como de clase para que nadie la modifique o se modifique
                                //        en memnoria........

                                //if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                                //    System.out.println("::ANDRES11:: contenidoCelda: " + contenidoCeldaLocal + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId() + " TIPO: " + tipoPagoUgpp);
                            
                                

                                filaConceptoContableColumnaDinamica = sheet.getRow(4);
                                conceptoContable.setCuentaContable(convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)));

                                filaConceptoContableColumnaDinamica = sheet.getRow(5);
                                conceptoContable.setNombreConceptoAportante(convertToString(filaConceptoContableColumnaDinamica.getCell(cantidadColumnas + contConceptosContables, Row.CREATE_NULL_AS_BLANK)));
                                conceptoContable.setValor(new BigDecimal(convertToZero(contenidoCeldaLocal)));
                                conceptoContable.setIdnominadetalle(nominaDetalle);
                                conceptoContable.setIdUsuarioCreacion(CONTEXTO_SOLICITUD.getIdUsuario());

                                
                                //if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                                //    System.out.println("::ANDRES12:: conceptoContable: " + conceptoContable.getValor() + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId() + " TIPO: " + tipoPagoUgpp);
                            
                                
                                                                
                                //if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                                //    System.out.println("::ANDRES13:: convert valor: " + new BigDecimal(convertToZero(contenidoCeldaLocal)) + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId() + " TIPO: " + tipoPagoUgpp);
                            
                                
                                
                                
                                try {
                                    stopByError(); // parada por ERROR
                                    transaction.begin();
                                    em.persist(conceptoContable);
                                    em.flush();
                                    transaction.commit();
                                    //LOG.info("Set CONCEPTOCONTABLE: " + conceptoContable.getId());
                                } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
                                    LOG.error("EXCEPCION Columna: " + (columna+1) + " PERSISTIR CONCEPTOCONTABLE Ex: " + ex);
                                    throw new Exception(ex);
                                }

                            }
                        } catch (Exception e) {
                            LOG.error("Error NOMINA Columna: " + (columna+1) + " contConceptosContables: " + (contConceptosContables + 1) + ". Error en registro de conceptos contables. Ex: " + e);
                            throw new Exception(e);
                        }
                        contConceptosContables++;
                    }

                    if (contConceptosContables == cantidadColumnasDinamicas && valorTotalConcepto > 0) {
                        guardarConceptoContableDetalle(nominaDetalle, valorTotalConcepto, transaction);
                    }
                }
                //********************* FIN CONCEPTO CONTABLES ************************//
            }
            //Long dif = Calendar.getInstance().getTime().getTime() - start.getTime().getTime();
            //LOG.info("End process NominaMultiThread (" + (getCOUNT_THREAD() + 1)
            //        + "/" + TOTAL_THREAD + ") " + Thread.currentThread().getName()
            //        + " [" + dif + " ms]");
        } catch (Exception e) {
            LOG.error("EXCEPCION NOMINA Columna: " + (columna+1) + ": Ex: " + e.getMessage());
            e.printStackTrace();
            setError(true);
        } finally {
            // LOG.info("em.getFlushMode(End): " + em.getFlushMode());            
        }
    }

    /**
     * Metodo encargado de crear un nuevo Aportante Independiente
     *
     * @param fila
     * @param tipoIdentificacion
     * @param transaction
     * @param nominaDetalle
     * @throws Exception
     */
    private void nuevoAportanteIndependiente(Row fila, TipoIdentificacion tipoIdentificacion,
            UserTransaction transaction, NominaDetalle nominaDetalle) throws Exception {
        //********************* DATOS APORTES INDEPENDIENTES **************//
        AportesIndependiente aportesIndependiente = new AportesIndependiente();
        int columna = 40; //OBSERVACIONES APORTANTE
        aportesIndependiente.setObservacionesAportante(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 41; //OBSERVACIONES UGPP
        aportesIndependiente.setObservacionesUgpp(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 42; //OBSERVACIONES APORTANTE SALUD
        aportesIndependiente.setObservacionesSalud(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 43; //OBSERVACIONES APORTANTE PENSION
        aportesIndependiente.setObservacionesPension(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 44; //OBSERVACIONES APORTANTE FSP
        aportesIndependiente.setObservacionesFsp(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 45; //OBSERVACIONES APORTANTE PENSION
        aportesIndependiente.setObservacionesAltoRiesgo(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 46;//OBSERVACIONES APORTANTE ARL
        aportesIndependiente.setObservacionesArl(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 47;//OBSERVACIONES APORTANTE CCF
        aportesIndependiente.setObservacionesCcf(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 48;//OBSERVACIONES APORTANTE SENA
        aportesIndependiente.setObservacionesSena(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        columna = 49;//OBSERVACIONES APORTANTE ICBF
        aportesIndependiente.setObservacionesIcbf(convertToString(fila.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
        aportesIndependiente.setIdnominadetalle(nominaDetalle);
        aportesIndependiente.setIdUsuarioCreacion(CONTEXTO_SOLICITUD.getIdUsuario());

        try {
            stopByError(); // parada por ERROR
            transaction.begin();
            em.persist(aportesIndependiente);
            em.flush();
            transaction.commit();
            //LOG.info("Set APORTESINDEPENDIENTE: " + aportesIndependiente.getId());
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("EXCEPCION PERSISTIR APORTESINDEPENDIENTE: " + ex);
            throw new Exception(ex);
        }
        //********************* DATOS APORTES INDEPENDIENTES **************//
    }

    /**
     * Metodo encargado de crear un nuevo Concepto contable detalle
     *
     * @param valor
     * @param nominaDetalle
     * @param contenidoCelda
     * @param tipoPagoUgpp
     * @param transaction
     * @throws Exception
     */
    private void nuevoConceptoContableDetalle(String valor, NominaDetalle nominaDetalle,
            String contenidoCelda, String tipoPagoUgpp, UserTransaction transaction) throws Exception {
        
        
        ConceptoContableDetalle conceptoContableDetalle = new ConceptoContableDetalle();

        
        /*
        if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
        {
           System.out.println("::ANDRES9:: contenidoCelda: " + contenidoCelda + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId());
           System.out.println("::ANDRES10:: valor: " + valor + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId());  
           System.out.println("::ANDRES10:: tipoPagoUgpp: " + tipoPagoUgpp + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId()); 
        }
        */
        
        
        
        if (!valor.equals("")) {
            
            /*
            if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                System.out.println("::ANDRES11:: contenidoCelda: " + contenidoCelda + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId());
            */
            
            conceptoContableDetalle.setIdSubsistema(new BigDecimal(valor));
        } else {
            
            /*
            if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
                System.out.println("::ANDRES12:: contenidoCelda: " + contenidoCelda + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId());
            */
            
            conceptoContableDetalle.setIdSubsistema(new BigDecimal(0));
        }

        
        conceptoContableDetalle.setIdnominadetalle(nominaDetalle);
        
        /*
        if(nominaDetalle.getIdcotizante().getNumeroIdentificacion().equals("1000393339"))
            System.out.println("::ANDRES13:: setValor: " + (new BigDecimal(convertToZero(contenidoCelda))) + " MES: " + nominaDetalle.getMes() + " ID: " + nominaDetalle.getId());
        */
        
        conceptoContableDetalle.setValor(new BigDecimal(convertToZero(contenidoCelda)));
        conceptoContableDetalle.setTipoPagoUgpp(tipoPagoUgpp);

        try {
            stopByError(); // parada por ERROR
            transaction.begin();
            em.persist(conceptoContableDetalle);
            em.flush();
            transaction.commit();
            //LOG.info("Set CONCEPTOCONTABLEDETALLE: " + conceptoContableDetalle.getId());
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("EXCEPCION PERSISTIR CONCEPTOCONTABLEDETALLE: " + ex);
            throw new Exception(ex);
        }
    }

    /**
     * Metodo encargado de guardar los detalles de los conceptos contables
     *
     * @param nominaDetalle
     * @param valorTotalConcepto
     * @param transaction
     * @throws Exception
     */
    private void guardarConceptoContableDetalle(NominaDetalle nominaDetalle, int valorTotalConcepto,
            UserTransaction transaction) throws Exception {
        ConceptoContableDetalle conceptoContableDetalle = new ConceptoContableDetalle();
        conceptoContableDetalle.setIdSubsistema(new BigDecimal("99"));
        conceptoContableDetalle.setIdnominadetalle(nominaDetalle);
        conceptoContableDetalle.setValor(new BigDecimal(convertToZero(Integer.toString(valorTotalConcepto))));
        conceptoContableDetalle.setTipoPagoUgpp("TP NO SALARIAL");

        try {
            stopByError(); // parada por ERROR
            transaction.begin();
            em.persist(conceptoContableDetalle);
            em.flush();
            transaction.commit();
            //LOG.info("Set CONCEPTOCONTABLEDETALLE TOTAL TPNOSALARIAL: " + conceptoContableDetalle.getId());
        } catch (IllegalStateException | SecurityException | HeuristicMixedException | HeuristicRollbackException | NotSupportedException | RollbackException | SystemException ex) {
            LOG.error("EXCEPCION PERSISTIR CONCEPTOCONTABLEDETALLE TOTAL TPNOSALARIAL: " + ex);
            throw new Exception(ex);
        }
    }

    /**
     * Metodo encargado de encontrar y devolver el identificador del hilo
     * asignado
     *
     * @return
     */
    private int getIdThread() {
        String[] arrayName = Thread.currentThread().getName().split("-");
        String sId = arrayName[arrayName.length - 1];
        int id = (Integer.valueOf(sId) - 1);
        return id;
    }

    /**
     * Metodo encargado de asignar las EntityManager correspondiente para su
     * ejecucion
     */
    private void asignarEntityManager() {
        em = getEntityManager(getIdThread());
    }

}
