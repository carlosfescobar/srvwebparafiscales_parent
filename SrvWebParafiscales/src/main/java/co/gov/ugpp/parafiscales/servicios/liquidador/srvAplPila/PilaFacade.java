package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplPila;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplpila.PilaTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import java.io.Serializable;
import java.util.Calendar;

/**
 *
 * @author franzjr
 */
public interface PilaFacade extends Serializable {

    /**
     * A través de esta operación se lee fuentes de datos externas 
     * para consolidar un registro que será almacenado en la base de datos del CORE, 
     * que esta sujeto a un proceso de depuración de acuerdo con las lógica del 
     * negocio definida por la UGPP
     * @param aportante
     * @param fecInicial
     * @param fecFinal
     * @param idExpediente
     * @param contextoSolicitud
     * @return 
     */
    public PilaTipo cargarPilaDepurada(AportanteTipo aportante, Calendar fecInicial, Calendar fecFinal, String idExpediente, ContextoTransaccionalTipo contextoSolicitud) throws AppException;

}
