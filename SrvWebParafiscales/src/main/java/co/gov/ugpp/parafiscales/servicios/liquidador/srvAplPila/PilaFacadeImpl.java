package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplPila;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.liquidador.srvaplpila.PilaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.PilaDao;
import co.gov.ugpp.parafiscales.persistence.dao.PilaDepuradaDao;
import co.gov.ugpp.parafiscales.persistence.dao.PilaDetalleDao;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Pila;
import co.gov.ugpp.parafiscales.persistence.entity.PilaDepurada;
import co.gov.ugpp.parafiscales.persistence.entity.PilaDetalle;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class PilaFacadeImpl extends AbstractFacade implements PilaFacade {

    @EJB
    private ExpedienteDao expedienteDao;

    @EJB
    private PilaDao pilaDao;

    @EJB
    private PilaDetalleDao pilaDetalleDao;
    
    @EJB
    private PilaDepuradaDao pilaDepuradaDao;
    
    
    @EJB
    private AportanteDao aportanteDao;
    

    @Override
    public PilaTipo cargarPilaDepurada(AportanteTipo aportante, Calendar fecInicial, Calendar fecFinal, String idExpediente, ContextoTransaccionalTipo contextoSolicitud) throws AppException {

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();
        
        
     
        

            if (aportante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El aportante no puede ser nulo"));
            } else {
                if (aportante.getPersonaJuridica() == null && aportante.getPersonaNatural() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                            "La Persona no puede ser nula"));
                } else {
                    if (aportante.getPersonaJuridica().getIdPersona() == null && aportante.getPersonaNatural().getIdPersona() == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                "El id persona no puede ser nulo"));
                    } else {
                        if (StringUtils.isBlank(aportante.getPersonaJuridica().getIdPersona().getValNumeroIdentificacion())
                                || StringUtils.isBlank(aportante.getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                                    "El Numero Identificacion y/o el tipo no puede ser nulo"));
                        }
                    }
                }
            }
            

                    
                    
                    
            if (fecInicial == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "La fecha inicial no puede ser nula"));
            }
            if (fecFinal == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "La fecha final no puede ser nula"));
            }
            if (StringUtils.isBlank(idExpediente)) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El id del expediente no puede ser nulo"));
            }
            if (errorTipoList.size() > 0) {
                throw new AppException(errorTipoList);
            }
        
            
            
            if (!(aportante.getPersonaJuridica().getIdPersona().getCodTipoIdentificacion().matches("[0-9]*"))){     
                throw new AppException("El tipo de identificación debe ser numerico: " + aportante.getPersonaJuridica().getIdPersona().getCodTipoIdentificacion());
            }
            
            Identificacion identificacionAcreedor = new Identificacion(aportante.getPersonaJuridica().getIdPersona().getCodTipoIdentificacion(), aportante.getPersonaJuridica().getIdPersona().getValNumeroIdentificacion());
  
            Aportante aportanteLiquidador = aportanteDao.findByIdentificacion(identificacionAcreedor);
            
            
            if (aportanteLiquidador == null) {
                   errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                        "El aportante no existe con los datos suministrados (tipo y numero de identificación)."));
            }


            
            

        String periodoInicial = String.format("%02d", fecInicial.get(Calendar.YEAR)) + String.format("%02d", fecInicial.get(Calendar.MONTH) + 1);
        String periodoFinal = String.format("%02d", fecFinal.get(Calendar.YEAR)) + String.format("%02d", fecFinal.get(Calendar.MONTH) + 1);
        
        Query queryPilaDepurada = pilaDepuradaDao.getEntityManager().createNamedQuery("PilaDepurada.findByLiquidar");

        String nit = "";


        try {
            nit = aportante.getPersonaNatural().getIdPersona().getValNumeroIdentificacion();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        
        
        if (StringUtils.isBlank(nit)) {
            try {
                nit = aportante.getPersonaJuridica().getIdPersona().getValNumeroIdentificacion();
                
            } catch (Exception ex) {
                throw new AppException("No se encontro aportante con el nit: " + nit);
            }
        }

        
        queryPilaDepurada.setParameter("nit", nit);
        queryPilaDepurada.setParameter("periodo_inicial", periodoInicial);
        queryPilaDepurada.setParameter("periodo_final", periodoFinal);

        List<PilaDepurada> listPilaDepurada = null;
        try {
            listPilaDepurada = (List<PilaDepurada>) queryPilaDepurada.getResultList();
        } catch (Exception e) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "error en consulta de lista pila depurada"));
        }
        

        Expediente expedienteLiquidador = expedienteDao.find(idExpediente);
   
        
        if (expedienteLiquidador == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encuentra el expediente con el id: " + idExpediente));
        }


        if (!errorTipoList.isEmpty()) {

            throw new AppException(errorTipoList);

        }

        Pila pila = new Pila();
        try {
            pila.setEstado("1");
            pila.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());
            pila.setFechacreacion(new Date());
            pila.setIdexpediente(expedienteLiquidador);
            pila.setPilaDetalleList(new ArrayList<PilaDetalle>());
            pilaDao.create(pila);

            
            
        } catch (Exception e) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                    "Hubo un problema al intentar realizar el registro del maestro de pila: "));
            throw new AppException(errorTipoList);

        }

        
        
        
        for (PilaDepurada itemPilaDepurada : listPilaDepurada) {
            try {
                
                PilaDetalle pilaDetalle = new PilaDetalle();
                pilaDetalle.setId(BigDecimal.valueOf(System.currentTimeMillis()));
                

                pilaDetalle.setCodigoEPS(itemPilaDepurada.getCodigoEPS());
                pilaDetalle.setDiasCotSalud(itemPilaDepurada.getDiasCotSalud());
                pilaDetalle.setIbcSalud(itemPilaDepurada.getIbcSalud());
                pilaDetalle.setTarifaSalud(itemPilaDepurada.getTarifaSalud());
                pilaDetalle.setCotObligatoriaSalud(itemPilaDepurada.getCotObligatoriaSalud());
                pilaDetalle.setCodigoAFP(itemPilaDepurada.getCodigoAFP());
                pilaDetalle.setDiasCotPension(itemPilaDepurada.getDiasCotPension());
                pilaDetalle.setIbcPension(itemPilaDepurada.getIbcPension());
                pilaDetalle.setTarifaPension(itemPilaDepurada.getTarifaPension());        
                pilaDetalle.setAporteCotObligatoriaPension(itemPilaDepurada.getAporteCotObligatoriaPension());        
                
                
                
                
                        //**************************PENDIENTES POR CONFIRMAR

                        //PARA ESTOS DOS CAMPOS NICOLAS AFIRMA:
                        //EXISTE UN CAMPO Q SE LLAMA FSP es la sumatoria de estos 2 campos (CO, CP) solicitados en las reglas de pila 
                        //depurada si necesitan los campos por separado hay que modificar las reglas

                        //COLUMNA: CO	CAMPO: cotización PAGADA PILA FSP Subcuenta de solidaridad
        //        pilaDetalle.setCotizacionPagadaSolidaridad(new BigDecimal(itemPilaDepurada.getTotalFSP())); 
                
                        //COLUMNA: CP	CAMPO: cotización PAGADA PILA FSP Subcuenta de Subsistencia
       //         pilaDetalle.setCotizacionPagadaSubsistencia(new BigDecimal(itemPilaDepurada.getTotalFSP())); 
                
                 
                            //**************************PENDIENTES POR CREAR:
                 
                             //COLUMNA: CX	CAMPO: TARIFA PILA PENSIÓN ADICIONAL ACT. ALTO RIESGO
         //       pilaDetalle.setTarifaPensionActividadAltoRiesgo(BigDecimal.ZERO);  
              
                            //COLUMNA: CY	CAMPO: cotización PAGADA PILA PENSIÓN ADICIONAL ACT. ALTO RIESGO           
         //       pilaDetalle.setCotizacionPagadaPensionAltoRiesgo(BigDecimal.ZERO);
                
                
       
               
                
                pilaDetalle.setCodigoArp(itemPilaDepurada.getCodigoArp());   
                pilaDetalle.setTarifaCentroTrabajo(itemPilaDepurada.getTarifaCentroTrabajo()); 
                pilaDetalle.setDiasCotRprof(itemPilaDepurada.getDiasCotRprof());  
                pilaDetalle.setIbcRprof(itemPilaDepurada.getIbcRprof()); 
                pilaDetalle.setDiasCotRprof(itemPilaDepurada.getDiasCotRprof()); 
                pilaDetalle.setCotObligatoriaArp(itemPilaDepurada.getCotObligatoriaArp()); 
                pilaDetalle.setCodigoCCF(itemPilaDepurada.getCodigoCCF()); 
                pilaDetalle.setDiasCotCcf(itemPilaDepurada.getDiasCotCcf()); 
                pilaDetalle.setIbcCcf(itemPilaDepurada.getIbcCcf());
                pilaDetalle.setTarifaAportesCcf(itemPilaDepurada.getTarifaAportesCcf());
                pilaDetalle.setValorAportesCcfIbcTarifa(itemPilaDepurada.getValorAportesCcfIbcTarifa());
                pilaDetalle.setTarifaAportesSena(itemPilaDepurada.getTarifaAportesSena());
                pilaDetalle.setValorAportesParafiscalesSena(itemPilaDepurada.getValorAportesParafiscalesSena());
                pilaDetalle.setTarifaAportesIcbf(itemPilaDepurada.getTarifaAportesIcbf());
                pilaDetalle.setValorAportesParafiscalesIcbf(itemPilaDepurada.getValorAportesParafiscalesIcbf());
                pilaDetalle.setPlanilla(itemPilaDepurada.getPlanilla());

                
                pilaDetalle.setIdUsuarioCreacion(contextoSolicitud.getIdUsuario());
                pilaDetalle.setIdpila(pila);

                pilaDetalleDao.create(pilaDetalle);


            } catch (Exception e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                        "Hubo un problema al intentar realizar el registro del maestro de pila..: "+e.toString()));
                throw new AppException(errorTipoList);
            }
        }

        
        
        PilaTipo pilaTipo = new PilaTipo();
        pilaTipo.setIdPila(pila.getId().toString());

        return pilaTipo;

    }

}
