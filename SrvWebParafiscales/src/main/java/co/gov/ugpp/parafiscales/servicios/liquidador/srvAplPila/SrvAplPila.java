package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplPila;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.liquidador.srvaplpila.MsjOpCargarPilaDepuradaFallo;
import co.gov.ugpp.liquidador.srvaplpila.OpCargarPilaDepuradaResTipo;
import co.gov.ugpp.liquidador.srvaplpila.PilaTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplPila", 
        portName = "portSrvAplPila", 
        endpointInterface = "co.gov.ugpp.liquidador.srvaplpila.PortSrvAplPila")

@HandlerChain(file = "handler-chain.xml")
public class SrvAplPila extends AbstractSrvApl {
    
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplPila.class);
    
    @EJB
    private PilaFacade pilaFacade;

    public co.gov.ugpp.liquidador.srvaplpila.OpCargarPilaDepuradaResTipo opCargarPilaDepurada(co.gov.ugpp.liquidador.srvaplpila.OpCargarPilaDepuradaSolTipo msjOpCargarPilaDepuradaSol) throws MsjOpCargarPilaDepuradaFallo {
        LOG.info("Op: OpCrearConciliacionContable ::: INIT");
        
        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCargarPilaDepuradaSol.getContextoTransaccional();

        PilaTipo pila;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            pila = pilaFacade.cargarPilaDepurada(msjOpCargarPilaDepuradaSol.getAportante(), msjOpCargarPilaDepuradaSol.getFecInicial(), msjOpCargarPilaDepuradaSol.getFecFinal(), msjOpCargarPilaDepuradaSol.getIdExpediente(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCargarPilaDepuradaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCargarPilaDepuradaFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }

        OpCargarPilaDepuradaResTipo cargarPilaDepuradaResTipo = new OpCargarPilaDepuradaResTipo();
        cargarPilaDepuradaResTipo.setPila(pila);
        cargarPilaDepuradaResTipo.setContextoRespuesta(contextoRespuesta);

        LOG.info("Op: OpCrearConciliacionContable ::: END");
        
        return cargarPilaDepuradaResTipo;
    }
    
}
