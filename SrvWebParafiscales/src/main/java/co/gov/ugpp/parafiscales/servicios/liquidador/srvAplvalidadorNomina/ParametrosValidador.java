package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplValidadorNomina;

/**
 *
 * @author jgonzalezt
 */
public class ParametrosValidador {

    private String archivo;
    private String nombreArchivo;
    private Long idFormato;
    private Long numVersion;
    private String nombreUsuario;

    /**
     * @return the archivo
     */
    public String getArchivo() {
        return archivo;
    }

    /**
     * @param archivo the archivo to set
     */
    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    /**
     * @return the nombreArchivo
     */
    public String getNombreArchivo() {
        return nombreArchivo;
    }

    /**
     * @param nombreArchivo the nombreArchivo to set
     */
    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }

    /**
     * @return the idFormato
     */
    public Long getIdFormato() {
        return idFormato;
    }

    /**
     * @param idFormato the idFormato to set
     */
    public void setIdFormato(Long idFormato) {
        this.idFormato = idFormato;
    }

    /**
     * @return the numVersion
     */
    public Long getNumVersion() {
        return numVersion;
    }

    /**
     * @param numVersion the numVersion to set
     */
    public void setNumVersion(Long numVersion) {
        this.numVersion = numVersion;
    }

    /**
     * @return the nombreUsuario
     */
    public String getNombreUsuario() {
        return nombreUsuario;
    }

    /**
     * @param nombreUsuario the nombreUsuario to set
     */
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
}
