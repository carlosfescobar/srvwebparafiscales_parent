package co.gov.ugpp.parafiscales.servicios.liquidador.srvAplValidadorNomina;

import co.gov.ugpp.parafiscales.validadorinformacion.ErrorValidacionArchivoTipo;
import co.gov.ugpp.parafiscales.validadorinformacion.ValidadorArchivos;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author jgonzalezt
 */
@WebService(serviceName = "SrvAplValidadorNomina")
public class SrvAplValidadorNomina {

    @EJB
    private ValidadorArchivos ejbObj;

    @WebMethod(operationName = "OpValidarNomina")
    public String OpValidarNomina(@WebParam(name = "parametros") ParametrosValidador parametros) {
        
        String archivo = parametros.getArchivo();
        Map infoNegocio = new HashMap();
        ArrayList<ErrorValidacionArchivoTipo> listaErrores;
        Long idEjecucion;
        InputStream inpExcel = null;
        
        try {
            byte[] binarioExcel = Base64.decode(archivo);
            inpExcel = new ByteArrayInputStream(binarioExcel);
        } catch (Base64DecodingException ex) {
            System.out.println("::VALIDADOR:: Exception: " + ex.toString());
        }
        
        Map objReturn = ejbObj.validarArchivo(
                inpExcel,
                parametros.getNombreArchivo(),
                parametros.getIdFormato(),
                parametros.getNumVersion(),
                null,
                parametros.getNombreUsuario(),
                null
        );
        
        return objReturn.get("idEjecucion").toString();
    }
}
