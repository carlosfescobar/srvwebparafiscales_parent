package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplActoAdministrativo;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface ActoAdministrativoFacade extends Serializable{
    
    ActoAdministrativoTipo buscarPorIdActoAdminsitrativo(String idActoAdministrativo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void crearActoAdministrativo(ActoAdministrativoTipo actoAdministrativo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    PagerData<ActoAdministrativoTipo> buscarPorCriteriosActoAdminsitrativo(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    void actualizarActo(ActoAdministrativoTipo actoAdministrativo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
}
