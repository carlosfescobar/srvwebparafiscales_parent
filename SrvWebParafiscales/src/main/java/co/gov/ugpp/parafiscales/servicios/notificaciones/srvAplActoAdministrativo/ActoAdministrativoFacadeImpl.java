package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplActoAdministrativo;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.ActoAdministrativoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.AportanteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.notificaciones.validacionactoadministrativotipo.v1.ValidacionActoAdministrativoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class ActoAdministrativoFacadeImpl extends AbstractFacade implements ActoAdministrativoFacade {

    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;
    @EJB
    private ExpedienteDao expedienteDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private DocumentoDao documentoEcmDao;

    AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();
    ActoAdministrativoAssembler actoAdministrativoAssembler = ActoAdministrativoAssembler.getInstance();

    @Override
    public ActoAdministrativoTipo buscarPorIdActoAdminsitrativo(String idActoAdministrativo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (StringUtils.isBlank(idActoAdministrativo)) {
            throw new AppException("Debe proporcionar los parametros de busqueda de los objetos a consultar");
        }

        ActoAdministrativo acto = actoAdministrativoDao.find(idActoAdministrativo);

        if (acto != null) {
            return actoAdministrativoAssembler.assembleServicio(acto);
        } else {
            return null;
        }
    }

    @Override
    public void crearActoAdministrativo(ActoAdministrativoTipo actoAdministrativoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (actoAdministrativoTipo == null || StringUtils.isBlank(actoAdministrativoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        ActoAdministrativo acto = actoAdministrativoDao.find(actoAdministrativoTipo.getIdActoAdministrativo());
        if (acto != null) {
            throw new AppException("El acto a crear ya existe");
        }
        if (actoAdministrativoTipo.getFecActoAdministrativo() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "La fecha del acto administrativo no puede ser nulo"));
        }
        ActoAdministrativo actoCrear = new ActoAdministrativo();
        actoCrear.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        actoCrear.setFechaCreacion(DateUtil.currentCalendar());
        assembleEntidad(actoCrear, actoAdministrativoTipo, errorTipoList, true);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        actoAdministrativoDao.create(actoCrear);
        ActoAdministrativo actoAdministrativoE = actoAdministrativoDao.find(actoAdministrativoTipo.getIdActoAdministrativo());

        if (actoAdministrativoTipo.getValidaciones() != null) {
            List<ValidacionActoAdministrativo> list = new ArrayList<ValidacionActoAdministrativo>();

            for (ValidacionActoAdministrativoTipo validacionTipo : actoAdministrativoTipo.getValidaciones()) {
                ValidacionActoAdministrativo validacion = new ValidacionActoAdministrativo();

                validacion.setActoAdministrativo(actoAdministrativoE);
                validacion.setDesObservacion(validacionTipo.getDesObservacion());

                if (StringUtils.isNotBlank(validacionTipo.getCodEstadoAceptacionEtapa())) {
                    ValorDominio codEstadoAceptacionEtapa = valorDominioDao.find(validacionTipo.getCodEstadoAceptacionEtapa());
                    if (codEstadoAceptacionEtapa == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEstadoAceptacionEtapa proporcionado no existe con el ID: " + validacionTipo.getCodEstadoAceptacionEtapa()));
                    } else {
                        validacion.setCodEstadoAceptacionEtapa(codEstadoAceptacionEtapa);
                    }
                }
                if (StringUtils.isNotBlank(validacionTipo.getCodEtapaValidacion())) {
                    ValorDominio codEtapaValidacion = valorDominioDao.find(validacionTipo.getCodEtapaValidacion());
                    if (codEtapaValidacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEtapaValidacion proporcionado no existe con el ID: " + validacionTipo.getCodEtapaValidacion()));
                    } else {
                        validacion.setCodEtapaValidacion(codEtapaValidacion);
                    }
                }
                list.add(validacion);
            }
            actoAdministrativoE.getValidaciones().addAll(list);
        }

        actoAdministrativoDao.edit(actoAdministrativoE);
    }

    public void assembleEntidad(final ActoAdministrativo acto, ActoAdministrativoTipo servicio, final List<ErrorTipo> errorTipoList, boolean crearActo) throws AppException {

        if (StringUtils.isNotBlank(servicio.getCodTipoActoAdministrativo())) {
            ValorDominio tipoActoAdministrativo = valorDominioDao.find(servicio.getCodTipoActoAdministrativo());
            if (tipoActoAdministrativo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio tipoActoAdministrativo proporcionado no existe con el ID: " + servicio.getCodTipoActoAdministrativo()));
            } else {
                acto.setCodTipoActoAdministrativo(tipoActoAdministrativo);
            }
        }
        if (!StringUtils.isBlank(servicio.getDescParteResolutivaActoAdministrativo())) {
            acto.setDescParteResolActoAdm(servicio.getDescParteResolutivaActoAdministrativo());
        }

        DocumentoEcm documento = null;
        if (servicio.getDocumento() != null) {
            documento = documentoEcmDao.find(servicio.getDocumento().getIdDocumento());
        }

        if (servicio.getExpediente() != null) {
            Expediente expediente = expedienteDao.find(servicio.getExpediente().getIdNumExpediente());
            if (expediente == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente proporcionado no existe con el ID: " + servicio.getExpediente().getIdNumExpediente()));
            } else {
                List<DocumentoEcm> documentos = new ArrayList<DocumentoEcm>();
                List<ExpedienteDocumentoEcm> documentosExpediente = expediente.getExpedienteDocumentoList();

                for (ExpedienteDocumentoEcm expedienteDocumentoEcm : documentosExpediente) {

                    if (documento != null && expedienteDocumentoEcm.getDocumentoEcm().getId().equals(documento.getId())) {
                        expedienteDocumentoEcm.setCodOrigen(acto.getCodTipoActoAdministrativo());
                    }

                    documentos.add(expedienteDocumentoEcm.getDocumentoEcm());
                }

                String idDoc = servicio.getDocumento() != null ? servicio.getDocumento().getIdDocumento() : "";
                if (StringUtils.isNotBlank(idDoc)) {
                    DocumentoEcm documentoEcm = documentoEcmDao.find(idDoc);
                    if (documentoEcm == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El documento no existe con el  ID: " + idDoc));
                    } else if (!documentos.contains(documentoEcm)) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El documento no esta asociado al expediente, aunque si existe como documento " + idDoc));
                    } else {
                        acto.setDocumentoEcm(documentoEcm);
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "El documento no puede ser nulo"));
                }

                acto.setExpediente(expediente);
            }
        } else {

            if (crearActo) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "El expediente es nulo"));
            }

        }

        if (servicio.getFecActoAdministrativo() != null) {
            acto.setFecActoAdministrativo(servicio.getFecActoAdministrativo());
        }
        if (!StringUtils.isBlank(servicio.getValVigencia())) {
            acto.setValVigencia(servicio.getValVigencia());
        }
        if (StringUtils.isNotBlank(servicio.getIdActoAdministrativo())) {
            acto.setId(servicio.getIdActoAdministrativo());
        }
        if (servicio.getValidaciones() != null) {

            for (ValidacionActoAdministrativoTipo validacionTipo : servicio.getValidaciones()) {
                if (StringUtils.isNotBlank(validacionTipo.getCodEstadoAceptacionEtapa())) {
                    ValorDominio codEstadoAceptacionEtapa = valorDominioDao.find(validacionTipo.getCodEstadoAceptacionEtapa());
                    if (codEstadoAceptacionEtapa == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEstadoAceptacionEtapa proporcionado no existe con el ID: " + validacionTipo.getCodEstadoAceptacionEtapa()));
                    }
                }
                if (StringUtils.isNotBlank(validacionTipo.getCodEtapaValidacion())) {
                    ValorDominio codEtapaValidacion = valorDominioDao.find(validacionTipo.getCodEtapaValidacion());
                    if (codEtapaValidacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEstadoAceptacionEtapa proporcionado no existe con el ID: " + validacionTipo.getCodEtapaValidacion()));
                    }
                }
            }
        }

        if (StringUtils.isNotBlank(servicio.getCodAreaOrigen())) {
            ValorDominio areaOrigen = valorDominioDao.find(servicio.getCodAreaOrigen());
            if (areaOrigen == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codAreaOrigen proporcionado no existe con el ID: " + servicio.getCodAreaOrigen()));
            } else {
                acto.setCodAreaOrigen(areaOrigen);
            }
        }
    }

    @Override
    public PagerData<ActoAdministrativoTipo> buscarPorCriteriosActoAdminsitrativo(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<ActoAdministrativo> pagerDataEntity
                = actoAdministrativoDao.findPorCriterios(parametro, criteriosOrdenamiento, valTamPagina, valNumPagina);

        final List<ActoAdministrativoTipo> actoAdministrativoList = actoAdministrativoAssembler.assembleCollectionServicio(
                pagerDataEntity.getData());

        return new PagerData<ActoAdministrativoTipo>(actoAdministrativoList, pagerDataEntity.getNumPages());
    }

    @Override
    public void actualizarActo(ActoAdministrativoTipo actoAdministrativoTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (actoAdministrativoTipo == null || StringUtils.isBlank(actoAdministrativoTipo.getIdActoAdministrativo())) {
            throw new AppException("Debe proporcionar el objeto a crear o el id del acto es vacio");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final ActoAdministrativo actoAdministrativo = actoAdministrativoDao.find(actoAdministrativoTipo.getIdActoAdministrativo());

        if (actoAdministrativo == null) {
            throw new AppException("No se encontrÃ³ un acto con el valor:" + actoAdministrativoTipo.getIdActoAdministrativo());
        }

        assembleEntidad(actoAdministrativo, actoAdministrativoTipo, errorTipoList, false);
        actoAdministrativo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());

        if (actoAdministrativoTipo.getValidaciones() != null) {
            List<ValidacionActoAdministrativo> list = new ArrayList<ValidacionActoAdministrativo>();

            for (ValidacionActoAdministrativoTipo validacionTipo : actoAdministrativoTipo.getValidaciones()) {
                ValidacionActoAdministrativo validacion = new ValidacionActoAdministrativo();

                validacion.setActoAdministrativo(actoAdministrativo);
                validacion.setDesObservacion(validacionTipo.getDesObservacion());

                if (validacionTipo.getCodEstadoAceptacionEtapa() != null) {
                    ValorDominio codEstadoAceptacionEtapa = valorDominioDao.find(validacionTipo.getCodEstadoAceptacionEtapa());
                    if (codEstadoAceptacionEtapa == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEstadoAceptacionEtapa proporcionado no existe con el ID: " + validacionTipo.getCodEstadoAceptacionEtapa()));
                    } else {
                        validacion.setCodEstadoAceptacionEtapa(codEstadoAceptacionEtapa);
                    }
                }
                if (validacionTipo.getCodEtapaValidacion() != null) {
                    ValorDominio codEtapaValidacion = valorDominioDao.find(validacionTipo.getCodEtapaValidacion());
                    if (codEtapaValidacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codEstadoAceptacionEtapa proporcionado no existe con el ID: " + validacionTipo.getCodEtapaValidacion()));
                    } else {
                        validacion.setCodEtapaValidacion(codEtapaValidacion);
                    }
                }
                list.add(validacion);
            }
            actoAdministrativo.setValidaciones(list);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        actoAdministrativo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        actoAdministrativo.setFechaModificacion(DateUtil.currentCalendar());
        actoAdministrativoDao.edit(actoAdministrativo);
    }

}
