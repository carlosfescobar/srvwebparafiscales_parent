package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplActoAdministrativo;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.MsjOpActualizarActoAdministrativoFallo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.MsjOpBuscarPorCriteriosActoAdministrativoFallo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.MsjOpBuscarPorIdActoAdministrativoFallo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.MsjOpCrearActoAdministrativoFallo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpActualizarActoAdministrativoRespTipo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorCriteriosActoAdministrativoRespTipo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorIdActoAdministrativoRespTipo;
import co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpCrearActoAdministrativoRespTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplActoAdministrativo",
        portName = "portSrvAplActoAdministrativoSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.PortSrvAplActoAdministrativoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplActoAdministrativo/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplActoAdministrativo extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplActoAdministrativo.class);

    @EJB
    private ActoAdministrativoFacade actoAdministrativoFacade;

    public co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpCrearActoAdministrativoRespTipo opCrearActoAdministrativo(co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpCrearActoAdministrativoSolTipo msjOpCrearActoAdministrativoSol) throws MsjOpCrearActoAdministrativoFallo {
        LOG.info("Op: opCrearActoAdministrativo ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearActoAdministrativoSol.getContextoTransaccional();

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            actoAdministrativoFacade.crearActoAdministrativo(
                    msjOpCrearActoAdministrativoSol.getActoAdministrativo(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opCrearActoAdministrativo ::: END");

        OpCrearActoAdministrativoRespTipo crearActoAdministrativoRespTipo = new OpCrearActoAdministrativoRespTipo();
        crearActoAdministrativoRespTipo.setContextoRespuesta(contextoRespuesta);

        return crearActoAdministrativoRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorIdActoAdministrativoRespTipo opBuscarPorIdActoAdministrativo(co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorIdActoAdministrativoSolTipo msjOpBuscarPorIdActoAdministrativoSol) throws MsjOpBuscarPorIdActoAdministrativoFallo {
        LOG.info("Op: opCrearActoAdministrativo ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdActoAdministrativoSol.getContextoTransaccional();

        ActoAdministrativoTipo actoAdministrativoTipo;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            actoAdministrativoTipo = actoAdministrativoFacade.buscarPorIdActoAdminsitrativo(
                    msjOpBuscarPorIdActoAdministrativoSol.getIdActoAdministrativo(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opCrearActoAdministrativo ::: END");

        OpBuscarPorIdActoAdministrativoRespTipo buscarPorIdActoAdministrativoRespTipo = new OpBuscarPorIdActoAdministrativoRespTipo();
        buscarPorIdActoAdministrativoRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdActoAdministrativoRespTipo.setActoAdministrativo(actoAdministrativoTipo);

        return buscarPorIdActoAdministrativoRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorCriteriosActoAdministrativoRespTipo opBuscarPorCriteriosActoAdministrativo(co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpBuscarPorCriteriosActoAdministrativoSolTipo msjOpBuscarPorCriteriosActoAdministrativoSol) throws MsjOpBuscarPorCriteriosActoAdministrativoFallo {
        LOG.info("Op: opBuscarPorCriteriosActoAdministrativo ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorCriteriosActoAdministrativoSol.getContextoTransaccional();

        final PagerData<ActoAdministrativoTipo> actos;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            actos = actoAdministrativoFacade.buscarPorCriteriosActoAdminsitrativo(
                    msjOpBuscarPorCriteriosActoAdministrativoSol.getParametro(),
                    contextoSolicitud.getCriteriosOrdenamiento(),
                    contextoSolicitud.getValTamPagina(),
                    contextoSolicitud.getValNumPagina(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opBuscarPorCriteriosActoAdministrativo ::: END");

        OpBuscarPorCriteriosActoAdministrativoRespTipo buscarPorCriteriosActoAdministrativoRespTipo = new OpBuscarPorCriteriosActoAdministrativoRespTipo();
        contextoRespuesta.setValCantidadPaginas(actos.getNumPages());
        buscarPorCriteriosActoAdministrativoRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosActoAdministrativoRespTipo.getActoAdministrativo().addAll(actos.getData());

        return buscarPorCriteriosActoAdministrativoRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpActualizarActoAdministrativoRespTipo opActualizarActoAdministrativo(co.gov.ugpp.notificaciones.srvaplactoadministrativo.v1.OpActualizarActoAdministrativoSolTipo msjOpActualizarActoAdministrativoSol) throws MsjOpActualizarActoAdministrativoFallo {
        LOG.info("Op: opActualizarActoAdministrativo ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarActoAdministrativoSol.getContextoTransaccional();

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            actoAdministrativoFacade.actualizarActo(msjOpActualizarActoAdministrativoSol.getActoAdministrativo(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarActoAdministrativoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opActualizarActoAdministrativo ::: END");

        OpActualizarActoAdministrativoRespTipo actualizarActoAdministrativoRespTipo = new OpActualizarActoAdministrativoRespTipo();
        actualizarActoAdministrativoRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarActoAdministrativoRespTipo;
    }

}
