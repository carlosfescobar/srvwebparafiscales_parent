package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplInteresado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;
import java.io.Serializable;
import javax.ejb.Local;

/**
 *
 * @author franzjr
 */
@Local
public interface InteresadoFacade extends Serializable {

    public InteresadoTipo buscarPorIdInteresado(IdentificacionTipo identificacion, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarInteresado(InteresadoTipo interesadoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

}
