package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplInteresado;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.InteresadoAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.populator.Populator;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class InteresadoFacadeImpl extends AbstractFacade implements InteresadoFacade {

    @EJB
    private PersonaDao personaDao;

    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private Populator populator;

    InteresadoAssembler intereresadoAssembler = InteresadoAssembler.getInstance();
    IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();

    @Override
    public InteresadoTipo buscarPorIdInteresado(IdentificacionTipo id, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        InteresadoTipo interesado = new InteresadoTipo();

        if (id != null) {
            if (StringUtils.isBlank(id.getCodTipoIdentificacion()) || StringUtils.isBlank(id.getValNumeroIdentificacion())) {
                throw new AppException("Debe proporcionar los ID de los objetos a consultar");
            }
            ValorDominio codTipoIdentificacion = valorDominioDao.find(id.getCodTipoIdentificacion());
            if (codTipoIdentificacion == null) {
                throw new AppException("El tipo de documento a consultar no existe con el ID: " + id.getCodTipoIdentificacion());
            }

            Identificacion identificacion = mapper.map(id, Identificacion.class);
            Persona persona = personaDao.findByIdentificacion(identificacion);
            if (persona != null) {
                interesado = intereresadoAssembler.assembleServicio(persona);
            } else {
                //TODO Buscar en RUA RUE y PILA
                interesado = new InteresadoTipo();
            }
        }

        return interesado;
    }

    @Override
    public void actualizarInteresado(InteresadoTipo interesadoTipo, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (interesadoTipo == null) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        Identificacion identificacion = null;
        if (interesadoTipo.getPersonaJuridica() != null) {
            identificacion = mapper.map(interesadoTipo.getPersonaJuridica().getIdPersona(), Identificacion.class);
        } else if (interesadoTipo.getPersonaNatural() != null) {
            identificacion = mapper.map(interesadoTipo.getPersonaNatural().getIdPersona(), Identificacion.class);
        }
        if (identificacion == null) {
            throw new AppException("Los campos estan vacios para la busqueda del interesado a actualizar");
        }

        Persona persona = personaDao.findByIdentificacion(identificacion);

        if (persona == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El interesado a actualizar no existe con el valor :" + identificacion.toString()));
            throw new AppException(errorTipoList);
        } else if (interesadoTipo.getPersonaNatural() != null) {
            persona = populator.populatePersonaFromPersonaNatural(interesadoTipo.getPersonaNatural(),
                    contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_NATURAL, errorTipoList);
        } else {
            persona = populator.poupulatePersonaFromPersonaJuridica(interesadoTipo.getPersonaJuridica(),
                    contextoTransaccionalTipo, persona, ClasificacionPersonaEnum.PERSONA_JURIDICA, errorTipoList);
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }
        
        persona.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        persona.setFechaModificacion(DateUtil.currentCalendar());
        personaDao.edit(persona);
    }

}
