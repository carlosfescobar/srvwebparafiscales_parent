package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplInteresado;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.notificaciones.srvaplinteresado.v1.MsjOpActualizarInteresadoFallo;
import co.gov.ugpp.notificaciones.srvaplinteresado.v1.MsjOpBuscarPorIdInteresadoFallo;
import co.gov.ugpp.notificaciones.srvaplinteresado.v1.OpActualizarInteresadoRespTipo;
import co.gov.ugpp.notificaciones.srvaplinteresado.v1.OpActualizarInteresadoSolTipo;
import co.gov.ugpp.notificaciones.srvaplinteresado.v1.OpBuscarPorIdInteresadoRespTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplInteresado",
        portName = "portSrvAplInteresadoSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplinteresado.v1.PortSrvAplInteresadoSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplInteresado/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplInteresado extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplInteresado.class);

    @EJB
    private InteresadoFacade interesadoFacade;

    public co.gov.ugpp.notificaciones.srvaplinteresado.v1.OpBuscarPorIdInteresadoRespTipo opBuscarPorIdInteresado(co.gov.ugpp.notificaciones.srvaplinteresado.v1.OpBuscarPorIdInteresadoSolTipo msjOpBuscarPorIdInteresadoSol) throws co.gov.ugpp.notificaciones.srvaplinteresado.v1.MsjOpBuscarPorIdInteresadoFallo {
        LOG.info("Op: opBuscarPorIdInteresado ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorIdInteresadoSol.getContextoTransaccional();

        InteresadoTipo interesadoTipo;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);

            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            interesadoTipo = interesadoFacade.buscarPorIdInteresado(msjOpBuscarPorIdInteresadoSol.getIdentificacion(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInteresadoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorIdInteresadoFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opBuscarPorIdInteresado ::: END");

        OpBuscarPorIdInteresadoRespTipo buscarPorIdInteresadoRespTipo = new OpBuscarPorIdInteresadoRespTipo();
        buscarPorIdInteresadoRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorIdInteresadoRespTipo.setInteresado(interesadoTipo);

        return buscarPorIdInteresadoRespTipo;
    }

    public OpActualizarInteresadoRespTipo opActualizarInteresado(OpActualizarInteresadoSolTipo msjOpActualizarInteresadoSolTipo) throws co.gov.ugpp.notificaciones.srvaplinteresado.v1.MsjOpActualizarInteresadoFallo {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = msjOpActualizarInteresadoSolTipo.getContextoTransaccional();
        final InteresadoTipo interesadoTipo = msjOpActualizarInteresadoSolTipo.getInteresado();

        final ContextoRespuestaTipo cr = new ContextoRespuestaTipo();

        try {
            this.initContextoRespuesta(contextoTransaccionalTipo, cr);
            ldapAuthentication.validateLdapAuthentication(contextoTransaccionalTipo.getIdUsuarioAplicacion(), contextoTransaccionalTipo.getValClaveUsuarioAplicacion());

            interesadoFacade.actualizarInteresado(interesadoTipo, contextoTransaccionalTipo);
        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarInteresadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarInteresadoFallo("Error ", ErrorUtil.buildFalloTipo(cr, ex), ex);
        }

        final OpActualizarInteresadoRespTipo resp = new OpActualizarInteresadoRespTipo();
        resp.setContextoRespuesta(cr);
        return resp;
    }
}
