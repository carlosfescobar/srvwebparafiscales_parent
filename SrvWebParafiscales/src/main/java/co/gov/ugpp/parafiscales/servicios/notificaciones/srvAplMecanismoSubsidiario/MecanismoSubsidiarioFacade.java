package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplMecanismoSubsidiario;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.schema.notificaciones.mecanismosubsidiariotipo.v1.MecanismoSubsidiarioTipo;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface MecanismoSubsidiarioFacade extends Serializable {
    
    public Long crearMecanismo(MecanismoSubsidiarioTipo mecanismoSubsidiario,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarMecanismo(MecanismoSubsidiarioTipo mecanismoSubsidiario,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public PagerData<MecanismoSubsidiarioTipo> buscarPorCriteriosMecanismo(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina,
            Long valNumPagina, ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;
    
    
}
