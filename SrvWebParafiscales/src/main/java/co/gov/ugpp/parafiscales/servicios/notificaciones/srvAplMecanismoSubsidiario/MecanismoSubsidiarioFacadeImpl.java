package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplMecanismoSubsidiario;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.MecanismoSubsidiarioDao;
import co.gov.ugpp.parafiscales.persistence.dao.NotificacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.MecanismoAnexoTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.MecanismoSubsidiario;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.DocumentoAssembler;
import co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplNotificacion.NotificacionFacade;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.schema.notificaciones.mecanismosubsidiariotipo.v1.MecanismoSubsidiarioTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class MecanismoSubsidiarioFacadeImpl extends AbstractFacade implements MecanismoSubsidiarioFacade {

    @EJB
    private MecanismoSubsidiarioDao mecanismoSubsidiarioDao;
    @EJB
    private DocumentoDao documentoEcmDao;
    @EJB
    private NotificacionDao notificacionDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private ArchivoDao archivoDao;
    @EJB
    private NotificacionFacade notificacionFacade;

    private DocumentoAssembler documentoAssembler = DocumentoAssembler.getInstance();

    @Override
    public Long crearMecanismo(MecanismoSubsidiarioTipo mecanismoSubsidiarioTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (mecanismoSubsidiarioTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        MecanismoSubsidiario mecanismo = new MecanismoSubsidiario();
        mecanismo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());

        if (mecanismoSubsidiarioTipo.getNotificaciones() != null) {
            if (mecanismoSubsidiarioTipo.getNotificaciones().isEmpty()) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.FALLO, "La lista de notificaciones no puede estar vacia"));
            }
        } else {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.FALLO, "La lista de notificaciones no puede ser nula"));
        }

        assembleEntidad(mecanismo, mecanismoSubsidiarioTipo, errorTipoList, contextoTransaccionalTipo);

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        mecanismo.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        mecanismo.setFechaCreacion(DateUtil.currentCalendar());

        Long idMecanismo = mecanismoSubsidiarioDao.create(mecanismo);
        return idMecanismo;
    }

    @Override
    public void actualizarMecanismo(MecanismoSubsidiarioTipo mecanismoSubsidiarioTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (mecanismoSubsidiarioTipo == null) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }
        if (mecanismoSubsidiarioTipo.getIdMecanismoSubsidiario() == null) {
            throw new AppException("Debe proporcionar el id objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final MecanismoSubsidiario mecanismo = mecanismoSubsidiarioDao.find(mecanismoSubsidiarioTipo.getIdMecanismoSubsidiario());

        if (mecanismo == null) {
            throw new AppException("No se encontró una mecanismo con el valor:" + mecanismoSubsidiarioTipo.getIdMecanismoSubsidiario());
        }

        assembleEntidad(mecanismo, mecanismoSubsidiarioTipo, errorTipoList, contextoTransaccionalTipo);
        mecanismo.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        mecanismo.setFechaModificacion(DateUtil.currentCalendar());

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        mecanismoSubsidiarioDao.edit(mecanismo);
    }

    public void assembleEntidad(final MecanismoSubsidiario mecanismo, MecanismoSubsidiarioTipo servicio, final List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (StringUtils.isNotBlank(servicio.getCodTipoEdictoAviso())) {
            ValorDominio codTipoEdictoAviso = valorDominioDao.find(servicio.getCodTipoEdictoAviso());
            if (codTipoEdictoAviso == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoEdictoAviso proporcionado no existe con el ID: " + servicio.getCodTipoEdictoAviso()));
            } else {
                mecanismo.setCodTipoEdictoAviso(codTipoEdictoAviso);
            }
        }
        if (servicio.getIdMecanismoSubsidiario() != null) {
            mecanismo.setIdMecanismoSubsidiario(servicio.getIdMecanismoSubsidiario());
        }
        if (!StringUtils.isBlank(servicio.getDescObservacionesDesfijado())) {
            mecanismo.setDescObservacionesDesfijado(servicio.getDescObservacionesDesfijado());
        }
        if (!StringUtils.isBlank(servicio.getDescObservacionesFijado())) {
            mecanismo.setDescObservacionesFijado(servicio.getDescObservacionesFijado());
        }
        if (servicio.getDocumento() != null) {
            DocumentoEcm documento = documentoEcmDao.find(servicio.getDocumento().getIdDocumento());
            if (documento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El documento proporcionado no existe con el ID: " + servicio.getDocumento().getIdDocumento()));
            } else {
                mecanismo.setDocumentoEcm(documento);
            }
        }
        if (servicio.getEsFijado() != null) {
            mecanismo.setEsFijado(Boolean.valueOf(servicio.getEsFijado()));
        }
        if (servicio.getEsPublicadoWeb() != null) {
            mecanismo.setEsPublicadoWeb(Boolean.valueOf(servicio.getEsPublicadoWeb()));
        }
        if (servicio.getFecCreacionMecanismoSubsidiario() != null) {
            mecanismo.setFecCreacionMecanismoSubsidi(servicio.getFecCreacionMecanismoSubsidiario());
        }
        if (servicio.getFecDesfijacionCartelera() != null) {
            mecanismo.setFecDesfijacionCartelera(servicio.getFecDesfijacionCartelera());
        }
        if (servicio.getFecFijacionCartelera() != null) {
            mecanismo.setFecFijacionCartelera(servicio.getFecFijacionCartelera());
        }
        if (servicio.getFecPublicacionWeb() != null) {
            mecanismo.setFecPublicacionWeb(servicio.getFecPublicacionWeb());
        }
        if (servicio.getFecDespublicacionWeb() != null) {
            mecanismo.setFecDespublicacionWeb(servicio.getFecDespublicacionWeb());
        }
        if (mecanismo.getFecDespublicacionWeb() != null) {
            if (mecanismo.getFecPublicacionWeb() != null) {
                if (mecanismo.getFecPublicacionWeb().after(mecanismo.getFecDespublicacionWeb())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "La fecha de despublicación debe ser superior a la fecha de publicación"));
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La fecha de despublicacion web no se puede registar ya que la fecha de publicacion es nula"));
            }
        }
        if (servicio.getNotificaciones() != null) {
            if (!servicio.getNotificaciones().isEmpty()) {
                List<Notificacion> listNotificaion = new ArrayList<Notificacion>();
                for (NotificacionTipo notificacionTipo : servicio.getNotificaciones()) {
                    Notificacion notificacion = notificacionDao.find(Long.parseLong(notificacionTipo.getIdNotificacion()));
                    if (notificacion == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "La notificacion proporcionada no existe con el ID: " + notificacionTipo.getIdNotificacion()));
                    } else {
                        listNotificaion.add(notificacion);
                    }
                }
                mecanismo.setNotificacionList(listNotificaion);
            }
        }
        // Se Obtienen los Archivos Temporales
        if (servicio.getArchivosTemporales() != null && !servicio.getArchivosTemporales().isEmpty()) {

            for (ParametroValoresTipo archivoTemporal : servicio.getArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivoTemporal.getIdLlave())) {
                    ValorDominio codTipoAnexo = valorDominioDao.find(archivoTemporal.getIdLlave());
                    if (codTipoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoAnexo con el ID: " + archivoTemporal.getIdLlave()));
                    } else {
                        for (ParametroTipo archivo : archivoTemporal.getValValor()) {
                            if (StringUtils.isNotBlank(archivo.getIdLlave())) {
                                Archivo anexo = archivoDao.find(Long.valueOf(archivo.getIdLlave()));
                                if (anexo == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró un archivo con elID: " + archivo.getIdLlave()));
                                } else {
                                    MecanismoAnexoTemporal mecanismoAnexoTemporal = new MecanismoAnexoTemporal();
                                    mecanismoAnexoTemporal.setArchivo(anexo);
                                    mecanismoAnexoTemporal.setCodTipoAnexo(codTipoAnexo);
                                    mecanismoAnexoTemporal.setMecanismoSubsidiario(mecanismo);
                                    mecanismoAnexoTemporal.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                    mecanismo.getAnexosTemporales().add(mecanismoAnexoTemporal);
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoAnexo")));
                }

            }

        }
        if (StringUtils.isNotBlank(servicio.getCodEstadoMecanismoSubsidiario())) {

            ValorDominio estadoMecanismoSubsidirario = valorDominioDao.find(servicio.getCodEstadoMecanismoSubsidiario());
            if (estadoMecanismoSubsidirario == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un  con estadoMecanismoSubsidirario con el ID: " + servicio.getCodEstadoMecanismoSubsidiario()));
            } else {
                mecanismo.setCodEstadoMecanismoSubsidiario(estadoMecanismoSubsidirario);
            }

        }
    }

    @Override
    public PagerData<MecanismoSubsidiarioTipo> buscarPorCriteriosMecanismo(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        final PagerData<MecanismoSubsidiario> pagerDataEntity
                = mecanismoSubsidiarioDao.findPorCriterios(parametro, criteriosOrdenamiento, valTamPagina, valNumPagina);

        final List<MecanismoSubsidiarioTipo> mecanimosTipo = assembleCollectionServicio(pagerDataEntity.getData());

        return new PagerData<MecanismoSubsidiarioTipo>(mecanimosTipo, pagerDataEntity.getNumPages());
    }

    public List<MecanismoSubsidiarioTipo> assembleCollectionServicio(Collection<MecanismoSubsidiario> collection) {

        List<MecanismoSubsidiarioTipo> arrayL = new ArrayList<MecanismoSubsidiarioTipo>();

        for (MecanismoSubsidiario parcial : collection) {

            arrayL.add(assembleServicio(parcial));
        }
        return arrayL;

    }

    public MecanismoSubsidiarioTipo assembleServicio(MecanismoSubsidiario entidad) {

        MecanismoSubsidiarioTipo mecanismoServicio = new MecanismoSubsidiarioTipo();

        if (entidad.getId() != null) {
            mecanismoServicio.setIdMecanismoSubsidiario(entidad.getId());
        }
        if (StringUtils.isNotBlank(entidad.getDescObservacionesDesfijado())) {
            mecanismoServicio.setDescObservacionesDesfijado(entidad.getDescObservacionesDesfijado());
        }
        if (StringUtils.isNotBlank(entidad.getDescObservacionesFijado())) {
            mecanismoServicio.setDescObservacionesFijado(entidad.getDescObservacionesFijado());
        }
        if (entidad.getDocumentoEcm() != null) {
            mecanismoServicio.setDocumento(documentoAssembler.assembleServicio(entidad.getDocumentoEcm()));
        }
        if (entidad.getEsFijado() != null) {
            mecanismoServicio.setEsFijado(String.valueOf(entidad.getEsFijado()));
        }
        if (entidad.getEsPublicadoWeb() != null) {
            mecanismoServicio.setEsPublicadoWeb(String.valueOf(entidad.getEsPublicadoWeb()));
        }
        if (entidad.getFecCreacionMecanismoSubsidi() != null) {
            mecanismoServicio.setFecCreacionMecanismoSubsidiario(entidad.getFecCreacionMecanismoSubsidi());
        }
        if (entidad.getFecDesfijacionCartelera() != null) {
            mecanismoServicio.setFecDesfijacionCartelera(entidad.getFecDesfijacionCartelera());
        }
        if (entidad.getFecPublicacionWeb() != null) {
            mecanismoServicio.setFecDespublicacionWeb(entidad.getFecPublicacionWeb());
        }
        if (entidad.getFecFijacionCartelera() != null) {
            mecanismoServicio.setFecFijacionCartelera(entidad.getFecFijacionCartelera());
        }
        if (entidad.getFecPublicacionWeb() != null) {
            mecanismoServicio.setFecPublicacionWeb(entidad.getFecPublicacionWeb());
        }
        if (entidad.getNotificacionList() != null) {
            mecanismoServicio.getNotificaciones().addAll(notificacionFacade.assembleCollectionServicio(entidad.getNotificacionList()));
        }
        if (entidad.getCodTipoEdictoAviso() != null) {
            mecanismoServicio.setCodTipoEdictoAviso(entidad.getCodTipoEdictoAviso().getId());
        }
        if (entidad.getAnexosTemporales() != null && !entidad.getAnexosTemporales().isEmpty()) {
            List<ParametroValoresTipo> anexosTemporales = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> anexosTemporalesMecanismo = new HashMap<String, List<ParametroTipo>>();
            for (MecanismoAnexoTemporal anexoTemporal : entidad.getAnexosTemporales()) {
                List<ParametroTipo> archivosMecanismo = anexosTemporalesMecanismo.get(anexoTemporal.getCodTipoAnexo().getId());
                if (archivosMecanismo == null) {
                    archivosMecanismo = new ArrayList<ParametroTipo>();
                }
                ParametroTipo anexoMecanismo = new ParametroTipo();
                anexoMecanismo.setIdLlave(anexoTemporal.getArchivo().getId().toString());
                anexoMecanismo.setValValor(anexoTemporal.getArchivo().getValNombreArchivo());
                archivosMecanismo.add(anexoMecanismo);
                anexosTemporalesMecanismo.put(anexoTemporal.getCodTipoAnexo().getId(), archivosMecanismo);
            }
            for (Map.Entry<String, List<ParametroTipo>> entry : anexosTemporalesMecanismo.entrySet()) {
                ParametroValoresTipo anexo = new ParametroValoresTipo();
                anexo.setIdLlave(entry.getKey());
                anexo.getValValor().addAll(entry.getValue());
                anexosTemporales.add(anexo);
            }
            mecanismoServicio.getArchivosTemporales().addAll(anexosTemporales);
        }

        if (entidad.getCodEstadoMecanismoSubsidiario() != null) {
            mecanismoServicio.setCodEstadoMecanismoSubsidiario(entidad.getCodEstadoMecanismoSubsidiario().getId());
            mecanismoServicio.setDescEstadoMecanismoSubsidiario(entidad.getCodEstadoMecanismoSubsidiario().getNombre());
        }
        
        return mecanismoServicio;
    }

}
