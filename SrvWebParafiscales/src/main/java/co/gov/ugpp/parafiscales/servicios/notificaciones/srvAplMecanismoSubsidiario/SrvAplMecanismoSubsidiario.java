package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplMecanismoSubsidiario;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.MsjOpActualizarMecanismoSubsidiarioFallo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.MsjOpBuscarPorCriteriosMecanismoSubsidiarioFallo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.MsjOpCrearMecanismoSubsidiarioFallo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpActualizarMecanismoSubsidiarioRespTipo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo;
import co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpCrearMecanismoSubsidiarioRespTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.mecanismosubsidiariotipo.v1.MecanismoSubsidiarioTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplMecanismoSubsidiario",
        portName = "portSrvAplMecanismoSubsidiarioSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.PortSrvAplMecanismoSubsidiarioSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplMecanismoSubsidiario/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplMecanismoSubsidiario extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplMecanismoSubsidiario.class);

    @EJB
    private MecanismoSubsidiarioFacade mecanismoFacade;

    public co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpCrearMecanismoSubsidiarioRespTipo opCrearMecanismoSubsidiario(co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpCrearMecanismoSubsidiarioSolTipo msjOpCrearMecanismoSubsidiarioSol) throws MsjOpCrearMecanismoSubsidiarioFallo {
        LOG.info("Op: opCrearMecanismoSubsidiario ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearMecanismoSubsidiarioSol.getContextoTransaccional();
        Long idMecanismo;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            idMecanismo = mecanismoFacade.crearMecanismo(
                    msjOpCrearMecanismoSubsidiarioSol.getMecanismoSubsidiario(), contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        LOG.info("Op: opCrearMecanismoSubsidiario ::: END");

        OpCrearMecanismoSubsidiarioRespTipo crearMecanismoSubsidiarioRespTipo = new OpCrearMecanismoSubsidiarioRespTipo();
        crearMecanismoSubsidiarioRespTipo.setContextoRespuesta(contextoRespuesta);
        crearMecanismoSubsidiarioRespTipo.setIdMecanismoSubsidiario(String.valueOf(idMecanismo));

        return crearMecanismoSubsidiarioRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo opBuscarPorCriteriosMecanismoSubsidiario(co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpBuscarPorCriteriosMecanismoSubsidiarioSolTipo msjOpBuscarPorCriteriosMecanismoSubsidiarioSol) throws MsjOpBuscarPorCriteriosMecanismoSubsidiarioFallo {
        LOG.info("Op: opBuscarPorCriteriosMecanismoSubsidiario ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorCriteriosMecanismoSubsidiarioSol.getContextoTransaccional();

        final PagerData<MecanismoSubsidiarioTipo> mecanismos;

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            mecanismos = mecanismoFacade.buscarPorCriteriosMecanismo(
                    msjOpBuscarPorCriteriosMecanismoSubsidiarioSol.getParametro(),
                    contextoSolicitud.getCriteriosOrdenamiento(),
                    contextoSolicitud.getValTamPagina(),
                    contextoSolicitud.getValNumPagina(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opBuscarPorCriteriosMecanismoSubsidiario ::: END");

        OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo buscarPorCriteriosMecanismoSubsidiarioRespTipo = new OpBuscarPorCriteriosMecanismoSubsidiarioRespTipo();
        contextoRespuesta.setValCantidadPaginas(mecanismos.getNumPages());
        buscarPorCriteriosMecanismoSubsidiarioRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosMecanismoSubsidiarioRespTipo.getMecanismoSubsidiario().addAll(mecanismos.getData());

        return buscarPorCriteriosMecanismoSubsidiarioRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpActualizarMecanismoSubsidiarioRespTipo opActualizarMecanismoSubsidiario(co.gov.ugpp.notificaciones.srvaplmecanismosubsidiario.v1.OpActualizarMecanismoSubsidiarioSolTipo msjOpActualizarMecanismoSubsidiarioSol) throws MsjOpActualizarMecanismoSubsidiarioFallo {
        LOG.info("Op: opActualizarMecanismoSubsidiario ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarMecanismoSubsidiarioSol.getContextoTransaccional();

        try {

            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            mecanismoFacade.actualizarMecanismo(
                    msjOpActualizarMecanismoSubsidiarioSol.getMecanismoSubsidiario(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarMecanismoSubsidiarioFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opActualizarMecanismoSubsidiario ::: END");

        OpActualizarMecanismoSubsidiarioRespTipo actualizarMecanismoSubsidiarioRespTipo = new OpActualizarMecanismoSubsidiarioRespTipo();
        actualizarMecanismoSubsidiarioRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarMecanismoSubsidiarioRespTipo;
    }

}
