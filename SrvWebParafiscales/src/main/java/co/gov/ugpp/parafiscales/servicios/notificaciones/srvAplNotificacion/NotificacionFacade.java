package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplNotificacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author franzjr
 */
public interface NotificacionFacade extends Serializable {

    public Long crearNotificacion(NotificacionTipo notificacion,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public void actualizarNotificacion(NotificacionTipo notificacion,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException;

    public List<NotificacionTipo> assembleCollectionServicio(Collection<Notificacion> collection);

    public PagerData<NotificacionTipo> buscarPorCriteriosNotificacion(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento,
            Long valTamPagina, Long valNumPagina) throws AppException;    
    
}
