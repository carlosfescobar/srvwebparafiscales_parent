package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplNotificacion;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.persistence.dao.ActoAdministrativoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ArchivoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDao;
import co.gov.ugpp.parafiscales.persistence.dao.NotificacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosCodnot;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosIdnot;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.parafiscales.persistence.entity.NotificacionAnexoTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.servicios.helpers.ActoAdministrativoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.AportanteAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.IdentificacionAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.InteresadoAssembler;
import co.gov.ugpp.parafiscales.servicios.helpers.PersonaAssembler;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.PropsReader;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author franzjr
 */
@Stateless
@TransactionManagement
public class NotificacionFacadeImpl extends AbstractFacade implements NotificacionFacade {

    @EJB
    private NotificacionDao notificacionDao;
    @EJB
    private ActoAdministrativoDao actoAdministrativoDao;
    @EJB
    private ValorDominioDao valorDominioDao;
    @EJB
    private ExpedienteDao expedienteDao;
    @EJB
    private PersonaDao personaDao;
    @EJB
    private DocumentoDao documentoDao;
    @EJB
    private ArchivoDao archivoDao;

    private static final byte[] VAL_CONTENIDO = new byte[]{(byte) 0xe0, 0x4f, (byte) 0xd0,
        0x20, (byte) 0xea, 0x3a, 0x69, 0x10, (byte) 0xa2, (byte) 0xd8, 0x08, 0x00, 0x2b,
        0x30, 0x30, (byte) 0x9d};

    private AportanteAssembler aportanteAssembler = AportanteAssembler.getInstance();
    private IdentificacionAssembler identificacionAssembler = IdentificacionAssembler.getInstance();
    private InteresadoAssembler interesadoAssembler = InteresadoAssembler.getInstance();
    private PersonaAssembler personaAssembler = PersonaAssembler.getInstance();
    private ActoAdministrativoAssembler actoAdministrativoAssembler = ActoAdministrativoAssembler.getInstance();

    @Override
    public Long crearNotificacion(NotificacionTipo notificacionTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (notificacionTipo == null) {
            throw new AppException("Debe proporcionar el objeto a crear");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        if (StringUtils.isNotBlank(notificacionTipo.getIdNotificacion())) {
            throw new AppException("No es necesario agregar el id de la notificacion al crear");
        }

        Notificacion notificacion = new Notificacion();
        notificacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());
        assembleEntidad(notificacion, notificacionTipo, errorTipoList, contextoTransaccionalTipo);

        if (notificacionTipo.getActoAdministrativo() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El Acto Administrativo no puede ser nulo"));
        }

        if (notificacionTipo.getInteresado() == null) {

            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "El Interesado no puede ser nulo"));
        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        notificacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
        notificacion.setFechaCreacion(DateUtil.currentCalendar());

        Long idNot = notificacionDao.create(notificacion);
        Notificacion not = notificacionDao.find(idNot);

        if (notificacionTipo.getIdDocumentosAnexos() != null) {
            List<DocumentosAnexosIdnot> list = new ArrayList<DocumentosAnexosIdnot>();

            for (String idDoc : notificacionTipo.getIdDocumentosAnexos()) {
                DocumentosAnexosIdnot docAnexoId = new DocumentosAnexosIdnot();
                if (StringUtils.isNotBlank(idDoc)) {
                    DocumentoEcm documentoEcm = documentoDao.find(idDoc);
                    if (documentoEcm == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El documentoIdAnexo no existe con el  ID: " + idDoc));
                    } else {
                        docAnexoId.setIdDocumentosAnexos(idDoc);
                        docAnexoId.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        docAnexoId.setFechaCreacion(DateUtil.currentCalendar());
                    }
                }
                docAnexoId.setIdNotificacion(not);
                docAnexoId.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                docAnexoId.setFechaCreacion(DateUtil.currentCalendar());
                list.add(docAnexoId);
            }
            not.setDocumentosAnexosIdnotList(list);

        }
        if (notificacionTipo.getCodDocumentosAnexosNotificacionPersonal() != null) {
            List<DocumentosAnexosCodnot> list = new ArrayList<DocumentosAnexosCodnot>();

            for (String codDoc : notificacionTipo.getCodDocumentosAnexosNotificacionPersonal()) {
                DocumentosAnexosCodnot docAnexoCod = new DocumentosAnexosCodnot();

                if (StringUtils.isNotBlank(codDoc)) {
                    ValorDominio codDocumentoAnexo = valorDominioDao.find(codDoc);
                    if (codDocumentoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codDocumentoAnexo proporcionado no existe con el ID: " + codDoc));
                    } else {
                        docAnexoCod.setDocumentosAnexosNotif(codDoc);
                        docAnexoCod.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        docAnexoCod.setFechaCreacion(DateUtil.currentCalendar());
                    }
                }

                docAnexoCod.setIdNotificacion(not);
                docAnexoCod.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                docAnexoCod.setFechaCreacion(DateUtil.currentCalendar());
                list.add(docAnexoCod);
            }
            not.setDocumentosAnexosCodnotList(list);

        }

        notificacionDao.edit(not);

        return idNot;
    }

    @Override
    public void actualizarNotificacion(NotificacionTipo notificacionTipo,
            ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (notificacionTipo == null || StringUtils.isBlank(notificacionTipo.getIdNotificacion())) {
            throw new AppException("Debe proporcionar el objeto a actualizar");
        }

        final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

        final Notificacion notificacion = notificacionDao.find(Long.valueOf(notificacionTipo.getIdNotificacion()));

        if (notificacion == null) {
            throw new AppException("No se encontró una notificacion con el valor:" + notificacionTipo.getIdNotificacion());
        }

        assembleEntidad(notificacion, notificacionTipo, errorTipoList, contextoTransaccionalTipo);
        notificacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuarioAplicacion());

        if (notificacionTipo.getIdDocumentosAnexos() != null) {
            List<DocumentosAnexosIdnot> list = new ArrayList<DocumentosAnexosIdnot>();

            for (String idDoc : notificacionTipo.getIdDocumentosAnexos()) {
                DocumentosAnexosIdnot docAnexoId = new DocumentosAnexosIdnot();
                if (StringUtils.isNotBlank(idDoc)) {
                    DocumentoEcm documentoEcm = documentoDao.find(idDoc);
                    if (documentoEcm == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El documentoIdAnexo no existe con el  ID: " + idDoc));
                    } else {
                        docAnexoId.setIdDocumentosAnexos(idDoc);
                        docAnexoId.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                        docAnexoId.setFechaModificacion(DateUtil.currentCalendar());
                    }
                }
                docAnexoId.setIdNotificacion(notificacion);
                docAnexoId.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                docAnexoId.setFechaModificacion(DateUtil.currentCalendar());
                list.add(docAnexoId);
            }
            notificacion.setDocumentosAnexosIdnotList(list);

        }
        if (notificacionTipo.getCodDocumentosAnexosNotificacionPersonal() != null) {
            List<DocumentosAnexosCodnot> list = new ArrayList<DocumentosAnexosCodnot>();

            for (String codDoc : notificacionTipo.getCodDocumentosAnexosNotificacionPersonal()) {
                DocumentosAnexosCodnot docAnexoCod = new DocumentosAnexosCodnot();

                if (StringUtils.isNotBlank(codDoc)) {
                    ValorDominio codDocumentoAnexo = valorDominioDao.find(codDoc);
                    if (codDocumentoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codDocumentoAnexo proporcionado no existe con el ID: " + codDoc));
                    } else {
                        docAnexoCod.setDocumentosAnexosNotif(codDoc);
                        docAnexoCod.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                        docAnexoCod.setFechaModificacion(DateUtil.currentCalendar());
                    }
                }

                docAnexoCod.setIdNotificacion(notificacion);
                docAnexoCod.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                docAnexoCod.setFechaModificacion(DateUtil.currentCalendar());
                list.add(docAnexoCod);
            }
            notificacion.setDocumentosAnexosCodnotList(list);

        }

        if (!errorTipoList.isEmpty()) {
            throw new AppException(errorTipoList);
        }

        notificacion.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        notificacion.setFechaModificacion(DateUtil.currentCalendar());

        notificacionDao.edit(notificacion);
    }

    public void assembleEntidad(final Notificacion notificacion, NotificacionTipo servicio, final List<ErrorTipo> errorTipoList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        ActoAdministrativo actoAdminNot = null;
        if (servicio.getActoAdministrativo() != null) {
            try {
                actoAdminNot = actoAdministrativoDao.find(servicio.getActoAdministrativo().getIdActoAdministrativo());
                if (actoAdminNot == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El actoAdminNot proporcionado no existe con el ID: " + servicio.getActoAdministrativo().getIdActoAdministrativo()));
                } else {
                    notificacion.setActoAdministrativo(actoAdminNot);
                }
            } catch (NumberFormatException e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Error al intentar relacionar el actoAdministrativo: Al intentar parsear el id: " + e.getMessage()));
            } catch (AppException e) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Error al intentar relacionar el actoAdministrativo: " + e.getMessage()));
            }

        }
        if (servicio.getCodCalidadNotificado() != null) {
            ValorDominio codCalidadNotificado = valorDominioDao.find(servicio.getCodCalidadNotificado());
            if (codCalidadNotificado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codCalidadNotificado proporcionado no existe con el ID: " + servicio.getCodCalidadNotificado()));
            } else {
                notificacion.setCodCalidadNotificado(codCalidadNotificado);
            }
        }
        if (servicio.getCodTipoNotificacion() != null) {
            ValorDominio codTipoNotificacion = valorDominioDao.find(servicio.getCodTipoNotificacion());
            if (codTipoNotificacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoNotificacion proporcionado no existe con el ID: " + servicio.getCodTipoNotificacion()));
            } else {
                notificacion.setCodTipoNotificacion(codTipoNotificacion);
            }
        }
        if (servicio.getArchivosTemporales() != null
                && !servicio.getArchivosTemporales().isEmpty()) {
            for (ParametroValoresTipo archivoTemporal : servicio.getArchivosTemporales()) {
                if (StringUtils.isNotBlank(archivoTemporal.getIdLlave())) {
                    final ValorDominio codTipoAnexo = valorDominioDao.find(archivoTemporal.getIdLlave());
                    if (codTipoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró codTipoAnexo con el ID: " + archivoTemporal.getIdLlave()));
                    } else {
                        for (ParametroTipo archivo : archivoTemporal.getValValor()) {
                            if (StringUtils.isNotBlank(archivo.getIdLlave())) {
                                Archivo anexo = archivoDao.find(Long.valueOf(archivo.getIdLlave()));
                                if (anexo == null) {
                                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                            "No se encontró Archivo con el ID: " + archivo.getIdLlave()));
                                } else {
                                    NotificacionAnexoTemporal notificacionAnexoTemporal = new NotificacionAnexoTemporal();
                                    notificacionAnexoTemporal.setNotificacion(notificacion);
                                    notificacionAnexoTemporal.setArchivo(anexo);
                                    notificacionAnexoTemporal.setCodTipoAnexo(codTipoAnexo);
                                    notificacionAnexoTemporal.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                                    notificacion.getAnexosTemporales().add(notificacionAnexoTemporal);
                                }
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            PropsReader.getKeyParam("msgCampoRequerido", "codTipoAnexo")));
                }
            }
        }

        if (!StringUtils.isBlank(servicio.getDescActaNotificacionValidadaObservaciones())) {
            notificacion.setDesActaNotififcacionValidad(servicio.getDescActaNotificacionValidadaObservaciones());
        }
        if (!StringUtils.isBlank(servicio.getDescCalidadNotificado())) {
            notificacion.setDesCalidadNotificado(servicio.getDescCalidadNotificado());
        }
        if (!StringUtils.isBlank(servicio.getDescDevolucionActoAdministrativoObservaciones())) {
            notificacion.setDesDevolucionActoAdministr(servicio.getDescDevolucionActoAdministrativoObservaciones());
        }
        if (servicio.getDocActaNotificacionPersonal() != null) {
            notificacion.setDocActaNotPresoId(Long.parseLong(servicio.getDocActaNotificacionPersonal().getIdArchivo()));
        }
        if (servicio.getEsActaNotificacionValidada() != null) {
            notificacion.setEsActaNotificacionValidada(Boolean.valueOf(servicio.getEsActaNotificacionValidada()));
        }
        if (servicio.getEsDevolucionActoAdministrativoAprobada() != null) {
            notificacion.setEsDevlucionActoAdministrat(Boolean.valueOf(servicio.getEsDevolucionActoAdministrativoAprobada()));
        }
        if (servicio.getEsMecanismoSubsidiario() != null) {
            notificacion.setEsMecanismoSubsidiario(Boolean.valueOf(servicio.getEsMecanismoSubsidiario()));
        }
        if (StringUtils.isNotBlank(servicio.getCodTipoMecanismoSubsidiario())) {
            ValorDominio codTipoMecanismoSubsidiario = valorDominioDao.find(servicio.getCodTipoMecanismoSubsidiario());
            if (codTipoMecanismoSubsidiario == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio CodTipoMecanismoSubsidiario proporcionado no existe con el ID: " + servicio.getCodTipoMecanismoSubsidiario()));
            } else {
                notificacion.setCodTipoMecanismoSubsidiario(codTipoMecanismoSubsidiario);
            }
        }
        if (servicio.getFecNotificacion() != null) {
            notificacion.setFecNotificacion(servicio.getFecNotificacion());
        }
        if (servicio.getIdNotificacion() != null) {
            notificacion.setId(Long.parseLong(servicio.getIdNotificacion()));
        }
        if (servicio.getIdRadicadoDocumentosAnexosNotificacion() != null) {
            notificacion.setIdRadicadoDocumentosAnexos(servicio.getIdRadicadoDocumentosAnexosNotificacion());
        }
        if (servicio.getIdRadicadoDocumentosAnexosNotificacion() != null) {
            notificacion.setIdRadicadoDocumentosAnexos(servicio.getIdRadicadoDocumentosAnexosNotificacion());
        }
        if (servicio.getInteresado() != null) {
            Persona persona = null;
            if (servicio.getInteresado().getPersonaJuridica() != null) {
                Identificacion identificacion = mapper.map(servicio.getInteresado().getPersonaJuridica().getIdPersona(), Identificacion.class);
                persona = personaDao.findByIdentificacion(identificacion);
            } else if (servicio.getInteresado().getPersonaNatural() != null) {
                Identificacion identificacion = mapper.map(servicio.getInteresado().getPersonaNatural().getIdPersona(), Identificacion.class);
                persona = personaDao.findByIdentificacion(identificacion);
            }
            if (persona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La persona asociada al interesado asociado a la notificacion, no existe "));
            } else {
                notificacion.setInteresadoId(persona);
            }
        }
        if (servicio.getPersonaNaturalNotificada() != null) {
            Persona persona = null;
            Identificacion identificacion = mapper.map(servicio.getPersonaNaturalNotificada().getIdPersona(), Identificacion.class);
            persona = personaDao.findByIdentificacion(identificacion);
            if (persona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "La persona asociada a la notificacion, no existe "));
            } else {
                notificacion.setPersona(persona);
            }
        }

        if (servicio.getIdDocumentosAnexos() != null && actoAdminNot != null) {

            if (actoAdminNot.getExpediente() == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El expediente no existe o El acto Adminsitrativo no tiene expediente: idActoAdminsitrativo: " + (servicio.getActoAdministrativo() != null ? servicio.getActoAdministrativo().getIdActoAdministrativo() : "nulo")));
            } else {

                Expediente expediente = expedienteDao.find(actoAdminNot.getExpediente().getId());

                if (expediente != null) {
                    List<DocumentoEcm> documentos = new ArrayList<DocumentoEcm>();
                    List<ExpedienteDocumentoEcm> documentosExpediente = expediente.getExpedienteDocumentoList();

                    for (ExpedienteDocumentoEcm expedienteDocumentoEcm : documentosExpediente) {
                        documentos.add(expedienteDocumentoEcm.getDocumentoEcm());
                    }

                    for (String idDoc : servicio.getIdDocumentosAnexos()) {
                        if (StringUtils.isNotBlank(idDoc)) {
                            DocumentoEcm documentoEcm = documentoDao.find(idDoc);
                            if (documentoEcm == null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "El documentoIdAnexo no existe con el  ID: " + idDoc));
                            } else if (!documentos.contains(documentoEcm)) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "El documentoIdAnexo no esta asociado al expediente, aunque si existe como documento " + idDoc));
                            }
                        }
                    }
                } else {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El expediente no existe o El acto Adminsitrativo no tiene expediente: idActoAdminsitrativo: " + (servicio.getActoAdministrativo() != null ? servicio.getActoAdministrativo().getIdActoAdministrativo() : "nulo")));

                }
            }

        }
        if (servicio.getCodDocumentosAnexosNotificacionPersonal() != null) {
            for (String codDoc : servicio.getCodDocumentosAnexosNotificacionPersonal()) {
                if (StringUtils.isNotBlank(codDoc)) {
                    ValorDominio codDocumentoAnexo = valorDominioDao.find(codDoc);
                    if (codDocumentoAnexo == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "El valor dominio codDocumentoAnexo proporcionado no existe con el ID: " + codDoc));
                    }
                }
            }
        }

        //INICIO CAMBIOS REALIZADOS POR EVERIS 30/07/2014
        if (StringUtils.isNotBlank(servicio.getCodEstadoNotificacion())) {
            ValorDominio codEstadoNotificacion = valorDominioDao.find(servicio.getCodEstadoNotificacion());
            if (codEstadoNotificacion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codEstadoNotificacion proporcionado no existe con el ID: " + servicio.getCodEstadoNotificacion()));
            } else {
                notificacion.setCodEstadoNotificacion(codEstadoNotificacion);
            }
        }
        if (StringUtils.isNotBlank(servicio.getCodTipoNotificacionDefinitiva())) {
            ValorDominio codTipoNotificacionDefinitiva = valorDominioDao.find(servicio.getCodTipoNotificacionDefinitiva());
            if (codTipoNotificacionDefinitiva == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "El valor dominio codTipoNotificacionDefinitiva proporcionado no existe con el ID: " + servicio.getCodTipoNotificacionDefinitiva()));
            } else {
                notificacion.setCodTipoNotificacionDefinitva(codTipoNotificacionDefinitiva);
            }
        }

        //FIN CAMBIOS REALIZADOS POR EVERIS 30/07/2014
    }

    @Override
    public PagerData<NotificacionTipo> buscarPorCriteriosNotificacion(List<ParametroTipo> parametro,
            List<CriterioOrdenamientoTipo> criteriosOrdenamiento, Long valTamPagina, Long valNumPagina) throws AppException {

        final PagerData<Notificacion> notificaciones = notificacionDao.findPorCriterios(parametro, criteriosOrdenamiento,
                valTamPagina, valNumPagina);

        final List<NotificacionTipo> notificacionTipo = assembleCollectionServicio(notificaciones.getData());

        return new PagerData<NotificacionTipo>(notificacionTipo, notificaciones.getNumPages());
    }

    public List<NotificacionTipo> assembleCollectionServicio(Collection<Notificacion> collection) {

        List<NotificacionTipo> arrayL = new ArrayList<NotificacionTipo>();

        for (Notificacion parcial : collection) {

            arrayL.add(assembleServicio(parcial));
        }
        return arrayL;

    }

    public NotificacionTipo assembleServicio(Notificacion entidad) {

        NotificacionTipo notificacionServicio = new NotificacionTipo();

        if (entidad.getId() != null) {
            notificacionServicio.setIdNotificacion(String.valueOf(entidad.getId()));
        }
        if (entidad.getCodCalidadNotificado() != null) {
            notificacionServicio.setCodCalidadNotificado(entidad.getCodCalidadNotificado().getId());
        }
        if (entidad.getEsActaNotificacionValidada() != null) {
            notificacionServicio.setEsActaNotificacionValidada(String.valueOf(entidad.getEsActaNotificacionValidada()));
        }
        if (entidad.getEsDevlucionActoAdministrat() != null) {
            notificacionServicio.setEsDevolucionActoAdministrativoAprobada(String.valueOf(entidad.getEsDevlucionActoAdministrat()));
        }
        if (entidad.getEsMecanismoSubsidiario() != null) {
            notificacionServicio.setEsMecanismoSubsidiario(String.valueOf(entidad.getEsMecanismoSubsidiario()));
        }
        if (entidad.getActoAdministrativo() != null) {
            notificacionServicio.setActoAdministrativo(actoAdministrativoAssembler.assembleServicio(entidad.getActoAdministrativo()));
        }
        if (entidad.getCodCalidadNotificado() != null) {
            notificacionServicio.setCodCalidadNotificado(entidad.getCodCalidadNotificado().getId());
        }
        if (entidad.getCodTipoNotificacion() != null) {
            notificacionServicio.setCodTipoNotificacion(entidad.getCodTipoNotificacion().getId());
        }
        if (entidad.getCodTipoMecanismoSubsidiario() != null) {
            notificacionServicio.setCodTipoMecanismoSubsidiario(entidad.getCodTipoMecanismoSubsidiario().getId());
        }
        if (entidad.getDesActaNotififcacionValidad() != null) {
            notificacionServicio.setDescActaNotificacionValidadaObservaciones(entidad.getDesActaNotififcacionValidad());
        }
        if (entidad.getDesCalidadNotificado() != null) {
            notificacionServicio.setDescCalidadNotificado(entidad.getDesCalidadNotificado());
        }
        if (entidad.getDesDevolucionActoAdministr() != null) {
            notificacionServicio.setDescDevolucionActoAdministrativoObservaciones(entidad.getDesDevolucionActoAdministr());
        }
        if (entidad.getCodEstadoNotificacion() != null) {
            notificacionServicio.setCodEstadoNotificacion(entidad.getCodEstadoNotificacion().getId());
        }
        if (entidad.getCodTipoNotificacionDefinitva() != null) {
            notificacionServicio.setCodTipoNotificacionDefinitiva(entidad.getCodTipoNotificacionDefinitva().getId());
        }
        //Se obtienen los anexos temporales
        if (entidad.getAnexosTemporales() != null && !entidad.getAnexosTemporales().isEmpty()) {
            List<ParametroValoresTipo> anexosTemporales = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> anexosTemporalesNotificacion = new HashMap<String, List<ParametroTipo>>();
            for (NotificacionAnexoTemporal anexoTemporal : entidad.getAnexosTemporales()) {
                List<ParametroTipo> archivosNotificacion = anexosTemporalesNotificacion.get(anexoTemporal.getCodTipoAnexo().getId());
                if (archivosNotificacion == null) {
                    archivosNotificacion = new ArrayList<ParametroTipo>();
                }
                ParametroTipo anexoNotificacion = new ParametroTipo();
                anexoNotificacion.setIdLlave(anexoTemporal.getArchivo().getId().toString());
                anexoNotificacion.setValValor(anexoTemporal.getArchivo().getValNombreArchivo());
                archivosNotificacion.add(anexoNotificacion);
                anexosTemporalesNotificacion.put(anexoTemporal.getCodTipoAnexo().getId(), archivosNotificacion);
            }
            for (Map.Entry<String, List<ParametroTipo>> entry : anexosTemporalesNotificacion.entrySet()) {
                ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                documentoFinal.setIdLlave(entry.getKey());
                documentoFinal.getValValor().addAll(entry.getValue());
                anexosTemporales.add(documentoFinal);
            }
            notificacionServicio.getArchivosTemporales().addAll(anexosTemporales);
        }
        if (entidad.getDocActaNotPresoId() != null) {
            ArchivoTipo archivoTipo = new ArchivoTipo();
            archivoTipo.setIdArchivo(String.valueOf(entidad.getDocActaNotPresoId()));

            archivoTipo.setValContenidoArchivo(VAL_CONTENIDO);

            notificacionServicio.setDocActaNotificacionPersonal(archivoTipo);
        }
        if (entidad.getDocumentosAnexosCodnotList() != null) {
            List<String> list = new ArrayList<String>();
            for (DocumentosAnexosCodnot documentosAnexosCodnot : entidad.getDocumentosAnexosCodnotList()) {
                list.add(String.valueOf(documentosAnexosCodnot.getDocumentosAnexosNotifId()));
            }
            notificacionServicio.getCodDocumentosAnexosNotificacionPersonal().addAll(list);
        }
        if (entidad.getDocumentosAnexosCodnotList() != null) {
            List<String> list = new ArrayList<String>();
            for (DocumentosAnexosCodnot documentosAnexosCodnot : entidad.getDocumentosAnexosCodnotList()) {
                list.add(String.valueOf(documentosAnexosCodnot.getDocumentosAnexosNotifId()));
            }
            notificacionServicio.getCodDocumentosAnexosNotificacionPersonal().addAll(list);
        }
        if (entidad.getDocumentosAnexosIdnotList() != null) {
            List<String> list = new ArrayList<String>();
            for (DocumentosAnexosIdnot documentosAnexosIdnot : entidad.getDocumentosAnexosIdnotList()) {
                list.add(String.valueOf(documentosAnexosIdnot.getIdDocumentosAnexos()));
            }
            notificacionServicio.getIdDocumentosAnexos().addAll(list);
        }
        if (entidad.getFecNotificacion() != null) {
            notificacionServicio.setFecNotificacion(entidad.getFecNotificacion());
        }
        if (StringUtils.isNotBlank(entidad.getIdRadicadoDocumentosAnexos())) {
            notificacionServicio.setIdRadicadoDocumentosAnexosNotificacion(entidad.getIdRadicadoDocumentosAnexos());
        }
        if (entidad.getPersona() != null) {
            Object persona = personaAssembler.assembleServicio(entidad.getPersona());

            if (persona instanceof PersonaNaturalTipo) {
                notificacionServicio.setPersonaNaturalNotificada((PersonaNaturalTipo) persona);
            }
        }
        if (entidad.getInteresadoId() != null) {

            Persona persona;
            try {
                persona = personaDao.find(entidad.getInteresadoId().getId());

                if (persona != null) {

                    InteresadoTipo interesado = interesadoAssembler.assembleServicio(persona);
                    notificacionServicio.setInteresado(interesado);
                }

            } catch (AppException ex) {
                ex.printStackTrace();
            }

        }

        return notificacionServicio;
    }

}
