package co.gov.ugpp.parafiscales.servicios.notificaciones.srvAplNotificacion;

import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpActualizarNotificacionFallo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpBuscarPorCriteriosNotificacionFallo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpCrearNotificacionFallo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpActualizarNotificacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpBuscarPorCriteriosNotificacionRespTipo;
import co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpCrearNotificacionRespTipo;
import co.gov.ugpp.parafiscales.AbstractSrvApl;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.PagerData;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;
import javax.ejb.EJB;
import javax.jws.HandlerChain;
import javax.jws.WebService;
import org.slf4j.LoggerFactory;

/**
 *
 * @author franzjr
 */
@WebService(serviceName = "SrvAplNotificacion",
        portName = "portSrvAplNotificacionSOAP",
        endpointInterface = "co.gov.ugpp.notificaciones.srvaplnotificacion.v1.PortSrvAplNotificacionSOAP",
        targetNamespace = "http://www.ugpp.gov.co/Notificaciones/SrvAplNotificacion/v1")
@HandlerChain(file = "handler-chain.xml")
public class SrvAplNotificacion extends AbstractSrvApl {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SrvAplNotificacion.class);

    @EJB
    private NotificacionFacade notificacionFacade;

    public co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpCrearNotificacionRespTipo opCrearNotificacion(co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpCrearNotificacionSolTipo msjOpCrearNotificacionSol) throws co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpCrearNotificacionFallo {
        LOG.info("Op: opCrearNotificacion ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpCrearNotificacionSol.getContextoTransaccional();
        Long idNotificacion;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            idNotificacion = notificacionFacade.crearNotificacion(msjOpCrearNotificacionSol.getNotificacion(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpCrearNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opCrearNotificacion ::: END");

        OpCrearNotificacionRespTipo crearNotificacionRespTipo = new OpCrearNotificacionRespTipo();
        crearNotificacionRespTipo.setContextoRespuesta(contextoRespuesta);
        crearNotificacionRespTipo.setIdNotificacion(String.valueOf(idNotificacion));

        return crearNotificacionRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpBuscarPorCriteriosNotificacionRespTipo opBuscarPorCriteriosNotificacion(co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpBuscarPorCriteriosNotificacionSolTipo msjOpBuscarPorCriteriosNotificacionSol) throws co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpBuscarPorCriteriosNotificacionFallo {
        LOG.info("Op: opBuscarPorCriteriosNotificacion ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpBuscarPorCriteriosNotificacionSol.getContextoTransaccional();

        final PagerData<NotificacionTipo> notificaciones;

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            notificaciones = notificacionFacade.buscarPorCriteriosNotificacion(msjOpBuscarPorCriteriosNotificacionSol.getParametro(),
                    contextoSolicitud.getCriteriosOrdenamiento(),
                    contextoSolicitud.getValTamPagina(),
                    contextoSolicitud.getValNumPagina());

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpBuscarPorCriteriosNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opBuscarPorCriteriosNotificacion ::: END");

        OpBuscarPorCriteriosNotificacionRespTipo buscarPorCriteriosNotificacionRespTipo = new OpBuscarPorCriteriosNotificacionRespTipo();
        contextoRespuesta.setValCantidadPaginas(notificaciones.getNumPages());
        buscarPorCriteriosNotificacionRespTipo.setContextoRespuesta(contextoRespuesta);
        buscarPorCriteriosNotificacionRespTipo.getNotificacion().addAll(notificaciones.getData());

        return buscarPorCriteriosNotificacionRespTipo;
    }

    public co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpActualizarNotificacionRespTipo opActualizarNotificacion(co.gov.ugpp.notificaciones.srvaplnotificacion.v1.OpActualizarNotificacionSolTipo msjOpActualizarNotificacionSol) throws co.gov.ugpp.notificaciones.srvaplnotificacion.v1.MsjOpActualizarNotificacionFallo {
        LOG.info("Op: opBuscarPorIdInteresado ::: INIT");

        ContextoRespuestaTipo contextoRespuesta = new ContextoRespuestaTipo();
        ContextoTransaccionalTipo contextoSolicitud = msjOpActualizarNotificacionSol.getContextoTransaccional();

        try {
            this.initContextoRespuesta(contextoSolicitud, contextoRespuesta);
            ldapAuthentication.validateLdapAuthentication(
                    contextoSolicitud.getIdUsuarioAplicacion(), contextoSolicitud.getValClaveUsuarioAplicacion());

            notificacionFacade.actualizarNotificacion(msjOpActualizarNotificacionSol.getNotificacion(),
                    contextoSolicitud);

        } catch (AppException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        } catch (RuntimeException ex) {
            LOG.error(ex.getMessage(), ex);
            throw new MsjOpActualizarNotificacionFallo("Error ", ErrorUtil.buildFalloTipo(contextoRespuesta, ex), ex);
        }
        
        LOG.info("Op: opBuscarPorIdInteresado ::: END");

        OpActualizarNotificacionRespTipo actualizarNotificacionRespTipo = new OpActualizarNotificacionRespTipo();

        actualizarNotificacionRespTipo.setContextoRespuesta(contextoRespuesta);

        return actualizarNotificacionRespTipo;
    }

}
