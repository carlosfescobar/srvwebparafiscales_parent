package co.gov.ugpp.parafiscales.servicios.util;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.TechnicalException;
import co.gov.ugpp.parafiscales.procesos.transversales.AuditoriaFacade;
import co.gov.ugpp.parafiscales.util.DateUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import javax.ejb.EJB;
import javax.xml.namespace.QName;
import javax.xml.soap.AttachmentPart;
import javax.xml.soap.Node;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 *
 * @author Mauricio Guerrero
 * @author jsaenz
 */
public class AuditoriaServicioHandler implements SOAPHandler<SOAPMessageContext> {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(AuditoriaServicioHandler.class);

    @EJB
    private AuditoriaFacade auditoriaFacade;

    @Override
    public boolean handleMessage(final SOAPMessageContext messageContext) {
        this.logMessage(messageContext);
        this.configureAttachments(messageContext);
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        return Collections.emptySet();
    }

    @Override
    public boolean handleFault(final SOAPMessageContext messageContext) {
        this.logMessage(messageContext);
        return true;
    }

    @Override
    public void close(final MessageContext context) {
    }

    private void logMessage(final SOAPMessageContext messageContext) {
        final SOAPMessage msg = messageContext.getMessage();
        final ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            msg.writeTo(out);
            final byte[] bytes = out.toByteArray();
            final ContextoTransaccionalTipo contextoTransaccionalTipo;
            final SOAPBody body = msg.getSOAPBody();
            final boolean isRespuesta = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
            if (isRespuesta) {
                contextoTransaccionalTipo = this.generateContextoTransaccional(body, "contextoRespuesta");
                auditoriaFacade.registerOutbound(contextoTransaccionalTipo, bytes);
            } else {
                contextoTransaccionalTipo = this.generateContextoTransaccional(body, "contextoTransaccional");
                auditoriaFacade.registerInbound(contextoTransaccionalTipo, bytes);
            }
        } catch (Exception ex) {
            LOG.error("Error al registrar la auditoria", ex);
            throw new TechnicalException("Error al registrar la auditoria", ex);
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                LOG.warn("Error al cerrar el outputStream", ex);
            }
        }
    }

    private ContextoTransaccionalTipo generateContextoTransaccional(final SOAPBody body, final String tipoContexto) {

        final Map<String, String> mapElements = new HashMap<String, String>();

        final Iterator childElements = body.getChildElements();

        while (childElements.hasNext()) {
            final Node childElement = (Node) childElements.next();
            if (childElement.getNodeType() == Node.ELEMENT_NODE) {
                final Element elementOp = (Element) childElement;
                final Element elementContexto = this.findElementContexto(elementOp, tipoContexto);
                if (elementContexto != null) {
                    final NodeList nodeListCtx = elementContexto.getChildNodes();
                    for (int j = 0, m = nodeListCtx.getLength(); j < m; j++) {
                        final Node nodeCtx = (Node) nodeListCtx.item(j);
                        if (nodeCtx.getNodeType() == Node.ELEMENT_NODE) {
                            final Element elementCtx = (Element) nodeCtx;
                            mapElements.put(elementCtx.getTagName(), elementCtx.getTextContent());
                        }
                    }
                }
            }
        }

        final ContextoTransaccionalTipo contextoTransaccionalTipo = this.buildContextoTransaccional(mapElements);

        return contextoTransaccionalTipo;
    }

    private ContextoTransaccionalTipo buildContextoTransaccional(final Map<String, String> mapElements) {

        final ContextoTransaccionalTipo contextoTransaccionalTipo = new ContextoTransaccionalTipo();

        contextoTransaccionalTipo.setIdTx(mapElements.get("idTx"));
        contextoTransaccionalTipo.setFechaInicioTx(DateUtil.parseStrDateTimeToCalendar(mapElements.get("fechaInicioTx")));
        contextoTransaccionalTipo.setIdInstanciaProceso(mapElements.get("idInstanciaProceso"));
        contextoTransaccionalTipo.setIdDefinicionProceso(mapElements.get("idDefinicionProceso"));
        contextoTransaccionalTipo.setValNombreDefinicionProceso(mapElements.get("valNombreDefinicionProceso"));
        contextoTransaccionalTipo.setIdInstanciaActividad(mapElements.get("idInstanciaActividad"));
        contextoTransaccionalTipo.setValNombreDefinicionActividad(mapElements.get("valNombreDefinicionActividad"));
        contextoTransaccionalTipo.setIdUsuarioAplicacion(mapElements.get("idUsuarioAplicacion"));
        contextoTransaccionalTipo.setValClaveUsuarioAplicacion(mapElements.get("valClaveUsuarioAplicacion"));
        contextoTransaccionalTipo.setIdUsuario(mapElements.get("idUsuario"));
        contextoTransaccionalTipo.setIdEmisor(mapElements.get("idEmisor"));
        contextoTransaccionalTipo.setValTamPagina(StringUtils.isBlank(mapElements.get("valTamPagina")) ? null
                : Long.valueOf(mapElements.get("valTamPagina")));
        contextoTransaccionalTipo.setValNumPagina(StringUtils.isBlank(mapElements.get("valNumPagina")) ? null
                : Long.valueOf(mapElements.get("valNumPagina")));
        
        return contextoTransaccionalTipo;

    }

    private void configureAttachments(final SOAPMessageContext messageContext) {
        final SOAPMessage msg = messageContext.getMessage();
        final Iterator attachments = msg.getAttachments();
        if (attachments != null) {
            try {
                while (attachments.hasNext()) {
                    final AttachmentPart attachment = (AttachmentPart) attachments.next();
                    messageContext.put("archivo", attachment.getDataHandler().getInputStream());
                    messageContext.setScope("archivo", MessageContext.Scope.APPLICATION);
                }
            } catch (IOException e) {
                throw new TechnicalException("Error configurando attachments", e);
            } catch (SOAPException e) {
                throw new TechnicalException("Error configurando attachments", e);
            }
        }
    }

    private Element findElementContexto(final Element elementOp, final String tipoContexto) {

        final NodeList nodeList = elementOp.getChildNodes();
        Element element = null;

        for (int i = 0, n = nodeList.getLength(); i < n; i++) {
            final Node node = (Node) nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                if (tipoContexto.equals(node.getNodeName())) {
                    element = (Element) node;
                    break;
                } else {
                    element = this.findElementContexto((Element) node, tipoContexto);
                }
            }
        }

        return element;
    }

}
