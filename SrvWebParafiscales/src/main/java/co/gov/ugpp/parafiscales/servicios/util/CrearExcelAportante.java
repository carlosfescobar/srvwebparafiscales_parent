package co.gov.ugpp.parafiscales.servicios.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.BorderFormatting;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.RegionUtil;
import org.apache.poi.ss.util.WorkbookUtil;

/**
 *
 * @author Mauricio Guerrero
 */
public class CrearExcelAportante {

    /**
     * Funcion encargada de cargar la informacion requerida del archivo excel
     * que se genera
     *
     * @param rs Resultado de la consulta al procedimiento almacenado
     * @return
     */
    public static byte[] crearExcelAportante(ResultSet rs) throws IOException {

        Workbook wb = cargarEncabezadosAportante();
        Sheet hoja1 = wb.getSheet("EXTRACTO");

        Font fuente = wb.createFont();
        fuente.setFontHeightInPoints((short) 11);
        fuente.setFontName("Arial Narrow");
        fuente.setBoldweight((short) 5);

        CellStyle estilo = wb.createCellStyle();
        estilo.setWrapText(true);

        estilo = wb.createCellStyle();
        estilo.setFont(fuente);

        Row row = hoja1.getRow(0);
        Cell cell = row.getCell(1);

        int fila = 0;

        if (rs != null) {
            try {
                if (rs.next()) {
                    // informacion basica
                    for (int i = 0; i < 10; i++) {
                        row = hoja1.getRow(i);
                        cell = row.getCell(1);
                        try {
                            cell.setCellValue(rs.getString(cell.getStringCellValue().toString()));
                        } catch (SQLException ex) {
                            Logger.getLogger(CrearExcelAportante.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                    row = hoja1.getRow(10);
                    cell = row.getCell(1);
                    String nombre = rs.getString("primer_nombre") +" "+ rs.getString("segundo_nombre") +" "+ rs.getString("primer_apellido") +" "+ rs.getString("segundo_apellido");
                    cell.setCellValue(nombre);
                    cell.setCellStyle(estilo);
                    
                    //campos de tabla
                    estilo.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
                    estilo.setBorderTop(BorderFormatting.BORDER_THIN);
                    estilo.setBorderBottom(BorderFormatting.BORDER_THIN);
                    estilo.setBorderLeft(BorderFormatting.BORDER_THIN);
                    estilo.setBorderRight(BorderFormatting.BORDER_THIN);

                    fila = 15;

                    row = hoja1.createRow(fila);

                    cell = row.createCell(0);
                    cell.setCellValue(rs.getString("AÑO_RESTO"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(1);
                    cell.setCellValue(rs.getString("MES_RESTO"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(2);
                    cell.setCellValue(rs.getString("fecha_pago"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(3);
                    cell.setCellValue(rs.getString("tipo_planilla"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(4);
                    cell.setCellValue(rs.getString("planilla_asociada"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(5);
                    cell.setCellValue("codigo");
                    cell.setCellStyle(estilo);

                    cell = row.createCell(6);
                    cell.setCellValue(rs.getString("planilla_archivo"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(7);
                    cell.setCellValue(rs.getString("tipo_aportante"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(8);
                    cell.setCellValue(rs.getString("tipo_cotizante"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(9);
                    cell.setCellValue(rs.getString("forma_presentacion"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(10);
                    cell.setCellValue(rs.getString("tarifa_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(11);
                    cell.setCellValue(rs.getString("tarifa_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(12);
                    cell.setCellValue(rs.getString("tarifa_centro_trabajo"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(13);
                    cell.setCellValue(rs.getString("tarifa_aportes_ccf"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(14);
                    cell.setCellValue(rs.getString("ibc_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(15);
                    cell.setCellValue(rs.getString("cot_obligatoria_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(16);
                    cell.setCellValue(rs.getString("ibc_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(17);
                    cell.setCellValue(rs.getString("aporte_cot_obligatoria_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(18);
                    cell.setCellValue(rs.getString("ibc_rprof"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(19);
                    cell.setCellValue(rs.getString("cot_obligatoria_arp"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(20);
                    cell.setCellValue(rs.getString("ibc_ccf"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(21);
                    cell.setCellValue(rs.getString("valor_aportes_ccf_ibc_tarifa"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(22);
                    cell.setCellValue(rs.getString("salario_basico"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(23);
                    cell.setCellValue(rs.getString("ingreso"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(24);
                    cell.setCellValue(rs.getString("retiro"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(25);
                    cell.setCellValue(rs.getString("dias_cot_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(26);
                    cell.setCellValue(rs.getString("dias_cot_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(27);
                    cell.setCellValue(rs.getString("dias_cot_rprof"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(28);
                    cell.setCellValue(rs.getString("dias_cot_ccf"));
                    cell.setCellStyle(estilo);

                }

                //iteracion demas filas del resultset
                while (rs.next()) {
                    row = hoja1.createRow(fila + 1);

                    cell = row.createCell(0);
                    cell.setCellValue(rs.getString("AÑO_RESTO"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(1);
                    cell.setCellValue(rs.getString("MES_RESTO"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(2);
                    cell.setCellValue(rs.getString("fecha_pago"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(3);
                    cell.setCellValue(rs.getString("tipo_planilla"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(4);
                    cell.setCellValue(rs.getString("planilla_asociada"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(5);
                    cell.setCellValue(rs.getString("codigo"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(6);
                    cell.setCellValue(rs.getString("planilla_archivo"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(7);
                    cell.setCellValue(rs.getString("tipo_aportante"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(8);
                    cell.setCellValue(rs.getString("tipo_cotizante"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(9);
                    cell.setCellValue(rs.getString("forma_presentacion"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(10);
                    cell.setCellValue(rs.getString("tarifa_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(11);
                    cell.setCellValue(rs.getString("tarifa_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(12);
                    cell.setCellValue(rs.getString("tarifa_centro_trabajo"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(13);
                    cell.setCellValue(rs.getString("tarifa_aportes_ccf"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(14);
                    cell.setCellValue(rs.getString("ibc_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(15);
                    cell.setCellValue(rs.getString("cot_obligatoria_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(16);
                    cell.setCellValue(rs.getString("ibc_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(17);
                    cell.setCellValue(rs.getString("aporte_cot_obligatoria_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(18);
                    cell.setCellValue(rs.getString("ibc_rprof"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(19);
                    cell.setCellValue(rs.getString("cot_obligatoria_arp"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(20);
                    cell.setCellValue(rs.getString("ibc_ccf"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(21);
                    cell.setCellValue(rs.getString("valor_aportes_ccf_ibc_tarifa"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(22);
                    cell.setCellValue(rs.getString("salario_basico"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(23);
                    cell.setCellValue(rs.getString("ingreso"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(24);
                    cell.setCellValue(rs.getString("retiro"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(25);
                    cell.setCellValue(rs.getString("dias_cot_salud"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(26);
                    cell.setCellValue(rs.getString("dias_cot_pension"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(27);
                    cell.setCellValue(rs.getString("dias_cot_rprof"));
                    cell.setCellStyle(estilo);

                    cell = row.createCell(28);
                    cell.setCellValue(rs.getString("dias_cot_ccf"));
                    cell.setCellStyle(estilo);
                }

            } catch (SQLException ex) {
                Logger.getLogger(CrearExcelAportante.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            wb.write(bos);
        } catch (IOException ex) {
            Logger.getLogger(CrearExcelAportante.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            bos.close();
        }
        byte[] bytes = bos.toByteArray();

        return bytes;
    }

    /**
     * Funcion encargada de cargar los encabezados basicos del archivo excel que
     * se genera
     *
     * @param rs resultado de la consulta al procedimiento almacenado
     * @return
     */
    private static Workbook cargarEncabezadosAportante() {
        Workbook wb = new HSSFWorkbook();

        Font fuente = wb.createFont();
        fuente.setFontHeightInPoints((short) 11);
        fuente.setFontName("Arial Narrow");
        fuente.setBoldweight((short) 5);

        String safeName = WorkbookUtil.createSafeSheetName("EXTRACTO");
        Sheet hoja1 = wb.createSheet(safeName);

        CellStyle estilo = wb.createCellStyle();
        estilo.setWrapText(true);

        estilo = wb.createCellStyle();
        estilo.setFont(fuente);

        Row row = hoja1.createRow(0);

        Cell cell = row.createCell(0);
        cell.setCellValue("NIT APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("NUMERO_ID_APORTANTE");//nit
        cell.setCellStyle(estilo);

        row = hoja1.createRow(1);

        cell = row.createCell(0);
        cell.setCellValue("RAZON_SOCIAL_RUT_2013 APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_RazonSocial");//razon social
        cell.setCellStyle(estilo);

        row = hoja1.createRow(2);

        cell = row.createCell(0);
        cell.setCellValue("COD_DEPTO_RUT_2013 APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_Dpto");//departamento
        cell.setCellStyle(estilo);

        row = hoja1.createRow(3);

        cell = row.createCell(0);
        cell.setCellValue("COD_MUNI_RUT_2013 APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_Ciudad");//ciudad
        cell.setCellStyle(estilo);

        row = hoja1.createRow(4);

        cell = row.createCell(0);
        cell.setCellValue("DIRECCION_RUT_2013 APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_Direccion");//direccion
        cell.setCellStyle(estilo);

        row = hoja1.createRow(5);

        cell = row.createCell(0);
        cell.setCellValue("ACT_ECONOMICA_RUT_2009");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_CodActividEcon");//actividad economica
        cell.setCellStyle(estilo);

        row = hoja1.createRow(6);

        cell = row.createCell(0);
        cell.setCellValue("AP_Clase (CLASE DE APORTANTE)");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_Clase");//clase aportante
        cell.setCellStyle(estilo);

        row = hoja1.createRow(7);

        cell = row.createCell(0);
        cell.setCellValue("AP_TipoPersona  (APORTANTE)");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_TipoPersona");//tipo persona
        cell.setCellStyle(estilo);

        row = hoja1.createRow(8);

        cell = row.createCell(0);
        cell.setCellValue("AP_CodOperador");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("AP_CodOperador");//codigo operador
        cell.setCellStyle(estilo);

        row = hoja1.createRow(9);

        cell = row.createCell(0);
        cell.setCellValue("numero_identificacion COTIZANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("numero_id_cot");//codigo operador
        cell.setCellStyle(estilo);

        row = hoja1.createRow(10);

        cell = row.createCell(0);
        cell.setCellValue("NOMBRE COTIZANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("primer_nombre segundo_nombre primer apellido segundo apellido");//codigo operador
        cell.setCellStyle(estilo);

        CellRangeAddress region = CellRangeAddress.valueOf("A1:B9");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        region = CellRangeAddress.valueOf("A10:B11");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        estilo.setAlignment(CellStyle.ALIGN_CENTER_SELECTION);
        estilo.setBorderTop(BorderFormatting.BORDER_THIN);
        estilo.setBorderBottom(BorderFormatting.BORDER_THIN);
        estilo.setBorderLeft(BorderFormatting.BORDER_THIN);
        estilo.setBorderRight(BorderFormatting.BORDER_THIN);

        row = hoja1.createRow(12);

        cell = row.createCell(0);
        cell.setCellValue("PERIODOS");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("A13:B14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(2);
        cell.setCellValue("DETALLE PLANILLA");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("C13:G14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(7);
        cell.setCellValue("TIPO DE APORTANTE Y TIPO DE COTIZANTE");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("H13:J14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(10);
        cell.setCellValue("TARIFAS");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("K13:N14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(14);
        cell.setCellValue("IBC - COTIZACIONES");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("O13:V14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(22);
        cell.setCellValue("SALARIO BASICO");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("W13:W14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(23);
        cell.setCellValue("NOVEDADES PILA");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("X13:Y14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        cell = row.createCell(25);
        cell.setCellValue("DIAS COTIZADOS POR SUBSISTEMA");
        cell.setCellStyle(estilo);

        region = CellRangeAddress.valueOf("Z13:AC14");

        RegionUtil.setBorderBottom(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderTop(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderLeft(BorderFormatting.BORDER_THIN, region, hoja1, wb);
        RegionUtil.setBorderRight(BorderFormatting.BORDER_THIN, region, hoja1, wb);

        hoja1.addMergedRegion(region);

        row = hoja1.createRow(14);

        cell = row.createCell(0);
        cell.setCellValue("AÑO");
        cell.setCellStyle(estilo);

        cell = row.createCell(1);
        cell.setCellValue("MES");
        cell.setCellStyle(estilo);

        cell = row.createCell(2);
        cell.setCellValue("FECHA_PAGO");
        cell.setCellStyle(estilo);

        cell = row.createCell(3);
        cell.setCellValue("TIPO_PLANILLA");
        cell.setCellStyle(estilo);

        cell = row.createCell(4);
        cell.setCellValue("PLANILLA_ASOCIADA");
        cell.setCellStyle(estilo);

        cell = row.createCell(5);
        cell.setCellValue("PLANILLA");
        cell.setCellStyle(estilo);

        cell = row.createCell(6);
        cell.setCellValue("PLANILLA_ARCHIVO");
        cell.setCellStyle(estilo);

        cell = row.createCell(7);
        cell.setCellValue("TIPO_APORTANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(8);
        cell.setCellValue("TIPO_COTIZANTE");
        cell.setCellStyle(estilo);

        cell = row.createCell(9);
        cell.setCellValue("FORMA_PRESENTACION");
        cell.setCellStyle(estilo);

        cell = row.createCell(10);
        cell.setCellValue("TARIFA_SALUD");
        cell.setCellStyle(estilo);

        cell = row.createCell(11);
        cell.setCellValue("TARIFA_PENSION");
        cell.setCellStyle(estilo);

        cell = row.createCell(12);
        cell.setCellValue("TARIFA_CENTRO_TRABAJO");
        cell.setCellStyle(estilo);

        cell = row.createCell(13);
        cell.setCellValue("TARIFA_APORTES_CCF");
        cell.setCellStyle(estilo);

        cell = row.createCell(14);
        cell.setCellValue("IBC_SALUD");
        cell.setCellStyle(estilo);

        cell = row.createCell(15);
        cell.setCellValue("COT_OBLIGATORIA_SALUD");
        cell.setCellStyle(estilo);

        cell = row.createCell(16);
        cell.setCellValue("IBC_PENSION");
        cell.setCellStyle(estilo);

        cell = row.createCell(17);
        cell.setCellValue("APORTANTE_COT_OBLIGATORIA_PENSION");
        cell.setCellStyle(estilo);

        cell = row.createCell(18);
        cell.setCellValue("IBC_RPROF(IBC RIESGOS)");
        cell.setCellStyle(estilo);

        cell = row.createCell(19);
        cell.setCellValue("COT_OBLIGATORIA_ARP");
        cell.setCellStyle(estilo);

        cell = row.createCell(20);
        cell.setCellValue("IBC_CCF");
        cell.setCellStyle(estilo);

        cell = row.createCell(21);
        cell.setCellValue("VALORES_APORTES_CCF_IBC_TARIFA");
        cell.setCellStyle(estilo);

        cell = row.createCell(22);
        cell.setCellValue("SALARIO_BASICO");
        cell.setCellStyle(estilo);

        cell = row.createCell(23);
        cell.setCellValue("NOVEDAD_INGRESO");
        cell.setCellStyle(estilo);

        cell = row.createCell(24);
        cell.setCellValue("NOVEDAD_RETIRO");
        cell.setCellStyle(estilo);

        cell = row.createCell(25);
        cell.setCellValue("DIAS_COT_SALUD");
        cell.setCellStyle(estilo);

        cell = row.createCell(26);
        cell.setCellValue("DIAS_COT_PENSION");
        cell.setCellStyle(estilo);

        cell = row.createCell(27);
        cell.setCellValue("DIAS_COT_RPROF");
        cell.setCellStyle(estilo);

        cell = row.createCell(28);
        cell.setCellValue("DIAS_COT_CCF");
        cell.setCellStyle(estilo);

        hoja1.autoSizeColumn(0);
        hoja1.autoSizeColumn(1);

        return wb;
    }
}
