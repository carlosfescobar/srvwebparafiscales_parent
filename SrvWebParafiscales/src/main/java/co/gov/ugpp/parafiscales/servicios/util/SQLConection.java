package co.gov.ugpp.parafiscales.servicios.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import co.gov.ugpp.esb.schema.criterioordenamientotipo.CriterioOrdenamientoTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.LdapAuthentication;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.exception.TechnicalException;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.sql.CallableStatement;
import java.sql.SQLException;
import java.sql.Types;
import org.slf4j.LoggerFactory;


import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

/**
 * Clase que gestiona las conexiones a base de datos externas no validadas por
 * el modelo de datos
 *
 * @author franzjr
 */
public class SQLConection {

    private Properties ugppSQLProperties;

    private final String driverClassName;

    private final String urlUbicacionPersona;
    private final String usernameUbicacionPersona;
    private final String passwordUbicacionPersona;
    private final String tableUbicacionPersona;

    private final String urlPILA;
    private final String usernamePILA;
    private final String passwordPILA;
    private final String tablePILA;

    private final String urlRUA;
    private final String usernameRUA;
    private final String passwordRUA;
    private final String tableRUA;
    private final String tableJOINRUA;

    private final String urlAportante;
    private final String usernameAportante;
    private final String passwordAportante;
    private final String queryAportante;

    private final String urlScoring;
    private final String usernameScoring;
    private final String passwordScoring;
    private final String queryScoring;

    private final String urlLiquidador;
    private final String usernameLiquidador;
    private final String passwordLiquidador;
    private final String queryLiquidador;

    private static final SQLConection sqlSingleton = new SQLConection();

    public static SQLConection getInstance() {
        return sqlSingleton;
    }

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(SQLConection.class);

    private static final String file = "sql.properties";
    private static final Properties props = loadProperties();
    private Connection connection;

    private static Properties loadProperties() {
        final Properties props = new Properties();
        InputStream input = null;
        try {
            input = SQLConection.class.getClassLoader().getResourceAsStream(file);
            props.load(input);
        } catch (IOException ex) {
            throw new TechnicalException("Archivo de configuración de LDAP no encontrado", ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException ex) {
                    LOG.warn("No se pudo cerrar el archivo ldap.properties", ex);
                }
            }
        }
        return props;
    }

    public static final Properties obtainProperties() {
        return props;
    }

    /**
     * Constructor encargado de leer las propiedades de las base de datos,
     * tablas y esquemas.
     */
    public SQLConection() {

        Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: SQLConection ::: INIT");

        ugppSQLProperties = loadProperties();

        driverClassName = ugppSQLProperties.getProperty("database.driverClassNameDefault");

        urlUbicacionPersona = ugppSQLProperties.getProperty("database.urlUbicacionPersona");
        usernameUbicacionPersona = ugppSQLProperties.getProperty("database.usernameUbicacionPersona");
        passwordUbicacionPersona = ugppSQLProperties.getProperty("database.passwordUbicacionPersona");
        tableUbicacionPersona = ugppSQLProperties.getProperty("database.tableUbicacionPersona");

        urlPILA = ugppSQLProperties.getProperty("database.urlPILA");
        usernamePILA = ugppSQLProperties.getProperty("database.usernamePILA");
        passwordPILA = ugppSQLProperties.getProperty("database.passwordPILA");
        tablePILA = ugppSQLProperties.getProperty("database.tablePILA");

        urlRUA = ugppSQLProperties.getProperty("database.urlRUA");
        usernameRUA = ugppSQLProperties.getProperty("database.usernameRUA");
        passwordRUA = ugppSQLProperties.getProperty("database.passwordRUA");
        tableRUA = ugppSQLProperties.getProperty("database.tableRUA");
        tableJOINRUA = ugppSQLProperties.getProperty("database.tableJOINRUA");

        urlAportante = ugppSQLProperties.getProperty("database.urlAportante");
        usernameAportante = ugppSQLProperties.getProperty("database.usernameAportante");
        passwordAportante = ugppSQLProperties.getProperty("database.passwordAportante");
        queryAportante = ugppSQLProperties.getProperty("database.queryAportante");

        urlScoring = ugppSQLProperties.getProperty("database.urlScoring");
        usernameScoring = ugppSQLProperties.getProperty("database.usernameScoring");
        passwordScoring = ugppSQLProperties.getProperty("database.passwordScoring");
        queryScoring = ugppSQLProperties.getProperty("database.queryScoring");

        //QA
        urlLiquidador = ugppSQLProperties.getProperty("database.urlLiquidador");
        usernameLiquidador = ugppSQLProperties.getProperty("database.usernameLiquidador");
        passwordLiquidador = ugppSQLProperties.getProperty("database.passwordLiquidador");
        
        //PRODUCCION
        //urlLiquidador = ugppSQLProperties.getProperty("database.urlLiquidadorPRO");
        //usernameLiquidador = ugppSQLProperties.getProperty("database.usernameLiquidadorPRO");
        //passwordLiquidador = ugppSQLProperties.getProperty("database.passwordLiquidadorPRO");

        queryLiquidador = ugppSQLProperties.getProperty("database.queryLiquidador");

        Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: SQLConection ::: END");

    }

    /**
     * Retorna los datos del Registro Unico de Aportantes, a partir de su numero
     * de identificacion y su tipo de identificacion.
     *
     * @param numIdentificacion
     * @param tipoIdentificacion
     * @return
     * @throws Exception
     */
    public ResultSet datosRUA(String numIdentificacion, String tipoIdentificacion) throws Exception {
        try {

            Class.forName(this.driverClassName);
            Connection connection = DriverManager.getConnection(
                    this.urlRUA,
                    this.usernameRUA,
                    this.passwordRUA);

            String query = " execute   dbo.SpAnalizarPagosAportanteNV(?,?,?,?,?,?)  SELECT * FROM " + this.tableRUA
                    + " INNER JOIN " + this.tableJOINRUA
                    + " ON " + this.tableJOINRUA + ".[t_bTipI_sint_TipoIdentificacionID] "
                    + " = " + this.tableRUA + ".[t_bPer_sint_TipoDocumento] "
                    + " WHERE t_bPer_vch_Documento = '" + numIdentificacion
                    + "' AND t_bTipI_chr_TipoIDNemo = '" + tipoIdentificacion + ".';";

            LOG.debug("Op: datosRUA ::: QUERY: " + query);
            
            Statement statement = connection.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            throw e;
        }
    }

    
    
    
    
    public ResultSet liquidadorDobleLineaProceso1(String idhojacalculoliquidacion) throws Exception {
        
        String query = null;
        
        try {

            Class.forName(this.driverClassName);
            Connection conexion = DriverManager.getConnection(
                    this.urlLiquidador,
                    this.usernameLiquidador,
                    this.passwordLiquidador);

            query =  "insert into hoja_cal_liquidacion_detal_bk select * from HOJA_CALCULO_LIQUIDACION_DETAL where IDHOJACALCULOLIQUIDACION = "+ idhojacalculoliquidacion;

            Statement statement = conexion.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION liquidadorDobleLineaProceso1: " + idhojacalculoliquidacion + "-" + query + "-" + e.toString());
            throw e;
        }
    }
    
    
    
    public ResultSet liquidadorDobleLineaProceso2(String idhojacalculoliquidacion) throws Exception {
        
        String query = null;
        
        try {

            Class.forName(this.driverClassName);
            Connection conexion = DriverManager.getConnection(
                    this.urlLiquidador,
                    this.usernameLiquidador,
                    this.passwordLiquidador);

            query = "execute USP_REEMPLAZA_VLRS("+ idhojacalculoliquidacion +")";
            Statement statement = conexion.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION liquidadorDobleLineaProceso2: " + idhojacalculoliquidacion + "-" + query + "-" + e.toString());
            throw e;
        }
    }
    
    
    
    
    /*
    public ResultSet procesoDobleLineaAntesDeLiquidar(String idhojacalculoliquidacion) throws Exception {
        
        String query = null;
        
        try {

            Class.forName(this.driverClassName);
            Connection conexion = DriverManager.getConnection(
                    this.urlLiquidador,
                    this.usernameLiquidador,
                    this.passwordLiquidador);

            query = "execute USP_CALCULA_VLRS("+ idhojacalculoliquidacion +")";
            Statement statement = conexion.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION procesoDobleLineaAntesDeLiquidar: " + idhojacalculoliquidacion + "-" + query + "-" + e);
            throw e;
        }
    }
    */
    
    
    
     public ResultSet procesoDobleLineaAntesDeLiquidar(String idhojacalculoliquidacion) throws Exception {
        
        ResultSet result = null;
        String sql = null;
        
        try {
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(this.urlLiquidador, this.usernameLiquidador, this.passwordLiquidador);

            sql = "BEGIN USP_CALCULA_VLRS(?); END;";
            CallableStatement cs = connection.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(idhojacalculoliquidacion));
            cs.execute();
            result = cs.getResultSet();
            //System.out.println("::Liquidador:: se ejecuta SQL: BEGIN USP_REEMPLAZA_VLRS("+ idhojacalculoliquidacion +"); END;");
              
        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION procesoDobleLineaAntesDeLiquidar: " + idhojacalculoliquidacion + "-" + sql + "-" + "-" + e);
            throw e;
        }
        
        return result;

    }

    
    
    
    
    
    
    
       
    public ResultSet liquidadorDobleLineaProceso2B(String idhojacalculoliquidacion) throws Exception {
        
        ResultSet result = null;
        String sql = null;
        
        try {
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(this.urlLiquidador, this.usernameLiquidador, this.passwordLiquidador);

            sql = "BEGIN USP_REEMPLAZA_VLRS(?); END;";
            CallableStatement cs = connection.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(idhojacalculoliquidacion));
            cs.execute();
            result = cs.getResultSet();
            //System.out.println("::Liquidador:: se ejecuta SQL: BEGIN USP_REEMPLAZA_VLRS("+ idhojacalculoliquidacion +"); END;");
              
        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION liquidadorDobleLineaProceso2B: " + idhojacalculoliquidacion + "-" + sql + "-" + "-" + e);
            throw e;
        }
        
        return result;
    }
   
    
    
    
    
    
    
        
       
    public ResultSet liquidadorComparacionVariacionIBC(String idhojacalculoliquidacion) throws Exception {
        
        ResultSet result = null;
        String sql = null;
        
        try {
            
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(this.urlLiquidador, this.usernameLiquidador, this.passwordLiquidador);

            sql = "BEGIN USP_COMPARACION_IBC (?); END;";
            CallableStatement cs = connection.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(idhojacalculoliquidacion));
            cs.execute();
            result = cs.getResultSet();
            //System.out.println("::Liquidador:: se ejecuta SQL: BEGIN USP_REEMPLAZA_VLRS("+ idhojacalculoliquidacion +"); END;");
              
        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION liquidadorComparacionVariacionIBC: " + idhojacalculoliquidacion + "-" + sql + "-" + "-" + e);
            throw e;
        }
        
        return result;
    }
   
    
    
    
    
    
    
    
    
    
    /**
     * Retorna los datos de la tabla PILA, a partir de su numero de
     * identificacion y su tipo de identificacion.
     *
     * @param numIdentificacion
     * @param tipoIdentificacion
     * @return
     * @throws Exception
     */
    public ResultSet datosPILA(String numIdentificacion, String tipoIdentificacion) throws Exception {

        String query = null;
        
        try {

            Class.forName(this.driverClassName);
            Connection connection = DriverManager.getConnection(
                    this.urlPILA,
                    this.usernamePILA,
                    this.passwordPILA);

            query = "SELECT * FROM " + this.tablePILA + " WHERE " + " tipo_identificacion = '" + tipoIdentificacion
                    + "' AND numero_identificacion = '" + numIdentificacion + "'";

            //Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: datosRUA ::: QUERY: " + query);

            Statement statement = connection.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            LOG.error("::ERROR EXCEPTION datosPILA: " + "-" + query + "-" + e);
            throw e;
        }

    }

    /**
     * Retorna la ubicacion geografica del aportante, a partir de criterios y
     * parametros especificados por el cliente.
     *
     * @param parametros
     * @param criterios
     * @param tamPagina
     * @param numPagina
     * @return
     * @throws Exception
     */
    public Integer ubicacionPersonaCount(List<ParametroTipo> parametros, List<CriterioOrdenamientoTipo> criterios, Integer tamPagina,
            Integer numPagina) throws Exception {

        try {
            Class.forName(this.driverClassName);
            connection = DriverManager.getConnection(
                    this.urlUbicacionPersona,
                    this.usernameUbicacionPersona,
                    this.passwordUbicacionPersona);
            /**
             * CONSULTA
             */
            String where = "";
            String orderBy = "";

            if (parametros != null) {
                for (ParametroTipo parametro : parametros) {
                    if (parametro.getIdLlave() != null && !parametro.getIdLlave().equals("") && parametro.getValValor() != null
                            && !parametro.getValValor().equals("")) {
                        where += where.equals("") ? parametro.getIdLlave() + " = " + "'" + parametro.getValValor()
                                + "'" : " AND " + parametro.getIdLlave() + " = " + "'" + parametro.getValValor() + "'";
                    }
                }
                Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: Ubicacion Persona ::: WHERE: " + where);

            }

            if (criterios != null) {
                for (CriterioOrdenamientoTipo criterio : criterios) {
                    if (criterio.getValNombreCampo() != null && !criterio.getValNombreCampo().equals("")) {
                        orderBy += orderBy.equals("") ? criterio.getValNombreCampo() : " , "
                                + criterio.getValNombreCampo();
                    }
                }
                Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: Ubicacion Persona ::: ORDER BY: " + orderBy);

            }

            String query = "";

            query = "SELECT count(*) FROM " + this.tableUbicacionPersona
                    + (where != "" || !where.equals("") ? " WHERE " + where : "")
                    + (orderBy != "" || !orderBy.equals("") ? " ORDER BY " + orderBy : "");

            Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: Ubicacion Persona ::: QUERY: " + query);

            Statement statement = connection.createStatement();

            ResultSet result = statement.executeQuery(query);

            Integer cantidadRegristros = 0;

            while (result.next()) {
                cantidadRegristros = result.getInt(1);
            }

            return cantidadRegristros;

        } catch (Exception e) {
            LOG.error(":: ERROR EXCEPTION ubicacionPersonaCount:: "+ e);
            throw e;
        } finally {
            if (!connection.isClosed()) {
                connection.close();
            }
        }

    }

    /**
     * Retorna la ubicacion geografica del aportante, a partir de criterios y
     * parametros especificados por el cliente.
     *
     * @param parametros
     * @param criterios
     * @param tamPagina
     * @param numPagina
     * @return
     * @throws Exception
     */
    public ResultSet ubicacionPersona(List<ParametroTipo> parametros, List<CriterioOrdenamientoTipo> criterios, Integer tamPagina,
            Integer numPagina) throws Exception {

        try {
            final List<ErrorTipo> errorTipoList = new ArrayList<ErrorTipo>();

            Class.forName(this.driverClassName);
            connection = DriverManager.getConnection(
                    this.urlUbicacionPersona,
                    this.usernameUbicacionPersona,
                    this.passwordUbicacionPersona);
            /**
             * CONSULTA
             */
            String where = "";
            String orderBy = "";

            if (parametros != null) {
                for (ParametroTipo parametro : parametros) {
                    if (parametro.getIdLlave() != null && !parametro.getIdLlave().equals("") && parametro.getValValor() != null
                            && !parametro.getValValor().equals("")) {
                        where += where.equals("") ? parametro.getIdLlave() + " = " + "'" + parametro.getValValor()
                                + "'" : " AND " + parametro.getIdLlave() + " = " + "'" + parametro.getValValor() + "'";
                    }
                    if (parametro.getIdLlave() == null || parametro.getIdLlave().equals("") || parametro.getValValor() == null
                            || parametro.getValValor().equals("")) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_BASE_DATOS,
                                "No puede enviar el parametro vacio"));
                    }
                }
    
                LOG.debug("Op: Ubicacion Persona ::: WHERE: " + where);

            }

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }

            if (criterios != null) {
                for (CriterioOrdenamientoTipo criterio : criterios) {
                    if (criterio.getValNombreCampo() != null && !criterio.getValNombreCampo().equals("")) {
                        orderBy += orderBy.equals("") ? criterio.getValNombreCampo() : " , "
                                + criterio.getValNombreCampo();
                    }
                }
  
                LOG.debug("Op: Ubicacion Persona ::: ORDER BY: " + orderBy);

            }

            String query = "";
            if (numPagina != null && tamPagina != null && numPagina > -1 && tamPagina > -1) {

                query = "SELECT * FROM ( "
                        + "SELECT *,ROW_NUMBER() OVER(ORDER BY " + this.tableUbicacionPersona + ".id_directorio) AS rownum "
                        + "FROM " + this.tableUbicacionPersona
                        + (where != "" || !where.equals("") ? " WHERE " + where : "")
                        + ") AS RESULTADO "
                        + "WHERE rownum > " + tamPagina + " * (" + numPagina + "-1) AND rownum <= " + tamPagina + " * " + numPagina + " "
                        + "ORDER BY rownum"
                        + (orderBy != "" || !orderBy.equals("") ? " , " + orderBy : "");

            } else {
                query = "SELECT * FROM " + this.tableUbicacionPersona
                        + (where != "" || !where.equals("") ? " WHERE " + where : "")
                        + (orderBy != "" || !orderBy.equals("") ? " ORDER BY " + orderBy : "");
            }

            Logger.getLogger(SQLConection.class.getName()).log(Level.INFO, "Op: Ubicacion Persona ::: QUERY: " + query);

            Statement statement = connection.createStatement();

            return statement.executeQuery(query);

        } catch (Exception e) {
            LOG.debug(":: ERROR EXCEPTION ubicacionPersona:: " + e);
            throw e;
        }

    }

    public void closeConnection() throws SQLException {
        if (connection != null) {
            if (!connection.isClosed()) {
                connection.close();
            }
        }
    }

    /**
     * Retorna la ubicacion geografica del aportante, a partir de su numero de
     * identificacion y su tipo de identificacion.
     *
     * @param identificacion
     * @param tipoIdentificacion
     * @return
     * @throws Exception
     */
    public ResultSet ubicacionPersona(String identificacion, String tipoIdentificacion) throws Exception {

        List<ParametroTipo> parametros = new ArrayList<ParametroTipo>();

        ParametroTipo parametroNumeroIdentificacion = new ParametroTipo();
        parametroNumeroIdentificacion.setIdLlave("numero_identificacion");
        parametroNumeroIdentificacion.setValValor(identificacion);

        ParametroTipo parametroTipoIdentificacion = new ParametroTipo();
        parametroTipoIdentificacion.setIdLlave("tipo_identificacion");
        parametroTipoIdentificacion.setValValor(tipoIdentificacion);//8012853

        return ubicacionPersona(parametros, null, 1, 1);

    }

    public Integer fuctionStoredCalculaScoring(Denuncia denuncia) throws Exception {

        String SQL = this.queryScoring;

        Class.forName("oracle.jdbc.driver.OracleDriver");
        Connection connection = DriverManager
                .getConnection(this.urlScoring, this.usernameScoring, this.passwordScoring);

        CallableStatement call = connection.prepareCall(SQL);
        call.registerOutParameter(2, Types.INTEGER); // parametro de salida
        call.registerOutParameter(3, Types.VARCHAR); // parametro de salida
        call.setString(1, String.valueOf(denuncia.getIdDenuncia())); // parametro de entrada

        call.execute();

        Integer result = call.getInt(2);

        return result;
    }

    /**
     * Retorna consulta para analisis pago persuasivo y analisis pago aportante
     *
     * @param numIdentificacion
     * @param tipoIdentificacion
     * @param fechaDesde
     * @param fechaHasta
     * @return
     * @throws Exception
     */
    
    public Boolean analisisPagosAportanteNV(String tipoIdentificacion, String numIdentificacion,String tipoIdentificacionCOT, String numIdentificacionCOT, String fechaDesde, String fechaHasta) {
        /*
        
        CREADO POR YESID TAPIAS 29-03-2016
        
        */
        	Connection Conexion  = null;
		DataSource ds =null;
        
                String Resultado ="";
        	try 
		{
                    Context ctx = new InitialContext();
                    ds = (DataSource) ctx.lookup("jdbc/PILA");
                    Conexion = ds.getConnection();

                     try 
                     {

                            CallableStatement cstmt;
                            cstmt = Conexion.prepareCall("{CALL dbo.SpAnalizarPagosAportanteNV(?,?,?,?,?,?,?)}");  
                            
                            cstmt.setString(1, tipoIdentificacion);
                            cstmt.setString(2, numIdentificacion);
                            cstmt.setString(3, tipoIdentificacionCOT);
                            cstmt.setString(4, numIdentificacionCOT);
                            cstmt.setString(5, fechaDesde);
                            cstmt.setString(6, fechaHasta);
                            cstmt.registerOutParameter (7, Types.VARCHAR);
                            cstmt.execute();
                            Resultado = cstmt.getString(7);
                            cstmt.close();  
    
                    } catch (Exception e) {
                         LOG.debug("sqlconextion Exception error = "+e);
                    }
                
                
                }catch(Exception e)
                {   
                     LOG.debug("ERROR DE CONEXION NUEVA= "+e);
                }
           return Resultado.equals("SI") ? true : false;
    }

    
    
    public ResultSet analisisPagosAportante(String tipoIdentificacion, String numIdentificacion,  String fechaDesde, String fechaHasta) throws Exception {
            ResultSet result = null;
            try 
            {

                Class.forName(this.driverClassName);
                Connection connection = DriverManager.getConnection(this.urlAportante, this.usernameAportante, this.passwordAportante);

                String sp = queryAportante;
                CallableStatement cs = connection.prepareCall(sp,  ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
             //     cs.registerOutParameter(1, OracleTypes.CURSOR);

                cs.setString("TIPO_ID_APORTANTE", tipoIdentificacion); //
                cs.setString("NUMERO_ID_APORTANTE", numIdentificacion); // 

                cs.setString("FECHA_DESDE", fechaDesde);//"FECHA_DESDE"
                cs.setString("FECHA_HASTA", fechaHasta);//"FECHA_HASTA"

                LOG.debug("Op: AnalizarPagos ::: QUERY: " + sp);
                
                 result = cs.executeQuery();
            } 
            catch (Exception e) 
            {
                LOG.debug("SQL CONEXION ERROR: "+e);
                throw e;
            }

        return result;
    }

    
    public ResultSet consultaLiquidador(int modo, String idExpediente, String tipoIdientificacion, String numIdentificacion, String parametro) throws Exception {
        ResultSet result = null;
        
        try {
            System.out.println("DATA: modo:" + modo + " idExpediente: " + idExpediente
                    + " tipoIdientificacion: " + tipoIdientificacion + " numIdentificacion: " + numIdentificacion
                    + " parametro: " + parametro);

            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(this.urlLiquidador, this.usernameLiquidador, this.passwordLiquidador);

            String resultado = "";
            
            //MOD ANDRES: 27/10/2016
            //String sql = queryLiquidador; 
            String sql = "BEGIN usp_hoja_trabajo2(?,?); END;";

            
            LOG.debug("INFO generarExcel QUERY 1: " + sql);
            
            
            CallableStatement cs = connection.prepareCall(sql);
            cs.setInt(1, Integer.parseInt(parametro));
            cs.registerOutParameter(2, java.sql.Types.VARCHAR);
            cs.execute();
            resultado = cs.getString(2);
            
            LOG.debug("INFO generarExcel QUERY 2: " + resultado);
            
            cs = connection.prepareCall(resultado);
            cs.execute();
            
            LOG.debug("INFO generarExcel QUERY 3: " + cs.getResultSet().getFetchSize());
            
            result = cs.getResultSet();
              

        } catch (Exception e) {
                 
            LOG.debug("EXCEPCION ERROR generarExcel " + e);
            throw e;
        }
        
        return result;
    }

}
