package co.gov.ugpp.parafiscales.servicios.util.files;

import co.gov.ugpp.parafiscales.persistence.entity.AbstractEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Util for files and services in liq.
 *
 * @author franzjr
 */
@Entity
@Table(name = "LIQ_CAMPO_CARGA")
public class CampoCarga extends AbstractEntity<Long> {

    @Column(name = "ORDEN")
    private int orden;

    @Column(name = "NOMBRE_COLUMNA")
    private String nombreColumna;

    @Column(name = "TIPO_DATO")
    private String tipoDato;

    @Column(name = "ES_OBLIGATORIO")
    private boolean obligatorio = false;

    @Column(name = "MIN_LONGITUD")
    private int minLongitud;

    @Column(name = "MAX_LONGITUD")
    private int maxLongitud;

    @Column(name = "VALOR")
    private String valor = "";

    @JoinColumn(name = "ESTRUCTURA", referencedColumnName = "ID_ESTRUCTURA")
    @ManyToOne(fetch = FetchType.LAZY)
    private Estructura estructura;

    public Estructura getEstructura() {
        return estructura;
    }

    public void setEstructura(Estructura estructura) {
        this.estructura = estructura;
    }

    @Id
    @NotNull
    private Long id;

    public CampoCarga() {
    }

    public CampoCarga(String nombreColumna, String tipoDato,
            boolean obligatorio, int maxLongitud) {
        this.nombreColumna = nombreColumna;
        this.tipoDato = tipoDato;
        this.obligatorio = obligatorio;
        this.maxLongitud = maxLongitud;
    }

    public CampoCarga(String nombreColumna, String tipoDato,
            boolean obligatorio, int minLongitud, int maxLongitud) {
        this.nombreColumna = nombreColumna;
        this.tipoDato = tipoDato;
        this.obligatorio = obligatorio;
        this.minLongitud = minLongitud;
        this.maxLongitud = maxLongitud;
    }

    public CampoCarga(String nombreColumna, String tipoDato,
            boolean obligatorio, int minLongitud, int maxLongitud, String valor) {
        this.nombreColumna = nombreColumna;
        this.tipoDato = tipoDato;
        this.obligatorio = obligatorio;
        this.minLongitud = minLongitud;
        this.maxLongitud = maxLongitud;
        this.valor = valor;
    }

    public int getOrden() {
        return this.orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public String getNombreColumna() {
        return this.nombreColumna;
    }

    public void setNombreColumna(String nombreColumna) {
        this.nombreColumna = nombreColumna.toUpperCase();
    }

    public String getTipoDato() {
        return this.tipoDato;
    }

    public void setTipoDato(String tipoDato) {
        this.tipoDato = tipoDato;
    }

    public boolean isObligatorio() {
        return this.obligatorio;
    }

    public void setObligatorio(boolean obligatorio) {
        this.obligatorio = obligatorio;
    }

    public int getMaxLongitud() {
        return this.maxLongitud;
    }

    public void setMaxLongitud(int maxLongitud) {
        this.maxLongitud = maxLongitud;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getMinLongitud() {
        return minLongitud;
    }

    public void setMinLongitud(int minLongitud) {
        this.minLongitud = minLongitud;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
