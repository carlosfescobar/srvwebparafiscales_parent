package co.gov.ugpp.parafiscales.servicios.util.files;

import co.gov.ugpp.parafiscales.persistence.entity.AbstractEntity;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author franzjr
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "estructura.findByFormatoPK",
            query = "SELECT f FROM Estructura f WHERE f.formato.formatoPK.idFormato = :idFormato AND f.formato.formatoPK.idVersion = :idVersion")})
@Table(name = "LIQ_ESTRUCTURA")
public class Estructura extends AbstractEntity<Long> {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "estructura", fetch = FetchType.LAZY)
    private List<CampoCarga> campos;

    public List<CampoCarga> getCampos() {
        return campos;
    }

    public void setCampos(List<CampoCarga> campos) {
        this.campos = campos;
    }

    public Estructura() {
    }

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_ESTRUCTURA")
    private Long id;

    @Column(name = "ESTRUCTURA")
    private String estructura;

    public String getEstructura() {
        return estructura;
    }

    public void setEstructura(String estructura) {
        this.estructura = estructura;
    }

    @ManyToOne
    @JoinColumns({
        @JoinColumn(name = "ID_FORMATO", referencedColumnName = "ID_FORMATO"),
        @JoinColumn(name = "ID_VERSION", referencedColumnName = "ID_VERSION")
    })
    private Formato formato;

    public Formato getFormato() {
        return formato;
    }

    public void setFormato(Formato formato) {
        this.formato = formato;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estructura)) {
            return false;
        }
        Estructura other = (Estructura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.gov.ugpp.parafiscales.servicios.util.files.Estructura[ id=" + id + " ]";
    }

}
