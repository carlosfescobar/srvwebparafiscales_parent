package co.gov.ugpp.parafiscales.servicios.util.files;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Util Srv file.
 * @author franzjr
 */

public class EstructuraCont implements Serializable{

    private ArrayList<CampoCarga> listaCampos;
    private static final EstructuraCont instance = new EstructuraCont();

    public static EstructuraCont getInstance() {
        return instance;
    }

    public EstructuraCont() {
        listaCampos = new ArrayList<CampoCarga>();
        listaCampos.add(new CampoCarga("Año","NUMBER",true,9999));
        listaCampos.add(new CampoCarga("No. Cuenta Contable","NUMBER",false,150000000));
        listaCampos.add(new CampoCarga("Nombre Cuenta Contable","VARCHAR2",false,150));
        listaCampos.add(new CampoCarga("Saldo Inicial","NUMBER",true,1000000000));
        listaCampos.add(new CampoCarga("Saldo Final","NUMBER",true,1000000000));
        listaCampos.add(new CampoCarga("Vr. Neto Movimiento","NUMBER",false,1000000000));
    }

    public ArrayList<CampoCarga> getListaCampos() {
        return listaCampos;
    }

    public void setListaCampos(ArrayList<CampoCarga> listaCampos) {
        this.listaCampos = listaCampos;
    }
    
    
}
