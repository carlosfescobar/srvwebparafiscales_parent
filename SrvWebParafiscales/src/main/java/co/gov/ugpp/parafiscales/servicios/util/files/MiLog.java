package co.gov.ugpp.parafiscales.servicios.util.files;

/**
 * Util NA. Srv Liq.
 *
 * @author franzjr
 */
public class MiLog {

    private int fila;
    private int columna;
    private String razon;
    private String valor;

    public MiLog(int fila, int columna, String razon, String valor) {
        super();
        this.fila = fila;
        this.columna = columna;
        this.razon = razon;
        this.valor = valor;
    }

    public int getFila() {
        return fila;
    }

    public void setFila(int fila) {
        this.fila = fila;
    }

    public int getColumna() {
        return columna;
    }

    public void setColumna(int columna) {
        this.columna = columna;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String toString() {
        return "[" + this.fila + "," + this.columna + "] <razon=" + this.razon
                + "> <valor = " + this.valor + ">";
    }
}
