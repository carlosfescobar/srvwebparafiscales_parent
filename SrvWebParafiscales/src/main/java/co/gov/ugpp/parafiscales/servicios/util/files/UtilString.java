package co.gov.ugpp.parafiscales.servicios.util.files;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Util Srv Liq. Add Java Doc. DONE
 * @author franzjr
 */

public class UtilString {

	private static String palabras[] = { "EL", "LA", "LOS", "LAS", "EN", "POR",
			"PARA", "DE", "CON", "COMO", "DEL", "DONDE" };

        /**
         * convierte fecha
         * @param dat
         * @return 
         */
	public static String convertirFecha(Date dat) {
		if (dat == null) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(dat);
	}

	/**
         * convierte fecha y hora
         * @param dat
         * @return 
         */
	public static String convertirFechaTiempo(Date dat) {
		if (dat == null) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(dat);
	}

	/**
         * convierte hora
         * @param dat
         * @return 
         */
	public static String convertirTiempo(Date dat) {
		if (dat == null) {
			return "";
		}
		SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
		return format.format(dat);
	}
	
	/**
         * utilidad para consultas
         * @param consulta
         * @param variable
         * @param parametro
         * @return 
         */
	public static String addAndToQuery(String consulta, String variable,
			String parametro) {
		Vector<String> vector = splitString(consulta);
		String nuevaConsulta = "(" + " ( " + variable + "." + parametro + " )"
				+ " LIKE ";

		for (int i = 0; i < vector.size(); i++) {
			nuevaConsulta = nuevaConsulta + "'%" + vector.elementAt(i) + "%' ";
			if (i < (vector.size() - 1)) {
				nuevaConsulta = nuevaConsulta + "AND " + " ( " + variable + "."
						+ parametro + " )" + " LIKE ";
			} else {
				nuevaConsulta = nuevaConsulta + ")";
			}
		}
		return nuevaConsulta;

	}

	/**
         * utilidad para consultas
         * @param consulta
         * @param variable
         * @param parametro
         * @return 
         */
	public static String addOrToQuery(String consulta, String variable,
			String parametro) {
		Vector<String> vector = splitString(consulta);
		String nuevaConsulta = "(" + " ( " + variable + "." + parametro + " )"
				+ " LIKE ";

		for (int i = 0; i < vector.size(); i++) {
			nuevaConsulta = nuevaConsulta + "'%" + vector.elementAt(i) + "%' ";
			if (i < (vector.size() - 1)) {
				nuevaConsulta = nuevaConsulta + "OR " + " ( " + variable + "."
						+ parametro + " )" + " LIKE ";
			} else {
				nuevaConsulta = nuevaConsulta + ")";
			}
		}
		return nuevaConsulta;

	}

	/**
         * Parte una cadena de palabras
         * @param cadena
         * @return 
         */
	private static Vector<String> splitString(String cadena) {
		Vector<String> vec = new Vector<String>();
		StringTokenizer token = new StringTokenizer(cadena);
		while (token.hasMoreTokens()) {
			String cad = token.nextToken().toUpperCase();
			if (!esPalabraComun(cad)) {
				vec.add(cad.toLowerCase());
			}
		}
		return vec;

	}

	/**
         * valida si la palabra es comun
         * @param cad
         * @return 
         */
	private static boolean esPalabraComun(String cad) {
		for (int i = 0; i < palabras.length; i++)
			if (cad.equals(palabras[i]))
				return true;
		return false;
	}

	/**
         * quita palabras comunes de una cadena
         * @param cad
         * @return 
         */
	public static String tokenizar(String cad) {
		cad = cad.replaceAll("-", "");
		System.out.println("CADENA SIN - :" + cad);
		StringBuffer sb = new StringBuffer("");
		for (String s : splitString(cad)) {
			sb.append(s + " ");
		}
		return sb.toString();
	}

	/**
         * Permite obtener formatos de tiempo para servicios de tiempo Quartz
         * @param fechaReserva
         * @return 
         */
	@SuppressWarnings("deprecation")
	public static String obtenerFormatoCron(Date fechaReserva) {
		String reservaCron = "";
		reservaCron += fechaReserva.getSeconds() + " ";
		reservaCron += fechaReserva.getMinutes() + " ";
		reservaCron += fechaReserva.getHours() + " ";
		reservaCron += (fechaReserva.getDate()) + " ";
		reservaCron += (fechaReserva.getMonth() + 1) + " ";
		reservaCron += "?";
		return reservaCron;
	}
	
	
	@SuppressWarnings({ "deprecation", "static-access" })
	public static int calcularEdad(Date fechaNacimiento){
		System.out.println("************* INGRESO AL METODO CALCULAR EDAD ******");
		int edad = 0;
		Calendar hoy = Calendar.getInstance();
		Date date = fechaNacimiento;
		Calendar diaNacimiento = Calendar.getInstance();
		diaNacimiento.set(date.getYear()+1900, date.getMonth(), date.getDate());
		System.out.println("****************** LA FECHA DE HOY ES: " + hoy.get(Calendar.YEAR));
		System.out.println("****************** LA FECHA DE NACIMIENTO ES: " + diaNacimiento.get(Calendar.YEAR));
		if(hoy.get(Calendar.MONTH) > diaNacimiento.get(Calendar.MONTH) || 
				(hoy.get(Calendar.MONTH) == diaNacimiento.get(Calendar.MONTH) && 
						hoy.get(Calendar.DATE) >= diaNacimiento.get(Calendar.DATE))){
			edad = hoy.get(Calendar.YEAR) - diaNacimiento.get(Calendar.YEAR);
		}else {
			edad = (hoy.get(Calendar.YEAR) - diaNacimiento.get(Calendar.YEAR)) - 1;
		}
		System.out.println("************* LA EDAD ES: " + edad);
		return edad;
	}

}