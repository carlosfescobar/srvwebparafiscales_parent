package co.gov.ugpp.parafiscales.servicios.util.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.model.UploadedFile;

/**
 * Util Srv Liq.
 *
 * @author franzjr
 */
public class ValidarArchivoXlsx {

    private List<CampoCarga> estructura;
    private List<MiLog> logs;
    private String msn = "";

    public ValidarArchivoXlsx(List<CampoCarga> estructura) {
        this.estructura = estructura;
        this.logs = new ArrayList<MiLog>();
    }

    public void validar(UploadedFile arch) {
        try {
            InputStream inp = arch.getInputstream();
            Workbook wb;
            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            validarEncabezado(sheet);
            validarColumnas(sheet);
            validarEstructura(sheet);
        } catch (FileNotFoundException e) {
            logs.add(new MiLog(0, 0,
                    "Archivo no encontrado",
                    ""));
        } catch (IOException e) {
            logs.add(new MiLog(0, 0,
                    "Error de entrada o salida",
                    ""));
        } catch (Exception e) {
            logs.add(new MiLog(0, 0,
                    "Ocurrio un error al validar el archivo", ""));
        }
    }

    public void validarEncabezado(Sheet sheet) {
        Row rw = sheet.getRow(0);
        if (rw != null) {
            Iterator<CampoCarga> it = estructura.iterator();
            for (int i = 0; i < estructura.size(); i++) {
                Cell cell = rw.getCell(i, Row.CREATE_NULL_AS_BLANK);
                CampoCarga cm = it.next();
                if (cell == null) {
                    logs.add(new MiLog(1, i + 1, "Celda vacia", "NULL"));
                } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
                    String cad = cell.getStringCellValue().trim();
                    if (!cad.equalsIgnoreCase(cm.getNombreColumna())) {
                        System.out.println("no coincide el encabezado");
                        logs.add(new MiLog(1, i + 1,
                                "No coincide con el formato del encabezado",
                                leerCelda(cell)));
                    }
                }
            }
        } else {
            logs.add(new MiLog(0, 0,
                    "El archivo debe tener cabezera", ""));
        }
    }

    private void validarColumnas(Sheet sheet) {
        int last = sheet.getLastRowNum();
        int maxCol = estructura.size();
        for (int j = 2; j <= last + 1; j++) {
            Row row = sheet.getRow(j - 1);
            if (row != null) {
                Iterator<Cell> iter = row.iterator();
                int con = 0;
                while (iter.hasNext()) {
                    con++;
                    iter.next();
                }
                if (con > maxCol) {
                    logs
                            .add(new MiLog(
                                            j,
                                            0,
                                            "La cantidad de columnas ingresadas en este registro es mayor que las aceptadas por la estructura.",
                                            null));
                }
            }
        }
    }

    public void validarEstructura(Sheet sheet) {
        int last = sheet.getLastRowNum();
        int maxCol = estructura.size();
        int delrow = 0;
        Row lastrow = sheet.getRow(1);
        do {
            lastrow = sheet.getRow(last);
            if (lastrow == null) {
                delrow = maxCol;
            } else {
                delrow = 0;
                for (int i = 0; i < estructura.size(); i++) {
                    if (lastrow.getCell(i, Row.CREATE_NULL_AS_BLANK).getCellType() == Cell.CELL_TYPE_BLANK) {
                        delrow++;
                    }
                }
            }
            if (delrow >= maxCol) {
                if (last == 0) {
                    delrow = maxCol - 1;
                } else {
                    last--;
                }
            }
        } while (delrow >= maxCol);

        if (last < 1) {
            logs.add(new MiLog(0, 0,
                    "El archivo debe tener por lo menos un registro", ""));
        } else {
            for (int j = 2; j <= last + 1; j++) {
                Row row = sheet.getRow(j - 1);
                Iterator<CampoCarga> it = estructura.iterator();
                for (int i = 0; i < estructura.size(); i++) {
                    Cell cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
                    if (!validarCelda(cell, it.next())) {
                        logs.add(new MiLog(row.getRowNum() + 1, i + 1, getMsn(),
                                leerCelda(cell)));
                    }

                }
            }
        }
    }

    public boolean validarCelda(Cell cell, CampoCarga colu) {
        if (!validarObligatorio(cell)) {
            if (colu.isObligatorio()) {
                return false;
            } else {
                return true;
            }
        }
        if ((colu.getTipoDato().equals("VARCHAR2") || colu.getTipoDato()
                .equals("CHAR"))) {
            return validarCadena(cell, colu.getMaxLongitud());
        }

        if (colu.getTipoDato().equals("NUMBER")) {
            return validarNumero(cell, colu);
        }
        if (colu.getTipoDato().equals("DATE")) {
            return validarFecha(cell);
        }
        if (colu.getTipoDato().equals("BOOLEAN")) {
            return validarBoolean(cell);
        }
        return false;
    }

    private boolean validarObligatorio(Cell cell) {
        if (cell == null) {
            setMsn("El valor de la celda es obligatorio");
            return false;
        } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK) {
            setMsn("El valor de la celda es obligatorio");
            return false;
        }
        return true;
    }

    private boolean validarCadena(Cell cell, int lon) {
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {
            String st = cell.getStringCellValue();
            if (st.length() <= lon) {
                return true;
            } else {
                setMsn("La cadena es demasiado grande la logitud debe ser: "
                        + lon);
                return false;
            }
        } else {
            setMsn("No coinciden el tipo de dato, debe ser una cadena de texto");
            return false;
        }
    }

    private boolean validarNumero(Cell cell, CampoCarga colu) {
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            if (cell.getNumericCellValue() <= colu.getMaxLongitud() && cell.getNumericCellValue() >= colu.getMinLongitud()) {
                return true;
            } else {
                setMsn("No coinciden el tipo de dato, debe ser un numero entre " + colu.getMinLongitud() + " y " + colu.getMaxLongitud());
            }
        } else {
            setMsn("No coinciden el tipo de dato, debe ser un numero ");
        }
        return false;

    }

    private boolean validarBoolean(Cell cell) {
        if (cell.getStringCellValue().equalsIgnoreCase("x")) {
            return true;
        }
        setMsn("No coinciden el tipo de dato, debe ser x o vacio");
        return false;
    }

    private boolean validarFecha(Cell cell) {
        if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
            if (DateUtil.isCellDateFormatted(cell)) {
                return true;
            }
        }
        setMsn("No coinciden el tipo de dato, debe ser una fecha");
        return false;
    }

    public String leerCelda(Cell cell) {
        String ret = "";
        if (cell != null) {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    ret = cell.getRichStringCellValue().getString();
                    break;
                case Cell.CELL_TYPE_NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        ret = UtilString.convertirFecha(cell.getDateCellValue());
                    } else {
                        ret = "" + cell.getNumericCellValue();
                    }
                    break;
                case Cell.CELL_TYPE_BOOLEAN:
                    if (cell.getBooleanCellValue()) {
                        ret = "x";
                    } else {
                        ret = "";
                    }
                    break;
                case Cell.CELL_TYPE_BLANK:
                    break;
            }
        }
        return ret;
    }

    public List<CampoCarga> getEstructura() {
        return estructura;
    }

    public void setEstructura(List<CampoCarga> estructura) {
        this.estructura = estructura;
    }

    public List<MiLog> getLogs() {
        return logs;
    }

    public void setLogs(List<MiLog> logs) {
        this.logs = logs;
    }

    /**
     * @return the msn
     */
    public String getMsn() {
        return msn;
    }

    /**
     * @param msn the msn to set
     */
    public void setMsn(String msn) {
        this.msn = msn;
    }
}
