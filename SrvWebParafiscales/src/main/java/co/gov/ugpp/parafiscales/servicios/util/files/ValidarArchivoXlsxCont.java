package co.gov.ugpp.parafiscales.servicios.util.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.model.UploadedFile;

/**
 * Util Srv Liq.
 *
 * @author franzjr
 */
public class ValidarArchivoXlsxCont extends ValidarArchivoXlsxBase {

    private int sizeheader = 1;

    public ValidarArchivoXlsxCont(List<CampoCarga> estructura) {
        super(estructura);
    }

    public void validar(UploadedFile arch) {
        try {
            InputStream inp = arch.getInputstream();
            Workbook wb;
            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            super.validarEncabezado(sheet);
            validarColumnas(sheet);
            validarEstructura(sheet);
        } catch (FileNotFoundException e) {
            getLogs().add(new MiLog(0, 0,
                    "Archivo no encontrado",
                    ""));
        } catch (IOException e) {
            getLogs().add(new MiLog(0, 0,
                    "Error de entrada o salida",
                    ""));
        } catch (Exception e) {
            getLogs().add(new MiLog(0, 0,
                    "Ocurrio un error al validar el archivo", e.getMessage()));
        }
    }

    public void validar(InputStream inp) {
        try {
            Workbook wb;
            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            super.validarEncabezado(sheet);
            validarColumnas(sheet);
            validarEstructura(sheet);
        } catch (FileNotFoundException e) {
            getLogs().add(new MiLog(0, 0,
                    "Archivo no encontrado",
                    ""));
        } catch (IOException e) {
            getLogs().add(new MiLog(0, 0,
                    "Error de entrada o salida",
                    ""));
        } catch (Exception e) {
            getLogs().add(new MiLog(0, 0,
                    "Ocurrio un error al validar el archivo", e.getMessage()));
        }
    }

    private void validarColumnas(Sheet sheet) {
        int last = sheet.getLastRowNum();
        int maxCol = getEstructura().size();
        for (int j = sizeheader + 1; j <= last + 1; j++) {
            Row row = sheet.getRow(j - 1);
            if (row != null) {
                Iterator<Cell> iter = row.iterator();
                int con = 0;
                while (iter.hasNext()) {
                    con++;
                    iter.next();
                }
                if (con > maxCol) {
                    getLogs()
                            .add(new MiLog(
                                            j,
                                            0,
                                            "La cantidad de columnas ingresadas en este registro es mayor que las aceptadas por la estructura.",
                                            null));
                }
            }
        }
    }

    public void validarEstructura(Sheet sheet) {
        int last = sheet.getLastRowNum();
        int maxCol = getEstructura().size();
        int delrow = 0;
        Row lastrow = sheet.getRow(1);
        do {
            lastrow = sheet.getRow(last);
            if (lastrow == null) {
                delrow = maxCol;
            } else {
                delrow = 0;
                for (int i = 0; i < getEstructura().size(); i++) {
                    if (lastrow.getCell(i, Row.CREATE_NULL_AS_BLANK).getCellType() == Cell.CELL_TYPE_BLANK) {
                        delrow++;
                    }
                }
            }
            if (delrow >= maxCol) {
                if (last == 0) {
                    delrow = maxCol - 1;
                } else {
                    last--;
                }
            }
        } while (delrow >= maxCol);

        if (last < 1) {
            getLogs().add(new MiLog(0, 0,
                    "El archivo debe tener por lo menos un registro", ""));
        } else {
            for (int j = sizeheader + 1; j <= last + 1; j++) {
                Row row = sheet.getRow(j - 1);
                Iterator<CampoCarga> it = getEstructura().iterator();
                for (int i = 0; i < getEstructura().size(); i++) {
                    Cell cell = row.getCell(i, Row.CREATE_NULL_AS_BLANK);
                    if (!super.validarCelda(cell, it.next())) {
                        getLogs().add(new MiLog(row.getRowNum() + 1, i + 1, getMsn(),
                                leerCelda(cell)));
                    }

                }
            }
        }
    }
}
