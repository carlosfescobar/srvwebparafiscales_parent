package co.gov.ugpp.parafiscales.servicios.util.files;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.primefaces.model.UploadedFile;

/**
 * Util Srv Liq.
 *
 * @author franzjr
 */
public class ValidarArchivoXlsxNomina extends ValidarArchivoXlsxBase {

    private int sizeheader = 6;
    private int _inicio_columnas_dinamicas = 39;
    int _iterador_dinamicas = 0;

    public ValidarArchivoXlsxNomina(List<CampoCarga> estructura) {
        super(estructura);
    }

    public void validar(UploadedFile arch) {
        try {
            InputStream inp = arch.getInputstream();
            Workbook wb;
            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            validarEncabezado(sheet);
            validarColumnas(sheet);
            validarEstructura(sheet);
            validarDinamicas(sheet);
        } catch (FileNotFoundException e) {
            getLogs().add(new MiLog(0, 0,
                    "Archivo no encontrado",
                    ""));
        } catch (IOException e) {
            getLogs().add(new MiLog(0, 0,
                    "Error de entrada o salida",
                    ""));
        } catch (Exception e) {
            getLogs().add(new MiLog(0, 0,
                    "Ocurrio un error al validar el archivo", e.getMessage()));
        }
    }

    public void validar(InputStream inp) {
        try {
            Workbook wb;
            wb = WorkbookFactory.create(inp);
            Sheet sheet = wb.getSheetAt(0);
            validarEncabezado(sheet);
            validarColumnas(sheet);
            validarEstructura(sheet);
            validarDinamicas(sheet);
        } catch (FileNotFoundException e) {
            getLogs().add(new MiLog(0, 0,
                    "Archivo no encontrado",
                    ""));
        } catch (IOException e) {
            getLogs().add(new MiLog(0, 0,
                    "Error de entrada o salida",
                    ""));
        } catch (Exception e) {
            getLogs().add(new MiLog(0, 0,
                    "Ocurrio un error al validar el archivo", e.getMessage()));
        }
    }

    public void validarEncabezado(Sheet sheet) {

        try {
            System.out.println("@DEBUG: validarEncabezado : INICIO");

            Row rw = sheet.getRow(sizeheader - 1);
            if (rw != null) {
                Iterator<CampoCarga> it = getEstructura().iterator();
                int i = 0;

                boolean _post_dinamicas = false;

                while (i < getEstructura().size()) {

                    Cell cell = rw.getCell(i + _iterador_dinamicas, Row.CREATE_NULL_AS_BLANK);

                    System.out.println("@DEBUG: validarEncabezado (i): " + i);

                    if (cell == null) {

                        getLogs().add(new MiLog(sizeheader, i + 1, "Celda vacia", "NULL"));

                    } else if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

                        String cad = cell.getStringCellValue().trim();

                        if ((i < _inicio_columnas_dinamicas) || _post_dinamicas) {
                            CampoCarga cm = it.next();
                            if (!cad.equalsIgnoreCase(cm.getNombreColumna())) {
                                System.out.println("no coincide el encabezado");
                                getLogs().add(new MiLog(sizeheader, i + _iterador_dinamicas + 1,
                                        "No coincide con el formato del encabezado",
                                        leerCelda(cell)));
                            }
                        } else {
                            try {
                                while (!_post_dinamicas) {

                                    cell = rw.getCell(i + _iterador_dinamicas, Row.CREATE_NULL_AS_BLANK);

                                    cad = cell.getStringCellValue().trim();

                                    if (!cad.equals("TIPO INGRESO")) {
                                        _iterador_dinamicas++;
                                    } else {
                                        _post_dinamicas = true;
                                        i--; //se resta una unidad a "i" para que entre nuevamente al ciclo y ejecute la validacion del campo "tipo_ingreso"
                                    }
                                }
                            } catch (Exception _exception_inner) {

                                getLogs().add(new MiLog(sizeheader, _inicio_columnas_dinamicas + 1,
                                        "No coincide con el formato del encabezado", "No es posible determinar el inicio y final de las columnas dinámicas."));

                                System.out.println("@DEBUG: validarEncabezado : Error :: " + _exception_inner.getMessage());
                                continue;
                            }
                        }
                    }

                    i++;
                }
            } else {
                getLogs().add(new MiLog(0, 0,
                        "El archivo debe tener cabezera", ""));
            }

            if (getLogs().size() < 1) {
                System.out.println("@DEBUG: validarEncabezado : OK");
            }
        } catch (Exception _exception) {
            System.out.println("@DEBUG: validarEncabezado : Error :: " + _exception.getMessage());
        }
    }

    private void validarColumnas(Sheet sheet) {
        try {
            int last = sheet.getLastRowNum();
            int maxCol = getEstructura().size();
            int i = 0;
            while (i < getEstructura().size()) {
                Row row = null;

                if (i < _inicio_columnas_dinamicas) {
                    row = sheet.getRow(i - 1);
                } else {
                    row = sheet.getRow(i + _iterador_dinamicas - 1);
                }

                if (row != null) {
                    Iterator<Cell> iter = row.iterator();
                    int con = 0;
                    while (iter.hasNext()) {
                        con++;
                        iter.next();
                    }
                    if ((con - _iterador_dinamicas) > maxCol) {
                        getLogs().add(new MiLog(i, 0,
                                "La cantidad de columnas ingresadas en este registro es mayor que las aceptadas por la estructura 333.",
                                null));
                    }
                }
                i++;
            }
        } catch (Exception e) {
            /*getLogs().add(new MiLog(0, 0,
             "Ocurrio un error al validar el archivo", e.getMessage()));*/
            System.out.println("@DEBUG: validarColumnas : Error :: " + e.getMessage());
        }
    }

    public void validarEstructura(Sheet sheet) {
        int last = sheet.getLastRowNum();
        int maxCol = getEstructura().size();
        int delrow = 0;
        Row lastrow = sheet.getRow(1);

        do {
            lastrow = sheet.getRow(last);
            if (lastrow == null) {
                delrow = maxCol;
            } else {
                delrow = 0;
                for (int i = 0; i < getEstructura().size(); i++) {
                    if (lastrow.getCell(i, Row.CREATE_NULL_AS_BLANK).getCellType() == Cell.CELL_TYPE_BLANK) {
                        delrow++;
                    }
                }
            }
            if (delrow >= maxCol) {
                if (last == 0) {
                    delrow = maxCol - 1;
                } else {
                    last--;
                }
            }
        } while (delrow >= maxCol);

        if (last < 1) {
            getLogs().add(new MiLog(0, 0,
                    "El archivo debe tener por lo menos un registro", ""));
        } else {

            Row _row_header = sheet.getRow(sizeheader - 1);
            String _temporal_string = "";
            int _iterador_columnas = 0;
            String _tipo_ingreso = "";
            String _ing = "";
            String _ret = "";
            String _fecha_costo = "";

            for (int j = sizeheader + 1; j <= last + 1; j++) {
                Row row = sheet.getRow(j - 1);

                Iterator<CampoCarga> it = getEstructura().iterator();
                int i = 0;
                while (i < getEstructura().size()) {
                    //for (int i = 0; i < getEstructura().size(); i++) {
                    Cell cell = null;

                    if (i < _inicio_columnas_dinamicas) {
                        _iterador_columnas = i;
                    } else {
                        _iterador_columnas = i + _iterador_dinamicas;
                    }

                    cell = row.getCell(_iterador_columnas, Row.CREATE_NULL_AS_BLANK);

                    if (!validarCelda(cell, it.next())) {

                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, getMsn(),
                                leerCelda(cell)));
                    } else {
                        Cell _cell_referencia = null;

                        switch (i) {

                            case 1:
                            case 9:
                                _temporal_string = leerCelda(cell);
                                _temporal_string = _temporal_string.trim();
                                if (!_temporal_string.equalsIgnoreCase("Cedula de Ciudadania") && !_temporal_string.equalsIgnoreCase("Nit")
                                        && !_temporal_string.equalsIgnoreCase("Cedula de Extranjera") && !_temporal_string.equalsIgnoreCase("Pasaporte")) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Valor incorrecto. Valores posibles ('Cedula de Ciudadania','Nit','Cedula de Extranjera','Pasaporte') ",
                                            _temporal_string));
                                }
                                break;

                            case 22:
                            case 38:

                                _temporal_string = leerCelda(cell);
                                if (!_temporal_string.equalsIgnoreCase("x") && !_temporal_string.equalsIgnoreCase("")) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Valor incorrecto. Valores posibles ('X','') ",
                                            _temporal_string));
                                }
                                break;

                            case 17:
                                _ing = leerCelda(cell);
                            case 18:
                                if (_ing.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 19:
                                _ret = leerCelda(cell);
                                break;
                            case 20:
                                if (_ret.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 21:

                                break;
                            case 23:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 24:
                                _cell_referencia = row.getCell(_iterador_columnas - 2, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 25:

                            case 26:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 27:
                                _cell_referencia = row.getCell(_iterador_columnas - 2, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 28:

                            case 29:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 30:
                                _cell_referencia = row.getCell(_iterador_columnas - 2, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 31:

                            case 32:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 33:
                                _cell_referencia = row.getCell(_iterador_columnas - 2, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 34:

                            case 35:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }

                                break;
                            case 36:
                                _cell_referencia = row.getCell(_iterador_columnas - 1, Row.CREATE_NULL_AS_BLANK);
                                _temporal_string = leerCelda(_cell_referencia);

                                if (_temporal_string.equalsIgnoreCase("x")) {
                                    _temporal_string = leerCelda(cell);
                                    if (_temporal_string.equalsIgnoreCase("")) {
                                        getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que su predecesor esta marcado con 'X' ",
                                                _temporal_string));
                                    }
                                }
                                break;
                            case 37:
                            case 39:
                                _tipo_ingreso = leerCelda(cell);
                                break;

                            case 40:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && !_tipo_ingreso.equals("")) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que TIPO INGRESO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;

                            case 41:
                                _temporal_string = leerCelda(cell);
                                _temporal_string = _temporal_string.trim();
                                if (!_temporal_string.equalsIgnoreCase("Cedula de Ciudadania") && !_temporal_string.equalsIgnoreCase("Nit")
                                        && !_temporal_string.equalsIgnoreCase("Cedula de Extranjera") && !_temporal_string.equalsIgnoreCase("Pasaporte")
                                        && _tipo_ingreso.equals("Contrato")) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Valor incorrecto. Valores posibles ('Cedula de Ciudadania','Nit','Cedula de Extranjera','Pasaporte') ",
                                            _temporal_string));
                                }
                                break;
                            case 42:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "1 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'.",
                                            _temporal_string));
                                }
                                break;
                            case 43:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "2 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'.",
                                            _temporal_string));
                                }
                                break;
                            case 44:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "3 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'." + _temporal_string,
                                            _temporal_string));
                                }
                                break;
                            case 45:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "4 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'.",
                                            _temporal_string));
                                }
                                break;
                            case 46:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "5 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'.",
                                            _temporal_string));
                                }
                                break;
                            case 47:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (_tipo_ingreso.equalsIgnoreCase("Contrato"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "6 Campo obligatorio, ya que 'TIPO INGRESO' es igual a 'Contrato'.",
                                            _temporal_string));
                                }
                                break;

                            case 49:
                                _fecha_costo = leerCelda(cell);
                                break;

                            case 50:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (!_fecha_costo.equalsIgnoreCase(""))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que FECHA COSTO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;
                            case 51:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (!_fecha_costo.equalsIgnoreCase(""))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que FECHA COSTO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;
                            case 52:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (!_fecha_costo.equalsIgnoreCase(""))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que FECHA COSTO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;
                            case 53:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (!_fecha_costo.equalsIgnoreCase(""))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que FECHA COSTO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;
                            case 54:
                                _temporal_string = leerCelda(cell);
                                if (_temporal_string.equalsIgnoreCase("") && (!_fecha_costo.equalsIgnoreCase(""))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Campo obligatorio, ya que FECHA COSTO esta diligenciado.",
                                            _temporal_string));
                                }
                                break;
                        }

                        switch (i) {
                            case 5:
                            case 6:
                            case 14:
                            case 15:
                            case 16:
                                _temporal_string = leerCelda(cell);
                                if ((!_temporal_string.equalsIgnoreCase("")) && (!_temporal_string.equalsIgnoreCase("si")) && (!_temporal_string.equalsIgnoreCase("no"))) {
                                    getLogs().add(new MiLog(row.getRowNum() + 1, _iterador_columnas + 1, "Valor incorrecto. Valores posibles ('si','no') ",
                                            _temporal_string));
                                }
                                break;
                        }
                    }

                    i++;
                }
            }
        }
    }

    public boolean validarCelda(Cell cell, CampoCarga colu) {
        if (!super.validarObligatorio(cell)) {
            if (colu.isObligatorio()) {
                return false;
            } else {
                return true;
            }
        }
        if ((colu.getTipoDato().equals("VARCHAR2") || colu.getTipoDato()
                .equals("CHAR"))) {
            return super.validarCadena(cell, colu.getMaxLongitud());
        }

        if (colu.getTipoDato().equals("NUMBER")) {
            return super.validarNumero(cell, colu);
        }
        if (colu.getTipoDato().equals("DATE")) {
            return validarFecha(cell);
        }
        if (colu.getTipoDato().equals("BOOLEAN")) {
            return super.validarBoolean(cell);
        }
        return false;
    }

    public boolean validarFecha(Cell cell) {
        /*if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
         if (DateUtil.isCellDateFormatted(cell)) {
         return false;
         }
         }else{*/
        if (cell.getCellType() == Cell.CELL_TYPE_STRING) {

            try {
                SimpleDateFormat _simple_format_date = new SimpleDateFormat("dd/MM/yyyy");
                Date _temporal_date = null;
                _temporal_date = _simple_format_date.parse(cell.getStringCellValue());
                return true;
            } catch (Exception _exception) {
            }
        }
        //}    
        setMsn("No coinciden el tipo de dato, debe ser una fecha. Formato DD/MM/YYYY");
        return false;
    }

    public boolean validarDinamicas(Sheet sheet) {

        int last = sheet.getLastRowNum();
        int maxCol = getEstructura().size();
        int delrow = 0;
        Row lastrow = sheet.getRow(1);
        do {
            lastrow = sheet.getRow(last);
            if (lastrow == null) {
                delrow = maxCol;
            } else {
                delrow = 0;
                for (int i = 0; i < getEstructura().size(); i++) {
                    if (lastrow.getCell(i, Row.CREATE_NULL_AS_BLANK).getCellType() == Cell.CELL_TYPE_BLANK) {
                        delrow++;
                    }
                }
            }
            if (delrow >= maxCol) {
                if (last == 0) {
                    delrow = maxCol - 1;
                } else {
                    last--;
                }
            }
        } while (delrow >= maxCol);

        int _iterator_row = 0;
        System.out.println("@DEBUG: validarDinamicas : INICIO :: " + last);
        try {
            while (_iterator_row <= last) {

                System.out.println("@DEBUG: validarEncabezado : _iterator_row :: " + _iterator_row);

                Row _row = sheet.getRow(_iterator_row);
                int _columna = 0;
                while (_columna < _iterador_dinamicas) {
                    Cell cell = null;
                    cell = _row.getCell(_columna + _inicio_columnas_dinamicas);

                    switch (_columna) {
                        case 0:
                            String _content = "";
                            switch (_iterator_row) {
                                case 0:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("Código Grupo")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;

                                case 1:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("Nombre Grupo")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;
                                case 2:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("Subsistemas")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;

                                case 3:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("Código Cuenta contable")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;

                                case 4:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("Nombre Cuenta contable")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;

                                default:
                                    _content = cell.getStringCellValue().trim();

                                    if (!_content.equalsIgnoreCase("UGPP")) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1,
                                                "No coincide con el formato del encabezado", leerCelda(cell)));
                                    }
                                    break;
                            }
                            break;

                        default:
                            switch (_iterator_row) {
                                case 0:
                                    if (!validarCelda(cell, new CampoCarga("", "NUMBER", true, 2500))) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1, getMsn(),
                                                leerCelda(cell)));
                                    }
                                    break;

                                case 1:
                                case 2:
                                case 3:
                                case 4:
                                case 5:
                                    if (!validarCelda(cell, new CampoCarga("", "VARCHAR2", true, 2500))) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1, getMsn(),
                                                leerCelda(cell)));
                                    }
                                    break;

                                default:
                                    if (!validarCelda(cell, new CampoCarga("", "NUMBER", false, 1000000000))) {
                                        getLogs().add(new MiLog(_iterator_row + 1, _inicio_columnas_dinamicas + _columna + 1, getMsn(),
                                                leerCelda(cell)));
                                    }
                                    break;
                            }
                            break;
                    }
                    _columna++;
                }

                _iterator_row++;
            }

        } catch (Exception _exception) {
            System.out.println(_iterator_row + " @DEBUG: validarDinamicas : Error :: " + _exception.getMessage());
            getLogs().add(new MiLog(0, 0,
                    "error en validacion", _exception.getMessage()));
        }
        return false;
    }
}
