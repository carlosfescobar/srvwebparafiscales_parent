/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.servicios.util.liquidador;

import java.util.Comparator;

/**
 *
 * @author Port-100
 */
public class ColumnaDb implements Comparable<ColumnaDb>{
    private String columna;
    private String tabla;

    public ColumnaDb() {
    }
    
    public ColumnaDb(String columna, String tabla) {
        this.columna = columna;
        this.tabla = tabla;
    }

    public String getColumna() {
        return columna;
    }

    public void setColumna(String columna) {
        this.columna = columna;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }
    
    @Override
    public int compareTo(ColumnaDb compareColumnaDb) {
 
		String compareQuantity = ((ColumnaDb) compareColumnaDb).getTabla(); 
 
		//ascending order
		return this.tabla.compareTo(compareQuantity);
 
		//descending order
		//return compareQuantity - this.quantity;
 
	}
 
	public static Comparator<ColumnaDb> ColumnaDbNameComparator 
                          = new Comparator<ColumnaDb>() {
 
            @Override
	    public int compare(ColumnaDb atrr1, ColumnaDb atrr2) {
 
	      String tabla = atrr1.getTabla().toUpperCase();
	      String columna = atrr2.getColumna().toUpperCase();
 
	      return tabla.compareTo(columna);
 
	    }
 
	};
}
