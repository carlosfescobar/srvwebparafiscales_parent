package co.gov.ugpp.parafiscales.servicios.util.liquidador;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable;
import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContableDetalle;
import co.gov.ugpp.parafiscales.servicios.util.files.CampoCarga;
import co.gov.ugpp.parafiscales.servicios.util.files.EstructuraCont;
import co.gov.ugpp.parafiscales.servicios.util.files.ValidarArchivoXlsxBase;
import co.gov.ugpp.parafiscales.servicios.util.files.ValidarArchivoXlsxCont;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 *
 * @author franzjr
 */
public class Contabilidad {

    private ValidarArchivoXlsxBase validador;

    

    public void validarArchivo(InputStream input_stream, List<ErrorTipo> errorTipoList, List<CampoCarga> estructura) {
        try {
            ConciliacionContable cc = new ConciliacionContable();
            
            validador = new ValidarArchivoXlsxCont(estructura);
            
            List<ConciliacionContableDetalle> lccd = new ArrayList<ConciliacionContableDetalle>();
            Workbook wb;
            wb = WorkbookFactory.create(input_stream);
            Sheet sheet = wb.getSheetAt(0);

            int last = sheet.getLastRowNum();
            for (int j = 2; j <= last + 1; j++) {
                try {
                    Row row = sheet.getRow(j - 1);
                    lccd.add(crearEntidad(row, sheet, cc));
                } catch (Exception e) {
                    Integer fila = j - 1;
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "El archivo contiene el siguiente error en la linea: " + fila + " error: " + e.getMessage()));
                }
            }
            
            validador.validar(input_stream);
            
        } catch (Exception e) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "El archivo contiene el siguiente error: " + e.getMessage()));
        }
    }

    public List<ConciliacionContableDetalle> registrarArchivo(InputStream input_stream, ConciliacionContable cc, List<ErrorTipo> errorTipoList) {
        try {
            validador = new ValidarArchivoXlsxCont(EstructuraCont.getInstance().getListaCampos());
            List<ConciliacionContableDetalle> lccd = new ArrayList<ConciliacionContableDetalle>();
            Workbook wb;
            wb = WorkbookFactory.create(input_stream);
            Sheet sheet = wb.getSheetAt(0);

            int last = sheet.getLastRowNum();
            for (int j = 2; j <= last + 1; j++) {
                try {
                    Row row = sheet.getRow(j - 1);
                    lccd.add(crearEntidad(row, sheet, cc));
                } catch (Exception e) {
                    Integer fila = j - 1;
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "El archivo contiene el siguiente error en la linea: " + fila + " error: " + e.getMessage()));
                }
            }
            return lccd;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<ConciliacionContableDetalle>();
        }
    }

    public ConciliacionContableDetalle crearEntidad(Row row, Sheet sheet, ConciliacionContable cc) throws Exception {
        Hashtable<String, Object> params = new Hashtable<String, Object>();
        int columna = 1;
        try {
            columna = 0;
            BigInteger anio = BigInteger.valueOf(Long.valueOf(validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK))));

            columna = 1;
            String ncc = validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK));

            columna = 2;
            String nmcc = validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK));

            columna = 3;
            BigDecimal si = BigDecimal.valueOf(Double.valueOf(validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK))));

            columna = 4;
            BigDecimal sf = BigDecimal.valueOf(Double.valueOf(validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK))));

            columna = 5;
            BigDecimal vnm = BigDecimal.valueOf(Double.valueOf(validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK))));
            /*double numericValue = Double.valueOf(validador.transformarCadena(row.getCell(columna, Row.CREATE_NULL_AS_BLANK)));
             Date date = HSSFDateUtil.getJavaDate(numericValue);
             GregorianCalendar gc = new GregorianCalendar();
             gc.setTime(date);*/

            ConciliacionContableDetalle ccd = new ConciliacionContableDetalle();
            ccd.setIdconcilicacioncontable(cc);
            ccd.setAno(anio);
            ccd.setNombrecuentacontable(nmcc);
            ccd.setNumerocuentacontable(ncc);
            ccd.setSaldofinal(sf);
            ccd.setSaldoinicial(si);
            ccd.setValornetomovimiento(vnm);

            return ccd;

        } catch (NumberFormatException e) {
            e.printStackTrace();
            throw e;
        }
    }
}
