package co.gov.ugpp.parafiscales.util;

/**
 * Clase de constantes
 *
 * @author jmunocab
 */
public interface Constants {

    //Cantidad de documentos de al entidad Solicitud
    public static final int CANTIDAD_DOCUMENTOS_SOLICITUD = 2;

    //Cantidad de documentos de al entidad Fiscalizacion
    public static final int CANTIDAD_DOCUMENTOS_FISCALIZACION = 3;

    //Cantidad de documentos de al entidad Denuncia
    public static final int CANTIDAD_DOCUMENTOS_DENUNCIA = 4;

    //Cantidad de documentos de al entidad ControlLiquidador
    public static final int CANTIDAD_DOCUMENTOS_CONTROL_LIQUIDADOR = 5;

    //Cantidad de documentos de al entidad RespuestaAdministradora
    public static final int CANTIDAD_DOCUMENTOS_RESPUESTA_ADMINISTRADORA = 3;

    //Posicionees de los parametros en los querys usados a traves de JDBC
    public static final int PAR_UNO = 1;
    public static final int PAR_DOS = 2;
    public static final int PAR_TRES = 3;
    public static final int PAR_CUATRO = 4;

    //Resultado maximo 1
    public static final int MAX_RESULTS_UNO = 1;

    //Mensajes de validacion de requeridos
    public static final Integer MAX_DESCRIPCION_BD = 4000;
    public static final Integer MAX_DESCRIPCION = 2000;
    public static final Integer MAX_NUM_8 = 8;
    public static final Integer MAX_VAL_20 = 20;
    public static final Integer MAX_VAL_50 = 50;
    public static final Integer MAX_VAL_70 = 70;
    public static final Integer MAX_VAL_30 = 30;
    public static final Integer MAX_VAL_100 = 100;

    /**
     * Parametros de efectividad
     */
    public static final String EFECTIVIDAD_CALCULO_CANTIDAD_SOLICITUDES = "cantidadSolicitudes";
    public static final String EFECTIVIDAD_CALCULO_PROCENTAJE_EFECTIVIDAD = "porcentajeEfectividad";

    public static final int NUM_ACT_ADMIN_NUMERO_LENGHT = 5;
    public static final char NUM_ACT_ADMIN_SEPARATOR = '-';
    public static final char NUM_ACT_ADMIN_FILL = '0';

    /**
     * Cantidad de campos a actualizar la tabla trazabilidad ejecución técnica
     */
    public final int CAMPOS_ACTUALIZAR_EJECUCION_TRAZABILIDAD_TECNICA = 5;

    /**
     * Cantidad de campos a persisitr la tabla trazabilidad ejecución técnica
     */
    public final int CAMPOS_PERSISITR_EJECUCION_TRAZABILIDAD_TECNICA = 8;

    /**
     * Cantidad de campos a persisitr la tabla trazabilidad técnica
     */
    public final int CAMPOS_PERSISITR_TRAZABILIDAD_TECNICA = 17;

    /**
     * Cantidad de campos a persisitr la tabla reintento
     */
    public final int CAMPOS_PERSISITR_REINTENTO = 6;

    /**
     * Cantidad de campos a persisitr de la tabla Hallazgo_Detalle
     */
    public final int CAMPOS_PERSISTIR_HALLAZGO_DETALLE = 11;
    /**
     * Cantidad de campos a persisitr de la tabla Hall_Det_Conci_Contable
     */
    public final int CAMPOS_PERSISTIR_HALL_DET_CONCI_CONTABLE = 11;

    /**
     * Cantidad de campos a actualizar los hallazgos de concilliacion contable
     */
    public final int CAMPOS_ACTUALIZAR_HALLAZGO_CONCILIACION_CONTABLE = 5;

    /**
     * Cantidad de campos a actualizar los hallazgos de nomina
     */
    public final int CAMPOS_ACTUALIZAR_HALLAZGO_NOMINA = 5;

    /**
     * Representan las posiciones en las que se actualizan los campos de los
     * hallazgos de la conciliacion
     */
    public final int ID_HALLAZGO_CONCILIACION = 0;
    public final int OBS_HALLAZGO_CONCILIACION = 1;
    public final int ID_USUARIO_MODIFICACION_CONCILIACION = 2;
    public final int FEC_ACTUALIZACION_CONCILIACION = 3;
    public final int ID_HALLAZGO_DETALLE_CONCILIACION = 4;

    /**
     * Representan las posiciones en las que se actualizan los campos de los
     * hallzagos de la nomina
     */
    public final int ID_HALLAZGO_NOMINA = 0;
    public final int OBS_HALLAZGO_NOMINA = 1;
    public final int ID_USUARIO_MODIFICACION_NOMINA = 2;
    public final int FEC_ACTUALIZACION_NOMINA = 3;
    public final int ID_HALLAZGO_DETALLE_NOMINA = 4;

    /**
     * Representan las posiciones en las que se retornan los objetos en la
     * consulta de obtenerInfoHallazgo
     */
    public final int EXPEDIENTE = 0;
    public final int NOMINA = 1;
    public final int PILA = 2;
    public final int CONCILIACION_CONTABLE = 3;

    /**
     * Representan las posiciones en las que se retornan los objetos en la
     * consulta de nomina.cruzarPilaconNomina
     */
    public final int TIPO_DOCUMENTO_CRUCE = 0;
    public final int NUMERO_IDENTIFICACION_CRUCE = 1;
    public final int NOMBRE_CRUCE = 2;
    public final int PERIODO_RESTO_CRUCE = 3;
    public final int TIPO_COTIZANTE_CRUCE = 4;

    /**
     * Representan las posiciones en las que se mapean los objetos para
     * persistir la tabla reintento
     */
    public final int FEC_CREACION_REINTENTO = 0;
    public final int FEC_MODIFICACION_REINTENTO = 1;
    public final int ID_TRANSACCION = 2;
    public final int ID_USUARIO_CREACION_REINTENTO = 3;
    public final int ID_USUARIO_MODIFICACION_REINTENTO = 4;
    public final int COD_PROCESO = 5;

    /**
     * Representan las posiciones en las que se mapean los objetos para
     * persistir la tabla hallazgoDetalleNominaPila
     */
    public final int HALLAZGO = 0;
    public final int DESC_HALLAZGO = 1;
    public final int TIPO_IDENTIFICACION = 2;
    public final int NUMERO_IDENTIFICACION = 3;
    public final int NOMBRE_COTIZANTE = 4;
    public final int PERIODO_RESTO = 5;
    public final int TIPO_COTIZANTE = 6;
    public final int USUARIO_CREACION = 7;
    public final int USUARIO_MODIFICACION = 8;
    public final int FECHA_CREACION = 9;
    public final int FECHA_MODIFICACION = 10;

    /**
     * Representan las posiciones en las que se mapean los objetos para
     * persistir la tabla Hall_Det_Conci_Contable
     */
    public final int ID_HALLAZGO = 0;
    public final int DESC_HALLAZGO_CON_CONTABLE = 1;
    public final int CONCEP_CONTABLE = 2;
    public final int VAL_CONTABILIDAD = 3;
    public final int VAL_NOMINA = 4;
    public final int VAL_DIFERENCIA_PROCENTAJE = 5;
    public final int VAL_DIFERENCIA_PESOS = 6;
    public final int USUARIO_CREACION_CON_CONTABLE = 7;
    public final int USUARIO_MODIFICACION_CON_CONTABLE = 8;
    public final int FECHA_CREACION_CON_CONTABLE = 9;
    public final int FECHA_MODIFICACION_CON_CONTABLE = 10;

    /**
     * Representan las posiciones en las que se crean los campos de la tabla
     * trazabilidad técnica
     */
    public final int ID_TRAZABILIDAD_INSTANCIA_PROC = 0;
    public final int COD_ESTADO_PROCESO = 1;
    public final int DESC_ESTADO_PROCESO = 2;
    public final int COD_ESTADO_ACTIVIDAD = 3;
    public final int DESC_ESTADO_ACTIVIDAD = 4;
    public final int ID_INSTANCIA_ACTIVIDAD = 5;
    public final int VAL_NOMBRE_ACTIVIDAD = 6;
    public final int ID_DEFINICION_ACTIVIDAD = 7;
    public final int ID_USUARIO = 8;
    public final int COD_ROL = 9;
    public final int VAL_NOMBRE_ROL = 10;
    public final int FEC_ESTADO_ACTIVIDAD = 11;
    public final int FEC_CREACION = 12;
    public final int FEC_ACTUALIZACION = 13;
    public final int ID_USUARIO_CREACION = 14;
    public final int ID_USUARIO_MODIFICACION = 15;
    public final int VAL_DIAS_FIN_TAREA = 16;

    /**
     * Representan las posiciones en las que se crean los campos de la tabla
     * trazabilidad ejecucion técnica
     */
    public final int COD_ESTADO_EJECUCION = 0;
    public final int FEC_INICIO_EJECUCION = 1;
    public final int FEC_FIN_EJECUCION = 2;
    public final int CANTIDAD_INTENTOS = 3;
    public final int ID_USUARIO_CREACION_TRAZABILIDAD = 4;
    public final int ID_USUARIO_MODIFICACION_TRAZABILIDAD = 5;
    public final int FEC_CREACION_TRAZABILIDAD = 6;
    public final int FEC_ACTUALIZACION_TRAZABILIDAD = 7;

    /**
     * Representan las posiciones en las que se crean los campos de la tabla
     * trazabilidad ejecucion técnica
     */
    public final int COD_ESTADO_EJECUCION_UPDATE = 0;
    public final int FEC_FIN_EJECUCION_UPDATE = 1;
    public final int ID_USUARIO_MODIFICACION_TRAZABILIDAD_UPDATE = 2;
    public final int FEC_MODIFICACION_TRAZABILIDAD_UPDATE = 3;
    public final int FEC_FIN_EJECUCION_PARAMETER = 4;

    /**
     * Representan la descripcion de los diferentes tipos hallazgos
     */
    public final String DESC_HALLAZGO_PILA_NOMINA = "msgCotizanteSinNomina";
    public final String DESC_HALLAZGO_NOMINA_VACACIONES = "msgCotizanteSinVacaciones";
    public final String DESC_HALLAZGO_CONCILIACION_CONTABLE = "msgDiferenciaConciliacionContable";

    /**
     * Representan los querys nativos utilizandos en el servicio de liquidador
     */
    public final String INSERT_HALLAZGO_DETALLE_CONCILIACION_CONTABLE = "insertarHallazgoDetalleConContable";
    public final String INSERT_HALLAZGO_DETALLE_NOMINA_PILA = "insertarHallazgoDetallenNomina";
    public final String ACTUALIZAR_CONCILIACION_CONTABLE = "actualizarValorNomina";
    public final String INSERT_REINTENTO = "insertarReintento";
    public final String ACTUALIZAR_HALLAZGO_NOMINA = "actualizarHallazgoNominaPila";
    public final String ACTUALIZAR_HALLAZGO_CONCILIACION_CONTABLE = "actualizarHallazgoConciliacionContable";

    public final String CRUZAR_PILA_NOMINA = "nomina.cruzarPilaconNomina";
    public final String CRUZAR_NOMINA_VACACIONES = "nomina.cruzarNominaVacaciones";
    public final String OBTENER_INFO_HALLAZGO = "hallazgo.obtenerInfoHallazgo";
    public final String SUMAR_NOMINA = "conciliacionContable.sumarNomina";
    public final String CRUZAR_CONCILIACION_CONTABLE = "conciliacionContable.cruzarConciliacionContable";

    /**
     * Representan los nombres de los scriptsen batch que se usa en el servicio
     * de trazabilidad técnica
     */
    public final String INSERT_TRAZABILIDAD_TECNICA = "insertarTrazabilidadTecnica";
    public final String INSERT_TRAZABILIDAD_EJECUCION_TECNICA = "insertarEjecucionTrazabilidad";
    public final String ACTUALIZAR_TRAZABILIDAD_EJECUCION_TECNICA = "actualizarEjecucionTrazabilidad";

    /**
     * Query utilizado para actualizar el codOrigen asociados a varios
     * documentos
     */
    public final String ACTUALIZAR_COD_ORIGEN_MASIVO = "expedienteDocumentoECM.actualizaOrigen";

    /**
     * Representan el nombre del idLlave que se espera para las fecha de inicio
     * y fin desde el listado de parmetros
     */
    public final String PERIODO_INICIAL = "fechaInicio";
    public final String PERIODO_FINAL = "fechaFin";
    public final String ANO = "ano";

    /**
     * Representan los tipos de medicion para el cruce de conciliacin contable
     */
    public static final char PORCENTAJE = '%';
    public static final char PESOS = '$';

    /**
     * Valor Dominio para indicar cuando no se tiene informacion completa de los
     * formatos de respuesta informacion
     */
    public final String INFORMACION_NO_DISPONIBLE = "999001";

    /**
     * Representan las posiciones en las que se reciben los parametros de la
     * consulta del cruce de conciliacion contable
     */
    public final int CONCEPTO_CONTABLE = 0;
    public final int VALOR_CONTABILIDAD = 1;
    public final int VALOR_NOMINA = 2;

    /**
     * Representa el nombre del concepto contable por el cual se va a filtrar el
     * cruce con vacaciones
     */
    public final String CONCEPTO_CONTABLE_VACACIONES = "VACACIONES PAGADAS POR LIQUIDACIÓN DE CONTRATO";

    /**
     * Representa el numero de la hoja que se toma como referencia para leer los
     * archivos de excel en las operaciones del servicio de respuesta
     * informacion
     */
    public final int NUMERO_HOJA_EXCEL_EVALUAR = 0;

    /**
     * Cantidad de columnas a evaluar en el formato de BANCOS
     */
    public final int FORMATO_BANCOS_COLUMNA_INICIO = 1;
    public final int FORMATO_BANCOS_COLUMNA_FIN = 11;

    /**
     * Cantidad de columnas a evaluar en el formato de TELEFONIA
     */
    public final int FORMATO_TELEFONIA_COLUMNA_INICIO = 1;
    public final int FORMATO_TELEFONIA_COLUMNA_FIN = 14;

    /**
     * Cantidad de columnas a evaluar en el formato de PILA
     */
    public final int FORMATO_PILA_COLUMNA_INICIO = 1;
    public final int FORMATO_PILA_COLUMNA_FIN = 7;

    /**
     * Cantidad de columnas a evaluar en el formato de SAYP
     */
    public final int FORMATO_SAYP_COLUMNA_INICIO = 1;
    public final int FORMATO_SAYP_COLUMNA_FIN = 8;

    /**
     * Representan las posiciones en las que se obtienen los datos del formato
     * de BANCOS para el servicio de respuesta informacion
     */
    public final int FORMATO_BANCOS_CELDA_NOMBRE_BANCO = 1;
    public final int FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO = 2;
    public final int FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO = 3;
    public final int FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS = 4;
    public final int FORMATO_BANCOS_CELDA_TIPO_CUENTA = 5;
    public final int FORMATO_BANCOS_CELDA_NUMERO_CUENTA = 6;
    public final int FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA = 7;
    public final int FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO = 8;
    public final int FORMATO_BANCOS_CELDA_TELEFONO = 9;
    public final int FORMATO_BANCOS_CELDA_CELULAR = 10;
    public final int FORMATO_BANCOS_CELDA_OBSERVACIONES = 11;

    /**
     * Representan las posiciones en las que se obtienen los datos del formato
     * de TELEFONIA para el servicio de respuesta informacion
     */
    public final int FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO = 1;
    public final int FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO = 2;
    public final int FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL = 3;
    public final int FORMATO_TELEFONIA_CELDA_DIRECCION = 4;
    public final int FORMATO_TELEFONIA_CELDA_DEPARTAMENTO = 5;
    public final int FORMATO_TELEFONIA_CELDA_CIUDAD = 6;
    public final int FORMATO_TELEFONIA_CELDA_DIRECCION_DOS = 7;
    public final int FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS = 8;
    public final int FORMATO_TELEFONIA_CELDA_CIUDAD_DOS = 9;
    public final int FORMATO_TELEFONIA_CELDA_TELEFONO = 10;
    public final int FORMATO_TELEFONIA_CELDA_TELEFONO_DOS = 11;
    public final int FORMATO_TELEFONIA_CELDA_CELULAR = 12;
    public final int FORMATO_TELEFONIA_CELDA_EMAIL = 13;
    public final int FORMATO_TELEFONIA_CELDA_FAX = 14;

    /**
     * Representan las posiciones en las que se obtienen los datos del formato
     * de PILA para el servicio de respuesta informaci�n
     */
    public final int FORMATO_PILA_CELDA_TIPO_DOCUMENTO = 1;
    public final int FORMATO_PILA_CELDA_NUMERO_DOCUMENTO = 2;
    public final int FORMATO_PILA_CELDA_NUMERO_PLANILLA = 3;
    public final int FORMATO_PILA_CELDA_PERIODO_SALUD = 4;
    public final int FORMATO_PILA_CELDA_FECHA_CARGUE_PILA = 5;
    public final int FORMATO_PILA_CELDA_NOMBRE_LOTE = 6;
    public final int FORMATO_PILA_CELDA_OBSERVACIONES = 7;

    /**
     * Representan las posiciones en las que se obtienen los datos del formato
     * de SAYP para el servicio de respuesta informacion
     */
    public final int FORMATO_SAYP_CELDA_TIPO_DOCUMENTO = 1;
    public final int FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO = 2;
    public final int FORMATO_SAYP_CELDA_RAZON_SOCIAL = 3;
    public final int FORMATO_SAYP_CELDA_FECHA_PAGO = 4;
    public final int FORMATO_SAYP_CELDA_VALOR_PAGADO = 5;
    public final int FORMATO_SAYP_CELDA_CONFIRMACION = 6;
    public final int FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR = 7;
    public final int FORMATO_SAYP_CELDA_OBSERVACIONES = 8;

    /**
     * Horario de los periodos de fiscalizacion
     */
    public static int HORA_INICIO_PERIODO = 00;
    public static int MINUTO_INICIO_PERIODO = 00;
    public static int SEGUNDO_INICIO_PERIODO = 00;
    public static int HORA_FIN_PERIODO = 23;
    public static int MINUTO_FIN_PERIODO = 59;
    public static int SEGUNDO_FIN_PERIODO = 59;

    /**
     * cantidad deControlUbicacionEnvio en estado enviado.
     */
    public static int ENVIADO = 1;

    /**
     * Prefijo de la llave primaria de formato
     */
    public static String FORMATO_PK = "formatoPK.";

    /**
     * Nombres de los atributos que componen la PK de formato
     */
    public static String FORMATO_PK_ID_FORMATO = "idFormato";
    public static String FORMATO_PK_ID_VERSION = "idVersion";

    /**
     * Constantes que se usan en el servicio que valida la estructura de un
     * archivo
     */
    public static int INDEX_SHEET_USER = 0;
    public static int INDEX_SHEET_REQUIRED = 3;
    public static int INDEX_SHEET_STRUCTURE = 4;
    public static int INDEX_SHEET_MIGRATION_STRUCTURE = 5;
    //Columna de encabezado
    public static int INDEX_ROW_COLUMN_HEADER_SHEET_REQUIRED = 0;
    //Primera columna desde donde se obtienen los datos ingresados por el usuario
    public static int FIRST_INDEX_COLUMN_USER_SHEET = 1;
    //La primera fila desde donde se obtiene los datos ingresados por el usuario
    public static int FIRST_INDEX_ROW_USER_SHEET = 0;
    //La primera fila de donde se obtienen los datos de la hoja de requeridos
    public static int FIRST_INDEX_ROW_SHEET_REQUIRED = 1;
    //La primera columna de donde se obtienen los datos de la hoja de requeridos
    public static int FIRST_INDEX_COLUMN_SHEET_REQUIRED = 0;
    ////La última columna de donde se obtienen los datos de la hoja de requeridos
    public static int LAST_INDEX_COLUMN_SHEET_REQUIRED = 3;
    //La primea columna de donde se obtienen los datos de las hojas de configuración de los docTypes
    public static int FIRST_INDEX_COLUMN_DOC_TYPE_SHEET = 0;
    //La ultima columna de donde se obtienen los datos de las hojas de configuración de los docTypes
    public static int LAST_INDEX_COLUMN_DOC_TYPE_SHEET = 1;
    //La prima fila de donde se obtiene la informaciòn de la hoja de configuración de los docTypes
    public static int FIRST_INDEX_ROW_DOC_TYPE_SHEET = 2;
    //La prima fila de donde se obtiene la informaciòn de la hoja de configuración de los docTypes de migración
    public static int FIRST_INDEX_ROW_DOC_TYPE_MIGRACION_SHEET = 1;
    //La fila en donde se encuentra el nombre del docType en la hoja de estructura de docType
    public static int ROW_INDEX_DOC_TYPE_NAME = 0;
    //Indica el parametro que indica que el valor de salida el el id de la fila
    public static int ROW_VALUE = 1;
    //Indica el valor del primer intento
    public static int FIRST_TRY = 1;
    //Cantidad de radicados de la trazabilidad acto administrativo
    public static int CANTIDAD_RADICADOS_TRAZABILIDAD_ACTO = 0;
    /**
     * Parametros fijos de consultas de los KPIS
     */
    public static int FECHA_INICIO = 1;
    public static int FECHA_FIN = 2;
    
    //Representa la primera posicion de una lista
    public static int FIRST_POSITION = 0;
    
    /**
     * Nombre de la Licencia de la libreria de ASPOSE
     */
    public static String LICENCE_ASPOSE = "Aspose.Total.Java.lic";
    
    /**
     * Nombre del prefijo de los grupo del LDAP
     */
    public static String PREFIJO_LDAP_GROUP = "CN=";
    
    /**
     * valores de criterios de ordenamiento
     */
    public static Long CRITERIO_ORDENAMIENTO_DESCENDENTE = 0L;
    public static Long CRITERIO_ORDENAMIENTO_ASCENDENTE = 1L;
}
