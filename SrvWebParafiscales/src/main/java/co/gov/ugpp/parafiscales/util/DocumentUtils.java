/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.hwpf.usermodel.Range;
import org.apache.poi.hwpf.usermodel.Table;
import org.apache.poi.hwpf.usermodel.TableCell;
import org.apache.poi.hwpf.usermodel.TableIterator;
import org.apache.poi.hwpf.usermodel.TableRow;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFFooter;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

/**
 *
 * @author jsaenzar
 */
public class DocumentUtils {

    /**
     * Actualizacion de parágrafos para .doc
     *
     * @param documentoDoc
     * @param mapDatosPlantilla
     */
    public static void updateTokensParagraphs(HWPFDocument documentoDoc, Map<String, List<String>> mapDatosPlantilla) {

        Range textoDocumento = documentoDoc.getRange();

        for (Map.Entry<String, List<String>> entry : mapDatosPlantilla.entrySet()) {
            if (textoDocumento.text().contains(entry.getKey())) {
                for (String valorReempazar : entry.getValue()) {
                    textoDocumento.replaceText(entry.getKey(), valorReempazar);
                }
            }
        }
    }

    public static void updateTokensTables(HWPFDocument documentoDoc, Map<String, List<String>> mapDatosPlantilla) {

        Range textoDocumento = documentoDoc.getRange();
        TableIterator itr = new TableIterator(textoDocumento);

        while (itr.hasNext()) {
            Table table = itr.next();
            for (int rowIndex = 0; rowIndex < table.numRows(); rowIndex++) {
                TableRow row = table.getRow(rowIndex);
                for (int colIndex = 0; colIndex < row.numCells(); colIndex++) {
                    TableCell cell = row.getCell(colIndex);
                    for (Map.Entry<String, List<String>> entry : mapDatosPlantilla.entrySet()) {
                        if (cell.text().contains(entry.getKey())) {
                            for (String valorReempazar : entry.getValue()) {
                                cell.replaceText(entry.getKey(), valorReempazar);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void updateTokensParagraphs(XWPFDocument doc, Map<String, List<String>> mapDatosPlantilla) {

        List<XWPFParagraph> paragraphs = doc.getParagraphs();
        for (XWPFParagraph paragraph : paragraphs) {
            String textoOriginal = paragraph.getParagraphText();

            if (StringUtils.isNotBlank(textoOriginal)) {
                String textoFinal = null;
                for (Map.Entry<String, List<String>> entry : mapDatosPlantilla.entrySet()) {
                    if (textoOriginal.contains(entry.getKey())) {
                        textoOriginal = textoOriginal.replace(entry.getKey(), StringUtils.join(entry.getValue(), ", "));
                        textoFinal = textoOriginal;
                    }
                }

                if (textoFinal != null) {
                    for (int k = 0; k < paragraph.getRuns().size(); k++) {
                        paragraph.getRuns().get(k).setText("", 0);
                    }
                    paragraph.getRuns().get(0).setText(textoFinal, 0);
                }
            }

        }

    }

    public static void updateTokensTables(XWPFDocument doc, Map<String, List<String>> mapDatosPlantilla) {

        for (XWPFTable table : doc.getTables()) {
            for (int r = 0; r < table.getRows().size(); r++) {
                XWPFTableRow row = table.getRow(r);
                for (int c = 0; c < row.getTableCells().size(); c++) {
                    XWPFTableCell cell = row.getCell(c);
                    for (int p = 0; p < cell.getParagraphs().size(); p++) {
                        XWPFParagraph para = cell.getParagraphs().get(p);
                        String texto = para.getParagraphText();
                        if (mapDatosPlantilla.containsKey(texto)) {
                            final String token = mapDatosPlantilla.get(texto).get(0);
                            cell.removeParagraph(0);
                            cell.setText(token);
                            if (mapDatosPlantilla.get(texto).size() > 1) {
                                for (int i = 1; i < mapDatosPlantilla.get(texto).size(); i++) {
                                    if (table.getRow(i + 1) == null) {
                                        XWPFTableRow row2 = table.createRow();
                                        XWPFTableCell cell2 = row2.getTableCells().get(c);
                                        cell2.setText(mapDatosPlantilla.get(texto).get(i));
                                    } else {
                                        XWPFTableRow row2 = table.getRow(i + 1);
                                        XWPFTableCell cell2 = row2.getTableCells().get(c);
                                        cell2.setText(mapDatosPlantilla.get(texto).get(i));

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static void updateTokensHeaders(XWPFDocument doc, Map<String, List<String>> mapDatosPlantilla) {

        List<XWPFHeader> hdrs = doc.getHeaderList();
        for (XWPFHeader hdr : hdrs) {
            for (XWPFParagraph paragraph : hdr.getParagraphs()) {
                String texto = paragraph.getParagraphText();
                String token = null;

                if (mapDatosPlantilla.containsKey(texto)) {
                    token = StringUtils.join(mapDatosPlantilla.get(texto), ", ");
                }

                if (token != null) {
                    for (int k = 0; k < paragraph.getRuns().size(); k++) {
                        paragraph.getRuns().get(k).setText("", 0);
                    }
                    paragraph.getRuns().get(0).setText(token, 0);
                }
            }
        }
    }

    public static void updateTokensFooters(XWPFDocument doc, Map<String, List<String>> mapDatosPlantilla) {

        List<XWPFFooter> footers = doc.getFooterList();
        for (XWPFFooter footer : footers) {
            for (XWPFParagraph paragraph : footer.getParagraphs()) {
                String texto = paragraph.getParagraphText();
                String token = null;

                if (mapDatosPlantilla.containsKey(texto)) {
                    token = StringUtils.join(mapDatosPlantilla.get(texto), ", ");
                }

                if (token != null) {
                    for (int k = 0; k < paragraph.getRuns().size(); k++) {
                        paragraph.getRuns().get(k).setText("", 0);
                    }
                    paragraph.getRuns().get(0).setText(token, 0);
                }
            }
        }
    }

    /**
     * Convierte un file a byte
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static byte[] getBytesFromFile(File file) throws IOException {

        java.io.InputStream is = new FileInputStream(file);

        // Get the size of the file
        long length = file.length();

        /*
         * You cannot create an array using a long type. It needs to be an int
         * type. Before converting to an int type, check to ensure that file is
         * not loarger than Integer.MAX_VALUE;
         */
        if (length > Integer.MAX_VALUE) {
            return null;
        }

        // Create the byte array to hold the data
        byte[] bytes = new byte[(int) length];

        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while ((offset < bytes.length)
                && ((numRead = is.read(bytes, offset, bytes.length - offset)) >= 0)) {

            offset += numRead;

        }

        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }

        is.close();
        return bytes;

    }
}
