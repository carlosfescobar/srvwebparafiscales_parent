package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_CELULAR;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NOMBRE_BANCO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_CUENTA;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_OBSERVACIONES;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TELEFONO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_CUENTA;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_FECHA_CARGUE_PILA;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_NOMBRE_LOTE;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_NUMERO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_NUMERO_PLANILLA;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_OBSERVACIONES;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_PERIODO_SALUD;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_PILA_CELDA_TIPO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_CONFIRMACION;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FECHA_PAGO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_OBSERVACIONES;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_RAZON_SOCIAL;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_TIPO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_SAYP_CELDA_VALOR_PAGADO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CELULAR;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_CIUDAD_DOS;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_DIRECCION_DOS;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_EMAIL;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_FAX;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TELEFONO_DOS;
import static co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum.FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author zrodrigu
 */
public class ExcelUtil {

    private static List<EncabezadosRespuestaInformacionEnum> encabezados;

    /**
     * Método que obtiene un dato específico de una hoja de trabajo
     * @param row la fila
     * @param indexColum el numero de la columna
     * @return el valor encontrado
     */
    public static String obtenerDatoHoja(final Row row, final int indexColum) {
        String returnValue = null;
        Cell cell = row.getCell(indexColum);
        if (cell != null) {
            if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
                returnValue = cell.getStringCellValue();
            } else if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
                returnValue = String.valueOf(cell.getNumericCellValue());
            }
        }
        return returnValue;
    }

    /**
     * Método que determina la columna final
     *
     * @param row la fila
     * @param beginIndex el index de la columna inicial
     * @return el indice de la columna final
     */
    public static int determinarIndexColumnaFinal(final Row row, final int beginIndex) {
        int columnaFinal = beginIndex;
        while (row.getCell(columnaFinal) != null && !(row.getCell(columnaFinal).getCellType() == Cell.CELL_TYPE_BLANK)) {
            columnaFinal++;
        }
        return columnaFinal;
    }

    /**
     * Método que obtiene los encabezados de las hojas
     *
     * @param row la primera fila de la hoja
     * @param beginIndex la columna inicio
     * @param endIndex la columna fin
     * @return los encabezados de la hoja de trabajo
     */
    public static Map<Integer, String> obtenerNombreEncabezados(final Row row, final int beginIndex, final int endIndex) {
        Map<Integer, String> encabezaddos = null;
        if (row != null) {
            encabezaddos = new HashMap<Integer, String>();
            for (int iterador = beginIndex; iterador < endIndex; iterador++) {
                Cell celda = row.getCell(iterador);
                encabezaddos.put(iterador, celda.getStringCellValue());
            }
        }
        return encabezaddos;
    }

    public static boolean evaluarFila(Row row, int desde, int hasta) {
        for (int i = desde; i <= hasta; i++) {
            if (row.getCell(i) != null && !(row.getCell(i).getCellType() == Cell.CELL_TYPE_BLANK)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Método que evalúa la obligatoriedad de celdas para una fila de una hoja
     *
     * @param fila la fila a evaluar
     * @param encabezados los encabezados que contienen la información sobre su
     * obligatoriedad
     * @param errorTipoList el listado de errores a llenar de acuerdo a la
     * validación
     */
    public static void evaluarRequeridos(final Row fila, final List<EncabezadosRespuestaInformacionEnum> encabezados,
            final List<ErrorTipo> errorTipoList) {

        for (EncabezadosRespuestaInformacionEnum encabezado : encabezados) {

            if (encabezado.isRequerido()) {
                if (fila.getCell(encabezado.getIndex()) == null
                        || fila.getCell(encabezado.getIndex()).getCellType() == Cell.CELL_TYPE_BLANK) {

                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_VALIDACION_ARCHIVO,
                            "El valor en la Columna: " + encabezado.getNombreColumna() + " en la fila: "
                            + (fila.getRowNum() + 1) + ", es requerido. Debe enviar un valor de tipo: " + encabezado.getTipoDato().getDescripcion()));
                }
            }
        }
    }

    public static List<EncabezadosRespuestaInformacionEnum> getEncabezadosBancos() {

        encabezados = Arrays.asList(FORMATO_BANCOS_CELDA_NOMBRE_BANCO, FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO,
                FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO, FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS,
                FORMATO_BANCOS_CELDA_TIPO_CUENTA, FORMATO_BANCOS_CELDA_NUMERO_CUENTA,
                FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA, FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO,
                FORMATO_BANCOS_CELDA_TELEFONO, FORMATO_BANCOS_CELDA_CELULAR, FORMATO_BANCOS_CELDA_OBSERVACIONES);

        return encabezados;
    }

    public static List<EncabezadosRespuestaInformacionEnum> getEncabezadosTelefonia() {

        encabezados = Arrays.asList(FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO, FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO,
                FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL, FORMATO_TELEFONIA_CELDA_DIRECCION, FORMATO_TELEFONIA_CELDA_DEPARTAMENTO,
                FORMATO_TELEFONIA_CELDA_CIUDAD, FORMATO_TELEFONIA_CELDA_DIRECCION_DOS, FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS,
                FORMATO_TELEFONIA_CELDA_CIUDAD_DOS, FORMATO_TELEFONIA_CELDA_TELEFONO, FORMATO_TELEFONIA_CELDA_TELEFONO_DOS,
                FORMATO_TELEFONIA_CELDA_CELULAR, FORMATO_TELEFONIA_CELDA_EMAIL, FORMATO_TELEFONIA_CELDA_FAX);

        return encabezados;
    }

    public static List<EncabezadosRespuestaInformacionEnum> getEncabezadosPILA() {

        encabezados = Arrays.asList(FORMATO_PILA_CELDA_TIPO_DOCUMENTO, FORMATO_PILA_CELDA_NUMERO_DOCUMENTO,
                FORMATO_PILA_CELDA_NUMERO_PLANILLA, FORMATO_PILA_CELDA_PERIODO_SALUD, FORMATO_PILA_CELDA_FECHA_CARGUE_PILA,
                FORMATO_PILA_CELDA_NOMBRE_LOTE, FORMATO_PILA_CELDA_OBSERVACIONES);

        return encabezados;
    }

    public static List<EncabezadosRespuestaInformacionEnum> getEncabezadosSAYP() {

        encabezados = Arrays.asList(FORMATO_SAYP_CELDA_TIPO_DOCUMENTO, FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO,
                FORMATO_SAYP_CELDA_RAZON_SOCIAL, FORMATO_SAYP_CELDA_FECHA_PAGO, FORMATO_SAYP_CELDA_VALOR_PAGADO,
                FORMATO_SAYP_CELDA_CONFIRMACION, FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR, FORMATO_SAYP_CELDA_OBSERVACIONES);

        return encabezados;
    }
}
