package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.EncabezadosRespuestaInformacionEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

/**
 *
 * @author vgarcigu
 */
public class FileStructureUtili {

    public static ErrorTipo crearErrorArchivo(final int numFila, final EncabezadosRespuestaInformacionEnum encabezado) {

        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El dato de la columna " + encabezado.getNombreColumna() + ", en la fila " + numFila
                + " no coincide el tipo de dato esperado. Se espera valor de tipo: " + encabezado.getTipoDato().getDescripcion());
        return errorTipo;
    }

    public static ErrorTipo crearErrorArchivo(int numFila, int numColumna) {

        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El valor de la columna " + numColumna + " y fila " + numFila + " no coincide con el tipo de dato esperado");
        return errorTipo;
    }

    public static ErrorTipo crearErrorArchivo(int numColumna, String nomColumna) {

        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El encabezado del archivo no cumple la estructura, la columna " + numColumna + " espera el título: " + nomColumna);
        return errorTipo;
    }

    public static ErrorTipo crearErrorArchivo(String nomColumna) {

        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("La columna " + nomColumna + ", es requerida");
        return errorTipo;
    }
    
    public static ErrorTipo crearErrorArchivo(int columnIndex) {

        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El valor de la columna " + columnIndex + ", es requerido");
        return errorTipo;
    }
    
    public static ErrorTipo crearErrorArchivoRequerido(int rowIndex, final String nomColumna) {
        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El valor de la Columna " +nomColumna+", en la Fila: "+ ++rowIndex + ", es requerido");
        return errorTipo;
    }
    
    public static ErrorTipo crearErrorArchivoNoDataType(int rowIndex, final String nomColumna) {
        ErrorTipo errorTipo = new ErrorTipo();
        errorTipo.setCodError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getCode());
        errorTipo.setValDescError(ErrorEnum.ERROR_VALIDACION_ARCHIVO.getMessage());
        errorTipo.setValDescErrorTecnico("El valor de la Columna " +nomColumna+", en la Fila: "+ ++rowIndex + ", no coincide con el tipo de dato esperado");
        return errorTipo;
    }

    public static byte[] getBytes(InputStream is) throws IOException {
        int len;
        int size = 1024;
        byte[] buf;
        if (is instanceof ByteArrayInputStream) {
            size = is.available();
            buf = new byte[size];
            len = is.read(buf, 0, size);
        } else {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            buf = new byte[size];
            while ((len = is.read(buf, 0, size)) != -1) {
                bos.write(buf, 0, len);
            }
            buf = bos.toByteArray();
        }
        return buf;
    }

    public static Sheet agregarFilas(Sheet origen, Sheet destino) {
        Iterator rowsOld = origen.rowIterator();
        int i = 0;
        if (destino.getLastRowNum() > 0) {
            i = destino.getLastRowNum() + 1;
            rowsOld.next();
        }
        while (rowsOld.hasNext()) {
            Row rowOld = (Row) rowsOld.next();
            Row rowNew = destino.createRow(i);
            rowNew.setHeight(rowOld.getHeight());
            if (rowOld != null) {
                Iterator cellsOld = rowOld.cellIterator();
                int j = 0;
                while (cellsOld.hasNext()) {
                    Cell cellOld = (Cell) cellsOld.next();
                    Cell cellNew = rowNew.createCell(j);
                    copyCell(cellOld, cellNew);
                    cellNew.setCellType(cellOld.getCellType());
                    j++;
                }
            }
            i++;
        }
        return destino;
    }

    private static void copyCell(Cell cellOld, Cell cellNew) {
        switch (cellOld.getCellType()) {
            case Cell.CELL_TYPE_STRING:
                cellNew.setCellValue(cellOld.getStringCellValue());
                break;
            case Cell.CELL_TYPE_NUMERIC:
                cellNew.setCellValue(cellOld.getNumericCellValue());
                break;
            case Cell.CELL_TYPE_BLANK:
                cellNew.setCellValue(Cell.CELL_TYPE_BLANK);
                break;
            case Cell.CELL_TYPE_BOOLEAN:
                cellNew.setCellValue(cellOld.getBooleanCellValue());
                break;
        }
    }
}
