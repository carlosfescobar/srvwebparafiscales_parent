/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import java.util.Comparator;

/**
 *
 * @author vgarcigu
 */
public class IdentificationComparator implements Comparator<Identificacion>{


    
    @Override
    public int compare(Identificacion o1, Identificacion o2) {
        if(o1.getCodTipoIdentificacion().equals(o2.getCodTipoIdentificacion()) &&
           o1.getValNumeroIdentificacion().equals(o2.getValNumeroIdentificacion())){
            return 0;
        }else{
            return 1;
        }
    }
    
  

    
}
