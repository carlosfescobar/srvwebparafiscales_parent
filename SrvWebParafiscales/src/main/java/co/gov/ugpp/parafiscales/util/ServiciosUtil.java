package co.gov.ugpp.parafiscales.util;


import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Auditoria;

/**
 * Esta clase contiene operaciones utilitarias y comunes a los servicios web.
 *
 * @author SOFTMANAGEMENT S.A.
 *
 */
public class ServiciosUtil {

    /**
     * Registra la informacion del contexto transaccional de una operacion de
     * servicio web en auditoria.
     *
     * @param contextoSolicitud Representa la informaci�n de registrar
     * encapsulada en un objeto de tipo ContextoTransaccionalTipo
     * @return Retorna la entidad auditoria preparada
     */
    public static Auditoria preparaContextoTransaccionalEnAuditoria(
            ContextoTransaccionalTipo contextoSolicitud) {

        Auditoria auditoriaTipo = new Auditoria(contextoSolicitud.getIdTx(),
                contextoSolicitud.getIdInstanciaProceso(), contextoSolicitud.getIdDefinicionProceso(),
                contextoSolicitud.getValNombreDefinicionProceso(), contextoSolicitud.getIdInstanciaActividad(),
                contextoSolicitud.getValNombreDefinicionActividad(), contextoSolicitud.getIdUsuarioAplicacion(),
                contextoSolicitud.getIdUsuario(), contextoSolicitud.getIdEmisor());

        return auditoriaTipo;

    }
}
