package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.SolicitudEntidadEstadoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ClasificacionPersona;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Clase utilitaria del CORE Parafiscales
 *
 * @author jmunocab
 */
public class Util {

    /**
     * Método que identifica cual es la última clasificación para una persona
     * dependiendo el atributo discriminador
     *
     * @param persona la persona a validar
     * @return valorDominio que contiene la información de la ultima
     * clasificación de la persona
     */
    public static ValorDominio obtenerUltimaClasificacionPersona(final Persona persona) {

        ValorDominio tipoPersonaActual = null;

        if (persona != null
                && persona.getClasificacionPersona() != null
                && (persona.getClasificacionesPersona() != null
                && !persona.getClasificacionesPersona().isEmpty())) {

            for (ClasificacionPersona clasificacionPersona : persona.getClasificacionesPersona()) {
                if (persona.getClasificacionPersona().getCode().equals(clasificacionPersona.getCodClasificacionPersona().getId())) {
                    if (tipoPersonaActual == null) {
                        tipoPersonaActual = clasificacionPersona.getCodTipoPersona();

                    } else if (tipoPersonaActual.getFecCreacionRegistro().after(clasificacionPersona.getCodTipoPersona().getFecCreacionRegistro())) {
                        tipoPersonaActual = clasificacionPersona.getCodTipoPersona();
                    }
                }
            }
        }
        return tipoPersonaActual;
    }

    /**
     * Método que valida si existe un documento especifico asociado a una
     * solicitud
     *
     * @param solicitudExpedienteDocumento el listado de documentos a validar
     * @param documentoTipo el documento origen
     * @return
     */
    public static boolean verificarExistenciaDocumentoSolicitud(final List<SolicitudExpedienteDocumentoEcm> solicitudExpedienteDocumento,
            final DocumentoTipo documentoTipo) {

        if ((solicitudExpedienteDocumento != null && !solicitudExpedienteDocumento.isEmpty())
                && (documentoTipo != null && StringUtils.isNotBlank(documentoTipo.getIdDocumento()))) {

            for (SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm : solicitudExpedienteDocumento) {
                final ExpedienteDocumentoEcm expedienteDocumentoEcm = solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm();

                if (documentoTipo.getIdDocumento().equals(expedienteDocumentoEcm.getDocumentoEcm().getId())) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Método que obtiene los estados de las solicitudes a obviar en la consulta
     * de efectividad
     *
     * @return los estados de las solicitudes a obviar
     */
    public static List<String> obtenerSolicitudesObviarEfectivdad() {

        List<String> solicitudesObviar
                = Arrays.asList(SolicitudEntidadEstadoEnum.ENVIADA.getCode());
        return solicitudesObviar;
    }

    /**
     * Método que obtiene la clasificacion de persona a partir de un string
     *
     * @param codDominioPersona la clasificación de la persona como String
     * @return la clasificación persona enum
     */
    public static ClasificacionPersonaEnum obtenerClasificacionPersonaFromString(final String codDominioPersona) {

        if (StringUtils.isNotBlank(codDominioPersona)) {
            for (ClasificacionPersonaEnum clasificacionPersonaEnum : ClasificacionPersonaEnum.values()) {
                if (codDominioPersona.equals(clasificacionPersonaEnum.getCode())) {
                    return clasificacionPersonaEnum;
                }
            }
        }
        return null;
    }

    /**
     * Método que obtiene una identificacionTipo a partir de una persona natural
     * o una jurídica
     *
     * @param personaNaturalTipo la persona natural
     * @param personaJuridicaTipo la persona juridica
     * @return la identificación que se obtuvo
     */
    public static IdentificacionTipo obtenerIdentificacionTipoFromChoice(final PersonaNaturalTipo personaNaturalTipo,
            final PersonaJuridicaTipo personaJuridicaTipo) {
        IdentificacionTipo identificacionResultante = null;
        if (personaNaturalTipo != null
                && personaNaturalTipo.getIdPersona() != null
                && (StringUtils.isNotBlank(personaNaturalTipo.getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(personaNaturalTipo.getIdPersona().getValNumeroIdentificacion()))) {
            identificacionResultante = personaNaturalTipo.getIdPersona();

        } else if (personaJuridicaTipo != null
                && personaJuridicaTipo.getIdPersona() != null
                && (StringUtils.isNotBlank(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdPersona().getValNumeroIdentificacion()))) {
            identificacionResultante = personaJuridicaTipo.getIdPersona();
        }
        return identificacionResultante;
    }

    /**
     * Método que valida si un aportanteTipo es igual a otro a partir de su
     * identificación
     *
     * @param aportanteTipoSrc el aportanteTipo origen
     * @param aportanteTipoDest el aportanteTipo destino
     * @return true si son iguales, false de lo contrarion
     */
    public static boolean aportanteEquals(final AportanteTipo aportanteTipoSrc, final AportanteTipo aportanteTipoDest) {
        //Determina si los aportantes son iguales
        boolean sonIguales = false;
        if (aportanteTipoSrc != null
                && aportanteTipoDest != null) {

            IdentificacionTipo identificacionAportanteSrc = Util.obtenerIdentificacionTipoFromChoice(aportanteTipoSrc.getPersonaNatural(), aportanteTipoSrc.getPersonaJuridica());
            IdentificacionTipo identificacionAportanteDest = Util.obtenerIdentificacionTipoFromChoice(aportanteTipoDest.getPersonaNatural(), aportanteTipoDest.getPersonaJuridica());

            if (identificacionAportanteSrc != null
                    && identificacionAportanteDest != null) {
                if (identificacionAportanteSrc.getCodTipoIdentificacion().equals(identificacionAportanteDest.getCodTipoIdentificacion())
                        && identificacionAportanteSrc.getValNumeroIdentificacion().equals(identificacionAportanteDest.getValNumeroIdentificacion())) {
                    sonIguales = true;
                }
            }
        }
        return sonIguales;
    }

    /**
     * Método que valida si un rangoFechaTipo es igual a un rango de fechas
     * determinado
     *
     * @param rangoFechaTipoSrc el objeto origen
     * @param fechaInicioValidar la fecha inicio a compara
     * @param fechaFinValidar la fecha fin a comparar
     * @return true si son iguales, false de lo contrario
     */
    public static boolean rangoFechaEquals(final RangoFechaTipo rangoFechaTipoSrc,
            final Calendar fechaInicioValidar, final Calendar fechaFinValidar) {
        //Determina si los rangos de fecha son iguales
        boolean sonIguales = false;
        if ((rangoFechaTipoSrc != null
                && rangoFechaTipoSrc.getFecInicio() != null && rangoFechaTipoSrc.getFecFin() != null)
                && fechaFinValidar != null && fechaInicioValidar != null) {
            //Valores de la fecha inicio del rangoFechaTipo
            int mesInicioRangoFecha = rangoFechaTipoSrc.getFecInicio().get(Calendar.MONTH);
            int diaInicioRangoFecha = rangoFechaTipoSrc.getFecInicio().get(Calendar.DAY_OF_MONTH);
            int anoInicioRangoFecha = rangoFechaTipoSrc.getFecInicio().get(Calendar.YEAR);
            //Valores de la fecha fin del rango fechaTipo
            int mesFinRangoFecha = rangoFechaTipoSrc.getFecFin().get(Calendar.MONTH);
            int diaFinRangoFecha = rangoFechaTipoSrc.getFecFin().get(Calendar.DAY_OF_MONTH);
            int anoFinRangoFecha = rangoFechaTipoSrc.getFecFin().get(Calendar.YEAR);
            //Valores de la fecha inicio a validar
            int mesInicioValidar = fechaInicioValidar.get(Calendar.MONTH);
            int diaInicioValidar = fechaInicioValidar.get(Calendar.DAY_OF_MONTH);
            int anoInicioValidar = fechaInicioValidar.get(Calendar.YEAR);
            //Valores de la fecha fin a validar
            int mesFinValidar = fechaFinValidar.get(Calendar.MONTH);
            int diaFinValidar = fechaFinValidar.get(Calendar.DAY_OF_MONTH);
            int anoFinValidar = fechaFinValidar.get(Calendar.YEAR);

            if (((mesInicioRangoFecha == mesInicioValidar)
                    && (diaInicioRangoFecha == diaInicioValidar)
                    && (anoInicioRangoFecha == anoInicioValidar))
                    && ((mesFinRangoFecha == mesFinValidar)
                    && (diaFinRangoFecha == diaFinValidar)
                    && (anoFinRangoFecha == anoFinValidar))) {
                sonIguales = true;
            }
        }
        return sonIguales;
    }

    /**
     * Método que aproxima la fecha de inicio al primer dia del mes y la fecha
     * fin al último día
     *
     * @param fechaInicio la fecha de inicio
     * @param fechaFin la fecha fin
     * @return el periodo aproximado
     */
    public static RangoFechaTipo aproximarPeriodo(final Calendar fechaInicio, final Calendar fechaFin) {
        fechaInicio.set(Calendar.DAY_OF_MONTH, fechaInicio.getActualMinimum(Calendar.DAY_OF_MONTH));
        fechaInicio.set(Calendar.HOUR_OF_DAY, Constants.HORA_INICIO_PERIODO);
        fechaInicio.set(Calendar.MINUTE, Constants.MINUTO_INICIO_PERIODO);
        fechaInicio.set(Calendar.SECOND, Constants.SEGUNDO_INICIO_PERIODO);

        fechaFin.set(Calendar.DAY_OF_MONTH, fechaFin.getActualMaximum(Calendar.DAY_OF_MONTH));
        fechaFin.set(Calendar.HOUR_OF_DAY, Constants.HORA_INICIO_PERIODO);
        fechaFin.set(Calendar.MINUTE, Constants.MINUTO_INICIO_PERIODO);
        fechaFin.set(Calendar.SECOND, Constants.SEGUNDO_INICIO_PERIODO);

        RangoFechaTipo rangoFechaTipo = new RangoFechaTipo();
        rangoFechaTipo.setFecInicio(fechaInicio);
        rangoFechaTipo.setFecFin(fechaFin);
        return rangoFechaTipo;
    }

    /**
     * Método que arma un listado de documentosTipo a partir de un arreglo de
     * Strings
     *
     * @param idDocumentos los identificadores de los documentos
     * @return el listado de documentos armados a partir del listado de strings
     */
    public static List<DocumentoTipo> buildDocumentosFromParametroList(final List<ParametroTipo> idDocumentos) {
        List<DocumentoTipo> documentosGenerados = null;
        if (idDocumentos != null && !idDocumentos.isEmpty()) {
            documentosGenerados = new ArrayList<DocumentoTipo>();
            for (ParametroTipo idDocumento : idDocumentos) {
                DocumentoTipo documentoGenerado = new DocumentoTipo();
                documentoGenerado.setIdDocumento(idDocumento.getIdLlave());
                documentosGenerados.add(documentoGenerado);
            }
        }
        return documentosGenerados;
    }
    
        /**
     * Método que arma un listado de documentosTipo a partir de un arreglo de
     * Strings
     *
     * @param idDocumentos los identificadores de los documentos
     * @return el listado de documentos armados a partir del listado de strings
     */
    public static List<DocumentoTipo> buildDocumentosFromStringList(final List<String> idDocumentos) {
        List<DocumentoTipo> documentosGenerados = null;
        if (idDocumentos != null && !idDocumentos.isEmpty()) {
            documentosGenerados = new ArrayList<DocumentoTipo>();
            for (String idDocumento : idDocumentos) {
                DocumentoTipo documentoGenerado = new DocumentoTipo();
                documentoGenerado.setIdDocumento(idDocumento);
                documentosGenerados.add(documentoGenerado);
            }
        }
        return documentosGenerados;
    }

    /**
     * @param parametros los parámetros de búsqueda
     */
    public static void buildParametrosBusquedaFormato(final List<ParametroTipo> parametros) {
        for (ParametroTipo parametroTipo : parametros) {
            if (Constants.FORMATO_PK_ID_FORMATO.equals(parametroTipo.getIdLlave())) {
                final String llaveIdFormato = Constants.FORMATO_PK.concat(Constants.FORMATO_PK_ID_FORMATO);
                parametroTipo.setIdLlave(llaveIdFormato);
            } else if (Constants.FORMATO_PK_ID_VERSION.equals(parametroTipo.getIdLlave())) {
                final String llaveidVersion = Constants.FORMATO_PK.concat(Constants.FORMATO_PK_ID_VERSION);
                parametroTipo.setIdLlave(llaveidVersion);
            }
        }
    }
}
