package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenProcesoEnum;
import co.gov.ugpp.parafiscales.enums.TipoConsultaSolicitudInformacionEnum;
import co.gov.ugpp.parafiscales.enums.TipoMedioEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudDocAnexo;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.fiscalizacion.requerimientoinformaciontipo.v1.RequerimientoInformacionTipo;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientotipo.v1.RespuestaRequerimientoTipo;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.datosplantillatipo.v1.DatosPlantillaTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1.RadicadoTipo;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import co.gov.ugpp.schema.sancion.sanciontipo.v1.SancionTipo;
import co.gov.ugpp.schema.sanciones.investigadotipo.v1.InvestigadoTipo;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1.PagoTipo;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1.SolicitudProgramaTipo;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import co.gov.ugpp.schema.transversales.documentoplantillatipo.v1.DocumentoPlantillaTipo;
import co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.EntidadNegocioTipo;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import co.gov.ugpp.schema.transversales.migraciontipo.v1.MigracionTipo;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmunocab
 */
public class Validator {

    public static void checkLiquidacion(final LiquidacionTipo liquidacionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (liquidacionTipo.getExpediente() == null
                || StringUtils.isBlank(liquidacionTipo.getExpediente().getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "identificador del Expediente")));
        }
        if (liquidacionTipo.getAportante() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "aportante")));
        } else {
            IdentificacionTipo identificacionAportante = Util.obtenerIdentificacionTipoFromChoice(liquidacionTipo.getAportante().getPersonaNatural(), liquidacionTipo.getAportante().getPersonaJuridica());
            if (identificacionAportante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        PropsReader.getKeyParam("msgCampoRequerido",
                                "aportante")));
            }
        }
    }

    public static void checkRespuestaRequerimientoInformacion(final RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (respuestaRequerimientoInformacionTipo.getFecRadicadoEntrada() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "fecRadicadoEntrada")));
        }
        if (StringUtils.isBlank(respuestaRequerimientoInformacionTipo.getIdRadicadoEntrada())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idRadicadoEntrada")));
        }
    }

    public static void checkRadicado(final RadicadoTipo radicadoTipo,
            final List<ErrorTipo> errorTipoList) {

        if (radicadoTipo.getFecRadicado() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "fecRadicado")));
        }
        if (StringUtils.isBlank(radicadoTipo.getIdRadicado())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idRadicado")));
        }

        if (StringUtils.isBlank(radicadoTipo.getCodRadicado())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codRadicado")));
        }
    }

    public static void checkAnalisisSancion(final AnalisisSancionTipo analisisSancionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (analisisSancionTipo.getUvt() == null
                || StringUtils.isBlank(analisisSancionTipo.getUvt().getIdUVT())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "uvt")));
        }
        if (StringUtils.isBlank(analisisSancionTipo.getDiasIncumplimiento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "diasIncumplimiento")));
        }

        if (StringUtils.isBlank(analisisSancionTipo.getValSancionNumeros())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valSancionNumeros")));
        }

        if (StringUtils.isBlank(analisisSancionTipo.getValSancionLetras())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valSancionLetras")));
        }

    }

    public static void checkInformacionPrueba(final InformacionPruebaTipo informacionPruebaTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(informacionPruebaTipo.getCodTipoRequerimiento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codTipoRequerimiento")));
        }

        if (StringUtils.isBlank(informacionPruebaTipo.getEsRequiereInformacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "esRequiereInformacion")));
        }
    }

    public static void checkMigracionCaso(final MigracionCasoTipo migracionCasoTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(migracionCasoTipo.getIdMigracion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idMigracion")));
        }
        if (StringUtils.isBlank(migracionCasoTipo.getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idNumExpediente")));
        }
        if (migracionCasoTipo.getIdAportante() == null
                || StringUtils.isBlank(migracionCasoTipo.getIdAportante().getCodTipoIdentificacion())
                || StringUtils.isBlank(migracionCasoTipo.getIdAportante().getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idAportante")));
        }
    }

    public static void checkMigracion(final MigracionTipo migracionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(migracionTipo.getCodEscenarioMigrado())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codEscenarioMigrado")));
        }
        if (StringUtils.isBlank(migracionTipo.getIdUsuario())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idUsuario")));
        }
        if (migracionTipo.getFecMigracion() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "fecMigracion")));
        }
    }

    public static void checkSancion(final SancionTipo sancionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(sancionTipo.getCodProcesoPadre())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codProcesoPadre")));
        }

        if (StringUtils.isBlank(sancionTipo.getCodTipoSancion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codTipoSancion")));
        }

        if (StringUtils.isBlank(sancionTipo.getCodEstadoSancion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codEstadoSancion")));
        }

        if (sancionTipo.getInvestigado() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "investigado")));
        } else {
            checkChoicePersonaNaturalPersonaJuridica(sancionTipo.getInvestigado().getPersonaNatural(),
                    sancionTipo.getInvestigado().getPersonaJuridica(), errorTipoList, InvestigadoTipo.class);
        }
    }

    public static void checkSolicitarInformacion(final SolicitudInformacionTipo solicitudInformacionTipo,
            final List<ErrorTipo> errorTipoList) {

        ValidatorSolicitudTipo.validateSolicitudTipo(solicitudInformacionTipo, errorTipoList);

        if (StringUtils.isBlank(solicitudInformacionTipo.getCodTipoConsulta())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codTipoConsulta")));
        }
        if (StringUtils.isBlank(solicitudInformacionTipo.getEsConsultaNoAportante())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "esConsultaNoAportante")));
        }
        if (StringUtils.isBlank(solicitudInformacionTipo.getEsConsultaAprobada())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "esConsultaNoAprobada")));
        }

        //Se agrega validación adicional: los pagos solo son requeridos cuando el codTipoConsulta sea (210001,160001)
        if (StringUtils.isNotBlank(solicitudInformacionTipo.getCodTipoConsulta())) {
            if (solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_PAGOS_SAYP_DETERMINACION.getCode())
                    || solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_PAGOS_SAYP_CORRESPONDENCIA.getCode())) {

                if (solicitudInformacionTipo.getPagos() == null || solicitudInformacionTipo.getPagos().isEmpty()) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "Pagos")));
                }
            }

            //Se agrega validación adicional:se debe enviar o los pagos o las planillas cuando el codTipoConsulta sea (210002, 160002)
            if (solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_DE_PAGOS_PILA_WEB_DETERMINACION.getCode())
                    || solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_DE_PAGOS_PILA_WEB_CORRESPONDENCIA.getCode())) {

                if ((solicitudInformacionTipo.getPlanillas() == null || solicitudInformacionTipo.getPlanillas().isEmpty())
                        && (solicitudInformacionTipo.getPagos() == null || solicitudInformacionTipo.getPagos().isEmpty())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "Planillas o Pagos")));
                }
            }

            //Se agrega validación adicional: las planillas solo son requeridas cuando el codTipoConsulta sea (210003, 160003)
            if (solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_DE_PAGOS_OFICIO_OPERADORES_PILA_DETERMINACION.getCode())
                    || solicitudInformacionTipo.getCodTipoConsulta().equals(TipoConsultaSolicitudInformacionEnum.VERIFICACION_DE_PAGOS_OFICIO_OPERADORES_PILA_CORRESPONDENCIA.getCode())) {

                if (solicitudInformacionTipo.getPlanillas() == null || solicitudInformacionTipo.getPlanillas().isEmpty()) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "Planillas")));
                }
            }
        }
        if (StringUtils.isNotBlank(solicitudInformacionTipo.getCodMedioSolictud())) {

            if (solicitudInformacionTipo.getCodMedioSolictud().equals(TipoMedioEnum.CORRESPONDENCIA_OFICIO.getCode())
                    || solicitudInformacionTipo.getCodMedioSolictud().equals(TipoMedioEnum.PROGRAMA.getCode())) {

                if (solicitudInformacionTipo.getEntidadExterna() == null
                        || solicitudInformacionTipo.getEntidadExterna().getIdPersona() == null
                        || StringUtils.isBlank(solicitudInformacionTipo.getEntidadExterna().getIdPersona().getCodTipoIdentificacion())
                        || StringUtils.isBlank(solicitudInformacionTipo.getEntidadExterna().getIdPersona().getValNumeroIdentificacion())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "entidadExterna")));
                }
            }
        }

        if (StringUtils.length(solicitudInformacionTipo.getDescObservacion()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "descObservacion", Constants.MAX_DESCRIPCION.toString())));
        }
        if (StringUtils.length(solicitudInformacionTipo.getDescObservacionAprobada()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "descObservacionAprobada", Constants.MAX_DESCRIPCION.toString())));
        }

        if (solicitudInformacionTipo.getPagos() != null && !solicitudInformacionTipo.getPagos().isEmpty()) {

            for (PagoTipo pago : solicitudInformacionTipo.getPagos()) {

                if (pago != null && pago.getPeriodoPago() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "PeriodoPago")));
                }
            }
        }

        //Se valida a nivel de negocio los pagos y planillas
        checkPagosAndPlanillas(solicitudInformacionTipo.getPagos(), solicitudInformacionTipo.getPlanillas(), errorTipoList);
        //Se valida a nivel de negocio el aportante y el noAportante
        checkAportanteNoAportanteAndCotizantes(solicitudInformacionTipo.getNoAportante(), solicitudInformacionTipo.getAportante(),
                solicitudInformacionTipo.getCotizantes(), errorTipoList);

    }

    public static void checkSolicitarInformacionPrograma(final SolicitudProgramaTipo solicitudProgramaTipo,
            final List<ErrorTipo> errorTipoList) {

        ValidatorSolicitudTipo.validateSolicitudTipo(solicitudProgramaTipo, errorTipoList);

        if (solicitudProgramaTipo.getCodFuenteInformacion() == null
                || solicitudProgramaTipo.getCodFuenteInformacion().isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codFuenteInformacion")));
        }

        if (StringUtils.isBlank(solicitudProgramaTipo.getValFiltros())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valFiltros")));
        }
        if (StringUtils.isBlank(solicitudProgramaTipo.getValCruces())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valCruces")));
        }
        if (StringUtils.isBlank(solicitudProgramaTipo.getValResultadoEsperado())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valResultadoEsperado")));
        }
        if (StringUtils.isBlank(solicitudProgramaTipo.getValRutaArchivoRequerimiento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valRutaArchivoRequerimiento")));
        }
        if (StringUtils.length(solicitudProgramaTipo.getValFiltros()) > Constants.MAX_DESCRIPCION_BD) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valFiltros", Constants.MAX_DESCRIPCION_BD.toString())));
        }
        if (StringUtils.length(solicitudProgramaTipo.getValResumen()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valResumen", Constants.MAX_DESCRIPCION.toString())));
        }
        if (StringUtils.length(solicitudProgramaTipo.getValRutaArchivoRequerimiento()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valRutaArchivoRequerimiento", Constants.MAX_DESCRIPCION.toString())));
        }

        if (StringUtils.length(solicitudProgramaTipo.getValCruces()) > Constants.MAX_DESCRIPCION_BD) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valCruces", Constants.MAX_DESCRIPCION_BD.toString())));
        }
        if (StringUtils.length(solicitudProgramaTipo.getValResultadoEsperado()) > Constants.MAX_DESCRIPCION_BD) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valResultadoEsperado", Constants.MAX_DESCRIPCION_BD.toString())));
        }
    }

    public static void checkExpediente(final ExpedienteTipo expedienteTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idNumExpediente")));
        }
        if (StringUtils.isBlank(expedienteTipo.getCodSeccion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codSeccion")));
        }
        if (expedienteTipo.getSerieDocumental() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "serieDocumental")));
        }
        if (expedienteTipo.getSerieDocumental() != null
                && StringUtils.isBlank(expedienteTipo.getSerieDocumental().getCodSerie())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codSerieDocumental")));
        }
        if (StringUtils.isBlank(expedienteTipo.getValFondo())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valFondo")));
        }
        if (StringUtils.isBlank(expedienteTipo.getValEntidadPredecesora())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "valEntidadPrececesora")));
        }
        if (StringUtils.length(expedienteTipo.getDescDescripcion()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "descDescripcion", Constants.MAX_DESCRIPCION.toString())));
        }
    }

    public static void checkDocumentoPlantilla(final DocumentoPlantillaTipo documentoPlantillaTipo,
            final List<ErrorTipo> errorTipoList) {

        if (documentoPlantillaTipo.getValContenidoDocumento() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "ValContenidoDocumento")));
        }

        if (StringUtils.isBlank(documentoPlantillaTipo.getCodTipoMIMEDocumento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "CodTipoMIMEDocumento")));
        }
        if (documentoPlantillaTipo.getDatosPlantilla() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "DatosPlantilla")));
        } else {
            for (DatosPlantillaTipo datosPlantillaTipo : documentoPlantillaTipo.getDatosPlantilla()) {
                if (StringUtils.isBlank(datosPlantillaTipo.getIdLlave())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "LLave")));
                }
                if (datosPlantillaTipo.getValValor() == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            PropsReader.getKeyParam("msgCampoRequerido",
                                    "datosPlantillaTipo.getValValor")));
                }
            }
        }
    }

    public static Boolean parseBooleanFromString(String fieldValue, String className,
            String fieldName, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(fieldValue)) {
            return null;
        }

        if (!"true".equals(fieldValue) && !"false".equals(fieldValue)) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCaracteresInvalidos",
                            fieldName)));
        }

        return Boolean.valueOf(fieldValue);
    }

    private static void checkPagosAndPlanillas(final List<PagoTipo> pagoTipos,
            final List<PlanillaTipo> planillaTipos, final List<ErrorTipo> errorTipoList) {

        if (pagoTipos != null && !pagoTipos.isEmpty()
                && planillaTipos != null && !planillaTipos.isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No se puede crear la solicitudInformacion, se debe enviar o un listado de pagos o un listado de planillas"));
        }
    }

    private static void checkAportanteNoAportanteAndCotizantes(final NoAportanteTipo noAportanteTipo,
            final AportanteTipo aportanteTipo,
            final List<CotizanteTipo> cotizanteTipoList,
            final List<ErrorTipo> errorTipoList) {

        int cantidadObjetos = 0;

        if (noAportanteTipo != null) {
            ++cantidadObjetos;
        }
        if (aportanteTipo != null) {
            ++cantidadObjetos;
        }
        if (cotizanteTipoList != null
                && !cotizanteTipoList.isEmpty()) {
            ++cantidadObjetos;
        }

        if (cantidadObjetos != 1) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No se puede crear la solicitudInformacion, se debe enviar o un Aportante o un NoAportante o un listado de cotizantes"));
        }
    }

    public static void checkPersonaNatural(final PersonaNaturalTipo personaNatural, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(personaNatural.getCodTipoPersonaNatural())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoPersonaNatural")));
        }
        if (personaNatural.getIdPersona() == null
                || StringUtils.isBlank(personaNatural.getIdPersona().getCodTipoIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoIdentificacion")));
        }
        if (personaNatural.getIdPersona() == null
                || StringUtils.isBlank(personaNatural.getIdPersona().getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNumeroIdentificacion")));
        }
        if (StringUtils.isBlank(personaNatural.getCodFuente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codFuente")));
        }
    }

    public static void checkPersonaJuridica(final PersonaJuridicaTipo personaJuridicaTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(personaJuridicaTipo.getCodTipoPersonaJuridica())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoPersonaJuridica")));
        }
        if (personaJuridicaTipo.getIdPersona() == null
                || StringUtils.isBlank(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoIdentificacion")));
        }
        if (personaJuridicaTipo.getIdPersona() == null
                || StringUtils.isBlank(personaJuridicaTipo.getIdPersona().getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNumeroIdentificacion")));
        }
        if (StringUtils.isBlank(personaJuridicaTipo.getValNombreRazonSocial())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNombreRazonSocial")));
        }
        if (StringUtils.isBlank(personaJuridicaTipo.getCodFuente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codFuente")));
        }
    }

    public static void checkDocumento(final DocumentoTipo documentoTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(documentoTipo.getIdDocumento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idDocumento")));
        }
        if (StringUtils.isNotBlank(documentoTipo.getIdDocumento())
                && StringUtils.length(documentoTipo.getIdDocumento()) > Constants.MAX_DESCRIPCION) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "idDocumento", Constants.MAX_DESCRIPCION.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValNombreDocumento())
                && StringUtils.length(documentoTipo.getValNombreDocumento()) > Constants.MAX_VAL_100) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valNombreDocumento", Constants.MAX_VAL_100.toString())));
        }
        if (StringUtils.isBlank(documentoTipo.getValNombreDocumento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNombreDocumento")));
        }
        if (StringUtils.isBlank(documentoTipo.getFecDocumento())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "fecDocumento")));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValNombreTipoDocumental())
                && StringUtils.length(documentoTipo.getValNombreTipoDocumental()) > Constants.MAX_VAL_70) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valNombreTipoDocumental", Constants.MAX_VAL_70.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getIdRadicadoCorrespondencia())
                && StringUtils.length(documentoTipo.getIdRadicadoCorrespondencia()) > Constants.MAX_VAL_30) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "idRadicadoCorrespondencia", Constants.MAX_VAL_30.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getCodTipoDocumento())
                && StringUtils.length(documentoTipo.getCodTipoDocumento()) > Constants.MAX_VAL_100) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "codTipoDocumento", Constants.MAX_VAL_100.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValAutorOriginador())
                && StringUtils.length(documentoTipo.getValAutorOriginador()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valAutoOriginador", Constants.MAX_VAL_50.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValNaturalezaDocumento())
                && StringUtils.length(documentoTipo.getValNaturalezaDocumento()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valNaturalezaDocumento", Constants.MAX_VAL_50.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValOrigenDocumento())
                && StringUtils.length(documentoTipo.getValOrigenDocumento()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valOrigenDocumento", Constants.MAX_VAL_50.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getDescObservacionLegibilidad())
                && StringUtils.length(documentoTipo.getDescObservacionLegibilidad()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "descObservacionLegibilidad", Constants.MAX_VAL_50.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValLegible())
                && StringUtils.length(documentoTipo.getValLegible()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valLegible", Constants.MAX_VAL_50.toString())));
        }
        if (StringUtils.isNotBlank(documentoTipo.getValTipoFirma())
                && StringUtils.length(documentoTipo.getValTipoFirma()) > Constants.MAX_VAL_50) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valTipoFirma", Constants.MAX_VAL_50.toString())));
        }
        if (documentoTipo.getNumFolios() != null
                && StringUtils.length(documentoTipo.getNumFolios().toString()) > Constants.MAX_NUM_8) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "numFolios", Constants.MAX_NUM_8.toString())));
        }
    }

    public static void checkExpedienteDocumentoEcm(final ExpedienteDocumentoEcm expedienteDocumentoEcm,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(expedienteDocumentoEcm.getIdExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idExpediente")));
        }
        if (expedienteDocumentoEcm.getDocumentoEcm() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idDocumento")));
        }
    }

    public static void checkArchivo(final ArchivoTipo archivoTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(archivoTipo.getCodTipoMIMEArchivo())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoMimeArchivo")));
        }
        if (StringUtils.isBlank(archivoTipo.getValNombreArchivo())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNombreArchivo")));
        }
        if (StringUtils.isNotBlank(archivoTipo.getValNombreArchivo())
                && StringUtils.length(archivoTipo.getValNombreArchivo()) > Constants.MAX_VAL_100) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "valNombreArchivo", Constants.MAX_VAL_100.toString())));
        }
        if (StringUtils.isNotBlank(archivoTipo.getCodTipoMIMEArchivo())
                && StringUtils.length(archivoTipo.getCodTipoMIMEArchivo()) > Constants.MAX_VAL_100) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "codTipoMIMEArchivo", Constants.MAX_VAL_100.toString())));
        }

        if (archivoTipo.getValContenidoArchivo() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "ValContenidoArchivo")));
        }

    }

    public static void checkDenuncia(final DenunciaTipo denunciaTipo,
            final List<ErrorTipo> errorTipoList) {

        if (denunciaTipo.getFecInicioPeriodo() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "FecInicioPeriodo")));
        }

        if (denunciaTipo.getFecFinPeriodo() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "FecFinPeriodo")));
        }
//        if (StringUtils.isBlank(denunciaTipo.getCodCanalDenuncia())) {
//            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
//                    PropsReader.getKeyParam("msgCampoRequerido", className, "CodCanalDenuncia")));
//        }
        if (denunciaTipo.getAportante() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "aportante")));
        } else {
            if (denunciaTipo.getAportante().getPersonaJuridica() == null
                    && denunciaTipo.getAportante().getPersonaNatural() == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        PropsReader.getKeyParam("msgCampoRequerido", "aportante")));
            } else {
                if (denunciaTipo.getAportante().getPersonaJuridica() != null) {
                    checkIdentificacion(denunciaTipo.getAportante().getPersonaJuridica().getIdPersona(), errorTipoList);
                } else if (denunciaTipo.getAportante().getPersonaNatural() != null) {
                    checkIdentificacion(denunciaTipo.getAportante().getPersonaNatural().getIdPersona(), errorTipoList);
                }
            }
        }

        if (denunciaTipo.getExpediente() == null
                || StringUtils.isBlank(denunciaTipo.getExpediente().getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "IdNumExpediente")));
        }

        if (StringUtils.isBlank(denunciaTipo.getCodTipoDenuncia())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "CodTipoDenuncia")));
        }

    }

    public static void checkFiscalizacion(final FiscalizacionTipo fiscalizacionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isNotBlank(fiscalizacionTipo.getCodOrigenProceso())
                && !OrigenProcesoEnum.PROGRAMA.getCode().equals(fiscalizacionTipo.getCodOrigenProceso())) {
            if (fiscalizacionTipo.getDenuncia() == null
                    || StringUtils.isBlank(fiscalizacionTipo.getDenuncia().getIdDenuncia())) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        PropsReader.getKeyParam("msgCampoRequerido", "idDenuncia")));
            }
        }

        if (fiscalizacionTipo.getExpediente() == null
                || StringUtils.isBlank(fiscalizacionTipo.getExpediente().getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "identificador del Expediente")));
        }

        if (fiscalizacionTipo.getAportante() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "aportante")));
        } else {
            checkChoicePersonaNaturalPersonaJuridica(fiscalizacionTipo.getAportante().getPersonaNatural(),
                    fiscalizacionTipo.getAportante().getPersonaJuridica(), errorTipoList, AportanteTipo.class);
        }
        if (fiscalizacionTipo.getPeriodoFiscalizacion() == null
                || (fiscalizacionTipo.getPeriodoFiscalizacion().getFecFin() == null
                || fiscalizacionTipo.getPeriodoFiscalizacion().getFecInicio() == null)) {

            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "periodoFiscalizacion")));
        }
    }

    private static void checkIdentificacion(final IdentificacionTipo identificacionTipo, final List<ErrorTipo> errorTipoList) {

        if (identificacionTipo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "IdentificacionTipo")));
        } else if (StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion())
                || StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoIdentificacion y valNumeroIdentificacion del aportante")));
        }
    }

    public static void checkSoilicitudDocAnexo(final SolicitudDocAnexo solicitudDocAnexo,
            final List<ErrorTipo> errorTipoList) {

        if (solicitudDocAnexo.getSolicitud()
                == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idSolicitud")));
        }

        if (StringUtils.isBlank(solicitudDocAnexo.getDocAnexo())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "documentoAnexo")));
        }
    }

    public static void checkRecursoReconsideracion(final RecursoReconsideracionTipo recursoReconsideracionTipo,
            final List<ErrorTipo> errorTipoList) {

        if (recursoReconsideracionTipo.getExpediente() == null
                || StringUtils.isBlank(recursoReconsideracionTipo.getExpediente().getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idExpediente")));
        }

        if (StringUtils.isBlank(recursoReconsideracionTipo.getCodProcesoEjecutador())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codProcesoEjecutador")));
        }

        if (recursoReconsideracionTipo.getAportante() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "aportante")));
        } else {
            if (recursoReconsideracionTipo.getAportante().getPersonaJuridica() == null
                    && recursoReconsideracionTipo.getAportante().getPersonaNatural() == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        PropsReader.getKeyParam("msgCampoRequerido", "aportante")));
            } else {
                if (recursoReconsideracionTipo.getAportante().getPersonaJuridica() != null) {
                    checkIdentificacion(recursoReconsideracionTipo.getAportante().getPersonaJuridica().getIdPersona(), errorTipoList);
                } else if (recursoReconsideracionTipo.getAportante().getPersonaNatural() != null) {
                    checkIdentificacion(recursoReconsideracionTipo.getAportante().getPersonaNatural().getIdPersona(), errorTipoList);
                }
            }
        }
        if (recursoReconsideracionTipo.getAportante() != null) {
            checkChoicePersonaNaturalPersonaJuridica(recursoReconsideracionTipo.getAportante().getPersonaNatural(),
                    recursoReconsideracionTipo.getAportante().getPersonaJuridica(), errorTipoList, AportanteTipo.class);
        }

        if (recursoReconsideracionTipo.getFecEntradaRecursoReconsideracion() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "fecEntradaRecursoReconsideracion")));
        }

        if (StringUtils.isBlank(recursoReconsideracionTipo.getIdRadicadoEntradaRecursoReconsideracion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idRadicadoEntradaRecursoReconsideracion")));
        }

    }

    public static void checkSeguimiento(final SeguimientoTipo seguimientoTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(seguimientoTipo.getIdExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idExpediente")));
        }
    }

    public static void checkParametrosFormatoEstructura(final List<ParametroTipo> parametroTipoList, final List<ErrorTipo> errorTipoList) {

        String idFormato = "";
        String idVersion = "";
        for (ParametroTipo parametroTipo : parametroTipoList) {
            if ("idFormato".equals(parametroTipo.getIdLlave())) {
                idFormato = parametroTipo.getValValor();
            } else if ("idVersion".equals(parametroTipo.getIdLlave())) {
                idVersion = parametroTipo.getValValor();
            }
        }

        if (StringUtils.isBlank(idFormato)
                || StringUtils.isBlank(idVersion)) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCamposRequeridos", "idFormato y idVersion")));
        }

    }

    public static void checkFormatoEstructura(final FormatoEstructuraTipo formatoEstructuraTipo, final List<ErrorTipo> errorTipoList) {

        if (formatoEstructuraTipo.getArchivo()
                == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "archivo")));
        } else {
            Validator.checkArchivo(formatoEstructuraTipo.getArchivo(), errorTipoList);
        }

    }

    public static void checkDocumentacionEsperada(final DocumentacionEsperadaTipo documentacionEsperadaTipo,
            final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(documentacionEsperadaTipo.getIdInstanciaProcesoReanudacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idInstanciaProcesoReanudacion")));
        }

        if (StringUtils.isBlank(documentacionEsperadaTipo.getIdRadicadoSalidaCorrespondencia())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idRadicadoSalidaCorrespondencia")));
        }

        if (StringUtils.isBlank(documentacionEsperadaTipo.getIdExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idExpediente")));
        }

        if (StringUtils.isBlank(documentacionEsperadaTipo.getValNombreModeloProceso())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNombreModeloProceso")));
        }

        if (StringUtils.isBlank(documentacionEsperadaTipo.getValDescripcion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valDescripcion")));
        }

        if (StringUtils.isBlank(documentacionEsperadaTipo.getCodEstado())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codEstado")));
        }

    }

    public static void checkRadiacion(final RadicacionTipo radicacionTipo, final List<ErrorTipo> errorTipoList) {

        /*
         if (StringUtils.isBlank(radicacionTipo.getIdRadicadoEntrada())) {
         errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
         PropsReader.getKeyParam("msgCampoRequerido", "idRadicadoEntrada")));
         }
         */
        if (StringUtils.isBlank(radicacionTipo.getIdRadicadoSalida())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idRadicadoSalida")));
        }

        /*
         if (radicacionTipo.getFecRadicadoSalida() == null) {
         errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
         PropsReader.getKeyParam("msgCampoRequerido", "fecRadicadoEntrada")));
         }
         */
    }

    public static void checkUbicacionPersona(final UbicacionPersonaTipo ubicacionPersonaTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(ubicacionPersonaTipo.getCodTipoDireccion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoDireccion")));
        }

        if (StringUtils.isBlank(ubicacionPersonaTipo.getValDireccion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valDireccion")));
        }

        if (ubicacionPersonaTipo.getMunicipio() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "municipio")));
        }

        if (ubicacionPersonaTipo.getMunicipio() != null
                && StringUtils.isBlank(ubicacionPersonaTipo.getMunicipio().getCodMunicipio())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codMunicipio")));
        }
    }

    public static void checkNomina(final IdentificacionTipo identificacionTipo, final ExpedienteTipo expedienteTipo, final List<ParametroTipo> parametroTipoList,
            final List<ErrorTipo> errorTipoList) {

        final String classNameidentificacion = IdentificacionTipo.class.getSimpleName();
        final String classNameexpediente = ExpedienteTipo.class.getSimpleName();
        final String classNameparametrolist = ParametroTipo.class.getSimpleName();

        if (identificacionTipo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "identificacionTipo")));
        }

        if (identificacionTipo != null && StringUtils.isBlank(identificacionTipo.getCodTipoIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoIdentificacion")));
        }

        if (identificacionTipo != null && StringUtils.isBlank(identificacionTipo.getValNumeroIdentificacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "valNumeroIdentificacion")));
        }

        if (StringUtils.isBlank(expedienteTipo.getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "idNumExpediente")));
        }
        if (parametroTipoList == null
                || parametroTipoList.isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "parametroTipoList")));
        }
    }

    public static void checkRequerimientoInformacionTipo(final RequerimientoInformacionTipo requerimientoInformacionTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(requerimientoInformacionTipo.getIdActoRequerimientoInformacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idActoRequerimientoInformacion")));
        }

        if (StringUtils.isNotBlank(requerimientoInformacionTipo.getIdActoRequerimientoInformacion())
                && StringUtils.length(requerimientoInformacionTipo.getIdActoRequerimientoInformacion()) > Constants.MAX_VAL_20) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "idActoRequerimientoInformacion", Constants.MAX_VAL_20.toString())));
        }

        if (StringUtils.isNotBlank(requerimientoInformacionTipo.getIdRadicadoEntradaRequerimientoInformacion())
                && StringUtils.length(requerimientoInformacionTipo.getIdRadicadoEntradaRequerimientoInformacion()) > Constants.MAX_VAL_20) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgMaxCaracteres",
                            "idRadicadoEntradaRequerimientoInformacion()", Constants.MAX_VAL_20.toString())));
        }
    }

    public static void checkDuplicateIdentificacion(final List<Identificacion> identificacionList, final List<ErrorTipo> errorTipoList, final Class srcObj) {

        for (Identificacion identificacion : identificacionList) {
            int vecesRepetidas = 0;
            for (Identificacion identificacionValidada : identificacionList) {
                if (identificacion.equals(identificacionValidada)) {
                    vecesRepetidas++;
                }
            }
            if (vecesRepetidas > 1) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        PropsReader.getKeyParam("msgIdentificacionDuplicada",
                                identificacion.toString())));
                identificacionList.remove(identificacion);
            }
        }
    }

    public static void checkChoicePersonaNaturalPersonaJuridica(final PersonaNaturalTipo personaNaturalTipo,
            final PersonaJuridicaTipo personaJuridicaTipo, final List<ErrorTipo> errorTipoList, final Class srcObj) {

        if (personaJuridicaTipo == null
                && personaNaturalTipo == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgChoiceNaturalJuridica")));

        } else if (((personaJuridicaTipo != null
                && personaJuridicaTipo.getIdPersona() != null)
                && (StringUtils.isNotBlank(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion())
                || StringUtils.isNotBlank(personaJuridicaTipo.getIdPersona().getValNumeroIdentificacion())))
                && ((personaNaturalTipo != null
                && personaNaturalTipo.getIdPersona() != null)
                && (StringUtils.isNotBlank(personaNaturalTipo.getIdPersona().getCodTipoIdentificacion())
                || StringUtils.isNotBlank(personaNaturalTipo.getIdPersona().getValNumeroIdentificacion())))) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgChoiceNaturalJuridica")));

        } else if (((personaJuridicaTipo != null
                && personaJuridicaTipo.getIdPersona() != null)
                && (StringUtils.isBlank(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isBlank(personaJuridicaTipo.getIdPersona().getValNumeroIdentificacion())))
                && ((personaNaturalTipo != null
                && personaNaturalTipo.getIdPersona() != null)
                && (StringUtils.isBlank(personaNaturalTipo.getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isBlank(personaNaturalTipo.getIdPersona().getValNumeroIdentificacion())))) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgChoiceNaturalJuridica")));
        }
    }

    public static void checkAgrupacionFiscalizacionActo(final AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo, final List<ErrorTipo> errorTipoList) {

        if (agrupacionFiscalizacionActoTipo.getFiscalizacion() == null
                || StringUtils.isBlank(agrupacionFiscalizacionActoTipo.getFiscalizacion().getIdFiscalizacion())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idFiscalizacion")));
        }

        if (agrupacionFiscalizacionActoTipo.getActosAdministrativos() == null
                || agrupacionFiscalizacionActoTipo.getActosAdministrativos().isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "actosAdministrativos")));
        }
    }

    public static void checkEntidadNegocio(final EntidadNegocioTipo entidadNegocioTipo, final List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(entidadNegocioTipo.getCodProceso())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codProceso")));
        }
        if (StringUtils.isBlank(entidadNegocioTipo.getIdEntidadNegocio())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "idEntidadNegocio")));
        }
    }

    private static void checkAportanteEntidaExterna(final AportanteTipo aportanteTipo, final EntidadExternaTipo entidadExternaTipo,
            final List<ErrorTipo> errorTipoList) {

        int cantidadObjetos = 0;

        if (aportanteTipo != null) {
            ++cantidadObjetos;
        }
        if (entidadExternaTipo != null) {
            ++cantidadObjetos;
        }

        if (cantidadObjetos > 1) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    "No se puede crear la prueba, se debe enviar o un Aportante o una Entidad Externa"));
        }
    }

    public static void checkPrueba(final PruebaTipo pruebaTipo, final List<ErrorTipo> errorTipoList) {

        if (pruebaTipo.getCodTipoPrueba() == null
                || StringUtils.isBlank(pruebaTipo.getCodTipoPrueba())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoPrueba")));
        }
        if (pruebaTipo.getFecSolicitudPrueba() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "fecSolicitudPrueba")));
        }

        if (pruebaTipo.getDescInformacionSolicitada() == null
                || StringUtils.isBlank(pruebaTipo.getDescInformacionSolicitada())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "descInformacionSolicitada")));
        }
        //Se valida a nivel de negocio el aportante y la entidad externa
        checkAportanteEntidaExterna(pruebaTipo.getAportante(), pruebaTipo.getEntidadExterna(), errorTipoList);
    }

    public static void checktrazabilidadRadicadoSalida(final AcuseEntregaDocumentoTipo acuseEntregaDocumentoTipo, List<ErrorTipo> errorTipoList) {

        if (StringUtils.isBlank(acuseEntregaDocumentoTipo.getNumRadicadoSalida())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "numRadicadoSalida")));
        }
        if (StringUtils.isBlank(acuseEntregaDocumentoTipo.getNumGuia())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "numGuia")));
        }
        if (StringUtils.isBlank(acuseEntregaDocumentoTipo.getCodEstadoEnvio())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codEstadoEnvio")));
        }
    }
}
