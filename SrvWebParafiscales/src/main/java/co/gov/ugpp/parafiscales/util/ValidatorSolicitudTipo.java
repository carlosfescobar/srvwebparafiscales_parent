package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * @author jmunocab
 */
public final class ValidatorSolicitudTipo {

    public static <E extends SolicitudTipo> void validateSolicitudTipo(E dtoClass, final List<ErrorTipo> errorTipoList) {

//        final String className = dtoClass.getClass().getSimpleName();

        if (dtoClass.getFecSolicitud() == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "fecSolicitud")));
        }
        if (dtoClass.getSolicitante() == null
                || StringUtils.isBlank(dtoClass.getSolicitante().getIdFuncionario())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "solicitante")));
        }
        if (StringUtils.isBlank(dtoClass.getCodTipoSolicitud())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", "codTipoSolicitud")));
        }
        if (StringUtils.isBlank(dtoClass.getCodMedioSolictud())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", 
                            "codMedioSolicitud")));
        }
        if (StringUtils.isBlank(dtoClass.getCodEstadoSolicitud())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "codEstadoSolicitud")));
        }
        if (StringUtils.isBlank(dtoClass.getEsAprobarSolicitud())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido",
                            "esAprobarSolicitud")));
        }
        if (dtoClass.getExpediente() == null
                || StringUtils.isBlank(dtoClass.getExpediente().getIdNumExpediente())) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                    PropsReader.getKeyParam("msgCampoRequerido", 
                            " identificador del Expediente")));
        }
    }
}
