
package co.gov.ugpp.parafiscales.util.adapter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter2
    extends XmlAdapter<String, Long>
{


    public Long unmarshal(String value) {
        return (co.gov.ugpp.parafiscales.util.JavaNumberAdapter.parseStrToLong(value));
    }

    public String marshal(Long value) {
        return (co.gov.ugpp.parafiscales.util.JavaNumberAdapter.parseLongToStr(value));
    }

}
