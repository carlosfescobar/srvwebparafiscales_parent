
package co.gov.ugpp.parafiscales.util.adapter;

import java.util.Calendar;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter3
    extends XmlAdapter<String, Calendar>
{


    public Calendar unmarshal(String value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseStrDateToCalendar(value));
    }

    public String marshal(Calendar value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseCalendarToStrDate(value));
    }

}
