
package co.gov.ugpp.parafiscales.util.adapter;

import java.util.Calendar;
import javax.xml.bind.annotation.adapters.XmlAdapter;

public class Adapter4
    extends XmlAdapter<String, Calendar>
{


    public Calendar unmarshal(String value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseStrTimeToCalendar(value));
    }

    public String marshal(Calendar value) {
        return (co.gov.ugpp.parafiscales.util.DateUtil.parseCalendarToStrTime(value));
    }

}
