package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Accion;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author zrodriguez
 */
public class AccionToAccionTipoConverter extends AbstractBidirectionalConverter<AccionTipo, Accion> {

    public AccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Accion convertTo(AccionTipo srcObj) {
        Accion accion = new Accion();
        this.copyTo(srcObj, accion);
        return accion;
    }

    @Override
    public void copyTo(AccionTipo srcObj, Accion destObj) {
//        destObj.setDescAccion(srcObj.getDescAccion());
        destObj.setFecEjecucionAccion(DateUtil.parseStrDateTimeToCalendar(srcObj.getFecEjecucionAccion()));
       
    }

    @Override
    public AccionTipo convertFrom(Accion srcObj) {
        AccionTipo accionTipo = new AccionTipo();
        this.copyFrom(srcObj, accionTipo);
        return accionTipo;
    }

    @Override
    public void copyFrom(Accion srcObj, AccionTipo destObj) {
        //destObj.setCodEstadoFiscalizacion(srcObj.getCodEstadoFiscalizacion() == null ? null : srcObj.getCodEstadoFiscalizacion().getId());
            //destObj.setDescEstadoFiscalizacion(srcObj.getCodEstadoFiscalizacion() == null ? null : srcObj.getCodEstadoFiscalizacion().getNombre());
        destObj.setCodAccion(srcObj.getCodAccion()==null ? null: srcObj.getCodAccion().getId());
        
        destObj.setDescAccion(srcObj.getCodAccion()== null ? null :srcObj.getCodAccion().getNombre());
        //destObj.setDescAccion(srcObj.getCodAccion().getDescValorDominio());
//        destObj.setDescAccion(srcObj.getDescAccion());
        destObj.setFecEjecucionAccion(srcObj.getFecEjecucionAccion()== null ? null : DateUtil.parseCalendarToStrDate(srcObj.getFecEjecucionAccion()));    
    }
}
