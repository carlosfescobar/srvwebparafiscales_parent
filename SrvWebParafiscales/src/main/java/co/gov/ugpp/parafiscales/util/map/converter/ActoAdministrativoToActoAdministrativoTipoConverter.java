package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;

/**
 *
 * @author jmuncab
 */
public class ActoAdministrativoToActoAdministrativoTipoConverter extends AbstractCustomConverter<ActoAdministrativo, ActoAdministrativoTipo> {

    public ActoAdministrativoToActoAdministrativoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ActoAdministrativoTipo convert(ActoAdministrativo srcObj) {
        return copy(srcObj, new ActoAdministrativoTipo());
    }

    @Override
    public ActoAdministrativoTipo copy(ActoAdministrativo srcObj, ActoAdministrativoTipo destObj) {
        
        destObj.setIdActoAdministrativo(srcObj.getId() == null ? null : srcObj.getId());
        destObj.setCodTipoActoAdministrativo(srcObj.getCodTipoActoAdministrativo() == null ? null : srcObj.getCodTipoActoAdministrativo().getId());
        destObj.setDescParteResolutivaActoAdministrativo(srcObj.getDescParteResolActoAdm());
        destObj.setDocumento(this.getMapperFacade().map(srcObj.getDocumentoEcm(), DocumentoTipo.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setFecActoAdministrativo(srcObj.getFecActoAdministrativo());
        destObj.setValVigencia(srcObj.getValVigencia());
        destObj.setCodAreaOrigen(srcObj.getCodAreaOrigen() == null ? null : srcObj.getCodAreaOrigen().getId());
        destObj.setDescAreaOrigen(srcObj.getCodAreaOrigen() == null ? null :srcObj.getCodAreaOrigen().getNombre());
        return destObj;
    }
}
