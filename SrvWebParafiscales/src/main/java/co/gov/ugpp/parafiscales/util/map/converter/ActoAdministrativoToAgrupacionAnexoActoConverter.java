package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativoDocumentoAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidadorArchivoTemporal;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.transversales.agrupacionactoadministrativoanexotipo.v1.AgrupacionActoAdministrativoAnexoTipo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmuncab
 */
public class ActoAdministrativoToAgrupacionAnexoActoConverter extends AbstractCustomConverter<ActoAdministrativo, AgrupacionActoAdministrativoAnexoTipo> {

    public ActoAdministrativoToAgrupacionAnexoActoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AgrupacionActoAdministrativoAnexoTipo convert(ActoAdministrativo srcObj) {
        return copy(srcObj, new AgrupacionActoAdministrativoAnexoTipo());
    }

    @Override
    public AgrupacionActoAdministrativoAnexoTipo copy(ActoAdministrativo srcObj, AgrupacionActoAdministrativoAnexoTipo destObj) {
        
        destObj.setActoAdministrativo(this.getMapperFacade().map(srcObj, ActoAdministrativoTipo.class));
        //Se obtienen los anexos al acto administrativo
        if (srcObj.getDocumentosAnexos() != null && !srcObj.getDocumentosAnexos().isEmpty()) {
            List<ParametroValoresTipo> documentosAnexosActoAdministrativo = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> documentosAnexosActo = new HashMap<String, List<ParametroTipo>>();
            for (ActoAdministrativoDocumentoAnexo documentoAnexoActoCargado : srcObj.getDocumentosAnexos()) {
                List<ParametroTipo> documentosAnexosCargados = documentosAnexosActo.get(documentoAnexoActoCargado.getCodTipoAnexoActoAdministrativo().getId());
                if (documentosAnexosCargados == null) {
                    documentosAnexosCargados = new ArrayList<ParametroTipo>();
                }
                ParametroTipo documentoAnexoActo = new ParametroTipo();
                documentoAnexoActo.setIdLlave(documentoAnexoActoCargado.getDocumentoAnexo().getId());
                documentoAnexoActo.setValValor(documentoAnexoActoCargado.getDocumentoAnexo().getValNombre());
                documentosAnexosCargados.add(documentoAnexoActo);
                documentosAnexosActo.put(documentoAnexoActoCargado.getCodTipoAnexoActoAdministrativo().getId(), documentosAnexosCargados);
            }
            for (Map.Entry<String, List<ParametroTipo>> entry : documentosAnexosActo.entrySet()) {
                ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                documentoFinal.setIdLlave(entry.getKey());
                documentoFinal.getValValor().addAll(entry.getValue());
                documentosAnexosActoAdministrativo.add(documentoFinal);
            }
            destObj.getIdDocumentosAnexos().addAll(documentosAnexosActoAdministrativo);
        }
        return destObj;
    }
}
