package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Afectado;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;

/**
 *
 * @author jlsaenzar
 */
public class AfectadoTipo2AfectadoConverter extends AbstractBidirectionalConverter<AfectadoTipo, Afectado> {

    public AfectadoTipo2AfectadoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    @Override
    public Afectado convertTo(AfectadoTipo srcObj) {
        Afectado afectado = new Afectado();
        this.copyTo(srcObj, afectado);
        return afectado;
    }

    @Override
    public void copyTo(AfectadoTipo srcObj, Afectado destObj) {
        this.getMapperFacade().map(srcObj, destObj.getPersona());
    }

    @Override
    public AfectadoTipo convertFrom(Afectado srcObj) {
        AfectadoTipo afectado = new AfectadoTipo();
        this.copyFrom(srcObj, afectado);
        return afectado;
    }

    @Override
    public void copyFrom(Afectado srcObj, AfectadoTipo destObj) {
        final Persona persona = srcObj.getPersona();

        if (persona != null) {
            final String tipoPersona = persona.getCodNaturalezaPersona() == null ? null : persona.getCodNaturalezaPersona().getId();
            if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
                destObj.setPersonaJuridica(this.getMapperFacade().map(persona, PersonaJuridicaTipo.class));
            } else {
                destObj.setPersonaNatural(this.getMapperFacade().map(persona, PersonaNaturalTipo.class));
            }
        }
        destObj.setFecIngresoEmpresa(srcObj.getFecIngresoEmpresa() == null ? null : srcObj.getFecIngresoEmpresa());
        destObj.setValIBC(srcObj.getvalIBC() == null ? null : srcObj.getvalIBC());
    }
}
