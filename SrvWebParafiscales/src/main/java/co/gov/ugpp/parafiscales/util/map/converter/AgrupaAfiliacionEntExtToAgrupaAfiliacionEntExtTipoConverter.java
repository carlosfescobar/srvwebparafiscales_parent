package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionEntidadExternaPersona;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;

/**
 *
 * @author jmuncab
 */
public class AgrupaAfiliacionEntExtToAgrupaAfiliacionEntExtTipoConverter extends AbstractCustomConverter<AgrupacionAfiliacionesPorEntidad, AgrupacionAfiliacionesPorEntidadTipo> {

    public AgrupaAfiliacionEntExtToAgrupaAfiliacionEntExtTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AgrupacionAfiliacionesPorEntidadTipo convert(AgrupacionAfiliacionesPorEntidad srcObj) {
        return copy(srcObj, new AgrupacionAfiliacionesPorEntidadTipo());
    }

    @Override
    public AgrupacionAfiliacionesPorEntidadTipo copy(AgrupacionAfiliacionesPorEntidad srcObj, AgrupacionAfiliacionesPorEntidadTipo destObj) {

        destObj.setIdAgrupacionAfiliacionesPorEntidad(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class));
        destObj.getPersonasPorAfiliar().addAll(this.getMapperFacade().map(srcObj.getPersonasAfiliar(), AgrupacionEntidadExternaPersona.class, PersonaNaturalTipo.class));
        destObj.setIdDocumentoOficioAfiliaciones(srcObj.getIdDocumentoOficioAfiliaciones() == null ? null : srcObj.getIdDocumentoOficioAfiliaciones().getId());
        destObj.setOficioAfiliacionesParcial(this.getMapperFacade().map(srcObj.getOficioAfiliacionesParcial(), AprobacionDocumentoTipo.class));
        return destObj;
    }
}
