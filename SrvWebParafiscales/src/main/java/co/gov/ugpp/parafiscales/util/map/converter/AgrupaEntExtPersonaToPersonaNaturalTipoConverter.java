package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionEntidadExternaPersona;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class AgrupaEntExtPersonaToPersonaNaturalTipoConverter extends AbstractCustomConverter<AgrupacionEntidadExternaPersona, PersonaNaturalTipo> {

    public AgrupaEntExtPersonaToPersonaNaturalTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public PersonaNaturalTipo convert(AgrupacionEntidadExternaPersona srcObj) {
        return copy(srcObj, new PersonaNaturalTipo());
    }

    @Override
    public PersonaNaturalTipo copy(AgrupacionEntidadExternaPersona srcObj, PersonaNaturalTipo destObj) {

        destObj = this.getMapperFacade().map(srcObj.getPersona(), PersonaNaturalTipo.class);
        return destObj;
    }
}
