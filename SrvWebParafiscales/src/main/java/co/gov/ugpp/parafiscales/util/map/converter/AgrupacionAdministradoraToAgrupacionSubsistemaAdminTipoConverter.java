package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAdministradora;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author zrodrigu
 */
public class AgrupacionAdministradoraToAgrupacionSubsistemaAdminTipoConverter extends AbstractCustomConverter<AgrupacionAdministradora, EntidadExternaTipo> {

    public AgrupacionAdministradoraToAgrupacionSubsistemaAdminTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public EntidadExternaTipo convert(AgrupacionAdministradora srcObj) {
        return copy(srcObj, new EntidadExternaTipo());
    }

    @Override
    public EntidadExternaTipo copy(AgrupacionAdministradora srcObj, EntidadExternaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class);

        return destObj;
    }

}
