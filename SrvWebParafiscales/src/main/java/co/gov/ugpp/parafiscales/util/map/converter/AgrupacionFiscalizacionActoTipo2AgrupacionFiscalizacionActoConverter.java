package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionActoAdministrativo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;

/**
 *
 * @author cpaingo
 */
public class AgrupacionFiscalizacionActoTipo2AgrupacionFiscalizacionActoConverter extends AbstractBidirectionalConverter<AgrupacionFiscalizacionActoTipo, Fiscalizacion> {

    public AgrupacionFiscalizacionActoTipo2AgrupacionFiscalizacionActoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Fiscalizacion convertTo(AgrupacionFiscalizacionActoTipo srcObj) {
        Fiscalizacion fiscalizacionHasActoAdmi = new Fiscalizacion();
        this.copy(srcObj, fiscalizacionHasActoAdmi);
        return fiscalizacionHasActoAdmi;
    }

    @Override
    public void copyTo(AgrupacionFiscalizacionActoTipo srcObj, Fiscalizacion destObj) {}

    @Override
    public AgrupacionFiscalizacionActoTipo convertFrom(Fiscalizacion srcObj) {
        AgrupacionFiscalizacionActoTipo agrupacionFiscalizacionActoTipo = new AgrupacionFiscalizacionActoTipo();
        this.copyFrom(srcObj, agrupacionFiscalizacionActoTipo);
        return agrupacionFiscalizacionActoTipo;
    }

    @Override
    public void copyFrom(Fiscalizacion srcObj, AgrupacionFiscalizacionActoTipo destObj) {
        destObj.setFiscalizacion(this.getMapperFacade().map(srcObj, FiscalizacionTipo.class));
        destObj.getActosAdministrativos().addAll(this.getMapperFacade().map(srcObj.getActosAdministrativosFiscalizacion(), FiscalizacionActoAdministrativo.class, ActoAdministrativoTipo.class));
    }

}
