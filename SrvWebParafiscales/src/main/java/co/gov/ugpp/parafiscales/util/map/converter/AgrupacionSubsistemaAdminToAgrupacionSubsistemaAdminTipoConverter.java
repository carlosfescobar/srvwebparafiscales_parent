package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author zrodrigu
 */
public class AgrupacionSubsistemaAdminToAgrupacionSubsistemaAdminTipoConverter extends AbstractBidirectionalConverter<AgrupacionSubsistemaTipo, AgrupacionSubsistemaAdmin> {

    public AgrupacionSubsistemaAdminToAgrupacionSubsistemaAdminTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AgrupacionSubsistemaAdmin convertTo(AgrupacionSubsistemaTipo srcObj) {
        AgrupacionSubsistemaAdmin agrupacionSubsistemaAdmin = new AgrupacionSubsistemaAdmin();
        this.copyTo(srcObj, agrupacionSubsistemaAdmin);
        return agrupacionSubsistemaAdmin;
    }

    @Override
    public void copyTo(AgrupacionSubsistemaTipo srcObj, AgrupacionSubsistemaAdmin destObj) {
        destObj.setIdSubsistema(this.getMapperFacade().map(srcObj.getSubsistema(), Subsistema.class));
        destObj.getAdministradoras().addAll(this.getMapperFacade().map(srcObj.getAdministradora(), EntidadExternaTipo.class, AgrupacionAdministradora.class));
    }

    @Override
    public AgrupacionSubsistemaTipo convertFrom(AgrupacionSubsistemaAdmin srcObj) {
        AgrupacionSubsistemaTipo agrupacionSubsistemaTipo = new AgrupacionSubsistemaTipo();
        this.copyFrom(srcObj, agrupacionSubsistemaTipo);
        return agrupacionSubsistemaTipo;
    }

    @Override
    public void copyFrom(AgrupacionSubsistemaAdmin srcObj, AgrupacionSubsistemaTipo destObj) {
        destObj.setIdAgrupacionAdministradora(srcObj.getId() ==null ? null: srcObj.getId().toString());
        destObj.setSubsistema(this.getMapperFacade().map(srcObj.getIdSubsistema(), SubsistemaTipo.class));
        destObj.getAdministradora().addAll(this.getMapperFacade().map(srcObj.getAdministradoras(), AgrupacionAdministradora.class, EntidadExternaTipo.class));
    }
}
