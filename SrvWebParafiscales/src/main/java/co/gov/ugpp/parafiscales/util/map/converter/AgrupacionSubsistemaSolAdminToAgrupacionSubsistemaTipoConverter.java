package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaSolAdmin;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;

/**
 *
 * @author Everis
 */
public class AgrupacionSubsistemaSolAdminToAgrupacionSubsistemaTipoConverter extends AbstractCustomConverter<AgrupacionSubsistemaSolAdmin, AgrupacionSubsistemaTipo> {

    public AgrupacionSubsistemaSolAdminToAgrupacionSubsistemaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AgrupacionSubsistemaTipo convert(AgrupacionSubsistemaSolAdmin srcObj) {
        return copy(srcObj, new AgrupacionSubsistemaTipo());
    }

    @Override
    public AgrupacionSubsistemaTipo copy(AgrupacionSubsistemaSolAdmin srcObj, AgrupacionSubsistemaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAdministradora(), AgrupacionSubsistemaTipo.class);
        return destObj;
    }
}