package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AnalisisSancion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;

/**
 *
 * @author jmuncab
 */
public class AnalisisSancionToAnalisisSancionTipoConverter extends AbstractCustomConverter<AnalisisSancion, AnalisisSancionTipo> {
    
    public AnalisisSancionToAnalisisSancionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public AnalisisSancionTipo convert(AnalisisSancion srcObj) {
        return copy(srcObj, new AnalisisSancionTipo());
    }
    
    @Override
    public AnalisisSancionTipo copy(AnalisisSancion srcObj, AnalisisSancionTipo destObj) {
        
        destObj.setIdAnalisisSancion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setDesObservacionesImporteSancion(srcObj.getDesObserImporteSancion());
        destObj.setDiasIncumplimiento(srcObj.getDiasIncumplimiento());
        destObj.setFecLlegadaExtemporanea(srcObj.getFecLlegadaTemporanea());
        destObj.setFecVencimientoTermino(srcObj.getFecVencimientoTermino());
        destObj.setValSancionLetras(srcObj.getValSancionLetras());
        destObj.setValSancionNumeros(srcObj.getValSancionNumeros());
        destObj.setUvt(this.getMapperFacade().map(srcObj.getUvt(), UVTTipo.class));
        return destObj;
    }
}
