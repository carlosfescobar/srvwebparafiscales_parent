/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.apoderadotipo.v1.ApoderadoTipo;

/**
 *
 * @author cpatingo
 */
public class ApoderadoTipo2ApoderadoConverter extends AbstractBidirectionalConverter<ApoderadoTipo, Persona> {

    public ApoderadoTipo2ApoderadoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Persona convertTo(ApoderadoTipo srcObj) {
       Persona apoderado = new Persona();
       this.copyTo(srcObj, apoderado);
       return apoderado;
    }

    @Override
    public void copyTo(ApoderadoTipo srcObj, Persona destObj) {
        this.getMapperFacade().map(srcObj, destObj);
    }

    @Override
    public ApoderadoTipo convertFrom(Persona srcObj) {
        ApoderadoTipo apoderadoTipo = new ApoderadoTipo();
        this.copyFrom(srcObj, apoderadoTipo);
        return apoderadoTipo;
    }

    @Override
    public void copyFrom(Persona srcObj, ApoderadoTipo destObj) {
        final Persona persona = srcObj;
        final String tipoPersona = persona.getCodNaturalezaPersona() == null ? null : persona.getCodNaturalezaPersona().getId();
        if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
            destObj.setPersonaJuridica(this.getMapperFacade().map(persona, PersonaJuridicaTipo.class));
        } else {
            destObj.setPersonaNatural(this.getMapperFacade().map(persona, PersonaNaturalTipo.class));
        }
    }
    
}
