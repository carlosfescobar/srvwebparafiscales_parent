package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;

/**
 *
 * @author jlsaenzar
 */
public class AportanteTipo2AportanteConverter extends AbstractBidirectionalConverter<AportanteTipo, Aportante> {

    public AportanteTipo2AportanteConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Aportante convertTo(AportanteTipo srcObj) {
        Aportante aportante = new Aportante();
        this.copyTo(srcObj, aportante);
        return aportante;
    }

    @Override
    public void copyTo(AportanteTipo srcObj, Aportante destObj) {
        this.getMapperFacade().map(srcObj, destObj.getPersona());
    }

    @Override
    public AportanteTipo convertFrom(Aportante srcObj) {
        AportanteTipo aportanteTipo = new AportanteTipo();
        this.copyFrom(srcObj, aportanteTipo);
        return aportanteTipo;
    }

    @Override
    public void copyFrom(Aportante srcObj, AportanteTipo destObj) {

        srcObj.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.APORTANTE);

        final Persona persona = srcObj.getPersona();
        final String tipoPersona = persona.getCodNaturalezaPersona() == null ? null : persona.getCodNaturalezaPersona().getId();

        if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
            destObj.setPersonaJuridica(this.getMapperFacade().map(persona, PersonaJuridicaTipo.class));
        } else {
            destObj.setPersonaNatural(this.getMapperFacade().map(persona, PersonaNaturalTipo.class));
        }
    }
}
