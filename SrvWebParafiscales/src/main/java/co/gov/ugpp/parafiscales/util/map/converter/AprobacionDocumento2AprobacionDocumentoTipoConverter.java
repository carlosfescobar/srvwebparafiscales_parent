package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionValidacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;

/**
 *
 * @author gchavezm
 */
public class AprobacionDocumento2AprobacionDocumentoTipoConverter extends AbstractBidirectionalConverter<AprobacionDocumentoTipo, AprobacionDocumento> {

    public AprobacionDocumento2AprobacionDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public AprobacionDocumento convertTo(AprobacionDocumentoTipo srcObj) {
        AprobacionDocumento aprobacionDocumento = new AprobacionDocumento();
        this.copyTo(srcObj, aprobacionDocumento);
        return aprobacionDocumento;
    }

    @Override
    public void copyTo(AprobacionDocumentoTipo srcObj, AprobacionDocumento destObj) {

        destObj.setArchivo(this.getMapperFacade().map(srcObj.getIdArchivo(),Archivo.class));
        destObj.getValidacionCreacionDocumento().addAll(this.getMapperFacade().map(srcObj.getValidacionCreacionDocumento(), ValidacionCreacionDocumentoTipo.class, AprobacionValidacionDocumento.class));
       
    }

    @Override
    public AprobacionDocumentoTipo convertFrom(AprobacionDocumento srcObj) {
        AprobacionDocumentoTipo aprobacionDocumentoTipo = new AprobacionDocumentoTipo();
        this.copyFrom(srcObj, aprobacionDocumentoTipo);
        return aprobacionDocumentoTipo;
    }

    @Override
    public void copyFrom(AprobacionDocumento srcObj, AprobacionDocumentoTipo destObj) {
        
        destObj.setIdArchivo(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.getValidacionCreacionDocumento().addAll(this.getMapperFacade().map(srcObj.getValidacionCreacionDocumento(), AprobacionValidacionDocumento.class, ValidacionCreacionDocumentoTipo.class));
      
    }
}

