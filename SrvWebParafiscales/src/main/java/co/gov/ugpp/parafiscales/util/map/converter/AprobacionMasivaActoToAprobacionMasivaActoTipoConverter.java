package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AprobacionMasivaActo;
import co.gov.ugpp.parafiscales.persistence.entity.ControlAprobacionActo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmunocab
 */
public class AprobacionMasivaActoToAprobacionMasivaActoTipoConverter extends AbstractCustomConverter<AprobacionMasivaActo, AprobacionMasivaActoTipo> {

    public AprobacionMasivaActoToAprobacionMasivaActoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AprobacionMasivaActoTipo convert(AprobacionMasivaActo srcObj) {
        return copy(srcObj, new AprobacionMasivaActoTipo());
    }

    @Override
    public AprobacionMasivaActoTipo copy(AprobacionMasivaActo srcObj, AprobacionMasivaActoTipo destObj) {
        destObj.setIdAprobacionMasiva(String.valueOf(srcObj.getId()));
        destObj.setFecEnvioAprobacion(srcObj.getFecEnvioAprobacion());
        destObj.setCodEstadoAprobacionMasiva(srcObj.getCodEstadoAprobacion() == null ? null : srcObj.getCodEstadoAprobacion().getId());
        destObj.setDescEstadoAprobacionMasiva(srcObj.getCodEstadoAprobacion() == null ? null : srcObj.getCodEstadoAprobacion().getNombre());
        destObj.getActosAdministrativosPorAprobar().addAll(this.getMapperFacade().map(srcObj.getActosAdministrativos(), ControlAprobacionActo.class, ControlAprobacionActoTipo.class));
        if (StringUtils.isNotBlank(srcObj.getValUsuarioResponsable())) {
            FuncionarioTipo funcionarioApruebaActo = new FuncionarioTipo();
            funcionarioApruebaActo.setIdFuncionario(srcObj.getValUsuarioResponsable());
            destObj.setIdFuncionarioResponsable(funcionarioApruebaActo);
        }
        return destObj;
    }

}
