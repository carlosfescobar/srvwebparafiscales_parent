package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AprobacionValidacionDocumento;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;

/**
 *
 * @author jmuncab
 */
public class AprobacionValidacionDocumentoToValidacionCreacionDocumentoTipoConverter extends AbstractCustomConverter<AprobacionValidacionDocumento, ValidacionCreacionDocumentoTipo> {

    public AprobacionValidacionDocumentoToValidacionCreacionDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ValidacionCreacionDocumentoTipo convert(AprobacionValidacionDocumento srcObj) {
        return copy(srcObj, new ValidacionCreacionDocumentoTipo());
    }

    @Override
    public ValidacionCreacionDocumentoTipo copy(AprobacionValidacionDocumento srcObj, ValidacionCreacionDocumentoTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getIdValidacionCreacionDocumento(), ValidacionCreacionDocumentoTipo.class);
        return destObj;
    }

}
