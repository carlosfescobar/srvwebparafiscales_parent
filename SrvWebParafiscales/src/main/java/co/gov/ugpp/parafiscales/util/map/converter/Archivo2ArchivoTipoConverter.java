package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;

/**
 *
 * @author jmuncab
 */
public class Archivo2ArchivoTipoConverter extends AbstractBidirectionalConverter<ArchivoTipo, Archivo> {

    public Archivo2ArchivoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Archivo convertTo(ArchivoTipo srcObj) {
        Archivo archivo = new Archivo();
        this.copyTo(srcObj, archivo);
        return archivo;
    }

    @Override
    public void copyTo(ArchivoTipo srcObj, Archivo destObj) {

        destObj.setValNombreArchivo(srcObj.getValNombreArchivo());
        destObj.setCodTipoMimeArchivo(srcObj.getCodTipoMIMEArchivo());
        destObj.setValContenidoArchivo(srcObj.getValContenidoArchivo());
        destObj.setValContenidoFirma(srcObj.getValContenidoFirma());
    }

    @Override
    public ArchivoTipo convertFrom(Archivo srcObj) {
        ArchivoTipo archivoTipo = new ArchivoTipo();
        this.copyFrom(srcObj, archivoTipo);
        return archivoTipo;
    }

    @Override
    public void copyFrom(Archivo srcObj, ArchivoTipo destObj) {

        destObj.setIdArchivo(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setValNombreArchivo(srcObj.getValNombreArchivo());
        destObj.setCodTipoMIMEArchivo(srcObj.getCodTipoMimeArchivo());
        destObj.setValContenidoArchivo(srcObj.getValContenidoArchivo());
        destObj.setValContenidoFirma(srcObj.getValContenidoFirma());
    }
}
