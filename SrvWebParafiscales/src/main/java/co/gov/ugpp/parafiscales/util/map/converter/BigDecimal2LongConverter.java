package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import java.math.BigDecimal;

/**
 *
 * @author rpadilla
 */
public class BigDecimal2LongConverter extends AbstractBidirectionalConverter<BigDecimal, Long> {

    public BigDecimal2LongConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Long convertTo(BigDecimal source) {
        return source == null ? null : source.longValue();
    }

    @Override
    public BigDecimal convertFrom(Long source) {
        return source == null ? null : BigDecimal.valueOf(source);
    }

    @Override
    public void copyTo(BigDecimal srcObj, Long destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void copyFrom(Long srcObj, BigDecimal destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
