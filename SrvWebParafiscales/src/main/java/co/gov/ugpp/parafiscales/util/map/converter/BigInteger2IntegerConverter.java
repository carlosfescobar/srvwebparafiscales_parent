package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import java.math.BigInteger;

/**
 *
 * @author rpadilla
 */
public class BigInteger2IntegerConverter extends AbstractBidirectionalConverter<BigInteger, Integer> {

    public BigInteger2IntegerConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Integer convertTo(BigInteger srcObj) {
        return srcObj == null ? null : srcObj.intValue();
    }

    @Override
    public BigInteger convertFrom(Integer srcObj) {
        return srcObj == null ? null : BigInteger.valueOf(srcObj);
    }

    @Override
    public void copyTo(BigInteger srcObj, Integer destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void copyFrom(Integer srcObj, BigInteger destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
