package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import java.math.BigInteger;

/**
 *
 * @author rpadilla
 */
public class BigInteger2LongConverter extends AbstractBidirectionalConverter<BigInteger, Long> {

    public BigInteger2LongConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Long convertTo(BigInteger srcObj) {
        return srcObj == null ? null : srcObj.longValue();
    }

    @Override
    public BigInteger convertFrom(Long srcObj) {
        return srcObj == null ? null : BigInteger.valueOf(srcObj);
    }

    @Override
    public void copyTo(BigInteger srcObj, Long destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void copyFrom(Long srcObj, BigInteger destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
