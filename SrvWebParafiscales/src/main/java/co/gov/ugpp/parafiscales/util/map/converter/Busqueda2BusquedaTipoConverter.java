package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Busqueda;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class Busqueda2BusquedaTipoConverter extends AbstractBidirectionalConverter<BusquedaTipo, Busqueda> {

    public Busqueda2BusquedaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Busqueda convertTo(BusquedaTipo srcObj) {
        Busqueda busqueda = new Busqueda();
        this.copyTo(srcObj, busqueda);
        return busqueda;
    }

    @Override
    public void copyTo(BusquedaTipo srcObj, Busqueda destObj) {

        destObj.setCodTipoBusqueda(this.getMapperFacade().map(srcObj.getCodTipoBusqueda(), ValorDominio.class));
        destObj.setFechaProgVisita(srcObj.getFecProgramadaVisita());
        FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
        funcionarioTipo.setIdFuncionario(this.getMapperFacade().map(srcObj.getFuncionarioEncargado(), String.class));
        destObj.setValUsuario(funcionarioTipo);
        destObj.setDesObservacionProgVisita(srcObj.getDesObservacionProgramacionVisita());
        destObj.setAutoComisorio(this.getMapperFacade().map(srcObj.getAutoComisorio(), DocumentoEcm.class));
        destObj.setCodResultadoBusqeda(this.getMapperFacade().map(srcObj.getCodResultadoBusqueda(), ValorDominio.class));
        destObj.setValPlazoEntrega(srcObj.getValPlazoEntrega());
        destObj.setDescObservaciones(srcObj.getDescObservaciones());
        destObj.setValPersonaContactada(srcObj.getValPersonaContactada());
        destObj.setValCargoPersonaContactada(srcObj.getValCargoPersonaContactada());
    }

    @Override
    public BusquedaTipo convertFrom(Busqueda srcObj) {
        BusquedaTipo busquedaTipo = new BusquedaTipo();
        this.copyFrom(srcObj, busquedaTipo);
        return busquedaTipo;
    }

    @Override
    public void copyFrom(Busqueda srcObj, BusquedaTipo destObj) {

        destObj.setIdBusqueda(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodTipoBusqueda(this.getMapperFacade().map(srcObj.getCodTipoBusqueda(), String.class));
        destObj.setFecProgramadaVisita(srcObj.getFechaProgVisita());
        destObj.setDesObservacionProgramacionVisita(srcObj.getDesObservacionProgVisita());
        destObj.setAutoComisorio(this.getMapperFacade().map(srcObj.getAutoComisorio(), DocumentoTipo.class));
        destObj.setCodResultadoBusqueda(this.getMapperFacade().map(srcObj.getCodResultadoBusqeda(), String.class));
        destObj.setValPlazoEntrega(srcObj.getValPlazoEntrega());
        destObj.setDescObservaciones(srcObj.getDescObservaciones());
        destObj.setValPersonaContactada(srcObj.getValPersonaContactada());
        destObj.setValCargoPersonaContactada(srcObj.getValCargoPersonaContactada());

        if (StringUtils.isNotBlank(srcObj.getValUsuario())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getValUsuario());
            destObj.setFuncionarioEncargado(funcionarioTipo);
        }
    }
}
