package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.persistence.entity.MigracionCasoParafiscales;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class CasoMigradoToCasoMigradoTipoConverter extends AbstractCustomConverter<MigracionCasoParafiscales, MigracionCasoTipo> {

    public CasoMigradoToCasoMigradoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public MigracionCasoTipo convert(MigracionCasoParafiscales srcObj) {
        return copy(srcObj, new MigracionCasoTipo());
    }

    @Override
    public MigracionCasoTipo copy(MigracionCasoParafiscales srcObj, MigracionCasoTipo destObj) {
        destObj.setIdFila(srcObj.getIdFila());
        destObj.setIdMigracion(srcObj.getMigracionParafiscales().getId().toString());
        destObj.setCodEstadoCaso(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getId());
        destObj.setDescEstadoCaso(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getNombre());
        destObj.setDescErrorCaso(srcObj.getDescErrorCaso());
        destObj.setIdNumExpediente(srcObj.getIdExpediente());
        destObj.setIdInstanciaProceso(srcObj.getIdInstanciaProceso());
        if (StringUtils.isNotBlank(srcObj.getCodTipoDocumento())
                && StringUtils.isNotBlank(srcObj.getValNumeroDocumento())) {
            IdentificacionTipo identificacionTipo = new IdentificacionTipo();
            identificacionTipo.setCodTipoIdentificacion(srcObj.getCodTipoDocumento());
            identificacionTipo.setValNumeroIdentificacion(srcObj.getValNumeroDocumento());
        }
        return destObj;
    }
}
