package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ConciliacionContable;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.liquidador.conciliacioncontabletipo.v1.ConciliacionContableTipo;

/**
 *
 * @author jmuncab
 */
public class ConciliacionContableToConciliacionContableTipoConverter extends AbstractCustomConverter<ConciliacionContable, ConciliacionContableTipo> {

    public ConciliacionContableToConciliacionContableTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }


    @Override
    public ConciliacionContableTipo convert(ConciliacionContable srcObj) {
       return copy(srcObj, new ConciliacionContableTipo());
    }

    @Override
    public ConciliacionContableTipo copy(ConciliacionContable srcObj, ConciliacionContableTipo destObj) {
       destObj.setIdConciliacionContable(srcObj.getId().toString());
       destObj.setEstado(srcObj.getEstado());
       return  destObj;
    }
}
