package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaDecisionesRecurso;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class ConsDecRecursoToConsDecRecursoTipoConverter extends AbstractCustomConverter<ConstanciaDecisionesRecurso, ParametroTipo> {

    public ConsDecRecursoToConsDecRecursoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ParametroTipo convert(ConstanciaDecisionesRecurso srcObj) {
        return copy(srcObj, new ParametroTipo());
    }

    @Override
    public ParametroTipo copy(ConstanciaDecisionesRecurso srcObj, ParametroTipo destObj) {
        destObj.setIdLlave(srcObj.getCodDecisionRecurso() == null ? null : srcObj.getCodDecisionRecurso().getId());
        destObj.setValValor(this.getMapperFacade().map(srcObj.getEsDecisionRecurso(), String.class));
        return destObj;
    }
}
