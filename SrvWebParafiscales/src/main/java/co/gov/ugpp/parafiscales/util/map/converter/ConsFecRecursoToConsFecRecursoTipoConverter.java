package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaFechaRecurso;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class ConsFecRecursoToConsFecRecursoTipoConverter extends AbstractCustomConverter<ConstanciaFechaRecurso, ParametroTipo> {

    public ConsFecRecursoToConsFecRecursoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ParametroTipo convert(ConstanciaFechaRecurso srcObj) {
        return copy(srcObj, new ParametroTipo());
    }

    @Override
    public ParametroTipo copy(ConstanciaFechaRecurso srcObj, ParametroTipo destObj) {
        destObj.setIdLlave(srcObj.getCodTipoFecRecurso() == null ? null : srcObj.getCodTipoFecRecurso().getId());
        destObj.setValValor(srcObj.getFecRecurso() == null ? null : DateUtil.parseCalendarToStrDateBPMFormat(srcObj.getFecRecurso()));
        return destObj;
    }
}
