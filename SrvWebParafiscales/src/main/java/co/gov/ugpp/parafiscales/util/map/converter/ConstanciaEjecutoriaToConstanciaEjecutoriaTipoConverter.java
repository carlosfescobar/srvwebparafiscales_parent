package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaDecisionesRecurso;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaEjecutoria;
import co.gov.ugpp.parafiscales.persistence.entity.ConstanciaFechaRecurso;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;

/**
 *
 * @author jmuncab
 */
public class ConstanciaEjecutoriaToConstanciaEjecutoriaTipoConverter extends AbstractCustomConverter<ConstanciaEjecutoria, ConstanciaEjecutoriaTipo> {

    public ConstanciaEjecutoriaToConstanciaEjecutoriaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ConstanciaEjecutoriaTipo convert(ConstanciaEjecutoria srcObj) {
        return copy(srcObj, new ConstanciaEjecutoriaTipo());
    }

    @Override
    public ConstanciaEjecutoriaTipo copy(ConstanciaEjecutoria srcObj, ConstanciaEjecutoriaTipo destObj) {

        destObj.setIdConstanciaEjecutoria(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodFallo(srcObj.getCodFallo() == null ? null : srcObj.getCodFallo().getId());
        destObj.setDescFallo(srcObj.getCodFallo() == null ? null : srcObj.getCodFallo().getNombre());
        destObj.getFecRecursos().addAll(this.getMapperFacade().map(srcObj.getFechasRecurso(), ConstanciaFechaRecurso.class, ParametroTipo.class));
        destObj.getValDecisionesRecursos().addAll(this.getMapperFacade().map(srcObj.getDecisionesRecurso(), ConstanciaDecisionesRecurso.class, ParametroTipo.class));
        destObj.setFecConstanciaEjecutoria(srcObj.getFecConstanciaEjecutoria());
        destObj.setFecCreacionDocumentoConstancia(srcObj.getFecCreacionDocumentoConstancia());
        destObj.setIdDocumentoConstanciaEjecutoria(srcObj.getDocumentoConstanciaEjecutoria() == null ? null : srcObj.getDocumentoConstanciaEjecutoria().getId());
        return destObj;
    }
}
