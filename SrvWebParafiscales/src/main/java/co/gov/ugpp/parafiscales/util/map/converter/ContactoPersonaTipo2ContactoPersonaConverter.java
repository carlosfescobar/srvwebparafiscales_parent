package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author rpadilla
 */
public class ContactoPersonaTipo2ContactoPersonaConverter extends AbstractBidirectionalConverter<ContactoPersonaTipo, ContactoPersona> {

    public ContactoPersonaTipo2ContactoPersonaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ContactoPersona convertTo(ContactoPersonaTipo srcObj) {
        ContactoPersona contactoPersona = new ContactoPersona();
        this.copyTo(srcObj, contactoPersona);
        return contactoPersona;
    }

    @Override
    public void copyTo(ContactoPersonaTipo srcObj, ContactoPersona destObj) {
        destObj.setEsAutorizaNotificacionElectronica(Boolean.valueOf(srcObj.getEsAutorizaNotificacionElectronica()));
        destObj.getCorreoElectronicoList().addAll(this.getMapperFacade().map(srcObj.getCorreosElectronicos(), CorreoElectronicoTipo.class, CorreoElectronico.class));
        destObj.getTelefonoList().addAll(this.getMapperFacade().map(srcObj.getTelefonos(), TelefonoTipo.class, Telefono.class));
        destObj.getUbicacionList().addAll(this.getMapperFacade().map(srcObj.getUbicaciones(), UbicacionPersonaTipo.class,
                Ubicacion.class));
    }

    @Override
    public ContactoPersonaTipo convertFrom(ContactoPersona srcObj) {
        ContactoPersonaTipo contactoPersonaTipo = new ContactoPersonaTipo();
        this.copyFrom(srcObj, contactoPersonaTipo);
        return contactoPersonaTipo;
    }

    @Override
    public void copyFrom(ContactoPersona srcObj, ContactoPersonaTipo destObj) {
        destObj.setEsAutorizaNotificacionElectronica(this.getMapperFacade().map(srcObj.getEsAutorizaNotificacionElectronica(), String.class));
        if (srcObj.getCorreoElectronicoList() != null && !srcObj.getCorreoElectronicoList().isEmpty()) {
            List<CorreoElectronico> correosElectronicos = new ArrayList<CorreoElectronico>();
            for (CorreoElectronico correo : srcObj.getCorreoElectronicoList()) {
                if (correo.getCodFuente() != null || StringUtils.isNotBlank(correo.getValCorreoElectronico())) {
                    correosElectronicos.add(correo);
                }
            }
            destObj.getCorreosElectronicos().addAll(this.getMapperFacade().map(correosElectronicos, CorreoElectronico.class, CorreoElectronicoTipo.class));
        }

        if (srcObj.getTelefonoList() != null) {
            destObj.getTelefonos().addAll(this.getMapperFacade().map(srcObj.getTelefonoList(), Telefono.class, TelefonoTipo.class));
        }
        if (srcObj.getUbicacionList() != null) {
            destObj.getUbicaciones().addAll(this.getMapperFacade().map(srcObj.getUbicacionList(), Ubicacion.class,
                    UbicacionPersonaTipo.class));
        }
    }
}
