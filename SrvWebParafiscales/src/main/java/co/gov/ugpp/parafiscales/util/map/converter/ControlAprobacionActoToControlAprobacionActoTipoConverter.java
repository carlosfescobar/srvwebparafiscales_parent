/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ControlAprobacionActo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author yrinconh
 */
public class ControlAprobacionActoToControlAprobacionActoTipoConverter extends AbstractCustomConverter<ControlAprobacionActo, ControlAprobacionActoTipo> {
    
    public ControlAprobacionActoToControlAprobacionActoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public ControlAprobacionActoTipo convert(ControlAprobacionActo srcObj) {
        return copy(srcObj, new ControlAprobacionActoTipo());
    }
    
    @Override
    public ControlAprobacionActoTipo copy(ControlAprobacionActo srcObj, ControlAprobacionActoTipo destObj) {
        destObj.setIdControlAprobacionActo(String.valueOf(srcObj.getId()));
        destObj.setIdArchivo(srcObj.getIdArchivo() == null ? null : String.valueOf(srcObj.getIdArchivo()));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.setFecAprobacionActo(srcObj.getFecAprobacionActo());
        destObj.setFecTareaAprobacionEnCola(srcObj.getFecTareaAprobacionEnCola());        
        destObj.setIdActoAdministrativoCreado(srcObj.getIdActoAdministrativoCreado() == null ? null : srcObj.getIdActoAdministrativoCreado().getId());
        destObj.setCodTipoActoAprobacion(srcObj.getCodTipoActoAprobacion() == null ? null : srcObj.getCodTipoActoAprobacion().getId());
        destObj.setDescTipoActoAprobacion(srcObj.getCodTipoActoAprobacion() == null ? null : srcObj.getCodTipoActoAprobacion().getNombre());
        destObj.setCodEstadoAprobacion(srcObj.getCodEstadoAprobacion()== null ? null : srcObj.getCodEstadoAprobacion().getId());
        destObj.setDescEstadoAprobacion(srcObj.getCodEstadoAprobacion()== null ? null : srcObj.getCodEstadoAprobacion().getNombre());
        destObj.setValRolEncargadoAprobar(srcObj.getValRolEncargadoAprobar() == null ? null : srcObj.getValRolEncargadoAprobar());
        destObj.setIdAprobacionMasivaActo(srcObj.getAprobacionMasivaActo() == null ? null :  srcObj.getAprobacionMasivaActo().getId().toString());
        
        if (StringUtils.isNotBlank(srcObj.getIdFuncionarioApruebaActo())) {
                FuncionarioTipo funcionarioApruebaActo = new FuncionarioTipo();
                funcionarioApruebaActo.setIdFuncionario(srcObj.getIdFuncionarioApruebaActo());
                destObj.setIdFuncionarioApruebaActo(funcionarioApruebaActo);
            }
        if (StringUtils.isNotBlank(srcObj.getIdFuncionarioElaboraActo())) {
                FuncionarioTipo funcionarioElaboraActo = new FuncionarioTipo();
                funcionarioElaboraActo.setIdFuncionario(srcObj.getIdFuncionarioElaboraActo());
                destObj.setIdFuncionarioElaboraActo(funcionarioElaboraActo);
            }
        
        return destObj;
    }
    
}
