package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ControlEnvioComunicacion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1.ControlEnvioComunicacionTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;

/**
 *
 * @author zrodrigu
 */
public class ControlEnvioComunicacionToControlEnvioComunicacionTipoConverter extends AbstractCustomConverter<ControlEnvioComunicacion, ControlEnvioComunicacionTipo> {

    public ControlEnvioComunicacionToControlEnvioComunicacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ControlEnvioComunicacionTipo convert(ControlEnvioComunicacion srcObj) {
        return copy(srcObj, new ControlEnvioComunicacionTipo());
    }

    @Override
    public ControlEnvioComunicacionTipo copy(ControlEnvioComunicacion srcObj, ControlEnvioComunicacionTipo destObj) {

        destObj.setIdControlEnvioComunicacion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setNotificacion(this.getMapperFacade().map(srcObj.getNotificacion(), NotificacionTipo.class));
        destObj.setCodEstado(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getId());
        destObj.setDescEstado(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getNombre());
        destObj.setDescObservacionesRechazo(srcObj.getDescObservacionesRechazo());
        destObj.setDescObservacionesCorreccion(srcObj.getDescObservacionesCorreccion());
        destObj.setFecRechazo(srcObj.getFecRechazo() == null ? null : srcObj.getFecRechazo());
        destObj.setFecCorreccion(srcObj.getFecCorreccion() == null ? null : srcObj.getFecCorreccion());
        destObj.setIdUsuarioRechaza(srcObj.getIdUsuarioRechaza());
        destObj.setIdUsuarioCorrige(srcObj.getIdUsuarioCorrige());

        return destObj;
    }
}
