package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.enums.OrigenTipoDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidadorArchivoTemporal;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;
import co.gov.ugpp.schema.liquidador.conciliacioncontabletipo.v1.ConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.controlliquidadortipo.v1.ControlLiquidadorTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.schema.transversales.nominatipo.v1.NominaTipo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author jmuncab
 */
public class ControlLiquidadorToControlLiquidadorTipoConverter extends AbstractCustomConverter<ControlLiquidador, ControlLiquidadorTipo> {

    public ControlLiquidadorToControlLiquidadorTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ControlLiquidadorTipo convert(ControlLiquidador srcObj) {
        return copy(srcObj, new ControlLiquidadorTipo());
    }

    @Override
    public ControlLiquidadorTipo copy(ControlLiquidador srcObj, ControlLiquidadorTipo destObj) {

        destObj.setIdControlLiquidador(srcObj.getId().toString());
        destObj.setIdEnvioConciliacion(srcObj.getEnvioConciliacion() == null ? null : srcObj.getEnvioConciliacion().toString());
        destObj.setIdEnvioNomina(srcObj.getEnvioNomina() == null ? null : srcObj.getEnvioNomina().toString());
        destObj.setIdInformacionExternaConciliacion(srcObj.getInfoExtConciliacion() == null ? null : srcObj.getInfoExtConciliacion().toString());
        destObj.setIdInformacionExternaNomina(srcObj.getInformacionExterNomina() == null ? null : srcObj.getInformacionExterNomina().toString());
        destObj.setCodLineaAccion(srcObj.getCodLineaAccion() == null ? null : srcObj.getCodLineaAccion().getId());
        destObj.setCodOrigen(srcObj.getCodOrigen() == null ? null : srcObj.getCodOrigen().getId());
        destObj.setCodProcesoSolicitador(srcObj.getCodProcesoSolicitador() == null ? null : srcObj.getCodProcesoSolicitador().getId());
        destObj.setCodTipoDenuncia(srcObj.getCodTipoDenuncia() == null ? null : srcObj.getCodTipoDenuncia().getId());
        destObj.setFormatoConciliacion(this.getMapperFacade().map(srcObj.getFormatoConciliacion(), FormatoTipo.class));
        destObj.setFormatoNomina(this.getMapperFacade().map(srcObj.getFormatoNomina(), FormatoTipo.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.setHallazgosConciliacionNomina(this.getMapperFacade().map(srcObj.getHallazgoConcilNomina(),  HallazgoConciliacionContableTipo.class));
        destObj.setHallazgosNominaPila(this.getMapperFacade().map(srcObj.getHallazgoNominaPila(), HallazgoNominaTipo.class));
        destObj.setValPrograma(srcObj.getValPrograma());
        destObj.setDescCargueNominaConciliacion(srcObj.getDesCargueNominaConciliacion());
        destObj.setEsSolicitarAclaraciones(this.getMapperFacade().map(srcObj.getEsSolicitarAclaraciones(), String.class));
        destObj.setIdPilaDepurada(srcObj.getIdPilaDepurada());
        destObj.setFecRadicadoEntrada(srcObj.getFecRadicadoEntrada());
        destObj.setFecRadicadoSalida(srcObj.getFecRadicadoSalida());
        destObj.setIdRadicadoEntrada(srcObj.getIdRadicadoEntrada());
        destObj.setIdRadicadoSalida(srcObj.getIdRadicadoSalida());
        destObj.setConciliacionContable(this.getMapperFacade().map(srcObj.getConciliacionContable(), ConciliacionContableTipo.class));
        destObj.setHojaCalculo(this.getMapperFacade().map(srcObj.getHojaCalculoLiquidacion(), HojaCalculoTipo.class));
        destObj.setNomina(this.getMapperFacade().map(srcObj.getNomina(), NominaTipo.class));
        destObj.setCodTipoEjecucionLiquidador(this.getMapperFacade().map(srcObj.getCodTipoEjecucionLiquidador(), String.class));
        destObj.setValLiquidacionAporte(srcObj.getValLiquidacionAporte());
        destObj.setValLiquidacionSancionInexactitud(srcObj.getValLiquidacionSancionInexactitud());
        destObj.setValLiquidacionSancionOmision(srcObj.getValLiquidacionSancionOmision());
        destObj.setEsIncumplimiento(this.getMapperFacade().map(srcObj.getEsIncumplimiento(), String.class));
        destObj.setCodCausalCierre(srcObj.getCodCausalCierre() == null ? null : srcObj.getCodCausalCierre().getId());
        destObj.setValPartidasGlobales(srcObj.getValPartidasGlobales());
        //Se poblan los archivos temporales del control liquidador
        if (srcObj.getArchivosTemporales() != null && !srcObj.getArchivosTemporales().isEmpty()) {
            List<ParametroValoresTipo> archivosTemporales = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> archivosTemporalesControlLiquidador = new HashMap<String, List<ParametroTipo>>();
            for (ControlLiquidadorArchivoTemporal archivoTemporal : srcObj.getArchivosTemporales()) {
                List<ParametroTipo> archivosControlLiquidador = archivosTemporalesControlLiquidador.get(archivoTemporal.getCodTipoArchivo().getId());
                if (archivosControlLiquidador == null) {
                    archivosControlLiquidador = new ArrayList<ParametroTipo>();
                }
                ParametroTipo archivoControlLiquidador = new ParametroTipo();
                archivoControlLiquidador.setIdLlave(archivoTemporal.getArchivo().getId().toString());
                archivoControlLiquidador.setValValor(archivoTemporal.getArchivo().getValNombreArchivo());
                archivosControlLiquidador.add(archivoControlLiquidador);
                archivosTemporalesControlLiquidador.put(archivoTemporal.getCodTipoArchivo().getId(), archivosControlLiquidador);
            }
            for (Entry<String, List<ParametroTipo>> entry : archivosTemporalesControlLiquidador.entrySet()) {
                ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                documentoFinal.setIdLlave(entry.getKey());
                documentoFinal.getValValor().addAll(entry.getValue());
                archivosTemporales.add(documentoFinal);
            }
            destObj.getArchivosTemporales().addAll(archivosTemporales);
        }
        //Se poblan los documentos finales del control liquidador
        if (srcObj.getExpediente() != null
                && srcObj.getExpediente().getExpedienteDocumentoList() != null
                && !srcObj.getExpediente().getExpedienteDocumentoList().isEmpty()) {
            Map<String, List<ParametroTipo>> documentosTipificadosControlLiquidador = null;
            for (ExpedienteDocumentoEcm expedienteDocumentoEcm : srcObj.getExpediente().getExpedienteDocumentoList()) {
                if (expedienteDocumentoEcm.getCodOrigen() != null) {
                    for (OrigenTipoDocumentoEnum origenTipoDocumentoEnum : OrigenTipoDocumentoEnum.values()) {
                        if (origenTipoDocumentoEnum.getCodOrigenDocumento().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            if (documentosTipificadosControlLiquidador == null) {
                                documentosTipificadosControlLiquidador = new HashMap<String, List<ParametroTipo>>();
                            }
                            List<ParametroTipo> documentosControlLiquidador = documentosTipificadosControlLiquidador.get(origenTipoDocumentoEnum.getCodTipoDocumento());
                            if (documentosControlLiquidador == null || documentosControlLiquidador.isEmpty()) {
                                documentosControlLiquidador = new ArrayList<ParametroTipo>();
                            }
                            final ParametroTipo documentoControlLiquidador = new ParametroTipo();
                            documentoControlLiquidador.setIdLlave(expedienteDocumentoEcm.getDocumentoEcm().getId());
                            documentoControlLiquidador.setValValor(expedienteDocumentoEcm.getDocumentoEcm().getValNombre());
                            documentosControlLiquidador.add(documentoControlLiquidador);
                            documentosTipificadosControlLiquidador.put(origenTipoDocumentoEnum.getCodTipoDocumento(), documentosControlLiquidador);
                        }
                    }
                }
            }
            if (documentosTipificadosControlLiquidador != null && !documentosTipificadosControlLiquidador.isEmpty()) {
                List<ParametroValoresTipo> documentosFinales = new ArrayList<ParametroValoresTipo>();
                for (Entry<String, List<ParametroTipo>> entry : documentosTipificadosControlLiquidador.entrySet()) {
                    ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                    documentoFinal.setIdLlave(entry.getKey());
                    documentoFinal.getValValor().addAll(entry.getValue());
                    documentosFinales.add(documentoFinal);
                }
                destObj.getDocumentosFinales().addAll(documentosFinales);
            }
        }
        return destObj;
    }
}
