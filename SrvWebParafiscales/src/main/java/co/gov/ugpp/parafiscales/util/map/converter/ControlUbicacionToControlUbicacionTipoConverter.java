package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ControlUbicacionEnvio;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;

/**
 *
 * @author jmuncab
 */
public class ControlUbicacionToControlUbicacionTipoConverter extends AbstractCustomConverter<ControlUbicacionEnvio, ControlUbicacionEnvioTipo> {

    public ControlUbicacionToControlUbicacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ControlUbicacionEnvioTipo convert(ControlUbicacionEnvio srcObj) {
        return copy(srcObj, new ControlUbicacionEnvioTipo());
    }

    @Override
    public ControlUbicacionEnvioTipo copy(ControlUbicacionEnvio srcObj, ControlUbicacionEnvioTipo destObj) {

        destObj.setIdControlUbicacion(srcObj.getId().toString());
        destObj.setCodEstadoUbicacion(srcObj.getCodEstadoUbicacion() == null ? null : srcObj.getCodEstadoUbicacion().getId());
        destObj.setDescEstadoUbicacion(srcObj.getCodEstadoUbicacion() == null ? null : srcObj.getCodEstadoUbicacion().getNombre());
        destObj.setEventoEnvio(this.getMapperFacade().map(srcObj.getEventosEnvio(), EventoEnvioTipo.class));
        destObj.setUbicacion(this.getMapperFacade().map(srcObj.getUbicacion(), UbicacionPersonaTipo.class));
        return destObj;
    }
}
