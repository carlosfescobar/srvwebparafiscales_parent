package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;

/**
 *
 * @author rpadilla
 */
public class CorreoElectronicoTipo2CorreoElectronicoConverter extends AbstractBidirectionalConverter<CorreoElectronicoTipo, CorreoElectronico> {
    
    public CorreoElectronicoTipo2CorreoElectronicoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public CorreoElectronico convertTo(CorreoElectronicoTipo srcObj) {
        CorreoElectronico correoElectronico = new CorreoElectronico();
        this.copyTo(srcObj, correoElectronico);
        return correoElectronico;
    }
    
    @Override
    public void copyTo(CorreoElectronicoTipo srcObj, CorreoElectronico destObj) {
      destObj.setValCorreoElectronico(srcObj.getValCorreoElectronico());
      destObj.setCodFuente(this.getMapperFacade().map(srcObj.getCodFuente(), ValorDominio.class));
        
    }
    
    @Override
    public CorreoElectronicoTipo convertFrom(CorreoElectronico srcObj) {
        CorreoElectronicoTipo correoElectronicoTipo = new CorreoElectronicoTipo();
        this.copyFrom(srcObj, correoElectronicoTipo);
        return correoElectronicoTipo;
    }
    
    @Override
    public void copyFrom(CorreoElectronico srcObj, CorreoElectronicoTipo destObj) {
        destObj.setValCorreoElectronico(srcObj.getValCorreoElectronico());
        destObj.setCodFuente(srcObj.getCodFuente() == null ? null : srcObj.getCodFuente().getId());
      destObj.setDescFuenteCorreo(srcObj.getCodFuente()== null ? null :srcObj.getCodFuente().getNombre());
    }
    
}
