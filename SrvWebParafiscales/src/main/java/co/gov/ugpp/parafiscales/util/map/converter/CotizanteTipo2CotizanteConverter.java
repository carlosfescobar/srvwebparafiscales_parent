package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Cotizante;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.Converter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;

/**
 *
 * @author rpadilla
 */
public class CotizanteTipo2CotizanteConverter extends AbstractBidirectionalConverter<CotizanteTipo, Cotizante> {

    private final Converter personaNaturalTipo2PersonaConverter;

    public CotizanteTipo2CotizanteConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
        personaNaturalTipo2PersonaConverter = this.getConverterFactory().getConverter(PersonaNaturalTipo.class, Persona.class);
    }

    @Override
    public Cotizante convertTo(CotizanteTipo srcObj) {
        Cotizante cotizante = new Cotizante();
        this.copyTo(srcObj, cotizante);
        return cotizante;
    }

    @Override
    public void copyTo(CotizanteTipo srcObj, Cotizante destObj) {
        personaNaturalTipo2PersonaConverter.copy(srcObj, destObj.getPersona());
    }

    @Override
    public CotizanteTipo convertFrom(Cotizante srcObj) {
        CotizanteTipo cotizanteTipo = new CotizanteTipo();
        this.copyFrom(srcObj, cotizanteTipo);
        return cotizanteTipo;
    }

    @Override
    public void copyFrom(Cotizante srcObj, CotizanteTipo destObj) {
        
        destObj.setDescObservacion(srcObj.getInfoCotizante() == null ? null : srcObj.getInfoCotizante().getDescObservacion());
        destObj.setDocCotizante(srcObj.getInfoCotizante() == null ? null : this.getMapperFacade().map(srcObj.getInfoCotizante().getArchivo(), ArchivoTipo.class));
        //Antes de llamar al converter de persona natural, se setea el atributo discriminador
        srcObj.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.COTIZANTE);
        personaNaturalTipo2PersonaConverter.copy(srcObj.getPersona(), destObj);
    }
}
