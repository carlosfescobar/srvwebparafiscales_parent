package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudCotizante;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;

/**
 *
 * @author jsaenzar
 */
public class CotizanteTipo2SolicitudCotizanteConverter extends AbstractBidirectionalConverter<CotizanteTipo, SolicitudCotizante> {

    public CotizanteTipo2SolicitudCotizanteConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudCotizante convertTo(CotizanteTipo srcObj) {
        SolicitudCotizante solicitudCotizante = new SolicitudCotizante();
        this.copyTo(srcObj, solicitudCotizante);
        return solicitudCotizante;
    }

    @Override
    public void copyTo(CotizanteTipo srcObj, SolicitudCotizante destObj) {
        this.getMapperFacade().map(srcObj, destObj.getCotizante());
    }

    @Override
    public CotizanteTipo convertFrom(SolicitudCotizante srcObj) {
        CotizanteTipo cotizanteTipo = new CotizanteTipo();
        this.copyFrom(srcObj, cotizanteTipo);
        cotizanteTipo.setDocCotizante(this.getMapperFacade().map(srcObj.getArchivo(), ArchivoTipo.class));
        return cotizanteTipo;
    }

    @Override
    public void copyFrom(SolicitudCotizante srcObj, CotizanteTipo destObj) {
        this.getMapperFacade().map(srcObj.getCotizante(), destObj);
    }

}
