package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;

/**
 *
 * @author jmuncab
 */
public class DenunciaToDenunciaTipoConverter extends AbstractCustomConverter<Denuncia, DenunciaTipo> {

    public DenunciaToDenunciaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public DenunciaTipo convert(Denuncia srcObj) {
        return copy(srcObj, new DenunciaTipo());
    }

    @Override
    public DenunciaTipo copy(Denuncia srcObj, DenunciaTipo destObj) {

        if (srcObj.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(srcObj, destObj);
        } else {
            destObj.setIdDenuncia(srcObj.getIdDenuncia().toString());
            destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
            destObj.setCodCanalDenuncia(srcObj.getCodCanaDenuncial() == null ? null : srcObj.getCodCanaDenuncial().getId());
            destObj.setCodCategoriaDenuncia(srcObj.getCodCategoriaDenuncia() == null ? null : srcObj.getCodCategoriaDenuncia().getId());
            destObj.setCodDecisionAnalisisPagos(srcObj.getCodDAPagos() == null ? null : srcObj.getCodDAPagos().getId());
            destObj.setCodTipoDenuncia(srcObj.getCodTipoDenuncia() == null ? null : srcObj.getCodTipoDenuncia().getId());
            destObj.setCodTipoEnvioComite(srcObj.getCodTipoEnvioComite() == null ? null : srcObj.getCodTipoEnvioComite().getId());
            destObj.setDenunciante(this.getMapperFacade().map(srcObj.getIdDenunciante(), DenuncianteTipo.class));
            destObj.setDescInformacionFaltante(srcObj.getDescInformacionFaltante());
            destObj.setDescObservaciones(srcObj.getDescObservaciones());
            destObj.setEsAprobadoCierre(this.getMapperFacade().map(srcObj.getEsAprobadoCierre(), String.class));
            destObj.setEsCasoCerrado(this.getMapperFacade().map(srcObj.getEsCasoCerrado(), String.class));
            destObj.setEsCompetenciaUGPP(this.getMapperFacade().map(srcObj.getEsCompetenciaUgPp(), String.class));
            destObj.setEsEnFiscalizacion(this.getMapperFacade().map(srcObj.getEsEnFiscalizacion(), String.class));
            destObj.setEsInformacionCompleta(this.getMapperFacade().map(srcObj.getEsInformacionCompleta(), String.class));
            destObj.setEsPago(this.getMapperFacade().map(srcObj.getEsPago(), String.class));
            destObj.setEsSuspenderTerminos(this.getMapperFacade().map(srcObj.getEsSuspenderTermino(), String.class));
            destObj.setEsTieneAnexos(this.getMapperFacade().map(srcObj.getEsTieneAnexos(), String.class));
            destObj.setEsTieneDerechoPeticion(this.getMapperFacade().map(srcObj.getEsTieneDerechoPeticion(), String.class));
            destObj.setEsPagosValidados(this.getMapperFacade().map(srcObj.getEsPagosValidados(), String.class));
            //destObj.setEsPagosValidados(this.getMapperFacade().map(srcObj.getEsPagosValidados(), String.class));
            destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
            destObj.setFecFinPeriodo(srcObj.getFecFinPeriodo());
            destObj.setFecInicioPeriodo(srcObj.getFecInicioPeriodo());
            destObj.setFecRadicadoDenuncia(DateUtil.parseCalendarToStrDateNoHour(srcObj.getFecRadicadoDenuncia()));
            destObj.setIdRadicadoDenuncia(srcObj.getIdRadicadoDenuncia());

            if (srcObj.getExpediente() != null
                    && (srcObj.getExpediente().getExpedienteDocumentoList() != null
                    && !srcObj.getExpediente().getExpedienteDocumentoList().isEmpty())) {

                for (ExpedienteDocumentoEcm expedienteDocumentoEcm
                        : srcObj.getExpediente().getExpedienteDocumentoList()) {
                    if (expedienteDocumentoEcm.getCodOrigen() != null) {
                        if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_CIERRE_CASO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            DocumentoEcm documentoEcm = expedienteDocumentoEcm.getDocumentoEcm();
                            destObj.setIdDocumentoCierreCaso(documentoEcm.getId());
                        } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_COMPLETITUD_INFO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            DocumentoEcm documentoEcm = expedienteDocumentoEcm.getDocumentoEcm();
                            destObj.setIdDocumentoCompletitudInformacion(documentoEcm.getId());
                        } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_PERSUASIVO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            DocumentoEcm documentoEcm = expedienteDocumentoEcm.getDocumentoEcm();
                            destObj.setIdDocumentoPersuasivo(documentoEcm.getId());
                        } else if (OrigenDocumentoEnum.DENUNCIA_ID_DOCUMENTO_SUSP_TERMINOS.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            DocumentoEcm documentoEcm = expedienteDocumentoEcm.getDocumentoEcm();
                            destObj.setIdDocumentoSuspensionTerminos(documentoEcm.getId());
                        }
                    }
                }
            }
        }
        return destObj;
    }

    private void mapUseSpecifiedFields(Denuncia srcObj, DenunciaTipo destObj) {

        destObj.setIdDenuncia(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.setDenunciante(this.getMapperFacade().map(srcObj.getIdDenunciante(), DenuncianteTipo.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
    }
}
