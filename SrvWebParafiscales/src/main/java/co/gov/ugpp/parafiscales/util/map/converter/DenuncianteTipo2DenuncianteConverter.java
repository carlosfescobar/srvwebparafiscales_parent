package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Denunciante;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;

/**
 *
 * @author jlsaenzar
 */
public class DenuncianteTipo2DenuncianteConverter extends AbstractBidirectionalConverter<DenuncianteTipo, Denunciante> {

    public DenuncianteTipo2DenuncianteConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Denunciante convertTo(DenuncianteTipo srcObj) {
        Denunciante denunciante = new Denunciante();
        this.copyTo(srcObj, denunciante);
        return denunciante;
    }

    @Override
    public void copyTo(DenuncianteTipo srcObj, Denunciante destObj) {
        this.getMapperFacade().map(srcObj, destObj.getPersona());
    }

    @Override
    public DenuncianteTipo convertFrom(Denunciante srcObj) {
        DenuncianteTipo denunciante = new DenuncianteTipo();
        this.copyFrom(srcObj, denunciante);
        return denunciante;
    }

    @Override
    public void copyFrom(Denunciante srcObj, DenuncianteTipo destObj) {

        final Persona persona = srcObj.getPersona();

        if (persona != null) {
            persona.setClasificacionPersona(ClasificacionPersonaEnum.DENUNCIANTE);

            final String tipoPersona = persona.getCodNaturalezaPersona() == null ? null : persona.getCodNaturalezaPersona().getId();
            if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
                destObj.setPersonaJuridica(this.getMapperFacade().map(persona, PersonaJuridicaTipo.class));
            } else {
                destObj.setPersonaNatural(this.getMapperFacade().map(persona, PersonaNaturalTipo.class));
            }
        }
        destObj.setCodTipoDenunciante(srcObj.getCodTipoDenunciante() == null ? null : srcObj.getCodTipoDenunciante().getId());
    }
}
