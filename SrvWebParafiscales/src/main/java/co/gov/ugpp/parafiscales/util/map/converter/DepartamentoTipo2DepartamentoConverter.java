package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.departamentotipo.v1.DepartamentoTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Departamento;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class DepartamentoTipo2DepartamentoConverter extends AbstractBidirectionalConverter<DepartamentoTipo, Departamento> {

    public DepartamentoTipo2DepartamentoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Departamento convertTo(DepartamentoTipo srcObj) {
        Departamento departamento = new Departamento();
        this.copyTo(srcObj, departamento);
        return departamento;
    }

    @Override
    public void copyTo(DepartamentoTipo srcObj, Departamento destObj) {
        destObj.setCodigo(srcObj.getCodDepartamento());
        destObj.setValNombre(srcObj.getValNombre());
    }

    @Override
    public DepartamentoTipo convertFrom(Departamento srcObj) {
        DepartamentoTipo departamentoTipo = new DepartamentoTipo();
        this.copyFrom(srcObj, departamentoTipo);
        return departamentoTipo;
    }

    @Override
    public void copyFrom(Departamento srcObj, DepartamentoTipo destObj) {
        destObj.setValNombre(srcObj.getValNombre());
        destObj.setCodDepartamento(srcObj.getCodigo().toString());
    }
}
