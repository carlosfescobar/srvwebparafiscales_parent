package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.DocumentacionEsperada;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class DocumentacionEsperada2DocumentacionEsperadaTipoConverter extends AbstractBidirectionalConverter<DocumentacionEsperadaTipo, DocumentacionEsperada> {

    public DocumentacionEsperada2DocumentacionEsperadaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public DocumentacionEsperada convertTo(DocumentacionEsperadaTipo srcObj) {
        DocumentacionEsperada documentacionEsperada = new DocumentacionEsperada();
        this.copyTo(srcObj, documentacionEsperada);
        return documentacionEsperada;
    }

    @Override
    public void copyTo(DocumentacionEsperadaTipo srcObj, DocumentacionEsperada destObj) {

        destObj.setFecEntradaDocumentos(srcObj.getFecEntradaDocumentos());
        destObj.setIdInstanciaProcesoReanudacion(srcObj.getIdInstanciaProcesoReanudacion());
        destObj.setIdRadicadoEntradaCorrespondencia(srcObj.getIdRadicadoEntradaCorrespondencia());
        destObj.setIdRadicadoSalidaCorrespond(srcObj.getIdRadicadoSalidaCorrespondencia());
        destObj.setValDescripcion(srcObj.getValDescripcion());
        destObj.setValNombreModeloProceso(srcObj.getValNombreModeloProceso());
    }

    @Override
    public DocumentacionEsperadaTipo convertFrom(DocumentacionEsperada srcObj) {
        DocumentacionEsperadaTipo documentacionEsperadaTipo = new DocumentacionEsperadaTipo();
        this.copyFrom(srcObj, documentacionEsperadaTipo);
        return documentacionEsperadaTipo;
    }

    @Override
    public void copyFrom(DocumentacionEsperada srcObj, DocumentacionEsperadaTipo destObj) {

        destObj.setIdInformacionEsperada(srcObj.getId().toString());
        destObj.setIdExpediente(srcObj.getExpediente() == null ? null : srcObj.getExpediente().getId());
        destObj.setCodEstado(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getId());
        destObj.setFecEntradaDocumentos(srcObj.getFecSalidaDocumentos());
        destObj.setIdInstanciaProcesoReanudacion(srcObj.getIdInstanciaProcesoReanudacion());
        destObj.setIdRadicadoEntradaCorrespondencia(srcObj.getIdRadicadoEntradaCorrespondencia());
        destObj.setIdRadicadoSalidaCorrespondencia(srcObj.getIdRadicadoSalidaCorrespond());
        destObj.setValDescripcion(srcObj.getValDescripcion());
        destObj.setValNombreModeloProceso(srcObj.getValNombreModeloProceso());

        if (StringUtils.isNotBlank(srcObj.getFuncionario())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getFuncionario());
            destObj.setFuncionario(funcionarioTipo);
        }
    }
}
