package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativoDocumentoAnexo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class DocumentoAnexoActoToStringConverter extends AbstractCustomConverter<ActoAdministrativoDocumentoAnexo, String> {

    public DocumentoAnexoActoToStringConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(ActoAdministrativoDocumentoAnexo srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(ActoAdministrativoDocumentoAnexo srcObj, String destObj) {
        destObj = srcObj.getDocumentoAnexo().getId();
        return destObj;
    }
}
