package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.MetadataDocumentoTipo;

/**
 *
 * @author rpadilla
 */
public class DocumentoTipo2DocumentoEcmConverter extends AbstractBidirectionalConverter<DocumentoTipo, DocumentoEcm> {

    public DocumentoTipo2DocumentoEcmConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public DocumentoEcm convertTo(DocumentoTipo srcObj) {
        DocumentoEcm documentoEcm = new DocumentoEcm();
        this.copyTo(srcObj, documentoEcm);
        return documentoEcm;
    }

    @Override
    public void copyTo(DocumentoTipo srcObj, DocumentoEcm destObj) {
        destObj.setId(srcObj.getIdDocumento());
        destObj.setNumeroRadicado(srcObj.getIdRadicadoCorrespondencia());
        destObj.setValNombre(srcObj.getValNombreDocumento());
        destObj.setValPaginas(srcObj.getValPaginas() == null ? null : srcObj.getValPaginas().longValue());
        destObj.setValAutoOriginador(srcObj.getValAutorOriginador());
        destObj.setValNaturalezaDocumento(srcObj.getValNaturalezaDocumento());
        destObj.setValOrigenDocumento(srcObj.getValOrigenDocumento());
        destObj.setDescObsLegibilidad(srcObj.getDescObservacionLegibilidad());
        destObj.setValNombreTipoDocumental(srcObj.getValNombreTipoDocumental());
        destObj.setNumFolios(srcObj.getNumFolios() == null ? null : srcObj.getNumFolios().longValue());
        destObj.setValLegible(srcObj.getValLegible());
        destObj.setValTipoFirma(srcObj.getValTipoFirma());
        destObj.setEsMover(this.getMapperFacade().map(srcObj.getEsMover(), Boolean.class));
        destObj.setFecDocumento(DateUtil.parseStrDateTimeToCalendar(srcObj.getFecDocumento()));
        destObj.setFecRadicado(DateUtil.parseStrDateTimeToCalendar(srcObj.getFecRadicacionCorrespondencia()));
    }

    @Override
    public DocumentoTipo convertFrom(DocumentoEcm srcObj) {
        DocumentoTipo documentoTipo = new DocumentoTipo();
        this.copyFrom(srcObj, documentoTipo);
        return documentoTipo;
    }

    @Override
    public void copyFrom(DocumentoEcm srcObj, DocumentoTipo destObj) {
//        TO-DO -> FALTA DE DEFINICION SI USAR ARCHIVOTIPO O DOCUMENTOTIPO
        destObj.setIdDocumento(srcObj.getId());
        destObj.setValNombreDocumento(srcObj.getValNombre());
        destObj.setFecDocumento(DateUtil.parseCalendarToStrDateTime(srcObj.getFecDocumento()));
        destObj.setCodTipoDocumento(srcObj.getCodTipoDocumento());
        destObj.setIdRadicadoCorrespondencia(srcObj.getNumeroRadicado());
        destObj.setFecRadicacionCorrespondencia(DateUtil.parseCalendarToStrDateTime((srcObj.getFecRadicado())));
        destObj.setValPaginas(srcObj.getValPaginas());
        destObj.setValAutorOriginador(srcObj.getValAutoOriginador());
        destObj.setValNaturalezaDocumento(srcObj.getValNaturalezaDocumento());
        destObj.setValOrigenDocumento(srcObj.getValOrigenDocumento());
        destObj.setDescObservacionLegibilidad(srcObj.getDescObsLegibilidad());
        destObj.setEsMover(this.getMapperFacade().map(srcObj.getEsMover(), String.class));
        destObj.setValNombreTipoDocumental(srcObj.getValNombreTipoDocumental());
        destObj.setNumFolios(srcObj.getNumFolios());
        destObj.setValLegible(srcObj.getValLegible());
        destObj.setValTipoFirma(srcObj.getValTipoFirma());
        destObj.setMetadataDocumento(this.getMapperFacade().map(srcObj.getMetadataDocumento(), MetadataDocumentoTipo.class));
    }
}
