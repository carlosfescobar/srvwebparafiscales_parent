package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.Converter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author jlsaenzar
 */
public class EntidadExternaTipo2EntidadExternaConverter extends AbstractBidirectionalConverter<EntidadExternaTipo, EntidadExterna> {

    private final Converter personaJuridicaTipo2PersonaConverter;

    public EntidadExternaTipo2EntidadExternaConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
        personaJuridicaTipo2PersonaConverter
                = this.getConverterFactory().getConverter(PersonaJuridicaTipo.class, Persona.class);
    }

    @Override
    public EntidadExterna convertTo(EntidadExternaTipo srcObj) {
        EntidadExterna entidadExterna = new EntidadExterna();
        this.copyTo(srcObj, entidadExterna);
        return entidadExterna;
    }

    @Override
    public void copyTo(EntidadExternaTipo srcObj, EntidadExterna destObj) {
        personaJuridicaTipo2PersonaConverter.copy(srcObj, destObj.getPersona());
        destObj.setEsOperadorPila(srcObj.getEsPila() == null ? null : Boolean.valueOf(srcObj.getEsPila()));
    }

    @Override
    public EntidadExternaTipo convertFrom(EntidadExterna srcObj) {
        EntidadExternaTipo entidadExternaTipo = new EntidadExternaTipo();
        this.copyFrom(srcObj, entidadExternaTipo);
        return entidadExternaTipo;
    }

    @Override
    public void copyFrom(EntidadExterna srcObj, EntidadExternaTipo destObj) {

        srcObj.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.ENTIDAD_EXTERNA);

        personaJuridicaTipo2PersonaConverter.copy(srcObj.getPersona(), destObj);
        destObj.setEsPila(srcObj.getEsOperadorPila() == null ? null : srcObj.getEsOperadorPila().toString());
        destObj.setCodTipoEntidadExterna(srcObj.getCodTipoEntidadExterna() == null ? null : srcObj.getCodTipoEntidadExterna().getId());
        destObj.setIdEntidadExterna(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodTipoProvedorInformacion(srcObj.getCodTipoProveedor() == null ? null : srcObj.getCodTipoProveedor().getId());
    }
}
