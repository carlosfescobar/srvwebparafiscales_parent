package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocioArchivoTemporal;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class EntidadNegocioArchivoTemporalToStringConverter extends AbstractCustomConverter<EntidadNegocioArchivoTemporal, String> {

    public EntidadNegocioArchivoTemporalToStringConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(EntidadNegocioArchivoTemporal srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(EntidadNegocioArchivoTemporal srcObj, String destObj) {
        return srcObj.getArchivoTemporal().getId().toString();
    }
}
