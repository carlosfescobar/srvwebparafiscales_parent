package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocio;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadNegocioArchivoTemporal;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.EntidadNegocioTipo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmuncab
 */
public class EntidadNegocioToEntidadNegocioTipoConverter extends AbstractCustomConverter<EntidadNegocio, EntidadNegocioTipo> {

    public EntidadNegocioToEntidadNegocioTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public EntidadNegocioTipo convert(EntidadNegocio srcObj) {
        return copy(srcObj, new EntidadNegocioTipo());
    }

    @Override
    public EntidadNegocioTipo copy(EntidadNegocio srcObj, EntidadNegocioTipo destObj) {

        destObj.setCodProceso(srcObj.getCodProceso() == null ? null : srcObj.getCodProceso().getId());
        destObj.setIdEntidadNegocio(srcObj.getIdEntidadNegocio());
        //Se obtienen los archivos de la entidad de negocio
        if (srcObj.getArchivosTemporales() != null && !srcObj.getArchivosTemporales().isEmpty()) {
            List<ParametroValoresTipo> archivosTemporalesEntidadNegocio = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> documentosAnexosActo = new HashMap<String, List<ParametroTipo>>();
            for (EntidadNegocioArchivoTemporal documentoAnexoActoCargado : srcObj.getArchivosTemporales()) {
                List<ParametroTipo> documentosAnexosCargados = documentosAnexosActo.get(documentoAnexoActoCargado.getCodTipoArchivoTemporal().getId());
                if (documentosAnexosCargados == null) {
                    documentosAnexosCargados = new ArrayList<ParametroTipo>();
                }
                ParametroTipo documentoAnexoActo = new ParametroTipo();
                documentoAnexoActo.setIdLlave(documentoAnexoActoCargado.getArchivoTemporal().getId().toString());
                documentoAnexoActo.setValValor(documentoAnexoActoCargado.getArchivoTemporal().getValNombreArchivo());
                documentosAnexosCargados.add(documentoAnexoActo);
                documentosAnexosActo.put(documentoAnexoActoCargado.getCodTipoArchivoTemporal().getId(), documentosAnexosCargados);
            }
            for (Map.Entry<String, List<ParametroTipo>> entry : documentosAnexosActo.entrySet()) {
                ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                documentoFinal.setIdLlave(entry.getKey());
                documentoFinal.getValValor().addAll(entry.getValue());
                archivosTemporalesEntidadNegocio.add(documentoFinal);
            }
            destObj.getIdArchivosTemporales().addAll(archivosTemporalesEntidadNegocio);
        }

        return destObj;
    }
}
