package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.EventoEnvio;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;

/**
 *
 * @author jmuncab
 */
public class EventoEnvioToEventoEnvioTipoConverter extends AbstractCustomConverter<EventoEnvio, EventoEnvioTipo> {

    public EventoEnvioToEventoEnvioTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public EventoEnvioTipo convert(EventoEnvio srcObj) {
        return copy(srcObj, new EventoEnvioTipo());
    }

    @Override
    public EventoEnvioTipo copy(EventoEnvio srcObj, EventoEnvioTipo destObj) {

        destObj.setIdEventoEnvio(srcObj.getId().toString());
        destObj.setCodCausalDevolucion(srcObj.getCodCausalDevolucion() == null ? null : srcObj.getCodCausalDevolucion().getId());
        destObj.setCodEstado(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getId());
        destObj.setDescCausalDevolucion(srcObj.getCodCausalDevolucion() == null ? null : srcObj.getCodCausalDevolucion().getNombre());
        destObj.setDescEstado(srcObj.getCodEstado() == null ? null : srcObj.getCodEstado().getNombre());
        destObj.setEsAprobadoDevolucion(this.getMapperFacade().map(srcObj.getEsAprobadoDevolucion(), String.class));
        destObj.setFecRadicadoSalida(srcObj.getFecRadicadoSalida());
        destObj.setIdRadicadoSalida(srcObj.getIdRadicadoSalida());
        return destObj;
    }
}
