package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;

/**
 *
 * @author rpadilla
 */
public class ExpedienteTipo2ExpedienteConverter extends AbstractBidirectionalConverter<ExpedienteTipo, Expediente> {

    public ExpedienteTipo2ExpedienteConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Expediente convertTo(ExpedienteTipo srcObj) {
        Expediente expediente = new Expediente();
        this.copyTo(srcObj, expediente);
        return expediente;
    }

    @Override
    public void copyTo(ExpedienteTipo srcObj, Expediente destObj) {
        destObj.setId(srcObj.getIdNumExpediente());
        destObj.setDescDescripcion(srcObj.getDescDescripcion());
        destObj.setIdCarpetaFileNet(srcObj.getIdCarpetaFileNet());
        destObj.setIdEntidadPredecesora(srcObj.getValEntidadPredecesora());
        destObj.setIdFondo(srcObj.getValFondo());
        destObj.setIdSeccion(srcObj.getCodSeccion());
        destObj.setIdSubseccion(srcObj.getCodSubSeccion());
        destObj.setSeccion(srcObj.getValSeccion());
        destObj.setSubSeccion(srcObj.getValSubSeccion());
        destObj.setAnoApertura(srcObj.getValAnoApertura());

        if (srcObj.getSerieDocumental() != null) {
            destObj.setNombreSerie(srcObj.getSerieDocumental().getValNombreSerie());
            destObj.setNombreSubSerie(srcObj.getSerieDocumental().getValNombreSubserie());
            destObj.setIdSerieDocuemental(srcObj.getSerieDocumental().getCodSerie());
            destObj.setIdSubserieDocumental(srcObj.getSerieDocumental().getCodSubserie());
            destObj.setIdTipoDocumental(srcObj.getSerieDocumental().getCodTipoDocumental());
        }
    }

    @Override
    public ExpedienteTipo convertFrom(Expediente srcObj) {
        ExpedienteTipo expedienteTipo = new ExpedienteTipo();
        this.copyFrom(srcObj, expedienteTipo);
        return expedienteTipo;
    }

    @Override
    public void copyFrom(Expediente srcObj, ExpedienteTipo destObj) {

        destObj.setIdNumExpediente(srcObj.getId());
        destObj.setIdentificacion(this.getMapperFacade().map(srcObj.getPersona(), IdentificacionTipo.class));
        destObj.setCodSeccion(srcObj.getIdSeccion() == null ? null : srcObj.getIdSeccion());
        destObj.setCodSubSeccion(srcObj.getIdSubseccion() == null ? null : srcObj.getIdSubseccion());
        destObj.setValSeccion(srcObj.getSeccion());
        destObj.setValSubSeccion(srcObj.getSubSeccion());
        SerieDocumentalTipo serieDocumentalTipo = new SerieDocumentalTipo();
        serieDocumentalTipo.setValNombreSerie(srcObj.getNombreSerie());
        serieDocumentalTipo.setValNombreSubserie(srcObj.getNombreSubSerie());
        serieDocumentalTipo.setCodSerie(srcObj.getIdSerieDocuemental() == null ? null : srcObj.getIdSerieDocuemental());
        serieDocumentalTipo.setCodSubserie(srcObj.getIdSubserieDocumental() == null ? null : srcObj.getIdSubserieDocumental());
        serieDocumentalTipo.setCodTipoDocumental(srcObj.getIdTipoDocumental() == null ? null : srcObj.getIdTipoDocumental());
        destObj.setSerieDocumental(serieDocumentalTipo);
        destObj.setValAnoApertura(srcObj.getAnoApertura());
        destObj.setValFondo(srcObj.getIdFondo() == null ? null : srcObj.getIdFondo());
        destObj.setValEntidadPredecesora(srcObj.getIdEntidadPredecesora() == null ? null : srcObj.getIdEntidadPredecesora());
        destObj.setDescDescripcion(srcObj.getDescDescripcion());
        destObj.setIdCarpetaFileNet(srcObj.getIdCarpetaFileNet() == null ? null : srcObj.getIdCarpetaFileNet());
    }

}
