package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author jmuncab
 */
public class FiscalizacionAccionToAccionTipoConverter extends AbstractCustomConverter<FiscalizacionAccion, AccionTipo> {

    public FiscalizacionAccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AccionTipo convert(FiscalizacionAccion srcObj) {
        return copy(srcObj, new AccionTipo());
    }

    @Override
    public AccionTipo copy(FiscalizacionAccion srcObj, AccionTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAccion(), AccionTipo.class);
        return destObj;
    }

}
