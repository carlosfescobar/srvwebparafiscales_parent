package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionBusqueda;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;

/**
 *
 * @author jmuncab
 */
public class FiscalizacionBusquedaToBusquedaTipoConverter extends AbstractCustomConverter<FiscalizacionBusqueda, BusquedaTipo> {

    public FiscalizacionBusquedaToBusquedaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public BusquedaTipo convert(FiscalizacionBusqueda srcObj) {
        return copy(srcObj, new BusquedaTipo());
    }

    @Override
    public BusquedaTipo copy(FiscalizacionBusqueda srcObj, BusquedaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getBusqueda(), BusquedaTipo.class);
        return destObj;
    }

}
