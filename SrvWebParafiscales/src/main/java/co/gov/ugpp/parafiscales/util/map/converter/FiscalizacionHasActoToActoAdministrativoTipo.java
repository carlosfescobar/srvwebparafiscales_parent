package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionActoAdministrativo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;

/**
 *
 * @author jmuncab
 */
public class FiscalizacionHasActoToActoAdministrativoTipo extends AbstractCustomConverter<FiscalizacionActoAdministrativo, ActoAdministrativoTipo> {

    public FiscalizacionHasActoToActoAdministrativoTipo(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ActoAdministrativoTipo convert(FiscalizacionActoAdministrativo srcObj) {
        return copy(srcObj, new ActoAdministrativoTipo());
    }

    @Override
    public ActoAdministrativoTipo copy(FiscalizacionActoAdministrativo srcObj, ActoAdministrativoTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getActoAdministrativo(), ActoAdministrativoTipo.class);
        return destObj;
    }
}
