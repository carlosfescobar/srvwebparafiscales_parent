package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionActoAdministrativo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1.AgrupacionFiscalizacionActoTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;

/**
 *
 * @author jmuncab
 */
public class FiscalizacionHasActoToAgrupacionFiscalizacionActoTipo extends AbstractCustomConverter<Fiscalizacion, AgrupacionFiscalizacionActoTipo> {

    public FiscalizacionHasActoToAgrupacionFiscalizacionActoTipo(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AgrupacionFiscalizacionActoTipo convert(Fiscalizacion srcObj) {
        return copy(srcObj, new AgrupacionFiscalizacionActoTipo());
    }

    @Override
    public AgrupacionFiscalizacionActoTipo copy(Fiscalizacion srcObj, AgrupacionFiscalizacionActoTipo destObj) {
        destObj.setFiscalizacion(this.getMapperFacade().map(srcObj, FiscalizacionTipo.class));
        destObj.getActosAdministrativos().addAll(this.getMapperFacade().map(srcObj.getActosAdministrativosFiscalizacion(), FiscalizacionActoAdministrativo.class, ActoAdministrativoTipo.class));
        return destObj;
    }
}
