package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Denuncia;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionAccion;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionArchivoTemp;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionBusqueda;
import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionIncumplimiento;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author zrodriguez
 */
public class FiscalizacionTipo2FiscalizacionConverter extends AbstractBidirectionalConverter<FiscalizacionTipo, Fiscalizacion> {

    public FiscalizacionTipo2FiscalizacionConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Fiscalizacion convertTo(FiscalizacionTipo srcObj) {
        Fiscalizacion fiscalizacion = new Fiscalizacion();
        this.copy(srcObj, fiscalizacion);
        return fiscalizacion;
    }

    @Override
    public void copyTo(FiscalizacionTipo srcObj, Fiscalizacion destObj) {

        destObj.setDenuncia(this.getMapperFacade().map(srcObj.getDenuncia(), Denuncia.class));
        destObj.setIdLiquidador(srcObj.getIdLiquidador());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), Expediente.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), Aportante.class));
        destObj.setRequerimientoDeclararCorregirParcial(this.getMapperFacade().map(srcObj.getRequerimientoDeclararCorregirParcial(), AprobacionDocumento.class));
        destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getAutoArchivoParcial(), AprobacionDocumento.class));
        destObj.setActoAutoArchivo(this.getMapperFacade().map(srcObj.getIdActoAutoArchivo(), ActoAdministrativo.class));
        destObj.setEsEnvioComite(this.getMapperFacade().map(srcObj.getEsEnvioComite(), Boolean.class));
        destObj.setEsMenorCuantia(this.getMapperFacade().map(srcObj.getEsMenorCuantia(), Boolean.class));
        destObj.setEsContinuadoProceso(this.getMapperFacade().map(srcObj.getEsContinuadoProceso(), Boolean.class));
        destObj.setEsOmisoPension(this.getMapperFacade().map(srcObj.getEsOmisoPension(), Boolean.class));
        destObj.getCodIncumplimiento().addAll(this.getMapperFacade().map(srcObj.getCodIncumplimiento(), String.class, FiscalizacionIncumplimiento.class));
        destObj.setActorequerimientoDeclararCorregir(this.getMapperFacade().map(srcObj.getIdActoRequerimientoDeclararCorregir(), ActoAdministrativo.class));
        destObj.getBusquedas().addAll(this.getMapperFacade().map(srcObj.getBusquedas(), BusquedaTipo.class, FiscalizacionBusqueda.class));
        destObj.setEsMasActuaciones(this.getMapperFacade().map(srcObj.getEsMasActuaciones(), Boolean.class));
        destObj.setCodMecanismoConseguirInformacion(this.getMapperFacade().map(srcObj.getCodMecanismoConseguirInformacion(), ValorDominio.class));
        destObj.setActoinspeccionTributaria(this.getMapperFacade().map(srcObj.getIdActoInspeccionTributaria(), ActoAdministrativo.class));
        destObj.setDescObservacionesContinuadoProceso(srcObj.getDescObservacionesContinuadoProceso());
        destObj.setDescObservacionesMasActuaciones(srcObj.getDescObservacionesMasActuaciones());
        destObj.setDescObservacionesMecanismoConseguir(srcObj.getDescObservacionesMecanismoConseguir());
        destObj.setFiscalizador(this.getMapperFacade().map(srcObj.getFiscalizador(), String.class));
        destObj.setCodLineaAccion(this.getMapperFacade().map(srcObj.getCodMecanismoConseguirInformacion(), ValorDominio.class));
        destObj.setValLineaAccion(srcObj.getValLineaAccion());
        destObj.setCodOrigenProceso(this.getMapperFacade().map(srcObj.getCodOrigenProceso(), ValorDominio.class));
        destObj.setValOrigenProceso(srcObj.getValOrigenProceso());
        destObj.setValNombreprograma(srcObj.getValNombrePrograma());
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), AccionTipo.class, FiscalizacionAccion.class));
        destObj.setFecRadicadoOficioInconsistencias(srcObj.getFecRadicadoOficioInconsistencias());
        destObj.setIdRadicadoOficioInconsistencias(srcObj.getIdRadicadoOficioInconsistencias());
    }

    @Override
    public FiscalizacionTipo convertFrom(Fiscalizacion srcObj) {
        FiscalizacionTipo fiscalizacionTipo = new FiscalizacionTipo();
        this.copyFrom(srcObj, fiscalizacionTipo);
        return fiscalizacionTipo;
    }

    @Override
    public void copyFrom(Fiscalizacion srcObj, FiscalizacionTipo destObj) {
        if (srcObj.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(srcObj, destObj);
        } else {
            destObj.setIdFiscalizacion(srcObj.getId() == null ? null : srcObj.getId().toString());
            destObj.setDenuncia(this.getMapperFacade().map(srcObj.getDenuncia(), DenunciaTipo.class));
            destObj.setIdLiquidador(srcObj.getIdLiquidador());
            destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
            destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
            destObj.setIdActoRequerimientoInformacion(srcObj.getActoRequerimientoInformacion() == null ? null : srcObj.getActoRequerimientoInformacion().getId());
            destObj.setRequerimientoDeclararCorregirParcial(this.getMapperFacade().map(srcObj.getRequerimientoDeclararCorregirParcial(), AprobacionDocumentoTipo.class));
            destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getAutoArchivoParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoAutoArchivo(srcObj.getActoAutoArchivo() == null ? null : srcObj.getActoAutoArchivo().getId());
            destObj.setEsEnvioComite(this.getMapperFacade().map(srcObj.getEsEnvioComite(), String.class));
            destObj.setEsMenorCuantia(this.getMapperFacade().map(srcObj.getEsMenorCuantia(), String.class));
            destObj.setEsContinuadoProceso(this.getMapperFacade().map(srcObj.getEsContinuadoProceso(), String.class));
            destObj.setEsOmisoPension(this.getMapperFacade().map(srcObj.getEsOmisoPension(), String.class));
            destObj.getCodIncumplimiento().addAll(this.getMapperFacade().map(srcObj.getCodIncumplimiento(), FiscalizacionIncumplimiento.class, String.class));
            destObj.setIdActoRequerimientoDeclararCorregir(srcObj.getActorequerimientoDeclararCorregir() == null ? null : srcObj.getActorequerimientoDeclararCorregir().getId());
            destObj.getBusquedas().addAll(this.getMapperFacade().map(srcObj.getBusquedas(), FiscalizacionBusqueda.class, BusquedaTipo.class));
            destObj.setEsMasActuaciones(this.getMapperFacade().map(srcObj.getEsMasActuaciones(), String.class));
            destObj.setCodMecanismoConseguirInformacion(this.getMapperFacade().map(srcObj.getCodMecanismoConseguirInformacion(), String.class));
            destObj.setIdActoInspeccionTributaria(srcObj.getActoinspeccionTributaria() == null ? null : srcObj.getActoinspeccionTributaria().getId());
            destObj.setDescObservacionesContinuadoProceso(srcObj.getDescObservacionesContinuadoProceso());
            destObj.setDescObservacionesMasActuaciones(srcObj.getDescObservacionesMasActuaciones());
            destObj.setDescObservacionesMecanismoConseguir(srcObj.getDescObservacionesMecanismoConseguir());
            destObj.setCodLineaAccion(this.getMapperFacade().map(srcObj.getCodLineaAccion(), String.class));
            destObj.setValLineaAccion(srcObj.getValLineaAccion());
            destObj.setCodOrigenProceso(this.getMapperFacade().map(srcObj.getCodOrigenProceso(), String.class));
            destObj.setValOrigenProceso(srcObj.getValOrigenProceso());
            destObj.setValNombrePrograma(srcObj.getValNombreprograma());
            destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), FiscalizacionAccion.class, AccionTipo.class));
            destObj.setCodEstadoFiscalizacion(srcObj.getCodEstadoFiscalizacion() == null ? null : srcObj.getCodEstadoFiscalizacion().getId());
            destObj.setDescEstadoFiscalizacion(srcObj.getCodEstadoFiscalizacion() == null ? null : srcObj.getCodEstadoFiscalizacion().getNombre());
            destObj.setDescMecanismoConseguirInformacion(srcObj.getCodMecanismoConseguirInformacion() == null ? null : srcObj.getCodMecanismoConseguirInformacion().getNombre());
            destObj.setIdDocumentoRequerimientoAclaraciones(srcObj.getIdDocumentoRequerimientoAclaraciones() == null ? null : srcObj.getIdDocumentoRequerimientoAclaraciones().getId());
            destObj.setFecRadicadoOficioInconsistencias(srcObj.getFecRadicadoOficioInconsistencias());
            destObj.setIdRadicadoOficioInconsistencias(srcObj.getIdRadicadoOficioInconsistencias());
            destObj.setIdDocumentoAutoComisorio(srcObj.getActoAutoComisorio() == null ? null : srcObj.getActoAutoComisorio().getId());
            destObj.setCodCausalCierre(srcObj.getCodCausalCierre() == null ? null : srcObj.getCodCausalCierre().getId());
            destObj.setDescGestionPersuasiva(srcObj.getDescGestionPersuasiva());

            RangoFechaTipo rangoFechaTipo = new RangoFechaTipo();
            rangoFechaTipo.setFecInicio(srcObj.getFecInicio());
            rangoFechaTipo.setFecFin(srcObj.getFecFin());
            destObj.setPeriodoFiscalizacion(rangoFechaTipo);

            if (StringUtils.isNotBlank(srcObj.getFiscalizador())) {
                FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
                funcionarioTipo.setIdFuncionario(srcObj.getFiscalizador());
                destObj.setFiscalizador(funcionarioTipo);
            }

            if (srcObj.getExpediente() != null
                    && srcObj.getExpediente().getExpedienteDocumentoList() != null
                    && !srcObj.getExpediente().getExpedienteDocumentoList().isEmpty()) {

                int cantidadDocumentosEncontrados = 0;

                for (ExpedienteDocumentoEcm expedienteDocumentoEcm : srcObj.getExpediente().getExpedienteDocumentoList()) {
                    if (expedienteDocumentoEcm.getCodOrigen() != null) {
                        if (OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_AFIL_CALC_ACTUA.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            destObj.setIdDocumentoAfiliacionCalculoActuarial(expedienteDocumentoEcm.getDocumentoEcm().getId());
                            cantidadDocumentosEncontrados++;
                        } else if (OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_CAL_ACTUAR.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            destObj.setIdDocumentoCalculoActuarial(expedienteDocumentoEcm.getDocumentoEcm().getId());
                            cantidadDocumentosEncontrados++;
                        } else if (OrigenDocumentoEnum.FISCALIZACION_ID_DOCUMENTO_HOJA_TRABAJO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                            destObj.setIdDocumentoHojaTrabajo(expedienteDocumentoEcm.getDocumentoEcm().getId());
                            cantidadDocumentosEncontrados++;
                        }
                    }
                    if (Constants.CANTIDAD_DOCUMENTOS_FISCALIZACION == cantidadDocumentosEncontrados) {
                        break;
                    }
                }
            }
            
            
            
                 if (srcObj.getArchivosTemporales()!= null && !srcObj.getArchivosTemporales().isEmpty()) {
            List<ParametroValoresTipo> anexosTemporales = new ArrayList<ParametroValoresTipo>();
            Map<String, List<ParametroTipo>> anexosTemporalesFiscalizacion= new HashMap<String, List<ParametroTipo>>();
            for (FiscalizacionArchivoTemp anexoTemporal : srcObj.getArchivosTemporales()) {
                List<ParametroTipo> archivosNotificacion = anexosTemporalesFiscalizacion.get(anexoTemporal.getCodTipoAnexo().getId());
                if (archivosNotificacion == null) {
                    archivosNotificacion = new ArrayList<ParametroTipo>();
                }
                ParametroTipo anexoNotificacion = new ParametroTipo();
                anexoNotificacion.setIdLlave(anexoTemporal.getArchivo().getId().toString());
                anexoNotificacion.setValValor(anexoTemporal.getArchivo().getValNombreArchivo());
                archivosNotificacion.add(anexoNotificacion);
                anexosTemporalesFiscalizacion.put(anexoTemporal.getCodTipoAnexo().getId(), archivosNotificacion);
            }
            for (Map.Entry<String, List<ParametroTipo>> entry : anexosTemporalesFiscalizacion.entrySet()) {
                ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                documentoFinal.setIdLlave(entry.getKey());
                documentoFinal.getValValor().addAll(entry.getValue());
                anexosTemporales.add(documentoFinal);
            }
            destObj.getArchivosTemporales().addAll(anexosTemporales);
        }
        }
    }

    private void mapUseSpecifiedFields(Fiscalizacion srcObj, FiscalizacionTipo destObj) {
        
        destObj.setIdFiscalizacion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getAutoArchivoParcial(), AprobacionDocumentoTipo.class));
        destObj.setRequerimientoDeclararCorregirParcial(this.getMapperFacade().map(srcObj.getRequerimientoDeclararCorregirParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoAutoArchivo(srcObj.getActoAutoArchivo() == null ? null : srcObj.getActoAutoArchivo().getId());
        destObj.setIdActoRequerimientoDeclararCorregir(srcObj.getActorequerimientoDeclararCorregir() == null ? null : srcObj.getActorequerimientoDeclararCorregir().getId());
    }
}
