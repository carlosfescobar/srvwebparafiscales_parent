package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FiscalizacionIncumplimiento;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class FiscalizacionToIncumplimientoTipoConverter extends AbstractCustomConverter<FiscalizacionIncumplimiento, String> {

    public FiscalizacionToIncumplimientoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(FiscalizacionIncumplimiento srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(FiscalizacionIncumplimiento srcObj, String destObj) {
        destObj = (srcObj.getCodIncumplimiento() == null ? null : srcObj.getCodIncumplimiento().getId());
        return destObj;
    }

}
