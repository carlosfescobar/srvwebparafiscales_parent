package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1.FormatoEstructuraAreaTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;

/**
 *
 * @author jmuncab
 */
public class FormatoAreaToFormatoAreaTipoConverter extends AbstractCustomConverter<Formato, FormatoEstructuraAreaTipo> {

    public FormatoAreaToFormatoAreaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoEstructuraAreaTipo convert(Formato srcObj) {
        return copy(srcObj, new FormatoEstructuraAreaTipo());
    }

    @Override
    public FormatoEstructuraAreaTipo copy(Formato srcObj, FormatoEstructuraAreaTipo destObj) {
        destObj.setCodAreaNegocio(srcObj.getCodAreaNegocio() == null ? null : srcObj.getCodAreaNegocio().getId());
        destObj.setDescAreaNegocio(srcObj.getCodAreaNegocio() == null ? null : srcObj.getCodAreaNegocio().getNombre());
        destObj.setFecInicioVigencia(srcObj.getFecInicioVigencia());
        destObj.setFecFinVigencia(srcObj.getFecFinVigencia());
        destObj.setValUsuario(srcObj.getValUsuario());
        destObj.setDescDescripcion(srcObj.getDescripcion());
        destObj.setFormato(this.getMapperFacade().map(srcObj, FormatoTipo.class));
        destObj.setArchivo(this.getMapperFacade().map(srcObj.getArchivo(), ArchivoTipo.class));
        return destObj;
    }
}
