package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoArchivo;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import java.math.BigDecimal;

/**
 *
 * @author jsaenzar
 */
public class FormatoEstructuraTipo2FormatoArchivoConverter extends AbstractBidirectionalConverter<FormatoEstructuraTipo, FormatoArchivo> {

    public FormatoEstructuraTipo2FormatoArchivoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoArchivo convertTo(FormatoEstructuraTipo srcObj) {
        FormatoArchivo formatoArchivo = new FormatoArchivo();
        this.copyTo(srcObj, formatoArchivo);
        return formatoArchivo;
    }

    @Override
    public void copyTo(FormatoEstructuraTipo srcObj, FormatoArchivo destObj) {
        destObj.setValContenidoArchvo(srcObj.getArchivo().getValContenidoArchivo());
    }

    @Override
    public FormatoEstructuraTipo convertFrom(FormatoArchivo srcObj) {
        FormatoEstructuraTipo formatoEstructuraTipo = new FormatoEstructuraTipo();
        this.copyFrom(srcObj, formatoEstructuraTipo);
        return formatoEstructuraTipo;
    }

    @Override
    public void copyFrom(FormatoArchivo srcObj, FormatoEstructuraTipo destObj) {
        FormatoTipo formatoTipo = new FormatoTipo();
        formatoTipo.setIdFormato(srcObj.getId().getIdFormato());
        formatoTipo.setValVersion(this.getMapperFacade().map(srcObj.getId().getIdVersion(), BigDecimal.class));
        destObj.setFormato(formatoTipo);
        ArchivoTipo archivoTipo = new ArchivoTipo();
//        archivoTipo.setIdArchivo(srcObj.getId().);
        archivoTipo.setValContenidoArchivo(srcObj.getValContenidoArchvo());
        destObj.setArchivo(archivoTipo);
        
    }

}
