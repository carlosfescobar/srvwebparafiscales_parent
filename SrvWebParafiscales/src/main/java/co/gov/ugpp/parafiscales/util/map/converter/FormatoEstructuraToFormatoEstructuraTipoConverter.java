package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FormatoEstructura;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;

/**
 *
 * @author jmuncab
 */
public class FormatoEstructuraToFormatoEstructuraTipoConverter extends AbstractCustomConverter<FormatoEstructura, FormatoEstructuraTipo> {

    public FormatoEstructuraToFormatoEstructuraTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoEstructuraTipo convert(FormatoEstructura srcObj) {
        return copy(srcObj, new FormatoEstructuraTipo());
    }

    @Override
    public FormatoEstructuraTipo copy(FormatoEstructura srcObj, FormatoEstructuraTipo destObj) {
        destObj.setArchivo(this.getMapperFacade().map(srcObj.getArchivo(), ArchivoTipo.class));
        destObj.setFormato(this.getMapperFacade().map(srcObj.getFormato(), FormatoTipo.class));
        return destObj;
    }
}
