package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.FormatoEstructura;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import java.math.BigDecimal;

/**
 *
 * @author jmuncab
 */
public class FormatoEstructuraToFormatoTipoConverter extends AbstractCustomConverter<FormatoEstructura, FormatoTipo> {

    public FormatoEstructuraToFormatoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoTipo convert(FormatoEstructura srcObj) {
        return copy(srcObj, new FormatoTipo());
    }

    @Override
    public FormatoTipo copy(FormatoEstructura srcObj, FormatoTipo destObj) {
        destObj.setIdFormato((srcObj.getFormato() == null || srcObj.getFormato().getId() == null)
                ? null : srcObj.getFormato().getId().getIdFormato());
        destObj.setValVersion((srcObj.getFormato() == null || srcObj.getFormato().getId() == null)
                ? null : BigDecimal.valueOf(srcObj.getFormato().getId().getIdVersion()));
        return destObj;
    }
}
