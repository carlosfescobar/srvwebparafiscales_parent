package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;

/**
 *
 * @author jmuncab
 */
public class FormatoToFormatoEstructuraTipoConverter extends AbstractCustomConverter<Formato, FormatoEstructuraTipo> {

    public FormatoToFormatoEstructuraTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoEstructuraTipo convert(Formato srcObj) {
        return copy(srcObj, new FormatoEstructuraTipo());
    }

    @Override
    public FormatoEstructuraTipo copy(Formato srcObj, FormatoEstructuraTipo destObj) {
        destObj.setArchivo(this.getMapperFacade().map(srcObj.getArchivo(), ArchivoTipo.class));
        destObj.setFormato(this.getMapperFacade().map(srcObj, FormatoTipo.class));
        return destObj;
    }
}
