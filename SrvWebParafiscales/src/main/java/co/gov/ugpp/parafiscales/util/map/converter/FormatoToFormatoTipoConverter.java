package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import java.math.BigDecimal;

/**
 *
 * @author jsaenzar
 */
public class FormatoToFormatoTipoConverter extends AbstractCustomConverter<Formato, FormatoTipo> {

    public FormatoToFormatoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public FormatoTipo convert(Formato srcObj) {
        return copy(srcObj, new FormatoTipo());
    }

    @Override
    public FormatoTipo copy(Formato srcObj, FormatoTipo destObj) {
        destObj.setIdFormato(srcObj.getId().getIdFormato());
        destObj.setValVersion(BigDecimal.valueOf(srcObj.getId().getIdVersion()));
        return destObj;
    }
}
