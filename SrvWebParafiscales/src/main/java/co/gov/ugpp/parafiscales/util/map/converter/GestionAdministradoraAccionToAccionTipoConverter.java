package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author zrodrigu
 */
public class GestionAdministradoraAccionToAccionTipoConverter extends AbstractCustomConverter<GestionAdministradoraAccion, AccionTipo> {

    public GestionAdministradoraAccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AccionTipo convert(GestionAdministradoraAccion srcObj) {
        return copy(srcObj, new AccionTipo());
    }

    @Override
    public AccionTipo copy(GestionAdministradoraAccion srcObj, AccionTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAccion(), AccionTipo.class);
        return destObj;
    }

}
