package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraControlUbicacion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;

/**
 *
 * @author zrodrigu
 */
public class GestionAdministradoraControlUbicacionToControlUbicacionTipoConverter extends AbstractCustomConverter<GestionAdministradoraControlUbicacion, ControlUbicacionEnvioTipo> {

    public GestionAdministradoraControlUbicacionToControlUbicacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ControlUbicacionEnvioTipo convert(GestionAdministradoraControlUbicacion srcObj) {
        return copy(srcObj, new ControlUbicacionEnvioTipo());
    }

    @Override
    public ControlUbicacionEnvioTipo copy(GestionAdministradoraControlUbicacion srcObj, ControlUbicacionEnvioTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getControlUbicacionEnvio(), ControlUbicacionEnvioTipo.class);
        return destObj;
    }

}
