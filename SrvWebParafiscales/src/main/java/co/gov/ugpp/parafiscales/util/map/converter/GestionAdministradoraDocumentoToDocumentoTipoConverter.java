package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraDocumento;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author zrodrigu
 */
public class GestionAdministradoraDocumentoToDocumentoTipoConverter extends AbstractCustomConverter<GestionAdministradoraDocumento, String> {

    public GestionAdministradoraDocumentoToDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(GestionAdministradoraDocumento srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(GestionAdministradoraDocumento srcObj, String destObj) {
        destObj = (srcObj.getExpedienteDocumentoEcm() == null ? null : srcObj.getExpedienteDocumentoEcm().getDocumentoEcm().getId());
        return destObj;
    }

}
