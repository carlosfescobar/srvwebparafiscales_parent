package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraAccion;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraControlUbicacion;
import co.gov.ugpp.parafiscales.persistence.entity.GestionAdministradoraDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author zrodriguez
 */
public class GestionAdministradoraTipo2GestionAdministradoraConverter extends AbstractBidirectionalConverter<GestionAdministradoraTipo, GestionAdministradora> {

    public GestionAdministradoraTipo2GestionAdministradoraConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public GestionAdministradora convertTo(GestionAdministradoraTipo srcObj) {
        GestionAdministradora gestionAdministradora = new GestionAdministradora();
        this.copy(srcObj, gestionAdministradora);
        return gestionAdministradora;
    }

    @Override
    public void copyTo(GestionAdministradoraTipo srcObj, GestionAdministradora destObj) {

        destObj.setSubsistema(this.getMapperFacade().map(srcObj.getSubsistema(), Subsistema.class));
        destObj.setAdministradora(this.getMapperFacade().map(srcObj.getAdministradora(), EntidadExterna.class));
        destObj.getControlUbicaciones().addAll(this.getMapperFacade().map(srcObj.getControlUbicaciones(), ControlUbicacionEnvioTipo.class, GestionAdministradoraControlUbicacion.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), Expediente.class));
        destObj.setCodTipoSolicitud(this.getMapperFacade().map(srcObj.getCodTipoSolicitud(), ValorDominio.class));
        destObj.setCodTipoRespuesta(this.getMapperFacade().map(srcObj.getCodTipoRespuesta(), ValorDominio.class));
        destObj.setCodTipoPeriodicidad(this.getMapperFacade().map(srcObj.getCodTipoPeriodicidad(), ValorDominio.class));
        destObj.setCodTipoPlazoEntrega(this.getMapperFacade().map(srcObj.getCodTipoPlazoEntrega(), ValorDominio.class));
        destObj.setValCantidadPlazoEntrega(srcObj.getValCantidadPlazoEntrega());
        destObj.setValNombreSolicitud(srcObj.getValNombreSolicitud());
        destObj.setDescInformacionRequerida(srcObj.getDescInformacionRequerida());
        destObj.setFecFinSolicitud(srcObj.getFecFinSolicitud());
        destObj.setFecInicioSolicitud(srcObj.getFecInicioSolicitud());
        destObj.setFecProximaEspera(srcObj.getFecProximaEspera());
        destObj.setCodEstadoGestion(this.getMapperFacade().map(srcObj.getCodEstadoGestion(), ValorDominio.class));
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), AccionTipo.class, GestionAdministradoraAccion.class));
    }

    @Override
    public GestionAdministradoraTipo convertFrom(GestionAdministradora srcObj) {
        GestionAdministradoraTipo gestionAdministradoraTipo = new GestionAdministradoraTipo();
        this.copyFrom(srcObj, gestionAdministradoraTipo);
        return gestionAdministradoraTipo;
    }

    @Override
    public void copyFrom(GestionAdministradora srcObj, GestionAdministradoraTipo destObj) {

        destObj.setIdGestionAdministradora(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setSubsistema(this.getMapperFacade().map(srcObj.getSubsistema(), SubsistemaTipo.class));
        destObj.setAdministradora(this.getMapperFacade().map(srcObj.getAdministradora(), EntidadExternaTipo.class));
        destObj.getControlUbicaciones().addAll(this.getMapperFacade().map(srcObj.getControlUbicaciones(), GestionAdministradoraControlUbicacion.class, ControlUbicacionEnvioTipo.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setCodTipoSolicitud(srcObj.getCodTipoSolicitud() == null ? null : srcObj.getCodTipoSolicitud().getId());
        destObj.setDescTipoSolicitud(srcObj.getCodTipoSolicitud() == null ? null : srcObj.getCodTipoSolicitud().getNombre());
        destObj.setCodTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getId());
        destObj.setDescTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getNombre());
        destObj.setCodTipoPeriodicidad(srcObj.getCodTipoPeriodicidad() == null ? null : srcObj.getCodTipoPeriodicidad().getId());
        destObj.setDescTipoPeriodicidad(srcObj.getCodTipoPeriodicidad() == null ? null : srcObj.getCodTipoPeriodicidad().getNombre());
        destObj.setCodTipoPlazoEntrega(srcObj.getCodTipoPlazoEntrega() == null ? null : srcObj.getCodTipoPlazoEntrega().getId());
        destObj.setDescTipoPlazoEntrega(srcObj.getCodTipoPlazoEntrega() == null ? null : srcObj.getCodTipoPlazoEntrega().getNombre());
        destObj.setValCantidadPlazoEntrega(srcObj.getValCantidadPlazoEntrega());
        destObj.setDescInformacionRequerida(srcObj.getDescInformacionRequerida());
        destObj.setFecInicioSolicitud(srcObj.getFecInicioSolicitud());
        destObj.setFecFinSolicitud(srcObj.getFecFinSolicitud());
        destObj.setFecProximaEspera(srcObj.getFecProximaEspera());
        destObj.setValNombreSolicitud(srcObj.getValNombreSolicitud());
        destObj.setCodEstadoGestion(srcObj.getCodEstadoGestion() == null ? null : srcObj.getCodEstadoGestion().getId());
        destObj.setDescEstadoGestion(srcObj.getCodEstadoGestion() == null ? null : srcObj.getCodEstadoGestion().getNombre());
        destObj.setIdFormatoElegido(srcObj.getFormatoElegido() == null ? null : srcObj.getFormatoElegido().getId().getIdFormato().toString());
        destObj.setIdVersionElegido(srcObj.getFormatoElegido() == null ? null : srcObj.getFormatoElegido().getId().getIdVersion().toString());
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), GestionAdministradoraAccion.class, AccionTipo.class));

        if (srcObj.getIdDocumentoSolicitud() != null
                && !srcObj.getIdDocumentoSolicitud().isEmpty()) {
            for (GestionAdministradoraDocumento gestionAdministradoraDocumento : srcObj.getIdDocumentoSolicitud()) {
                if (gestionAdministradoraDocumento.getExpedienteDocumentoEcm() != null
                        && gestionAdministradoraDocumento.getExpedienteDocumentoEcm().getDocumentoEcm() != null) {
                    destObj.getIdDocumentoSolicitud().add(gestionAdministradoraDocumento.getExpedienteDocumentoEcm().getDocumentoEcm().getId());
                }
            }
        }

        if (StringUtils.isNotBlank(srcObj.getIdFuncionarioSolicitud())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getIdFuncionarioSolicitud());
            destObj.setIdFuncionarioSolicitud(funcionarioTipo);
        }
    }

}
