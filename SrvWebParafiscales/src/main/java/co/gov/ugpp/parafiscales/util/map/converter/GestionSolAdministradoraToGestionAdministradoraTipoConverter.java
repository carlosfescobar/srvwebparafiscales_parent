package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.GestionSolAdministradora;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;

/**
 *
 * @author Everis
 */
public class GestionSolAdministradoraToGestionAdministradoraTipoConverter extends AbstractCustomConverter<GestionSolAdministradora, GestionAdministradoraTipo> {

    public GestionSolAdministradoraToGestionAdministradoraTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public GestionAdministradoraTipo convert(GestionSolAdministradora srcObj) {
        return copy(srcObj, new GestionAdministradoraTipo());
    }

    @Override
    public GestionAdministradoraTipo copy(GestionSolAdministradora srcObj, GestionAdministradoraTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getGestionAdministradora(), GestionAdministradoraTipo.class);
        return destObj;
    }

}
