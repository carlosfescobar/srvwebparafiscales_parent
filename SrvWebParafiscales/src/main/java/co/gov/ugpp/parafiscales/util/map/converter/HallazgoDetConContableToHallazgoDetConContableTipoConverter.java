package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.HallazgoDetalleConciliacionContable;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.hallazgodetalleconciliacioncontabletipo.v1.HallazgoDetalleConciliacionContableTipo;

/**
 *
 * @author jmuncab
 */
public class HallazgoDetConContableToHallazgoDetConContableTipoConverter extends AbstractCustomConverter<HallazgoDetalleConciliacionContable, HallazgoDetalleConciliacionContableTipo> {

    public HallazgoDetConContableToHallazgoDetConContableTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public HallazgoDetalleConciliacionContableTipo convert(HallazgoDetalleConciliacionContable srcObj) {
        return copy(srcObj, new HallazgoDetalleConciliacionContableTipo());
    }

    @Override
    public HallazgoDetalleConciliacionContableTipo copy(HallazgoDetalleConciliacionContable srcObj, HallazgoDetalleConciliacionContableTipo destObj) {

        destObj.setIdHallazgoConciliacionContableDetalle(srcObj.getId().toString());
        destObj.setConceptoContable(srcObj.getConceptoContable());
        destObj.setDescHallazgo(srcObj.getDescHallazgo());
        destObj.setDescObservacionHallazgo(srcObj.getObservacionHallazgo());
        destObj.setEsHallazgo(this.getMapperFacade().map(srcObj.getEsHallazgo(), String.class));
        destObj.setValContabilidad(srcObj.getValorContabilidad() == null ? null : srcObj.getValorContabilidad().toString());
        destObj.setValDiferenciaPesos(srcObj.getDiferenciaPesos() == null ? null : srcObj.getDiferenciaPesos().toString());
        destObj.setValDiferenciaPorcentaje(srcObj.getDiferenciaPorcentaje());
        destObj.setValFechaCreacion(DateUtil.parseCalendarToStrDateTime(srcObj.getFechaCreacion()));
        destObj.setValFechaModificacion(DateUtil.parseCalendarToStrDateTime(srcObj.getFechaModificacion()));
        destObj.setValNomina(srcObj.getValorNomina() == null ? null : srcObj.getValorNomina().toString());
        destObj.setValUsuarioCreacion(srcObj.getIdUsuarioCreacion());
        destObj.setValUsuarioModificacion(srcObj.getIdUsuarioModificacion());
        return destObj;
    }
}
