package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.persistence.entity.HallazgoDetalleNomina;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1.HallazgoDetalleNominaTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class HallazgoDetNominaToHallazgoDetNominaTipoTipoConverter extends AbstractCustomConverter<HallazgoDetalleNomina, HallazgoDetalleNominaTipo> {

    public HallazgoDetNominaToHallazgoDetNominaTipoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public HallazgoDetalleNominaTipo convert(HallazgoDetalleNomina srcObj) {
        return copy(srcObj, new HallazgoDetalleNominaTipo());
    }

    @Override
    public HallazgoDetalleNominaTipo copy(HallazgoDetalleNomina srcObj, HallazgoDetalleNominaTipo destObj) {

        destObj.setIdHallazgoNominaDetalle(srcObj.getId().toString());
        destObj.setCodTipoCotizante(srcObj.getTipoCotizante() == null ? null : srcObj.getTipoCotizante().toString());
        destObj.setDescHallazgo(srcObj.getDescHallazgo());
        destObj.setDescObservacionHallazgo(srcObj.getObservacionHallazgo());
        destObj.setEsHallazgo(this.getMapperFacade().map(srcObj.getEsHallazgo(), String.class));
        destObj.setValFechaCreacion(DateUtil.parseCalendarToStrDateTime(srcObj.getFechaCreacion()));
        destObj.setValFechaModificacion(DateUtil.parseCalendarToStrDateTime(srcObj.getFechaModificacion()));
        destObj.setValNombreCotizante(srcObj.getValNombreCotizante());
        destObj.setValPeriodoResto(srcObj.getPeriodoResto());
        destObj.setValUsuarioCreacion(srcObj.getIdUsuarioCreacion());
        destObj.setValUsuarioModificacion(srcObj.getIdUsuarioModificacion());

        if (StringUtils.isNotBlank(srcObj.getCodTipoIdentificacion())
                && StringUtils.isNotBlank(srcObj.getValNumeroIdentificacion())) {
            IdentificacionTipo identificacionTipo = new IdentificacionTipo();
            identificacionTipo.setCodTipoIdentificacion(srcObj.getCodTipoIdentificacion());
            identificacionTipo.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
            destObj.setIdCotizante(identificacionTipo);
        }
        return destObj;
    }
}
