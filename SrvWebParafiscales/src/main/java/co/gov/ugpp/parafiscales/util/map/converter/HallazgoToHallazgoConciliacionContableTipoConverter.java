package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;

/**
 *
 * @author jmuncab
 */
public class HallazgoToHallazgoConciliacionContableTipoConverter extends AbstractCustomConverter<Hallazgo, HallazgoConciliacionContableTipo> {

    public HallazgoToHallazgoConciliacionContableTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public HallazgoConciliacionContableTipo convert(Hallazgo srcObj) {
        return copy(srcObj, new HallazgoConciliacionContableTipo());
    }

    @Override
    public HallazgoConciliacionContableTipo copy(Hallazgo srcObj, HallazgoConciliacionContableTipo destObj) {
        destObj.setIdHallazgo(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setIdPila(srcObj.getIdPila() == null ? null : srcObj.getIdPila().toString());
        destObj.setIdNomina(srcObj.getIdNomina() == null ? null : srcObj.getIdNomina().toString());
        destObj.setIdConciliacionContable(srcObj.getIdConciliacion() == null ? null : srcObj.getIdConciliacion().toString());
        return destObj;
    }
}
