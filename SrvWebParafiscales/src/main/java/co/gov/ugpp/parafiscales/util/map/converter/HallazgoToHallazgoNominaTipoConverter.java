package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Hallazgo;
import co.gov.ugpp.parafiscales.persistence.entity.HallazgoDetalleNomina;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1.HallazgoDetalleNominaTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;

/**
 *
 * @author jmuncab
 */
public class HallazgoToHallazgoNominaTipoConverter extends AbstractCustomConverter<Hallazgo, HallazgoNominaTipo> {

    public HallazgoToHallazgoNominaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public HallazgoNominaTipo convert(Hallazgo srcObj) {
        return copy(srcObj, new HallazgoNominaTipo());
    }

    @Override
    public HallazgoNominaTipo copy(Hallazgo srcObj, HallazgoNominaTipo destObj) {

        destObj.setIdHallazgo(srcObj.getId().toString());
        destObj.setIdConciliacionContable(srcObj.getIdConciliacion() == null ? null : srcObj.getIdConciliacion().toString());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setIdNomina(srcObj.getIdNomina() == null ? null : srcObj.getIdNomina().toString());
        destObj.setIdPila(srcObj.getIdPila() == null ? null : srcObj.getIdPila().toString());
        destObj.getHallazgosDetalle().addAll(this.getMapperFacade().map(srcObj.getHallazgoDetalleNominaList(), HallazgoDetalleNomina.class, HallazgoDetalleNominaTipo.class));
        return destObj;
    }
}
