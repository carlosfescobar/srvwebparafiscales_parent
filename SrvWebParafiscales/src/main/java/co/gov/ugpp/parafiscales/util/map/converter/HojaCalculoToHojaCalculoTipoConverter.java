package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.HojaCalculoLiquidacion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;

/**
 *
 * @author jmuncab
 */
public class HojaCalculoToHojaCalculoTipoConverter extends AbstractCustomConverter<HojaCalculoLiquidacion, HojaCalculoTipo> {

    public HojaCalculoToHojaCalculoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public HojaCalculoTipo convert(HojaCalculoLiquidacion srcObj) {
        return copy(srcObj, new HojaCalculoTipo());
    }

    @Override
    public HojaCalculoTipo copy(HojaCalculoLiquidacion srcObj, HojaCalculoTipo destObj) {
        destObj.setIdHojaCalculoLiquidacion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setDesEstado(srcObj.getDesestado());
        destObj.setCodEstado(srcObj.getCodesstado());
        destObj.setEsIncumplimiento(this.getMapperFacade().map(srcObj.getEsincumplimiento(), Boolean.class));
        return destObj;
    }
}
