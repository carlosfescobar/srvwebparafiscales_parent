package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class IdentificacionTipo2IdentificacionConverter extends AbstractBidirectionalConverter<IdentificacionTipo, Identificacion> {

    public IdentificacionTipo2IdentificacionConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Identificacion convertTo(IdentificacionTipo srcObj) {
        Identificacion identificacion = new Identificacion();
        this.copyTo(srcObj, identificacion);
        return identificacion;
    }

    @Override
    public void copyTo(IdentificacionTipo srcObj, Identificacion destObj) {
        destObj.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
        destObj.setCodTipoIdentificacion(srcObj.getCodTipoIdentificacion());
    }

    @Override
    public IdentificacionTipo convertFrom(Identificacion srcObj) {
        IdentificacionTipo identificacionTipo = new IdentificacionTipo();
        this.copyFrom(srcObj, identificacionTipo);
        return identificacionTipo;
    }

    @Override
    public void copyFrom(Identificacion srcObj, IdentificacionTipo destObj) {
        destObj.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
        destObj.setCodTipoIdentificacion(srcObj.getCodTipoIdentificacion());
        destObj.setMunicipioExpedicion(this.getMapperFacade().map(srcObj.getMunicipio(), MunicipioTipo.class));
    }

}
