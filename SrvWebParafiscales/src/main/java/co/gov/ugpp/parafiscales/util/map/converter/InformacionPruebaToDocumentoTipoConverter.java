package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.InformacionPruebaDocumentos;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class InformacionPruebaToDocumentoTipoConverter extends AbstractCustomConverter<InformacionPruebaDocumentos, String> {

    public InformacionPruebaToDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }


    @Override
    public String convert(InformacionPruebaDocumentos srcObj) {
     return copy(srcObj, new String());
    }

    @Override
    public String copy(InformacionPruebaDocumentos srcObj, String destObj) {
        destObj = (srcObj.getId() == null ? null : srcObj.getId().toString());
        return destObj;
    }

}
