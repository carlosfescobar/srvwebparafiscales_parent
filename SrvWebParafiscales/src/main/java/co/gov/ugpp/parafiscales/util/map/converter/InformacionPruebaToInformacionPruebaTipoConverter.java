package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.InformacionPrueba;
import co.gov.ugpp.parafiscales.persistence.entity.InformacionPruebaDocumentos;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class InformacionPruebaToInformacionPruebaTipoConverter extends AbstractCustomConverter<InformacionPrueba, InformacionPruebaTipo> {

    public InformacionPruebaToInformacionPruebaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public InformacionPruebaTipo convert(InformacionPrueba srcObj) {
        return copy(srcObj, new InformacionPruebaTipo());
    }

    @Override
    public InformacionPruebaTipo copy(InformacionPrueba srcObj, InformacionPruebaTipo destObj) {

        destObj.setIdInformacionPrueba(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setEsRequiereInformacion(this.getMapperFacade().map(srcObj.getEsRequerimientoInformacion(), String.class));
        destObj.setCodTipoRequerimiento(this.getMapperFacade().map(srcObj.getCodTipoRequerimiento(), String.class));
        destObj.setValAreaSeleccionada(srcObj.getValAreaSeleccionada());
        destObj.setDesInformacionRequerida(srcObj.getDesInformacionRequerida());
        destObj.setCodPersonaSolicitudExterna(this.getMapperFacade().map(srcObj.getCodPersonaSolicitudExterna(), String.class));
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class));
        destObj.getIdDocumentosRespuestaInterna().addAll(this.getMapperFacade().map(srcObj.getIdDocumentosRespuestaInterna(),InformacionPruebaDocumentos.class,String.class));
        destObj.setDesRespuestaInformacion(srcObj.getDesRespuestaInformacion());
        destObj.setEsInformacionExternaCompleta(this.getMapperFacade().map(srcObj.getEsInformacionExternaCompleta(), String.class));
        destObj.setRequerimientoInformacionParcial(this.getMapperFacade().map(srcObj.getRequerimientoInformacionParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoRequerimientoInformacion(srcObj.getIdActoRequerimientoInformacion()== null? null: srcObj.getIdActoRequerimientoInformacion().getId());

        if (StringUtils.isNotBlank(srcObj.getFuncionarioResuelveInterna())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getFuncionarioResuelveInterna());
            destObj.setFuncionarioResuelveInterna(funcionarioTipo);
        }

        return destObj;
    }
}
