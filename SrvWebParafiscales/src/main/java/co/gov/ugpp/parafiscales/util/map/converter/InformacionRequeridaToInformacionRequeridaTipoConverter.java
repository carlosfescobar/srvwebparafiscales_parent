package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SancionInformacionRequerida;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;

/**
 *
 * @author jmuncab
 */
public class InformacionRequeridaToInformacionRequeridaTipoConverter extends AbstractCustomConverter<SancionInformacionRequerida, InformacionPruebaTipo> {

    public InformacionRequeridaToInformacionRequeridaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public InformacionPruebaTipo convert(SancionInformacionRequerida srcObj) {
        return copy(srcObj, new InformacionPruebaTipo());
    }

    @Override
    public InformacionPruebaTipo copy(SancionInformacionRequerida srcObj, InformacionPruebaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getInformacionPrueba(), InformacionPruebaTipo.class);
        return destObj;
    }

  

}
