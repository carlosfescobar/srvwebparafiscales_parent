package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Investigado;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sanciones.investigadotipo.v1.InvestigadoTipo;

/**
 *
 * @author jlsaenzar
 */
public class InvestigadoTipo2InvestigadoConverter extends AbstractBidirectionalConverter<InvestigadoTipo, Investigado> {

    public InvestigadoTipo2InvestigadoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Investigado convertTo(InvestigadoTipo srcObj) {
        Investigado investigado = new Investigado();
        this.copyTo(srcObj, investigado);
        return investigado;
    }

    @Override
    public void copyTo(InvestigadoTipo srcObj, Investigado destObj) {
        this.getMapperFacade().map(srcObj, destObj.getPersona());
    }

    @Override
    public InvestigadoTipo convertFrom(Investigado srcObj) {
        InvestigadoTipo investigadoTipo = new InvestigadoTipo();
        this.copyFrom(srcObj, investigadoTipo);
        return investigadoTipo;
    }

    @Override
    public void copyFrom(Investigado srcObj, InvestigadoTipo destObj) {

        final Persona persona = srcObj.getPersona();
        final String tipoPersona = persona.getCodNaturalezaPersona() == null ? null : persona.getCodNaturalezaPersona().getId();
        
        persona.setClasificacionPersona(ClasificacionPersonaEnum.INVESTIGADO);

        if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
            destObj.setPersonaJuridica(this.getMapperFacade().map(persona, PersonaJuridicaTipo.class));
        } else {
            destObj.setPersonaNatural(this.getMapperFacade().map(persona, PersonaNaturalTipo.class));
        }
        destObj.setCodTipoInvestigado(srcObj.getCodTipoInvestigado() == null ? null : srcObj.getCodTipoInvestigado().getId());
    }

}
