package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author gchavezm
 */
public class LiquidacionAccionToAccionTipoConverter extends AbstractCustomConverter<LiquidacionAccion, AccionTipo> {

    public LiquidacionAccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AccionTipo convert(LiquidacionAccion srcObj) {
        return copy(srcObj, new AccionTipo());
    }

    @Override
    public AccionTipo copy(LiquidacionAccion srcObj, AccionTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAccion(), AccionTipo.class);
        return destObj;
    }

}
