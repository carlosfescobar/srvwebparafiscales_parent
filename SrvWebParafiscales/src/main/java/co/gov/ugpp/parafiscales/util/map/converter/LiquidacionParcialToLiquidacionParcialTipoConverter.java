package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionParcial;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;

/**
 *
 * @author jmuncab
 */
public class LiquidacionParcialToLiquidacionParcialTipoConverter extends AbstractCustomConverter<LiquidacionParcial, LiquidacionParcialTipo> {

    public LiquidacionParcialToLiquidacionParcialTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public LiquidacionParcialTipo convert(LiquidacionParcial srcObj) {
        return copy(srcObj, new LiquidacionParcialTipo());
    }

    @Override
    public LiquidacionParcialTipo copy(LiquidacionParcial srcObj, LiquidacionParcialTipo destObj) {

        destObj.setIdLiquidacionParcial(srcObj.getId()== null ?  null: srcObj.getId().toString());
        destObj.setCodAccionLiquidacionParcial(this.getMapperFacade().map(srcObj.getCodAccionLiquidacionParcial(),String.class));
        destObj.setUvt(this.getMapperFacade().map(srcObj.getUvt(),UVTTipo.class));
        destObj.setDesObservaciones(srcObj.getDesObservaciones());
        
        return destObj;

    }

}
