package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.Liquidacion;
import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionAccion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1.LiquidacionTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author gchavezm
 */
public class LiquidacionTipo2LiquidacionTipoConverter extends AbstractBidirectionalConverter<LiquidacionTipo, Liquidacion> {

    public LiquidacionTipo2LiquidacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Liquidacion convertTo(LiquidacionTipo srcObj) {
        Liquidacion liquidacion = new Liquidacion();
        this.copyTo(srcObj, liquidacion);
        return liquidacion;
    }

    @Override
    public void copyTo(LiquidacionTipo srcObj, Liquidacion destObj) {

        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), Expediente.class));
        destObj.setLiquidacionOficialParcial(this.getMapperFacade().map(srcObj.getLiquidacionOficialParcial(), AprobacionDocumento.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), Aportante.class));
        destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getIdActoAutoArchivo(), AprobacionDocumento.class));
        destObj.setEsAmpliadoRequerimiento(this.getMapperFacade().map(srcObj.getEsAmpliadoRequerimiento(), Boolean.class));
        destObj.setEsDeterminaDeuda(this.getMapperFacade().map(srcObj.getEsDeterminaDeuda(), Boolean.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), Aportante.class));
        destObj.setEsConfirmadaDeuda(this.getMapperFacade().map(srcObj.getEsConfirmadaDeuda(), Boolean.class));
        destObj.setEsRecibidoRecursoReconsideracion(this.getMapperFacade().map(srcObj.getEsRecibidoRecursoReconsideracion(), Boolean.class));
        destObj.setEsTodosEmpleadosAfiliados(this.getMapperFacade().map(srcObj.getEsTodosEmpleadosAfiliados(), Boolean.class));
        destObj.setDescObservacionesAmpliacionRequerimiento(this.getMapperFacade().map(srcObj.getDescObservacionesAmpliacionRequerimiento(), String.class));
        destObj.setCodEstadoEnvioMemorandoCobro(this.getMapperFacade().map(srcObj.getCodEstadoEnvioMemorandoCobro(), ValorDominio.class));
        destObj.setValSubdirectorCobranzas(srcObj.getSubdirectorCobranzas());
        destObj.setDescObservacionesAfiliaciones(srcObj.getDescObservacionesAfiliaciones());
        destObj.setCodEstadoLiquidacion(this.getMapperFacade().map(srcObj.getCodEstadoLiquidacion(), ValorDominio.class));
        destObj.setidFiscalizacion(this.getMapperFacade().map(srcObj.getIdFiscalizacion(), Fiscalizacion.class));
        FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
        funcionarioTipo.setIdFuncionario(this.getMapperFacade().map(srcObj.getSubdirectorCobranzas(), String.class));

        destObj.setValSubdirectorCobranzas(funcionarioTipo);
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), AccionTipo.class, LiquidacionAccion.class));

    }

    @Override
    public LiquidacionTipo convertFrom(Liquidacion srcObj) {
        LiquidacionTipo liquidacionTipo = new LiquidacionTipo();
        this.copyFrom(srcObj, liquidacionTipo);
        return liquidacionTipo;
    }

    @Override
    public void copyFrom(Liquidacion srcObj, LiquidacionTipo destObj) {
        if (srcObj.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(srcObj, destObj);
        } else {
            destObj.setIdLiquidacion(srcObj.getId() == null ? null : srcObj.getId().toString());
            destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
            destObj.setIdFiscalizacion(srcObj.getidFiscalizacion() == null ? null : srcObj.getidFiscalizacion().getId().toString());
            destObj.setLiquidacionOficialParcial(this.getMapperFacade().map(srcObj.getLiquidacionOficialParcial(), AprobacionDocumentoTipo.class));
            destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getAutoArchivoParcial(), AprobacionDocumentoTipo.class));
            destObj.setEsAmpliadoRequerimiento(this.getMapperFacade().map(srcObj.getEsAmpliadoRequerimiento(), String.class));
            destObj.setEsDeterminaDeuda(this.getMapperFacade().map(srcObj.getEsDeterminaDeuda(), String.class));
            destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
            destObj.setIdDocumentoConstanciaEjecutoria(srcObj.getidDocumentoConstanciaEjecutoria() == null ? null : srcObj.getidDocumentoConstanciaEjecutoria().getId().toString());
            destObj.setEsConfirmadaDeuda(this.getMapperFacade().map(srcObj.getEsConfirmadaDeuda(), String.class));
            destObj.setEsRecibidoRecursoReconsideracion(this.getMapperFacade().map(srcObj.getEsRecibidoRecursoReconsideracion(), String.class));
            destObj.setIdActoAutoArchivo(srcObj.getidActoAutoArchivo() == null ? null : srcObj.getidActoAutoArchivo().getId());
            destObj.setIdActoLiquidacionOficial(srcObj.getidActoLiquidacionOficial() == null ? null : srcObj.getidActoLiquidacionOficial().getId());
            destObj.setIdActoRequerimientoDeclararCorregirAmpliado(srcObj.getIdActoRequerimientoDeclararCorregirAmpliado() == null ? null : srcObj.getIdActoRequerimientoDeclararCorregirAmpliado().getId());
            destObj.setEsTodosEmpleadosAfiliados(this.getMapperFacade().map(srcObj.getEsTodosEmpleadosAfiliados(), String.class));
            destObj.setDescObservacionesAmpliacionRequerimiento(srcObj.getDescObservacionesAmpliacionRequerimiento());
            destObj.setIdLiquidadorLiquidacion(srcObj.getidLiquidadorLiquidacion() == null ? null : srcObj.getidLiquidadorLiquidacion().getId().toString());
            destObj.setIdLiquidadorLiquidacionAmpliada(srcObj.getidLiquidadorLiquidacionAmpliada() == null ? null : srcObj.getidLiquidadorLiquidacionAmpliada().getId().toString());
            destObj.setCodEstadoEnvioMemorandoCobro(this.getMapperFacade().map(srcObj.getCodEstadoEnvioMemorandoCobro(), String.class));
            destObj.setDescObservacionesAfiliaciones(srcObj.getDescObservacionesAfiliaciones());
            destObj.getAgrupacionAfiliacionesPorEntidad().addAll(this.getMapperFacade().map(srcObj.getAgrupacionAfiliacionesEntidad(), AgrupacionAfiliacionesPorEntidad.class, AgrupacionAfiliacionesPorEntidadTipo.class));
            destObj.setCodEstadoLiquidacion(this.getMapperFacade().map(srcObj.getCodEstadoLiquidacion(), String.class));
            destObj.setDescEstadoLiquidacion(srcObj.getCodEstadoLiquidacion() == null ? null : srcObj.getCodEstadoLiquidacion().getDescValorDominio());
            destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), LiquidacionAccion.class, AccionTipo.class));
            destObj.setRequerimientoDeclararAmpliadoParcial(this.getMapperFacade().map(srcObj.getRequerimientoDeclararAmpliadoParcial(), AprobacionDocumentoTipo.class));
            destObj.setCodCausalCierre(srcObj.getCodCausalCierre() == null ? null : srcObj.getCodCausalCierre().getId());
            destObj.setValUsuarioGeneraActo(srcObj.getValUsuarioGeneraActo());
            
            RangoFechaTipo rangoFechaTipo = new RangoFechaTipo();
            rangoFechaTipo.setFecInicio(srcObj.getFecInicio());
            rangoFechaTipo.setFecFin(srcObj.getFecFin());
            destObj.setPeriodoFiscalizacion(rangoFechaTipo);

            if (StringUtils.isNotBlank(srcObj.getLiquidador())) {
                FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
                funcionarioTipo.setIdFuncionario(srcObj.getLiquidador());
                destObj.setLiquidador(funcionarioTipo);
            }

            if (StringUtils.isNotBlank(srcObj.getSubdirectorCobranzas())) {
                FuncionarioTipo subDirectorCobranza = new FuncionarioTipo();
                subDirectorCobranza.setIdFuncionario(srcObj.getSubdirectorCobranzas());
                destObj.setSubdirectorCobranzas(subDirectorCobranza);
            }
        }
    }

    private void mapUseSpecifiedFields(Liquidacion srcObj, LiquidacionTipo destObj) {

        destObj.setIdLiquidacion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.setIdActoAutoArchivo(srcObj.getidActoAutoArchivo() == null ? null : srcObj.getidActoAutoArchivo().getId());

    }
}
