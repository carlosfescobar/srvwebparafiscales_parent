package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocAgrupador;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class MetadataDocAgrupadorToStringConverter extends AbstractCustomConverter<MetadataDocAgrupador, String> {

    public MetadataDocAgrupadorToStringConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(MetadataDocAgrupador srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(MetadataDocAgrupador srcObj, String destObj) {

        destObj = srcObj.getValAgrupador();
        return destObj;
    }
}
