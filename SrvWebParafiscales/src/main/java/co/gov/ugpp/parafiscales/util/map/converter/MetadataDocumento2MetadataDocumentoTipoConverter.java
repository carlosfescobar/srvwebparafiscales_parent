package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocAgrupador;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.SerieDocumental;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.MetadataDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;

/**
 *
 * @author jmuncab
 */
public class MetadataDocumento2MetadataDocumentoTipoConverter extends AbstractBidirectionalConverter<MetadataDocumentoTipo, MetadataDocumento> {

    public MetadataDocumento2MetadataDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public MetadataDocumento convertTo(MetadataDocumentoTipo srcObj) {
        MetadataDocumento metadataDocumento = new MetadataDocumento();
        this.copyTo(srcObj, metadataDocumento);
        return metadataDocumento;
    }

    @Override
    public void copyTo(MetadataDocumentoTipo srcObj, MetadataDocumento destObj) {
        destObj.setSerieDocumental(this.getMapperFacade().map(srcObj.getSerieDocumental(), SerieDocumental.class));
    }

    @Override
    public MetadataDocumentoTipo convertFrom(MetadataDocumento srcObj) {
        MetadataDocumentoTipo metadataDocumentoTipo = new MetadataDocumentoTipo();
        this.copyFrom(srcObj, metadataDocumentoTipo);
        return metadataDocumentoTipo;
    }

    @Override
    public void copyFrom(MetadataDocumento srcObj, MetadataDocumentoTipo destObj) {
        destObj.setSerieDocumental(this.getMapperFacade().map(srcObj.getSerieDocumental(), SerieDocumentalTipo.class));
        destObj.getValAgrupador().addAll(this.getMapperFacade().map(srcObj.getMetadataDocAgrupadorList(), MetadataDocAgrupador.class, String.class));
    }
}
