package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.MigracionCasoParafiscales;
import co.gov.ugpp.parafiscales.persistence.entity.MigracionParafiscales;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;
import co.gov.ugpp.schema.transversales.migraciontipo.v1.MigracionTipo;

/**
 *
 * @author jmuncab
 */
public class MigracionToMigracionTipoConverter extends AbstractCustomConverter<MigracionParafiscales, MigracionTipo> {

    public MigracionToMigracionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public MigracionTipo convert(MigracionParafiscales srcObj) {
        return copy(srcObj, new MigracionTipo());
    }

    @Override
    public MigracionTipo copy(MigracionParafiscales srcObj, MigracionTipo destObj) {
        destObj.setIdMigracion(srcObj.getId().toString());
        destObj.setCodEscenarioMigrado(srcObj.getCodCasoMigrado() == null ? null : srcObj.getCodCasoMigrado().getId());
        destObj.setDescEscenarioMigrado(srcObj.getCodCasoMigrado() == null ? null : srcObj.getCodCasoMigrado().getNombre());
        destObj.setFecMigracion(srcObj.getFecMigracion());
        destObj.setIdUsuario(srcObj.getIdUsuarioResponsable());
        destObj.getCasosMigrados().addAll(this.getMapperFacade().map(srcObj.getCasosMigrados(), MigracionCasoParafiscales.class, MigracionCasoTipo.class));
        return destObj;
    }
}
