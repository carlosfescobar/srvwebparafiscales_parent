package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.departamentotipo.v1.DepartamentoTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Departamento;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class MunicipioTipo2MunicipioConverter extends AbstractBidirectionalConverter<MunicipioTipo, Municipio> {

    public MunicipioTipo2MunicipioConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Municipio convertTo(MunicipioTipo srcObj) {
        Municipio municipio = new Municipio();
        this.copyTo(srcObj, municipio);
        return municipio;
    }

    @Override
    public void copyTo(MunicipioTipo srcObj, Municipio destObj) {
        destObj.setValNombre(srcObj.getValNombre());
        destObj.setDepartamento(this.getMapperFacade().map(srcObj.getDepartamento(), Departamento.class));
        destObj.setCodigo(srcObj.getCodMunicipio() == null ? null : String .valueOf(srcObj.getCodMunicipio()));
    }

    @Override
    public MunicipioTipo convertFrom(Municipio srcObj) {
        MunicipioTipo municipioTipo = new MunicipioTipo();
        this.copyFrom(srcObj, municipioTipo);
        return municipioTipo;
    }

    @Override
    public void copyFrom(Municipio srcObj, MunicipioTipo destObj) {
        destObj.setValNombre(srcObj.getValNombre());
        destObj.setDepartamento(this.getMapperFacade().map(srcObj.getDepartamento(), DepartamentoTipo.class));
        destObj.setCodMunicipio(String.valueOf(srcObj.getCodigo()));
    }
}
