//package co.gov.ugpp.parafiscales.util.map.converter;
//
//import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
//import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
//import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
//import co.gov.ugpp.parafiscales.persistence.entity.NoAportante;
//import co.gov.ugpp.parafiscales.persistence.entity.Persona;
//import co.gov.ugpp.parafiscales.util.map.Converter;
//import co.gov.ugpp.parafiscales.util.map.MapperFactory;
//import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
//
///**
// *
// * @author jlsaenzar
// */
//public class NoAportanteTipo2NoAportanteConverter extends AbstractBidirectionalConverter<NoAportanteTipo, NoAportante> {
//
//    private final Converter personaNaturalTipo2PersonaConverter; 
//
//    public NoAportanteTipo2NoAportanteConverter(final MapperFactory mapperFactory) {
//        super(mapperFactory);
//        personaNaturalTipo2PersonaConverter = this.getConverterFactory().getConverter(PersonaNaturalTipo.class, Persona.class);
//    }
//
//    @Override
//    public NoAportante convertTo(NoAportanteTipo srcObj) {
//        NoAportante noAportante = new NoAportante();
//        this.copyTo(srcObj, noAportante);
//        return noAportante;
//    }
//
//    @Override
//    public void copyTo(NoAportanteTipo srcObj, NoAportante destObj) {
//        personaNaturalTipo2PersonaConverter.copy(srcObj, destObj.getPersona());
//    }
//
//    @Override
//    public NoAportanteTipo convertFrom(NoAportante srcObj) {
//        NoAportanteTipo noAportanteTipo = new NoAportanteTipo();
//        this.copyFrom(srcObj, noAportanteTipo);
//        return noAportanteTipo;
//    }
//
//    @Override
//    public void copyFrom(NoAportante srcObj, NoAportanteTipo destObj) {
//        
//        srcObj.getPersona().setClasificacionPersona(ClasificacionPersonaEnum.NO_APORTANTE);
//        personaNaturalTipo2PersonaConverter.copy(srcObj.getPersona(), destObj);
//        destObj.setCodTipoNoAportante(this.getMapperFacade().map(srcObj.getCodTipoNoAportante(), String.class));
//    }
//
//}
