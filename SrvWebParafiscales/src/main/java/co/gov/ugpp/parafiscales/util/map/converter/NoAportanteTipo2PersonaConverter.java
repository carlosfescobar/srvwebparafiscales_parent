package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.Converter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;

/**
 *
 * @author jlsaenzar
 */
public class NoAportanteTipo2PersonaConverter extends AbstractBidirectionalConverter<NoAportanteTipo, Persona> {

    private final Converter personaNaturalTipo2PersonaConverter;

    public NoAportanteTipo2PersonaConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
        personaNaturalTipo2PersonaConverter = this.getConverterFactory().getConverter(PersonaNaturalTipo.class, Persona.class);
    }

    @Override
    public Persona convertTo(NoAportanteTipo srcObj) {
        Persona persona = new Persona();
        this.copyTo(srcObj, persona);
        return persona;
    }

    @Override
    public void copyTo(NoAportanteTipo srcObj, Persona destObj) {
        personaNaturalTipo2PersonaConverter.copy(srcObj, destObj);
    }

    @Override
    public NoAportanteTipo convertFrom(Persona srcObj) {
        NoAportanteTipo noAportanteTipo = new NoAportanteTipo();
        this.copyFrom(srcObj, noAportanteTipo);
        return noAportanteTipo;
    }

    @Override
    public void copyFrom(Persona srcObj, NoAportanteTipo destObj) {

        srcObj.setClasificacionPersona(ClasificacionPersonaEnum.NO_APORTANTE);
        personaNaturalTipo2PersonaConverter.copy(srcObj, destObj);
//        destObj.setCodTipoNoAportante(this.getMapperFacade().map(srcObj.getCodTipoNoAportante(), String.class));
    }

}
