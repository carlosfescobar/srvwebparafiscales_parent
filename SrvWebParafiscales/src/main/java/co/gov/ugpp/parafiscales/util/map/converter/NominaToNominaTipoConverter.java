package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Nomina;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.nominatipo.v1.NominaTipo;

/**
 *
 * @author jmuncab
 */
public class NominaToNominaTipoConverter extends AbstractCustomConverter<Nomina, NominaTipo> {

    public NominaToNominaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public NominaTipo convert(Nomina srcObj) {
        return copy(srcObj, new NominaTipo());
    }

    @Override
    public NominaTipo copy(Nomina srcObj, NominaTipo destObj) {
        destObj.setIdNomina(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodigoEstado(srcObj.getEstado());
        destObj.setEstado(srcObj.getEstadoNomina());
        return destObj;
    }
}
