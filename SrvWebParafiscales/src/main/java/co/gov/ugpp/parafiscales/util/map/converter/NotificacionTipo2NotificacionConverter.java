package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosCodnot;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentosAnexosIdnot;
import co.gov.ugpp.parafiscales.persistence.entity.Notificacion;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;

/**
 *
 * @author jmuncab
 */
public class NotificacionTipo2NotificacionConverter extends AbstractBidirectionalConverter<NotificacionTipo, Notificacion> {

    public NotificacionTipo2NotificacionConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Notificacion convertTo(NotificacionTipo srcObj) {
        Notificacion notificacion = new Notificacion();
        this.copyTo(srcObj, notificacion);
        return notificacion;
    }

    @Override
    public void copyTo(NotificacionTipo srcObj, Notificacion destObj) {
        destObj.setDesActaNotififcacionValidad(srcObj.getDescActaNotificacionValidadaObservaciones());
        destObj.setDesCalidadNotificado(srcObj.getDescCalidadNotificado());
        destObj.setDesDevolucionActoAdministr(srcObj.getDescDevolucionActoAdministrativoObservaciones());
        destObj.setEsActaNotificacionValidada(this.getMapperFacade().map(srcObj.getEsActaNotificacionValidada(), Boolean.class));
        destObj.setEsDevlucionActoAdministrat(this.getMapperFacade().map(srcObj.getEsDevolucionActoAdministrativoAprobada(), Boolean.class));
        destObj.setEsMecanismoSubsidiario(this.getMapperFacade().map(srcObj.getEsMecanismoSubsidiario(), Boolean.class));
        destObj.setFecNotificacion(srcObj.getFecNotificacion());
        destObj.setIdRadicadoDocumentosAnexos(srcObj.getIdRadicadoDocumentosAnexosNotificacion());
    }

    @Override
    public NotificacionTipo convertFrom(Notificacion srcObj) {
        NotificacionTipo notificacionTipo = new NotificacionTipo();
        this.copyFrom(srcObj, notificacionTipo);
        return notificacionTipo;
    }

    @Override
    public void copyFrom(Notificacion srcObj, NotificacionTipo destObj) {

        if (srcObj.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(srcObj, destObj);
        } else {
            destObj.setIdNotificacion(srcObj.getId() == null ? null : srcObj.getId().toString());
            destObj.setActoAdministrativo(this.getMapperFacade().map(srcObj.getActoAdministrativo(), ActoAdministrativoTipo.class));
            destObj.setCodCalidadNotificado(srcObj.getCodCalidadNotificado() == null ? null : srcObj.getCodCalidadNotificado().getId());
            destObj.setCodTipoNotificacion(srcObj.getCodTipoNotificacion() == null ? null : srcObj.getCodTipoNotificacion().getId());
            destObj.setDescActaNotificacionValidadaObservaciones(srcObj.getDesActaNotififcacionValidad());
            destObj.setDescCalidadNotificado(srcObj.getDesCalidadNotificado());
            destObj.setDescDevolucionActoAdministrativoObservaciones(srcObj.getDesDevolucionActoAdministr());
            destObj.setEsActaNotificacionValidada(this.getMapperFacade().map(srcObj.getEsActaNotificacionValidada(), String.class));
            destObj.setEsDevolucionActoAdministrativoAprobada(this.getMapperFacade().map(srcObj.getEsDevlucionActoAdministrat(), String.class));
            destObj.setEsMecanismoSubsidiario(this.getMapperFacade().map(srcObj.getEsMecanismoSubsidiario(), String.class));
            destObj.setFecNotificacion(srcObj.getFecNotificacion());
            destObj.setIdRadicadoDocumentosAnexosNotificacion(srcObj.getIdRadicadoDocumentosAnexos());
            destObj.setPersonaNaturalNotificada(this.getMapperFacade().map(srcObj.getPersona(), PersonaNaturalTipo.class));

            if (srcObj.getDocumentosAnexosCodnotList() != null || srcObj.getDocumentosAnexosCodnotList().size() > 0) {
                for (DocumentosAnexosCodnot docCodAnexo : srcObj.getDocumentosAnexosCodnotList()) {
                    destObj.getCodDocumentosAnexosNotificacionPersonal().add(docCodAnexo.getDocumentosAnexosNotif());
                }
            }
            if (srcObj.getDocumentosAnexosIdnotList() != null || srcObj.getDocumentosAnexosIdnotList().size() > 0) {
                for (DocumentosAnexosIdnot docIdAnexo : srcObj.getDocumentosAnexosIdnotList()) {
                    destObj.getIdDocumentosAnexos().add(docIdAnexo.getIdDocumentosAnexos());
                }
            }
        }
    }

    private void mapUseSpecifiedFields(Notificacion srcObj, NotificacionTipo destObj) {

        if (srcObj.getActoAdministrativo() != null) {
            ActoAdministrativoTipo actoAdministrativoTipo = new ActoAdministrativoTipo();
            actoAdministrativoTipo.setCodTipoActoAdministrativo(srcObj.getActoAdministrativo().getCodTipoActoAdministrativo() == null ? null : srcObj.getActoAdministrativo().getCodTipoActoAdministrativo().getId());
            actoAdministrativoTipo.setIdActoAdministrativo(srcObj.getActoAdministrativo().getId());
            actoAdministrativoTipo.setValVigencia(srcObj.getActoAdministrativo().getValVigencia());
            destObj.setActoAdministrativo(actoAdministrativoTipo);
        }
        destObj.setFecNotificacion(srcObj.getFecNotificacion());
        destObj.setCodTipoNotificacionDefinitiva(srcObj.getCodTipoNotificacionDefinitva() == null ? null : srcObj.getCodTipoNotificacionDefinitva().getId());
        destObj.setCodEstadoNotificacion(srcObj.getCodEstadoNotificacion() == null ? null : srcObj.getCodEstadoNotificacion().getId());
    }
}
