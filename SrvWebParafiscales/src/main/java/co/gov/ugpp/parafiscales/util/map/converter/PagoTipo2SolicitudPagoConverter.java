package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPago;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1.PagoTipo;

/**
 *
 * @author jsaenzar
 */
public class PagoTipo2SolicitudPagoConverter extends AbstractBidirectionalConverter<PagoTipo, SolicitudPago> {

    public PagoTipo2SolicitudPagoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudPago convertTo(PagoTipo srcObj) {
        SolicitudPago solicitudPago = new SolicitudPago();
        this.copyTo(srcObj, solicitudPago);
        return solicitudPago;
    }

    @Override
    public void copyTo(PagoTipo srcObj, SolicitudPago destObj) {
        destObj.setValValor(srcObj.getValValor() == null ? null : Long.valueOf(srcObj.getValValor()));
        destObj.setFecPeriodoPago(srcObj.getPeriodoPago());
    }

    @Override
    public PagoTipo convertFrom(SolicitudPago srcObj) {
        PagoTipo pagoTipo = new PagoTipo();
        this.copyFrom(srcObj, pagoTipo);
        return pagoTipo;
    }

    @Override
    public void copyFrom(SolicitudPago srcObj, PagoTipo destObj) {
        destObj.setIdPago(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setPeriodoPago(srcObj.getFecPeriodoPago());
        destObj.setValValor(srcObj.getValValor() == null ? null : srcObj.getValValor().toString());
    }

}
