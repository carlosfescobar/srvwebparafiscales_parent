package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class PersonaJuridicaTipo2PersonaConverter extends AbstractBidirectionalConverter<PersonaJuridicaTipo, Persona> {

    public PersonaJuridicaTipo2PersonaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Persona convertTo(PersonaJuridicaTipo srcObj) {
        Persona persona = new Persona();
        this.copyTo(srcObj, persona);
        return persona;
    }

    @Override
    public void copyTo(PersonaJuridicaTipo srcObj, Persona destObj) {
        destObj.setContacto(this.getMapperFacade().map(srcObj.getContacto(), ContactoPersona.class));
    }

    @Override
    public PersonaJuridicaTipo convertFrom(Persona srcObj) {
        PersonaJuridicaTipo personaJuridicaTipo = new PersonaJuridicaTipo();
        this.copyFrom(srcObj, personaJuridicaTipo);
        return personaJuridicaTipo;
    }

    @Override
    public void copyFrom(Persona srcObj, PersonaJuridicaTipo destObj) {

        if (srcObj.getClasificacionPersona() == null) {
            srcObj.setClasificacionPersona(ClasificacionPersonaEnum.PERSONA_JURIDICA);
        }

        final ValorDominio tipoPersonaActual = Util.obtenerUltimaClasificacionPersona(srcObj);

        IdentificacionTipo identificacionTipo = new IdentificacionTipo();
        identificacionTipo.setCodTipoIdentificacion(srcObj.getCodTipoIdentificacion().getId());
        identificacionTipo.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
        identificacionTipo.setMunicipioExpedicion(this.getMapperFacade().map(srcObj.getMunicipio(), MunicipioTipo.class));
        destObj.setIdPersona(identificacionTipo);
        destObj.setValNombreRazonSocial(srcObj.getValNombreRazonSocial());
        destObj.setCodSeccionActividadEconomica(srcObj.getCodSeccionActividadEcon() == null ? null : srcObj.getCodSeccionActividadEcon().getId());
        destObj.setDescSeccionActividadEconomica(srcObj.getCodSeccionActividadEcon() == null ? null : srcObj.getCodSeccionActividadEcon().getDescDescSeccion());
        destObj.setCodDivisionActividadEconomica(srcObj.getCodDivisionActividadEco() == null ? null : srcObj.getCodDivisionActividadEco().getId().toString());
        destObj.setDescDivisionActividadEconomica(srcObj.getCodDivisionActividadEco() == null ? null : srcObj.getCodDivisionActividadEco().getDescDivision());
        destObj.setCodGrupoActividadEconomica(srcObj.getCodGrupoActividadEconom() == null ? null : srcObj.getCodGrupoActividadEconom().getId().toString());
        destObj.setDescGrupoActividadEconomica(srcObj.getCodGrupoActividadEconom() == null ? null : srcObj.getCodGrupoActividadEconom().getDescGrupo());
        destObj.setCodClaseActividadEconomica(srcObj.getCodClaseActividadEconom() == null ? null : srcObj.getCodClaseActividadEconom().getId().toString());
        destObj.setDescClaseActividadEconomica(srcObj.getCodClaseActividadEconom() == null ? null : srcObj.getCodClaseActividadEconom().getDescClase());
        destObj.setContacto(this.getMapperFacade().map(srcObj.getContacto(), ContactoPersonaTipo.class));
        destObj.setIdRepresentanteLegal(this.getMapperFacade().map(srcObj.getRepresentanteLegal(), IdentificacionTipo.class));
        destObj.setIdAbogado(this.getMapperFacade().map(srcObj.getAbogado(), IdentificacionTipo.class));
        destObj.setIdAutorizado(this.getMapperFacade().map(srcObj.getAutorizado(), IdentificacionTipo.class));
        destObj.setValNumTrabajadores(srcObj.getValNumeroTrabajadores() == null ? null : srcObj.getValNumeroTrabajadores().toString());
        destObj.setCodFuente(srcObj.getCodFuente() == null ? null : srcObj.getCodFuente().getId());
        destObj.setCodTipoPersonaJuridica(tipoPersonaActual == null ? null : tipoPersonaActual.getId());
        destObj.setDescTipoPersonaJuridica(tipoPersonaActual == null ? null : tipoPersonaActual.getNombre());
    }

}
