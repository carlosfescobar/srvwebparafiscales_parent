package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class PersonaNaturalTipo2PersonaConverter extends AbstractBidirectionalConverter<PersonaNaturalTipo, Persona> {

    public PersonaNaturalTipo2PersonaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Persona convertTo(PersonaNaturalTipo srcObj) {
        Persona persona = new Persona();
        this.copyTo(srcObj, persona);
        return persona;
    }

    @Override
    public void copyTo(PersonaNaturalTipo srcObj, Persona destObj) {
        destObj.setValPrimerApellido(srcObj.getValPrimerApellido());
        destObj.setValSegundoApellido(srcObj.getValSegundoApellido());
        destObj.setValPrimerNombre(srcObj.getValPrimerNombre());
        destObj.setValSegundoNombre(srcObj.getValSegundoNombre());
        destObj.setContacto(this.getMapperFacade().map(srcObj.getContacto(), ContactoPersona.class));
    }

    @Override
    public PersonaNaturalTipo convertFrom(Persona srcObj) {
        PersonaNaturalTipo personaNaturalTipo = new PersonaNaturalTipo();
        this.copyFrom(srcObj, personaNaturalTipo);
        return personaNaturalTipo;
    }

    @Override
    public void copyFrom(Persona srcObj, PersonaNaturalTipo destObj) {

        if (srcObj.getClasificacionPersona() == null) {
            srcObj.setClasificacionPersona(ClasificacionPersonaEnum.PERSONA_NATURAL);
        }

        final ValorDominio tipoPersonaActual = Util.obtenerUltimaClasificacionPersona(srcObj);

        IdentificacionTipo identificacionTipo = new IdentificacionTipo();
        identificacionTipo.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
        identificacionTipo.setCodTipoIdentificacion(this.getMapperFacade().map(srcObj.getCodTipoIdentificacion(), String.class));
        identificacionTipo.setMunicipioExpedicion(this.getMapperFacade().map(srcObj.getMunicipio(), MunicipioTipo.class));
        destObj.setIdPersona(identificacionTipo);
        destObj.setIdPersona(identificacionTipo);
        destObj.setValPrimerNombre(srcObj.getValPrimerNombre());
        destObj.setValSegundoNombre(srcObj.getValSegundoNombre());
        destObj.setValPrimerApellido(srcObj.getValPrimerApellido());
        destObj.setValSegundoApellido(srcObj.getValSegundoApellido());
        destObj.setValNombreCompleto(srcObj.getNombreCompleto());
        destObj.setCodSexo(srcObj.getCodSexo() == null ? null : srcObj.getCodSexo().getId());
        destObj.setDescSexo(srcObj.getCodSexo() == null ? null : srcObj.getCodSexo().getNombre());
        destObj.setCodEstadoCivil(srcObj.getCodEstadoCivil() == null ? null : srcObj.getCodEstadoCivil().getId());
        destObj.setDescEstadoCivil(srcObj.getCodEstadoCivil() == null ? null : srcObj.getCodEstadoCivil().getNombre());
        destObj.setCodNivelEducativo(srcObj.getCodNivelEducativo() == null ? null : srcObj.getCodNivelEducativo().getId());
        destObj.setDescNivelEducativo(srcObj.getCodNivelEducativo() == null ? null : srcObj.getCodNivelEducativo().getNombre());
        destObj.setContacto(this.getMapperFacade().map(srcObj.getContacto(), ContactoPersonaTipo.class));
        destObj.setIdAbogado(this.getMapperFacade().map(srcObj.getAbogado(), IdentificacionTipo.class));
        destObj.setIdAutorizado(this.getMapperFacade().map(srcObj.getAutorizado(), IdentificacionTipo.class));
        destObj.setCodFuente(srcObj.getCodFuente() == null ? null : srcObj.getCodFuente().getId());
        destObj.setCodTipoPersonaNatural(tipoPersonaActual == null ? null : tipoPersonaActual.getId());
        destObj.setDescTipoPersonaNatural(tipoPersonaActual == null ? null : tipoPersonaActual.getNombre());

    }
}
