package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jlsaenzar
 */
public class PersonaTipo2PersonaConverter extends AbstractBidirectionalConverter<ConceptoPersonaTipo, Persona> {

    public PersonaTipo2PersonaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Persona convertTo(ConceptoPersonaTipo srcObj) {
        Persona persona = new Persona();
        this.copyTo(srcObj, persona);
        return persona;
    }

    @Override
    public void copyTo(ConceptoPersonaTipo srcObj, Persona destObj) {
        this.getMapperFacade().map(srcObj, destObj);
    }

    @Override
    public ConceptoPersonaTipo convertFrom(Persona srcObj) {
        ConceptoPersonaTipo conceptoPersonaTipo = new ConceptoPersonaTipo();
        this.copyFrom(srcObj, conceptoPersonaTipo);
        return conceptoPersonaTipo;
    }

    @Override
    public void copyFrom(Persona srcObj, ConceptoPersonaTipo destObj) {

        final String tipoPersona = srcObj.getCodNaturalezaPersona() == null ? null : srcObj.getCodNaturalezaPersona().getId();

        if (TipoPersonaEnum.PERSONA_JURIDICA.getCode().equals(tipoPersona)) {
            destObj.setPersonaJuridica(this.getMapperFacade().map(srcObj, PersonaJuridicaTipo.class));
        } else {
            destObj.setPersonaNatural(this.getMapperFacade().map(srcObj, PersonaNaturalTipo.class));
        }
    }
}
