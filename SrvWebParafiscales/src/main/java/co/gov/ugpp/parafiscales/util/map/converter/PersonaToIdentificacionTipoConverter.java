package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class PersonaToIdentificacionTipoConverter extends AbstractCustomConverter<Persona, IdentificacionTipo> {

    public PersonaToIdentificacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public IdentificacionTipo convert(Persona srcObj) {
        return copy(srcObj, new IdentificacionTipo());
    }

    @Override
    public IdentificacionTipo copy(Persona srcObj, IdentificacionTipo destObj) {

        destObj.setCodTipoIdentificacion(srcObj.getCodTipoIdentificacion() == null ? null : srcObj.getCodTipoIdentificacion().getId());
        destObj.setValNumeroIdentificacion(srcObj.getValNumeroIdentificacion());
        MunicipioTipo municipioTipo = this.getMapperFacade().map(srcObj.getMunicipio(), MunicipioTipo.class);
        destObj.setMunicipioExpedicion(municipioTipo);

        return destObj;
    }

}
