package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.Planilla;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;

/**
 *
 * @author jsaenzar
 */
public class PlanillaTipo2PlanillaConverter extends AbstractBidirectionalConverter<PlanillaTipo, Planilla> {

    public PlanillaTipo2PlanillaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Planilla convertTo(PlanillaTipo srcObj) {
        Planilla planilla = new Planilla();
        this.copyTo(srcObj, planilla);
        return planilla;
    }

    @Override
    public void copyTo(PlanillaTipo srcObj, Planilla destObj) {
//        destObj.setFecPeriodoPago(srcObj.getFecPeriodoPago());
    }

    @Override
    public PlanillaTipo convertFrom(Planilla srcObj) {
        PlanillaTipo planillaTipo = new PlanillaTipo();
        this.copyFrom(srcObj, planillaTipo);
        return planillaTipo;
    }

    @Override
    public void copyFrom(Planilla srcObj, PlanillaTipo destObj) {
        destObj.setIdNumeroPlanilla(srcObj.getId() == null ? null : srcObj.getId());
        destObj.setCodOperador(srcObj.getCodTipoOperador() == null ? null : srcObj.getCodTipoOperador().getId());
//        destObj.setFecPeriodoPago(srcObj.getFecPeriodoPago());
    }
}
