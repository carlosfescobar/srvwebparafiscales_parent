package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPlanilla;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;

/**
 *
 * @author jsaenzar
 */
public class PlanillaTipo2SolicitudPlanillaConverter extends AbstractBidirectionalConverter<PlanillaTipo, SolicitudPlanilla> {

    public PlanillaTipo2SolicitudPlanillaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudPlanilla convertTo(PlanillaTipo srcObj) {
        SolicitudPlanilla solicitudPlanilla = new SolicitudPlanilla();
        this.copyTo(srcObj, solicitudPlanilla);
        return solicitudPlanilla;
    }

    @Override
    public void copyTo(PlanillaTipo srcObj, SolicitudPlanilla destObj) {
        //destObj.setValAnio(srcObj.getFecPeriodoPago() == null ? null :(long) srcObj.getFecPeriodoPago().get(Calendar.YEAR));
        //destObj.setValMes(srcObj.getFecPeriodoPago() == null ? null : (short) srcObj.getFecPeriodoPago().get(Calendar.MONTH));
    }

    @Override
    public PlanillaTipo convertFrom(SolicitudPlanilla srcObj) {
        PlanillaTipo planillaTipo = new PlanillaTipo();
        this.copyFrom(srcObj, planillaTipo);
        return planillaTipo;
    }

    @Override
    public void copyFrom(SolicitudPlanilla srcObj, PlanillaTipo destObj) {
        final PlanillaTipo planillaTipo = this.getMapperFacade().map(srcObj.getPlanilla(), PlanillaTipo.class);
        destObj.setIdNumeroPlanilla(srcObj.getPlanilla() == null ? null : srcObj.getPlanilla().getId());
        destObj.setFecPeriodoPago(srcObj.getFecPeriodoPago());
        destObj.setCodOperador(srcObj.getPlanilla() == null ? null : planillaTipo.getCodOperador());
        destObj.setDescObservaciones(srcObj.getPlanilla() == null ? null : srcObj.getPlanilla().getValPlanilla());
        destObj.setCodTipoObservaciones(srcObj.getPlanilla().getCodTipoObservaciones() == null ? null : srcObj.getPlanilla().getCodTipoObservaciones().getId());
        destObj.setDescTipoObservaciones(srcObj.getPlanilla().getCodTipoObservaciones() == null ? null : srcObj.getPlanilla().getCodTipoObservaciones().getNombre());

    }
}
