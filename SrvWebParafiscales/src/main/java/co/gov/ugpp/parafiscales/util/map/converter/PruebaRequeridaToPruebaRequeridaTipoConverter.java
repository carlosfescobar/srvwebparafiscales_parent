package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SancionPruebaRequerida;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;

/**
 *
 * @author jmuncab
 */
public class PruebaRequeridaToPruebaRequeridaTipoConverter extends AbstractCustomConverter<SancionPruebaRequerida, InformacionPruebaTipo> {

    public PruebaRequeridaToPruebaRequeridaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public InformacionPruebaTipo convert(SancionPruebaRequerida srcObj) {
        return copy(srcObj, new InformacionPruebaTipo());
    }

    @Override
    public InformacionPruebaTipo copy(SancionPruebaRequerida srcObj, InformacionPruebaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getInformacionPrueba(), InformacionPruebaTipo.class);
        return destObj;
    }

  

}
