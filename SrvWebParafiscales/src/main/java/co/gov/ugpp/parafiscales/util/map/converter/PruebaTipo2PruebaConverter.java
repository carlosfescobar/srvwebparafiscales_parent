package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Prueba;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;

/**
 *
 * @author zrodriguez
 */
public class PruebaTipo2PruebaConverter extends AbstractBidirectionalConverter<PruebaTipo, Prueba> {

    public PruebaTipo2PruebaConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Prueba convertTo(PruebaTipo srcObj) {
        Prueba prueba = new Prueba();
        this.copy(srcObj, prueba);
        return prueba;
    }

    @Override
    public void copyTo(PruebaTipo srcObj, Prueba destObj) {
        destObj.setCodTipoPrueba(this.getMapperFacade().map(srcObj.getCodTipoPrueba(), ValorDominio.class));
        destObj.setCodTipoPruebaExterna(this.getMapperFacade().map(srcObj.getCodTipoPruebaExterna(), ValorDominio.class));
        destObj.setFechaSolicitudPrueba(srcObj.getFecSolicitudPrueba());
        destObj.setValTiempoEsperaPruebaExterna(srcObj.getValTiempoEsperaPruebaExterna());
        destObj.setDesInformacionSolicitada(srcObj.getDescInformacionSolicitada());
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExterna.class));
        destObj.setEsPruebaSolicitud(this.getMapperFacade().map(srcObj.getEsPruebaSolicitada(), Boolean.class));
        destObj.setEsSolicitudPruebaInterna(this.getMapperFacade().map(srcObj.getEsSolicitadaPruebaInterna(), Boolean.class));
        destObj.setEsResueltaSolicitudInterna(this.getMapperFacade().map(srcObj.getEsResueltaSolicitudInterna(), Boolean.class));
        destObj.setEsContinuadoProcesoInterno(this.getMapperFacade().map(srcObj.getEsContinuadoProcesoInterno(), Boolean.class));
        destObj.setSolicitudPruebaExternaParcial(this.getMapperFacade().map(srcObj.getSolicitudPruebaExternaParcial(), AprobacionDocumento.class));
        destObj.setIdActoSolicitudPruebaExterna(this.getMapperFacade().map(srcObj.getIdActoSolicitudPruebaExterna(), ActoAdministrativo.class));
        destObj.setInspeccionTributariaParcial(this.getMapperFacade().map(srcObj.getInspeccionTributariaParcial(), AprobacionDocumento.class));
        destObj.setIdActoInspeccionTributaria(this.getMapperFacade().map(srcObj.getIdActoInspeccionTributaria(), ActoAdministrativo.class));
        destObj.setDescObservacionesSolicitudInterna(srcObj.getDescObservacionesSolicitudInterna());
        destObj.setFecInspeccionTributaria(srcObj.getFecInspeccionTributaria());
        destObj.setValNombreInspector(srcObj.getValNombreInspector());
        destObj.setValNombrePersonaContactadaInspeccion(srcObj.getValNombrePersonaContactadaInspeccion());
        destObj.setValCargoPersonaContactadaInspeccion(srcObj.getValCargoPersonaContactadaInspeccion());
        destObj.setIdDocumentoInformeInspeccion(srcObj.getIdDocumentoInformeInspeccion());
        destObj.setDescObservacionesInspeccion(srcObj.getDescObservacionesInspeccion());
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), Aportante.class));

    }

    @Override
    public PruebaTipo convertFrom(Prueba srcObj) {
        PruebaTipo pruebaTipo = new PruebaTipo();
        this.copy(srcObj, pruebaTipo);
        return pruebaTipo;
    }

    @Override
    public void copyFrom(Prueba srcObj, PruebaTipo destObj) {

        destObj.setIdPrueba(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodTipoPrueba(this.getMapperFacade().map(srcObj.getCodTipoPrueba(), String.class));
        destObj.setFecSolicitudPrueba(srcObj.getFechaSolicitudPrueba());
        destObj.setValTiempoEsperaPruebaExterna(srcObj.getValTiempoEsperaPruebaExterna());
        destObj.setDescInformacionSolicitada(srcObj.getDesInformacionSolicitada());
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class));
        destObj.setEsPruebaSolicitada(this.getMapperFacade().map(srcObj.getEsPruebaSolicitud(), String.class));
        destObj.setEsSolicitadaPruebaInterna(this.getMapperFacade().map(srcObj.getEsSolicitudPruebaInterna(), String.class));
        destObj.setEsResueltaSolicitudInterna(this.getMapperFacade().map(srcObj.getEsResueltaSolicitudInterna(), String.class));
        destObj.setEsContinuadoProcesoInterno(this.getMapperFacade().map(srcObj.getEsContinuadoProcesoInterno(), String.class));
        destObj.setSolicitudPruebaExternaParcial(this.getMapperFacade().map(srcObj.getSolicitudPruebaExternaParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoSolicitudPruebaExterna(srcObj.getIdActoSolicitudPruebaExterna()==null ? null :srcObj.getIdActoSolicitudPruebaExterna().getId());
        destObj.setInspeccionTributariaParcial(this.getMapperFacade().map(srcObj.getInspeccionTributariaParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoInspeccionTributaria(srcObj.getIdActoInspeccionTributaria()== null ? null : srcObj.getIdActoInspeccionTributaria().getId());
        destObj.setDescObservacionesSolicitudInterna(srcObj.getDescObservacionesSolicitudInterna());
        destObj.setFecInspeccionTributaria(srcObj.getFecInspeccionTributaria());
        destObj.setValNombreInspector(srcObj.getValNombreInspector());
        destObj.setValNombrePersonaContactadaInspeccion(srcObj.getValNombrePersonaContactadaInspeccion());
        destObj.setValCargoPersonaContactadaInspeccion(srcObj.getValCargoPersonaContactadaInspeccion());
        destObj.setIdDocumentoInformeInspeccion(srcObj.getIdDocumentoInformeInspeccion());
        destObj.setDescObservacionesInspeccion(srcObj.getDescObservacionesInspeccion());
        destObj.setCodTipoPruebaExterna(this.getMapperFacade().map(srcObj.getCodTipoPruebaExterna(), String.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));

    }
}
