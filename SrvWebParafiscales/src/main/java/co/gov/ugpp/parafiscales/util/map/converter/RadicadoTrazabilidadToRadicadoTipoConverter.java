package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RadicadoTrazabilidad;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1.RadicadoTipo;

/**
 *
 * @author jmuncab
 */
public class RadicadoTrazabilidadToRadicadoTipoConverter extends AbstractBidirectionalConverter<RadicadoTipo, RadicadoTrazabilidad> {
    
    public RadicadoTrazabilidadToRadicadoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public RadicadoTrazabilidad convertTo(RadicadoTipo srcObj) {
        RadicadoTrazabilidad radicadoTrazabilidad = new RadicadoTrazabilidad();
        this.copyTo(srcObj, radicadoTrazabilidad);
        return radicadoTrazabilidad;
    }
    
    @Override
    public void copyTo(RadicadoTipo srcObj, RadicadoTrazabilidad destObj) {

        destObj.setFechaRadicado(srcObj.getFecRadicado());
        destObj.setIdRadicado(srcObj.getIdRadicado());
        
    }
    
    @Override
    public RadicadoTipo convertFrom(RadicadoTrazabilidad srcObj) {
        RadicadoTipo radicadoTipo = new RadicadoTipo();
        this.copyFrom(srcObj, radicadoTipo);
        return radicadoTipo;
    }
    
    @Override
    public void copyFrom(RadicadoTrazabilidad srcObj, RadicadoTipo destObj) {
        
        destObj.setIdRadicado(srcObj.getIdRadicado());
        destObj.setFecRadicado(srcObj.getFechaRadicado());
        destObj.setCodRadicado(this.getMapperFacade().map(srcObj.getCodRadicado(), String.class));
        
    }
}
