package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.ControlLiquidador;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.Prueba;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoReposicion;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoRequisitos;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoCausalFallo;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconArchivoTemp;
import co.gov.ugpp.parafiscales.persistence.entity.RecursoReconsideracion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jmuncab
 */
public class RecursoReconsideracionToRecursoReconsideracionTipoConverter extends AbstractBidirectionalConverter<RecursoReconsideracion, RecursoReconsideracionTipo> {

    public RecursoReconsideracionToRecursoReconsideracionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public RecursoReconsideracionTipo convertTo(RecursoReconsideracion srcObj) {
        RecursoReconsideracionTipo reconsideracionTipo = new RecursoReconsideracionTipo();
        this.copy(srcObj, reconsideracionTipo);
        return reconsideracionTipo;
    }

    @Override

    public void copyTo(RecursoReconsideracion srcObj, RecursoReconsideracionTipo destObj) {
        if (srcObj.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(srcObj, destObj);
        } else {
            destObj.setIdRecursoReconsideracion(srcObj.getId() == null ? null : srcObj.getId().toString());
            destObj.setCodProcesoEjecutador(this.getMapperFacade().map(srcObj.getCodProcesoEjecutador(), String.class));
            destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
            destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
            destObj.setFecEntradaRecursoReconsideracion((srcObj.getFecEntradaRecursoReconsideracion()));
            destObj.setIdRadicadoEntradaRecursoReconsideracion((srcObj.getIdRadicadoEntradaRecursoReconsideracion()));
            destObj.setIdActoRequerimientoDeclararCorregir((srcObj.getIdActoRequerimientoDeclararCorregir() == null ? null : srcObj.getIdActoRequerimientoDeclararCorregir().getId()));
            destObj.setEsDigitalizadoExpediente(this.getMapperFacade().map(srcObj.getEsDsEsDigitalizadoExpediente(), String.class));
            destObj.setDescObservacionesDigitalizacion((srcObj.getDescObservacionesDigitalizacion()));
            destObj.setEsAdmisibleRecurso(this.getMapperFacade().map(srcObj.getEsAdmisibleRecurso(), String.class));
            destObj.setEsSubsanable(this.getMapperFacade().map(srcObj.getEsSubsanable(), String.class));
            destObj.getCodDocumentosAnexosRequisitos().addAll(this.getMapperFacade().map(srcObj.getCodDocumentosAnexosRequisitos(), RecursoAnexoRequisitos.class, String.class));
            destObj.setDescObservacionesRequisitos((srcObj.getDescObservacionesRequisitos()));
            destObj.setAutoAdmisorioParcial(this.getMapperFacade().map(srcObj.getAutoAdmisorioParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoAutoAdmisorio(srcObj.getIdActoAutoAdmisorio() == null ? null : srcObj.getIdActoAutoAdmisorio().getId());
            destObj.setAutoInadmisorioParcial(this.getMapperFacade().map(srcObj.getAutoInadmisorioParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoAutoInadmisorio(srcObj.getIdActoAutoInadmisorio() == null ? null : srcObj.getIdActoAutoInadmisorio().getId());
            destObj.setEsPresentadoRecursoReposicion(this.getMapperFacade().map(srcObj.getEsPresentadoRecursoReposicion(), String.class));
            destObj.setEsSubsanadaInadmisibilidad(this.getMapperFacade().map(srcObj.getEsSubsanadaInadmisibilidad(), String.class));
            destObj.setFecEntradaRecursoReposicion((srcObj.getFecEntradaRecursoReposicion()));
            destObj.setIdRadicadoEntradaRecursoReposicion((srcObj.getIdRadicadoEntradaRecursoReposicion()));
            destObj.getCodDocumentosAnexosRecursoReposicion().addAll(this.getMapperFacade().map(srcObj.getCodDocumentosAnexosRecursoReposicion(), RecursoAnexoReposicion.class, String.class));
            destObj.setDescObservacionesRecursoReposicion((srcObj.getDescObservacionesRecursoReposicion()));
            destObj.setActoAprobacionInadmisionParcial(this.getMapperFacade().map(srcObj.getActoAprobacionInadmisionParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoAprobacionInadmision(srcObj.getIdActoAprobacionInadmision() == null ? null : srcObj.getIdActoAprobacionInadmision().getId());
            destObj.setActoReponeAdmiteParcial(this.getMapperFacade().map(srcObj.getActoReponeAdmiteParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoReponeAdmite(srcObj.getIdActoReponeAdmite() == null ? null : srcObj.getIdActoReponeAdmite().getId());
            destObj.setEsPruebasSuficientes(this.getMapperFacade().map(srcObj.getEsPruebasSuficientes(), String.class));
            destObj.setEsNecesarioEjecutarLiquidador(this.getMapperFacade().map(srcObj.getEsNecesarioEjecutarLiquidador(), String.class));
            destObj.getPruebas().addAll(this.getMapperFacade().map(srcObj.getPruebas(), Prueba.class, PruebaTipo.class));
            destObj.setValImporteSancion(srcObj.getValImporteSancion());
            destObj.setValNuevoImporteSancion(srcObj.getValNuevoImporteSancion());
            destObj.setDescObservacionesCalculoSancion(srcObj.getDescObservacionesCalculoSancion());
            destObj.setCodFalloFinal(this.getMapperFacade().map(srcObj.getCodFalloFinal(), String.class));
            destObj.setValFinalActoAdministrativo(srcObj.getValFinalActoAdministrativo());
            destObj.setValFinalActoAdministrativo(srcObj.getValFinalActoAdministrativo());
            destObj.setDescObservacionesFallo(srcObj.getDescObservacionesFallo());
            destObj.getCodCausalesFallo().addAll(this.getMapperFacade().map(srcObj.getcodCausalesFallo(), RecursoCausalFallo.class, String.class));
            destObj.setActoFalloParcial(this.getMapperFacade().map(srcObj.getActoFalloParcial(), AprobacionDocumentoTipo.class));
            destObj.setIdActoFallo(srcObj.getIdActoFallo() == null ? null : srcObj.getIdActoFallo().getId());
            destObj.setIdLiquidacion(srcObj.getIdLiquidacion());
            destObj.setIdSancion(srcObj.getIdSancion());
            destObj.setIdLiquidador(srcObj.getIdLiquidador() == null ? null : srcObj.getIdLiquidador().getId().toString());
            destObj.setCodEstado(this.getMapperFacade().map(srcObj.getCodEstado(), String.class));

            if (srcObj.getArchivosTemporales() != null && !srcObj.getArchivosTemporales().isEmpty()) {
                List<ParametroValoresTipo> anexosTemporales = new ArrayList<ParametroValoresTipo>();
                Map<String, List<ParametroTipo>> anexosTemporalesRecursoReconsideracion = new HashMap<String, List<ParametroTipo>>();
                for (RecursoReconArchivoTemp anexoTemporal : srcObj.getArchivosTemporales()) {
                    List<ParametroTipo> archivosNotificacion = anexosTemporalesRecursoReconsideracion.get(anexoTemporal.getCodTipoAnexo().getId());
                    if (archivosNotificacion == null) {
                        archivosNotificacion = new ArrayList<ParametroTipo>();
                    }
                    ParametroTipo anexoNotificacion = new ParametroTipo();
                    anexoNotificacion.setIdLlave(anexoTemporal.getArchivo().getId().toString());
                    anexoNotificacion.setValValor(anexoTemporal.getArchivo().getValNombreArchivo());
                    archivosNotificacion.add(anexoNotificacion);
                    anexosTemporalesRecursoReconsideracion.put(anexoTemporal.getCodTipoAnexo().getId(), archivosNotificacion);
                }
                for (Map.Entry<String, List<ParametroTipo>> entry : anexosTemporalesRecursoReconsideracion.entrySet()) {
                    ParametroValoresTipo documentoFinal = new ParametroValoresTipo();
                    documentoFinal.setIdLlave(entry.getKey());
                    documentoFinal.getValValor().addAll(entry.getValue());
                    anexosTemporales.add(documentoFinal);
                }
                destObj.getArchivosTemporales().addAll(anexosTemporales);
            }
        }
    }

    private void mapUseSpecifiedFields(RecursoReconsideracion srcObj, RecursoReconsideracionTipo destObj) {

        destObj.setIdRecursoReconsideracion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), AportanteTipo.class));
        destObj.getPruebas().addAll(this.getMapperFacade().map(srcObj.getPruebas(), Prueba.class, PruebaTipo.class));

    }

    @Override
    public RecursoReconsideracion convertFrom(RecursoReconsideracionTipo srcObj) {
        RecursoReconsideracion reconsideracion = new RecursoReconsideracion();
        this.copyFrom(srcObj, reconsideracion);
        return reconsideracion;
    }

    @Override
    public void copyFrom(RecursoReconsideracionTipo srcObj, RecursoReconsideracion destObj) {

        destObj.setCodProcesoEjecutador(this.getMapperFacade().map(srcObj.getCodProcesoEjecutador(), ValorDominio.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), Expediente.class));
        destObj.setAportante(this.getMapperFacade().map(srcObj.getAportante(), Aportante.class));
        destObj.setFecEntradaRecursoReconsideracion((srcObj.getFecEntradaRecursoReconsideracion()));
        destObj.setIdRadicadoEntradaRecursoReconsideracion((srcObj.getIdRadicadoEntradaRecursoReconsideracion()));
        destObj.setIdActoRequerimientoDeclararCorregir((this.getMapperFacade().map(srcObj.getIdActoRequerimientoDeclararCorregir(), ActoAdministrativo.class)));
        destObj.setEsDigitalizadoExpediente(this.getMapperFacade().map(srcObj.getEsDigitalizadoExpediente(), Boolean.class));
        destObj.setDescObservacionesDigitalizacion((srcObj.getDescObservacionesDigitalizacion()));
        destObj.setEsAdmisibleRecurso(this.getMapperFacade().map(srcObj.getEsAdmisibleRecurso(), Boolean.class));
        destObj.setEsSubsanable(this.getMapperFacade().map(srcObj.getEsSubsanable(), Boolean.class));
        destObj.getCodDocumentosAnexosRequisitos().addAll(this.getMapperFacade().map(srcObj.getCodDocumentosAnexosRequisitos(), String.class, RecursoAnexoRequisitos.class));
        destObj.setDescObservacionesRequisitos((srcObj.getDescObservacionesRequisitos()));
        destObj.setAutoAdmisorioParcial(this.getMapperFacade().map(srcObj.getAutoAdmisorioParcial().getIdArchivo(), AprobacionDocumento.class));
        destObj.setIdActoAutoAdmisorio(this.getMapperFacade().map(srcObj.getIdActoAutoAdmisorio(), ActoAdministrativo.class));
        destObj.setAutoInadmisorioParcial(this.getMapperFacade().map(srcObj.getAutoInadmisorioParcial().getIdArchivo(), AprobacionDocumento.class));
        destObj.setIdActoAutoInadmisorio(this.getMapperFacade().map(srcObj.getIdActoAutoInadmisorio(), ActoAdministrativo.class));
        destObj.setEsPresentadoRecursoReposicion(this.getMapperFacade().map(srcObj.getEsPresentadoRecursoReposicion(), Boolean.class));
        destObj.setEsSubsanadaInadmisibilidad(this.getMapperFacade().map(srcObj.getEsSubsanadaInadmisibilidad(), Boolean.class));
        destObj.setFecEntradaRecursoReposicion((srcObj.getFecEntradaRecursoReposicion()));
        destObj.setIdRadicadoEntradaRecursoReposicion((srcObj.getIdRadicadoEntradaRecursoReposicion()));
        destObj.getCodDocumentosAnexosRecursoReposicion().addAll(this.getMapperFacade().map(srcObj.getCodDocumentosAnexosRecursoReposicion(), String.class, RecursoAnexoReposicion.class));
        destObj.setDescObservacionesRecursoReposicion((srcObj.getDescObservacionesRecursoReposicion()));
        destObj.setActoAprobacionInadmisionParcial(this.getMapperFacade().map(srcObj.getActoAprobacionInadmisionParcial().getIdArchivo(), AprobacionDocumento.class));
        destObj.setIdActoAprobacionInadmision(this.getMapperFacade().map(srcObj.getIdActoAprobacionInadmision(), ActoAdministrativo.class));
        destObj.setActoReponeAdmiteParcial(this.getMapperFacade().map(srcObj.getActoReponeAdmiteParcial().getIdArchivo(), AprobacionDocumento.class));
        destObj.setIdActoReponeAdmite(this.getMapperFacade().map(srcObj.getIdActoReponeAdmite(), ActoAdministrativo.class));
        destObj.setEsPruebasSuficientes(this.getMapperFacade().map(srcObj.getEsPruebasSuficientes(), Boolean.class));
        destObj.setEsNecesarioEjecutarLiquidador(this.getMapperFacade().map(srcObj.getEsNecesarioEjecutarLiquidador(), Boolean.class));
        destObj.getPruebas().addAll(this.getMapperFacade().map(srcObj.getPruebas(), PruebaTipo.class, Prueba.class));
        destObj.setValImporteSancion(srcObj.getValImporteSancion());
        destObj.setValNuevoImporteSancion(srcObj.getValNuevoImporteSancion());
        destObj.setDescObservacionesCalculoSancion(srcObj.getDescObservacionesCalculoSancion());
        destObj.setCodFalloFinal(this.getMapperFacade().map(srcObj.getCodFalloFinal(), ValorDominio.class));
        destObj.setValFinalActoAdministrativo(srcObj.getValFinalActoAdministrativo());
        destObj.setValFinalActoAdministrativo(srcObj.getValFinalActoAdministrativo());
        destObj.getcodCausalesFallo().addAll(this.getMapperFacade().map(srcObj.getCodCausalesFallo(), String.class, RecursoCausalFallo.class));
        destObj.setIdLiquidacion(srcObj.getIdLiquidacion());
        destObj.setIdSancion(srcObj.getIdSancion());
        destObj.setIdActoFallo(this.getMapperFacade().map(srcObj.getIdActoFallo(), ActoAdministrativo.class));
        destObj.setIdLiquidador(this.getMapperFacade().map(srcObj.getIdLiquidador(), ControlLiquidador.class));
        destObj.setCodEstado(this.getMapperFacade().map(srcObj.getCodEstado(), ValorDominio.class));
    }

}
