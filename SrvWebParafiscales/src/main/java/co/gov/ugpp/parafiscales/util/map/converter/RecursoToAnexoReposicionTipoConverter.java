package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoReposicion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author zrodrigu
 */
public class RecursoToAnexoReposicionTipoConverter extends AbstractCustomConverter<RecursoAnexoReposicion, String> {

    public RecursoToAnexoReposicionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

  

    @Override
    public String convert(RecursoAnexoReposicion srcObj) {
          return copy(srcObj, new String());
    }

    @Override
    public String copy(RecursoAnexoReposicion srcObj, String destObj) {
         destObj = (srcObj.getCodDocumentosAnexosRecursoReposicion()== null ? null : srcObj.getCodDocumentosAnexosRecursoReposicion().getId());
        return destObj;
    }

}
