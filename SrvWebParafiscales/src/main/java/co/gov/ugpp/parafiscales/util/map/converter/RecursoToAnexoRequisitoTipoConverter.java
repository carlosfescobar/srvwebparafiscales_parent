package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RecursoAnexoRequisitos;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author zrodrigu
 */
public class RecursoToAnexoRequisitoTipoConverter extends AbstractCustomConverter<RecursoAnexoRequisitos, String> {

    public RecursoToAnexoRequisitoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(RecursoAnexoRequisitos srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(RecursoAnexoRequisitos srcObj, String destObj) {
        destObj = (srcObj.getCodDocumentosAnexosRequisitos() == null ? null : srcObj.getCodDocumentosAnexosRequisitos().getId());
        return destObj;
    }

}
