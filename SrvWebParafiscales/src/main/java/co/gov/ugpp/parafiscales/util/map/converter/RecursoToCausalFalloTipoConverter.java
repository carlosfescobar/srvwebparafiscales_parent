package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RecursoCausalFallo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author zrodrigu
 */
public class RecursoToCausalFalloTipoConverter extends AbstractCustomConverter<RecursoCausalFallo, String> {

    public RecursoToCausalFalloTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(RecursoCausalFallo srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(RecursoCausalFallo srcObj, String destObj) {
        destObj = (srcObj.getCodCausalesFallo() == null ? null : srcObj.getCodCausalesFallo().getId());
        return destObj;
    }

}
