package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ReenvioComunicacion;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadRadicadoSalida;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1.ReenvioComunicacionTipo;

/**
 *
 * @author yrinconh
 */
public class ReenvioComunicacionToReenvioComunicacionTipoConverter extends AbstractCustomConverter<ReenvioComunicacion, ReenvioComunicacionTipo> {

    public ReenvioComunicacionToReenvioComunicacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ReenvioComunicacionTipo convert(ReenvioComunicacion srcObj) {
        return copy(srcObj, new ReenvioComunicacionTipo());
    }

    @Override
    public ReenvioComunicacionTipo copy(ReenvioComunicacion srcObj, ReenvioComunicacionTipo destObj) {
        destObj.setIdRadicado(srcObj.getIdRadicado());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setIdPersonaDestinatario(this.getMapperFacade().map(srcObj.getIdPersonaDestinatario(), IdentificacionTipo.class));
        destObj.setCodProcesoEnviaComunicacion(srcObj.getCodProcesoEnviaComunicacion() == null ? null : srcObj.getCodProcesoEnviaComunicacion().getId());
        destObj.setDescProcesoEnviaComunicacion(srcObj.getCodProcesoEnviaComunicacion() == null ? null : srcObj.getCodProcesoEnviaComunicacion().getNombre());
        destObj.setCodTipoDireccionEnviada(srcObj.getCodTipoDireccionEnviada() == null ? null : srcObj.getCodTipoDireccionEnviada().getId());
        destObj.setDescTipoDireccionEnviada(srcObj.getCodTipoDireccionEnviada() == null ? null : srcObj.getCodTipoDireccionEnviada().getNombre());
        destObj.setValDireccionEnviada(srcObj.getValDireccionEnviada() == null ? null : srcObj.getValDireccionEnviada());
        destObj.setDescObservaciones(srcObj.getDescObservaciones() == null ? null : srcObj.getDescObservaciones());
        destObj.setCodFuenteInformacion(srcObj.getCodFuenteInformacion() == null ? null : srcObj.getCodFuenteInformacion().getId());
        destObj.setEsDevolucion(this.getMapperFacade().map(srcObj.getEsDevolucion(), String.class));
        destObj.setEsReenvio(this.getMapperFacade().map(srcObj.getEsReenvio(), String.class));
        destObj.getTrazabilidadRadicadoSalida().addAll(this.getMapperFacade().map(srcObj.getTrazabilidadRadicadoSalida() == null ? null : srcObj.getTrazabilidadRadicadoSalida(), TrazabilidadRadicadoSalida.class, AcuseEntregaDocumentoTipo.class));

        return destObj;
    }

}
