package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradoraDecisionCaso;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1.RespuestaAdministradoraTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;

/**
 *
 * @author jmuncab
 */
public class RespuestaAdminToRespuestaAdminTipoConverter extends AbstractCustomConverter<RespuestaAdministradora, RespuestaAdministradoraTipo> {
    
    public RespuestaAdminToRespuestaAdminTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public RespuestaAdministradoraTipo convert(RespuestaAdministradora srcObj) {
        return copy(srcObj, new RespuestaAdministradoraTipo());
    }
    
    @Override
    public RespuestaAdministradoraTipo copy(RespuestaAdministradora srcObj, RespuestaAdministradoraTipo destObj) {
        
        destObj.setIdRespuestaAdministradora(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.getCodDecisionCaso().addAll(this.getMapperFacade().map(srcObj.getCodDecisionCaso(), RespuestaAdministradoraDecisionCaso.class, String.class));
        destObj.setCodTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getId());
        destObj.setCodTipoSancion(srcObj.getCodTipoSancion() == null ? null : srcObj.getCodTipoSancion().getId());
        destObj.setDescObservacionesEntidadVigilancia(srcObj.getDescObservacionesEntidadVigilancia());
        destObj.setDescObservacionesReenvio(srcObj.getDescObservacionesReenvio());
        destObj.setDescTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getNombre());
        destObj.setDescTipoSancion(srcObj.getCodTipoSancion() == null ? null : srcObj.getCodTipoSancion().getNombre());
        destObj.setEsConcedidaProrroga(this.getMapperFacade().map(srcObj.getEsConcedidaProrroga(), String.class));
        destObj.setEsInformacionCorrecta(this.getMapperFacade().map(srcObj.getEsInformacionCorrecta(), String.class));
        destObj.setEsProrroga(this.getMapperFacade().map(srcObj.getEsProrroga(), String.class));
        destObj.setFecInicioEspera(srcObj.getFecInicioEspera());
        destObj.setFecRespuesta(srcObj.getFecRespuesta());
        destObj.setIdRadicadoEntrada(srcObj.getIdRadicadoEntrada());
        destObj.setValDiasConcedidosProrroga(srcObj.getDiasConcedidosProrroga() == null ? null : srcObj.getDiasConcedidosProrroga().toString());
        destObj.setIdGestionAdministradoraTipo(srcObj.getGestionAdministradora() == null ? null : srcObj.getGestionAdministradora().getId().toString());
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setDescObservacionesSancion(srcObj.getDescObservacionesSancion());
        
        if (srcObj.getExpediente() != null
                && srcObj.getExpediente().getExpedienteDocumentoList() != null
                && !srcObj.getExpediente().getExpedienteDocumentoList().isEmpty()) {
            
            int cantidadDocumentosEncontrados = 0;
            
            for (ExpedienteDocumentoEcm expedienteDocumentoEcm : srcObj.getExpediente().getExpedienteDocumentoList()) {
                if (expedienteDocumentoEcm.getCodOrigen() != null) {
                    if (OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_ENTIDAD_VIGILANCIA.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        destObj.setIdDocumentoEntidadVigilancia(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    } else if (OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_PRORROGA.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        destObj.setIdDocumentoProrroga(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    } else if (OrigenDocumentoEnum.RESPUESTA_ADMINISTRADORA_ID_DOCUMENTO_REENVIO.getCode().equals(expedienteDocumentoEcm.getCodOrigen().getId())) {
                        destObj.setIdDocumentoReenvio(expedienteDocumentoEcm.getDocumentoEcm().getId());
                        cantidadDocumentosEncontrados++;
                    }
                }
                if (Constants.CANTIDAD_DOCUMENTOS_RESPUESTA_ADMINISTRADORA == cantidadDocumentosEncontrados) {
                    break;
                }
            }
        }
        return destObj;
    }
}
