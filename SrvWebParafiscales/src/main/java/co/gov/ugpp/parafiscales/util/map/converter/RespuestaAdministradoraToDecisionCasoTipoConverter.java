package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaAdministradoraDecisionCaso;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class RespuestaAdministradoraToDecisionCasoTipoConverter extends AbstractCustomConverter<RespuestaAdministradoraDecisionCaso, String> {

    public RespuestaAdministradoraToDecisionCasoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(RespuestaAdministradoraDecisionCaso srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(RespuestaAdministradoraDecisionCaso srcObj, String destObj) {
        destObj = (srcObj.getCodDecisionCaso()== null ? null : srcObj.getCodDecisionCaso().getId());
        return destObj;
    }

}
