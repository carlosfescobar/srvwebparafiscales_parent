package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Fiscalizacion;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoListaChequeo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.respuestarequerimientotipo.v1.RespuestaRequerimientoTipo;


/**
 *
 * @author zridrigu
 */
public class RespuestaRequerimientoInformacionTipoToRespuestaRequerimientoInformacionConverter extends AbstractBidirectionalConverter<RespuestaRequerimientoTipo, RespuestaRequerimientoInformacion> {

    public RespuestaRequerimientoInformacionTipoToRespuestaRequerimientoInformacionConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public RespuestaRequerimientoInformacion convertTo(RespuestaRequerimientoTipo srcObj) {
        RespuestaRequerimientoInformacion respuestaRequerimientoInformacion = new RespuestaRequerimientoInformacion();
        this.copy(srcObj, respuestaRequerimientoInformacion);
        return respuestaRequerimientoInformacion;
    }

    @Override
    public void copyTo(RespuestaRequerimientoTipo srcObj, RespuestaRequerimientoInformacion destObj) {
        destObj.setIdFiscalizacion(this.getMapperFacade().map(srcObj.getIdFiscalizacion(), Fiscalizacion.class));
        destObj.setIdRadicadoEntrada(srcObj.getIdRadicadoEntrada());
        destObj.setFechaRadicadoEntrada(srcObj.getFecRadicadoEntrada() == null ? null : srcObj.getFecRadicadoEntrada());

    }

    @Override
    public RespuestaRequerimientoTipo convertFrom(RespuestaRequerimientoInformacion srcObj) {
        RespuestaRequerimientoTipo respuestaRequerimientoInformacionTipo = new RespuestaRequerimientoTipo();
        this.copy(srcObj, respuestaRequerimientoInformacionTipo);
        return respuestaRequerimientoInformacionTipo;
    }

    @Override
    public void copyFrom(RespuestaRequerimientoInformacion srcObj, RespuestaRequerimientoTipo destObj) {
        destObj.setIdRespuestaRequerimientoInformacion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setIdFiscalizacion(this.getMapperFacade().map(srcObj.getIdFiscalizacion().getId(), String.class));
        destObj.setIdRadicadoEntrada(srcObj.getIdRadicadoEntrada());
        destObj.setFecRadicadoEntrada(srcObj.getFechaRadicadoEntrada() == null ? null : srcObj.getFechaRadicadoEntrada());
        destObj.getCodListadoChequeo().addAll(this.getMapperFacade().map(srcObj.getCodListadoChequeo(), RespuestaRequerimientoListaChequeo.class, String.class));

    }
}
