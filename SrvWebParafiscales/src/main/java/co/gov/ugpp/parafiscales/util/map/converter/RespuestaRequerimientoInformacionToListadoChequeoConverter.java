package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.RespuestaRequerimientoListaChequeo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class RespuestaRequerimientoInformacionToListadoChequeoConverter extends AbstractCustomConverter<RespuestaRequerimientoListaChequeo, String> {

    public RespuestaRequerimientoInformacionToListadoChequeoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(RespuestaRequerimientoListaChequeo srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(RespuestaRequerimientoListaChequeo srcObj, String destObj) {
        destObj = (srcObj.getCodListadoChequeo() == null ? null : srcObj.getCodListadoChequeo().getId());
        return destObj;
    }

}
