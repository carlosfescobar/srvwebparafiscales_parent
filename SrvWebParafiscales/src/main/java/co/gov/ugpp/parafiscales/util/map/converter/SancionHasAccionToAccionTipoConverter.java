package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SancionHasAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author jmuncab
 */
public class SancionHasAccionToAccionTipoConverter extends AbstractCustomConverter<SancionHasAccion, AccionTipo> {

    public SancionHasAccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AccionTipo convert(SancionHasAccion srcObj) {
        return copy(srcObj, new AccionTipo());
    }

    @Override
    public AccionTipo copy(SancionHasAccion srcObj, AccionTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAccion(), AccionTipo.class);
        return destObj;
    }

}
