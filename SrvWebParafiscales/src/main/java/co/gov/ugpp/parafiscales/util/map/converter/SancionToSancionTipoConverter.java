package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.AnalisisSancion;
import co.gov.ugpp.parafiscales.persistence.entity.SancionInformacionRequerida;
import co.gov.ugpp.parafiscales.persistence.entity.LiquidacionParcial;
import co.gov.ugpp.parafiscales.persistence.entity.SancionPruebaRequerida;
import co.gov.ugpp.parafiscales.persistence.entity.Sancion;
import co.gov.ugpp.parafiscales.persistence.entity.SancionHasAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;
import co.gov.ugpp.schema.sancion.sanciontipo.v1.SancionTipo;

/**
 *
 * @author jmuncab
 */
public class SancionToSancionTipoConverter extends AbstractCustomConverter<Sancion, SancionTipo> {

    public SancionToSancionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SancionTipo convert(Sancion srcObj) {
        return copy(srcObj, new SancionTipo());
    }

    @Override
    public SancionTipo copy(Sancion srcObj, SancionTipo destObj) {

        destObj.setIdSancion(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodProcesoPadre(this.getMapperFacade().map(srcObj.getCodProcesoPadre(), String.class));
        destObj.setIdProcesoPadre(srcObj.getIdProcesoPadre());
        destObj.setExpedienteCaso(this.getMapperFacade().map(srcObj.getExpedienteCaso(), ExpedienteTipo.class));
        destObj.setExpedienteSancion(this.getMapperFacade().map(srcObj.getExpedienteSancion(), ExpedienteTipo.class));
        destObj.setCodTipoSancion(this.getMapperFacade().map(srcObj.getCodTipoSancion(), String.class));
        destObj.setCodEstadoSancion(this.getMapperFacade().map(srcObj.getCodEstadoSancion(), String.class));
        destObj.setInvestigado(this.getMapperFacade().map(srcObj.getInvestigado(), ConceptoPersonaTipo.class));
        destObj.getInformacionRequerida().addAll(this.getMapperFacade().map(srcObj.getInformacionRequerida(), SancionInformacionRequerida.class, InformacionPruebaTipo.class));
        destObj.getPruebaRequerida().addAll(this.getMapperFacade().map(srcObj.getPruebaRequerida(), SancionPruebaRequerida.class, InformacionPruebaTipo.class));
        destObj.setEsSancion(this.getMapperFacade().map(srcObj.getEsSancion(), String.class));
        destObj.setDesObservacionesNoProcedeSancion(srcObj.getDesObservacionesNoProcedeSancion());
        destObj.getAnalisisSancion().addAll(this.getMapperFacade().map(srcObj.getAnalisisSancion(), AnalisisSancion.class, AnalisisSancionTipo.class));
        destObj.setPliegoCargosParcial(this.getMapperFacade().map(srcObj.getPliegoCargosParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoPliegoCargos(srcObj.getIdActoPliegoCargos() == null ? null : srcObj.getIdActoPliegoCargos().getId());
        destObj.setResolucionSancionParcial(this.getMapperFacade().map(srcObj.getResolucionSancionParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoResolucionSancion(srcObj.getIdActoResolucionSancion() == null ? null : srcObj.getIdActoResolucionSancion().getId());
        destObj.setAutoArchivoParcial(this.getMapperFacade().map(srcObj.getAutoArchivoParcial(), AprobacionDocumentoTipo.class));
        destObj.setIdActoAutoArchivo(srcObj.getIdActoAutoArchivo() == null ? null : srcObj.getIdActoAutoArchivo().getId());
        destObj.setIdRecursoReconsideracion(srcObj.getIdRecursoReconsideracion()== null ? null : srcObj.getIdRecursoReconsideracion().getId().toString());
        destObj.setIdConstanciaEjecutoria(srcObj.getIdConstanciaEjecutoria()== null ? null : srcObj.getIdConstanciaEjecutoria().getId().toString());
        destObj.getLiquidacionesParciales().addAll(this.getMapperFacade().map(srcObj.getLiquidacionesParciales(), LiquidacionParcial.class, LiquidacionParcialTipo.class));
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), SancionHasAccion.class, AccionTipo.class));
        destObj.setFecLlegadaExtemporaneaFiscalizacion(srcObj.getFecLlegadaExtemporaneaFiscalizacion());
        destObj.setCodDecisionExtemporaneaLiquidacion(this.getMapperFacade().map(srcObj.getCodDecisionExtemporaneaLiquidacion(), String.class));
        destObj.setDesObservacionDecisionExtemporanea(srcObj.getDesObservacionDecisionExtemporanea());

        if (srcObj.getGestorSancion() != null) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getGestorSancion());
            destObj.setGestorSancion(funcionarioTipo);
        }
        return destObj;

    }
}
