package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.Seguimiento;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;

/**
 *
 * @author rpadilla
 */
public class SeguimientoTipo2SeguimientoConverter extends AbstractBidirectionalConverter<SeguimientoTipo, Seguimiento> {

    public SeguimientoTipo2SeguimientoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Seguimiento convertTo(SeguimientoTipo srcObj) {
        Seguimiento seguimiento = new Seguimiento();
        this.copyTo(srcObj, seguimiento);
        return seguimiento;
    }

    @Override
    public void copyTo(SeguimientoTipo srcObj, Seguimiento destObj) {
        destObj.setCargoPersonaContactada(srcObj.getValCargoPersonaContactada());
        destObj.setDescObsProgramacion(srcObj.getDesObservacionProgramacionSeguimiento());
        destObj.setDescObservaciones(srcObj.getDescObservaciones());
        destObj.setValUsuarioEncargado(srcObj.getValEncargadoSeguimiento());
        destObj.setValPersonaContactada(srcObj.getValPersonaContactada());
        destObj.setPlazoEntrega(srcObj.getValPlazoEntrega());
        destObj.setFecProgramadaSeguimiento(srcObj.getFecProgramadaSeguimiento());
    }

    @Override
    public SeguimientoTipo convertFrom(Seguimiento srcObj) {
        SeguimientoTipo seguimientoTipo = new SeguimientoTipo();
        this.copyFrom(srcObj, seguimientoTipo);
        return seguimientoTipo;
    }

    @Override
    public void copyFrom(Seguimiento srcObj, SeguimientoTipo destObj) {
        destObj.setIdSeguimiento(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setIdExpediente(srcObj.getExpediente() == null? null: srcObj.getExpediente().getId());
        destObj.setDescObservaciones(srcObj.getDescObservaciones());
        destObj.setValPersonaContactada(srcObj.getValPersonaContactada());
        destObj.setValCargoPersonaContactada(srcObj.getCargoPersonaContactada());
        destObj.setValPlazoEntrega(srcObj.getPlazoEntrega());
        destObj.setFecProgramadaSeguimiento(srcObj.getFecProgramadaSeguimiento());
        destObj.setValEncargadoSeguimiento(srcObj.getValUsuarioEncargado());
        destObj.setDesObservacionProgramacionSeguimiento(srcObj.getDescObsProgramacion());
    }

}
