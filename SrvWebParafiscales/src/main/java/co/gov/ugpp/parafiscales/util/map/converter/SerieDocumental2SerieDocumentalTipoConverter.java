package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SerieDocumental;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;

/**
 *
 * @author jmuncab
 */
public class SerieDocumental2SerieDocumentalTipoConverter extends AbstractBidirectionalConverter<SerieDocumentalTipo, SerieDocumental> {

    public SerieDocumental2SerieDocumentalTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SerieDocumental convertTo(SerieDocumentalTipo srcObj) {
        SerieDocumental serieDocumental = new SerieDocumental();
        this.copyTo(srcObj, serieDocumental);
        return serieDocumental;
    }

    @Override
    public void copyTo(SerieDocumentalTipo srcObj, SerieDocumental destObj) {
        destObj.setCodSerie(srcObj.getCodSerie());
        destObj.setCodSubSerie(srcObj.getCodSubserie());
        destObj.setCodTipoDocumental(srcObj.getCodTipoDocumental());
        destObj.setNombreSerie(srcObj.getValNombreSerie());
        destObj.setNombreSubSerie(srcObj.getValNombreSubserie());
    }

    @Override
    public SerieDocumentalTipo convertFrom(SerieDocumental srcObj) {
        SerieDocumentalTipo serieDocumentalTipo = new SerieDocumentalTipo();
        this.copyFrom(srcObj, serieDocumentalTipo);
        return serieDocumentalTipo;
    }

    @Override
    public void copyFrom(SerieDocumental srcObj, SerieDocumentalTipo destObj) {
        destObj.setCodSerie(srcObj.getCodSerie());
        destObj.setCodSubserie(srcObj.getCodSubSerie());
        destObj.setCodTipoDocumental(srcObj.getCodTipoDocumental());
        destObj.setValNombreSerie(srcObj.getNombreSerie());
        destObj.setValNombreSubserie(srcObj.getNombreSubSerie());
    }

}
