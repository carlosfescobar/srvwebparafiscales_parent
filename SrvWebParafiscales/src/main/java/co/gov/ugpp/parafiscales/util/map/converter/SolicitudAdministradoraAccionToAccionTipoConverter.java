package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraAccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;

/**
 *
 * @author Everis
 */
public class SolicitudAdministradoraAccionToAccionTipoConverter extends AbstractCustomConverter<SolicitudAdministradoraAccion, AccionTipo> {

    public SolicitudAdministradoraAccionToAccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AccionTipo convert(SolicitudAdministradoraAccion srcObj) {
          return copy(srcObj, new AccionTipo());
    }

    @Override
    public AccionTipo copy(SolicitudAdministradoraAccion srcObj, AccionTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getAccion(), AccionTipo.class);
        return destObj;
    }

}
