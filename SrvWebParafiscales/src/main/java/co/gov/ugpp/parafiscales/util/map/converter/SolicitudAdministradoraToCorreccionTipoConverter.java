package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraCorreccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author Everis
 */
public class SolicitudAdministradoraToCorreccionTipoConverter extends AbstractCustomConverter<SolicitudAdministradoraCorreccion, String> {

    public SolicitudAdministradoraToCorreccionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(SolicitudAdministradoraCorreccion srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(SolicitudAdministradoraCorreccion srcObj, String destObj) {
        destObj = (srcObj.getDescSolicitudCorreccion() == null ? null : srcObj.getDescSolicitudCorreccion());
        return destObj;
    }

}
