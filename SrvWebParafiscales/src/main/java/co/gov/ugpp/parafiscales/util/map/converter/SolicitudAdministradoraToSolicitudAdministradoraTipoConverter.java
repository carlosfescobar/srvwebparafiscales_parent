package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionSubsistemaSolAdmin;
import co.gov.ugpp.parafiscales.persistence.entity.GestionSolAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradora;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraAccion;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudAdministradoraCorreccion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1.SolicitudAdministradoraTipo;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Everis
 */
public class SolicitudAdministradoraToSolicitudAdministradoraTipoConverter extends AbstractCustomConverter<SolicitudAdministradora, SolicitudAdministradoraTipo> {
    
    public SolicitudAdministradoraToSolicitudAdministradoraTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public SolicitudAdministradoraTipo convert(SolicitudAdministradora srcObj) {
        return copy(srcObj, new SolicitudAdministradoraTipo());
    }
    
    @Override
    public SolicitudAdministradoraTipo copy(SolicitudAdministradora srcObj, SolicitudAdministradoraTipo destObj) {
        destObj.setIdSolicitudAdministradoras(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setCodEstadoSolicitud(srcObj.getCodEstadoSolicitud() == null ? null : srcObj.getCodEstadoSolicitud().getId());
        destObj.setDescEstadoSolicitud(srcObj.getCodEstadoSolicitud() == null ? null : srcObj.getCodEstadoSolicitud().getNombre());
        destObj.setCodTipoSolicitud(srcObj.getCodTipoSolicitud() == null ? null : srcObj.getCodTipoSolicitud().getId());
        destObj.setDescTipoSolicitud(srcObj.getCodTipoSolicitud() == null ? null : srcObj.getCodTipoSolicitud().getNombre());
        destObj.setCodTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getId());
        destObj.setDescTipoRespuesta(srcObj.getCodTipoRespuesta() == null ? null : srcObj.getCodTipoRespuesta().getNombre());
        destObj.setCodTipoPeriodicidad(srcObj.getCodTipoPeriodicidad() == null ? null : srcObj.getCodTipoPeriodicidad().getId());
        destObj.setDescTipoPeriodicidad(srcObj.getCodTipoPeriodicidad() == null ? null : srcObj.getCodTipoPeriodicidad().getNombre());
        destObj.setCodTipoPlazoEntrega(srcObj.getCodTipoPlazoEntrega()== null ? null : srcObj.getCodTipoPlazoEntrega().getId());
        destObj.setDescTipoPlazoEntrega(srcObj.getCodTipoPlazoEntrega() == null ? null : srcObj.getCodTipoPlazoEntrega().getNombre());
        destObj.setDescInformacionRequerida(srcObj.getDescInformacionRequerida());
        destObj.setValCantidadPlazoEntrega(srcObj.getValCantidadPlazoEntrega());
        destObj.setValNombreSolicitud(srcObj.getValNombreSolicitud());
        destObj.setFecInicioSolicitud(srcObj.getFecInicioSolicitud());
        destObj.setFecFinSolicitud(srcObj.getFecFinSolicitud());
        destObj.setEsNecesitaCorreccion(this.getMapperFacade().map(srcObj.getEsNecesitaCorreccion(), String.class));
        destObj.setEsSolicitudAceptada(this.getMapperFacade().map(srcObj.getEsSolicitudAceptada(), String.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setValidacionEstructura(this.getMapperFacade().map(srcObj.getValidacionEstructura(), ValidacionEstructuraTipo.class));
        destObj.getAdministradorasDefinitivas().addAll(this.getMapperFacade().map(srcObj.getAdministradorasDefinitivas(), GestionSolAdministradora.class, GestionAdministradoraTipo.class));
        destObj.getAdministradorasSeleccionadas().addAll(this.getMapperFacade().map(srcObj.getAdministradorasSeleccionadas(), AgrupacionSubsistemaSolAdmin.class, AgrupacionSubsistemaTipo.class));
        destObj.getDescSolicitudCorreccion().addAll(this.getMapperFacade().map(srcObj.getSolicitudesCorreccion(), SolicitudAdministradoraCorreccion.class, String.class));
        destObj.getAcciones().addAll(this.getMapperFacade().map(srcObj.getAcciones(), SolicitudAdministradoraAccion.class, AccionTipo.class));
        destObj.setIdArchivoSolicitudInicial(srcObj.getIdArchivoSolicitudInicial() == null ? null : srcObj.getIdArchivoSolicitudInicial().getId().toString());
        
          if (StringUtils.isNotBlank(srcObj.getIdFuncionarioSolicitud())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getIdFuncionarioSolicitud());
            destObj.setIdFuncionarioSolicitud(funcionarioTipo);
        }
        
        
        return destObj;
    }
}
