package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudArchivoAnexo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;

/**
 *
 * @author jmuncab
 */
public class SolicitudArchivoAnexoToArchivoTipoConverter extends AbstractCustomConverter<SolicitudArchivoAnexo, ArchivoTipo> {

    public SolicitudArchivoAnexoToArchivoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ArchivoTipo convert(SolicitudArchivoAnexo srcObj) {
        return copy(srcObj, new ArchivoTipo());
    }

    @Override
    public ArchivoTipo copy(SolicitudArchivoAnexo srcObj, ArchivoTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getArchivo(), ArchivoTipo.class);
        return destObj;
    }
}
