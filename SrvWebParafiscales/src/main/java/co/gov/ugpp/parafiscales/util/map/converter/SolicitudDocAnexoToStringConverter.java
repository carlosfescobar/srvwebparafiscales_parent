package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudDocAnexo;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class SolicitudDocAnexoToStringConverter extends AbstractCustomConverter<SolicitudDocAnexo, String> {

    public SolicitudDocAnexoToStringConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(SolicitudDocAnexo srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(SolicitudDocAnexo srcObj, String destObj) {        
        destObj = srcObj.getDocAnexo();
        return destObj;
    }
}
