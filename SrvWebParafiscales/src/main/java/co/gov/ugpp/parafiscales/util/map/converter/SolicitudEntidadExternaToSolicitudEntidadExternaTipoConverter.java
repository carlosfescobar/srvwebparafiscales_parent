package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudEntidadExterna;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1.SolicitudEntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author jmuncab
 */
public class SolicitudEntidadExternaToSolicitudEntidadExternaTipoConverter
    extends AbstractCustomConverter<SolicitudEntidadExterna, SolicitudEntidadExternaTipo> {

    public SolicitudEntidadExternaToSolicitudEntidadExternaTipoConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudEntidadExternaTipo convert(final SolicitudEntidadExterna srcObj) {
        return copy(srcObj, new SolicitudEntidadExternaTipo());
    }

    @Override
    public SolicitudEntidadExternaTipo copy(final SolicitudEntidadExterna srcObj, final SolicitudEntidadExternaTipo destObj) {

        destObj.setCodTipoEstado(srcObj.getCodTipoEstado() == null ? null : srcObj.getCodTipoEstado().getId());
        destObj.setEsValidaRespuesta(srcObj.getEsValidaRespuesta() == null ? null : srcObj.getEsValidaRespuesta().toString());
        destObj.setFecRadicadoRespuesta(srcObj.getFecRadicadoRespuesta());
        destObj.setIdRadicadoRespuesta(srcObj.getNumeroRadicadoRespuesta() == null ? null : srcObj.getNumeroRadicadoRespuesta().toString());
        destObj.setIdSolicitudEntidadExterna(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setValNumeroReiteracion(srcObj.getValNumeroReiteracion() == null ? null : srcObj.getValNumeroReiteracion().toString());
        destObj.setValObservacionesRespuesta(srcObj.getValObservacionesRespuesta());              
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class));
        
        return destObj;
    }
}
