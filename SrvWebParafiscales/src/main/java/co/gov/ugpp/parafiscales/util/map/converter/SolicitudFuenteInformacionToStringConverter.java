package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudFuenteInformacion;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author jmuncab
 */
public class SolicitudFuenteInformacionToStringConverter extends AbstractCustomConverter<SolicitudFuenteInformacion, String> {

    public SolicitudFuenteInformacionToStringConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(SolicitudFuenteInformacion srcObj) {
        return copy(srcObj, new String());
    }

    @Override
    public String copy(SolicitudFuenteInformacion srcObj, String destObj) {
        destObj = srcObj.getCodFuenteInformacion() == null ? null : srcObj.getCodFuenteInformacion().getId();
        return destObj;
    }
}
