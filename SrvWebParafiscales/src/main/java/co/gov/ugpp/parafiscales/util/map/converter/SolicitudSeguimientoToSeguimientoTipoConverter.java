package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SolicitudSeguimiento;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;

/**
 *
 * @author jmuncab
 */
public class SolicitudSeguimientoToSeguimientoTipoConverter extends AbstractCustomConverter<SolicitudSeguimiento, SeguimientoTipo> {

    public SolicitudSeguimientoToSeguimientoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SeguimientoTipo convert(SolicitudSeguimiento srcObj) {
        return copy(srcObj, new SeguimientoTipo());
    }

    @Override
    public SeguimientoTipo copy(SolicitudSeguimiento srcObj, SeguimientoTipo destObj) {

        destObj = this.getMapperFacade().map(srcObj.getSeguimiento(), SeguimientoTipo.class);
        return destObj;

    }
}
