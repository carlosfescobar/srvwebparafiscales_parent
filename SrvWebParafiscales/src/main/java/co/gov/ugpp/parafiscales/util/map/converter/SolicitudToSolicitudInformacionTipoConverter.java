package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudCotizante;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudDocAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPago;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPlanilla;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudSeguimiento;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1.PagoTipo;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author rpadilla
 */
public class SolicitudToSolicitudInformacionTipoConverter extends AbstractCustomConverter<Solicitud, SolicitudInformacionTipo> {

    public SolicitudToSolicitudInformacionTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudInformacionTipo convert(Solicitud source) {
        return copy(source, new SolicitudInformacionTipo());
    }

    @Override
    public SolicitudInformacionTipo copy(Solicitud source, SolicitudInformacionTipo destination) {

        if (source.isUseOnlySpecifiedFields()) {
            this.mapUseSpecifiedFields(source, destination);
        } else {
            //AtributosPropios de Solicitud
            final String vFechaSolicitud = DateUtil.parseCalendarToStrDate(source.getFecSolicitud());
            destination.setFecSolicitud(DateUtil.parseStrDateTimeToCalendar(vFechaSolicitud));
            destination.setIdSolicitud(source.getId() == null ? null : source.getId().toString());
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(source.getValUsuario());
            destination.setSolicitante(funcionarioTipo);
            destination.setCodTipoSolicitud(this.getMapperFacade().map(source.getCodTipoSolicitud(), String.class));
            destination.setCodMedioSolictud(this.getMapperFacade().map(source.getCodMedio(), String.class));
            destination.setCodEstadoSolicitud(this.getMapperFacade().map(source.getCodEstadoSolicitud(), String.class));
            destination.setEsAprobarSolicitud(this.getMapperFacade().map(source.getEsAprobarSolicitud(), String.class));
            destination.setExpediente(this.getMapperFacade().map(source.getExpediente(), ExpedienteTipo.class));
            //Atributos propios de SolicitudInform
            destination.getSeguimiento().addAll(this.getMapperFacade().map(source.getSolicitudSeguimientoList(), SolicitudSeguimiento.class, SeguimientoTipo.class));
            destination.setCodTipoConsulta(this.getMapperFacade().map(source.getCodTipoConsulta(), String.class));
            destination.setEsConsultaNoAportante(this.getMapperFacade().map(source.getEsConsultaNoAportante(), String.class));
            destination.setNoAportante(this.getMapperFacade().map(source.getNoAportante(), NoAportanteTipo.class));
            destination.setAportante(this.getMapperFacade().map(source.getAportante(), AportanteTipo.class));
            destination.getCotizantes().addAll(this.getMapperFacade().map(source.getSoliciutdCotizanteList(), SolicitudCotizante.class, CotizanteTipo.class));
            destination.getPlanillas().addAll(this.getMapperFacade().map(source.getSolicitudPlanillaList(), SolicitudPlanilla.class, PlanillaTipo.class));
            destination.getPagos().addAll(this.getMapperFacade().map(source.getSolicitudPagoList(), SolicitudPago.class, PagoTipo.class));
            destination.setDocsSolicitudAportante(this.getMapperFacade().map(source.getDocSolicitudAportante(), ArchivoTipo.class));
            destination.getIdDocumentoAnexo().addAll(this.getMapperFacade().map(source.getSolicitudDocAnexoList(), SolicitudDocAnexo.class, String.class));
            destination.setEsConsultaAprobada(this.getMapperFacade().map(source.getEsConsultaAprobada(), String.class));
            destination.setDescObservacion(source.getDescObservacion());
            destination.setDescObservacionAprobada(source.getDescObservacionAprobada());
            destination.setEntidadExterna(this.getMapperFacade().map(source.getEntidadExterna(), EntidadExternaTipo.class));
            destination.setIdRadicadoEntrada(source.getRadicadoEntrada() == null ? null : source.getRadicadoEntrada().getId());
            destination.setIdRadicadoSalida(source.getIdRadicadoSalida());
            destination.setDescObservacionNoAportante(source.getDescObserNoAportante());
            if (source.getDocumentosSolicitud() != null
                    && !source.getDocumentosSolicitud().isEmpty()) {

                int cantidadDocumentosEncontrados = 0;

                for (SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm : source.getDocumentosSolicitud()) {
                    DocumentoEcm documentoEcm = solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getDocumentoEcm();
                    if (solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen() != null
                            && StringUtils.isNotBlank(solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId())) {
                        if (OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION.getCode().equals(solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId())) {
                            destination.setDocSolicitudInformacion(this.getMapperFacade().map(documentoEcm, DocumentoTipo.class));
                            cantidadDocumentosEncontrados++;
                        } else if (OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION.getCode().equals(solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId())) {
                            destination.setDocSolicitudReiteracion(this.getMapperFacade().map(documentoEcm, DocumentoTipo.class));
                            cantidadDocumentosEncontrados++;
                        }
                    }
                    if (Constants.CANTIDAD_DOCUMENTOS_SOLICITUD == cantidadDocumentosEncontrados) {
                        break;
                    }
                }
            }
        }
        return destination;
    }

    private void mapUseSpecifiedFields(Solicitud source, SolicitudInformacionTipo destination) {

        destination.setIdSolicitud(source.getId() == null ? null : source.getId().toString());

        if (source.getEntidadExterna() != null) {

            EntidadExternaTipo entidadExterna = new EntidadExternaTipo();
            IdentificacionTipo identificacionTipo = new IdentificacionTipo();

            identificacionTipo.setValNumeroIdentificacion(source.getEntidadExterna().getPersona().getValNumeroIdentificacion());
            identificacionTipo.setCodTipoIdentificacion(source.getEntidadExterna().getPersona().getCodTipoIdentificacion() == null ? null : source.getEntidadExterna().getPersona().getCodTipoIdentificacion().getId());

            entidadExterna.setValNombreRazonSocial(source.getEntidadExterna().getPersona().getValNombreRazonSocial());
            entidadExterna.setIdPersona(identificacionTipo);

            destination.setEntidadExterna(entidadExterna);
        }
        destination.setSolicitante(null);
        destination.setCodTipoSolicitud(this.getMapperFacade().map(source.getCodTipoSolicitud(), String.class));
        destination.setCodTipoConsulta(this.getMapperFacade().map(source.getCodTipoConsulta(), String.class));
        destination.setCodMedioSolictud(this.getMapperFacade().map(source.getCodMedio(), String.class));
        final String vFechaSolicitud = DateUtil.parseCalendarToStrDate(source.getFecSolicitud());
        destination.setFecSolicitud(DateUtil.parseStrDateTimeToCalendar(vFechaSolicitud));
    }
}
