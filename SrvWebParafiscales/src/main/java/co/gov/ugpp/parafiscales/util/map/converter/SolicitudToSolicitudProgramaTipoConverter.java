package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudArchivoAnexo;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudFuenteInformacion;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudSeguimiento;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1.SolicitudProgramaTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class SolicitudToSolicitudProgramaTipoConverter extends AbstractCustomConverter<Solicitud, SolicitudProgramaTipo> {

    public SolicitudToSolicitudProgramaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudProgramaTipo convert(Solicitud srcObj) {
        return copy(srcObj, new SolicitudProgramaTipo());
    }

    @Override
    public SolicitudProgramaTipo copy(Solicitud srcObj, SolicitudProgramaTipo destObj) {

        final String vFechaSolicitud = DateUtil.parseCalendarToStrDate(srcObj.getFecSolicitud());
        destObj.setFecSolicitud(DateUtil.parseStrDateTimeToCalendar(vFechaSolicitud));
        destObj.setIdSolicitud(srcObj.getId() == null ? null : srcObj.getId().toString());
        FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
        funcionarioTipo.setIdFuncionario(srcObj.getValUsuario());
        destObj.setSolicitante(funcionarioTipo);
        destObj.setCodTipoSolicitud(this.getMapperFacade().map(srcObj.getCodTipoSolicitud(), String.class));
        destObj.setCodMedioSolictud(this.getMapperFacade().map(srcObj.getCodMedio(), String.class));
        destObj.setCodEstadoSolicitud(this.getMapperFacade().map(srcObj.getCodEstadoSolicitud(), String.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));
        destObj.setValResumen(srcObj.getValResumen());
        destObj.setValFiltros(srcObj.getDescFiltro());
        destObj.setValCruces(srcObj.getDescCruces());
        destObj.setValResultadoEsperado(srcObj.getDescResultadoEsperado());
        destObj.setValRutaArchivoRequerimiento(srcObj.getValRutaArchivoResultado());
        destObj.setValObservacionRequerimiento(srcObj.getVlaObservacionRequerimiento());
        destObj.setIdRadicadoEntrada(srcObj.getRadicadoEntrada() == null ? null : srcObj.getRadicadoEntrada().getId());
        destObj.setIdRadicadoSalida(srcObj.getIdRadicadoSalida());
        destObj.getCodFuenteInformacion().addAll(this.getMapperFacade().map(srcObj.getSolicitudFuenteInformacionList(), SolicitudFuenteInformacion.class, String.class));
        destObj.getDocAnexos().addAll(this.getMapperFacade().map(srcObj.getSolicitudArchivoAnexoList(), SolicitudArchivoAnexo.class, ArchivoTipo.class));
        destObj.setEsAprobarSolicitud(this.getMapperFacade().map(srcObj.getEsAprobarSolicitud(), String.class));
        destObj.setValPorcentajeEfectividad(srcObj.getValPorcentajeEjectividad());
        destObj.getSeguimiento().addAll(this.getMapperFacade().map(srcObj.getSolicitudSeguimientoList(), SolicitudSeguimiento.class, SeguimientoTipo.class));

        if (srcObj.getDocumentosSolicitud() != null
                && !srcObj.getDocumentosSolicitud().isEmpty()) {
            for (SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm : srcObj.getDocumentosSolicitud()) {
                DocumentoEcm documentoEcm = solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getDocumentoEcm();
                if (solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen() != null
                        && StringUtils.isNotBlank(solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId())) {
                    if (solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId().equals(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION.getCode())) {
                        destObj.setDocSolicitudInformacion(this.getMapperFacade().map(documentoEcm, DocumentoTipo.class));
                    } else if (solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm().getCodOrigen().getId().equals(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION.getCode())) {
                        destObj.setDocSolicitudReiteracion(this.getMapperFacade().map(documentoEcm, DocumentoTipo.class));
                    }
                }
            }
        }

        return destObj;
    }
}
