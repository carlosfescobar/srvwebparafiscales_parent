package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jmuncab
 */
public class SolicitudToSolicitudTipoConverter extends AbstractCustomConverter<Solicitud, SolicitudTipo> {

    public SolicitudToSolicitudTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public SolicitudTipo convert(Solicitud srcObj) {
        return copy(srcObj, new SolicitudTipo());
    }

    @Override
    public SolicitudTipo copy(Solicitud srcObj, SolicitudTipo destObj) {
        destObj.setIdSolicitud(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setFecSolicitud(srcObj.getFecSolicitud());
        destObj.setCodTipoSolicitud(this.getMapperFacade().map(srcObj.getCodTipoSolicitud(), String.class));
        destObj.setCodMedioSolictud(this.getMapperFacade().map(srcObj.getCodMedio(), String.class));
        destObj.setCodEstadoSolicitud(this.getMapperFacade().map(srcObj.getCodEstadoSolicitud(), String.class));
        destObj.setEsAprobarSolicitud(this.getMapperFacade().map(srcObj.getEsConsultaAprobada(), String.class));
        destObj.setExpediente(this.getMapperFacade().map(srcObj.getExpediente(), ExpedienteTipo.class));

        if (StringUtils.isNotBlank(srcObj.getValUsuario())) {
            FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
            funcionarioTipo.setIdFuncionario(srcObj.getValUsuario());
            destObj.setSolicitante(funcionarioTipo);
        }
        return destObj;
    }
}
