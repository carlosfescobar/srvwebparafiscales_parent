package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class String2BooleanConverter extends AbstractBidirectionalConverter<String, Boolean> {

    public String2BooleanConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Boolean convertTo(final String srcObj) {
        return srcObj == null ? null : Boolean.valueOf(srcObj);
    }

    @Override
    public String convertFrom(final Boolean srcObj) {
        return srcObj == null ? null : srcObj.toString();
    }

    @Override
    public void copyTo(final String srcObj, final Boolean destObj) {
         throw new UnsupportedOperationException("Operacion no soportada");
    }

    @Override
    public void copyFrom(final Boolean srcObj, final String destObj) {
        throw new UnsupportedOperationException("Operacion no soportada");
    }    
}
