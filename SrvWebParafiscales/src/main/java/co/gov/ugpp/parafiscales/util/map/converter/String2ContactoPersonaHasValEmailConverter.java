package co.gov.ugpp.parafiscales.util.map.converter;

//package co.gov.ugpp.parafiscales.procesos.util.map.converter;
//
//import co.gov.ugpp.parafiscales.procesos.persistence.entity.ContactoPersonaHasValEmail;
//import co.gov.ugpp.parafiscales.procesos.persistence.entity.FormatoPK;
//
///**
// *
// * @author rpadilla
// */
//public class String2ContactoPersonaHasValEmailConverter extends BidirectionalConverter<String, ContactoPersonaHasValEmail> {
//
//    @Override
//    public ContactoPersonaHasValEmail convertTo(final String source, final Class<ContactoPersonaHasValEmail> destinationType) {
//        final ContactoPersonaHasValEmail contactoPersonaHasValEmail = new ContactoPersonaHasValEmail();
//        final FormatoPK contactoPersonaHasValEmailPK = new FormatoPK();
//        contactoPersonaHasValEmailPK.setValEmail(source);
//        contactoPersonaHasValEmail.setContactoPersonaHasValEmailPK(contactoPersonaHasValEmailPK);
//        return contactoPersonaHasValEmail;
//    }
//
//    @Override
//    public String convertFrom(final ContactoPersonaHasValEmail source, final Class<String> destinationType) {
//        if (source == null || source.getContactoPersonaHasValEmailPK() == null) {
//            return null;
//        }
//        return source.getContactoPersonaHasValEmailPK().getValEmail();
//    }
//
//}
