package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class String2CorreoElectronicoConverter extends AbstractBidirectionalConverter<String, CorreoElectronico> {

    public String2CorreoElectronicoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public CorreoElectronico convertTo(String srcObj) {
        CorreoElectronico correoElectronico = new CorreoElectronico();
        this.copyTo(srcObj, correoElectronico);
        return correoElectronico;
    }

    @Override
    public void copyTo(String srcObj, CorreoElectronico destObj) {
        destObj.setValCorreoElectronico(srcObj);
    }
    
    @Override
    public String convertFrom(CorreoElectronico srcObj) {
        if (srcObj == null) {
            return null;
        }
        return srcObj.getValCorreoElectronico();
    }    

    @Override
    public void copyFrom(CorreoElectronico srcObj, String destObj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
