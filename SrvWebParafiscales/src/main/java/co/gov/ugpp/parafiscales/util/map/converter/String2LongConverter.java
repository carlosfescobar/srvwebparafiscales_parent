package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class String2LongConverter extends AbstractBidirectionalConverter<String, Long> {

    public String2LongConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Long convertTo(String srcObj) {
        return srcObj == null ? null : Long.valueOf(srcObj);
    }

    @Override
    public String convertFrom(Long srcObj) {
        return srcObj == null ? null : srcObj.toString();
    }

    @Override
    public void copyTo(String srcObj, Long destObj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void copyFrom(Long srcObj, String destObj) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
