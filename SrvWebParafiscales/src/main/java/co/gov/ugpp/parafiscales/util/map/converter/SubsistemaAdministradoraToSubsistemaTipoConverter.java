package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.SubsistemaAdministradora;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author zrodrigu
 */
public class SubsistemaAdministradoraToSubsistemaTipoConverter extends AbstractCustomConverter<SubsistemaAdministradora, EntidadExternaTipo> {

    public SubsistemaAdministradoraToSubsistemaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public EntidadExternaTipo convert(SubsistemaAdministradora srcObj) {
        return copy(srcObj, new EntidadExternaTipo());
    }

    @Override
    public EntidadExternaTipo copy(SubsistemaAdministradora srcObj, EntidadExternaTipo destObj) {
        destObj = this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class);

        return destObj;
    }

}
