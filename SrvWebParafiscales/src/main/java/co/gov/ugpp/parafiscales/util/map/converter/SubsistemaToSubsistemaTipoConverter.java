package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Subsistema;
import co.gov.ugpp.parafiscales.persistence.entity.SubsistemaAdministradora;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;

/**
 *
 * @author zrodrigu
 */
public class SubsistemaToSubsistemaTipoConverter extends AbstractBidirectionalConverter<SubsistemaTipo, Subsistema> {
    
    public SubsistemaToSubsistemaTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public Subsistema convertTo(SubsistemaTipo srcObj) {
        Subsistema subsistema = new Subsistema();
        this.copyTo(srcObj, subsistema);
        return subsistema;
    }
    
    @Override
    public void copyTo(SubsistemaTipo srcObj, Subsistema destObj) {
        destObj.setEntidadVigilanciaControl(this.getMapperFacade().map(srcObj.getEntidadVigilanciaControl(), Persona.class));
        destObj.getAdministradoras().addAll(this.getMapperFacade().map(srcObj.getAdministradoras(), EntidadExternaTipo.class, SubsistemaAdministradora.class));
    }
    
    @Override
    public SubsistemaTipo convertFrom(Subsistema srcObj) {
        SubsistemaTipo subsistemaTipo = new SubsistemaTipo();
        this.copyFrom(srcObj, subsistemaTipo);
        return subsistemaTipo;
    }
    
    @Override
    public void copyFrom(Subsistema srcObj, SubsistemaTipo destObj) {
        destObj.setIdSubsistema(srcObj.getId() == null ? null : srcObj.getId());
        destObj.setEntidadVigilanciaControl(this.getMapperFacade().map(srcObj.getEntidadVigilanciaControl(), PersonaJuridicaTipo.class));
        destObj.getAdministradoras().addAll(this.getMapperFacade().map(srcObj.getAdministradoras(), SubsistemaAdministradora.class, EntidadExternaTipo.class));
    }
}
