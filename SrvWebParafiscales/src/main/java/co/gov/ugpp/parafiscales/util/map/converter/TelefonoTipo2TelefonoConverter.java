package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class TelefonoTipo2TelefonoConverter extends AbstractBidirectionalConverter<TelefonoTipo, Telefono> {
    
    public TelefonoTipo2TelefonoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }
    
    @Override
    public Telefono convertTo(TelefonoTipo srcObj) {
        Telefono telefono = new Telefono();
        this.copyTo(srcObj, telefono);
        return telefono;
    }
    
    @Override
    public void copyTo(TelefonoTipo srcObj, Telefono destObj) {
        destObj.setValNumero(srcObj.getValNumeroTelefono() == null ? null : srcObj.getValNumeroTelefono().toString());
        
    }
    
    @Override
    public TelefonoTipo convertFrom(Telefono srcObj) {
        TelefonoTipo telefonoTipo = new TelefonoTipo();
        this.copyFrom(srcObj, telefonoTipo);
        return telefonoTipo;
    }
    
    @Override
    public void copyFrom(Telefono srcObj, TelefonoTipo destObj) {
        destObj.setCodTipoTelefono(srcObj.getCodTipoTelefono() == null ? null : srcObj.getCodTipoTelefono().getId());
        destObj.setValNumeroTelefono(srcObj.getValNumero() == null ? null : srcObj.getValNumero());
//        destObj.setValNumeroTelefono(Long.valueOf(srcObj.getValNumero()));
    }
    
}
