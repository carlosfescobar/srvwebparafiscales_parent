package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Traslado;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.denuncias.trasladotipo.v1.TrasladoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;

/**
 *
 * @author zrodriguez
 */
public class TrasladoToTrasladoTipoConverter extends AbstractBidirectionalConverter<TrasladoTipo, Traslado> {

    public TrasladoToTrasladoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Traslado convertTo(TrasladoTipo srcObj) {
        Traslado traslado = new Traslado();
        this.copyTo(srcObj, traslado);
        return traslado;
    }

    @Override
    public void copyTo(TrasladoTipo srcObj, Traslado destObj) {
        destObj.setIdNumeroExpediente(this.getMapperFacade().map(srcObj.getExpediente(), String.class));
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExterna.class));
        destObj.setFuncionarioId(this.getMapperFacade().map(srcObj.getFuncionario(), String.class));
        destObj.setDescMotivoTraslado(srcObj.getDescMotivoTraslado());
        destObj.setFecTraslado(srcObj.getFecTraslado());
    }

    @Override
    public TrasladoTipo convertFrom(Traslado srcObj) {
        TrasladoTipo trasladoTipo = new TrasladoTipo();
        this.copyFrom(srcObj, trasladoTipo);
        return trasladoTipo;
    }

    @Override
    public void copyFrom(Traslado srcObj, TrasladoTipo destObj) {
        destObj.setIdTraslado(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setEntidadExterna(this.getMapperFacade().map(srcObj.getEntidadExterna(), EntidadExternaTipo.class));
        destObj.setDescMotivoTraslado(srcObj.getDescMotivoTraslado());
        destObj.setCodEstadoTraslado(srcObj.getEstadoTraslado());
        destObj.setFecTraslado(srcObj.getFecTraslado());
        destObj.setIdDocumentoTrasladoDenunciante(srcObj.getDocumentoTrasladoDenunciante() == null ? null : srcObj.getDocumentoTrasladoDenunciante().getId());
        destObj.setIdDocumentoTrasladoEntidadExterna(srcObj.getDocumentoTrasladoEntidadExterna() == null ? null : srcObj.getDocumentoTrasladoEntidadExterna().getId());
        destObj.setDenuncia(this.getMapperFacade().map(srcObj.getDenuncia(), DenunciaTipo.class));
        destObj.setIdRadicadoDocumentoEntidadExterna(srcObj.getIdRadicadoDocumentoEntidadExterna());
        destObj.setFechaRadicadoDocumentoEntidadExterna(srcObj.getFechaRadicadoDocumentoEntidadExterna()==null ? null: srcObj.getFechaRadicadoDocumentoEntidadExterna());

        ExpedienteTipo expedienteTipo = new ExpedienteTipo();
        expedienteTipo.setIdNumExpediente(srcObj.getIdNumeroExpediente());
        destObj.setExpediente(expedienteTipo);

        FuncionarioTipo funcionarioTipo = new FuncionarioTipo();
        funcionarioTipo.setIdFuncionario(srcObj.getFuncionarioId());
        destObj.setFuncionario(funcionarioTipo);
    }
}
