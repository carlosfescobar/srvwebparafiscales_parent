package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.EventoActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;
import co.gov.ugpp.schema.fiscalizacion.trazabilidadactoadministrativotipo.v1.TrazabilidadActoAdministrativoTipo;

/**
 *
 * @author jmunocab
 */
public class TrazabilidadActoAdministrativoTipo2TrazabilidadActoAdministrativoConverter extends AbstractBidirectionalConverter<TrazabilidadActoAdministrativoTipo, TrazabilidadActoAdministrativo> {

    public TrazabilidadActoAdministrativoTipo2TrazabilidadActoAdministrativoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public TrazabilidadActoAdministrativo convertTo(TrazabilidadActoAdministrativoTipo srcObj) {
        TrazabilidadActoAdministrativo accionActoAdministrativo = new TrazabilidadActoAdministrativo();
        this.copy(srcObj, accionActoAdministrativo);
        return accionActoAdministrativo;
    }

    @Override
    public void copyTo(TrazabilidadActoAdministrativoTipo srcObj, TrazabilidadActoAdministrativo destObj) {
    }

    @Override
    public TrazabilidadActoAdministrativoTipo convertFrom(TrazabilidadActoAdministrativo srcObj) {
        TrazabilidadActoAdministrativoTipo trazabilidadActoAdministrativoTipo = new TrazabilidadActoAdministrativoTipo();
        this.copyFrom(srcObj, trazabilidadActoAdministrativoTipo);
        return trazabilidadActoAdministrativoTipo;
    }

    @Override
    public void copyFrom(TrazabilidadActoAdministrativo srcObj, TrazabilidadActoAdministrativoTipo destObj) {

        destObj.setIdActoAdministrativo(srcObj.getId());
        destObj.setCodTipoNotificacionDefinitiva(srcObj.getCodTipoNotificacionDefinitiva() == null ? null : srcObj.getCodTipoNotificacionDefinitiva().getId());
        destObj.setDescTipoNotificacionDefinitiva(srcObj.getCodTipoNotificacionDefinitiva() == null ? null : srcObj.getCodTipoNotificacionDefinitiva().getNombre());
        destObj.setFecNotificacionDefinitiva(srcObj.getFecNotificacionDefinitiva());
        destObj.setFecRadicadoSalidaDefinitivo(srcObj.getFecRadicadoSalidaDefinitivo());
        destObj.setIdRadicadoSalidaDefinitivo(srcObj.getIdRadicadoSalidaDefinitivo());
        destObj.setValDiasProrroga(srcObj.getValDiasProrroga());
        destObj.getEnvioActoAdministrativo().addAll(this.getMapperFacade().map(srcObj.getEnvioActoAdministrativoList(), EventoActoAdministrativo.class, EnvioActoAdministrativoTipo.class));
        destObj.setUbicacionDefinitiva(this.getMapperFacade().map(srcObj.getUbicacionDefinitiva(), UbicacionPersonaTipo.class));
        if (srcObj.getRadicacionesEntrada() != null
                && srcObj.getRadicacionesEntrada().isEmpty()) {
            destObj.setFecRadicadoEntrada(srcObj.getRadicacionesEntrada().get(Constants.CANTIDAD_RADICADOS_TRAZABILIDAD_ACTO).getFechaRadicado());
            destObj.setIdRadicadoEntrada(srcObj.getRadicacionesEntrada().get(Constants.CANTIDAD_RADICADOS_TRAZABILIDAD_ACTO).getIdRadicado());
        }

    }
}
