package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.EventoActoAdministrativo;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;

/**
 *
 * @author jmuncab
 */
public class TrazabilidadEnvioActoToEnvioActoAdministrativoTipoConverter extends AbstractBidirectionalConverter<EnvioActoAdministrativoTipo, EventoActoAdministrativo> {

    public TrazabilidadEnvioActoToEnvioActoAdministrativoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public EventoActoAdministrativo convertTo(EnvioActoAdministrativoTipo srcObj) {
        EventoActoAdministrativo trazabilidadEnvioActo = new EventoActoAdministrativo();
        this.copyTo(srcObj, trazabilidadEnvioActo);
        return trazabilidadEnvioActo;
    }

    @Override
    public void copyTo(EnvioActoAdministrativoTipo srcObj, EventoActoAdministrativo destObj) {

        destObj.setFecRadicadoSalida(DateUtil.parseStrDateTimeToCalendar(srcObj.getFecRadicadosSalida()));
        destObj.setIdRadicadoSalida(srcObj.getIdRadicadosSalida());
        destObj.setDescCausalDevolucion(srcObj.getDescCausalDevolucion());
    }

    @Override
    public EnvioActoAdministrativoTipo convertFrom(EventoActoAdministrativo srcObj) {
        EnvioActoAdministrativoTipo envioActoAdministrativoTipo = new EnvioActoAdministrativoTipo();
        this.copyFrom(srcObj, envioActoAdministrativoTipo);
        return envioActoAdministrativoTipo;
    }

    @Override
    public void copyFrom(EventoActoAdministrativo srcObj, EnvioActoAdministrativoTipo destObj) {

        destObj.setIdRadicadosSalida(srcObj.getIdRadicadoSalida());
        destObj.setFecRadicadosSalida(DateUtil.parseCalendarToStrDate(srcObj.getFecRadicadoSalida()));
        destObj.setCodEstadoNotificacion(srcObj.getCodEstadNotificacion() == null ? null : srcObj.getCodEstadNotificacion().getId());
        destObj.setDescEstadoNotificacion(srcObj.getCodEstadNotificacion() == null ? null : srcObj.getCodEstadNotificacion().getNombre());
        destObj.setDescCausalDevolucion(srcObj.getDescCausalDevolucion());
        destObj.setUbicacionNotificada(this.getMapperFacade().map(srcObj.getUbicacionNotificada(), UbicacionPersonaTipo.class));

    }
}
