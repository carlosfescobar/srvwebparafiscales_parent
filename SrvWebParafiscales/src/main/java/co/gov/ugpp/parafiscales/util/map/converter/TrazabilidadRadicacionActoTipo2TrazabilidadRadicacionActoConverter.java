package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.EventoActoAdministrativo;
import co.gov.ugpp.parafiscales.persistence.entity.RadicadoTrazabilidad;
import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadActoAdministrativo;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;
import co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1.RadicadoTipo;
import co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1.TrazabilidadRadicacionActoTipo;

/**
 *
 * @author jmunocab
 */
public class TrazabilidadRadicacionActoTipo2TrazabilidadRadicacionActoConverter extends AbstractBidirectionalConverter<TrazabilidadRadicacionActoTipo, TrazabilidadActoAdministrativo> {

    public TrazabilidadRadicacionActoTipo2TrazabilidadRadicacionActoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public TrazabilidadActoAdministrativo convertTo(TrazabilidadRadicacionActoTipo srcObj) {
        TrazabilidadActoAdministrativo trazabilidadRadicacionActo = new TrazabilidadActoAdministrativo();
        this.copy(srcObj, trazabilidadRadicacionActo);
        return trazabilidadRadicacionActo;
    }

    @Override
    public void copyTo(TrazabilidadRadicacionActoTipo srcObj, TrazabilidadActoAdministrativo destObj) {

    }

    @Override
    public TrazabilidadRadicacionActoTipo convertFrom(TrazabilidadActoAdministrativo srcObj) {
        TrazabilidadRadicacionActoTipo trazabilidadRadicacionActoTipo = new TrazabilidadRadicacionActoTipo();
        this.copyFrom(srcObj, trazabilidadRadicacionActoTipo);
        return trazabilidadRadicacionActoTipo;
    }

    @Override
    public void copyFrom(TrazabilidadActoAdministrativo srcObj, TrazabilidadRadicacionActoTipo destObj) {

        destObj.setIdActoAdministrativo(srcObj.getId());
        destObj.setCodTipoNotificacionDefinitiva(srcObj.getCodTipoNotificacionDefinitiva() == null ? null : srcObj.getCodTipoNotificacionDefinitiva().getId());
        destObj.setDescTipoNotificacionDefinitiva(srcObj.getCodTipoNotificacionDefinitiva() == null ? null : srcObj.getCodTipoNotificacionDefinitiva().getNombre());
        destObj.setFecNotificacionDefinitiva(srcObj.getFecNotificacionDefinitiva());
        destObj.setFecRadicadoSalidaDefinitivo(srcObj.getFecRadicadoSalidaDefinitivo());
        destObj.getRadicacionesEntrada().addAll(this.getMapperFacade().map(srcObj.getRadicacionesEntrada(), RadicadoTrazabilidad.class, RadicadoTipo.class));
        destObj.setIdRadicadoSalidaDefinitivo(srcObj.getIdRadicadoSalidaDefinitivo());
        destObj.setValDiasProrroga(srcObj.getValDiasProrroga());
        destObj.getEnvioActoAdministrativo().addAll(this.getMapperFacade().map(srcObj.getEnvioActoAdministrativoList(), EventoActoAdministrativo.class, EnvioActoAdministrativoTipo.class));
        destObj.setUbicacionDefinitiva(this.getMapperFacade().map(srcObj.getUbicacionDefinitiva(), UbicacionPersonaTipo.class));

    }
}
