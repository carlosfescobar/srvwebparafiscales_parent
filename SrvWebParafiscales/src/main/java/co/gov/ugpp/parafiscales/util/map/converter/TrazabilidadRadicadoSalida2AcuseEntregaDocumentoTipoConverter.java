package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.TrazabilidadRadicadoSalida;

import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;

/**
 *
 * @author zrodrigu
 */
public class TrazabilidadRadicadoSalida2AcuseEntregaDocumentoTipoConverter extends AbstractCustomConverter<TrazabilidadRadicadoSalida, AcuseEntregaDocumentoTipo> {

    public TrazabilidadRadicadoSalida2AcuseEntregaDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public AcuseEntregaDocumentoTipo convert(TrazabilidadRadicadoSalida srcObj) {
        return copy(srcObj, new AcuseEntregaDocumentoTipo());
    }

    @Override
    public AcuseEntregaDocumentoTipo copy(TrazabilidadRadicadoSalida srcObj, AcuseEntregaDocumentoTipo destObj) {
        destObj.setNumRadicadoSalida(srcObj.getNumRadicadoSalida());
        destObj.setNumGuia(srcObj.getIdGuia());
        destObj.setDescCausalDevolucion(srcObj.getDescCausalDevolucion());
        destObj.setFecEntregaDevolucion(srcObj.getFecEntregaDevolucion() == null ? null : srcObj.getFecEntregaDevolucion());
        destObj.setFecEnvio(srcObj.getFecEnvio() == null ? null : srcObj.getFecEnvio());
        destObj.setCodEstadoEntrega(srcObj.getCodEstadoEntrega()== null ? null : srcObj.getCodEstadoEntrega().getId());
        destObj.setCodEstadoEnvio(srcObj.getCodEstadoEnvio()== null ? null : srcObj.getCodEstadoEnvio().getId());
        destObj.setDescEstadoEnvio(srcObj.getCodEstadoEnvio()== null ? null : srcObj.getCodEstadoEnvio().getNombre());
        return destObj;
    }

}
