package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.UVT;

import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;

/**
 *
 * @author jmuncab
 */
public class UVTToUVTTipoConverter extends AbstractCustomConverter<UVT, UVTTipo> {

    public UVTToUVTTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public UVTTipo convert(UVT srcObj) {
        return copy(srcObj, new UVTTipo());
    }

    @Override
    public UVTTipo copy(UVT srcObj, UVTTipo destObj) {
        destObj.setIdUVT(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setValUVTNumeros(srcObj.getValUVTNumeros());
        destObj.setValUVTLetras(srcObj.getValUVTLetras());
        destObj.setValNumeroResolucion(srcObj.getValNumeroResolucion());
        destObj.setValCincoUVTNumeros(srcObj.getValCincoLetrasUVTNum());
        destObj.setValCincoUVTLetras(srcObj.getValCincoLetrasUVTLetras());
        destObj.setFecResolucion(srcObj.getFecResolucion() == null ? null : srcObj.getFecResolucion());

        RangoFechaTipo rangoFechaTipo = new RangoFechaTipo();
        rangoFechaTipo.setFecInicio(srcObj.getFecInicio());
        rangoFechaTipo.setFecFin(srcObj.getFecFin());
        destObj.setPeriodoVigencia(rangoFechaTipo);
        return destObj;

    }

}
