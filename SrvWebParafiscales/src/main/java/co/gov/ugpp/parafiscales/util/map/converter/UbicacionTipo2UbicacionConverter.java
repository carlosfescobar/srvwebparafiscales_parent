package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class UbicacionTipo2UbicacionConverter extends AbstractBidirectionalConverter<UbicacionPersonaTipo, Ubicacion> {

    public UbicacionTipo2UbicacionConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public Ubicacion convertTo(UbicacionPersonaTipo srcObj) {
        Ubicacion ubicacion = new Ubicacion();
        this.copyTo(srcObj, ubicacion);
        return ubicacion;
    }

    @Override
    public void copyTo(UbicacionPersonaTipo srcObj, Ubicacion destObj) {
        destObj.setValDireccion(srcObj.getValDireccion());
    }

    @Override
    public UbicacionPersonaTipo convertFrom(Ubicacion srcObj) {
        UbicacionPersonaTipo ubicacionPersonaTipo = new UbicacionPersonaTipo();
        this.copyFrom(srcObj, ubicacionPersonaTipo);
        return ubicacionPersonaTipo;
    }

    @Override
    public void copyFrom(Ubicacion srcObj, UbicacionPersonaTipo destObj) {
        destObj.setValDireccion(srcObj.getValDireccion());
        destObj.setCodTipoDireccion(srcObj.getCodTipoDireccion() == null ? null: srcObj.getCodTipoDireccion().getId());
        destObj.setDescTipoDireccion(srcObj.getCodTipoDireccion()== null ? null :srcObj.getCodTipoDireccion().getNombre());
        destObj.setMunicipio(this.getMapperFacade().map(srcObj.getMunicipio(), MunicipioTipo.class));
    }

}
