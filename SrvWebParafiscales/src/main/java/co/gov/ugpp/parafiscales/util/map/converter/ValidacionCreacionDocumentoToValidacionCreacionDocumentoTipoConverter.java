package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionCreacionDocumento;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.map.AbstractBidirectionalConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;

/**
 *
 * @author zrodriguez
 */
public class ValidacionCreacionDocumentoToValidacionCreacionDocumentoTipoConverter extends AbstractBidirectionalConverter<ValidacionCreacionDocumento ,ValidacionCreacionDocumentoTipo> {

    public ValidacionCreacionDocumentoToValidacionCreacionDocumentoTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ValidacionCreacionDocumentoTipo convertTo(ValidacionCreacionDocumento srcObj) {
        ValidacionCreacionDocumentoTipo validacionCreacionDocumentoTipo = new ValidacionCreacionDocumentoTipo();
        this.copyTo(srcObj, validacionCreacionDocumentoTipo);
        return validacionCreacionDocumentoTipo;
    }

    @Override
    public void copyTo(ValidacionCreacionDocumento srcObj, ValidacionCreacionDocumentoTipo  destObj) {
        destObj.setIdValidacionCreacionDocumento(srcObj.getId() == null ? null : srcObj.getId().toString());
        destObj.setEsAprobado(this.getMapperFacade().map(srcObj.getESAprobado(), String.class));
        destObj.setFechaValidacion(srcObj.getFecValidacion() == null ? null : DateUtil.parseCalendarToStrDateBPMFormat(srcObj.getFecValidacion()));
        destObj.setValComentarios(srcObj.getValComentarios());
    }

    @Override
    public ValidacionCreacionDocumento convertFrom(ValidacionCreacionDocumentoTipo srcObj) {
        ValidacionCreacionDocumento validacionCreacionDocumento = new ValidacionCreacionDocumento();
        this.copyFrom(srcObj, validacionCreacionDocumento);
        return validacionCreacionDocumento;
    }

    @Override
    public void copyFrom(ValidacionCreacionDocumentoTipo srcObj, ValidacionCreacionDocumento destObj) {
        destObj.setESAprobado(this.getMapperFacade().map(srcObj.getEsAprobado(), Boolean.class));
        destObj.setFecValidacion(DateUtil.parseStrDateTimeToCalendar(srcObj.getFechaValidacion()));
        destObj.setValComentarios(srcObj.getValComentarios());
    }
}
