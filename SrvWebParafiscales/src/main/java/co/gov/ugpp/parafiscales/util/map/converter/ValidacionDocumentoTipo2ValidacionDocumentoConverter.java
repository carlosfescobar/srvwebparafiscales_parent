package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionDocumento;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.transversales.validaciondocumentotipo.v1.ValidacionDocumentoTipo;

/**
 *
 * @author jmuncab
 */
public class ValidacionDocumentoTipo2ValidacionDocumentoConverter extends AbstractCustomConverter<ValidacionDocumentoTipo, ValidacionDocumento> {

    public ValidacionDocumentoTipo2ValidacionDocumentoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ValidacionDocumento convert(ValidacionDocumentoTipo srcObj) {
        return this.copy(srcObj, new ValidacionDocumento());
    }

    @Override
    public ValidacionDocumento copy(ValidacionDocumentoTipo srcObj, ValidacionDocumento destObj) {

        destObj.setDesCausalDevolucion(srcObj.getDesCausalDevolucion());
        destObj.setEsDevolucion(this.getMapperFacade().map(srcObj.getEsDevolucion(), Boolean.class));
        destObj.setIdDocumento(srcObj.getIdDocumento());

        return destObj;
    }
}
