package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ValidacionEstructura;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;

/**
 *
 * @author jmuncab
 */
public class ValidacionEstructuraToValidacionEstructuraTipoConverter extends AbstractCustomConverter<ValidacionEstructura, ValidacionEstructuraTipo> {

    public ValidacionEstructuraToValidacionEstructuraTipoConverter(MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public ValidacionEstructuraTipo convert(ValidacionEstructura srcObj) {
        return copy(srcObj, new ValidacionEstructuraTipo());
    }

    @Override
    public ValidacionEstructuraTipo copy(ValidacionEstructura srcObj, ValidacionEstructuraTipo destObj) {

        destObj.setIdValidacionEstructura(srcObj.getId().toString());
        destObj.setDescEstructuraNoAprobada(srcObj.getDescEstructuraNoAprobada());
        destObj.setEsAprobadaNuevaEstructura(this.getMapperFacade().map(srcObj.getEsAprobadaNuevaEstructura(), String.class));
        destObj.setEsNecesarioNuevaEstructura(this.getMapperFacade().map(srcObj.getEsNecesarioNuevaEstructura(), String.class));
        destObj.setIdArchivoEstructuraSugerida(srcObj.getArchivo() == null ? null : srcObj.getArchivo().getId().toString());
        destObj.setIdFormatoElegido(srcObj.getFormatoElegido() == null ? null : srcObj.getFormatoElegido().getId().getIdFormato().toString());
        destObj.setIdVersionElegido(srcObj.getFormatoElegido() == null ? null : srcObj.getFormatoElegido().getId().getIdVersion().toString());
        destObj.setIdFormatoEstructuraNueva(srcObj.getFormatoEstructuraNueva() == null ? null : srcObj.getFormatoEstructuraNueva().getId().getIdFormato().toString());
        destObj.setIdVersionEstructuraNueva(srcObj.getFormatoEstructuraNueva() == null ? null : srcObj.getFormatoEstructuraNueva().getId().getIdVersion().toString());
        return destObj;
    }
}
