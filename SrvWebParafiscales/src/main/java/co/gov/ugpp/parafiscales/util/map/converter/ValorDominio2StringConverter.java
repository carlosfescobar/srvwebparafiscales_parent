package co.gov.ugpp.parafiscales.util.map.converter;

import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.map.AbstractCustomConverter;
import co.gov.ugpp.parafiscales.util.map.MapperFactory;

/**
 *
 * @author rpadilla
 */
public class ValorDominio2StringConverter extends AbstractCustomConverter<ValorDominio, String> {

    public ValorDominio2StringConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    @Override
    public String convert(ValorDominio srcObj) {
        return srcObj.getId();
    }

    @Override
    public String copy(ValorDominio srcObj, String destObj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
