package co.gov.ugpp.parafiscales.util.populator;

import co.gov.ugpp.esb.schema.contactopersonatipo.v1.ContactoPersonaTipo;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.esb.schema.telefonotipo.v1.TelefonoTipo;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.AbstractFacade;
import co.gov.ugpp.parafiscales.enums.ClasificacionPersonaEnum;
import co.gov.ugpp.parafiscales.enums.DominioEnum;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.enums.OrigenDocumentoEnum;
import co.gov.ugpp.parafiscales.enums.SolicitudEntidadEstadoEnum;
import co.gov.ugpp.parafiscales.enums.TipoPersonaEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.AportanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.AprobacionDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.ClaseCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.CorreoElectronicoDao;
import co.gov.ugpp.parafiscales.persistence.dao.CotizanteDao;
import co.gov.ugpp.parafiscales.persistence.dao.DivisionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.DocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.DominioDao;
import co.gov.ugpp.parafiscales.persistence.dao.EntidadExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.EnvioInformacionExternaDao;
import co.gov.ugpp.parafiscales.persistence.dao.ExpedienteDocumentoDao;
import co.gov.ugpp.parafiscales.persistence.dao.GrupoCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.MunicipioDao;
import co.gov.ugpp.parafiscales.persistence.dao.PersonaDao;
import co.gov.ugpp.parafiscales.persistence.dao.PlanillaDao;
import co.gov.ugpp.parafiscales.persistence.dao.SeccionCiiuDao;
import co.gov.ugpp.parafiscales.persistence.dao.SolicitudExpedienteDocumentoEcmDao;
import co.gov.ugpp.parafiscales.persistence.dao.TelefonoDao;
import co.gov.ugpp.parafiscales.persistence.dao.UbicacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValorDominioDao;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionAfiliacionesPorEntidad;
import co.gov.ugpp.parafiscales.persistence.entity.AgrupacionEntidadExternaPersona;
import co.gov.ugpp.parafiscales.persistence.entity.Aportante;
import co.gov.ugpp.parafiscales.persistence.entity.AprobacionDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Archivo;
import co.gov.ugpp.parafiscales.persistence.entity.ClaseCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.ClasificacionPersona;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.Cotizante;
import co.gov.ugpp.parafiscales.persistence.entity.CotizanteInformacionBasica;
import co.gov.ugpp.parafiscales.persistence.entity.DivisionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentacionEsperada;
import co.gov.ugpp.parafiscales.persistence.entity.DocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.Dominio;
import co.gov.ugpp.parafiscales.persistence.entity.EntidadExterna;
import co.gov.ugpp.parafiscales.persistence.entity.EnvioInformacionExterna;
import co.gov.ugpp.parafiscales.persistence.entity.Expediente;
import co.gov.ugpp.parafiscales.persistence.entity.ExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.GrupoCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocAgrupador;
import co.gov.ugpp.parafiscales.persistence.entity.MetadataDocumento;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.Planilla;
import co.gov.ugpp.parafiscales.persistence.entity.SeccionCiiu;
import co.gov.ugpp.parafiscales.persistence.entity.Seguimiento;
import co.gov.ugpp.parafiscales.persistence.entity.Solicitud;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudCotizante;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudExpedienteDocumentoEcm;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPago;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudPlanilla;
import co.gov.ugpp.parafiscales.persistence.entity.SolicitudSeguimiento;
import co.gov.ugpp.parafiscales.persistence.entity.Telefono;
import co.gov.ugpp.parafiscales.persistence.entity.Ubicacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.procesos.gestiondocumental.ExpedienteFacade;
import co.gov.ugpp.parafiscales.procesos.transversales.FuncionarioFacade;
import co.gov.ugpp.parafiscales.util.DateUtil;
import co.gov.ugpp.parafiscales.util.ErrorUtil;
import co.gov.ugpp.parafiscales.util.Util;
import co.gov.ugpp.parafiscales.util.Validator;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;
import co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1.PagoTipo;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;
import co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.CorreoElectronicoTipo;
import co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1.DocumentacionEsperadaTipo;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Método que se encarga de llenar de manera transversal entidades de negocio y
 * realizar validaciones de negocio específicas
 *
 * @author everis Colombia
 * @version 1.0
 */
@Stateless
@TransactionManagement
public class Populator extends AbstractFacade {

    private static final Logger LOG = LoggerFactory.getLogger(Populator.class);

    @EJB
    private ValorDominioDao valorDominioDao;

    @EJB
    private MunicipioDao municipioDao;

    @EJB
    private PersonaDao personaDao;

    @EJB
    private SolicitudExpedienteDocumentoEcmDao solicitudExpedienteDocumentoEcmDao;

    @EJB
    private ExpedienteDocumentoDao expedienteDocumentoDao;

    @EJB
    private SeccionCiiuDao seccionCiiuDao;

    @EJB
    private DivisionCiiuDao divisionCiiuDao;

    @EJB
    private GrupoCiiuDao grupoCiiuDao;

    @EJB
    private ClaseCiiuDao claseCiiuDao;

    @EJB
    private CotizanteDao cotizanteDao;

    @EJB
    private AportanteDao aportanteDao;

    @EJB
    private PlanillaDao planillaDao;

    @EJB
    private EnvioInformacionExternaDao envioInformacionExternaDao;

    @EJB
    private DominioDao dominioDao;

    @EJB
    private ExpedienteFacade expedienteFacade;

    @EJB
    private FuncionarioFacade funcionarioFacade;

    @EJB
    private TelefonoDao telefonoDao;

    @EJB
    private UbicacionDao ubicacionDao;

    @EJB
    private DocumentoDao documentoDao;

    @EJB
    private CorreoElectronicoDao correoElectronicoDao;

    @EJB
    private AprobacionDocumentoDao aprobacionDocumentoDao;

    @EJB
    private EntidadExternaDao entidadExternaDao;

    /**
     * Método que llena un listado de ubicaciones
     *
     * @param ubicacionesTipo el objeto origen
     * @param errorTipoList almacena el listado de errores
     * @param contactoPersona el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateUbicacionList(List<UbicacionPersonaTipo> ubicacionesTipo,
            final List<ErrorTipo> errorTipoList, final ContactoPersona contactoPersona, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (ubicacionesTipo != null
                && !ubicacionesTipo.isEmpty()) {

            //Se valida si ya existen ubicaciones para ese contacto
            if (contactoPersona.getUbicacionList() != null
                    && !contactoPersona.getUbicacionList().isEmpty()) {
                //Se iteran todas las ubicaciones para dejar unicamente las nuevas
                for (UbicacionPersonaTipo ubicacionPersonaTipo : ubicacionesTipo) {
                    //Determina si la dirección enviada ya existe
                    boolean existeDireccion = false;
                    if (StringUtils.isNotBlank(ubicacionPersonaTipo.getValDireccion())) {
                        for (Ubicacion ubicacionRegistrada : contactoPersona.getUbicacionList()) {
                            //Primero se valida que las direcciones sean diferentes
                            if ((ubicacionPersonaTipo.getValDireccion().trim().equals(ubicacionRegistrada.getValDireccion().trim()))) {
                                //Si las direcciones son iguales, se procede a validar si tienen el mismo tipo de dirección
                                if (StringUtils.isNotBlank(ubicacionPersonaTipo.getCodTipoDireccion())
                                        && (ubicacionRegistrada.getCodTipoDireccion().getId().equals(ubicacionPersonaTipo.getCodTipoDireccion()))) {
                                    existeDireccion = true;
                                    break;
                                }
                            }
                        }
                    }
                    //Como la dirección no existe, se procede a crearla
                    if (!existeDireccion) {
                        this.populateUbicacion(ubicacionPersonaTipo, contactoPersona, errorTipoList, contextoTransaccionalTipo);
                    }
                }
            } else {
                //Como no existen ubicaciones para ese contacto, se crean todas las ubicaciones envidadas en el request
                for (UbicacionPersonaTipo ubicacionPersonaTipo : ubicacionesTipo) {
                    this.populateUbicacion(ubicacionPersonaTipo, contactoPersona, errorTipoList, contextoTransaccionalTipo);
                }
            }
        }
    }

    /**
     * Método que llena la entidad Telefono
     *
     * @param telefonosTipo el objeto origen
     * @param errorTipoList almacena el listado de errores
     * @param contactoPersona el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateTelefonoList(final List<TelefonoTipo> telefonosTipo,
            final List<ErrorTipo> errorTipoList, final ContactoPersona contactoPersona, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (telefonosTipo != null
                && !telefonosTipo.isEmpty()) {

            //Se valida si ya existen teléfonos para ese contacto
            if (contactoPersona.getTelefonoList() != null
                    && !contactoPersona.getTelefonoList().isEmpty()) {
                //Se iteran todas los teléfonos para dejar unicamente las nuevas
                for (TelefonoTipo telefonoTipo : telefonosTipo) {
                    //Determina si el teléfono enviado ya existe
                    boolean existeTelefono = false;
                    if (StringUtils.isNotBlank(telefonoTipo.getValNumeroTelefono())) {
                        for (Telefono telefono : contactoPersona.getTelefonoList()) {
                            //Primero se valida si los telefonos son diferentes
                            if (telefonoTipo.getValNumeroTelefono().trim().equals(telefono.getValNumero().trim())) {
                                //Si son iguales, se valida si el tipo de telefono es diferente
                                if (StringUtils.isNotBlank(telefonoTipo.getCodTipoTelefono())
                                        && telefono.getCodTipoTelefono() != null
                                        && (telefonoTipo.getCodTipoTelefono().trim().equals(telefono.getCodTipoTelefono().getId().trim()))) {
                                    existeTelefono = true;
                                    break;
                                }
                            }
                        }
                        if (!existeTelefono) {
                            this.populateTelefono(telefonoTipo, errorTipoList, contactoPersona, contextoTransaccionalTipo);
                        }
                    }
                }
            } else {
                //Como no existen telefonos para este contacto, se procede a crearlos todos los que se envían en el request
                for (TelefonoTipo telefonoTipo : telefonosTipo) {
                    this.populateTelefono(telefonoTipo, errorTipoList, contactoPersona, contextoTransaccionalTipo);
                }
            }
        }
    }

    /**
     * Método que llena los correos electrónicos para un contacto
     *
     * @param correosElectronicos el listado de nuevos correos electrónicos
     * @param errorTipoList almacena el listado de errores
     * @param contactoPersona el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateCorreoElectronicoList(final List<CorreoElectronicoTipo> correosElectronicos,
            final List<ErrorTipo> errorTipoList, final ContactoPersona contactoPersona, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (correosElectronicos != null
                && !correosElectronicos.isEmpty()) {

            //Se valida si ya existen correos electrónicos para ese contacto
            if (contactoPersona.getCorreoElectronicoList() != null
                    && !contactoPersona.getCorreoElectronicoList().isEmpty()) {

                for (CorreoElectronicoTipo valEmail : correosElectronicos) {
                    //Determina si el correoElectrónico enviado ya existe
                    boolean existeCorreoElectronico = false;
                    for (CorreoElectronico correoElectronico : contactoPersona.getCorreoElectronicoList()) {
                        if (valEmail != null && StringUtils.isNotBlank(valEmail.getValCorreoElectronico())
                                && (valEmail.getValCorreoElectronico().trim().equals(correoElectronico.getValCorreoElectronico().trim()))) {
                            existeCorreoElectronico = true;
                            break;
                        }
                    }
                    if (!existeCorreoElectronico) {
                        this.populateCorreoElectronico(valEmail, contactoPersona, errorTipoList, contextoTransaccionalTipo);
                    }
                }
            } else {
                //Como no existen correos para este contacto, se procede a crearlos todos los que se envían en el request
                for (CorreoElectronicoTipo correoElectronico : correosElectronicos) {
                    this.populateCorreoElectronico(correoElectronico, contactoPersona, errorTipoList, contextoTransaccionalTipo);

                }
            }
        }
    }

    /**
     * Método que se encarga de crear UN correo electrónico
     *
     * @param valEmail el valor del correo electrónico
     * @param contactoPersona el objeto destino
     * @param errorTipoList almacena el listado de errores
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    private void populateCorreoElectronico(final CorreoElectronicoTipo valEmail, final ContactoPersona contactoPersona,
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {
        if (valEmail != null) {
            CorreoElectronico correoElectronico = new CorreoElectronico();
            correoElectronico.setValCorreoElectronico(valEmail.getValCorreoElectronico());
            correoElectronico.setContactoPersona(contactoPersona);
            correoElectronico.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            correoElectronicoDao.create(correoElectronico);
            contactoPersona.getCorreoElectronicoList().add(correoElectronico);
        }
    }

    /**
     * Método que se encarga de crear UNA ubicacion
     *
     * @param ubicacionPersonaTipo el objeto origen
     * @param contactoPersona el objeto destino
     * @param errorTipoList almacena el listado de errores
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    private void populateUbicacion(final UbicacionPersonaTipo ubicacionPersonaTipo, final ContactoPersona contactoPersona,
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        Validator.checkUbicacionPersona(ubicacionPersonaTipo, errorTipoList);

        Ubicacion ubicacion = new Ubicacion();
        ubicacion.setValDireccion(ubicacionPersonaTipo.getValDireccion());
        ubicacion.setContactoPersona(contactoPersona);
        ubicacion.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        if (StringUtils.isNotBlank(ubicacionPersonaTipo.getCodTipoDireccion())) {
            final List<ValorDominio> tiposDireccion = valorDominioDao.findByDominio(DominioEnum.COD_TIPO_DIRECCION);
            if (tiposDireccion != null
                    && !tiposDireccion.isEmpty()) {
                ValorDominio codTipoDireccionSol = null;
                for (ValorDominio codTipoDireccionExistente : tiposDireccion) {
                    if (ubicacionPersonaTipo.getCodTipoDireccion().equals(codTipoDireccionExistente.getId())) {
                        codTipoDireccionSol = codTipoDireccionExistente;
                        break;
                    }
                }
                if (codTipoDireccionSol == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "El codTipoDireccionEnviado: " + ubicacionPersonaTipo.getCodTipoDireccion() + ", no hace referencia a alguno de los tipos de dirección existentes"));
                } else {
                    ubicacion.setCodTipoDireccion(codTipoDireccionSol);
                }
            } else {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontraron tipos de direcciones con el codDominio: " + DominioEnum.COD_TIPO_DIRECCION.getCode()));
            }
        }
        if (ubicacionPersonaTipo.getMunicipio() != null
                && StringUtils.isNotBlank(ubicacionPersonaTipo.getMunicipio().getCodMunicipio())) {
            Municipio municipio = municipioDao.findByCodigo(ubicacionPersonaTipo.getMunicipio().getCodMunicipio());
            if (municipio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró Municipio con el valor:" + ubicacionPersonaTipo.getMunicipio().getCodMunicipio()));
            } else {
                ubicacion.setMunicipio(municipio);
            }
        }
        ubicacionDao.create(ubicacion);
        contactoPersona.getUbicacionList().add(ubicacion);
    }

    /**
     * Método que crea UN teléfono
     *
     * @param telefonoTipo el objeto origen
     * @param errorTipoList almacena el listado de errores
     * @param contactoPersona el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    private void populateTelefono(final TelefonoTipo telefonoTipo, final List<ErrorTipo> errorTipoList, final ContactoPersona contactoPersona,
            final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        Telefono telefono = new Telefono();
        telefono.setValNumero(telefonoTipo.getValNumeroTelefono());
        telefono.setContactoPersona(contactoPersona);
        telefono.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

        if (StringUtils.isNotBlank(telefonoTipo.getCodTipoTelefono())) {
            ValorDominio valorDominio = valorDominioDao.find(telefonoTipo.getCodTipoTelefono());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoTelefono con el valor:" + telefonoTipo.getCodTipoTelefono()));
            } else {
                telefono.setCodTipoTelefono(valorDominio);
            }
        }
        telefonoDao.create(telefono);
        contactoPersona.getTelefonoList().add(telefono);
    }

    /**
     * Método que crea una ubicación
     *
     * @param ubicacionPersonaTipo el objeto origen
     * @param ubicacion el objeto destino
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void crearUbicacion(final UbicacionPersonaTipo ubicacionPersonaTipo, final Ubicacion ubicacion, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(ubicacionPersonaTipo.getCodTipoDireccion())) {
            ValorDominio codTipoDireccion = valorDominioDao.find(ubicacionPersonaTipo.getCodTipoDireccion());
            if (codTipoDireccion == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoDireccion con el ID: " + ubicacionPersonaTipo.getCodTipoDireccion()));
            } else {
                ubicacion.setCodTipoDireccion(codTipoDireccion);
            }

        }
        if (ubicacionPersonaTipo.getMunicipio() != null
                && StringUtils.isNotBlank(ubicacionPersonaTipo.getMunicipio().getCodMunicipio())) {
            Municipio municipio = municipioDao.findByCodigo(ubicacionPersonaTipo.getMunicipio().getCodMunicipio());
            if (municipio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró municipio con el ID: " + ubicacionPersonaTipo.getMunicipio().getCodMunicipio()));
            } else {
                ubicacion.setMunicipio(municipio);
            }
        }
        if (StringUtils.isNotBlank(ubicacionPersonaTipo.getValDireccion())) {
            ubicacion.setValDireccion(ubicacionPersonaTipo.getValDireccion());
        }

    }

    /**
     * Método que llena la entidad contactoPersona
     *
     * @param contactoPersonaTipo el objeto origen
     * @param contactoPersona el objeto destino
     * @param persona la persona a la cual pertenece el contacto
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList almacena el listado de errores
     * @return el nuevo contactoPersona creado para la persona
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public ContactoPersona populateContactoPersona(final ContactoPersonaTipo contactoPersonaTipo,
            ContactoPersona contactoPersona, final Persona persona, final ContextoTransaccionalTipo contextoTransaccionalTipo,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (contactoPersona == null) {
            contactoPersona = new ContactoPersona();
            contactoPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
            contactoPersona.setPersona(persona);
        } else {
            contactoPersona.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
        }

        Validator.parseBooleanFromString(contactoPersonaTipo.getEsAutorizaNotificacionElectronica(),
                contactoPersonaTipo.getClass().getSimpleName(),
                "esAutorizaNotificacionElectronica", errorTipoList);
        Boolean esAutorizacion = mapper.map(contactoPersonaTipo.getEsAutorizaNotificacionElectronica(), Boolean.class);
        contactoPersona.setEsAutorizaNotificacionElectronica(esAutorizacion);

        if (contactoPersonaTipo.getTelefonos() != null
                && !contactoPersonaTipo.getTelefonos().isEmpty()) {
            this.populateTelefonoList(contactoPersonaTipo.getTelefonos(), errorTipoList, contactoPersona, contextoTransaccionalTipo);
        }
        if (contactoPersonaTipo.getUbicaciones() != null
                && !contactoPersonaTipo.getUbicaciones().isEmpty()) {
            this.populateUbicacionList(contactoPersonaTipo.getUbicaciones(), errorTipoList, contactoPersona, contextoTransaccionalTipo);
        }
        if (contactoPersonaTipo.getCorreosElectronicos() != null
                && !contactoPersonaTipo.getCorreosElectronicos().isEmpty()) {
            this.populateCorreoElectronicoList(contactoPersonaTipo.getCorreosElectronicos(), errorTipoList, contactoPersona, contextoTransaccionalTipo);
        }
        return contactoPersona;
    }

    /**
     * Método que llena la entidad Persona a través de una personaJuridica
     *
     * @param personaJuridicaTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param persona el objeto destino
     * @param clasificacionPersona indica que tipo de persona es (Aportante,
     * Cotizante, Entidad Externa... etc)
     * @param errorTipoList almacena el listado de errores
     * @return la persona modificada con los datos de la persona jurídica
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public Persona poupulatePersonaFromPersonaJuridica(final PersonaJuridicaTipo personaJuridicaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, Persona persona, final ClasificacionPersonaEnum clasificacionPersona,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (persona == null) {
            persona = new Persona();
        }
        if (personaJuridicaTipo.getIdPersona() != null) {

            persona.setValNumeroIdentificacion(personaJuridicaTipo.getIdPersona().getValNumeroIdentificacion());

            if (StringUtils.isNotBlank(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion())) {
                ValorDominio valorDominio = valorDominioDao.find(personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoIdentificacion con el valor:" + personaJuridicaTipo.getIdPersona().getCodTipoIdentificacion()));
                } else {
                    persona.setCodTipoIdentificacion(valorDominio);
                }
            }
            MunicipioTipo municipioTipo = personaJuridicaTipo.getIdPersona().getMunicipioExpedicion();

            if (municipioTipo != null
                    && StringUtils.isNotBlank(municipioTipo.getCodMunicipio())) {
                Municipio municipio = municipioDao.findByCodigo(municipioTipo.getCodMunicipio());
                if (municipio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró municipio con el valor:" + municipioTipo.getCodMunicipio()));
                } else {
                    persona.setMunicipio(municipio);
                }
            }
        }
        if (personaJuridicaTipo.getIdRepresentanteLegal() != null
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdRepresentanteLegal().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdRepresentanteLegal().getValNumeroIdentificacion())) {
            Identificacion identificacion = mapper.map(personaJuridicaTipo.getIdRepresentanteLegal(), Identificacion.class);
            Persona representanteLegal = personaDao.findByIdentificacion(identificacion);
            if (representanteLegal == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un representanteLegal con la identificacion: " + identificacion));
            } else {
                persona.setRepresentanteLegal(representanteLegal);
            }
        }
        if (personaJuridicaTipo.getIdAbogado() != null
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdAbogado().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdAbogado().getValNumeroIdentificacion())) {
            Identificacion identificacion = mapper.map(personaJuridicaTipo.getIdAbogado(), Identificacion.class);
            Persona abogado = personaDao.findByIdentificacion(identificacion);
            if (abogado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un abogado con la identificacion: " + identificacion));
            } else {
                persona.setAbogado(abogado);
            }
        }
        if (personaJuridicaTipo.getIdAutorizado() != null
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdAutorizado().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(personaJuridicaTipo.getIdAutorizado().getValNumeroIdentificacion())) {
            Identificacion identificacion = mapper.map(personaJuridicaTipo.getIdAutorizado(), Identificacion.class);
            Persona autorizado = personaDao.findByIdentificacion(identificacion);
            if (autorizado == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un autorizado con la identificacion: " + identificacion));
            } else {
                persona.setAutorizado(autorizado);
            }
        }
        ValorDominio valorDominio = valorDominioDao.find(TipoPersonaEnum.PERSONA_JURIDICA.getCode());
        if (valorDominio == null) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                    "No se encontró naturaleza de la persona con el valor:" + TipoPersonaEnum.PERSONA_JURIDICA.getCode()));
        } else {
            persona.setCodNaturalezaPersona(valorDominio);
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getValNumTrabajadores())) {
            try {
                persona.setValNumeroTrabajadores(Long.valueOf(personaJuridicaTipo.getValNumTrabajadores()));
            } catch (NumberFormatException nfe) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        "El número de trabajadores ingresado no es válido: " + personaJuridicaTipo.getValNumTrabajadores()));
            }
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodSeccionActividadEconomica())) {
            SeccionCiiu seccionCiiu = seccionCiiuDao.find(personaJuridicaTipo.getCodSeccionActividadEconomica());
            if (seccionCiiu == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codSeccionActividadEconomica con el valor:" + personaJuridicaTipo.getCodSeccionActividadEconomica()));
            } else {
                persona.setCodSeccionActividadEcon(seccionCiiu);
            }
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodClaseActividadEconomica())) {
            ClaseCiiu claseCiiu = claseCiiuDao.find(Long.valueOf(personaJuridicaTipo.getCodClaseActividadEconomica()));
            if (claseCiiu == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codClaseActividadEconimica con el valor:" + personaJuridicaTipo.getCodClaseActividadEconomica()));
            } else {
                persona.setCodClaseActividadEconom(claseCiiu);
            }
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodDivisionActividadEconomica())) {
            DivisionCiiu divisionCiiu = divisionCiiuDao.find(Long.valueOf(personaJuridicaTipo.getCodDivisionActividadEconomica()));
            if (divisionCiiu == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codDivisionActividadEconimica con el valor:" + personaJuridicaTipo.getCodDivisionActividadEconomica()));
            } else {
                persona.setCodDivisionActividadEco(divisionCiiu);
            }
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodGrupoActividadEconomica())) {
            GrupoCiiu grupoCiiu = grupoCiiuDao.find(Long.valueOf(personaJuridicaTipo.getCodGrupoActividadEconomica()));
            if (grupoCiiu == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codGrupoActividadEconimica con el valor:" + personaJuridicaTipo.getCodGrupoActividadEconomica()));
            } else {
                persona.setCodGrupoActividadEconom(grupoCiiu);
            }
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodFuente())) {
            valorDominio = valorDominioDao.find(personaJuridicaTipo.getCodFuente());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codFuente con el valor:" + personaJuridicaTipo.getCodFuente()));
            } else {
                persona.setCodFuente(valorDominio);
            }
        }
        if (personaJuridicaTipo.getContacto() != null) {
            persona.setContacto(this.populateContactoPersona(personaJuridicaTipo.getContacto(),
                    persona.getContacto(), persona, contextoTransaccionalTipo, errorTipoList));
        }
        if (StringUtils.isNotBlank(personaJuridicaTipo.getValNombreRazonSocial())) {
            persona.setValNombreRazonSocial(personaJuridicaTipo.getValNombreRazonSocial());
        }

        //Se coloca la nueva clasificacion persona
        if (StringUtils.isNotBlank(personaJuridicaTipo.getCodTipoPersonaJuridica())
                && clasificacionPersona != null) {
            ValorDominio codTipoPersona = valorDominioDao.find(personaJuridicaTipo.getCodTipoPersonaJuridica());

            if (codTipoPersona == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró codTipoPersona con el ID: " + personaJuridicaTipo.getCodTipoPersonaJuridica()));
            } else {

                Dominio codClasificacionPersona = dominioDao.find(clasificacionPersona.getCode());

                if (codClasificacionPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró la clasificación de la persona con el ID: " + clasificacionPersona.getCode()));
                } else {
                    ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                    clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                    clasificacionNuevaPersona.setPersona(persona);
                    clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                    persona.getClasificacionesPersona().add(clasificacionNuevaPersona);
                }
            }
        }
        return persona;
    }

    /**
     * Método que llena la entidad Persona a través de una personaNatural
     *
     * @param personaNaturalTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param persona el objeto destino
     * @param clasificacionPersona indica que tipo de persona es (Aportante,
     * Cotizante, Entidad Externa... etc)
     * @param errorTipoList almacena el listado de errores
     * @return la persona modificada con los datos de la persona natural
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public Persona populatePersonaFromPersonaNatural(final PersonaNaturalTipo personaNaturalTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, Persona persona, final ClasificacionPersonaEnum clasificacionPersona,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (persona == null) {
            persona = new Persona();
        }

        if (personaNaturalTipo.getIdPersona() != null) {

            persona.setValNumeroIdentificacion(personaNaturalTipo.getIdPersona().getValNumeroIdentificacion());

            if (StringUtils.isNotBlank(personaNaturalTipo.getIdPersona().getCodTipoIdentificacion())) {
                ValorDominio valorDominio = valorDominioDao.find(personaNaturalTipo.getIdPersona().getCodTipoIdentificacion());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoIdentificacion con el valor:" + personaNaturalTipo.getIdPersona().getCodTipoIdentificacion()));
                } else {
                    persona.setCodTipoIdentificacion(valorDominio);
                }
                MunicipioTipo municipioTipo = personaNaturalTipo.getIdPersona().getMunicipioExpedicion();

                if (municipioTipo != null
                        && StringUtils.isNotBlank(municipioTipo.getCodMunicipio())) {
                    Municipio municipio = municipioDao.findByCodigo(municipioTipo.getCodMunicipio());
                    if (municipio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró municipio con el valor:" + municipioTipo.getCodMunicipio()));
                    } else {
                        persona.setMunicipio(municipio);
                    }
                }
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getCodSexo())) {
                ValorDominio valorDominio = valorDominioDao.find(personaNaturalTipo.getCodSexo());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codSexo con el valor:" + personaNaturalTipo.getCodSexo()));
                } else {
                    persona.setCodSexo(valorDominio);
                }
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getCodEstadoCivil())) {
                ValorDominio valorDominio = valorDominioDao.find(personaNaturalTipo.getCodEstadoCivil());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codEstadoCivil con el valor:" + personaNaturalTipo.getCodEstadoCivil()));
                } else {
                    persona.setCodEstadoCivil(valorDominio);
                }
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getCodNivelEducativo())) {
                ValorDominio valorDominio = valorDominioDao.find(personaNaturalTipo.getCodNivelEducativo());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codNivelEducativo con el valor:" + personaNaturalTipo.getCodNivelEducativo()));
                } else {
                    persona.setCodNivelEducativo(valorDominio);
                }
            }
            if (personaNaturalTipo.getIdAbogado() != null
                    && StringUtils.isNotBlank(personaNaturalTipo.getIdAbogado().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(personaNaturalTipo.getIdAbogado().getValNumeroIdentificacion())) {
                Identificacion identificacion = mapper.map(personaNaturalTipo.getIdAbogado(), Identificacion.class);
                Persona abogado = personaDao.findByIdentificacion(identificacion);
                if (abogado == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un abogado con la identificacion: " + identificacion));
                } else {
                    persona.setAbogado(abogado);
                }
            }
            if (personaNaturalTipo.getIdAutorizado() != null
                    && StringUtils.isNotBlank(personaNaturalTipo.getIdAutorizado().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(personaNaturalTipo.getIdAutorizado().getValNumeroIdentificacion())) {
                Identificacion identificacion = mapper.map(personaNaturalTipo.getIdAutorizado(), Identificacion.class);
                Persona autorizado = personaDao.findByIdentificacion(identificacion);
                if (autorizado == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un autorizado con la identificacion: " + identificacion));
                } else {
                    persona.setAutorizado(autorizado);
                }
            }
            ValorDominio valorDominio = valorDominioDao.find(TipoPersonaEnum.PERSONA_NATURAL.getCode());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró naturaleza de la persona con el valor:" + TipoPersonaEnum.PERSONA_NATURAL.getCode()));
            } else {
                persona.setCodNaturalezaPersona(valorDominio);
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getCodFuente())) {
                valorDominio = valorDominioDao.find(personaNaturalTipo.getCodFuente());
                if (valorDominio == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codFuente con el valor:" + personaNaturalTipo.getCodFuente()));
                } else {
                    persona.setCodFuente(valorDominio);
                }
            }
            if (personaNaturalTipo.getContacto() != null) {
                persona.setContacto(this.populateContactoPersona(personaNaturalTipo.getContacto(),
                        persona.getContacto(), persona, contextoTransaccionalTipo, errorTipoList));
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getValPrimerNombre())) {
                persona.setValPrimerNombre(personaNaturalTipo.getValPrimerNombre());
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getValSegundoNombre())) {
                persona.setValSegundoNombre(personaNaturalTipo.getValSegundoNombre());
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getValPrimerApellido())) {
                persona.setValPrimerApellido(personaNaturalTipo.getValPrimerApellido());
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getValSegundoApellido())) {
                persona.setValSegundoApellido(personaNaturalTipo.getValSegundoApellido());
            }
            if (StringUtils.isNotBlank(personaNaturalTipo.getValNombreCompleto())) {
                persona.setValNombreRazonSocial(personaNaturalTipo.getValNombreCompleto());
            }

            //Se coloca la nueva clasificacion persona
            if (StringUtils.isNotBlank(personaNaturalTipo.getCodTipoPersonaNatural())
                    && clasificacionPersona != null) {
                ValorDominio codTipoPersona = valorDominioDao.find(personaNaturalTipo.getCodTipoPersonaNatural());

                if (codTipoPersona == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró codTipoPersona con el ID: " + personaNaturalTipo.getCodTipoPersonaNatural()));
                } else {

                    Dominio codClasificacionPersona = dominioDao.find(clasificacionPersona.getCode());

                    if (codClasificacionPersona == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                "No se encontró la clasificación de la persona con el ID: " + clasificacionPersona.getCode()));
                    } else {
                        ClasificacionPersona clasificacionNuevaPersona = new ClasificacionPersona();
                        clasificacionNuevaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        clasificacionNuevaPersona.setCodTipoPersona(codTipoPersona);
                        clasificacionNuevaPersona.setPersona(persona);
                        clasificacionNuevaPersona.setCodClasificacionPersona(codClasificacionPersona);

                        persona.getClasificacionesPersona().add(clasificacionNuevaPersona);

                    }
                }
            }
        }
        return persona;
    }

    /**
     *
     * @param <E> objeto origen
     * @param dtoClass objeto origen que extienda de solicitudTipo
     * @param solicitud el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */                                                
    public <E extends SolicitudTipo> void populateSolicitud(final E dtoClass, final Solicitud solicitud,
            final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        Expediente expediente = null;

        if ((dtoClass.getExpediente() == null
                || StringUtils.isBlank(dtoClass.getExpediente().getIdNumExpediente()))
                && (solicitud.getExpediente() != null && StringUtils.isNotBlank(solicitud.getExpediente().getId()))) {

            ExpedienteTipo expedienteTipo = new ExpedienteTipo();
            expedienteTipo.setIdNumExpediente(solicitud.getExpediente().getId());
            dtoClass.setExpediente(expedienteTipo);
        }

        if (dtoClass.getExpediente() != null
                && StringUtils.isNotBlank(dtoClass.getExpediente().getIdNumExpediente())) {

            Map<OrigenDocumentoEnum, List<DocumentoTipo>> documentos
                    = new EnumMap<OrigenDocumentoEnum, List<DocumentoTipo>>(OrigenDocumentoEnum.class);

            List<DocumentoTipo> documentoTipoList;

            if (dtoClass.getDocSolicitudInformacion() != null
                    && StringUtils.isNotBlank(dtoClass.getDocSolicitudInformacion().getIdDocumento())) {
                documentoTipoList = new ArrayList<DocumentoTipo>();
                documentoTipoList.add(dtoClass.getDocSolicitudInformacion());
                documentos.put(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION, documentoTipoList);
            }
            if (dtoClass.getDocSolicitudReiteracion() != null
                    && StringUtils.isNotBlank(dtoClass.getDocSolicitudReiteracion().getIdDocumento())) {
                documentoTipoList = new ArrayList<DocumentoTipo>();
                documentoTipoList.add(dtoClass.getDocSolicitudReiteracion());
                documentos.put(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION, documentoTipoList);
            }
            expediente = expedienteFacade.persistirExpedienteDocumento(dtoClass.getExpediente(),
                    documentos, errorTipoList, false, contextoTransaccionalTipo);

            if (!errorTipoList.isEmpty()) {
                throw new AppException(errorTipoList);
            }
            if (expediente != null) {
                solicitud.setExpediente(expediente);
            }
        }
        //Se realiza la asociaciòn entre los documentos y la solicitud
        this.populateExpedienteSolicitud(solicitud, dtoClass, contextoTransaccionalTipo, errorTipoList);

        if (dtoClass.getFecSolicitud() != null) {
            final String vFechaSolicitud = DateUtil.parseCalendarToStrDate(dtoClass.getFecSolicitud());
            solicitud.setFecSolicitud(DateUtil.parseStrDateTimeToCalendar(vFechaSolicitud));
        }
        if (dtoClass.getSolicitante() != null
                && StringUtils.isNotBlank(dtoClass.getSolicitante().getIdFuncionario())) {

            final FuncionarioTipo funcionarioTipo = funcionarioFacade.buscarFuncionario(dtoClass.getSolicitante(), contextoTransaccionalTipo);

            if (funcionarioTipo == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "Debe proporcionar un Solicitante (Funcionario) existente, ID: "
                        + dtoClass.getSolicitante().getIdFuncionario()));
            } else {
                solicitud.setValUsuario(funcionarioTipo.getIdFuncionario());
            }
        }
        if (StringUtils.isNotBlank(dtoClass.getCodMedioSolictud())) {
            ValorDominio valorDominio = valorDominioDao.find(dtoClass.getCodMedioSolictud());
            if (valorDominio == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un CodMedioSolictud con el valor: " + dtoClass.getCodMedioSolictud()));
            } else {
                solicitud.setCodMedio(valorDominio);
            }
        }
        if (StringUtils.isNotBlank(dtoClass.getCodEstadoSolicitud())) {
            ValorDominio codEstadoSolicitud = valorDominioDao.find(dtoClass.getCodEstadoSolicitud());
            if (codEstadoSolicitud == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un CodEstadoSolicitud con el valor: " + dtoClass.getCodEstadoSolicitud()));
            } else {

                if (SolicitudEntidadEstadoEnum.RESUELTA.getCode().equals(codEstadoSolicitud.getId())) {
                    solicitud.setFecRtaSolicitud(DateUtil.currentCalendar());

                } else if (SolicitudEntidadEstadoEnum.DEVUELTA.getCode().equals(codEstadoSolicitud.getId())) {
                    solicitud.setEsDevuelto(true);
                    solicitud.setFecDevolucionEntidad(DateUtil.currentCalendar());
                }
                solicitud.setCodEstadoSolicitud(codEstadoSolicitud);
            }
        }
        if (StringUtils.isNotBlank(dtoClass.getEsAprobarSolicitud())) {
            Validator.parseBooleanFromString(dtoClass.getEsAprobarSolicitud(), dtoClass.getClass().getSimpleName(),
                    "esAprobarSolicitud", errorTipoList);
            Boolean esConsultaAprobada = mapper.map(dtoClass.getEsAprobarSolicitud(), Boolean.class);
            solicitud.setEsAprobarSolicitud(esConsultaAprobada);
        }

        if (dtoClass.getSeguimiento() != null
                && !dtoClass.getSeguimiento().isEmpty()) {
            for (SeguimientoTipo seguimientoTipo : dtoClass.getSeguimiento()) {

                Seguimiento seguimiento = mapper.map(seguimientoTipo, Seguimiento.class);
                seguimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                seguimiento.setExpediente(expediente != null ? expediente : solicitud.getExpediente());

                SolicitudSeguimiento solicitudSeguimiento = new SolicitudSeguimiento();
                solicitudSeguimiento.setSeguimiento(seguimiento);
                solicitudSeguimiento.setSolicitud(solicitud);
                solicitudSeguimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                solicitud.getSolicitudSeguimientoList().add(solicitudSeguimiento);
            }
        }
        if (StringUtils.isNotBlank(dtoClass.getIdRadicadoEntrada())) {
            EnvioInformacionExterna envioInformacionExterna = envioInformacionExternaDao.find(dtoClass.getIdRadicadoEntrada());
            if (envioInformacionExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un envioInformacionExterna con el idRadicadoEntrada: " + dtoClass.getIdRadicadoEntrada()));
            } else {
                solicitud.setRadicadoEntrada(envioInformacionExterna);
            }
        }
        if (StringUtils.isNotBlank(dtoClass.getIdRadicadoSalida())) {
            solicitud.setIdRadicadoSalida(dtoClass.getIdRadicadoSalida());
        }
        
        
        
        //toaria hacer uno asi para el estado
        /*
        if (dtoClass.getSeguimiento() != null
                && !dtoClass.getSeguimiento().isEmpty()) {
            for (SeguimientoTipo seguimientoTipo : dtoClass.getSeguimiento()) {

                Seguimiento seguimiento = mapper.map(seguimientoTipo, Seguimiento.class);
                seguimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                seguimiento.setExpediente(expediente != null ? expediente : solicitud.getExpediente());

                SolicitudSeguimiento solicitudSeguimiento = new SolicitudSeguimiento();
                solicitudSeguimiento.setSeguimiento(seguimiento);
                solicitudSeguimiento.setSolicitud(solicitud);
                solicitudSeguimiento.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                solicitud.getSolicitudSeguimientoList().add(solicitudSeguimiento);
            }
        }
        */
        
        
        
        //String estadoPlantilla = solicitud.getSolicitudPlanillaList().getPlanilla().getEstadoPlanilla();
        //dtoClass.getE
                
                
        //dtoClass
        
        //String estado = solicitud.getSolicitudPlanillaList().get(0).getPlanilla().getEstadoPlanilla();
        
        //System.out.println("::ANDRES06:: solicitud getEstadoPlanilla: " + solicitud.getSolicitudPlanillaList().get(0).getPlanilla().getEstadoPlanilla());
        //System.out.println("::ANDRES06:: solicitud getId: " + solicitud.getSolicitudPlanillaList().get(0).getPlanilla().getId());
        
        
         //System.out.println("::ANDRES034:: solicitudInformacionTipo: " + solicitudInformacionTipo.getPlanillas().get(0).getEstadoPlanilla()); //1870002
        
        //System.out.println("::ANDRES06:: solicitudPlanilla getEstadoPlanilla: " + dtoClass.getSolicitante().getSolicitudPlanillaList().get(0).getPlanilla().getId());
        //System.out.println("::ANDRES06:: solicitudPlanilla getId: " + dtoClass.getPlanilla().getId());
        
        

                        
        
        
        
    }

    /**
     * Método que llena la entidad AgrupacionAfiliacionesPorEntidad
     *
     * @param agrupacionAfiliacionesPorEntidadTipo el objeto origen
     * @param agrupacionAfiliacionesPorEntidad el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateAgrupacionAfiliacionEntidad(final AgrupacionAfiliacionesPorEntidadTipo agrupacionAfiliacionesPorEntidadTipo,
            final AgrupacionAfiliacionesPorEntidad agrupacionAfiliacionesPorEntidad, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(agrupacionAfiliacionesPorEntidadTipo.getIdDocumentoOficioAfiliaciones())) {
            final DocumentoEcm documentoEcm
                    = documentoDao.find(String.valueOf(agrupacionAfiliacionesPorEntidadTipo.getIdDocumentoOficioAfiliaciones()));
            if (documentoEcm == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un  documentoOficioAfiliaciones con el ID: " + agrupacionAfiliacionesPorEntidadTipo.getIdDocumentoOficioAfiliaciones()));
            } else {
                agrupacionAfiliacionesPorEntidad.setIdDocumentoOficioAfiliaciones(documentoEcm);
            }
        }
        if (agrupacionAfiliacionesPorEntidadTipo.getOficioAfiliacionesParcial() != null
                && StringUtils.isNotBlank(agrupacionAfiliacionesPorEntidadTipo.getOficioAfiliacionesParcial().getIdArchivo())) {
            final AprobacionDocumento aprobacionDocumento = aprobacionDocumentoDao.find(Long.valueOf(agrupacionAfiliacionesPorEntidadTipo.getOficioAfiliacionesParcial().getIdArchivo()));
            if (aprobacionDocumento == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un oficioAfiliacionesParcial con el ID: " + agrupacionAfiliacionesPorEntidadTipo.getOficioAfiliacionesParcial().getIdArchivo()));
            } else {
                agrupacionAfiliacionesPorEntidad.setOficioAfiliacionesParcial(aprobacionDocumento);
            }
        }
        if (agrupacionAfiliacionesPorEntidadTipo.getEntidadExterna() != null
                && agrupacionAfiliacionesPorEntidadTipo.getEntidadExterna().getIdPersona() != null
                && (StringUtils.isNotBlank(agrupacionAfiliacionesPorEntidadTipo.getEntidadExterna().getIdPersona().getCodTipoIdentificacion())
                && StringUtils.isNotBlank(agrupacionAfiliacionesPorEntidadTipo.getEntidadExterna().getIdPersona().getValNumeroIdentificacion()))) {

            Identificacion identificacionEntidadExterna = mapper.map(agrupacionAfiliacionesPorEntidadTipo.getEntidadExterna().getIdPersona(), Identificacion.class);

            EntidadExterna entidadExterna = entidadExternaDao.findByIdentificacion(identificacionEntidadExterna);

            if (entidadExterna == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró una entidad externa con la identificacion: " + identificacionEntidadExterna));

            } else {

                agrupacionAfiliacionesPorEntidad.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                agrupacionAfiliacionesPorEntidad.setEntidadExterna(entidadExterna);

                for (PersonaNaturalTipo personaPorAfiliar : agrupacionAfiliacionesPorEntidadTipo.getPersonasPorAfiliar()) {
                    if (personaPorAfiliar != null
                            && personaPorAfiliar.getIdPersona() != null
                            && (StringUtils.isNotBlank(personaPorAfiliar.getIdPersona().getCodTipoIdentificacion())
                            && StringUtils.isNotBlank(personaPorAfiliar.getIdPersona().getValNumeroIdentificacion()))) {

                        Identificacion identificacion = mapper.map(personaPorAfiliar.getIdPersona(), Identificacion.class);
                        Persona personaAfiliar = personaDao.findByIdentificacion(identificacion);

                        if (personaAfiliar == null) {
                            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                    "No se encontró una persona a afiliar con la identificacion: " + identificacion + " a la entidad externa con identificación: " + identificacionEntidadExterna));
                        } else {
                            AgrupacionEntidadExternaPersona agrupacionEntidadExternaPersona = new AgrupacionEntidadExternaPersona();
                            agrupacionEntidadExternaPersona.setAgrupacionAfiliacionesPorEntidad(agrupacionAfiliacionesPorEntidad);
                            agrupacionEntidadExternaPersona.setPersona(personaAfiliar);
                            agrupacionEntidadExternaPersona.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                            agrupacionAfiliacionesPorEntidad.getPersonasAfiliar().add(agrupacionEntidadExternaPersona);
                        }
                    }
                }
            }

        } else if (agrupacionAfiliacionesPorEntidadTipo.getPersonasPorAfiliar() != null
                && !agrupacionAfiliacionesPorEntidadTipo.getPersonasPorAfiliar().isEmpty()) {
            errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NEGOCIO,
                    "Para crear la asociación entre la entidad externa y las personas por afiliar, es necesario enviar los datos de identificación de la entidad externa"));
        }
    }

    /**
     * Método que se encarga de llenar el listado de agrupadores para un
     * Documento
     *
     * @param valAgrupadores el listado de agrupadores (Objeto origen)
     * @param metadataDocumento objeto destino
     * @return el listado de agrupadores para un documento
     */
    public List<MetadataDocAgrupador> populateAgrupador(List<String> valAgrupadores, MetadataDocumento metadataDocumento, ContextoTransaccionalTipo contextoTransaccionalTipo) {

        List<MetadataDocAgrupador> metadataDocAgrupadorList = null;

        if (valAgrupadores != null
                && !valAgrupadores.isEmpty()) {
            metadataDocAgrupadorList = new ArrayList<MetadataDocAgrupador>();

            for (String agrupador : valAgrupadores) {
                MetadataDocAgrupador metadataDocAgrupador = new MetadataDocAgrupador();
                metadataDocAgrupador.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                metadataDocAgrupador.setMetadataDocumento(metadataDocumento);
                metadataDocAgrupador.setValAgrupador(agrupador);
                metadataDocAgrupadorList.add(metadataDocAgrupador);
            }
        }
        return metadataDocAgrupadorList;
    }

    /**
     * Método que llena al entidad Solicitud en el momento de ser CREADA
     *
     * @param solicitudInformacionTipo el objeto origen
     * @param solicitudInformacion el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateSolicitudInformacionOnCreate(final SolicitudInformacionTipo solicitudInformacionTipo,
            final Solicitud solicitudInformacion, final List<ErrorTipo> errorTipoList, final ContextoTransaccionalTipo contextoTransaccionalTipo) throws AppException {

        if (solicitudInformacionTipo.getPlanillas() != null
                && !solicitudInformacionTipo.getPlanillas().isEmpty()) {

            for (PlanillaTipo planillaTipo : solicitudInformacionTipo.getPlanillas()) {
                if (StringUtils.isNotBlank(planillaTipo.getIdNumeroPlanilla())) {
                    Planilla planilla = planillaDao.find(planillaTipo.getIdNumeroPlanilla().trim());

                    if (planilla == null) {
                        planilla = mapper.map(planillaTipo, Planilla.class);
                        planilla.setId(planillaTipo.getIdNumeroPlanilla().trim());
                        planilla.setValPlanilla(planillaTipo.getDescObservaciones());

                        if (StringUtils.isNotBlank(planillaTipo.getCodTipoObservaciones())) {
                            ValorDominio codTipoObservacion = valorDominioDao.find(planillaTipo.getCodTipoObservaciones());
                            if (codTipoObservacion == null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL, "NO se encontro un CodTipoObservacion con el id: " + planillaTipo.getCodTipoObservaciones()));
                            } else {
                                planilla.setCodTipoObservaciones(codTipoObservacion);
                            }
                        }
                        
                        
                        if (StringUtils.isNotBlank(planillaTipo.getEstadoPlanilla())) 
                            planilla.setEstadoPlanilla(planillaTipo.getEstadoPlanilla());
                        

                        if (!errorTipoList.isEmpty()) {
                            throw new AppException(errorTipoList);
                        }
                        
                        
                        

                        planilla.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                        if (StringUtils.isNotBlank(planillaTipo.getCodOperador()) && solicitudInformacionTipo.getEntidadExterna() != null
                                && StringUtils.isNotBlank(solicitudInformacionTipo.getEntidadExterna().getCodTipoEntidadExterna())) {

                            ValorDominio valorDominio = valorDominioDao.find(solicitudInformacionTipo.getEntidadExterna().getCodTipoEntidadExterna());

                            if (valorDominio == null) {
                                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                                        "No se encontró un CodOperador con el valor:" + solicitudInformacionTipo.getEntidadExterna().getCodTipoEntidadExterna()));
                            } else {
                                planilla.setCodTipoOperador(valorDominio);
                            }
                        }
                        planillaDao.create(planilla);
                    }
                    SolicitudPlanilla solicitudPlanilla = new SolicitudPlanilla();
                    solicitudPlanilla.setFecPeriodoPago(this.convertirFechaPeriodoPago(planillaTipo.getFecPeriodoPago().getTime()));
                    solicitudPlanilla.setSolicitud(solicitudInformacion);
                    solicitudPlanilla.setPlanilla(planilla);
                    solicitudPlanilla.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                    solicitudInformacion.getSolicitudPlanillaList().add(solicitudPlanilla);
                }
            }
        }

        if (solicitudInformacionTipo.getPagos() != null
                && !solicitudInformacionTipo.getPagos().isEmpty()) {

            for (PagoTipo pagoTipo : solicitudInformacionTipo.getPagos()) {
                SolicitudPago solicitudPago = mapper.map(pagoTipo, SolicitudPago.class);
                solicitudPago.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());
                solicitudPago.setFecPeriodoPago(this.convertirFechaPeriodoPago(solicitudPago.getFecPeriodoPago().getTime()));
                solicitudPago.setSolicitud(solicitudInformacion);
                solicitudInformacion.getSolicitudPagoList().add(solicitudPago);
            }
        }
        if (solicitudInformacionTipo.getCotizantes() != null
                && !solicitudInformacionTipo.getCotizantes().isEmpty()) {

            List<Identificacion> identificacionList = new ArrayList<Identificacion>();

            for (CotizanteTipo cotizanteTipo : solicitudInformacionTipo.getCotizantes()) {
                IdentificacionTipo identificacionTipo = cotizanteTipo.getIdPersona();
                Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);

                if (StringUtils.isNotBlank(identificacion.getCodTipoIdentificacion())
                        && StringUtils.isNotBlank(identificacion.getValNumeroIdentificacion())) {
                    identificacionList.add(identificacion);
                }
            }
            if (!identificacionList.isEmpty()) {

                Validator.checkDuplicateIdentificacion(identificacionList, errorTipoList, CotizanteTipo.class);
                List<Cotizante> cotizanteList = cotizanteDao.findByIdentificacionList(identificacionList);

                if (cotizanteList == null || cotizanteList.isEmpty()) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se ha encontrado una lista de Cotizantes con los valores :" + identificacionList));
                } else {
                    for (Cotizante cotizante : cotizanteList) {

                        SolicitudCotizante soliciutdCotizante = new SolicitudCotizante();
                        soliciutdCotizante.setSoliciutd(solicitudInformacion);
                        soliciutdCotizante.setCotizante(cotizante);
                        soliciutdCotizante.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                        for (CotizanteTipo cotizanteTipo : solicitudInformacionTipo.getCotizantes()) {
                            if (cotizante.getPersona().getCodTipoIdentificacion().getId().equals(cotizanteTipo.getIdPersona().getCodTipoIdentificacion())
                                    && cotizante.getPersona().getValNumeroIdentificacion().equals(cotizante.getPersona().getValNumeroIdentificacion())) {
                                solicitudInformacion.getSoliciutdCotizanteList().add(soliciutdCotizante);
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (solicitudInformacionTipo.getNoAportante() != null
                && solicitudInformacionTipo.getNoAportante().getIdPersona() != null) {
            IdentificacionTipo identificacionTipo = solicitudInformacionTipo.getNoAportante().getIdPersona();

            if (StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())) {

                Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
                Persona noAportante = personaDao.findByIdentificacion(identificacion);

                if (noAportante == null) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                            "No se encontró un NoAportante con la identificación:" + identificacion));
                } else {
                    solicitudInformacion.setNoAportante(noAportante);
                }
            }
        }
        if (solicitudInformacionTipo.getAportante() != null) {

            if (solicitudInformacionTipo.getAportante().getPersonaJuridica() != null
                    && solicitudInformacionTipo.getAportante().getPersonaJuridica().getIdPersona() != null
                    && (StringUtils.isNotBlank(solicitudInformacionTipo.getAportante().getPersonaJuridica().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(solicitudInformacionTipo.getAportante().getPersonaJuridica().getIdPersona().getValNumeroIdentificacion()))) {

                IdentificacionTipo identificacionTipo = solicitudInformacionTipo.getAportante().getPersonaJuridica().getIdPersona();
                this.cargarEntityAportante(identificacionTipo, solicitudInformacion, errorTipoList);

            } else if (solicitudInformacionTipo.getAportante().getPersonaNatural() != null
                    && solicitudInformacionTipo.getAportante().getPersonaNatural().getIdPersona() != null
                    && (StringUtils.isNotBlank(solicitudInformacionTipo.getAportante().getPersonaNatural().getIdPersona().getCodTipoIdentificacion())
                    && StringUtils.isNotBlank(solicitudInformacionTipo.getAportante().getPersonaNatural().getIdPersona().getValNumeroIdentificacion()))) {

                IdentificacionTipo identificacionTipo = solicitudInformacionTipo.getAportante().getPersonaNatural().getIdPersona();
                this.cargarEntityAportante(identificacionTipo, solicitudInformacion, errorTipoList);
            }
        }
    }

    /**
     * Método que carga el aportante para una solcitud
     *
     * @param identificacionTipo la identificación del aportante
     * @param solicitudInformacion la solicitud a la cual se le va a asginar el
     * aportante
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     * @return
     */
    private Solicitud cargarEntityAportante(final IdentificacionTipo identificacionTipo, final Solicitud solicitudInformacion,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if (StringUtils.isNotBlank(identificacionTipo.getCodTipoIdentificacion())
                && StringUtils.isNotBlank(identificacionTipo.getValNumeroIdentificacion())) {

            Identificacion identificacion = mapper.map(identificacionTipo, Identificacion.class);
            Aportante aportante = aportanteDao.findByIdentificacion(identificacion);

            if (aportante == null) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_INTEGRIDAD_REFERENCIAL,
                        "No se encontró un Aportante con la identificación:" + identificacion));
            } else {
                solicitudInformacion.setAportante(aportante);
            }
        }
        return solicitudInformacion;

    }

    /**
     * Método que llena al entidad Solicitud en el momento de ser ACTUALIZADA
     *
     * @param solicitudInformacionTipo el objeto origen
     * @param solicitudInformacion el objeto destino
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateSolicitudInformacionOnUpdate(final SolicitudInformacionTipo solicitudInformacionTipo,
            final Solicitud solicitudInformacion, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        //Por negocio, se valida si la solicitud tiene una lista de cotizantes, se actualizan los atributos
        //de cada cotizante, no se agregan nuevos ni se eliminan
        if (solicitudInformacion.getSoliciutdCotizanteList() != null
                && !solicitudInformacion.getSoliciutdCotizanteList().isEmpty()) {

            Map<SolicitudCotizante, CotizanteTipo> solicitudCotizantexCotizante
                    = this.populateSolicitudCotizante(solicitudInformacionTipo.getCotizantes(), solicitudInformacion.getSoliciutdCotizanteList(), errorTipoList);
            for (Entry<SolicitudCotizante, CotizanteTipo> entry : solicitudCotizantexCotizante.entrySet()) {

                SolicitudCotizante solicitudCotizante = entry.getKey();
                solicitudCotizante.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                Cotizante cotizante = solicitudCotizante.getCotizante();
                CotizanteTipo cotizanteTipo = entry.getValue();

                if (StringUtils.isNotBlank(cotizanteTipo.getDescObservacion())
                        || cotizanteTipo.getDocCotizante() != null) {
                    if (cotizante.getInfoCotizante() == null) {
                        cotizante.setInfoCotizante(new CotizanteInformacionBasica());
                        cotizante.getInfoCotizante().setCotizante(cotizante);
                    }
                    if (cotizanteTipo.getDocCotizante() != null) {
                        Validator.checkArchivo(cotizanteTipo.getDocCotizante(), errorTipoList);
                        Archivo archivo = mapper.map(cotizanteTipo.getDocCotizante(), Archivo.class);
                        solicitudCotizante.setArchivo(archivo);
                    }
                    cotizante.getInfoCotizante().setDescObservacion(cotizanteTipo.getDescObservacion());
                    cotizante.getInfoCotizante().setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                }
            }
        }
    }

    /**
     * Método que se encarga de organizar los cotizantes para una solicitud
     *
     * @param cotizanteTipoList los cotizates a relacionar en la solicitud
     * (Objeto origen)
     * @param solicitudCotizanteList las solicitudes a ser pobladas con los
     * cotizantes (Objeto destino)
     * @param errorTipoList almacena el listado de errores
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     * @return las soliciudes organizadas con sus cotizantes
     */
    @SuppressWarnings("IncompatibleEquals")
    private Map<SolicitudCotizante, CotizanteTipo> populateSolicitudCotizante(final List<CotizanteTipo> cotizanteTipoList,
            final List<SolicitudCotizante> solicitudCotizanteList, final List<ErrorTipo> errorTipoList) {

        Map<SolicitudCotizante, CotizanteTipo> solicitudCotizantexCotizante = new HashMap<SolicitudCotizante, CotizanteTipo>();

        for (SolicitudCotizante solicitudCotizante : solicitudCotizanteList) {
            for (CotizanteTipo cotizanteTipo : cotizanteTipoList) {
                if (solicitudCotizante.equals(cotizanteTipo)) {
                    solicitudCotizantexCotizante.put(solicitudCotizante, cotizanteTipo);
                    cotizanteTipoList.remove(cotizanteTipo);
                    break;
                }
            }
        }
        if (!cotizanteTipoList.isEmpty()) {
            for (CotizanteTipo cotizanteTipo : cotizanteTipoList) {
                errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                        "El Cotizante enviado no está asociado a la solicitud: codTipoIdentificacion: " + cotizanteTipo.getIdPersona().getCodTipoIdentificacion()
                        + "valNumeroIdentifcacion: " + cotizanteTipo.getIdPersona().getValNumeroIdentificacion()));
            }
        }
        return solicitudCotizantexCotizante;
    }

    /**
     * Mètodo que se encarga de crear la relación entre la solicitud, el
     * expediente y sus documentos
     *
     * @param solicitud la solicitud a asociar
     * @param solicitudTipo contiene los atributos de entrada en el request
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList la pila de errores donde se almacenan los errores de
     * negocio y aplicación
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    public void populateExpedienteSolicitud(final Solicitud solicitud, final SolicitudTipo solicitudTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) throws AppException {

        //Se valida que ninguno de los documentos que se envían en el request sean nulos
        if ((solicitudTipo.getDocSolicitudInformacion() != null && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudInformacion().getIdDocumento()))
                || (solicitudTipo.getDocSolicitudReiteracion() != null && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudReiteracion().getIdDocumento()))) {

            //Se añade validación de negocio, el id del documento de reiteración no puede ser el mismo que el de información
            if ((solicitudTipo.getDocSolicitudInformacion() != null && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudInformacion().getIdDocumento()))
                    && (solicitudTipo.getDocSolicitudReiteracion() != null && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudReiteracion().getIdDocumento()))) {

                if (solicitudTipo.getDocSolicitudInformacion().getIdDocumento().equals(solicitudTipo.getDocSolicitudReiteracion().getIdDocumento())) {
                    errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                            "El identificador del documento docSolicitudReiteracion no puede ser igual al identificador del documento docSolicitudInformacion: " + solicitudTipo.getDocSolicitudInformacion().getIdDocumento()));
                }
            }

            if (solicitud.getId() != null) {
                final List<SolicitudExpedienteDocumentoEcm> solicitudExpedienteDocumentoEcmList = solicitud.getDocumentosSolicitud();
                if (solicitudExpedienteDocumentoEcmList != null
                        && !solicitudExpedienteDocumentoEcmList.isEmpty()) {

                    //Se valida si hay cambio en los documentos para esa solicitud
                    if (solicitudTipo.getDocSolicitudInformacion() != null
                            && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudInformacion().getIdDocumento())) {

                        List<SolicitudExpedienteDocumentoEcm> solicitudExpedienteDocumentoEcmActual
                                = solicitudExpedienteDocumentoEcmDao.findByCodOrigenAndSolicitud(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION, solicitud);
                        if (solicitudExpedienteDocumentoEcmActual != null) {
                            //Se modifican todos los documentos con el codOrigen SOLICITUD_DOC_SOLICITUD_INFORMACION a null
                            boolean esModificable = this.modifySolicitudExpedienteDocumento(solicitudExpedienteDocumentoEcmActual,
                                    contextoTransaccionalTipo, solicitudTipo.getDocSolicitudInformacion(), OrigenDocumentoEnum.SOLICITUD_EX_DOC_SOLICITUD_INFORMACION);

                            if (esModificable) {
                                //Se llena el docSolicitudInformacion
                                this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudInformacion(),
                                        solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION, errorTipoList);
                            }
                        }
                    }
                    if (solicitudTipo.getDocSolicitudReiteracion() != null
                            && StringUtils.isNotBlank(solicitudTipo.getDocSolicitudReiteracion().getIdDocumento())) {

                        List<SolicitudExpedienteDocumentoEcm> solicitudExpedienteDocumentoEcmActual
                                = solicitudExpedienteDocumentoEcmDao.findByCodOrigenAndSolicitud(OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION, solicitud);
                        if (solicitudExpedienteDocumentoEcmActual != null) {
                            //Se modifican todos los documentos con el codOrigen SOLICITUD_DOC_SOLICITUD_INFORMACION a null
                            boolean esModificable = this.modifySolicitudExpedienteDocumento(solicitudExpedienteDocumentoEcmActual,
                                    contextoTransaccionalTipo, solicitudTipo.getDocSolicitudReiteracion(), OrigenDocumentoEnum.SOLICITUD_EX_DOC_SOLICITUD_REITERACION);

                            if (esModificable) {
                                //Se llena el docSolicitudReiteracion
                                this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudReiteracion(),
                                        solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION, errorTipoList);
                            }
                        }
                    }

                } else {
                    //Se llena el docSolicitudInformacion
                    this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudInformacion(), solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION, errorTipoList);
                    //Se llena el docSolicitudReiteracion
                    this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudReiteracion(), solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION, errorTipoList);
                }
            } else {
                //Se llena el docSolicitudInformacion
                this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudInformacion(), solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_INFORMACION, errorTipoList);
                //Se llena el docSolicitudReiteracion
                this.buildSolicitudExpediente(contextoTransaccionalTipo, solicitud, solicitudTipo.getDocSolicitudReiteracion(), solicitud.getExpediente(), OrigenDocumentoEnum.SOLICITUD_DOC_SOLICITUD_REITERACION, errorTipoList);
            }
        }
    }

    /**
     * Mètodo que llena los documentos para la solicitud
     *
     * @param contextoTransaccionalTipo contiene los datos de auditorìa
     * @param solicitud la solicitud que se va a persistir
     * @param documentoTipo el documento que se va a asociar a la solicitud
     * @param expediente el expediente que està asociado al documento
     * @throws AppException indica que ha ocurrido un error al consultar o crear
     * una entidad en la base de datos o que ha ocurrido un error de negocio
     */
    private void buildSolicitudExpediente(final ContextoTransaccionalTipo contextoTransaccionalTipo, final Solicitud solicitud,
            final DocumentoTipo documentoTipo, final Expediente expediente, final OrigenDocumentoEnum origenDocumentoEnum,
            final List<ErrorTipo> errorTipoList) throws AppException {

        if ((expediente != null && StringUtils.isNotBlank(expediente.getId()))
                && (documentoTipo != null && StringUtils.isNotBlank(documentoTipo.getIdDocumento()))) {
            ExpedienteDocumentoEcm expedienteDocumentoEcm = expedienteDocumentoDao.findDocumentoExpedienteByExpedienteAndDocumento(expediente.getId(), documentoTipo.getIdDocumento());
            if (expedienteDocumentoEcm != null) {

                SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm = new SolicitudExpedienteDocumentoEcm();
                solicitudExpedienteDocumentoEcm.setSolicitud(solicitud);
                solicitudExpedienteDocumentoEcm.setIdUsuarioCreacion(contextoTransaccionalTipo.getIdUsuario());

                if (origenDocumentoEnum != null
                        && StringUtils.isNotBlank(origenDocumentoEnum.getCode())) {
                    ValorDominio valorDominio = valorDominioDao.find(origenDocumentoEnum.getCode());
                    if (valorDominio == null) {
                        errorTipoList.add(ErrorUtil.buildErrorTipo(ErrorEnum.ERROR_NO_ESPERADO,
                                "No se encontró codOrigen con el valor: " + origenDocumentoEnum.getCode()));
                    } else {
                        expedienteDocumentoEcm.setCodOrigen(valorDominio);
                    }
                }
                solicitudExpedienteDocumentoEcm.setExpedienteDocumentoEcm(expedienteDocumentoEcm);
                solicitud.getDocumentosSolicitud().add(solicitudExpedienteDocumentoEcm);
            }
        }
    }

    /**
     * Método que convierte la fecha de periodo pago de un formato a otro
     *
     * @param fecha la fecha a convertir
     * @param formatoInicial el formato origen
     * @param formatoFinal el formato destino
     * @return la fecha en el formato destino
     */
    private Date convertirFechaConFomato(String fecha, String formatoInicial, String formatoFinal) {
        String resultado;
        Date dResultado = null;
        try {
            SimpleDateFormat sdfFormatoInicial = new SimpleDateFormat(formatoInicial);
            Date date = sdfFormatoInicial.parse(fecha);
            SimpleDateFormat sdfFormatoFinal = new SimpleDateFormat(formatoFinal);
            resultado = sdfFormatoFinal.format(date);
            dResultado = sdfFormatoFinal.parse(resultado);
        } catch (ParseException e) {
            LOG.warn("No se pudo parsear la fecha: " + fecha, e.getMessage());
        }
        return dResultado;
    }

    /**
     * Método que convierte la fecha de periodo pago
     *
     * @param fechaPeriodoPago la fecha a convertir
     * @return la fecha convertida
     */
    private Calendar convertirFechaPeriodoPago(Date fechaPeriodoPago) {

        String sFechaPeriodoPago = DateUtil.anoMesDia.format(fechaPeriodoPago);
        Date fechaPagoDate = this.convertirFechaConFomato(sFechaPeriodoPago, "yyyy-MM-dd", "yyyy-MM-dd'T'HH:mm:ss");
        Calendar cFechaPeridoPago = Calendar.getInstance();
        cFechaPeridoPago.setTime(fechaPagoDate);
        return cFechaPeridoPago;
    }

    /**
     * Método que llena al entidad DocumentacionEsperada en el momento de ser
     * CREADA
     *
     * @param documentacionEsperada el objeto destino
     * @param documentacionEsperadaTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList la pila de errores donde se almacenan los errores de
     * negocio y aplicación
     */
    public void populateDocumentacionEsperadaOnCreate(final DocumentacionEsperada documentacionEsperada,
            final DocumentacionEsperadaTipo documentacionEsperadaTipo, final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) {

        if (documentacionEsperadaTipo.getFecEntradaDocumentos() != null) {
            documentacionEsperada.setFecSalidaDocumentos(documentacionEsperadaTipo.getFecEntradaDocumentos());
        }
    }

    /**
     * Método que llena al entidad DocumentacionEsperada en el momento de ser
     * ACTUALIZADA
     *
     * @param documentacionEsperada el objeto destino
     * @param documentacionEsperadaTipo el objeto origen
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param errorTipoList la pila de errores donde se almacenan los errores de
     * negocio y aplicación
     */
    public void populateDocumentacionEsperadaOnUpdate(final DocumentacionEsperada documentacionEsperada, final DocumentacionEsperadaTipo documentacionEsperadaTipo,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final List<ErrorTipo> errorTipoList) {

        if (documentacionEsperadaTipo.getFecEntradaDocumentos() != null) {
            documentacionEsperada.setFecEntradaDocumentos(documentacionEsperadaTipo.getFecEntradaDocumentos());
        }
    }

    /**
     * Método que modifica el codOrigen de los documentos asociados a la
     * solicitud
     *
     * @param solicitudExpedienteDocumentoEcmList el listado de documentos a
     * modificar
     * @param nuevoDocumento le nuevo documento que se va a asociar a la
     * solicitud
     * @param contextoTransaccionalTipo contiene los datos de auditoría
     * @param docOrigenAnterior el codigo de origen con el que va a quedar el
     * documento anterior
     * @return true si se debe crear el nuevo registro, false de lo contrario
     * @throws co.gov.ugpp.parafiscales.exception.AppException se lanza si
     * ocurre un error actualizando el documento
     */
    public boolean modifySolicitudExpedienteDocumento(final List<SolicitudExpedienteDocumentoEcm> solicitudExpedienteDocumentoEcmList,
            final ContextoTransaccionalTipo contextoTransaccionalTipo, final DocumentoTipo nuevoDocumento, final OrigenDocumentoEnum docOrigenAnterior) throws AppException {

        //Indica si se debe realizar la nueva creación del registro entre el documento y la solicitud
        boolean esModificable = false;

        if (solicitudExpedienteDocumentoEcmList != null
                && !solicitudExpedienteDocumentoEcmList.isEmpty()) {

            //Se valida si el documento que se está tratando de crear es el mismo al que ya está asociado a la solicitud
            esModificable = Util.verificarExistenciaDocumentoSolicitud(solicitudExpedienteDocumentoEcmList, nuevoDocumento);

            if (esModificable) {
                for (SolicitudExpedienteDocumentoEcm solicitudExpedienteDocumentoEcm : solicitudExpedienteDocumentoEcmList) {
                    //Se obtiene el expediente documento y se modifica el codOrigen a null
                    final ExpedienteDocumentoEcm expedienteDocumentoEcm = solicitudExpedienteDocumentoEcm.getExpedienteDocumentoEcm();
                    expedienteDocumentoEcm.setIdUsuarioModificacion(contextoTransaccionalTipo.getIdUsuario());
                    //Se deja el codOrigen con el valor anterior (Se deja la marca que fue un documento de reiteración o información)
                    ValorDominio origenAnterior = valorDominioDao.find(docOrigenAnterior.getCode());
                    if (origenAnterior == null) {
                        LOG.info("Debido a que no se encontró origen anterior, se deja el origen sin registrar");
                        ValorDominio origenSinRegistrar = valorDominioDao.find(OrigenDocumentoEnum.SIN_REGISTRAR.getCode());
                        if (origenSinRegistrar == null) {
                            LOG.info("Debido a que no se encontró origen sin registrar, se deja nulo por defecto");
                            expedienteDocumentoEcm.setCodOrigen(null);
                        } else {
                            
                            //System.out.println("Op: ::ANDRES21:: modifySolicitudExpedienteDocumento expedienteDocumentoEcm.getId(): " + expedienteDocumentoEcm.getId());
                            
                            expedienteDocumentoEcm.setCodOrigen(origenSinRegistrar);
                        }
                    } else {
                        expedienteDocumentoEcm.setCodOrigen(origenAnterior);
                    }
                    expedienteDocumentoDao.edit(expedienteDocumentoEcm);
                }
            } else {
                LOG.info("No se realiza modificación del documento debido a que es igual al que posee la solicitud. idDocumento : " + nuevoDocumento.getIdDocumento());
            }
        }
        return esModificable;
    }
}
