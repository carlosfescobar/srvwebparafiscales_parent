/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.util.retry;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import java.io.Serializable;

/**
 *
 * @author jmunocab
 */
public interface RetryFacade extends Serializable{
    
    void createRetry(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException;
    
}
