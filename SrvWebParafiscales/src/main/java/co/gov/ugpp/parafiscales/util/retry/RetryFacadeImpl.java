package co.gov.ugpp.parafiscales.util.retry;

import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ReintentoDao;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.util.Constants;
import co.gov.ugpp.parafiscales.util.PropsReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Implementaciòn de la interfaz RetryFacade
 *
 * @author everis Colombia
 */
@Stateless
@TransactionManagement
@PermitAll
public class RetryFacadeImpl implements RetryFacade {
    
    private static final Logger LOG = LoggerFactory.getLogger(RetryFacadeImpl.class);

    @EJB
    private ReintentoDao reintentoDao;

    /**
     * Método que crea un nuevo reintento
     *
     * @param contextoTransaccionalTipo contiene la informaciòn del la
     * transacción y el usuario que la está ejecutando
     * @param codProceso el proceso que se está reintentando ejecutar
     * @throws AppException
     */
    @Override
    public void createRetry(final ContextoTransaccionalTipo contextoTransaccionalTipo, final ValorDominio codProceso) throws AppException {

        Object[] reintentoParams = new Object[Constants.CAMPOS_PERSISITR_REINTENTO];
        reintentoParams[Constants.FEC_CREACION_REINTENTO] = new Date();
        reintentoParams[Constants.FEC_MODIFICACION_REINTENTO] = new Date();
        reintentoParams[Constants.ID_TRANSACCION] = contextoTransaccionalTipo.getIdTx();
        reintentoParams[Constants.ID_USUARIO_CREACION_REINTENTO] = contextoTransaccionalTipo.getIdUsuario();
        reintentoParams[Constants.ID_USUARIO_MODIFICACION_REINTENTO] = contextoTransaccionalTipo.getIdUsuario();
        reintentoParams[Constants.COD_PROCESO] = codProceso.getId();

        List<Object[]> reintento = new ArrayList<Object[]>();
        reintento.add(reintentoParams);
        try {
            reintentoDao.ejecutarBatchQuery(PropsReader.getKeyParam(Constants.INSERT_REINTENTO), reintento);
        } catch (SQLException sqle) {
            throw new AppException("Error creando el reintendo de servicio"+sqle.getMessage());
        }
    }
}
