/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.ugpp.parafiscales.validadorinformacion;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;

/**
 *
 * @author jgonzalezt
 */
public class ErrorValidacionArchivoTipo extends ErrorTipo {
    
    private long idValidacion;
    private String mensaje;
    
    /**
     * @return the idValidacion
     */
    public long getIdValidacion() {
        return idValidacion;
    }

    /**
     * @param idValidacion the idValidacion to set
     */
    public void setIdValidacion(long idValidacion) {
        this.idValidacion = idValidacion;
    }

    /**
     * @return the mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * @param mensaje the mensaje to set
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}

