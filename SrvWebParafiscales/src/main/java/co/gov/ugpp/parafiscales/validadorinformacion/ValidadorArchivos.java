package co.gov.ugpp.parafiscales.validadorinformacion;

import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;
import co.gov.ugpp.parafiscales.enums.ErrorEnum;
import co.gov.ugpp.parafiscales.exception.AppException;
import co.gov.ugpp.parafiscales.persistence.dao.ErrorValidacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.InstanciaEjecucionValidacionDao;
import co.gov.ugpp.parafiscales.persistence.dao.ValidacionFormatoDao;
import co.gov.ugpp.parafiscales.persistence.entity.ErrorValidacion;
import co.gov.ugpp.parafiscales.persistence.entity.Formato;
import co.gov.ugpp.parafiscales.persistence.entity.FormatoPK;
import co.gov.ugpp.parafiscales.persistence.entity.InstanciaEjecucionValidacion;
import co.gov.ugpp.parafiscales.persistence.entity.ValidacionFormato;
import co.gov.ugpp.parafiscales.procesos.solicitarinformacion.ValidacionFormatoFacade;
import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyObject;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.codehaus.groovy.control.CompilationFailedException;
import org.codehaus.groovy.runtime.typehandling.GroovyCastException;

/**
 *
 * @author jgonzalezt
 */
@Stateless
public class ValidadorArchivos implements Serializable {

    private final String TIPOVALIDACIONGROOVY = "1";
    private Map<String, Object> objReturn;//lo que devolvera el validador
    private Map<String, Object> resultadoGroovy;//lo que devuelve el groovy
    private List<ErrorValidacionArchivoTipo> listaErrores; //declarar la lista de errores que solo admite tipo de dato lista nada mas

    private int limiteErrores;
    
    @EJB
    private ErrorValidacionDao errorvalidaciondao;
    @EJB
    private ValidacionFormatoDao validacionFormatoDao;
    @EJB
    private InstanciaEjecucionValidacionDao instanciaEjecucionValidacionDao;
    private InstanciaEjecucionValidacion unReg;
    private ValidacionFormatoFacade validacionFormatoFacade;

    /**
     * validarArchivo validador de informacion de parafiscales
     *
     * @param archivo
     * @param nombreArchivo
     * @param idFormato
     * @param numVersion
     * @param fechaVigencia
     * @param transacional
     * @param infoNegocio
     * @return
     */
    
    public Map validarArchivo(
            InputStream archivo,
            String nombreArchivo,
            Long idFormato,
            Long numVersion,
            Date fechaVigencia,
            String transacional,
            Map infoNegocio) {
        
        
        limiteErrores = 100;

        boolean parametrosValidos = false;
        if (!(infoNegocio instanceof Map) || infoNegocio.isEmpty()) {
            infoNegocio = new HashMap();
        }
        if (nombreArchivo == null || nombreArchivo.isEmpty()) {
            parametrosValidos = true;
            System.out.println("::VALIDADOR:: nombreArchivo: " + "El nombre del archivo no puede estar vacio");
            infoNegocio.put("nombreArchivo", "El nombre del archivo no puede estar vacio");
        }
        if (idFormato < 1 || idFormato == null) {
            parametrosValidos = true;
            System.out.println("::VALIDADOR:: idFormato: " + "El id de formato debe ser un numero entero mayor a cero");
            infoNegocio.put("idFormato", "El id de formato debe ser un numero entero mayor a cero");
        }
        if (numVersion < 1 || numVersion == null) {
            parametrosValidos = true;
            System.out.println("::VALIDADOR:: numVersion: " + "El numero de version debe ser un numero entero mayor a cero");
            infoNegocio.put("numVersion", "El numero de version debe ser un numero entero mayor a cero");
        }
        if (transacional.length() < 1 || transacional.isEmpty()) {
            parametrosValidos = true;
            System.out.println("::VALIDADOR:: transacional: " + "El contexto transaccional no debe estar vacio");
            infoNegocio.put("transacional", "El contexto transaccional no debe estar vacio");
        }
        Long numero = 0L;
        objReturn = new HashMap();
        
        
        //crear lista de errores vacia
        listaErrores = new ArrayList<ErrorValidacionArchivoTipo>();
        if (parametrosValidos) {
            infoNegocio.put("ErrorParametros", "Error en los parametros enviados");
            objReturn.put("listaErrores", listaErrores);
            objReturn.put("infoNegocio", infoNegocio);
            objReturn.put("idEjecucion", numero);
            return objReturn;
        }
        
        resultadoGroovy = new HashMap();
        
        
        System.out.println("::VALIDADOR:: idFormato: " + idFormato);
        System.out.println("::VALIDADOR:: numVersion: " + numVersion);
        //System.out.println("::VALIDADOR:: fecActual: " + new Date());
        

        //Obtener validaciones---------------
        List<ValidacionFormato> listaValidaciones = new ArrayList<ValidacionFormato>();
        try {
            listaValidaciones = validacionFormatoDao.Validaciones(idFormato, numVersion);
            if (listaValidaciones.isEmpty()) {
                listaValidaciones = null;
            }
        } catch (Exception e) {
            System.out.println("::VALIDADOR:: Exception: " + "Error al consultar validaciones:" + e.toString());
            infoNegocio.put("consultaValidaciones", "Error al consultar validaciones:" + e.toString());
        }
        try {
            if (listaValidaciones != null && !listaValidaciones.isEmpty()) {
                numero = intanciaEjecucion(idFormato, numVersion, nombreArchivo, transacional);
                if (numero > 0) {
                    int contador = 0;
                    for (ValidacionFormato validacion : listaValidaciones) {
                        
                        contador++;
                        
                        if(listaErrores.size() < limiteErrores)
                        {
                            //System.out.println("::VALIDADOR ANDRES1:: listaErrores.size(): " + listaErrores.size());
                            
                            if (validacion.getCodTipoValidacion().equals(TIPOVALIDACIONGROOVY)) 
                            {
                                try {
                                    archivo.reset();
                                    Long idValidacion = validacion.getId();
                                    String scriptGroovy = binaryToString((byte[]) validacion.getValScript());

                                    if (scriptGroovy.isEmpty() || scriptGroovy.length() < 1) {
                                        infoNegocio.put("ScriptValidacion-" + idValidacion, "La validacion " + validacion.getNombreClase() + " esta vacia");
                                        continue;
                                    }

                                    //retorno un objeto de tipo lista con lista de errores y el infonegocio que se envian
                                    resultadoGroovy = ejecutarvalidacion(idValidacion, scriptGroovy, transacional, archivo, infoNegocio, listaErrores);

                                    if (resultadoGroovy != null) 
                                    {
                                            //se toma el indice de List
                                            listaErrores = (List) resultadoGroovy.get("listaErrores"); // Poner en constantes los key
                                            infoNegocio = (Map) resultadoGroovy.get("infoNegocio");

                                            if(infoNegocio.containsKey("keyerror")){
                                                break;
                                            }
                                    }
                                } catch (Exception ex) {
                                    infoNegocio.put("CicloValidacion-" + contador, "Se presento el error:" + ex);
                                }
                                //if(contador == 1)break;
                            } 
                            else 
                            {
                                String nombreClase = validacion.getNombreClase();
                                List<ErrorTipo> errorTipo = new ArrayList();
                                infoNegocio.put("ValidacionJava-" + contador, "Id validacion java " + validacion.getId());
                                try {
                                    Workbook wb = WorkbookFactory.create(archivo);
                                    try {
                                        validacionFormatoFacade = (ValidacionFormatoFacade) this.obtenerInstanciaValidacionFormato(nombreClase).newInstance();
                                        validacionFormatoFacade.validarEstructura(wb, errorTipo);

                                        int numElementos = errorTipo.size();
                                        //System.out.println("::VALIDADOR:: El ArrayList tiene " + numElementos + " elementos");

                                    } catch (AppException e) {
                                        infoNegocio.put("Error catch", e);
                                    } catch (IllegalAccessException e) {
                                        infoNegocio.put("Error catch", e);
                                    } catch (InstantiationException e) {
                                        infoNegocio.put("Error catch", e);
                                    } catch (Exception e) {
                                        infoNegocio.put("Error catch", e);
                                    }
                                } catch (RuntimeException ex) {
                                    infoNegocio.put("Error catch", ex);
                                } catch (IOException ex) {
                                    infoNegocio.put("Error catch", ex);
                                } catch (InvalidFormatException ex) {
                                    infoNegocio.put("Error catch", ex);
                                }
                                infoNegocio.put("CicloValidacion-" + contador, "validacion java");
                            }
                        }
                    }
                    //archivo = null;
                    //System.out.println("::VALIDADOR:: Fin ciclos inicio registro errores");
                    //Registrar lista de errores
                    registrarErrores(listaErrores, numero, transacional, infoNegocio);
                    //Actualizar la finalizacion de las validaciones
                    if (finejecucion(numero, transacional)) {
                        System.out.println("::VALIDADOR:: PROCESO_VALIDACION: " + "COMPLETO");
                        infoNegocio.put("PROCESO_VALIDACION", "COMPLETO");
                    } else {
                        System.out.println("::VALIDADOR:: idEjecucionValidacion: " + "Error al finalizar la ejecucion");
                        infoNegocio.put("idEjecucionValidacion: ", "Error al finalizar la ejecucion");
                    }
                } else {
                    System.out.println("::VALIDADOR:: ErrorInstanciaEjecucion: " + "Fallo el Inicio de la Ejecucion de las Validaciones");
                    infoNegocio.put("ErrorInstanciaEjecucion ", "Fallo el Inicio de la Ejecucion de las Validaciones");
                }
            } else {
                System.out.println("::VALIDADOR:: ListaValidaciones: " + "No hay validaciones para la informacion cargada");
                infoNegocio.put("ListaValidaciones", " No hay validaciones para la informacion cargada");
            }
        } catch (Exception e) {
            
            System.out.println("::VALIDADOR:: Exception: " + e.getMessage());
            e.printStackTrace();
            
            objReturn.put("listaErrores", listaErrores);
            objReturn.put("infoNegocio", infoNegocio);
            objReturn.put("idEjecucion", numero);
            return objReturn;
        }
        
        objReturn.put("listaErrores", listaErrores);
        objReturn.put("infoNegocio", infoNegocio);
        objReturn.put("idEjecucion", numero);
        
        
        //System.out.println("::VALIDADOR:: listaErrores.size(): " + listaErrores.size());
        //System.out.println("::VALIDADOR:: infoNegocio.size(): " + infoNegocio.size());
        //System.out.println("::VALIDADOR:: idEjecucion: " + numero);
        
        return objReturn;
    }

    /**
     *
     * @param listaErrores
     * @param numero
     * @param transacional
     * @param infoNegocio
     * @return List<List>
     */
    private void registrarErrores(List<ErrorValidacionArchivoTipo> listaErrores, Long numero, String transacional, Map infoNegocio) {
        int contador = 0;
        ErrorValidacion errorvalidacion;
        if (listaErrores.size() > 0) {
            for (ErrorValidacionArchivoTipo fila : listaErrores) 
            {
                contador++;
                
                try 
                {
                    errorvalidacion = new ErrorValidacion();
                    errorvalidacion.setIdEjecucionValidacion(numero);
                    errorvalidacion.setIdValidacion(fila.getIdValidacion());
                    errorvalidacion.setCodError(fila.getCodError());
                    errorvalidacion.setValDescError(fila.getValDescError());
                    errorvalidacion.setValUbicacionError(fila.getValDescErrorTecnico());
                    errorvalidacion.setIdUsuarioCreacion(transacional);
                    errorvalidaciondao.create(errorvalidacion);
                } catch (AppException ex) {
                    infoNegocio.put("ErrorValidacion-" + contador, "Se presento el error:" + ex.toString());
                }
                
                
                if(contador == limiteErrores)
                {
                    //System.out.println("::VALIDADOR:: Se llego al Limite de registro de errores: " + contador);
                    break;
                }
            }
        }
    }

    /**
     *
     * @param idValidacion id de lavalidacion
     * @param groovy codigo del script
     * @param transacional usuario
     * @param archivo stream del excel a validar
     * @param infoNegocio map
     * @param listError listade errores
     * @return null o lista errores y map
     */
    private Map ejecutarvalidacion(Long idValidacion, String groovy, String transacional, InputStream archivo, Map infoNegocio, List<ErrorValidacionArchivoTipo> listError) {
        GroovyClassLoader classLoader = new GroovyClassLoader();
        try {
            Class gro = classLoader.parseClass(groovy);
            GroovyObject groovyObj = (GroovyObject) gro.newInstance();// SE CREA Y EJECUTA EN MEMORIA EL GROOVY
            return (Map) groovyObj.invokeMethod("ejecutar", new Object[]{archivo, infoNegocio, listError, idValidacion});
        } catch (IllegalArgumentException ex) {
            infoNegocio.put("IllegalArgumentException", "Error en ejecucion:" + ex.toString());
        } catch (InstantiationException ex) {
            infoNegocio.put("InstantiationException", "Error en ejecucion:" + ex.toString());
        } catch (IllegalAccessException ex) {
            infoNegocio.put("IllegalAccessException", "Error en ejecucion:" + ex.toString());
        } catch (CompilationFailedException e) {
            infoNegocio.put("CompilationFailedException", "Error en ejecucion:" + e.toString());
        } catch (GroovyCastException e) {
            infoNegocio.put("GroovyCastException", "Error en ejecucion:" + e.toString());
        } catch (ClassCastException e) {
            infoNegocio.put("ClassCastException", "Error en ejecucion:" + e.toString());
        } catch (Exception e) {
            infoNegocio.put("Exception", "Error en ejecucion:" + e.toString());
        }
        return null;
    }

    /**
     *
     * @param binario
     * @return String
     */
    private String binaryToString(byte[] binario) {
        try {
            return IOUtils.toString(binario);
        } catch (IOException ex) {
            return "";
        }
    }

    /**
     *
     * @param idFormato
     * @param numVersion
     * @param nombreArchivo
     * @param transacional
     * @return Long
     */
    private Long intanciaEjecucion(Long idFormato, Long numVersion, String nombreArchivo, String transacional) {
        unReg = new InstanciaEjecucionValidacion();
        unReg.setValNombreArchivo(nombreArchivo);
        unReg.setIdUsuarioCreacion(transacional);
        FormatoPK pk = new FormatoPK(idFormato, numVersion);
        Formato formato = new Formato();
        formato.setId(pk);
        unReg.setFormato(formato);
        Long numero = 0L;
        try {
            numero = instanciaEjecucionValidacionDao.create(unReg);
        } catch (Exception e) {
            System.out.println("Error entidad:" + e.toString());
        }
        return numero;
    }

    /**
     *
     * @param id
     * @param transacional
     * @return boolean
     */
    private boolean finejecucion(Long id, String transacional) {
        Calendar fechaCalendario = Calendar.getInstance();
        try {
            unReg = instanciaEjecucionValidacionDao.find(id);
            unReg.setIdUsuarioModificacion(transacional);
            unReg.setFechaModificacion(fechaCalendario);
            instanciaEjecucionValidacionDao.edit(unReg);
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
    }

    /**
     * Obtiene la clase que se debe utilizar para realizar la validación del
     * formato, se invoca durante la operacion validarRespuestaInformacion una
     * vez se obtiene el objeto validación formato de la base de datos
     *
     * @param validacionFormato contiene el nombre de la clase a utilizar
     * @return la clase que implementa la interfaz ValidacionFormatoFacade
     * @throws AppException Excepción que almacena la pila de errores y se
     * retorna a la capa de servicios
     */
    private Class obtenerInstanciaValidacionFormato(String rutaClass) throws AppException {
       // ClassLoader classLoader = RespuestaInformacionFacade.class.getClassLoader();

        //Class loadedClass = null;
        try {
            return Class.forName(rutaClass);
            //loadedClass = classLoader.loadClass(rutaClass);
        } catch (ClassNotFoundException cnf) {
            System.out.println(cnf);
            throw new AppException(ErrorEnum.ERROR_NO_ESPERADO.getMessage(), cnf);
        }
        //return loadedClass;

    }
}
