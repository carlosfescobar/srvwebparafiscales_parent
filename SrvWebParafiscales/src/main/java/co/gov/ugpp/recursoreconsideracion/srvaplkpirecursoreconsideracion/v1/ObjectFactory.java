
package co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarRecursoReconsideracionKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", "OpConsultarRecursoReconsideracionKPIFallo");
    private final static QName _OpConsultarRecursoReconsideracionKPISol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", "OpConsultarRecursoReconsideracionKPISol");
    private final static QName _OpConsultarRecursoReconsideracionKPIResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", "OpConsultarRecursoReconsideracionKPIResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarRecursoReconsideracionKPISolTipo }
     * 
     */
    public OpConsultarRecursoReconsideracionKPISolTipo createOpConsultarRecursoReconsideracionKPISolTipo() {
        return new OpConsultarRecursoReconsideracionKPISolTipo();
    }

    /**
     * Create an instance of {@link OpConsultarRecursoReconsideracionKPIRespTipo }
     * 
     */
    public OpConsultarRecursoReconsideracionKPIRespTipo createOpConsultarRecursoReconsideracionKPIRespTipo() {
        return new OpConsultarRecursoReconsideracionKPIRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", name = "OpConsultarRecursoReconsideracionKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarRecursoReconsideracionKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarRecursoReconsideracionKPIFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarRecursoReconsideracionKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", name = "OpConsultarRecursoReconsideracionKPISol")
    public JAXBElement<OpConsultarRecursoReconsideracionKPISolTipo> createOpConsultarRecursoReconsideracionKPISol(OpConsultarRecursoReconsideracionKPISolTipo value) {
        return new JAXBElement<OpConsultarRecursoReconsideracionKPISolTipo>(_OpConsultarRecursoReconsideracionKPISol_QNAME, OpConsultarRecursoReconsideracionKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarRecursoReconsideracionKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplKPIRecursoReconsideracion/v1", name = "OpConsultarRecursoReconsideracionKPIResp")
    public JAXBElement<OpConsultarRecursoReconsideracionKPIRespTipo> createOpConsultarRecursoReconsideracionKPIResp(OpConsultarRecursoReconsideracionKPIRespTipo value) {
        return new JAXBElement<OpConsultarRecursoReconsideracionKPIRespTipo>(_OpConsultarRecursoReconsideracionKPIResp_QNAME, OpConsultarRecursoReconsideracionKPIRespTipo.class, null, value);
    }

}
