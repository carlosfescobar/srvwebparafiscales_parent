
package co.gov.ugpp.recursoreconsideracion.srvaplkpirecursoreconsideracion.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1.RecursoReconsideracionTipo;


/**
 * <p>Clase Java para OpConsultarRecursoReconsideracionKPIRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpConsultarRecursoReconsideracionKPIRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="cantidadRecursos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="recursosReconsideracion" type="{http://www.ugpp.gov.co/schema/RecursoReconsideracion/RecursoReconsideracionTipo/v1}RecursoReconsideracionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpConsultarRecursoReconsideracionKPIRespTipo", propOrder = {
    "contextoRespuesta",
    "cantidadRecursos",
    "recursosReconsideracion"
})
public class OpConsultarRecursoReconsideracionKPIRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected String cantidadRecursos;
    protected List<RecursoReconsideracionTipo> recursosReconsideracion;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadRecursos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadRecursos() {
        return cantidadRecursos;
    }

    /**
     * Define el valor de la propiedad cantidadRecursos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadRecursos(String value) {
        this.cantidadRecursos = value;
    }

    /**
     * Gets the value of the recursosReconsideracion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the recursosReconsideracion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRecursosReconsideracion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RecursoReconsideracionTipo }
     * 
     * 
     */
    public List<RecursoReconsideracionTipo> getRecursosReconsideracion() {
        if (recursosReconsideracion == null) {
            recursosReconsideracion = new ArrayList<RecursoReconsideracionTipo>();
        }
        return this.recursosReconsideracion;
    }

}
