
package co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpCrearPruebaResp");
    private final static QName _OpBuscarPorIdPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpBuscarPorIdPruebaSol");
    private final static QName _OpCrearPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpCrearPruebaFallo");
    private final static QName _OpActualizarPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpActualizarPruebaResp");
    private final static QName _OpActualizarPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpActualizarPruebaFallo");
    private final static QName _OpBuscarPorIdPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpBuscarPorIdPruebaResp");
    private final static QName _OpCrearPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpCrearPruebaSol");
    private final static QName _OpBuscarPorIdPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpBuscarPorIdPruebaFallo");
    private final static QName _OpActualizarPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", "OpActualizarPruebaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.recursoreconsideracion.srvaplprueba.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPruebaSolTipo }
     * 
     */
    public OpBuscarPorIdPruebaSolTipo createOpBuscarPorIdPruebaSolTipo() {
        return new OpBuscarPorIdPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearPruebaRespTipo }
     * 
     */
    public OpCrearPruebaRespTipo createOpCrearPruebaRespTipo() {
        return new OpCrearPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPruebaRespTipo }
     * 
     */
    public OpBuscarPorIdPruebaRespTipo createOpBuscarPorIdPruebaRespTipo() {
        return new OpBuscarPorIdPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarPruebaRespTipo }
     * 
     */
    public OpActualizarPruebaRespTipo createOpActualizarPruebaRespTipo() {
        return new OpActualizarPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearPruebaSolTipo }
     * 
     */
    public OpCrearPruebaSolTipo createOpCrearPruebaSolTipo() {
        return new OpCrearPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarPruebaSolTipo }
     * 
     */
    public OpActualizarPruebaSolTipo createOpActualizarPruebaSolTipo() {
        return new OpActualizarPruebaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpCrearPruebaResp")
    public JAXBElement<OpCrearPruebaRespTipo> createOpCrearPruebaResp(OpCrearPruebaRespTipo value) {
        return new JAXBElement<OpCrearPruebaRespTipo>(_OpCrearPruebaResp_QNAME, OpCrearPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpBuscarPorIdPruebaSol")
    public JAXBElement<OpBuscarPorIdPruebaSolTipo> createOpBuscarPorIdPruebaSol(OpBuscarPorIdPruebaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdPruebaSolTipo>(_OpBuscarPorIdPruebaSol_QNAME, OpBuscarPorIdPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpCrearPruebaFallo")
    public JAXBElement<FalloTipo> createOpCrearPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpActualizarPruebaResp")
    public JAXBElement<OpActualizarPruebaRespTipo> createOpActualizarPruebaResp(OpActualizarPruebaRespTipo value) {
        return new JAXBElement<OpActualizarPruebaRespTipo>(_OpActualizarPruebaResp_QNAME, OpActualizarPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpActualizarPruebaFallo")
    public JAXBElement<FalloTipo> createOpActualizarPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpBuscarPorIdPruebaResp")
    public JAXBElement<OpBuscarPorIdPruebaRespTipo> createOpBuscarPorIdPruebaResp(OpBuscarPorIdPruebaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdPruebaRespTipo>(_OpBuscarPorIdPruebaResp_QNAME, OpBuscarPorIdPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpCrearPruebaSol")
    public JAXBElement<OpCrearPruebaSolTipo> createOpCrearPruebaSol(OpCrearPruebaSolTipo value) {
        return new JAXBElement<OpCrearPruebaSolTipo>(_OpCrearPruebaSol_QNAME, OpCrearPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpBuscarPorIdPruebaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplPrueba/v1", name = "OpActualizarPruebaSol")
    public JAXBElement<OpActualizarPruebaSolTipo> createOpActualizarPruebaSol(OpActualizarPruebaSolTipo value) {
        return new JAXBElement<OpActualizarPruebaSolTipo>(_OpActualizarPruebaSol_QNAME, OpActualizarPruebaSolTipo.class, null, value);
    }

}
