
package co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdRecursoReconsideracionSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpBuscarPorIdRecursoReconsideracionSol");
    private final static QName _OpBuscarPorIdRecursoReconsideracionResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpBuscarPorIdRecursoReconsideracionResp");
    private final static QName _OpActualizarRecursoReconsideracionFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpActualizarRecursoReconsideracionFallo");
    private final static QName _OpCrearRecursoReconsideracionSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpCrearRecursoReconsideracionSol");
    private final static QName _OpCrearRecursoReconsideracionFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpCrearRecursoReconsideracionFallo");
    private final static QName _OpCrearRecursoReconsideracionResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpCrearRecursoReconsideracionResp");
    private final static QName _OpBuscarPorIdRecursoReconsideracionFallo_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpBuscarPorIdRecursoReconsideracionFallo");
    private final static QName _OpActualizarRecursoReconsideracionSol_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpActualizarRecursoReconsideracionSol");
    private final static QName _OpActualizarRecursoReconsideracionResp_QNAME = new QName("http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", "OpActualizarRecursoReconsideracionResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.recursoreconsideracion.srvaplrecursoreconsideracion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearRecursoReconsideracionSolTipo }
     * 
     */
    public OpCrearRecursoReconsideracionSolTipo createOpCrearRecursoReconsideracionSolTipo() {
        return new OpCrearRecursoReconsideracionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearRecursoReconsideracionRespTipo }
     * 
     */
    public OpCrearRecursoReconsideracionRespTipo createOpCrearRecursoReconsideracionRespTipo() {
        return new OpCrearRecursoReconsideracionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRecursoReconsideracionRespTipo }
     * 
     */
    public OpActualizarRecursoReconsideracionRespTipo createOpActualizarRecursoReconsideracionRespTipo() {
        return new OpActualizarRecursoReconsideracionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarRecursoReconsideracionSolTipo }
     * 
     */
    public OpActualizarRecursoReconsideracionSolTipo createOpActualizarRecursoReconsideracionSolTipo() {
        return new OpActualizarRecursoReconsideracionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRecursoReconsideracionRespTipo }
     * 
     */
    public OpBuscarPorIdRecursoReconsideracionRespTipo createOpBuscarPorIdRecursoReconsideracionRespTipo() {
        return new OpBuscarPorIdRecursoReconsideracionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdRecursoReconsideracionSolTipo }
     * 
     */
    public OpBuscarPorIdRecursoReconsideracionSolTipo createOpBuscarPorIdRecursoReconsideracionSolTipo() {
        return new OpBuscarPorIdRecursoReconsideracionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRecursoReconsideracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpBuscarPorIdRecursoReconsideracionSol")
    public JAXBElement<OpBuscarPorIdRecursoReconsideracionSolTipo> createOpBuscarPorIdRecursoReconsideracionSol(OpBuscarPorIdRecursoReconsideracionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdRecursoReconsideracionSolTipo>(_OpBuscarPorIdRecursoReconsideracionSol_QNAME, OpBuscarPorIdRecursoReconsideracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdRecursoReconsideracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpBuscarPorIdRecursoReconsideracionResp")
    public JAXBElement<OpBuscarPorIdRecursoReconsideracionRespTipo> createOpBuscarPorIdRecursoReconsideracionResp(OpBuscarPorIdRecursoReconsideracionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdRecursoReconsideracionRespTipo>(_OpBuscarPorIdRecursoReconsideracionResp_QNAME, OpBuscarPorIdRecursoReconsideracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpActualizarRecursoReconsideracionFallo")
    public JAXBElement<FalloTipo> createOpActualizarRecursoReconsideracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarRecursoReconsideracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRecursoReconsideracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpCrearRecursoReconsideracionSol")
    public JAXBElement<OpCrearRecursoReconsideracionSolTipo> createOpCrearRecursoReconsideracionSol(OpCrearRecursoReconsideracionSolTipo value) {
        return new JAXBElement<OpCrearRecursoReconsideracionSolTipo>(_OpCrearRecursoReconsideracionSol_QNAME, OpCrearRecursoReconsideracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpCrearRecursoReconsideracionFallo")
    public JAXBElement<FalloTipo> createOpCrearRecursoReconsideracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearRecursoReconsideracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearRecursoReconsideracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpCrearRecursoReconsideracionResp")
    public JAXBElement<OpCrearRecursoReconsideracionRespTipo> createOpCrearRecursoReconsideracionResp(OpCrearRecursoReconsideracionRespTipo value) {
        return new JAXBElement<OpCrearRecursoReconsideracionRespTipo>(_OpCrearRecursoReconsideracionResp_QNAME, OpCrearRecursoReconsideracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpBuscarPorIdRecursoReconsideracionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdRecursoReconsideracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdRecursoReconsideracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRecursoReconsideracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpActualizarRecursoReconsideracionSol")
    public JAXBElement<OpActualizarRecursoReconsideracionSolTipo> createOpActualizarRecursoReconsideracionSol(OpActualizarRecursoReconsideracionSolTipo value) {
        return new JAXBElement<OpActualizarRecursoReconsideracionSolTipo>(_OpActualizarRecursoReconsideracionSol_QNAME, OpActualizarRecursoReconsideracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarRecursoReconsideracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/RecursoReconsideracion/SrvAplRecursoReconsideracion/v1", name = "OpActualizarRecursoReconsideracionResp")
    public JAXBElement<OpActualizarRecursoReconsideracionRespTipo> createOpActualizarRecursoReconsideracionResp(OpActualizarRecursoReconsideracionRespTipo value) {
        return new JAXBElement<OpActualizarRecursoReconsideracionRespTipo>(_OpActualizarRecursoReconsideracionResp_QNAME, OpActualizarRecursoReconsideracionRespTipo.class, null, value);
    }

}
