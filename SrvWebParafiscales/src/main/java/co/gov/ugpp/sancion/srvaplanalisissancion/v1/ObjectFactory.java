
package co.gov.ugpp.sancion.srvaplanalisissancion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sancion.srvaplanalisissancion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearAnalisisSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpCrearAnalisisSancionSol");
    private final static QName _OpBuscarPorIdAnalisisSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorIdAnalisisSancionFallo");
    private final static QName _OpActualizarAnalisisSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpActualizarAnalisisSancionResp");
    private final static QName _OpActualizarAnalisisSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpActualizarAnalisisSancionSol");
    private final static QName _OpBuscarPorIdAnalisisSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorIdAnalisisSancionResp");
    private final static QName _OpActualizarAnalisisSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpActualizarAnalisisSancionFallo");
    private final static QName _OpBuscarPorCriteriosAnalisisSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorCriteriosAnalisisSancionSol");
    private final static QName _OpCrearAnalisisSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpCrearAnalisisSancionResp");
    private final static QName _OpBuscarPorCriteriosAnalisisSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorCriteriosAnalisisSancionResp");
    private final static QName _OpBuscarPorCriteriosAnalisisSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorCriteriosAnalisisSancionFallo");
    private final static QName _OpBuscarPorIdAnalisisSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpBuscarPorIdAnalisisSancionSol");
    private final static QName _OpCrearAnalisisSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", "OpCrearAnalisisSancionFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sancion.srvaplanalisissancion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAnalisisSancionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAnalisisSancionSolTipo createOpBuscarPorCriteriosAnalisisSancionSolTipo() {
        return new OpBuscarPorCriteriosAnalisisSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAnalisisSancionRespTipo }
     * 
     */
    public OpCrearAnalisisSancionRespTipo createOpCrearAnalisisSancionRespTipo() {
        return new OpCrearAnalisisSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAnalisisSancionSolTipo }
     * 
     */
    public OpBuscarPorIdAnalisisSancionSolTipo createOpBuscarPorIdAnalisisSancionSolTipo() {
        return new OpBuscarPorIdAnalisisSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAnalisisSancionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAnalisisSancionRespTipo createOpBuscarPorCriteriosAnalisisSancionRespTipo() {
        return new OpBuscarPorCriteriosAnalisisSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAnalisisSancionRespTipo }
     * 
     */
    public OpActualizarAnalisisSancionRespTipo createOpActualizarAnalisisSancionRespTipo() {
        return new OpActualizarAnalisisSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAnalisisSancionSolTipo }
     * 
     */
    public OpCrearAnalisisSancionSolTipo createOpCrearAnalisisSancionSolTipo() {
        return new OpCrearAnalisisSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAnalisisSancionSolTipo }
     * 
     */
    public OpActualizarAnalisisSancionSolTipo createOpActualizarAnalisisSancionSolTipo() {
        return new OpActualizarAnalisisSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdAnalisisSancionRespTipo }
     * 
     */
    public OpBuscarPorIdAnalisisSancionRespTipo createOpBuscarPorIdAnalisisSancionRespTipo() {
        return new OpBuscarPorIdAnalisisSancionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAnalisisSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpCrearAnalisisSancionSol")
    public JAXBElement<OpCrearAnalisisSancionSolTipo> createOpCrearAnalisisSancionSol(OpCrearAnalisisSancionSolTipo value) {
        return new JAXBElement<OpCrearAnalisisSancionSolTipo>(_OpCrearAnalisisSancionSol_QNAME, OpCrearAnalisisSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorIdAnalisisSancionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdAnalisisSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdAnalisisSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAnalisisSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpActualizarAnalisisSancionResp")
    public JAXBElement<OpActualizarAnalisisSancionRespTipo> createOpActualizarAnalisisSancionResp(OpActualizarAnalisisSancionRespTipo value) {
        return new JAXBElement<OpActualizarAnalisisSancionRespTipo>(_OpActualizarAnalisisSancionResp_QNAME, OpActualizarAnalisisSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAnalisisSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpActualizarAnalisisSancionSol")
    public JAXBElement<OpActualizarAnalisisSancionSolTipo> createOpActualizarAnalisisSancionSol(OpActualizarAnalisisSancionSolTipo value) {
        return new JAXBElement<OpActualizarAnalisisSancionSolTipo>(_OpActualizarAnalisisSancionSol_QNAME, OpActualizarAnalisisSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAnalisisSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorIdAnalisisSancionResp")
    public JAXBElement<OpBuscarPorIdAnalisisSancionRespTipo> createOpBuscarPorIdAnalisisSancionResp(OpBuscarPorIdAnalisisSancionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdAnalisisSancionRespTipo>(_OpBuscarPorIdAnalisisSancionResp_QNAME, OpBuscarPorIdAnalisisSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpActualizarAnalisisSancionFallo")
    public JAXBElement<FalloTipo> createOpActualizarAnalisisSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAnalisisSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAnalisisSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorCriteriosAnalisisSancionSol")
    public JAXBElement<OpBuscarPorCriteriosAnalisisSancionSolTipo> createOpBuscarPorCriteriosAnalisisSancionSol(OpBuscarPorCriteriosAnalisisSancionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAnalisisSancionSolTipo>(_OpBuscarPorCriteriosAnalisisSancionSol_QNAME, OpBuscarPorCriteriosAnalisisSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAnalisisSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpCrearAnalisisSancionResp")
    public JAXBElement<OpCrearAnalisisSancionRespTipo> createOpCrearAnalisisSancionResp(OpCrearAnalisisSancionRespTipo value) {
        return new JAXBElement<OpCrearAnalisisSancionRespTipo>(_OpCrearAnalisisSancionResp_QNAME, OpCrearAnalisisSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAnalisisSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorCriteriosAnalisisSancionResp")
    public JAXBElement<OpBuscarPorCriteriosAnalisisSancionRespTipo> createOpBuscarPorCriteriosAnalisisSancionResp(OpBuscarPorCriteriosAnalisisSancionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAnalisisSancionRespTipo>(_OpBuscarPorCriteriosAnalisisSancionResp_QNAME, OpBuscarPorCriteriosAnalisisSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorCriteriosAnalisisSancionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAnalisisSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAnalisisSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdAnalisisSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpBuscarPorIdAnalisisSancionSol")
    public JAXBElement<OpBuscarPorIdAnalisisSancionSolTipo> createOpBuscarPorIdAnalisisSancionSol(OpBuscarPorIdAnalisisSancionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdAnalisisSancionSolTipo>(_OpBuscarPorIdAnalisisSancionSol_QNAME, OpBuscarPorIdAnalisisSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplAnalisisSancion/v1", name = "OpCrearAnalisisSancionFallo")
    public JAXBElement<FalloTipo> createOpCrearAnalisisSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAnalisisSancionFallo_QNAME, FalloTipo.class, null, value);
    }

}
