
package co.gov.ugpp.sancion.srvaplanalisissancion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;


/**
 * <p>Clase Java para OpActualizarAnalisisSancionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarAnalisisSancionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="analisisSancion" type="{http://www.ugpp.gov.co/schema/Sancion/AnalisisSancionTipo/v1}AnalisisSancionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarAnalisisSancionSolTipo", propOrder = {
    "contextoTransaccional",
    "analisisSancion"
})
public class OpActualizarAnalisisSancionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AnalisisSancionTipo analisisSancion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad analisisSancion.
     * 
     * @return
     *     possible object is
     *     {@link AnalisisSancionTipo }
     *     
     */
    public AnalisisSancionTipo getAnalisisSancion() {
        return analisisSancion;
    }

    /**
     * Define el valor de la propiedad analisisSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link AnalisisSancionTipo }
     *     
     */
    public void setAnalisisSancion(AnalisisSancionTipo value) {
        this.analisisSancion = value;
    }

}
