
package co.gov.ugpp.sancion.srvaplinformacionprueba.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sancion.srvaplinformacionprueba.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearInformacionPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpCrearInformacionPruebaSol");
    private final static QName _OpBuscarPorCriteriosInformacionPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorCriteriosInformacionPruebaFallo");
    private final static QName _OpBuscarPorIdInformacionPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorIdInformacionPruebaSol");
    private final static QName _OpCrearInformacionPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpCrearInformacionPruebaResp");
    private final static QName _OpBuscarPorIdInformacionPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorIdInformacionPruebaResp");
    private final static QName _OpBuscarPorCriteriosInformacionPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorCriteriosInformacionPruebaSol");
    private final static QName _OpActualizarInformacionPruebaSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpActualizarInformacionPruebaSol");
    private final static QName _OpActualizarInformacionPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpActualizarInformacionPruebaResp");
    private final static QName _OpBuscarPorIdInformacionPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorIdInformacionPruebaFallo");
    private final static QName _OpActualizarInformacionPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpActualizarInformacionPruebaFallo");
    private final static QName _OpCrearInformacionPruebaFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpCrearInformacionPruebaFallo");
    private final static QName _OpBuscarPorCriteriosInformacionPruebaResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", "OpBuscarPorCriteriosInformacionPruebaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sancion.srvaplinformacionprueba.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosInformacionPruebaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosInformacionPruebaSolTipo createOpBuscarPorCriteriosInformacionPruebaSolTipo() {
        return new OpBuscarPorCriteriosInformacionPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInformacionPruebaRespTipo }
     * 
     */
    public OpBuscarPorIdInformacionPruebaRespTipo createOpBuscarPorIdInformacionPruebaRespTipo() {
        return new OpBuscarPorIdInformacionPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearInformacionPruebaRespTipo }
     * 
     */
    public OpCrearInformacionPruebaRespTipo createOpCrearInformacionPruebaRespTipo() {
        return new OpCrearInformacionPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInformacionPruebaSolTipo }
     * 
     */
    public OpBuscarPorIdInformacionPruebaSolTipo createOpBuscarPorIdInformacionPruebaSolTipo() {
        return new OpBuscarPorIdInformacionPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearInformacionPruebaSolTipo }
     * 
     */
    public OpCrearInformacionPruebaSolTipo createOpCrearInformacionPruebaSolTipo() {
        return new OpCrearInformacionPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarInformacionPruebaRespTipo }
     * 
     */
    public OpActualizarInformacionPruebaRespTipo createOpActualizarInformacionPruebaRespTipo() {
        return new OpActualizarInformacionPruebaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarInformacionPruebaSolTipo }
     * 
     */
    public OpActualizarInformacionPruebaSolTipo createOpActualizarInformacionPruebaSolTipo() {
        return new OpActualizarInformacionPruebaSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosInformacionPruebaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosInformacionPruebaRespTipo createOpBuscarPorCriteriosInformacionPruebaRespTipo() {
        return new OpBuscarPorCriteriosInformacionPruebaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearInformacionPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpCrearInformacionPruebaSol")
    public JAXBElement<OpCrearInformacionPruebaSolTipo> createOpCrearInformacionPruebaSol(OpCrearInformacionPruebaSolTipo value) {
        return new JAXBElement<OpCrearInformacionPruebaSolTipo>(_OpCrearInformacionPruebaSol_QNAME, OpCrearInformacionPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorCriteriosInformacionPruebaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosInformacionPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosInformacionPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInformacionPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorIdInformacionPruebaSol")
    public JAXBElement<OpBuscarPorIdInformacionPruebaSolTipo> createOpBuscarPorIdInformacionPruebaSol(OpBuscarPorIdInformacionPruebaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdInformacionPruebaSolTipo>(_OpBuscarPorIdInformacionPruebaSol_QNAME, OpBuscarPorIdInformacionPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearInformacionPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpCrearInformacionPruebaResp")
    public JAXBElement<OpCrearInformacionPruebaRespTipo> createOpCrearInformacionPruebaResp(OpCrearInformacionPruebaRespTipo value) {
        return new JAXBElement<OpCrearInformacionPruebaRespTipo>(_OpCrearInformacionPruebaResp_QNAME, OpCrearInformacionPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInformacionPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorIdInformacionPruebaResp")
    public JAXBElement<OpBuscarPorIdInformacionPruebaRespTipo> createOpBuscarPorIdInformacionPruebaResp(OpBuscarPorIdInformacionPruebaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdInformacionPruebaRespTipo>(_OpBuscarPorIdInformacionPruebaResp_QNAME, OpBuscarPorIdInformacionPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosInformacionPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorCriteriosInformacionPruebaSol")
    public JAXBElement<OpBuscarPorCriteriosInformacionPruebaSolTipo> createOpBuscarPorCriteriosInformacionPruebaSol(OpBuscarPorCriteriosInformacionPruebaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosInformacionPruebaSolTipo>(_OpBuscarPorCriteriosInformacionPruebaSol_QNAME, OpBuscarPorCriteriosInformacionPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarInformacionPruebaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpActualizarInformacionPruebaSol")
    public JAXBElement<OpActualizarInformacionPruebaSolTipo> createOpActualizarInformacionPruebaSol(OpActualizarInformacionPruebaSolTipo value) {
        return new JAXBElement<OpActualizarInformacionPruebaSolTipo>(_OpActualizarInformacionPruebaSol_QNAME, OpActualizarInformacionPruebaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarInformacionPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpActualizarInformacionPruebaResp")
    public JAXBElement<OpActualizarInformacionPruebaRespTipo> createOpActualizarInformacionPruebaResp(OpActualizarInformacionPruebaRespTipo value) {
        return new JAXBElement<OpActualizarInformacionPruebaRespTipo>(_OpActualizarInformacionPruebaResp_QNAME, OpActualizarInformacionPruebaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorIdInformacionPruebaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdInformacionPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdInformacionPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpActualizarInformacionPruebaFallo")
    public JAXBElement<FalloTipo> createOpActualizarInformacionPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarInformacionPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpCrearInformacionPruebaFallo")
    public JAXBElement<FalloTipo> createOpCrearInformacionPruebaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearInformacionPruebaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosInformacionPruebaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplInformacionPrueba/v1", name = "OpBuscarPorCriteriosInformacionPruebaResp")
    public JAXBElement<OpBuscarPorCriteriosInformacionPruebaRespTipo> createOpBuscarPorCriteriosInformacionPruebaResp(OpBuscarPorCriteriosInformacionPruebaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosInformacionPruebaRespTipo>(_OpBuscarPorCriteriosInformacionPruebaResp_QNAME, OpBuscarPorCriteriosInformacionPruebaRespTipo.class, null, value);
    }

}
