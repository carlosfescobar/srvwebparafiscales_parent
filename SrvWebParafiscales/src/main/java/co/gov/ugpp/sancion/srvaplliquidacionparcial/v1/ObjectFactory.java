
package co.gov.ugpp.sancion.srvaplliquidacionparcial.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sancion.srvaplliquidacionparcial.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosLiquidacionParcialSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorCriteriosLiquidacionParcialSol");
    private final static QName _OpActualizarLiquidacionParcialFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpActualizarLiquidacionParcialFallo");
    private final static QName _OpBuscarPorCriteriosLiquidacionParcialFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorCriteriosLiquidacionParcialFallo");
    private final static QName _OpBuscarPorCriteriosLiquidacionParcialResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorCriteriosLiquidacionParcialResp");
    private final static QName _OpCrearLiquidacionParcialFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpCrearLiquidacionParcialFallo");
    private final static QName _OpBuscarPorIdLiquidacionParcialSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorIdLiquidacionParcialSol");
    private final static QName _OpCrearLiquidacionParcialResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpCrearLiquidacionParcialResp");
    private final static QName _OpActualizarLiquidacionParcialSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpActualizarLiquidacionParcialSol");
    private final static QName _OpBuscarPorIdLiquidacionParcialFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorIdLiquidacionParcialFallo");
    private final static QName _OpActualizarLiquidacionParcialResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpActualizarLiquidacionParcialResp");
    private final static QName _OpCrearLiquidacionParcialSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpCrearLiquidacionParcialSol");
    private final static QName _OpBuscarPorIdLiquidacionParcialResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "OpBuscarPorIdLiquidacionParcialResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sancion.srvaplliquidacionparcial.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdLiquidacionParcialRespTipo }
     * 
     */
    public OpBuscarPorIdLiquidacionParcialRespTipo createOpBuscarPorIdLiquidacionParcialRespTipo() {
        return new OpBuscarPorIdLiquidacionParcialRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearLiquidacionParcialSolTipo }
     * 
     */
    public OpCrearLiquidacionParcialSolTipo createOpCrearLiquidacionParcialSolTipo() {
        return new OpCrearLiquidacionParcialSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarLiquidacionParcialRespTipo }
     * 
     */
    public OpActualizarLiquidacionParcialRespTipo createOpActualizarLiquidacionParcialRespTipo() {
        return new OpActualizarLiquidacionParcialRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosLiquidacionParcialRespTipo }
     * 
     */
    public OpBuscarPorCriteriosLiquidacionParcialRespTipo createOpBuscarPorCriteriosLiquidacionParcialRespTipo() {
        return new OpBuscarPorCriteriosLiquidacionParcialRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosLiquidacionParcialSolTipo }
     * 
     */
    public OpBuscarPorCriteriosLiquidacionParcialSolTipo createOpBuscarPorCriteriosLiquidacionParcialSolTipo() {
        return new OpBuscarPorCriteriosLiquidacionParcialSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarLiquidacionParcialSolTipo }
     * 
     */
    public OpActualizarLiquidacionParcialSolTipo createOpActualizarLiquidacionParcialSolTipo() {
        return new OpActualizarLiquidacionParcialSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearLiquidacionParcialRespTipo }
     * 
     */
    public OpCrearLiquidacionParcialRespTipo createOpCrearLiquidacionParcialRespTipo() {
        return new OpCrearLiquidacionParcialRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdLiquidacionParcialSolTipo }
     * 
     */
    public OpBuscarPorIdLiquidacionParcialSolTipo createOpBuscarPorIdLiquidacionParcialSolTipo() {
        return new OpBuscarPorIdLiquidacionParcialSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosLiquidacionParcialSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorCriteriosLiquidacionParcialSol")
    public JAXBElement<OpBuscarPorCriteriosLiquidacionParcialSolTipo> createOpBuscarPorCriteriosLiquidacionParcialSol(OpBuscarPorCriteriosLiquidacionParcialSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosLiquidacionParcialSolTipo>(_OpBuscarPorCriteriosLiquidacionParcialSol_QNAME, OpBuscarPorCriteriosLiquidacionParcialSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpActualizarLiquidacionParcialFallo")
    public JAXBElement<FalloTipo> createOpActualizarLiquidacionParcialFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarLiquidacionParcialFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorCriteriosLiquidacionParcialFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosLiquidacionParcialFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosLiquidacionParcialFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosLiquidacionParcialRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorCriteriosLiquidacionParcialResp")
    public JAXBElement<OpBuscarPorCriteriosLiquidacionParcialRespTipo> createOpBuscarPorCriteriosLiquidacionParcialResp(OpBuscarPorCriteriosLiquidacionParcialRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosLiquidacionParcialRespTipo>(_OpBuscarPorCriteriosLiquidacionParcialResp_QNAME, OpBuscarPorCriteriosLiquidacionParcialRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpCrearLiquidacionParcialFallo")
    public JAXBElement<FalloTipo> createOpCrearLiquidacionParcialFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearLiquidacionParcialFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdLiquidacionParcialSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorIdLiquidacionParcialSol")
    public JAXBElement<OpBuscarPorIdLiquidacionParcialSolTipo> createOpBuscarPorIdLiquidacionParcialSol(OpBuscarPorIdLiquidacionParcialSolTipo value) {
        return new JAXBElement<OpBuscarPorIdLiquidacionParcialSolTipo>(_OpBuscarPorIdLiquidacionParcialSol_QNAME, OpBuscarPorIdLiquidacionParcialSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLiquidacionParcialRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpCrearLiquidacionParcialResp")
    public JAXBElement<OpCrearLiquidacionParcialRespTipo> createOpCrearLiquidacionParcialResp(OpCrearLiquidacionParcialRespTipo value) {
        return new JAXBElement<OpCrearLiquidacionParcialRespTipo>(_OpCrearLiquidacionParcialResp_QNAME, OpCrearLiquidacionParcialRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarLiquidacionParcialSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpActualizarLiquidacionParcialSol")
    public JAXBElement<OpActualizarLiquidacionParcialSolTipo> createOpActualizarLiquidacionParcialSol(OpActualizarLiquidacionParcialSolTipo value) {
        return new JAXBElement<OpActualizarLiquidacionParcialSolTipo>(_OpActualizarLiquidacionParcialSol_QNAME, OpActualizarLiquidacionParcialSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorIdLiquidacionParcialFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdLiquidacionParcialFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdLiquidacionParcialFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarLiquidacionParcialRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpActualizarLiquidacionParcialResp")
    public JAXBElement<OpActualizarLiquidacionParcialRespTipo> createOpActualizarLiquidacionParcialResp(OpActualizarLiquidacionParcialRespTipo value) {
        return new JAXBElement<OpActualizarLiquidacionParcialRespTipo>(_OpActualizarLiquidacionParcialResp_QNAME, OpActualizarLiquidacionParcialRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearLiquidacionParcialSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpCrearLiquidacionParcialSol")
    public JAXBElement<OpCrearLiquidacionParcialSolTipo> createOpCrearLiquidacionParcialSol(OpCrearLiquidacionParcialSolTipo value) {
        return new JAXBElement<OpCrearLiquidacionParcialSolTipo>(_OpCrearLiquidacionParcialSol_QNAME, OpCrearLiquidacionParcialSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdLiquidacionParcialRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", name = "OpBuscarPorIdLiquidacionParcialResp")
    public JAXBElement<OpBuscarPorIdLiquidacionParcialRespTipo> createOpBuscarPorIdLiquidacionParcialResp(OpBuscarPorIdLiquidacionParcialRespTipo value) {
        return new JAXBElement<OpBuscarPorIdLiquidacionParcialRespTipo>(_OpBuscarPorIdLiquidacionParcialResp_QNAME, OpBuscarPorIdLiquidacionParcialRespTipo.class, null, value);
    }

}
