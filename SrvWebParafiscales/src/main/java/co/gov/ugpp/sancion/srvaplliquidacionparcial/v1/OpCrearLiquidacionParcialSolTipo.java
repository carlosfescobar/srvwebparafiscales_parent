
package co.gov.ugpp.sancion.srvaplliquidacionparcial.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;


/**
 * <p>Clase Java para OpCrearLiquidacionParcialSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearLiquidacionParcialSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="liquidacionParcial" type="{http://www.ugpp.gov.co/schema/Sancion/LiquidacionParcialTipo/v1}LiquidacionParcialTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearLiquidacionParcialSolTipo", propOrder = {
    "contextoTransaccional",
    "liquidacionParcial"
})
public class OpCrearLiquidacionParcialSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected LiquidacionParcialTipo liquidacionParcial;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidacionParcial.
     * 
     * @return
     *     possible object is
     *     {@link LiquidacionParcialTipo }
     *     
     */
    public LiquidacionParcialTipo getLiquidacionParcial() {
        return liquidacionParcial;
    }

    /**
     * Define el valor de la propiedad liquidacionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link LiquidacionParcialTipo }
     *     
     */
    public void setLiquidacionParcial(LiquidacionParcialTipo value) {
        this.liquidacionParcial = value;
    }

}
