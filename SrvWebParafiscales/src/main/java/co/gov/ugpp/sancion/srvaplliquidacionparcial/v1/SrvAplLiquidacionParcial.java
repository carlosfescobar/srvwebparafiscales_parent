
package co.gov.ugpp.sancion.srvaplliquidacionparcial.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplLiquidacionParcial", targetNamespace = "http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplLiquidacionParcial?wsdl")
public class SrvAplLiquidacionParcial
    extends Service
{

    private final static URL SRVAPLLIQUIDACIONPARCIAL_WSDL_LOCATION;
    private final static WebServiceException SRVAPLLIQUIDACIONPARCIAL_EXCEPTION;
    private final static QName SRVAPLLIQUIDACIONPARCIAL_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "SrvAplLiquidacionParcial");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplLiquidacionParcial?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLLIQUIDACIONPARCIAL_WSDL_LOCATION = url;
        SRVAPLLIQUIDACIONPARCIAL_EXCEPTION = e;
    }

    public SrvAplLiquidacionParcial() {
        super(__getWsdlLocation(), SRVAPLLIQUIDACIONPARCIAL_QNAME);
    }

    public SrvAplLiquidacionParcial(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLLIQUIDACIONPARCIAL_QNAME, features);
    }

    public SrvAplLiquidacionParcial(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLLIQUIDACIONPARCIAL_QNAME);
    }

    public SrvAplLiquidacionParcial(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLLIQUIDACIONPARCIAL_QNAME, features);
    }

    public SrvAplLiquidacionParcial(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplLiquidacionParcial(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplLiquidacionParcialSOAP
     */
    @WebEndpoint(name = "portSrvAplLiquidacionParcialSOAP")
    public PortSrvAplLiquidacionParcialSOAP getPortSrvAplLiquidacionParcialSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "portSrvAplLiquidacionParcialSOAP"), PortSrvAplLiquidacionParcialSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplLiquidacionParcialSOAP
     */
    @WebEndpoint(name = "portSrvAplLiquidacionParcialSOAP")
    public PortSrvAplLiquidacionParcialSOAP getPortSrvAplLiquidacionParcialSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Sancion/SrvAplLiquidacionParcial/v1", "portSrvAplLiquidacionParcialSOAP"), PortSrvAplLiquidacionParcialSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLLIQUIDACIONPARCIAL_EXCEPTION!= null) {
            throw SRVAPLLIQUIDACIONPARCIAL_EXCEPTION;
        }
        return SRVAPLLIQUIDACIONPARCIAL_WSDL_LOCATION;
    }

}
