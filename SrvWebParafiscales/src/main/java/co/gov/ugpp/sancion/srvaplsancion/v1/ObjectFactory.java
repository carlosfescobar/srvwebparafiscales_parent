
package co.gov.ugpp.sancion.srvaplsancion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sancion.srvaplsancion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorIdSancionResp");
    private final static QName _OpBuscarPorIdSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorIdSancionSol");
    private final static QName _OpActualizarSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpActualizarSancionSol");
    private final static QName _OpBuscarPorCriteriosSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorCriteriosSancionSol");
    private final static QName _OpBuscarPorIdSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorIdSancionFallo");
    private final static QName _OpCrearSancionSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpCrearSancionSol");
    private final static QName _OpActualizarSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpActualizarSancionFallo");
    private final static QName _OpBuscarPorCriteriosSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorCriteriosSancionResp");
    private final static QName _OpCrearSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpCrearSancionResp");
    private final static QName _OpActualizarSancionResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpActualizarSancionResp");
    private final static QName _OpCrearSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpCrearSancionFallo");
    private final static QName _OpBuscarPorCriteriosSancionFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", "OpBuscarPorCriteriosSancionFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sancion.srvaplsancion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSancionRespTipo }
     * 
     */
    public OpBuscarPorIdSancionRespTipo createOpBuscarPorIdSancionRespTipo() {
        return new OpBuscarPorIdSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSancionSolTipo }
     * 
     */
    public OpBuscarPorIdSancionSolTipo createOpBuscarPorIdSancionSolTipo() {
        return new OpBuscarPorIdSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSancionSolTipo }
     * 
     */
    public OpActualizarSancionSolTipo createOpActualizarSancionSolTipo() {
        return new OpActualizarSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSancionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosSancionSolTipo createOpBuscarPorCriteriosSancionSolTipo() {
        return new OpBuscarPorCriteriosSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearSancionSolTipo }
     * 
     */
    public OpCrearSancionSolTipo createOpCrearSancionSolTipo() {
        return new OpCrearSancionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSancionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosSancionRespTipo createOpBuscarPorCriteriosSancionRespTipo() {
        return new OpBuscarPorCriteriosSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSancionRespTipo }
     * 
     */
    public OpActualizarSancionRespTipo createOpActualizarSancionRespTipo() {
        return new OpActualizarSancionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSancionRespTipo }
     * 
     */
    public OpCrearSancionRespTipo createOpCrearSancionRespTipo() {
        return new OpCrearSancionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorIdSancionResp")
    public JAXBElement<OpBuscarPorIdSancionRespTipo> createOpBuscarPorIdSancionResp(OpBuscarPorIdSancionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdSancionRespTipo>(_OpBuscarPorIdSancionResp_QNAME, OpBuscarPorIdSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorIdSancionSol")
    public JAXBElement<OpBuscarPorIdSancionSolTipo> createOpBuscarPorIdSancionSol(OpBuscarPorIdSancionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdSancionSolTipo>(_OpBuscarPorIdSancionSol_QNAME, OpBuscarPorIdSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpActualizarSancionSol")
    public JAXBElement<OpActualizarSancionSolTipo> createOpActualizarSancionSol(OpActualizarSancionSolTipo value) {
        return new JAXBElement<OpActualizarSancionSolTipo>(_OpActualizarSancionSol_QNAME, OpActualizarSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorCriteriosSancionSol")
    public JAXBElement<OpBuscarPorCriteriosSancionSolTipo> createOpBuscarPorCriteriosSancionSol(OpBuscarPorCriteriosSancionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSancionSolTipo>(_OpBuscarPorCriteriosSancionSol_QNAME, OpBuscarPorCriteriosSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorIdSancionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSancionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpCrearSancionSol")
    public JAXBElement<OpCrearSancionSolTipo> createOpCrearSancionSol(OpCrearSancionSolTipo value) {
        return new JAXBElement<OpCrearSancionSolTipo>(_OpCrearSancionSol_QNAME, OpCrearSancionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpActualizarSancionFallo")
    public JAXBElement<FalloTipo> createOpActualizarSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorCriteriosSancionResp")
    public JAXBElement<OpBuscarPorCriteriosSancionRespTipo> createOpBuscarPorCriteriosSancionResp(OpBuscarPorCriteriosSancionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSancionRespTipo>(_OpBuscarPorCriteriosSancionResp_QNAME, OpBuscarPorCriteriosSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpCrearSancionResp")
    public JAXBElement<OpCrearSancionRespTipo> createOpCrearSancionResp(OpCrearSancionRespTipo value) {
        return new JAXBElement<OpCrearSancionRespTipo>(_OpCrearSancionResp_QNAME, OpCrearSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSancionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpActualizarSancionResp")
    public JAXBElement<OpActualizarSancionRespTipo> createOpActualizarSancionResp(OpActualizarSancionRespTipo value) {
        return new JAXBElement<OpActualizarSancionRespTipo>(_OpActualizarSancionResp_QNAME, OpActualizarSancionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpCrearSancionFallo")
    public JAXBElement<FalloTipo> createOpCrearSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSancionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplSancion/v1", name = "OpBuscarPorCriteriosSancionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosSancionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosSancionFallo_QNAME, FalloTipo.class, null, value);
    }

}
