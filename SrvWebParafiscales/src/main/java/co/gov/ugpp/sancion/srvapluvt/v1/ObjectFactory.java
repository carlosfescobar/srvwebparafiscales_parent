
package co.gov.ugpp.sancion.srvapluvt.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sancion.srvapluvt.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarUVTActualResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpConsultarUVTActualResp");
    private final static QName _OpConsultarUVTActualSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpConsultarUVTActualSol");
    private final static QName _OpConsultarUVTActualFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpConsultarUVTActualFallo");
    private final static QName _OpBuscarPorCriteriosUVTResp_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpBuscarPorCriteriosUVTResp");
    private final static QName _OpBuscarPorCriteriosUVTFallo_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpBuscarPorCriteriosUVTFallo");
    private final static QName _OpBuscarPorCriteriosUVTSol_QNAME = new QName("http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", "OpBuscarPorCriteriosUVTSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sancion.srvapluvt.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarUVTActualRespTipo }
     * 
     */
    public OpConsultarUVTActualRespTipo createOpConsultarUVTActualRespTipo() {
        return new OpConsultarUVTActualRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarUVTActualSolTipo }
     * 
     */
    public OpConsultarUVTActualSolTipo createOpConsultarUVTActualSolTipo() {
        return new OpConsultarUVTActualSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosUVTSolTipo }
     * 
     */
    public OpBuscarPorCriteriosUVTSolTipo createOpBuscarPorCriteriosUVTSolTipo() {
        return new OpBuscarPorCriteriosUVTSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosUVTRespTipo }
     * 
     */
    public OpBuscarPorCriteriosUVTRespTipo createOpBuscarPorCriteriosUVTRespTipo() {
        return new OpBuscarPorCriteriosUVTRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarUVTActualRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpConsultarUVTActualResp")
    public JAXBElement<OpConsultarUVTActualRespTipo> createOpConsultarUVTActualResp(OpConsultarUVTActualRespTipo value) {
        return new JAXBElement<OpConsultarUVTActualRespTipo>(_OpConsultarUVTActualResp_QNAME, OpConsultarUVTActualRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarUVTActualSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpConsultarUVTActualSol")
    public JAXBElement<OpConsultarUVTActualSolTipo> createOpConsultarUVTActualSol(OpConsultarUVTActualSolTipo value) {
        return new JAXBElement<OpConsultarUVTActualSolTipo>(_OpConsultarUVTActualSol_QNAME, OpConsultarUVTActualSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpConsultarUVTActualFallo")
    public JAXBElement<FalloTipo> createOpConsultarUVTActualFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarUVTActualFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosUVTRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpBuscarPorCriteriosUVTResp")
    public JAXBElement<OpBuscarPorCriteriosUVTRespTipo> createOpBuscarPorCriteriosUVTResp(OpBuscarPorCriteriosUVTRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosUVTRespTipo>(_OpBuscarPorCriteriosUVTResp_QNAME, OpBuscarPorCriteriosUVTRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpBuscarPorCriteriosUVTFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosUVTFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosUVTFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosUVTSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sancion/SrvAplUVT/v1", name = "OpBuscarPorCriteriosUVTSol")
    public JAXBElement<OpBuscarPorCriteriosUVTSolTipo> createOpBuscarPorCriteriosUVTSol(OpBuscarPorCriteriosUVTSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosUVTSolTipo>(_OpBuscarPorCriteriosUVTSol_QNAME, OpBuscarPorCriteriosUVTSolTipo.class, null, value);
    }

}
