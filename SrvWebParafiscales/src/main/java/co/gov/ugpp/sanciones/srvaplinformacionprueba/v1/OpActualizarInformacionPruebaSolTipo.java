
package co.gov.ugpp.sanciones.srvaplinformacionprueba.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.sanciones.informacionpruebatipo.v1.InformacionPruebaTipo;


/**
 * <p>Clase Java para OpActualizarInformacionPruebaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarInformacionPruebaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="informacionPrueba" type="{http://www.ugpp.gov.co/schema/Sanciones/InformacionPruebaTipo/v1}InformacionPruebaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarInformacionPruebaSolTipo", propOrder = {
    "contextoTransaccional",
    "informacionPrueba"
})
public class OpActualizarInformacionPruebaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected InformacionPruebaTipo informacionPrueba;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad informacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link InformacionPruebaTipo }
     *     
     */
    public InformacionPruebaTipo getInformacionPrueba() {
        return informacionPrueba;
    }

    /**
     * Define el valor de la propiedad informacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link InformacionPruebaTipo }
     *     
     */
    public void setInformacionPrueba(InformacionPruebaTipo value) {
        this.informacionPrueba = value;
    }

}
