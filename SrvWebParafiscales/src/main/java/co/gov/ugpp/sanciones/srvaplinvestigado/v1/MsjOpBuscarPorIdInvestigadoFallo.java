
package co.gov.ugpp.sanciones.srvaplinvestigado.v1;

import javax.xml.ws.WebFault;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "OpBuscarPorIdInvestigadoFallo", targetNamespace = "http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1")
public class MsjOpBuscarPorIdInvestigadoFallo
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private FalloTipo faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public MsjOpBuscarPorIdInvestigadoFallo(String message, FalloTipo faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public MsjOpBuscarPorIdInvestigadoFallo(String message, FalloTipo faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo
     */
    public FalloTipo getFaultInfo() {
        return faultInfo;
    }

}
