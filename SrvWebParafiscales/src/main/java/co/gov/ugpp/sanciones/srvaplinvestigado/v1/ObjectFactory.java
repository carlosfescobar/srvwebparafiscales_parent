
package co.gov.ugpp.sanciones.srvaplinvestigado.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.sanciones.srvaplinvestigado.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdInvestigadoSol_QNAME = new QName("http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", "OpBuscarPorIdInvestigadoSol");
    private final static QName _OpBuscarPorIdInvestigadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", "OpBuscarPorIdInvestigadoFallo");
    private final static QName _OpBuscarPorIdInvestigadoResp_QNAME = new QName("http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", "OpBuscarPorIdInvestigadoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.sanciones.srvaplinvestigado.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInvestigadoRespTipo }
     * 
     */
    public OpBuscarPorIdInvestigadoRespTipo createOpBuscarPorIdInvestigadoRespTipo() {
        return new OpBuscarPorIdInvestigadoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdInvestigadoSolTipo }
     * 
     */
    public OpBuscarPorIdInvestigadoSolTipo createOpBuscarPorIdInvestigadoSolTipo() {
        return new OpBuscarPorIdInvestigadoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInvestigadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", name = "OpBuscarPorIdInvestigadoSol")
    public JAXBElement<OpBuscarPorIdInvestigadoSolTipo> createOpBuscarPorIdInvestigadoSol(OpBuscarPorIdInvestigadoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdInvestigadoSolTipo>(_OpBuscarPorIdInvestigadoSol_QNAME, OpBuscarPorIdInvestigadoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", name = "OpBuscarPorIdInvestigadoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdInvestigadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdInvestigadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdInvestigadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Sanciones/SrvAplInvestigado/v1", name = "OpBuscarPorIdInvestigadoResp")
    public JAXBElement<OpBuscarPorIdInvestigadoRespTipo> createOpBuscarPorIdInvestigadoResp(OpBuscarPorIdInvestigadoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdInvestigadoRespTipo>(_OpBuscarPorIdInvestigadoResp_QNAME, OpBuscarPorIdInvestigadoRespTipo.class, null, value);
    }

}
