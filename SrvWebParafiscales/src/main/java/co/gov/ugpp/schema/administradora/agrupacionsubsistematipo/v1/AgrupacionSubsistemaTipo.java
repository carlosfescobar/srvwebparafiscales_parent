
package co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;


/**
 * <p>Clase Java para AgrupacionSubsistemaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgrupacionSubsistemaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAgrupacionAdministradora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subsistema" type="{http://www.ugpp.gov.co/schema/Administradora/SubsistemaTipo/v1}SubsistemaTipo" minOccurs="0"/>
 *         &lt;element name="administradora" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgrupacionSubsistemaTipo", propOrder = {
    "idAgrupacionAdministradora",
    "subsistema",
    "administradora"
})
public class AgrupacionSubsistemaTipo {

    @XmlElement(nillable = true)
    protected String idAgrupacionAdministradora;
    @XmlElement(nillable = true)
    protected SubsistemaTipo subsistema;
    @XmlElement(nillable = true)
    protected List<EntidadExternaTipo> administradora;

    /**
     * Obtiene el valor de la propiedad idAgrupacionAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAgrupacionAdministradora() {
        return idAgrupacionAdministradora;
    }

    /**
     * Define el valor de la propiedad idAgrupacionAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAgrupacionAdministradora(String value) {
        this.idAgrupacionAdministradora = value;
    }

    /**
     * Obtiene el valor de la propiedad subsistema.
     * 
     * @return
     *     possible object is
     *     {@link SubsistemaTipo }
     *     
     */
    public SubsistemaTipo getSubsistema() {
        return subsistema;
    }

    /**
     * Define el valor de la propiedad subsistema.
     * 
     * @param value
     *     allowed object is
     *     {@link SubsistemaTipo }
     *     
     */
    public void setSubsistema(SubsistemaTipo value) {
        this.subsistema = value;
    }

    /**
     * Gets the value of the administradora property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the administradora property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdministradora().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntidadExternaTipo }
     * 
     * 
     */
    public List<EntidadExternaTipo> getAdministradora() {
        if (administradora == null) {
            administradora = new ArrayList<EntidadExternaTipo>();
        }
        return this.administradora;
    }

}
