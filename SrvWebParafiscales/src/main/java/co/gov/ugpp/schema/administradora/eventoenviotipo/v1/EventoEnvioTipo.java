
package co.gov.ugpp.schema.administradora.eventoenviotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para EventoEnvioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EventoEnvioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idEventoEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAprobadoDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EventoEnvioTipo", propOrder = {
    "idEventoEnvio",
    "codEstado",
    "descEstado",
    "codCausalDevolucion",
    "descCausalDevolucion",
    "esAprobadoDevolucion",
    "idRadicadoSalida",
    "fecRadicadoSalida"
})
public class EventoEnvioTipo {

    @XmlElement(nillable = true)
    protected String idEventoEnvio;
    @XmlElement(nillable = true)
    protected String codEstado;
    @XmlElement(nillable = true)
    protected String descEstado;
    @XmlElement(nillable = true)
    protected String codCausalDevolucion;
    @XmlElement(nillable = true)
    protected String descCausalDevolucion;
    @XmlElement(nillable = true)
    protected String esAprobadoDevolucion;
    @XmlElement(nillable = true)
    protected String idRadicadoSalida;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoSalida;

    /**
     * Obtiene el valor de la propiedad idEventoEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEventoEnvio() {
        return idEventoEnvio;
    }

    /**
     * Define el valor de la propiedad idEventoEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEventoEnvio(String value) {
        this.idEventoEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstado() {
        return descEstado;
    }

    /**
     * Define el valor de la propiedad descEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstado(String value) {
        this.descEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalDevolucion() {
        return codCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad codCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalDevolucion(String value) {
        this.codCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad descCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausalDevolucion() {
        return descCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad descCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausalDevolucion(String value) {
        this.descCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad esAprobadoDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAprobadoDevolucion() {
        return esAprobadoDevolucion;
    }

    /**
     * Define el valor de la propiedad esAprobadoDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAprobadoDevolucion(String value) {
        this.esAprobadoDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalida(String value) {
        this.idRadicadoSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad fecRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoSalida(Calendar value) {
        this.fecRadicadoSalida = value;
    }

}
