
package co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.administradora.subsistematipo.v1.SubsistemaTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1.ControlUbicacionEnvioTipo;


/**
 * <p>Clase Java para GestionAdministradoraTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="GestionAdministradoraTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idGestionAdministradora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="subsistema" type="{http://www.ugpp.gov.co/schema/Administradora/SubsistemaTipo/v1}SubsistemaTipo" minOccurs="0"/>
 *         &lt;element name="administradora" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="controlUbicaciones" type="{http://www.ugpp.gov.co/schema/Transversales/ControlUbicacionEnvioTipo/v1}ControlUbicacionEnvioTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idDocumentoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="codTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPeriodicidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPeriodicidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCantidadPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descInformacionRequerida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioSolicitud" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecFinSolicitud" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecProximaEspera" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="valNombreSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoGestion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoGestion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFormatoElegido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idVersionElegido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idFuncionarioSolicitud" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GestionAdministradoraTipo", propOrder = {
    "idGestionAdministradora",
    "subsistema",
    "administradora",
    "controlUbicaciones",
    "idDocumentoSolicitud",
    "expediente",
    "codTipoSolicitud",
    "descTipoSolicitud",
    "codTipoRespuesta",
    "descTipoRespuesta",
    "codTipoPeriodicidad",
    "descTipoPeriodicidad",
    "codTipoPlazoEntrega",
    "descTipoPlazoEntrega",
    "valCantidadPlazoEntrega",
    "descInformacionRequerida",
    "fecInicioSolicitud",
    "fecFinSolicitud",
    "fecProximaEspera",
    "valNombreSolicitud",
    "codEstadoGestion",
    "descEstadoGestion",
    "idFormatoElegido",
    "idVersionElegido",
    "acciones",
    "idFuncionarioSolicitud"
})
public class GestionAdministradoraTipo {

    @XmlElement(nillable = true)
    protected String idGestionAdministradora;
    @XmlElement(nillable = true)
    protected SubsistemaTipo subsistema;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo administradora;
    @XmlElement(nillable = true)
    protected List<ControlUbicacionEnvioTipo> controlUbicaciones;
    @XmlElement(nillable = true)
    protected List<String> idDocumentoSolicitud;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String codTipoSolicitud;
    @XmlElement(nillable = true)
    protected String descTipoSolicitud;
    @XmlElement(nillable = true)
    protected String codTipoRespuesta;
    @XmlElement(nillable = true)
    protected String descTipoRespuesta;
    @XmlElement(nillable = true)
    protected String codTipoPeriodicidad;
    @XmlElement(nillable = true)
    protected String descTipoPeriodicidad;
    @XmlElement(nillable = true)
    protected String codTipoPlazoEntrega;
    @XmlElement(nillable = true)
    protected String descTipoPlazoEntrega;
    @XmlElement(nillable = true)
    protected String valCantidadPlazoEntrega;
    @XmlElement(nillable = true)
    protected String descInformacionRequerida;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecInicioSolicitud;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecFinSolicitud;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecProximaEspera;
    @XmlElement(nillable = true)
    protected String valNombreSolicitud;
    @XmlElement(nillable = true)
    protected String codEstadoGestion;
    @XmlElement(nillable = true)
    protected String descEstadoGestion;
    @XmlElement(nillable = true)
    protected String idFormatoElegido;
    @XmlElement(nillable = true)
    protected String idVersionElegido;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(nillable = true)
    protected FuncionarioTipo idFuncionarioSolicitud;

    /**
     * Obtiene el valor de la propiedad idGestionAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGestionAdministradora() {
        return idGestionAdministradora;
    }

    /**
     * Define el valor de la propiedad idGestionAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGestionAdministradora(String value) {
        this.idGestionAdministradora = value;
    }

    /**
     * Obtiene el valor de la propiedad subsistema.
     * 
     * @return
     *     possible object is
     *     {@link SubsistemaTipo }
     *     
     */
    public SubsistemaTipo getSubsistema() {
        return subsistema;
    }

    /**
     * Define el valor de la propiedad subsistema.
     * 
     * @param value
     *     allowed object is
     *     {@link SubsistemaTipo }
     *     
     */
    public void setSubsistema(SubsistemaTipo value) {
        this.subsistema = value;
    }

    /**
     * Obtiene el valor de la propiedad administradora.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getAdministradora() {
        return administradora;
    }

    /**
     * Define el valor de la propiedad administradora.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setAdministradora(EntidadExternaTipo value) {
        this.administradora = value;
    }

    /**
     * Gets the value of the controlUbicaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the controlUbicaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getControlUbicaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlUbicacionEnvioTipo }
     * 
     * 
     */
    public List<ControlUbicacionEnvioTipo> getControlUbicaciones() {
        if (controlUbicaciones == null) {
            controlUbicaciones = new ArrayList<ControlUbicacionEnvioTipo>();
        }
        return this.controlUbicaciones;
    }

    /**
     * Gets the value of the idDocumentoSolicitud property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentoSolicitud property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentoSolicitud().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentoSolicitud() {
        if (idDocumentoSolicitud == null) {
            idDocumentoSolicitud = new ArrayList<String>();
        }
        return this.idDocumentoSolicitud;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    /**
     * Define el valor de la propiedad codTipoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSolicitud(String value) {
        this.codTipoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoSolicitud() {
        return descTipoSolicitud;
    }

    /**
     * Define el valor de la propiedad descTipoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoSolicitud(String value) {
        this.descTipoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad codTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoRespuesta(String value) {
        this.codTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoRespuesta() {
        return descTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad descTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoRespuesta(String value) {
        this.descTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPeriodicidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPeriodicidad() {
        return codTipoPeriodicidad;
    }

    /**
     * Define el valor de la propiedad codTipoPeriodicidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPeriodicidad(String value) {
        this.codTipoPeriodicidad = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPeriodicidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPeriodicidad() {
        return descTipoPeriodicidad;
    }

    /**
     * Define el valor de la propiedad descTipoPeriodicidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPeriodicidad(String value) {
        this.descTipoPeriodicidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPlazoEntrega() {
        return codTipoPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad codTipoPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPlazoEntrega(String value) {
        this.codTipoPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPlazoEntrega() {
        return descTipoPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad descTipoPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPlazoEntrega(String value) {
        this.descTipoPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad valCantidadPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCantidadPlazoEntrega() {
        return valCantidadPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valCantidadPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCantidadPlazoEntrega(String value) {
        this.valCantidadPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad descInformacionRequerida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescInformacionRequerida() {
        return descInformacionRequerida;
    }

    /**
     * Define el valor de la propiedad descInformacionRequerida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescInformacionRequerida(String value) {
        this.descInformacionRequerida = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioSolicitud() {
        return fecInicioSolicitud;
    }

    /**
     * Define el valor de la propiedad fecInicioSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioSolicitud(Calendar value) {
        this.fecInicioSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinSolicitud() {
        return fecFinSolicitud;
    }

    /**
     * Define el valor de la propiedad fecFinSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinSolicitud(Calendar value) {
        this.fecFinSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad fecProximaEspera.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecProximaEspera() {
        return fecProximaEspera;
    }

    /**
     * Define el valor de la propiedad fecProximaEspera.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecProximaEspera(Calendar value) {
        this.fecProximaEspera = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreSolicitud() {
        return valNombreSolicitud;
    }

    /**
     * Define el valor de la propiedad valNombreSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreSolicitud(String value) {
        this.valNombreSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoGestion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoGestion() {
        return codEstadoGestion;
    }

    /**
     * Define el valor de la propiedad codEstadoGestion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoGestion(String value) {
        this.codEstadoGestion = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoGestion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoGestion() {
        return descEstadoGestion;
    }

    /**
     * Define el valor de la propiedad descEstadoGestion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoGestion(String value) {
        this.descEstadoGestion = value;
    }

    /**
     * Obtiene el valor de la propiedad idFormatoElegido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFormatoElegido() {
        return idFormatoElegido;
    }

    /**
     * Define el valor de la propiedad idFormatoElegido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFormatoElegido(String value) {
        this.idFormatoElegido = value;
    }

    /**
     * Obtiene el valor de la propiedad idVersionElegido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVersionElegido() {
        return idVersionElegido;
    }

    /**
     * Define el valor de la propiedad idVersionElegido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVersionElegido(String value) {
        this.idVersionElegido = value;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad idFuncionarioSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getIdFuncionarioSolicitud() {
        return idFuncionarioSolicitud;
    }

    /**
     * Define el valor de la propiedad idFuncionarioSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setIdFuncionarioSolicitud(FuncionarioTipo value) {
        this.idFuncionarioSolicitud = value;
    }

}
