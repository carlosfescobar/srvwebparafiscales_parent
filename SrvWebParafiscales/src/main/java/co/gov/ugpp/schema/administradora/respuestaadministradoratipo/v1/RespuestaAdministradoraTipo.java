
package co.gov.ugpp.schema.administradora.respuestaadministradoratipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para RespuestaAdministradoraTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RespuestaAdministradoraTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRespuestaAdministradora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idGestionAdministradoraTipo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioEspera" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecRespuesta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esProrroga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esConcedidaProrroga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoProrroga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDiasConcedidosProrroga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInformacionCorrecta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDecisionCaso" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codTipoSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesReenvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentoReenvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesEntidadVigilancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoEntidadVigilancia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaAdministradoraTipo", propOrder = {
    "idRespuestaAdministradora",
    "idGestionAdministradoraTipo",
    "fecInicioEspera",
    "fecRespuesta",
    "codTipoRespuesta",
    "descTipoRespuesta",
    "idRadicadoEntrada",
    "esProrroga",
    "esConcedidaProrroga",
    "idDocumentoProrroga",
    "valDiasConcedidosProrroga",
    "esInformacionCorrecta",
    "codDecisionCaso",
    "codTipoSancion",
    "descTipoSancion",
    "descObservacionesReenvio",
    "expediente",
    "idDocumentoReenvio",
    "descObservacionesEntidadVigilancia",
    "idDocumentoEntidadVigilancia",
    "descObservacionesSancion"
})
public class RespuestaAdministradoraTipo {

    @XmlElement(nillable = true)
    protected String idRespuestaAdministradora;
    @XmlElement(nillable = true)
    protected String idGestionAdministradoraTipo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecInicioEspera;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRespuesta;
    @XmlElement(nillable = true)
    protected String codTipoRespuesta;
    @XmlElement(nillable = true)
    protected String descTipoRespuesta;
    @XmlElement(nillable = true)
    protected String idRadicadoEntrada;
    @XmlElement(nillable = true)
    protected String esProrroga;
    @XmlElement(nillable = true)
    protected String esConcedidaProrroga;
    @XmlElement(nillable = true)
    protected String idDocumentoProrroga;
    @XmlElement(nillable = true)
    protected String valDiasConcedidosProrroga;
    @XmlElement(nillable = true)
    protected String esInformacionCorrecta;
    @XmlElement(nillable = true)
    protected List<String> codDecisionCaso;
    @XmlElement(nillable = true)
    protected String codTipoSancion;
    @XmlElement(nillable = true)
    protected String descTipoSancion;
    @XmlElement(nillable = true)
    protected String descObservacionesReenvio;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String idDocumentoReenvio;
    @XmlElement(nillable = true)
    protected String descObservacionesEntidadVigilancia;
    @XmlElement(nillable = true)
    protected String idDocumentoEntidadVigilancia;
    @XmlElement(nillable = true)
    protected String descObservacionesSancion;

    /**
     * Obtiene el valor de la propiedad idRespuestaAdministradora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRespuestaAdministradora() {
        return idRespuestaAdministradora;
    }

    /**
     * Define el valor de la propiedad idRespuestaAdministradora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRespuestaAdministradora(String value) {
        this.idRespuestaAdministradora = value;
    }

    /**
     * Obtiene el valor de la propiedad idGestionAdministradoraTipo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdGestionAdministradoraTipo() {
        return idGestionAdministradoraTipo;
    }

    /**
     * Define el valor de la propiedad idGestionAdministradoraTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdGestionAdministradoraTipo(String value) {
        this.idGestionAdministradoraTipo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioEspera.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioEspera() {
        return fecInicioEspera;
    }

    /**
     * Define el valor de la propiedad fecInicioEspera.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioEspera(Calendar value) {
        this.fecInicioEspera = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRespuesta() {
        return fecRespuesta;
    }

    /**
     * Define el valor de la propiedad fecRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRespuesta(Calendar value) {
        this.fecRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad codTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoRespuesta(String value) {
        this.codTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoRespuesta() {
        return descTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad descTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoRespuesta(String value) {
        this.descTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntrada(String value) {
        this.idRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad esProrroga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsProrroga() {
        return esProrroga;
    }

    /**
     * Define el valor de la propiedad esProrroga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsProrroga(String value) {
        this.esProrroga = value;
    }

    /**
     * Obtiene el valor de la propiedad esConcedidaProrroga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConcedidaProrroga() {
        return esConcedidaProrroga;
    }

    /**
     * Define el valor de la propiedad esConcedidaProrroga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConcedidaProrroga(String value) {
        this.esConcedidaProrroga = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoProrroga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoProrroga() {
        return idDocumentoProrroga;
    }

    /**
     * Define el valor de la propiedad idDocumentoProrroga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoProrroga(String value) {
        this.idDocumentoProrroga = value;
    }

    /**
     * Obtiene el valor de la propiedad valDiasConcedidosProrroga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiasConcedidosProrroga() {
        return valDiasConcedidosProrroga;
    }

    /**
     * Define el valor de la propiedad valDiasConcedidosProrroga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiasConcedidosProrroga(String value) {
        this.valDiasConcedidosProrroga = value;
    }

    /**
     * Obtiene el valor de la propiedad esInformacionCorrecta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInformacionCorrecta() {
        return esInformacionCorrecta;
    }

    /**
     * Define el valor de la propiedad esInformacionCorrecta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInformacionCorrecta(String value) {
        this.esInformacionCorrecta = value;
    }

    /**
     * Gets the value of the codDecisionCaso property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codDecisionCaso property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodDecisionCaso().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodDecisionCaso() {
        if (codDecisionCaso == null) {
            codDecisionCaso = new ArrayList<String>();
        }
        return this.codDecisionCaso;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSancion() {
        return codTipoSancion;
    }

    /**
     * Define el valor de la propiedad codTipoSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSancion(String value) {
        this.codTipoSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoSancion() {
        return descTipoSancion;
    }

    /**
     * Define el valor de la propiedad descTipoSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoSancion(String value) {
        this.descTipoSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesReenvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesReenvio() {
        return descObservacionesReenvio;
    }

    /**
     * Define el valor de la propiedad descObservacionesReenvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesReenvio(String value) {
        this.descObservacionesReenvio = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoReenvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoReenvio() {
        return idDocumentoReenvio;
    }

    /**
     * Define el valor de la propiedad idDocumentoReenvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoReenvio(String value) {
        this.idDocumentoReenvio = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesEntidadVigilancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesEntidadVigilancia() {
        return descObservacionesEntidadVigilancia;
    }

    /**
     * Define el valor de la propiedad descObservacionesEntidadVigilancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesEntidadVigilancia(String value) {
        this.descObservacionesEntidadVigilancia = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoEntidadVigilancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoEntidadVigilancia() {
        return idDocumentoEntidadVigilancia;
    }

    /**
     * Define el valor de la propiedad idDocumentoEntidadVigilancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoEntidadVigilancia(String value) {
        this.idDocumentoEntidadVigilancia = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesSancion() {
        return descObservacionesSancion;
    }

    /**
     * Define el valor de la propiedad descObservacionesSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesSancion(String value) {
        this.descObservacionesSancion = value;
    }

}
