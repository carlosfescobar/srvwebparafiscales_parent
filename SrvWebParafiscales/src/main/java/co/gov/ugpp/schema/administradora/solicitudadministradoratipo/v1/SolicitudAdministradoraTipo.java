
package co.gov.ugpp.schema.administradora.solicitudadministradoratipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.administradora.agrupacionsubsistematipo.v1.AgrupacionSubsistemaTipo;
import co.gov.ugpp.schema.administradora.gestionadministradoratipo.v1.GestionAdministradoraTipo;
import co.gov.ugpp.schema.administradora.validacionestructuratipo.v1.ValidacionEstructuraTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para SolicitudAdministradoraTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudAdministradoraTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSolicitudAdministradoras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="codTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPeriodicidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPeriodicidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCantidadPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioSolicitud" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecFinSolicitud" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="valNombreSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descInformacionRequerida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSolicitudAceptada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esNecesitaCorreccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descSolicitudCorreccion" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="validacionEstructura" type="{http://www.ugpp.gov.co/schema/Administradora/ValidacionEstructuraTipo/v1}ValidacionEstructuraTipo" minOccurs="0"/>
 *         &lt;element name="administradorasSeleccionadas" type="{http://www.ugpp.gov.co/schema/Administradora/AgrupacionSubsistemaTipo/v1}AgrupacionSubsistemaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="administradorasDefinitivas" type="{http://www.ugpp.gov.co/schema/Administradora/GestionAdministradoraTipo/v1}GestionAdministradoraTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idArchivoSolicitudInicial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFuncionarioSolicitud" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudAdministradoraTipo", propOrder = {
    "idSolicitudAdministradoras",
    "expediente",
    "codTipoSolicitud",
    "descTipoSolicitud",
    "codTipoRespuesta",
    "descTipoRespuesta",
    "codTipoPeriodicidad",
    "descTipoPeriodicidad",
    "codTipoPlazoEntrega",
    "descTipoPlazoEntrega",
    "valCantidadPlazoEntrega",
    "fecInicioSolicitud",
    "fecFinSolicitud",
    "valNombreSolicitud",
    "descInformacionRequerida",
    "codEstadoSolicitud",
    "descEstadoSolicitud",
    "esSolicitudAceptada",
    "esNecesitaCorreccion",
    "descSolicitudCorreccion",
    "validacionEstructura",
    "administradorasSeleccionadas",
    "administradorasDefinitivas",
    "acciones",
    "idArchivoSolicitudInicial",
    "idFuncionarioSolicitud"
})
public class SolicitudAdministradoraTipo {

    @XmlElement(nillable = true)
    protected String idSolicitudAdministradoras;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String codTipoSolicitud;
    @XmlElement(nillable = true)
    protected String descTipoSolicitud;
    @XmlElement(nillable = true)
    protected String codTipoRespuesta;
    @XmlElement(nillable = true)
    protected String descTipoRespuesta;
    @XmlElement(nillable = true)
    protected String codTipoPeriodicidad;
    @XmlElement(nillable = true)
    protected String descTipoPeriodicidad;
    @XmlElement(nillable = true)
    protected String codTipoPlazoEntrega;
    @XmlElement(nillable = true)
    protected String descTipoPlazoEntrega;
    @XmlElement(nillable = true)
    protected String valCantidadPlazoEntrega;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecInicioSolicitud;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecFinSolicitud;
    @XmlElement(nillable = true)
    protected String valNombreSolicitud;
    @XmlElement(nillable = true)
    protected String descInformacionRequerida;
    @XmlElement(nillable = true)
    protected String codEstadoSolicitud;
    @XmlElement(nillable = true)
    protected String descEstadoSolicitud;
    @XmlElement(nillable = true)
    protected String esSolicitudAceptada;
    @XmlElement(nillable = true)
    protected String esNecesitaCorreccion;
    @XmlElement(nillable = true)
    protected List<String> descSolicitudCorreccion;
    @XmlElement(nillable = true)
    protected ValidacionEstructuraTipo validacionEstructura;
    @XmlElement(nillable = true)
    protected List<AgrupacionSubsistemaTipo> administradorasSeleccionadas;
    @XmlElement(nillable = true)
    protected List<GestionAdministradoraTipo> administradorasDefinitivas;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(nillable = true)
    protected String idArchivoSolicitudInicial;
    @XmlElement(nillable = true)
    protected FuncionarioTipo idFuncionarioSolicitud;

    /**
     * Obtiene el valor de la propiedad idSolicitudAdministradoras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSolicitudAdministradoras() {
        return idSolicitudAdministradoras;
    }

    /**
     * Define el valor de la propiedad idSolicitudAdministradoras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSolicitudAdministradoras(String value) {
        this.idSolicitudAdministradoras = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    /**
     * Define el valor de la propiedad codTipoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSolicitud(String value) {
        this.codTipoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoSolicitud() {
        return descTipoSolicitud;
    }

    /**
     * Define el valor de la propiedad descTipoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoSolicitud(String value) {
        this.descTipoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoRespuesta() {
        return codTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad codTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoRespuesta(String value) {
        this.codTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoRespuesta() {
        return descTipoRespuesta;
    }

    /**
     * Define el valor de la propiedad descTipoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoRespuesta(String value) {
        this.descTipoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPeriodicidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPeriodicidad() {
        return codTipoPeriodicidad;
    }

    /**
     * Define el valor de la propiedad codTipoPeriodicidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPeriodicidad(String value) {
        this.codTipoPeriodicidad = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPeriodicidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPeriodicidad() {
        return descTipoPeriodicidad;
    }

    /**
     * Define el valor de la propiedad descTipoPeriodicidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPeriodicidad(String value) {
        this.descTipoPeriodicidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPlazoEntrega() {
        return codTipoPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad codTipoPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPlazoEntrega(String value) {
        this.codTipoPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoPlazoEntrega() {
        return descTipoPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad descTipoPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoPlazoEntrega(String value) {
        this.descTipoPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad valCantidadPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCantidadPlazoEntrega() {
        return valCantidadPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valCantidadPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCantidadPlazoEntrega(String value) {
        this.valCantidadPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioSolicitud() {
        return fecInicioSolicitud;
    }

    /**
     * Define el valor de la propiedad fecInicioSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioSolicitud(Calendar value) {
        this.fecInicioSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinSolicitud() {
        return fecFinSolicitud;
    }

    /**
     * Define el valor de la propiedad fecFinSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinSolicitud(Calendar value) {
        this.fecFinSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreSolicitud() {
        return valNombreSolicitud;
    }

    /**
     * Define el valor de la propiedad valNombreSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreSolicitud(String value) {
        this.valNombreSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad descInformacionRequerida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescInformacionRequerida() {
        return descInformacionRequerida;
    }

    /**
     * Define el valor de la propiedad descInformacionRequerida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescInformacionRequerida(String value) {
        this.descInformacionRequerida = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoSolicitud() {
        return codEstadoSolicitud;
    }

    /**
     * Define el valor de la propiedad codEstadoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoSolicitud(String value) {
        this.codEstadoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoSolicitud() {
        return descEstadoSolicitud;
    }

    /**
     * Define el valor de la propiedad descEstadoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoSolicitud(String value) {
        this.descEstadoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad esSolicitudAceptada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSolicitudAceptada() {
        return esSolicitudAceptada;
    }

    /**
     * Define el valor de la propiedad esSolicitudAceptada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSolicitudAceptada(String value) {
        this.esSolicitudAceptada = value;
    }

    /**
     * Obtiene el valor de la propiedad esNecesitaCorreccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsNecesitaCorreccion() {
        return esNecesitaCorreccion;
    }

    /**
     * Define el valor de la propiedad esNecesitaCorreccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsNecesitaCorreccion(String value) {
        this.esNecesitaCorreccion = value;
    }

    /**
     * Gets the value of the descSolicitudCorreccion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descSolicitudCorreccion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescSolicitudCorreccion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDescSolicitudCorreccion() {
        if (descSolicitudCorreccion == null) {
            descSolicitudCorreccion = new ArrayList<String>();
        }
        return this.descSolicitudCorreccion;
    }

    /**
     * Obtiene el valor de la propiedad validacionEstructura.
     * 
     * @return
     *     possible object is
     *     {@link ValidacionEstructuraTipo }
     *     
     */
    public ValidacionEstructuraTipo getValidacionEstructura() {
        return validacionEstructura;
    }

    /**
     * Define el valor de la propiedad validacionEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidacionEstructuraTipo }
     *     
     */
    public void setValidacionEstructura(ValidacionEstructuraTipo value) {
        this.validacionEstructura = value;
    }

    /**
     * Gets the value of the administradorasSeleccionadas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the administradorasSeleccionadas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdministradorasSeleccionadas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgrupacionSubsistemaTipo }
     * 
     * 
     */
    public List<AgrupacionSubsistemaTipo> getAdministradorasSeleccionadas() {
        if (administradorasSeleccionadas == null) {
            administradorasSeleccionadas = new ArrayList<AgrupacionSubsistemaTipo>();
        }
        return this.administradorasSeleccionadas;
    }

    /**
     * Gets the value of the administradorasDefinitivas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the administradorasDefinitivas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdministradorasDefinitivas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GestionAdministradoraTipo }
     * 
     * 
     */
    public List<GestionAdministradoraTipo> getAdministradorasDefinitivas() {
        if (administradorasDefinitivas == null) {
            administradorasDefinitivas = new ArrayList<GestionAdministradoraTipo>();
        }
        return this.administradorasDefinitivas;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoSolicitudInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoSolicitudInicial() {
        return idArchivoSolicitudInicial;
    }

    /**
     * Define el valor de la propiedad idArchivoSolicitudInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoSolicitudInicial(String value) {
        this.idArchivoSolicitudInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad idFuncionarioSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getIdFuncionarioSolicitud() {
        return idFuncionarioSolicitud;
    }

    /**
     * Define el valor de la propiedad idFuncionarioSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setIdFuncionarioSolicitud(FuncionarioTipo value) {
        this.idFuncionarioSolicitud = value;
    }

}
