
package co.gov.ugpp.schema.administradora.subsistematipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;


/**
 * <p>Clase Java para SubsistemaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubsistemaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSubsistema" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="administradoras" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="entidadVigilanciaControl" type="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubsistemaTipo", propOrder = {
    "idSubsistema",
    "administradoras",
    "entidadVigilanciaControl"
})
public class SubsistemaTipo {

    @XmlElement(nillable = true)
    protected String idSubsistema;
    @XmlElement(nillable = true)
    protected List<EntidadExternaTipo> administradoras;
    @XmlElement(nillable = true)
    protected PersonaJuridicaTipo entidadVigilanciaControl;

    /**
     * Obtiene el valor de la propiedad idSubsistema.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSubsistema() {
        return idSubsistema;
    }

    /**
     * Define el valor de la propiedad idSubsistema.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSubsistema(String value) {
        this.idSubsistema = value;
    }

    /**
     * Gets the value of the administradoras property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the administradoras property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdministradoras().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EntidadExternaTipo }
     * 
     * 
     */
    public List<EntidadExternaTipo> getAdministradoras() {
        if (administradoras == null) {
            administradoras = new ArrayList<EntidadExternaTipo>();
        }
        return this.administradoras;
    }

    /**
     * Obtiene el valor de la propiedad entidadVigilanciaControl.
     * 
     * @return
     *     possible object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public PersonaJuridicaTipo getEntidadVigilanciaControl() {
        return entidadVigilanciaControl;
    }

    /**
     * Define el valor de la propiedad entidadVigilanciaControl.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public void setEntidadVigilanciaControl(PersonaJuridicaTipo value) {
        this.entidadVigilanciaControl = value;
    }

}
