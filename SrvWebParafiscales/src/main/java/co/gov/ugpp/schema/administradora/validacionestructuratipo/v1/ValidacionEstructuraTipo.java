
package co.gov.ugpp.schema.administradora.validacionestructuratipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ValidacionEstructuraTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionEstructuraTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idValidacionEstructura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idArchivoEstructuraSugerida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esNecesarioNuevaEstructura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAprobadaNuevaEstructura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstructuraNoAprobada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFormatoEstructuraNueva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idVersionEstructuraNueva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFormatoElegido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idVersionElegido" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionEstructuraTipo", propOrder = {
    "idValidacionEstructura",
    "idArchivoEstructuraSugerida",
    "esNecesarioNuevaEstructura",
    "esAprobadaNuevaEstructura",
    "descEstructuraNoAprobada",
    "idFormatoEstructuraNueva",
    "idVersionEstructuraNueva",
    "idFormatoElegido",
    "idVersionElegido"
})
public class ValidacionEstructuraTipo {

    @XmlElement(nillable = true)
    protected String idValidacionEstructura;
    @XmlElement(nillable = true)
    protected String idArchivoEstructuraSugerida;
    @XmlElement(nillable = true)
    protected String esNecesarioNuevaEstructura;
    @XmlElement(nillable = true)
    protected String esAprobadaNuevaEstructura;
    @XmlElement(nillable = true)
    protected String descEstructuraNoAprobada;
    @XmlElement(nillable = true)
    protected String idFormatoEstructuraNueva;
    @XmlElement(nillable = true)
    protected String idVersionEstructuraNueva;
    @XmlElement(nillable = true)
    protected String idFormatoElegido;
    @XmlElement(nillable = true)
    protected String idVersionElegido;

    /**
     * Obtiene el valor de la propiedad idValidacionEstructura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdValidacionEstructura() {
        return idValidacionEstructura;
    }

    /**
     * Define el valor de la propiedad idValidacionEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdValidacionEstructura(String value) {
        this.idValidacionEstructura = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivoEstructuraSugerida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivoEstructuraSugerida() {
        return idArchivoEstructuraSugerida;
    }

    /**
     * Define el valor de la propiedad idArchivoEstructuraSugerida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivoEstructuraSugerida(String value) {
        this.idArchivoEstructuraSugerida = value;
    }

    /**
     * Obtiene el valor de la propiedad esNecesarioNuevaEstructura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsNecesarioNuevaEstructura() {
        return esNecesarioNuevaEstructura;
    }

    /**
     * Define el valor de la propiedad esNecesarioNuevaEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsNecesarioNuevaEstructura(String value) {
        this.esNecesarioNuevaEstructura = value;
    }

    /**
     * Obtiene el valor de la propiedad esAprobadaNuevaEstructura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAprobadaNuevaEstructura() {
        return esAprobadaNuevaEstructura;
    }

    /**
     * Define el valor de la propiedad esAprobadaNuevaEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAprobadaNuevaEstructura(String value) {
        this.esAprobadaNuevaEstructura = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstructuraNoAprobada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstructuraNoAprobada() {
        return descEstructuraNoAprobada;
    }

    /**
     * Define el valor de la propiedad descEstructuraNoAprobada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstructuraNoAprobada(String value) {
        this.descEstructuraNoAprobada = value;
    }

    /**
     * Obtiene el valor de la propiedad idFormatoEstructuraNueva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFormatoEstructuraNueva() {
        return idFormatoEstructuraNueva;
    }

    /**
     * Define el valor de la propiedad idFormatoEstructuraNueva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFormatoEstructuraNueva(String value) {
        this.idFormatoEstructuraNueva = value;
    }

    /**
     * Obtiene el valor de la propiedad idVersionEstructuraNueva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVersionEstructuraNueva() {
        return idVersionEstructuraNueva;
    }

    /**
     * Define el valor de la propiedad idVersionEstructuraNueva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVersionEstructuraNueva(String value) {
        this.idVersionEstructuraNueva = value;
    }

    /**
     * Obtiene el valor de la propiedad idFormatoElegido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFormatoElegido() {
        return idFormatoElegido;
    }

    /**
     * Define el valor de la propiedad idFormatoElegido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFormatoElegido(String value) {
        this.idFormatoElegido = value;
    }

    /**
     * Obtiene el valor de la propiedad idVersionElegido.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVersionElegido() {
        return idVersionElegido;
    }

    /**
     * Define el valor de la propiedad idVersionElegido.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVersionElegido(String value) {
        this.idVersionElegido = value;
    }

}
