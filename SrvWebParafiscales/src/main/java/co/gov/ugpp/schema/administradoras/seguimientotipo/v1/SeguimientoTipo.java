
package co.gov.ugpp.schema.administradoras.seguimientotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para SeguimientoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SeguimientoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSeguimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecProgramadaSeguimiento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="valEncargadoSeguimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionProgramacionSeguimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SeguimientoTipo", propOrder = {
    "idSeguimiento",
    "idExpediente",
    "descObservaciones",
    "valPersonaContactada",
    "valCargoPersonaContactada",
    "valPlazoEntrega",
    "fecProgramadaSeguimiento",
    "valEncargadoSeguimiento",
    "desObservacionProgramacionSeguimiento"
})
public class SeguimientoTipo {

    @XmlElement(nillable = true)
    protected String idSeguimiento;
    @XmlElement(nillable = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String valPersonaContactada;
    @XmlElement(nillable = true)
    protected String valCargoPersonaContactada;
    @XmlElement(nillable = true)
    protected String valPlazoEntrega;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecProgramadaSeguimiento;
    @XmlElement(nillable = true)
    protected String valEncargadoSeguimiento;
    @XmlElement(nillable = true)
    protected String desObservacionProgramacionSeguimiento;

    /**
     * Obtiene el valor de la propiedad idSeguimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSeguimiento() {
        return idSeguimiento;
    }

    /**
     * Define el valor de la propiedad idSeguimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSeguimiento(String value) {
        this.idSeguimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad valPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPersonaContactada(String value) {
        this.valPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoPersonaContactada() {
        return valCargoPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valCargoPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoPersonaContactada(String value) {
        this.valCargoPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPlazoEntrega() {
        return valPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPlazoEntrega(String value) {
        this.valPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad fecProgramadaSeguimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecProgramadaSeguimiento() {
        return fecProgramadaSeguimiento;
    }

    /**
     * Define el valor de la propiedad fecProgramadaSeguimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecProgramadaSeguimiento(Calendar value) {
        this.fecProgramadaSeguimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad valEncargadoSeguimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValEncargadoSeguimiento() {
        return valEncargadoSeguimiento;
    }

    /**
     * Define el valor de la propiedad valEncargadoSeguimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValEncargadoSeguimiento(String value) {
        this.valEncargadoSeguimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionProgramacionSeguimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionProgramacionSeguimiento() {
        return desObservacionProgramacionSeguimiento;
    }

    /**
     * Define el valor de la propiedad desObservacionProgramacionSeguimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionProgramacionSeguimiento(String value) {
        this.desObservacionProgramacionSeguimiento = value;
    }

}
