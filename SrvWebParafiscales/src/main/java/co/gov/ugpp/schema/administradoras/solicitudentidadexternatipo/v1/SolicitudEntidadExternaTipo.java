
package co.gov.ugpp.schema.administradoras.solicitudentidadexternatipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;


/**
 * <p>Clase Java para SolicitudEntidadExternaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudEntidadExternaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSolicitudEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="idRadicadoRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoRespuesta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="esValidaRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valObservacionesRespuesta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="seguimientos" type="{http://www.ugpp.gov.co/schema/Administradoras/SeguimientoTipo/v1}SeguimientoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valNumeroReiteracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudEntidadExternaTipo", propOrder = {
    "idSolicitudEntidadExterna",
    "entidadExterna",
    "idRadicadoRespuesta",
    "fecRadicadoRespuesta",
    "esValidaRespuesta",
    "valObservacionesRespuesta",
    "seguimientos",
    "valNumeroReiteracion",
    "codTipoEstado"
})
public class SolicitudEntidadExternaTipo {

    @XmlElement(nillable = true)
    protected String idSolicitudEntidadExterna;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected String idRadicadoRespuesta;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecRadicadoRespuesta;
    @XmlElement(nillable = true)
    protected String esValidaRespuesta;
    @XmlElement(nillable = true)
    protected String valObservacionesRespuesta;
    @XmlElement(nillable = true)
    protected List<SeguimientoTipo> seguimientos;
    @XmlElement(nillable = true)
    protected String valNumeroReiteracion;
    @XmlElement(nillable = true)
    protected String codTipoEstado;

    /**
     * Obtiene el valor de la propiedad idSolicitudEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSolicitudEntidadExterna() {
        return idSolicitudEntidadExterna;
    }

    /**
     * Define el valor de la propiedad idSolicitudEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSolicitudEntidadExterna(String value) {
        this.idSolicitudEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoRespuesta() {
        return idRadicadoRespuesta;
    }

    /**
     * Define el valor de la propiedad idRadicadoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoRespuesta(String value) {
        this.idRadicadoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoRespuesta() {
        return fecRadicadoRespuesta;
    }

    /**
     * Define el valor de la propiedad fecRadicadoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoRespuesta(Calendar value) {
        this.fecRadicadoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad esValidaRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsValidaRespuesta() {
        return esValidaRespuesta;
    }

    /**
     * Define el valor de la propiedad esValidaRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsValidaRespuesta(String value) {
        this.esValidaRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad valObservacionesRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValObservacionesRespuesta() {
        return valObservacionesRespuesta;
    }

    /**
     * Define el valor de la propiedad valObservacionesRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValObservacionesRespuesta(String value) {
        this.valObservacionesRespuesta = value;
    }

    /**
     * Gets the value of the seguimientos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seguimientos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeguimientos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeguimientoTipo }
     * 
     * 
     */
    public List<SeguimientoTipo> getSeguimientos() {
        if (seguimientos == null) {
            seguimientos = new ArrayList<SeguimientoTipo>();
        }
        return this.seguimientos;
    }

    /**
     * Obtiene el valor de la propiedad valNumeroReiteracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNumeroReiteracion() {
        return valNumeroReiteracion;
    }

    /**
     * Define el valor de la propiedad valNumeroReiteracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumeroReiteracion(String value) {
        this.valNumeroReiteracion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEstado() {
        return codTipoEstado;
    }

    /**
     * Define el valor de la propiedad codTipoEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEstado(String value) {
        this.codTipoEstado = value;
    }

}
