
package co.gov.ugpp.schema.comunes.acciontipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para AccionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AccionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fecEjecucionAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccionTipo", propOrder = {
    "fecEjecucionAccion",
    "codAccion",
    "descAccion"
})
public class AccionTipo {

    @XmlElement(nillable = true)
    protected String fecEjecucionAccion;
    @XmlElement(nillable = true)
    protected String codAccion;
    @XmlElement(nillable = true)
    protected String descAccion;

    /**
     * Obtiene el valor de la propiedad fecEjecucionAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecEjecucionAccion() {
        return fecEjecucionAccion;
    }

    /**
     * Define el valor de la propiedad fecEjecucionAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEjecucionAccion(String value) {
        this.fecEjecucionAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAccion() {
        return codAccion;
    }

    /**
     * Define el valor de la propiedad codAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAccion(String value) {
        this.codAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad descAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAccion() {
        return descAccion;
    }

    /**
     * Define el valor de la propiedad descAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAccion(String value) {
        this.descAccion = value;
    }

}
