
package co.gov.ugpp.schema.denuncias.afectadotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;


/**
 * <p>Clase Java para AfectadoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AfectadoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="personaJuridica" type="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo" minOccurs="0"/>
 *           &lt;element name="personaNatural" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="valIBC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="fecIngresoEmpresa" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AfectadoTipo", propOrder = {
    "personaJuridica",
    "personaNatural",
    "valIBC",
    "fecIngresoEmpresa"
})
public class AfectadoTipo {

    @XmlElement(nillable = true)
    protected PersonaJuridicaTipo personaJuridica;
    @XmlElement(nillable = true)
    protected PersonaNaturalTipo personaNatural;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valIBC;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecIngresoEmpresa;

    /**
     * Obtiene el valor de la propiedad personaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public PersonaJuridicaTipo getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     * Define el valor de la propiedad personaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public void setPersonaJuridica(PersonaJuridicaTipo value) {
        this.personaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad personaNatural.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNatural() {
        return personaNatural;
    }

    /**
     * Define el valor de la propiedad personaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNatural(PersonaNaturalTipo value) {
        this.personaNatural = value;
    }

    /**
     * Obtiene el valor de la propiedad valIBC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValIBC() {
        return valIBC;
    }

    /**
     * Define el valor de la propiedad valIBC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValIBC(Long value) {
        this.valIBC = value;
    }

    /**
     * Obtiene el valor de la propiedad fecIngresoEmpresa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecIngresoEmpresa() {
        return fecIngresoEmpresa;
    }

    /**
     * Define el valor de la propiedad fecIngresoEmpresa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecIngresoEmpresa(Calendar value) {
        this.fecIngresoEmpresa = value;
    }

}
