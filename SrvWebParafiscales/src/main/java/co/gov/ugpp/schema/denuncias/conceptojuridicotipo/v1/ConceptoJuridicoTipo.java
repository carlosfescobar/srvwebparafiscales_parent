
package co.gov.ugpp.schema.denuncias.conceptojuridicotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;


/**
 * <p>Clase Java para ConceptoJuridicoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConceptoJuridicoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idConceptoJuridico" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idNumeroExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAsuntoConceptoJuridico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPrioridad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descMotivoPrioridad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descPreguntas" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptoJuridicoTipo", propOrder = {
    "idConceptoJuridico",
    "idNumeroExpediente",
    "descAsuntoConceptoJuridico",
    "codPrioridad",
    "descMotivoPrioridad",
    "descPreguntas",
    "documento",
    "descObservaciones"
})
public class ConceptoJuridicoTipo {

    @XmlElement(required = true)
    protected String idConceptoJuridico;
    @XmlElement(nillable = true)
    protected String idNumeroExpediente;
    @XmlElement(nillable = true)
    protected String descAsuntoConceptoJuridico;
    @XmlElement(nillable = true)
    protected String codPrioridad;
    @XmlElement(nillable = true)
    protected String descMotivoPrioridad;
    @XmlElement(nillable = true)
    protected List<String> descPreguntas;
    @XmlElement(nillable = true)
    protected DocumentoTipo documento;
    @XmlElement(nillable = true)
    protected List<String> descObservaciones;

    /**
     * Obtiene el valor de la propiedad idConceptoJuridico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConceptoJuridico() {
        return idConceptoJuridico;
    }

    /**
     * Define el valor de la propiedad idConceptoJuridico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConceptoJuridico(String value) {
        this.idConceptoJuridico = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumeroExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumeroExpediente() {
        return idNumeroExpediente;
    }

    /**
     * Define el valor de la propiedad idNumeroExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumeroExpediente(String value) {
        this.idNumeroExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad descAsuntoConceptoJuridico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAsuntoConceptoJuridico() {
        return descAsuntoConceptoJuridico;
    }

    /**
     * Define el valor de la propiedad descAsuntoConceptoJuridico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAsuntoConceptoJuridico(String value) {
        this.descAsuntoConceptoJuridico = value;
    }

    /**
     * Obtiene el valor de la propiedad codPrioridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPrioridad() {
        return codPrioridad;
    }

    /**
     * Define el valor de la propiedad codPrioridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPrioridad(String value) {
        this.codPrioridad = value;
    }

    /**
     * Obtiene el valor de la propiedad descMotivoPrioridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMotivoPrioridad() {
        return descMotivoPrioridad;
    }

    /**
     * Define el valor de la propiedad descMotivoPrioridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMotivoPrioridad(String value) {
        this.descMotivoPrioridad = value;
    }

    /**
     * Gets the value of the descPreguntas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descPreguntas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescPreguntas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDescPreguntas() {
        if (descPreguntas == null) {
            descPreguntas = new ArrayList<String>();
        }
        return this.descPreguntas;
    }

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocumento(DocumentoTipo value) {
        this.documento = value;
    }

    /**
     * Gets the value of the descObservaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the descObservaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDescObservaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDescObservaciones() {
        if (descObservaciones == null) {
            descObservaciones = new ArrayList<String>();
        }
        return this.descObservaciones;
    }

}
