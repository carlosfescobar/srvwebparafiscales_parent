
package co.gov.ugpp.schema.denuncias.cotizantetipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;


/**
 * <p>Clase Java para CotizanteTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CotizanteTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo">
 *       &lt;sequence>
 *         &lt;element name="docCotizante" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *         &lt;element name="descObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CotizanteTipo", propOrder = {
    "docCotizante",
    "descObservacion"
})
public class CotizanteTipo
    extends PersonaNaturalTipo
{

    @XmlElement(nillable = true)
    protected ArchivoTipo docCotizante;
    @XmlElement(nillable = true)
    protected String descObservacion;

    /**
     * Obtiene el valor de la propiedad docCotizante.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getDocCotizante() {
        return docCotizante;
    }

    /**
     * Define el valor de la propiedad docCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setDocCotizante(ArchivoTipo value) {
        this.docCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacion() {
        return descObservacion;
    }

    /**
     * Define el valor de la propiedad descObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacion(String value) {
        this.descObservacion = value;
    }

}
