
package co.gov.ugpp.schema.denuncias.denunciantetipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para DenuncianteTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DenuncianteTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="personaNatural" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo" minOccurs="0"/>
 *           &lt;element name="personaJuridica" type="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="codTipoDenunciante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DenuncianteTipo", propOrder = {
    "personaNatural",
    "personaJuridica",
    "codTipoDenunciante"
})
public class DenuncianteTipo {

    @XmlElement(nillable = true)
    protected PersonaNaturalTipo personaNatural;
    @XmlElement(nillable = true)
    protected PersonaJuridicaTipo personaJuridica;
    @XmlElement(nillable = true)
    protected String codTipoDenunciante;

    /**
     * Obtiene el valor de la propiedad personaNatural.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNatural() {
        return personaNatural;
    }

    /**
     * Define el valor de la propiedad personaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNatural(PersonaNaturalTipo value) {
        this.personaNatural = value;
    }

    /**
     * Obtiene el valor de la propiedad personaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public PersonaJuridicaTipo getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     * Define el valor de la propiedad personaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public void setPersonaJuridica(PersonaJuridicaTipo value) {
        this.personaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDenunciante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDenunciante() {
        return codTipoDenunciante;
    }

    /**
     * Define el valor de la propiedad codTipoDenunciante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDenunciante(String value) {
        this.codTipoDenunciante = value;
    }

}
