
package co.gov.ugpp.schema.denuncias.denunciatipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.afectadotipo.v1.AfectadoTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.denunciantetipo.v1.DenuncianteTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para DenunciaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DenunciaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDecisionAnalisisPagos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalInicialDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codCausalAnalisisDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codSubsistema" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fecInicioPeriodo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="fecFinPeriodo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="codCanalDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denunciante" type="{http://www.ugpp.gov.co/schema/Denuncias/DenuncianteTipo/v1}DenuncianteTipo" minOccurs="0"/>
 *         &lt;element name="afectado" type="{http://www.ugpp.gov.co/schema/Denuncias/AfectadoTipo/v1}AfectadoTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCategoriaDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esCompetenciaUGPP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esCasoCerrado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAprobadoCierre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInformacionCompleta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descInformacionFaltante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esEnFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSuspenderTerminos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esTieneDerechoPeticion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esTieneAnexos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoEnvioComite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoSuspensionTerminos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoCompletitudInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoCierreCaso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoPersuasivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPagosValidados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="esCompetenciaParcial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="observaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DenunciaTipo", propOrder = {
    "idDenuncia",
    "idRadicadoDenuncia",
    "fecRadicadoDenuncia",
    "codDecisionAnalisisPagos",
    "esPago",
    "codCausalInicialDenuncia",
    "codCausalAnalisisDenuncia",
    "codSubsistema",
    "fecInicioPeriodo",
    "fecFinPeriodo",
    "codCanalDenuncia",
    "denunciante",
    "afectado",
    "aportante",
    "expediente",
    "descObservaciones",
    "codCategoriaDenuncia",
    "esCompetenciaUGPP",
    "esCasoCerrado",
    "esAprobadoCierre",
    "esInformacionCompleta",
    "descInformacionFaltante",
    "esEnFiscalizacion",
    "esSuspenderTerminos",
    "esTieneDerechoPeticion",
    "esTieneAnexos",
    "codTipoDenuncia",
    "codEstadoDenuncia",
    "codTipoEnvioComite",
    "idDocumentoSuspensionTerminos",
    "idDocumentoCompletitudInformacion",
    "idDocumentoCierreCaso",
    "idDocumentoPersuasivo",
    "esPagosValidados",
    "acciones",
    "esCompetenciaParcial",
    "observaciones"
})
public class DenunciaTipo {

    @XmlElement(nillable = true)
    protected String idDenuncia;
    @XmlElement(nillable = true)
    protected String idRadicadoDenuncia;
    @XmlElement(nillable = true)
    protected String fecRadicadoDenuncia;
    @XmlElement(nillable = true)
    protected String codDecisionAnalisisPagos;
    @XmlElement(nillable = true)
    protected String esPago;
    @XmlElement(nillable = true)
    protected List<String> codCausalInicialDenuncia;
    @XmlElement(nillable = true)
    protected List<String> codCausalAnalisisDenuncia;
    @XmlElement(nillable = true)
    protected List<String> codSubsistema;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecInicioPeriodo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecFinPeriodo;
    @XmlElement(nillable = true)
    protected String codCanalDenuncia;
    @XmlElement(nillable = true)
    protected DenuncianteTipo denunciante;
    @XmlElement(nillable = true)
    protected AfectadoTipo afectado;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String codCategoriaDenuncia;
    @XmlElement(nillable = true)
    protected String esCompetenciaUGPP;
    @XmlElement(nillable = true)
    protected String esCasoCerrado;
    @XmlElement(nillable = true)
    protected String esAprobadoCierre;
    @XmlElement(nillable = true)
    protected String esInformacionCompleta;
    @XmlElement(nillable = true)
    protected String descInformacionFaltante;
    @XmlElement(nillable = true)
    protected String esEnFiscalizacion;
    @XmlElement(nillable = true)
    protected String esSuspenderTerminos;
    @XmlElement(nillable = true)
    protected String esTieneDerechoPeticion;
    @XmlElement(nillable = true)
    protected String esTieneAnexos;
    @XmlElement(nillable = true)
    protected String codTipoDenuncia;
    @XmlElement(nillable = true)
    protected String codEstadoDenuncia;
    @XmlElement(nillable = true)
    protected String codTipoEnvioComite;
    @XmlElement(nillable = true)
    protected String idDocumentoSuspensionTerminos;
    @XmlElement(nillable = true)
    protected String idDocumentoCompletitudInformacion;
    @XmlElement(nillable = true)
    protected String idDocumentoCierreCaso;
    @XmlElement(nillable = true)
    protected String idDocumentoPersuasivo;
    @XmlElement(nillable = true)
    protected String esPagosValidados;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(nillable = true)
    protected String esCompetenciaParcial;
    @XmlElement(nillable = true)
    protected String observaciones;

    
    public String getObservaciones() {
        return observaciones;
    }

    
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    
    
    
    
    
    public String getIdDenuncia() {
        return idDenuncia;
    }

    
    public void setIdDenuncia(String value) {
        this.idDenuncia = value;
    }
    
    

    /**
     * Obtiene el valor de la propiedad idRadicadoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoDenuncia() {
        return idRadicadoDenuncia;
    }

    /**
     * Define el valor de la propiedad idRadicadoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoDenuncia(String value) {
        this.idRadicadoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecRadicadoDenuncia() {
        return fecRadicadoDenuncia;
    }

    /**
     * Define el valor de la propiedad fecRadicadoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoDenuncia(String value) {
        this.fecRadicadoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad codDecisionAnalisisPagos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodDecisionAnalisisPagos() {
        return codDecisionAnalisisPagos;
    }

    /**
     * Define el valor de la propiedad codDecisionAnalisisPagos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodDecisionAnalisisPagos(String value) {
        this.codDecisionAnalisisPagos = value;
    }

    /**
     * Obtiene el valor de la propiedad esPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPago() {
        return esPago;
    }

    /**
     * Define el valor de la propiedad esPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPago(String value) {
        this.esPago = value;
    }

    /**
     * Gets the value of the codCausalInicialDenuncia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codCausalInicialDenuncia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodCausalInicialDenuncia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodCausalInicialDenuncia() {
        if (codCausalInicialDenuncia == null) {
            codCausalInicialDenuncia = new ArrayList<String>();
        }
        return this.codCausalInicialDenuncia;
    }

    /**
     * Gets the value of the codCausalAnalisisDenuncia property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codCausalAnalisisDenuncia property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodCausalAnalisisDenuncia().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodCausalAnalisisDenuncia() {
        if (codCausalAnalisisDenuncia == null) {
            codCausalAnalisisDenuncia = new ArrayList<String>();
        }
        return this.codCausalAnalisisDenuncia;
    }

    /**
     * Gets the value of the codSubsistema property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codSubsistema property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodSubsistema().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodSubsistema() {
        if (codSubsistema == null) {
            codSubsistema = new ArrayList<String>();
        }
        return this.codSubsistema;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioPeriodo() {
        return fecInicioPeriodo;
    }

    /**
     * Define el valor de la propiedad fecInicioPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioPeriodo(Calendar value) {
        this.fecInicioPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinPeriodo() {
        return fecFinPeriodo;
    }

    /**
     * Define el valor de la propiedad fecFinPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinPeriodo(Calendar value) {
        this.fecFinPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad codCanalDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCanalDenuncia() {
        return codCanalDenuncia;
    }

    /**
     * Define el valor de la propiedad codCanalDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCanalDenuncia(String value) {
        this.codCanalDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad denunciante.
     * 
     * @return
     *     possible object is
     *     {@link DenuncianteTipo }
     *     
     */
    public DenuncianteTipo getDenunciante() {
        return denunciante;
    }

    /**
     * Define el valor de la propiedad denunciante.
     * 
     * @param value
     *     allowed object is
     *     {@link DenuncianteTipo }
     *     
     */
    public void setDenunciante(DenuncianteTipo value) {
        this.denunciante = value;
    }

    /**
     * Obtiene el valor de la propiedad afectado.
     * 
     * @return
     *     possible object is
     *     {@link AfectadoTipo }
     *     
     */
    public AfectadoTipo getAfectado() {
        return afectado;
    }

    /**
     * Define el valor de la propiedad afectado.
     * 
     * @param value
     *     allowed object is
     *     {@link AfectadoTipo }
     *     
     */
    public void setAfectado(AfectadoTipo value) {
        this.afectado = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad codCategoriaDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCategoriaDenuncia() {
        return codCategoriaDenuncia;
    }

    /**
     * Define el valor de la propiedad codCategoriaDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCategoriaDenuncia(String value) {
        this.codCategoriaDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad esCompetenciaUGPP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCompetenciaUGPP() {
        return esCompetenciaUGPP;
    }

    /**
     * Define el valor de la propiedad esCompetenciaUGPP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCompetenciaUGPP(String value) {
        this.esCompetenciaUGPP = value;
    }

    /**
     * Obtiene el valor de la propiedad esCasoCerrado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCasoCerrado() {
        return esCasoCerrado;
    }

    /**
     * Define el valor de la propiedad esCasoCerrado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCasoCerrado(String value) {
        this.esCasoCerrado = value;
    }

    /**
     * Obtiene el valor de la propiedad esAprobadoCierre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAprobadoCierre() {
        return esAprobadoCierre;
    }

    /**
     * Define el valor de la propiedad esAprobadoCierre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAprobadoCierre(String value) {
        this.esAprobadoCierre = value;
    }

    /**
     * Obtiene el valor de la propiedad esInformacionCompleta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInformacionCompleta() {
        return esInformacionCompleta;
    }

    /**
     * Define el valor de la propiedad esInformacionCompleta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInformacionCompleta(String value) {
        this.esInformacionCompleta = value;
    }

    /**
     * Obtiene el valor de la propiedad descInformacionFaltante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescInformacionFaltante() {
        return descInformacionFaltante;
    }

    /**
     * Define el valor de la propiedad descInformacionFaltante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescInformacionFaltante(String value) {
        this.descInformacionFaltante = value;
    }

    /**
     * Obtiene el valor de la propiedad esEnFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsEnFiscalizacion() {
        return esEnFiscalizacion;
    }

    /**
     * Define el valor de la propiedad esEnFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsEnFiscalizacion(String value) {
        this.esEnFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esSuspenderTerminos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSuspenderTerminos() {
        return esSuspenderTerminos;
    }

    /**
     * Define el valor de la propiedad esSuspenderTerminos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSuspenderTerminos(String value) {
        this.esSuspenderTerminos = value;
    }

    /**
     * Obtiene el valor de la propiedad esTieneDerechoPeticion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTieneDerechoPeticion() {
        return esTieneDerechoPeticion;
    }

    /**
     * Define el valor de la propiedad esTieneDerechoPeticion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTieneDerechoPeticion(String value) {
        this.esTieneDerechoPeticion = value;
    }

    /**
     * Obtiene el valor de la propiedad esTieneAnexos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTieneAnexos() {
        return esTieneAnexos;
    }

    /**
     * Define el valor de la propiedad esTieneAnexos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTieneAnexos(String value) {
        this.esTieneAnexos = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDenuncia() {
        return codTipoDenuncia;
    }

    /**
     * Define el valor de la propiedad codTipoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDenuncia(String value) {
        this.codTipoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoDenuncia() {
        return codEstadoDenuncia;
    }

    /**
     * Define el valor de la propiedad codEstadoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoDenuncia(String value) {
        this.codEstadoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEnvioComite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEnvioComite() {
        return codTipoEnvioComite;
    }

    /**
     * Define el valor de la propiedad codTipoEnvioComite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEnvioComite(String value) {
        this.codTipoEnvioComite = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoSuspensionTerminos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoSuspensionTerminos() {
        return idDocumentoSuspensionTerminos;
    }

    /**
     * Define el valor de la propiedad idDocumentoSuspensionTerminos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoSuspensionTerminos(String value) {
        this.idDocumentoSuspensionTerminos = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoCompletitudInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoCompletitudInformacion() {
        return idDocumentoCompletitudInformacion;
    }

    /**
     * Define el valor de la propiedad idDocumentoCompletitudInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoCompletitudInformacion(String value) {
        this.idDocumentoCompletitudInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoCierreCaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoCierreCaso() {
        return idDocumentoCierreCaso;
    }

    /**
     * Define el valor de la propiedad idDocumentoCierreCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoCierreCaso(String value) {
        this.idDocumentoCierreCaso = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoPersuasivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoPersuasivo() {
        return idDocumentoPersuasivo;
    }

    /**
     * Define el valor de la propiedad idDocumentoPersuasivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoPersuasivo(String value) {
        this.idDocumentoPersuasivo = value;
    }

    /**
     * Obtiene el valor de la propiedad esPagosValidados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPagosValidados() {
        return esPagosValidados;
    }

    /**
     * Define el valor de la propiedad esPagosValidados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPagosValidados(String value) {
        this.esPagosValidados = value;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad esCompetenciaParcial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCompetenciaParcial() {
        return esCompetenciaParcial;
    }

    /**
     * Define el valor de la propiedad esCompetenciaParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCompetenciaParcial(String value) {
        this.esCompetenciaParcial = value;
    }

}
