
package co.gov.ugpp.schema.denuncias.entidadexternatipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;


/**
 * <p>Clase Java para EntidadExternaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EntidadExternaTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo">
 *       &lt;sequence>
 *         &lt;element name="idEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPila" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoProvedorInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntidadExternaTipo", propOrder = {
    "idEntidadExterna",
    "codTipoEntidadExterna",
    "esPila",
    "codTipoProvedorInformacion"
})
public class EntidadExternaTipo
    extends PersonaJuridicaTipo
{

    @XmlElement(nillable = true)
    protected String idEntidadExterna;
    @XmlElement(nillable = true)
    protected String codTipoEntidadExterna;
    @XmlElement(nillable = true)
    protected String esPila;
    @XmlElement(nillable = true)
    protected String codTipoProvedorInformacion;

    /**
     * Obtiene el valor de la propiedad idEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEntidadExterna() {
        return idEntidadExterna;
    }

    /**
     * Define el valor de la propiedad idEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEntidadExterna(String value) {
        this.idEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEntidadExterna() {
        return codTipoEntidadExterna;
    }

    /**
     * Define el valor de la propiedad codTipoEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEntidadExterna(String value) {
        this.codTipoEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad esPila.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPila() {
        return esPila;
    }

    /**
     * Define el valor de la propiedad esPila.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPila(String value) {
        this.esPila = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoProvedorInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoProvedorInformacion() {
        return codTipoProvedorInformacion;
    }

    /**
     * Define el valor de la propiedad codTipoProvedorInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoProvedorInformacion(String value) {
        this.codTipoProvedorInformacion = value;
    }

}
