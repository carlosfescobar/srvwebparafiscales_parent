
package co.gov.ugpp.schema.denuncias.funcionariotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para FuncionarioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FuncionarioTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo">
 *       &lt;sequence>
 *         &lt;element name="idFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDependenciaFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FuncionarioTipo", propOrder = {
    "idFuncionario",
    "valCargoFuncionario",
    "valDependenciaFuncionario"
})
public class FuncionarioTipo
    extends PersonaNaturalTipo
{

    @XmlElement(nillable = true)
    protected String idFuncionario;
    @XmlElement(nillable = true)
    protected String valCargoFuncionario;
    @XmlElement(nillable = true)
    protected String valDependenciaFuncionario;

    /**
     * Obtiene el valor de la propiedad idFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFuncionario() {
        return idFuncionario;
    }

    /**
     * Define el valor de la propiedad idFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFuncionario(String value) {
        this.idFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoFuncionario() {
        return valCargoFuncionario;
    }

    /**
     * Define el valor de la propiedad valCargoFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoFuncionario(String value) {
        this.valCargoFuncionario = value;
    }

    /**
     * Obtiene el valor de la propiedad valDependenciaFuncionario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDependenciaFuncionario() {
        return valDependenciaFuncionario;
    }

    /**
     * Define el valor de la propiedad valDependenciaFuncionario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDependenciaFuncionario(String value) {
        this.valDependenciaFuncionario = value;
    }

}
