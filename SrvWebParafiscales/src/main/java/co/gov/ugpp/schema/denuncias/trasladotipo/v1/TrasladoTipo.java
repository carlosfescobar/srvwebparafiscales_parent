
package co.gov.ugpp.schema.denuncias.trasladotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para TrasladoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TrasladoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idTraslado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoTrasladoDenunciante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoTrasladoEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="funcionario" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="fecTraslado" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="descMotivoTraslado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoTraslado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaRadicadoDocumentoEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idRadicadoDocumentoEntidadExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denuncia" type="{http://www.ugpp.gov.co/schema/Denuncias/DenunciaTipo/v1}DenunciaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrasladoTipo", propOrder = {
    "idTraslado",
    "idDocumentoTrasladoDenunciante",
    "idDocumentoTrasladoEntidadExterna",
    "expediente",
    "entidadExterna",
    "funcionario",
    "fecTraslado",
    "descMotivoTraslado",
    "codEstadoTraslado",
    "fechaRadicadoDocumentoEntidadExterna",
    "idRadicadoDocumentoEntidadExterna",
    "denuncia"
})
public class TrasladoTipo {

    @XmlElement(nillable = true)
    protected String idTraslado;
    @XmlElement(nillable = true)
    protected String idDocumentoTrasladoDenunciante;
    @XmlElement(nillable = true)
    protected String idDocumentoTrasladoEntidadExterna;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected FuncionarioTipo funcionario;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecTraslado;
    @XmlElement(nillable = true)
    protected String descMotivoTraslado;
    @XmlElement(nillable = true)
    protected String codEstadoTraslado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaRadicadoDocumentoEntidadExterna;
    @XmlElement(nillable = true)
    protected String idRadicadoDocumentoEntidadExterna;
    @XmlElement(nillable = true)
    protected DenunciaTipo denuncia;

    /**
     * Obtiene el valor de la propiedad idTraslado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTraslado() {
        return idTraslado;
    }

    /**
     * Define el valor de la propiedad idTraslado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTraslado(String value) {
        this.idTraslado = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoTrasladoDenunciante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoTrasladoDenunciante() {
        return idDocumentoTrasladoDenunciante;
    }

    /**
     * Define el valor de la propiedad idDocumentoTrasladoDenunciante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoTrasladoDenunciante(String value) {
        this.idDocumentoTrasladoDenunciante = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoTrasladoEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoTrasladoEntidadExterna() {
        return idDocumentoTrasladoEntidadExterna;
    }

    /**
     * Define el valor de la propiedad idDocumentoTrasladoEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoTrasladoEntidadExterna(String value) {
        this.idDocumentoTrasladoEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionario.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionario() {
        return funcionario;
    }

    /**
     * Define el valor de la propiedad funcionario.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionario(FuncionarioTipo value) {
        this.funcionario = value;
    }

    /**
     * Obtiene el valor de la propiedad fecTraslado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecTraslado() {
        return fecTraslado;
    }

    /**
     * Define el valor de la propiedad fecTraslado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecTraslado(Calendar value) {
        this.fecTraslado = value;
    }

    /**
     * Obtiene el valor de la propiedad descMotivoTraslado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMotivoTraslado() {
        return descMotivoTraslado;
    }

    /**
     * Define el valor de la propiedad descMotivoTraslado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMotivoTraslado(String value) {
        this.descMotivoTraslado = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoTraslado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoTraslado() {
        return codEstadoTraslado;
    }

    /**
     * Define el valor de la propiedad codEstadoTraslado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoTraslado(String value) {
        this.codEstadoTraslado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaRadicadoDocumentoEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaRadicadoDocumentoEntidadExterna() {
        return fechaRadicadoDocumentoEntidadExterna;
    }

    /**
     * Define el valor de la propiedad fechaRadicadoDocumentoEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaRadicadoDocumentoEntidadExterna(Calendar value) {
        this.fechaRadicadoDocumentoEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoDocumentoEntidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoDocumentoEntidadExterna() {
        return idRadicadoDocumentoEntidadExterna;
    }

    /**
     * Define el valor de la propiedad idRadicadoDocumentoEntidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoDocumentoEntidadExterna(String value) {
        this.idRadicadoDocumentoEntidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad denuncia.
     * 
     * @return
     *     possible object is
     *     {@link DenunciaTipo }
     *     
     */
    public DenunciaTipo getDenuncia() {
        return denuncia;
    }

    /**
     * Define el valor de la propiedad denuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link DenunciaTipo }
     *     
     */
    public void setDenuncia(DenunciaTipo value) {
        this.denuncia = value;
    }

}
