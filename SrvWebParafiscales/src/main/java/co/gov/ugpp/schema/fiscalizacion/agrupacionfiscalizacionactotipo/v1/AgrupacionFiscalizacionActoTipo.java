
package co.gov.ugpp.schema.fiscalizacion.agrupacionfiscalizacionactotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1.FiscalizacionTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;


/**
 * <p>Clase Java para AgrupacionFiscalizacionActoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgrupacionFiscalizacionActoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fiscalizacion" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/FiscalizacionTipo/v1}FiscalizacionTipo" minOccurs="0"/>
 *         &lt;element name="actosAdministrativos" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgrupacionFiscalizacionActoTipo", propOrder = {
    "fiscalizacion",
    "actosAdministrativos"
})
public class AgrupacionFiscalizacionActoTipo {

    @XmlElement(nillable = true)
    protected FiscalizacionTipo fiscalizacion;
    @XmlElement(required = true, nillable = true)
    protected List<ActoAdministrativoTipo> actosAdministrativos;

    /**
     * Obtiene el valor de la propiedad fiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link FiscalizacionTipo }
     *     
     */
    public FiscalizacionTipo getFiscalizacion() {
        return fiscalizacion;
    }

    /**
     * Define el valor de la propiedad fiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link FiscalizacionTipo }
     *     
     */
    public void setFiscalizacion(FiscalizacionTipo value) {
        this.fiscalizacion = value;
    }

    /**
     * Gets the value of the actosAdministrativos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actosAdministrativos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActosAdministrativos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ActoAdministrativoTipo }
     * 
     * 
     */
    public List<ActoAdministrativoTipo> getActosAdministrativos() {
        if (actosAdministrativos == null) {
            actosAdministrativos = new ArrayList<ActoAdministrativoTipo>();
        }
        return this.actosAdministrativos;
    }

}
