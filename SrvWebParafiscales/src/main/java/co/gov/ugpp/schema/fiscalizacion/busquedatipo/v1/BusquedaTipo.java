
package co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;


/**
 * <p>Clase Java para BusquedaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="BusquedaTipo"> 
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idBusqueda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoBusqueda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecProgramadaVisita" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="funcionarioEncargado" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="desObservacionProgramacionVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoComisorio" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="codResultadoBusqueda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valTelefonoAportante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valEtapa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombreFuncionario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valExtension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BusquedaTipo", propOrder = {
    "idBusqueda",
    "codTipoBusqueda",
    "fecProgramadaVisita",
    "funcionarioEncargado",
    "desObservacionProgramacionVisita",
    "autoComisorio",
    "codResultadoBusqueda",
    "valPlazoEntrega",
    "descObservaciones",
    "valPersonaContactada",
    "valCargoPersonaContactada",
    "valTelefonoAportante",
    "valEtapa",
    "valNombreFuncionario",
    "valExtension"
})



public class BusquedaTipo {

    @XmlElement(nillable = true)
    protected String idBusqueda;
    @XmlElement(nillable = true)
    protected String codTipoBusqueda;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecProgramadaVisita;
    @XmlElement(nillable = true)
    protected FuncionarioTipo funcionarioEncargado;
    @XmlElement(nillable = true)
    protected String desObservacionProgramacionVisita;
    @XmlElement(nillable = true)
    protected DocumentoTipo autoComisorio;
    @XmlElement(nillable = true)
    protected String codResultadoBusqueda;
    @XmlElement(nillable = true)
    protected String valPlazoEntrega;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String valPersonaContactada;
    @XmlElement(nillable = true)
    protected String valCargoPersonaContactada;
    
    
    @XmlElement(nillable = true)
    protected String valTelefonoAportante;
    
    @XmlElement(nillable = true)
    protected String valEtapa;
    
    @XmlElement(nillable = true)
    protected String valNombreFuncionario;
    
    @XmlElement(nillable = true)
    protected String valExtension;

    
    
    
    public String getValTelefonoAportante() {
        return valTelefonoAportante;
    }

    public void setValTelefonoAportante(String valTelefonoAportante) {
        this.valTelefonoAportante = valTelefonoAportante;
    }

    public String getValEtapa() {
        return valEtapa;
    }

    public void setValEtapa(String valEtapa) {
        this.valEtapa = valEtapa;
    }

    public String getValNombreFuncionario() {
        return valNombreFuncionario;
    }

    public void setValNombreFuncionario(String valNombreFuncionario) {
        this.valNombreFuncionario = valNombreFuncionario;
    }

    public String getValExtension() {
        return valExtension;
    }

    public void setValExtension(String valExtension) {
        this.valExtension = valExtension;
    }
    
                 

    /**
     * Obtiene el valor de la propiedad idBusqueda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdBusqueda() {
        return idBusqueda;
    }

    /**
     * Define el valor de la propiedad idBusqueda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdBusqueda(String value) {
        this.idBusqueda = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoBusqueda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoBusqueda() {
        return codTipoBusqueda;
    }

    /**
     * Define el valor de la propiedad codTipoBusqueda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoBusqueda(String value) {
        this.codTipoBusqueda = value;
    }

    /**
     * Obtiene el valor de la propiedad fecProgramadaVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecProgramadaVisita() {
        return fecProgramadaVisita;
    }

    /**
     * Define el valor de la propiedad fecProgramadaVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecProgramadaVisita(Calendar value) {
        this.fecProgramadaVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionarioEncargado.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionarioEncargado() {
        return funcionarioEncargado;
    }

    /**
     * Define el valor de la propiedad funcionarioEncargado.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionarioEncargado(FuncionarioTipo value) {
        this.funcionarioEncargado = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionProgramacionVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionProgramacionVisita() {
        return desObservacionProgramacionVisita;
    }

    /**
     * Define el valor de la propiedad desObservacionProgramacionVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionProgramacionVisita(String value) {
        this.desObservacionProgramacionVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad autoComisorio.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getAutoComisorio() {
        return autoComisorio;
    }

    /**
     * Define el valor de la propiedad autoComisorio.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setAutoComisorio(DocumentoTipo value) {
        this.autoComisorio = value;
    }

    /**
     * Obtiene el valor de la propiedad codResultadoBusqueda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultadoBusqueda() {
        return codResultadoBusqueda;
    }

    /**
     * Define el valor de la propiedad codResultadoBusqueda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultadoBusqueda(String value) {
        this.codResultadoBusqueda = value;
    }

    /**
     * Obtiene el valor de la propiedad valPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPlazoEntrega() {
        return valPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPlazoEntrega(String value) {
        this.valPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad valPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPersonaContactada(String value) {
        this.valPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoPersonaContactada() {
        return valCargoPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valCargoPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoPersonaContactada(String value) {
        this.valCargoPersonaContactada = value;
    }

}
