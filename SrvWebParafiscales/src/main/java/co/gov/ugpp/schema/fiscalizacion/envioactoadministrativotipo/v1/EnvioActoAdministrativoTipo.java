
package co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;


/**
 * <p>Clase Java para EnvioActoAdministrativoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnvioActoAdministrativoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRadicadosSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadosSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ubicacionNotificada" type="{http://www.ugpp.gov.co/esb/schema/UbicacionPersonaTipo/v1}UbicacionPersonaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnvioActoAdministrativoTipo", propOrder = {
    "idRadicadosSalida",
    "fecRadicadosSalida",
    "descEstadoNotificacion",
    "codEstadoNotificacion",
    "codCausalDevolucion",
    "descCausalDevolucion",
    "ubicacionNotificada"
})
public class EnvioActoAdministrativoTipo {

    @XmlElement(nillable = true)
    protected String idRadicadosSalida;
    @XmlElement(nillable = true)
    protected String fecRadicadosSalida;
    @XmlElement(nillable = true)
    protected String descEstadoNotificacion;
    @XmlElement(nillable = true)
    protected String codEstadoNotificacion;
    @XmlElement(nillable = true)
    protected String codCausalDevolucion;
    @XmlElement(nillable = true)
    protected String descCausalDevolucion;
    @XmlElement(nillable = true)
    protected UbicacionPersonaTipo ubicacionNotificada;

    /**
     * Obtiene el valor de la propiedad idRadicadosSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadosSalida() {
        return idRadicadosSalida;
    }

    /**
     * Define el valor de la propiedad idRadicadosSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadosSalida(String value) {
        this.idRadicadosSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadosSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecRadicadosSalida() {
        return fecRadicadosSalida;
    }

    /**
     * Define el valor de la propiedad fecRadicadosSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadosSalida(String value) {
        this.fecRadicadosSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoNotificacion() {
        return descEstadoNotificacion;
    }

    /**
     * Define el valor de la propiedad descEstadoNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoNotificacion(String value) {
        this.descEstadoNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoNotificacion() {
        return codEstadoNotificacion;
    }

    /**
     * Define el valor de la propiedad codEstadoNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoNotificacion(String value) {
        this.codEstadoNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalDevolucion() {
        return codCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad codCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalDevolucion(String value) {
        this.codCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad descCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausalDevolucion() {
        return descCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad descCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausalDevolucion(String value) {
        this.descCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacionNotificada.
     * 
     * @return
     *     possible object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public UbicacionPersonaTipo getUbicacionNotificada() {
        return ubicacionNotificada;
    }

    /**
     * Define el valor de la propiedad ubicacionNotificada.
     * 
     * @param value
     *     allowed object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public void setUbicacionNotificada(UbicacionPersonaTipo value) {
        this.ubicacionNotificada = value;
    }

}
