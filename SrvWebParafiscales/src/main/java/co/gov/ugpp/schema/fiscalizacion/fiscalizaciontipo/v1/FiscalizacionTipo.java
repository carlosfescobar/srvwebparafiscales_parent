
package co.gov.ugpp.schema.fiscalizacion.fiscalizaciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.denunciatipo.v1.DenunciaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.fiscalizacion.busquedatipo.v1.BusquedaTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para FiscalizacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FiscalizacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="denuncia" type="{http://www.ugpp.gov.co/schema/Denuncias/DenunciaTipo/v1}DenunciaTipo" minOccurs="0"/>
 *         &lt;element name="idLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idActoAutoArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoArchivoParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="esEnvioComite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esMenorCuantia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esContinuadoProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesContinuadoProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esOmisoPension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esEnviadoComite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoAfiliacionCalculoActuarial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="periodoFiscalizacion" type="{http://www.ugpp.gov.co/esb/schema/RangoFechaTipo/v1}RangoFechaTipo" minOccurs="0"/>
 *         &lt;element name="codIncumplimiento" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="requerimientoDeclararCorregirParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoDeclararCorregir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="busquedas" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/BusquedaTipo/v1}BusquedaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="esMasActuaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesMasActuaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMecanismoConseguirInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descMecanismoConseguirInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesMecanismoConseguir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idActoInspeccionTributaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoCalculoActuarial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fiscalizador" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentoAutoComisorio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoHojaTrabajo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoRequerimientoAclaraciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codLineaAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valLineaAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codOrigenProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valOrigenProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombrePrograma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codEstadoFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoOficioInconsistencias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoOficioInconsistencias" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codCausalCierre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descGestionPersuasiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FiscalizacionTipo", propOrder = {
    "idFiscalizacion",
    "denuncia",
    "idLiquidador",
    "expediente",
    "aportante",
    "idActoRequerimientoInformacion",
    "idActoAutoArchivo",
    "autoArchivoParcial",
    "esEnvioComite",
    "esMenorCuantia",
    "esContinuadoProceso",
    "descObservacionesContinuadoProceso",
    "esOmisoPension",
    "esEnviadoComite",
    "idDocumentoAfiliacionCalculoActuarial",
    "periodoFiscalizacion",
    "codIncumplimiento",
    "requerimientoDeclararCorregirParcial",
    "idActoRequerimientoDeclararCorregir",
    "busquedas",
    "esMasActuaciones",
    "descObservacionesMasActuaciones",
    "codMecanismoConseguirInformacion",
    "descMecanismoConseguirInformacion",
    "descObservacionesMecanismoConseguir",
    "idActoInspeccionTributaria",
    "idDocumentoCalculoActuarial",
    "fiscalizador",
    "idDocumentoAutoComisorio",
    "idDocumentoHojaTrabajo",
    "idDocumentoRequerimientoAclaraciones",
    "codLineaAccion",
    "valLineaAccion",
    "codOrigenProceso",
    "valOrigenProceso",
    "valNombrePrograma",
    "acciones",
    "codEstadoFiscalizacion",
    "descEstadoFiscalizacion",
    "idRadicadoOficioInconsistencias",
    "fecRadicadoOficioInconsistencias",
    "codCausalCierre",
    "archivosTemporales",
    "descGestionPersuasiva"
})
public class FiscalizacionTipo {

    @XmlElement(nillable = true)
    protected String idFiscalizacion;
    @XmlElement(nillable = true)
    protected DenunciaTipo denuncia;
    @XmlElement(nillable = true)
    protected String idLiquidador;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoInformacion;
    @XmlElement(nillable = true)
    protected String idActoAutoArchivo;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoArchivoParcial;
    @XmlElement(nillable = true)
    protected String esEnvioComite;
    @XmlElement(nillable = true)
    protected String esMenorCuantia;
    @XmlElement(nillable = true)
    protected String esContinuadoProceso;
    @XmlElement(nillable = true)
    protected String descObservacionesContinuadoProceso;
    @XmlElement(nillable = true)
    protected String esOmisoPension;
    @XmlElement(nillable = true)
    protected String esEnviadoComite;
    @XmlElement(nillable = true)
    protected String idDocumentoAfiliacionCalculoActuarial;
    protected RangoFechaTipo periodoFiscalizacion;
    @XmlElement(nillable = true)
    protected List<String> codIncumplimiento;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo requerimientoDeclararCorregirParcial;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoDeclararCorregir;
    @XmlElement(nillable = true)
    protected List<BusquedaTipo> busquedas;
    @XmlElement(nillable = true)
    protected String esMasActuaciones;
    @XmlElement(nillable = true)
    protected String descObservacionesMasActuaciones;
    @XmlElement(nillable = true)
    protected String codMecanismoConseguirInformacion;
    @XmlElement(nillable = true)
    protected String descMecanismoConseguirInformacion;
    @XmlElement(nillable = true)
    protected String descObservacionesMecanismoConseguir;
    @XmlElement(nillable = true)
    protected String idActoInspeccionTributaria;
    @XmlElement(nillable = true)
    protected String idDocumentoCalculoActuarial;
    @XmlElement(nillable = true)
    protected FuncionarioTipo fiscalizador;
    @XmlElement(nillable = true)
    protected String idDocumentoAutoComisorio;
    @XmlElement(nillable = true)
    protected String idDocumentoHojaTrabajo;
    @XmlElement(nillable = true)
    protected String idDocumentoRequerimientoAclaraciones;
    @XmlElement(nillable = true)
    protected String codLineaAccion;
    @XmlElement(nillable = true)
    protected String valLineaAccion;
    @XmlElement(nillable = true)
    protected String codOrigenProceso;
    @XmlElement(nillable = true)
    protected String valOrigenProceso;
    @XmlElement(nillable = true)
    protected String valNombrePrograma;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(nillable = true)
    protected String codEstadoFiscalizacion;
    @XmlElement(nillable = true)
    protected String descEstadoFiscalizacion;
    @XmlElement(nillable = true)
    protected String idRadicadoOficioInconsistencias;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoOficioInconsistencias;
    @XmlElement(nillable = true)
    protected String codCausalCierre;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> archivosTemporales;
    @XmlElement(nillable = true)
    protected String descGestionPersuasiva;

    /**
     * Obtiene el valor de la propiedad idFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFiscalizacion() {
        return idFiscalizacion;
    }

    /**
     * Define el valor de la propiedad idFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFiscalizacion(String value) {
        this.idFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad denuncia.
     * 
     * @return
     *     possible object is
     *     {@link DenunciaTipo }
     *     
     */
    public DenunciaTipo getDenuncia() {
        return denuncia;
    }

    /**
     * Define el valor de la propiedad denuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link DenunciaTipo }
     *     
     */
    public void setDenuncia(DenunciaTipo value) {
        this.denuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidador() {
        return idLiquidador;
    }

    /**
     * Define el valor de la propiedad idLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidador(String value) {
        this.idLiquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoInformacion() {
        return idActoRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoInformacion(String value) {
        this.idActoRequerimientoInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAutoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAutoArchivo() {
        return idActoAutoArchivo;
    }

    /**
     * Define el valor de la propiedad idActoAutoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAutoArchivo(String value) {
        this.idActoAutoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivoParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    /**
     * Define el valor de la propiedad autoArchivoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoArchivoParcial(AprobacionDocumentoTipo value) {
        this.autoArchivoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad esEnvioComite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsEnvioComite() {
        return esEnvioComite;
    }

    /**
     * Define el valor de la propiedad esEnvioComite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsEnvioComite(String value) {
        this.esEnvioComite = value;
    }

    /**
     * Obtiene el valor de la propiedad esMenorCuantia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsMenorCuantia() {
        return esMenorCuantia;
    }

    /**
     * Define el valor de la propiedad esMenorCuantia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsMenorCuantia(String value) {
        this.esMenorCuantia = value;
    }

    /**
     * Obtiene el valor de la propiedad esContinuadoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsContinuadoProceso() {
        return esContinuadoProceso;
    }

    /**
     * Define el valor de la propiedad esContinuadoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsContinuadoProceso(String value) {
        this.esContinuadoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesContinuadoProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesContinuadoProceso() {
        return descObservacionesContinuadoProceso;
    }

    /**
     * Define el valor de la propiedad descObservacionesContinuadoProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesContinuadoProceso(String value) {
        this.descObservacionesContinuadoProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad esOmisoPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsOmisoPension() {
        return esOmisoPension;
    }

    /**
     * Define el valor de la propiedad esOmisoPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsOmisoPension(String value) {
        this.esOmisoPension = value;
    }

    /**
     * Obtiene el valor de la propiedad esEnviadoComite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsEnviadoComite() {
        return esEnviadoComite;
    }

    /**
     * Define el valor de la propiedad esEnviadoComite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsEnviadoComite(String value) {
        this.esEnviadoComite = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoAfiliacionCalculoActuarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoAfiliacionCalculoActuarial() {
        return idDocumentoAfiliacionCalculoActuarial;
    }

    /**
     * Define el valor de la propiedad idDocumentoAfiliacionCalculoActuarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoAfiliacionCalculoActuarial(String value) {
        this.idDocumentoAfiliacionCalculoActuarial = value;
    }

    /**
     * Obtiene el valor de la propiedad periodoFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link RangoFechaTipo }
     *     
     */
    public RangoFechaTipo getPeriodoFiscalizacion() {
        return periodoFiscalizacion;
    }

    /**
     * Define el valor de la propiedad periodoFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoFechaTipo }
     *     
     */
    public void setPeriodoFiscalizacion(RangoFechaTipo value) {
        this.periodoFiscalizacion = value;
    }

    /**
     * Gets the value of the codIncumplimiento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codIncumplimiento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodIncumplimiento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodIncumplimiento() {
        if (codIncumplimiento == null) {
            codIncumplimiento = new ArrayList<String>();
        }
        return this.codIncumplimiento;
    }

    /**
     * Obtiene el valor de la propiedad requerimientoDeclararCorregirParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getRequerimientoDeclararCorregirParcial() {
        return requerimientoDeclararCorregirParcial;
    }

    /**
     * Define el valor de la propiedad requerimientoDeclararCorregirParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setRequerimientoDeclararCorregirParcial(AprobacionDocumentoTipo value) {
        this.requerimientoDeclararCorregirParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoDeclararCorregir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoDeclararCorregir() {
        return idActoRequerimientoDeclararCorregir;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoDeclararCorregir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoDeclararCorregir(String value) {
        this.idActoRequerimientoDeclararCorregir = value;
    }

    /**
     * Gets the value of the busquedas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the busquedas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBusquedas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BusquedaTipo }
     * 
     * 
     */
    public List<BusquedaTipo> getBusquedas() {
        if (busquedas == null) {
            busquedas = new ArrayList<BusquedaTipo>();
        }
        return this.busquedas;
    }

    /**
     * Obtiene el valor de la propiedad esMasActuaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsMasActuaciones() {
        return esMasActuaciones;
    }

    /**
     * Define el valor de la propiedad esMasActuaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsMasActuaciones(String value) {
        this.esMasActuaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesMasActuaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesMasActuaciones() {
        return descObservacionesMasActuaciones;
    }

    /**
     * Define el valor de la propiedad descObservacionesMasActuaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesMasActuaciones(String value) {
        this.descObservacionesMasActuaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad codMecanismoConseguirInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMecanismoConseguirInformacion() {
        return codMecanismoConseguirInformacion;
    }

    /**
     * Define el valor de la propiedad codMecanismoConseguirInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMecanismoConseguirInformacion(String value) {
        this.codMecanismoConseguirInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descMecanismoConseguirInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescMecanismoConseguirInformacion() {
        return descMecanismoConseguirInformacion;
    }

    /**
     * Define el valor de la propiedad descMecanismoConseguirInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescMecanismoConseguirInformacion(String value) {
        this.descMecanismoConseguirInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesMecanismoConseguir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesMecanismoConseguir() {
        return descObservacionesMecanismoConseguir;
    }

    /**
     * Define el valor de la propiedad descObservacionesMecanismoConseguir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesMecanismoConseguir(String value) {
        this.descObservacionesMecanismoConseguir = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoInspeccionTributaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoInspeccionTributaria() {
        return idActoInspeccionTributaria;
    }

    /**
     * Define el valor de la propiedad idActoInspeccionTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoInspeccionTributaria(String value) {
        this.idActoInspeccionTributaria = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoCalculoActuarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoCalculoActuarial() {
        return idDocumentoCalculoActuarial;
    }

    /**
     * Define el valor de la propiedad idDocumentoCalculoActuarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoCalculoActuarial(String value) {
        this.idDocumentoCalculoActuarial = value;
    }

    /**
     * Obtiene el valor de la propiedad fiscalizador.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFiscalizador() {
        return fiscalizador;
    }

    /**
     * Define el valor de la propiedad fiscalizador.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFiscalizador(FuncionarioTipo value) {
        this.fiscalizador = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoAutoComisorio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoAutoComisorio() {
        return idDocumentoAutoComisorio;
    }

    /**
     * Define el valor de la propiedad idDocumentoAutoComisorio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoAutoComisorio(String value) {
        this.idDocumentoAutoComisorio = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoHojaTrabajo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoHojaTrabajo() {
        return idDocumentoHojaTrabajo;
    }

    /**
     * Define el valor de la propiedad idDocumentoHojaTrabajo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoHojaTrabajo(String value) {
        this.idDocumentoHojaTrabajo = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoRequerimientoAclaraciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoRequerimientoAclaraciones() {
        return idDocumentoRequerimientoAclaraciones;
    }

    /**
     * Define el valor de la propiedad idDocumentoRequerimientoAclaraciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoRequerimientoAclaraciones(String value) {
        this.idDocumentoRequerimientoAclaraciones = value;
    }

    /**
     * Obtiene el valor de la propiedad codLineaAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodLineaAccion() {
        return codLineaAccion;
    }

    /**
     * Define el valor de la propiedad codLineaAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodLineaAccion(String value) {
        this.codLineaAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad valLineaAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLineaAccion() {
        return valLineaAccion;
    }

    /**
     * Define el valor de la propiedad valLineaAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLineaAccion(String value) {
        this.valLineaAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codOrigenProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrigenProceso() {
        return codOrigenProceso;
    }

    /**
     * Define el valor de la propiedad codOrigenProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrigenProceso(String value) {
        this.codOrigenProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad valOrigenProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValOrigenProceso() {
        return valOrigenProceso;
    }

    /**
     * Define el valor de la propiedad valOrigenProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValOrigenProceso(String value) {
        this.valOrigenProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombrePrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombrePrograma() {
        return valNombrePrograma;
    }

    /**
     * Define el valor de la propiedad valNombrePrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombrePrograma(String value) {
        this.valNombrePrograma = value;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoFiscalizacion() {
        return codEstadoFiscalizacion;
    }

    /**
     * Define el valor de la propiedad codEstadoFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoFiscalizacion(String value) {
        this.codEstadoFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoFiscalizacion() {
        return descEstadoFiscalizacion;
    }

    /**
     * Define el valor de la propiedad descEstadoFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoFiscalizacion(String value) {
        this.descEstadoFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoOficioInconsistencias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoOficioInconsistencias() {
        return idRadicadoOficioInconsistencias;
    }

    /**
     * Define el valor de la propiedad idRadicadoOficioInconsistencias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoOficioInconsistencias(String value) {
        this.idRadicadoOficioInconsistencias = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoOficioInconsistencias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoOficioInconsistencias() {
        return fecRadicadoOficioInconsistencias;
    }

    /**
     * Define el valor de la propiedad fecRadicadoOficioInconsistencias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoOficioInconsistencias(Calendar value) {
        this.fecRadicadoOficioInconsistencias = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalCierre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalCierre() {
        return codCausalCierre;
    }

    /**
     * Define el valor de la propiedad codCausalCierre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalCierre(String value) {
        this.codCausalCierre = value;
    }

    /**
     * Gets the value of the archivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.archivosTemporales;
    }

    /**
     * Obtiene el valor de la propiedad descGestionPersuasiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescGestionPersuasiva() {
        return descGestionPersuasiva;
    }

    /**
     * Define el valor de la propiedad descGestionPersuasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescGestionPersuasiva(String value) {
        this.descGestionPersuasiva = value;
    }

}
