
package co.gov.ugpp.schema.fiscalizacion.requerimientoinformaciontipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para RequerimientoInformacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RequerimientoInformacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntradaRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoEntradaRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequerimientoInformacionTipo", propOrder = {
    "codCausalDevolucion",
    "idActoRequerimientoInformacion",
    "idRadicadoEntradaRequerimientoInformacion",
    "fecRadicadoEntradaRequerimientoInformacion"
})
public class RequerimientoInformacionTipo {

    @XmlElement(nillable = true)
    protected List<String> codCausalDevolucion;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoInformacion;
    @XmlElement(nillable = true)
    protected String idRadicadoEntradaRequerimientoInformacion;
    @XmlElement(nillable = true)
    protected String fecRadicadoEntradaRequerimientoInformacion;

    /**
     * Gets the value of the codCausalDevolucion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codCausalDevolucion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodCausalDevolucion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodCausalDevolucion() {
        if (codCausalDevolucion == null) {
            codCausalDevolucion = new ArrayList<String>();
        }
        return this.codCausalDevolucion;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoInformacion() {
        return idActoRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoInformacion(String value) {
        this.idActoRequerimientoInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntradaRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntradaRequerimientoInformacion() {
        return idRadicadoEntradaRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntradaRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntradaRequerimientoInformacion(String value) {
        this.idRadicadoEntradaRequerimientoInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoEntradaRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecRadicadoEntradaRequerimientoInformacion() {
        return fecRadicadoEntradaRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad fecRadicadoEntradaRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoEntradaRequerimientoInformacion(String value) {
        this.fecRadicadoEntradaRequerimientoInformacion = value;
    }

}
