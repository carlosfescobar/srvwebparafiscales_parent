
package co.gov.ugpp.schema.fiscalizacion.respuestarequerimientoinformaciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para RespuestaRequerimientoInformacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RespuestaRequerimientoInformacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRespuestaRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codListadoChequeo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaRequerimientoInformacionTipo", propOrder = {
    "idRespuestaRequerimientoInformacion",
    "idFiscalizacion",
    "idRadicadoEntrada",
    "fecRadicadoEntrada",
    "codListadoChequeo"
})
public class RespuestaRequerimientoInformacionTipo {

    @XmlElement(nillable = true)
    protected String idRespuestaRequerimientoInformacion;
    @XmlElement(nillable = true)
    protected String idFiscalizacion;
    @XmlElement(nillable = true)
    protected String idRadicadoEntrada;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoEntrada;
    @XmlElement(nillable = true)
    protected List<String> codListadoChequeo;

    /**
     * Obtiene el valor de la propiedad idRespuestaRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRespuestaRequerimientoInformacion() {
        return idRespuestaRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad idRespuestaRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRespuestaRequerimientoInformacion(String value) {
        this.idRespuestaRequerimientoInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFiscalizacion() {
        return idFiscalizacion;
    }

    /**
     * Define el valor de la propiedad idFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFiscalizacion(String value) {
        this.idFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntrada(String value) {
        this.idRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoEntrada() {
        return fecRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad fecRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoEntrada(Calendar value) {
        this.fecRadicadoEntrada = value;
    }

    /**
     * Gets the value of the codListadoChequeo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codListadoChequeo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodListadoChequeo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodListadoChequeo() {
        if (codListadoChequeo == null) {
            codListadoChequeo = new ArrayList<String>();
        }
        return this.codListadoChequeo;
    }

}
