
package co.gov.ugpp.schema.fiscalizar.fiscalizaciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.fiscalizar.llamadatipo.v1.LlamadaTipo;
import co.gov.ugpp.schema.fiscalizar.visitatipo.v1.VisitaTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.transversales.liquidadortipo.v1.LiquidadorTipo;
import co.gov.ugpp.schema.transversales.validaciondocumentotipo.v1.ValidacionDocumentoTipo;


/**
 * <p>Clase Java para FiscalizacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FiscalizacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="codOrigenProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoLineaAcccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPrograma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visitas" type="{http://www.ugpp.gov.co/schema/Fiscalizar/VisitaTipo/v1}VisitaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="llamadas" type="{http://www.ugpp.gov.co/schema/Fiscalizar/LlamadaTipo/v1}LlamadaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fecVisita" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="valVisitaFiscalizador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valVisitaObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esBalancePruebas" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAuxCuentasContables" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esLibroMayorBalances" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esCertificacionVacaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esConvenciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esRelacionTrabajadoresMision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esCertificacionAccionistasSocios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tercero" type="{http://www.ugpp.gov.co/schema/Sanciones/TerceroTipo/v1}TerceroTipo" minOccurs="0"/>
 *         &lt;element name="codTipoAccionTomar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoCausalCierre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valAnalisisObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoArchivo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *         &lt;element name="docNomina" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="docContabilidad" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="valLiquidadorObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="fecFinFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="esMora" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInexactitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esOmisión" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reqDeclararCorregir" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *         &lt;element name="validacionDocumentos" type="{http://www.ugpp.gov.co/schema/Transversales/ValidacionDocumentoTipo/v1}ValidacionDocumentoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="liquidador" type="{http://www.ugpp.gov.co/schema/Transversales/LiquidadorTipo/v1}LiquidadorTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FiscalizacionTipo", propOrder = {
    "idExpediente",
    "aportante",
    "expediente",
    "codOrigenProceso",
    "codTipoLineaAcccion",
    "codTipoDenuncia",
    "valPrograma",
    "visitas",
    "llamadas",
    "fecVisita",
    "valVisitaFiscalizador",
    "valVisitaObservacion",
    "esBalancePruebas",
    "esNomina",
    "esAuxCuentasContables",
    "esLibroMayorBalances",
    "esCertificacionVacaciones",
    "esConvenciones",
    "esRelacionTrabajadoresMision",
    "esCertificacionAccionistasSocios",
    "tercero",
    "codTipoAccionTomar",
    "codTipoCausalCierre",
    "valAnalisisObservacion",
    "autoArchivo",
    "docNomina",
    "docContabilidad",
    "valLiquidadorObservacion",
    "fecInicioFiscalizacion",
    "fecFinFiscalizacion",
    "esMora",
    "esInexactitud",
    "esOmisi\u00f3n",
    "reqDeclararCorregir",
    "validacionDocumentos",
    "liquidador"
})
public class FiscalizacionTipo {

    @XmlElement(required = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String codOrigenProceso;
    @XmlElement(nillable = true)
    protected String codTipoLineaAcccion;
    @XmlElement(nillable = true)
    protected String codTipoDenuncia;
    @XmlElement(nillable = true)
    protected String valPrograma;
    @XmlElement(nillable = true)
    protected List<VisitaTipo> visitas;
    @XmlElement(nillable = true)
    protected List<LlamadaTipo> llamadas;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecVisita;
    @XmlElement(nillable = true)
    protected String valVisitaFiscalizador;
    @XmlElement(nillable = true)
    protected String valVisitaObservacion;
    @XmlElement(nillable = true)
    protected String esBalancePruebas;
    @XmlElement(nillable = true)
    protected String esNomina;
    @XmlElement(nillable = true)
    protected String esAuxCuentasContables;
    @XmlElement(nillable = true)
    protected String esLibroMayorBalances;
    @XmlElement(nillable = true)
    protected String esCertificacionVacaciones;
    @XmlElement(nillable = true)
    protected String esConvenciones;
    @XmlElement(nillable = true)
    protected String esRelacionTrabajadoresMision;
    @XmlElement(nillable = true)
    protected String esCertificacionAccionistasSocios;
    @XmlElement(nillable = true)
    protected String codTipoAccionTomar;
    @XmlElement(nillable = true)
    protected String codTipoCausalCierre;
    @XmlElement(nillable = true)
    protected String valAnalisisObservacion;
    @XmlElement(nillable = true)
    protected ActoAdministrativoTipo autoArchivo;
    @XmlElement(nillable = true)
    protected DocumentoTipo docNomina;
    @XmlElement(nillable = true)
    protected DocumentoTipo docContabilidad;
    @XmlElement(nillable = true)
    protected String valLiquidadorObservacion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecInicioFiscalizacion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecFinFiscalizacion;
    @XmlElement(nillable = true)
    protected String esMora;
    @XmlElement(nillable = true)
    protected String esInexactitud;
    @XmlElement(nillable = true)
    protected String esOmisión;
    @XmlElement(nillable = true)
    protected ActoAdministrativoTipo reqDeclararCorregir;
    @XmlElement(nillable = true)
    protected List<ValidacionDocumentoTipo> validacionDocumentos;
    @XmlElement(nillable = true)
    protected LiquidadorTipo liquidador;

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codOrigenProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrigenProceso() {
        return codOrigenProceso;
    }

    /**
     * Define el valor de la propiedad codOrigenProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrigenProceso(String value) {
        this.codOrigenProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoLineaAcccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoLineaAcccion() {
        return codTipoLineaAcccion;
    }

    /**
     * Define el valor de la propiedad codTipoLineaAcccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoLineaAcccion(String value) {
        this.codTipoLineaAcccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDenuncia() {
        return codTipoDenuncia;
    }

    /**
     * Define el valor de la propiedad codTipoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDenuncia(String value) {
        this.codTipoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad valPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPrograma() {
        return valPrograma;
    }

    /**
     * Define el valor de la propiedad valPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPrograma(String value) {
        this.valPrograma = value;
    }

    /**
     * Gets the value of the visitas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the visitas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVisitas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VisitaTipo }
     * 
     * 
     */
    public List<VisitaTipo> getVisitas() {
        if (visitas == null) {
            visitas = new ArrayList<VisitaTipo>();
        }
        return this.visitas;
    }

    /**
     * Gets the value of the llamadas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the llamadas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLlamadas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LlamadaTipo }
     * 
     * 
     */
    public List<LlamadaTipo> getLlamadas() {
        if (llamadas == null) {
            llamadas = new ArrayList<LlamadaTipo>();
        }
        return this.llamadas;
    }

    /**
     * Obtiene el valor de la propiedad fecVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecVisita() {
        return fecVisita;
    }

    /**
     * Define el valor de la propiedad fecVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecVisita(Calendar value) {
        this.fecVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad valVisitaFiscalizador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValVisitaFiscalizador() {
        return valVisitaFiscalizador;
    }

    /**
     * Define el valor de la propiedad valVisitaFiscalizador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValVisitaFiscalizador(String value) {
        this.valVisitaFiscalizador = value;
    }

    /**
     * Obtiene el valor de la propiedad valVisitaObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValVisitaObservacion() {
        return valVisitaObservacion;
    }

    /**
     * Define el valor de la propiedad valVisitaObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValVisitaObservacion(String value) {
        this.valVisitaObservacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esBalancePruebas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsBalancePruebas() {
        return esBalancePruebas;
    }

    /**
     * Define el valor de la propiedad esBalancePruebas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsBalancePruebas(String value) {
        this.esBalancePruebas = value;
    }

    /**
     * Obtiene el valor de la propiedad esNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsNomina() {
        return esNomina;
    }

    /**
     * Define el valor de la propiedad esNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsNomina(String value) {
        this.esNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad esAuxCuentasContables.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAuxCuentasContables() {
        return esAuxCuentasContables;
    }

    /**
     * Define el valor de la propiedad esAuxCuentasContables.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAuxCuentasContables(String value) {
        this.esAuxCuentasContables = value;
    }

    /**
     * Obtiene el valor de la propiedad esLibroMayorBalances.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsLibroMayorBalances() {
        return esLibroMayorBalances;
    }

    /**
     * Define el valor de la propiedad esLibroMayorBalances.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsLibroMayorBalances(String value) {
        this.esLibroMayorBalances = value;
    }

    /**
     * Obtiene el valor de la propiedad esCertificacionVacaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCertificacionVacaciones() {
        return esCertificacionVacaciones;
    }

    /**
     * Define el valor de la propiedad esCertificacionVacaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCertificacionVacaciones(String value) {
        this.esCertificacionVacaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad esConvenciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConvenciones() {
        return esConvenciones;
    }

    /**
     * Define el valor de la propiedad esConvenciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConvenciones(String value) {
        this.esConvenciones = value;
    }

    /**
     * Obtiene el valor de la propiedad esRelacionTrabajadoresMision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsRelacionTrabajadoresMision() {
        return esRelacionTrabajadoresMision;
    }

    /**
     * Define el valor de la propiedad esRelacionTrabajadoresMision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsRelacionTrabajadoresMision(String value) {
        this.esRelacionTrabajadoresMision = value;
    }

    /**
     * Obtiene el valor de la propiedad esCertificacionAccionistasSocios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCertificacionAccionistasSocios() {
        return esCertificacionAccionistasSocios;
    }

    /**
     * Define el valor de la propiedad esCertificacionAccionistasSocios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCertificacionAccionistasSocios(String value) {
        this.esCertificacionAccionistasSocios = value;
    }

    
    /**
     * Obtiene el valor de la propiedad codTipoAccionTomar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoAccionTomar() {
        return codTipoAccionTomar;
    }

    /**
     * Define el valor de la propiedad codTipoAccionTomar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoAccionTomar(String value) {
        this.codTipoAccionTomar = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoCausalCierre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoCausalCierre() {
        return codTipoCausalCierre;
    }

    /**
     * Define el valor de la propiedad codTipoCausalCierre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoCausalCierre(String value) {
        this.codTipoCausalCierre = value;
    }

    /**
     * Obtiene el valor de la propiedad valAnalisisObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValAnalisisObservacion() {
        return valAnalisisObservacion;
    }

    /**
     * Define el valor de la propiedad valAnalisisObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValAnalisisObservacion(String value) {
        this.valAnalisisObservacion = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getAutoArchivo() {
        return autoArchivo;
    }

    /**
     * Define el valor de la propiedad autoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setAutoArchivo(ActoAdministrativoTipo value) {
        this.autoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad docNomina.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocNomina() {
        return docNomina;
    }

    /**
     * Define el valor de la propiedad docNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocNomina(DocumentoTipo value) {
        this.docNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad docContabilidad.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocContabilidad() {
        return docContabilidad;
    }

    /**
     * Define el valor de la propiedad docContabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocContabilidad(DocumentoTipo value) {
        this.docContabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad valLiquidadorObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLiquidadorObservacion() {
        return valLiquidadorObservacion;
    }

    /**
     * Define el valor de la propiedad valLiquidadorObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLiquidadorObservacion(String value) {
        this.valLiquidadorObservacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioFiscalizacion() {
        return fecInicioFiscalizacion;
    }

    /**
     * Define el valor de la propiedad fecInicioFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioFiscalizacion(Calendar value) {
        this.fecInicioFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinFiscalizacion() {
        return fecFinFiscalizacion;
    }

    /**
     * Define el valor de la propiedad fecFinFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinFiscalizacion(Calendar value) {
        this.fecFinFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esMora.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsMora() {
        return esMora;
    }

    /**
     * Define el valor de la propiedad esMora.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsMora(String value) {
        this.esMora = value;
    }

    /**
     * Obtiene el valor de la propiedad esInexactitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInexactitud() {
        return esInexactitud;
    }

    /**
     * Define el valor de la propiedad esInexactitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInexactitud(String value) {
        this.esInexactitud = value;
    }

    /**
     * Obtiene el valor de la propiedad esOmisión.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsOmisión() {
        return esOmisión;
    }

    /**
     * Define el valor de la propiedad esOmisión.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsOmisión(String value) {
        this.esOmisión = value;
    }

    /**
     * Obtiene el valor de la propiedad reqDeclararCorregir.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getReqDeclararCorregir() {
        return reqDeclararCorregir;
    }

    /**
     * Define el valor de la propiedad reqDeclararCorregir.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setReqDeclararCorregir(ActoAdministrativoTipo value) {
        this.reqDeclararCorregir = value;
    }

    /**
     * Gets the value of the validacionDocumentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validacionDocumentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidacionDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidacionDocumentoTipo }
     * 
     * 
     */
    public List<ValidacionDocumentoTipo> getValidacionDocumentos() {
        if (validacionDocumentos == null) {
            validacionDocumentos = new ArrayList<ValidacionDocumentoTipo>();
        }
        return this.validacionDocumentos;
    }

    /**
     * Obtiene el valor de la propiedad liquidador.
     * 
     * @return
     *     possible object is
     *     {@link LiquidadorTipo }
     *     
     */
    public LiquidadorTipo getLiquidador() {
        return liquidador;
    }

    /**
     * Define el valor de la propiedad liquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link LiquidadorTipo }
     *     
     */
    public void setLiquidador(LiquidadorTipo value) {
        this.liquidador = value;
    }

}
