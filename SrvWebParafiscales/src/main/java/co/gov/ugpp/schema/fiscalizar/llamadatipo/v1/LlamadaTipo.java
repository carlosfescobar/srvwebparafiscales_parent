
package co.gov.ugpp.schema.fiscalizar.llamadatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;


/**
 * <p>Clase Java para LlamadaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LlamadaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLlamada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codResultadoLlamada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecProgramadaLlamada" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="valEncargadoLlamada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionProgramacionLlamada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LlamadaTipo", propOrder = {
    "idLlamada",
    "idExpediente",
    "codResultadoLlamada",
    "descObservaciones",
    "valPersonaContactada",
    "valCargoPersonaContactada",
    "valPlazoEntrega",
    "fecProgramadaLlamada",
    "valEncargadoLlamada",
    "desObservacionProgramacionLlamada"
})
public class LlamadaTipo {

    @XmlElement(nillable = true)
    protected String idLlamada;
    @XmlElement(nillable = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected String codResultadoLlamada;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String valPersonaContactada;
    @XmlElement(nillable = true)
    protected String valCargoPersonaContactada;
    @XmlElement(nillable = true)
    protected String valPlazoEntrega;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecProgramadaLlamada;
    @XmlElement(nillable = true)
    protected String valEncargadoLlamada;
    @XmlElement(nillable = true)
    protected String desObservacionProgramacionLlamada;

    /**
     * Obtiene el valor de la propiedad idLlamada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLlamada() {
        return idLlamada;
    }

    /**
     * Define el valor de la propiedad idLlamada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLlamada(String value) {
        this.idLlamada = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codResultadoLlamada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultadoLlamada() {
        return codResultadoLlamada;
    }

    /**
     * Define el valor de la propiedad codResultadoLlamada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultadoLlamada(String value) {
        this.codResultadoLlamada = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad valPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPersonaContactada(String value) {
        this.valPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoPersonaContactada() {
        return valCargoPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valCargoPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoPersonaContactada(String value) {
        this.valCargoPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPlazoEntrega() {
        return valPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPlazoEntrega(String value) {
        this.valPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad fecProgramadaLlamada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecProgramadaLlamada() {
        return fecProgramadaLlamada;
    }

    /**
     * Define el valor de la propiedad fecProgramadaLlamada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecProgramadaLlamada(Calendar value) {
        this.fecProgramadaLlamada = value;
    }

    /**
     * Obtiene el valor de la propiedad valEncargadoLlamada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValEncargadoLlamada() {
        return valEncargadoLlamada;
    }

    /**
     * Define el valor de la propiedad valEncargadoLlamada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValEncargadoLlamada(String value) {
        this.valEncargadoLlamada = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionProgramacionLlamada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionProgramacionLlamada() {
        return desObservacionProgramacionLlamada;
    }

    /**
     * Define el valor de la propiedad desObservacionProgramacionLlamada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionProgramacionLlamada(String value) {
        this.desObservacionProgramacionLlamada = value;
    }

}
