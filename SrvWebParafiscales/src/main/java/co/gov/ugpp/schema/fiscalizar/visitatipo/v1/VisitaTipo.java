
package co.gov.ugpp.schema.fiscalizar.visitatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;


/**
 * <p>Clase Java para VisitaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="VisitaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codResultadoVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoPersonaContactada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPlazoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecProgramadaVisita" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="valEncargadoVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionProgramacionVisita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VisitaTipo", propOrder = {
    "idVisita",
    "idExpediente",
    "codResultadoVisita",
    "descObservaciones",
    "valPersonaContactada",
    "valCargoPersonaContactada",
    "valPlazoEntrega",
    "fecProgramadaVisita",
    "valEncargadoVisita",
    "desObservacionProgramacionVisita"
})
public class VisitaTipo {

    @XmlElement(nillable = true)
    protected String idVisita;
    @XmlElement(nillable = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected String codResultadoVisita;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String valPersonaContactada;
    @XmlElement(nillable = true)
    protected String valCargoPersonaContactada;
    @XmlElement(nillable = true)
    protected String valPlazoEntrega;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecProgramadaVisita;
    @XmlElement(nillable = true)
    protected String valEncargadoVisita;
    @XmlElement(nillable = true)
    protected String desObservacionProgramacionVisita;

    /**
     * Obtiene el valor de la propiedad idVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVisita() {
        return idVisita;
    }

    /**
     * Define el valor de la propiedad idVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVisita(String value) {
        this.idVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codResultadoVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultadoVisita() {
        return codResultadoVisita;
    }

    /**
     * Define el valor de la propiedad codResultadoVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultadoVisita(String value) {
        this.codResultadoVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad valPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPersonaContactada() {
        return valPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPersonaContactada(String value) {
        this.valPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoPersonaContactada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoPersonaContactada() {
        return valCargoPersonaContactada;
    }

    /**
     * Define el valor de la propiedad valCargoPersonaContactada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoPersonaContactada(String value) {
        this.valCargoPersonaContactada = value;
    }

    /**
     * Obtiene el valor de la propiedad valPlazoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPlazoEntrega() {
        return valPlazoEntrega;
    }

    /**
     * Define el valor de la propiedad valPlazoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPlazoEntrega(String value) {
        this.valPlazoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad fecProgramadaVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecProgramadaVisita() {
        return fecProgramadaVisita;
    }

    /**
     * Define el valor de la propiedad fecProgramadaVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecProgramadaVisita(Calendar value) {
        this.fecProgramadaVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad valEncargadoVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValEncargadoVisita() {
        return valEncargadoVisita;
    }

    /**
     * Define el valor de la propiedad valEncargadoVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValEncargadoVisita(String value) {
        this.valEncargadoVisita = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionProgramacionVisita.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionProgramacionVisita() {
        return desObservacionProgramacionVisita;
    }

    /**
     * Define el valor de la propiedad desObservacionProgramacionVisita.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionProgramacionVisita(String value) {
        this.desObservacionProgramacionVisita = value;
    }

}
