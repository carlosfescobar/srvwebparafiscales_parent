
package co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para AcuseEntregaDocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AcuseEntregaDocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numGuia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecEntregaDevolucion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecEnvio" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoEntrega" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoEnvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AcuseEntregaDocumentoTipo", propOrder = {
    "numRadicadoSalida",
    "numGuia",
    "fecEntregaDevolucion",
    "fecEnvio",
    "descCausalDevolucion",
    "codEstadoEntrega",
    "codEstadoEnvio",
    "descEstadoEnvio"
})
public class AcuseEntregaDocumentoTipo {

    @XmlElement(nillable = true)
    protected String numRadicadoSalida;
    @XmlElement(nillable = true)
    protected String numGuia;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEntregaDevolucion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEnvio;
    @XmlElement(nillable = true)
    protected String descCausalDevolucion;
    @XmlElement(nillable = true)
    protected String codEstadoEntrega;
    @XmlElement(nillable = true)
    protected String codEstadoEnvio;
    @XmlElement(nillable = true)
    protected String descEstadoEnvio;

    /**
     * Obtiene el valor de la propiedad numRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumRadicadoSalida() {
        return numRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad numRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumRadicadoSalida(String value) {
        this.numRadicadoSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad numGuia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumGuia() {
        return numGuia;
    }

    /**
     * Define el valor de la propiedad numGuia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumGuia(String value) {
        this.numGuia = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEntregaDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEntregaDevolucion() {
        return fecEntregaDevolucion;
    }

    /**
     * Define el valor de la propiedad fecEntregaDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEntregaDevolucion(Calendar value) {
        this.fecEntregaDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEnvio() {
        return fecEnvio;
    }

    /**
     * Define el valor de la propiedad fecEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEnvio(Calendar value) {
        this.fecEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad descCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCausalDevolucion() {
        return descCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad descCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCausalDevolucion(String value) {
        this.descCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoEntrega.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoEntrega() {
        return codEstadoEntrega;
    }

    /**
     * Define el valor de la propiedad codEstadoEntrega.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoEntrega(String value) {
        this.codEstadoEntrega = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoEnvio() {
        return codEstadoEnvio;
    }

    /**
     * Define el valor de la propiedad codEstadoEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoEnvio(String value) {
        this.codEstadoEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoEnvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoEnvio() {
        return descEstadoEnvio;
    }

    /**
     * Define el valor de la propiedad descEstadoEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoEnvio(String value) {
        this.descEstadoEnvio = value;
    }

}
