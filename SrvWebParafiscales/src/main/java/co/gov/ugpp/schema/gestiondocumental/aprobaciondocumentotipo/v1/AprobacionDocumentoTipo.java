
package co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1.ValidacionCreacionDocumentoTipo;


/**
 * <p>Clase Java para AprobacionDocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AprobacionDocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="validacionCreacionDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ValidacionCreacionDocumentoTipo/v1}ValidacionCreacionDocumentoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AprobacionDocumentoTipo", propOrder = {
    "idArchivo",
    "validacionCreacionDocumento"
})
public class AprobacionDocumentoTipo {

    @XmlElement(nillable = true)
    protected String idArchivo;
    @XmlElement(nillable = true)
    protected List<ValidacionCreacionDocumentoTipo> validacionCreacionDocumento;

    /**
     * Obtiene el valor de la propiedad idArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivo() {
        return idArchivo;
    }

    /**
     * Define el valor de la propiedad idArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivo(String value) {
        this.idArchivo = value;
    }

    /**
     * Gets the value of the validacionCreacionDocumento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validacionCreacionDocumento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidacionCreacionDocumento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidacionCreacionDocumentoTipo }
     * 
     * 
     */
    public List<ValidacionCreacionDocumentoTipo> getValidacionCreacionDocumento() {
        if (validacionCreacionDocumento == null) {
            validacionCreacionDocumento = new ArrayList<ValidacionCreacionDocumentoTipo>();
        }
        return this.validacionCreacionDocumento;
    }

}
