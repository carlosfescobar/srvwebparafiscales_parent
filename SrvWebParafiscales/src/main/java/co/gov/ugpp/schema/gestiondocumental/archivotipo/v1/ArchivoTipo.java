
package co.gov.ugpp.schema.gestiondocumental.archivotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ArchivoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ArchivoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valNombreArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valContenidoArchivo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="valContenidoFirma" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="codTipoMIMEArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArchivoTipo", propOrder = {
    "valNombreArchivo",
    "idArchivo",
    "valContenidoArchivo",
    "valContenidoFirma",
    "codTipoMIMEArchivo"
})
public class ArchivoTipo {

    @XmlElement(nillable = true)
    protected String valNombreArchivo;
    @XmlElement(nillable = true)
    protected String idArchivo;
    @XmlElement(nillable = true)
    protected byte[] valContenidoArchivo;
    @XmlElement(nillable = true)
    protected byte[] valContenidoFirma;
    @XmlElement(nillable = true)
    protected String codTipoMIMEArchivo;

    /**
     * Obtiene el valor de la propiedad valNombreArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreArchivo() {
        return valNombreArchivo;
    }

    /**
     * Define el valor de la propiedad valNombreArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreArchivo(String value) {
        this.valNombreArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivo() {
        return idArchivo;
    }

    /**
     * Define el valor de la propiedad idArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivo(String value) {
        this.idArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad valContenidoArchivo.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getValContenidoArchivo() {
        return valContenidoArchivo;
    }

    /**
     * Define el valor de la propiedad valContenidoArchivo.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setValContenidoArchivo(byte[] value) {
        this.valContenidoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad valContenidoFirma.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getValContenidoFirma() {
        return valContenidoFirma;
    }

    /**
     * Define el valor de la propiedad valContenidoFirma.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setValContenidoFirma(byte[] value) {
        this.valContenidoFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoMIMEArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoMIMEArchivo() {
        return codTipoMIMEArchivo;
    }

    /**
     * Define el valor de la propiedad codTipoMIMEArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoMIMEArchivo(String value) {
        this.codTipoMIMEArchivo = value;
    }

}
