
package co.gov.ugpp.schema.gestiondocumental.documentotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.MetadataDocumentoTipo;


/**
 * <p>Clase Java para DocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="valNombreDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *         &lt;element name="fecDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPaginas" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="codTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valAutorOriginador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoCorrespondencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicacionCorrespondencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNaturalezaDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valOrigenDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionLegibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esMover" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombreTipoDocumental" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numFolios" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="valLegible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valTipoFirma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="metadataDocumento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/MetadataDocumentoTipo/v1}MetadataDocumentoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoTipo", propOrder = {
    "idDocumento",
    "valNombreDocumento",
    "docDocumento",
    "fecDocumento",
    "valPaginas",
    "codTipoDocumento",
    "valAutorOriginador",
    "idRadicadoCorrespondencia",
    "fecRadicacionCorrespondencia",
    "valNaturalezaDocumento",
    "valOrigenDocumento",
    "descObservacionLegibilidad",
    "esMover",
    "valNombreTipoDocumental",
    "numFolios",
    "valLegible",
    "valTipoFirma",
    "metadataDocumento"
})
public class DocumentoTipo {

    @XmlElement(required = true)
    protected String idDocumento;
    @XmlElement(nillable = true)
    protected String valNombreDocumento;
    @XmlElement(nillable = true)
    protected ArchivoTipo docDocumento;
    @XmlElement(nillable = true)
    protected String fecDocumento;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long valPaginas;
    @XmlElement(nillable = true)
    protected String codTipoDocumento;
    @XmlElement(nillable = true)
    protected String valAutorOriginador;
    @XmlElement(nillable = true)
    protected String idRadicadoCorrespondencia;
    @XmlElement(nillable = true)
    protected String fecRadicacionCorrespondencia;
    @XmlElement(nillable = true)
    protected String valNaturalezaDocumento;
    @XmlElement(nillable = true)
    protected String valOrigenDocumento;
    @XmlElement(nillable = true)
    protected String descObservacionLegibilidad;
    @XmlElement(nillable = true)
    protected String esMover;
    @XmlElement(nillable = true)
    protected String valNombreTipoDocumental;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long numFolios;
    @XmlElement(nillable = true)
    protected String valLegible;
    @XmlElement(nillable = true)
    protected String valTipoFirma;
    @XmlElement(nillable = true)
    protected MetadataDocumentoTipo metadataDocumento;

    /**
     * Obtiene el valor de la propiedad idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Define el valor de la propiedad idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreDocumento() {
        return valNombreDocumento;
    }

    /**
     * Define el valor de la propiedad valNombreDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreDocumento(String value) {
        this.valNombreDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad docDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getDocDocumento() {
        return docDocumento;
    }

    /**
     * Define el valor de la propiedad docDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setDocDocumento(ArchivoTipo value) {
        this.docDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad fecDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecDocumento() {
        return fecDocumento;
    }

    /**
     * Define el valor de la propiedad fecDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecDocumento(String value) {
        this.fecDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad valPaginas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getValPaginas() {
        return valPaginas;
    }

    /**
     * Define el valor de la propiedad valPaginas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPaginas(Long value) {
        this.valPaginas = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    /**
     * Define el valor de la propiedad codTipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDocumento(String value) {
        this.codTipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad valAutorOriginador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValAutorOriginador() {
        return valAutorOriginador;
    }

    /**
     * Define el valor de la propiedad valAutorOriginador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValAutorOriginador(String value) {
        this.valAutorOriginador = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoCorrespondencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoCorrespondencia() {
        return idRadicadoCorrespondencia;
    }

    /**
     * Define el valor de la propiedad idRadicadoCorrespondencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoCorrespondencia(String value) {
        this.idRadicadoCorrespondencia = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicacionCorrespondencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecRadicacionCorrespondencia() {
        return fecRadicacionCorrespondencia;
    }

    /**
     * Define el valor de la propiedad fecRadicacionCorrespondencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicacionCorrespondencia(String value) {
        this.fecRadicacionCorrespondencia = value;
    }

    /**
     * Obtiene el valor de la propiedad valNaturalezaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNaturalezaDocumento() {
        return valNaturalezaDocumento;
    }

    /**
     * Define el valor de la propiedad valNaturalezaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNaturalezaDocumento(String value) {
        this.valNaturalezaDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad valOrigenDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValOrigenDocumento() {
        return valOrigenDocumento;
    }

    /**
     * Define el valor de la propiedad valOrigenDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValOrigenDocumento(String value) {
        this.valOrigenDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionLegibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionLegibilidad() {
        return descObservacionLegibilidad;
    }

    /**
     * Define el valor de la propiedad descObservacionLegibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionLegibilidad(String value) {
        this.descObservacionLegibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad esMover.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsMover() {
        return esMover;
    }

    /**
     * Define el valor de la propiedad esMover.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsMover(String value) {
        this.esMover = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreTipoDocumental.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreTipoDocumental() {
        return valNombreTipoDocumental;
    }

    /**
     * Define el valor de la propiedad valNombreTipoDocumental.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreTipoDocumental(String value) {
        this.valNombreTipoDocumental = value;
    }

    /**
     * Obtiene el valor de la propiedad numFolios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getNumFolios() {
        return numFolios;
    }

    /**
     * Define el valor de la propiedad numFolios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumFolios(Long value) {
        this.numFolios = value;
    }

    /**
     * Obtiene el valor de la propiedad valLegible.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLegible() {
        return valLegible;
    }

    /**
     * Define el valor de la propiedad valLegible.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLegible(String value) {
        this.valLegible = value;
    }

    /**
     * Obtiene el valor de la propiedad valTipoFirma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValTipoFirma() {
        return valTipoFirma;
    }

    /**
     * Define el valor de la propiedad valTipoFirma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValTipoFirma(String value) {
        this.valTipoFirma = value;
    }

    /**
     * Obtiene el valor de la propiedad metadataDocumento.
     * 
     * @return
     *     possible object is
     *     {@link MetadataDocumentoTipo }
     *     
     */
    public MetadataDocumentoTipo getMetadataDocumento() {
        return metadataDocumento;
    }

    /**
     * Define el valor de la propiedad metadataDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link MetadataDocumentoTipo }
     *     
     */
    public void setMetadataDocumento(MetadataDocumentoTipo value) {
        this.metadataDocumento = value;
    }

}
