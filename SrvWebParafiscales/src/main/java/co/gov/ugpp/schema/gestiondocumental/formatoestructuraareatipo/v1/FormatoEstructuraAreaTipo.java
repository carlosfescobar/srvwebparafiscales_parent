
package co.gov.ugpp.schema.gestiondocumental.formatoestructuraareatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;


/**
 * <p>Clase Java para FormatoEstructuraAreaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FormatoEstructuraAreaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formato" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="archivo" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *         &lt;element name="codAreaNegocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAreaNegocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecFinVigencia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormatoEstructuraAreaTipo", propOrder = {
    "formato",
    "archivo",
    "codAreaNegocio",
    "descAreaNegocio",
    "valUsuario",
    "descDescripcion",
    "fecInicioVigencia",
    "fecFinVigencia"
})
public class FormatoEstructuraAreaTipo {

    @XmlElement(nillable = true)
    protected FormatoTipo formato;
    @XmlElement(nillable = true)
    protected ArchivoTipo archivo;
    @XmlElement(nillable = true)
    protected String codAreaNegocio;
    @XmlElement(nillable = true)
    protected String descAreaNegocio;
    @XmlElement(nillable = true)
    protected String valUsuario;
    @XmlElement(nillable = true)
    protected String descDescripcion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecInicioVigencia;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecFinVigencia;

    /**
     * Obtiene el valor de la propiedad formato.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormato() {
        return formato;
    }

    /**
     * Define el valor de la propiedad formato.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormato(FormatoTipo value) {
        this.formato = value;
    }

    /**
     * Obtiene el valor de la propiedad archivo.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getArchivo() {
        return archivo;
    }

    /**
     * Define el valor de la propiedad archivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setArchivo(ArchivoTipo value) {
        this.archivo = value;
    }

    /**
     * Obtiene el valor de la propiedad codAreaNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAreaNegocio() {
        return codAreaNegocio;
    }

    /**
     * Define el valor de la propiedad codAreaNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAreaNegocio(String value) {
        this.codAreaNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad descAreaNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAreaNegocio() {
        return descAreaNegocio;
    }

    /**
     * Define el valor de la propiedad descAreaNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAreaNegocio(String value) {
        this.descAreaNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuario() {
        return valUsuario;
    }

    /**
     * Define el valor de la propiedad valUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuario(String value) {
        this.valUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad descDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDescripcion() {
        return descDescripcion;
    }

    /**
     * Define el valor de la propiedad descDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDescripcion(String value) {
        this.descDescripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioVigencia() {
        return fecInicioVigencia;
    }

    /**
     * Define el valor de la propiedad fecInicioVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioVigencia(Calendar value) {
        this.fecInicioVigencia = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinVigencia() {
        return fecFinVigencia;
    }

    /**
     * Define el valor de la propiedad fecFinVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinVigencia(Calendar value) {
        this.fecFinVigencia = value;
    }

}
