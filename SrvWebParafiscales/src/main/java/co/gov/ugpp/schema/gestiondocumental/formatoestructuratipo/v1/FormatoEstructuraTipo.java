
package co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;


/**
 * <p>Clase Java para FormatoEstructuraTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FormatoEstructuraTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formato" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="archivo" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FormatoEstructuraTipo", propOrder = {
    "formato",
    "archivo"
})
public class FormatoEstructuraTipo {

    @XmlElement(nillable = true)
    protected FormatoTipo formato;
    @XmlElement(nillable = true)
    protected ArchivoTipo archivo;

    /**
     * Obtiene el valor de la propiedad formato.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormato() {
        return formato;
    }

    /**
     * Define el valor de la propiedad formato.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormato(FormatoTipo value) {
        this.formato = value;
    }

    /**
     * Obtiene el valor de la propiedad archivo.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getArchivo() {
        return archivo;
    }

    /**
     * Define el valor de la propiedad archivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setArchivo(ArchivoTipo value) {
        this.archivo = value;
    }

}
