
package co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.SerieDocumentalTipo;


/**
 * <p>Clase Java para MetadataDocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MetadataDocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="serieDocumental" type="{http://www.ugpp.gov.co/schema/GestionDocumental/SerieDocumentalTipo/v1}SerieDocumentalTipo" minOccurs="0"/>
 *         &lt;element name="valAgrupador" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadataDocumentoTipo", propOrder = {
    "serieDocumental",
    "valAgrupador"
})
public class MetadataDocumentoTipo {

    @XmlElement(nillable = true)
    protected SerieDocumentalTipo serieDocumental;
    @XmlElement(nillable = true)
    protected List<String> valAgrupador;

    /**
     * Obtiene el valor de la propiedad serieDocumental.
     * 
     * @return
     *     possible object is
     *     {@link SerieDocumentalTipo }
     *     
     */
    public SerieDocumentalTipo getSerieDocumental() {
        return serieDocumental;
    }

    /**
     * Define el valor de la propiedad serieDocumental.
     * 
     * @param value
     *     allowed object is
     *     {@link SerieDocumentalTipo }
     *     
     */
    public void setSerieDocumental(SerieDocumentalTipo value) {
        this.serieDocumental = value;
    }

    /**
     * Gets the value of the valAgrupador property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valAgrupador property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValAgrupador().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValAgrupador() {
        if (valAgrupador == null) {
            valAgrupador = new ArrayList<String>();
        }
        return this.valAgrupador;
    }

}
