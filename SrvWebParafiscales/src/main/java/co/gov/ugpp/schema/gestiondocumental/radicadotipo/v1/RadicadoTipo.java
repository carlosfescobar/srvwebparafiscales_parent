
package co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para RadicadoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RadicadoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRadicado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicado" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codRadicado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RadicadoTipo", propOrder = {
    "idRadicado",
    "fecRadicado",
    "codRadicado"
})
public class RadicadoTipo {

    @XmlElement(nillable = true)
    protected String idRadicado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicado;
    @XmlElement(nillable = true)
    protected String codRadicado;

    /**
     * Obtiene el valor de la propiedad idRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicado() {
        return idRadicado;
    }

    /**
     * Define el valor de la propiedad idRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicado(String value) {
        this.idRadicado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicado() {
        return fecRadicado;
    }

    /**
     * Define el valor de la propiedad fecRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicado(Calendar value) {
        this.fecRadicado = value;
    }

    /**
     * Obtiene el valor de la propiedad codRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRadicado() {
        return codRadicado;
    }

    /**
     * Define el valor de la propiedad codRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRadicado(String value) {
        this.codRadicado = value;
    }

}
