
package co.gov.ugpp.schema.gestiondocumental.validacioncreaciondocumentotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ValidacionCreacionDocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionCreacionDocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idValidacionCreacionDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAprobado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaValidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valComentarios" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionCreacionDocumentoTipo", propOrder = {
    "idValidacionCreacionDocumento",
    "esAprobado",
    "fechaValidacion",
    "valComentarios"
})
public class ValidacionCreacionDocumentoTipo {

    @XmlElement(nillable = true)
    protected String idValidacionCreacionDocumento;
    @XmlElement(nillable = true)
    protected String esAprobado;
    @XmlElement(nillable = true)
    protected String fechaValidacion;
    @XmlElement(nillable = true)
    protected String valComentarios;

    /**
     * Obtiene el valor de la propiedad idValidacionCreacionDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdValidacionCreacionDocumento() {
        return idValidacionCreacionDocumento;
    }

    /**
     * Define el valor de la propiedad idValidacionCreacionDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdValidacionCreacionDocumento(String value) {
        this.idValidacionCreacionDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad esAprobado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAprobado() {
        return esAprobado;
    }

    /**
     * Define el valor de la propiedad esAprobado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAprobado(String value) {
        this.esAprobado = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaValidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaValidacion() {
        return fechaValidacion;
    }

    /**
     * Define el valor de la propiedad fechaValidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaValidacion(String value) {
        this.fechaValidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valComentarios.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValComentarios() {
        return valComentarios;
    }

    /**
     * Define el valor de la propiedad valComentarios.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValComentarios(String value) {
        this.valComentarios = value;
    }

}
