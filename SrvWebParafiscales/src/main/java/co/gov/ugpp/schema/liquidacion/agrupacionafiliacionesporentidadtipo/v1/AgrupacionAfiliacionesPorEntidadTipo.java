
package co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;


/**
 * <p>Clase Java para AgrupacionAfiliacionesPorEntidadTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgrupacionAfiliacionesPorEntidadTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAgrupacionAfiliacionesPorEntidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="personasPorAfiliar" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo"/>
 *         &lt;element name="idDocumentoOficioAfiliaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oficioAfiliacionesParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgrupacionAfiliacionesPorEntidadTipo", propOrder = {
    "idAgrupacionAfiliacionesPorEntidad",
    "personasPorAfiliar",
    "entidadExterna",
    "idDocumentoOficioAfiliaciones",
    "oficioAfiliacionesParcial"
})
public class AgrupacionAfiliacionesPorEntidadTipo {

    @XmlElement(nillable = true)
    protected String idAgrupacionAfiliacionesPorEntidad;
    @XmlElement(nillable = true)
    protected List<PersonaNaturalTipo> personasPorAfiliar;
    @XmlElement(required = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected String idDocumentoOficioAfiliaciones;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo oficioAfiliacionesParcial;

    /**
     * Obtiene el valor de la propiedad idAgrupacionAfiliacionesPorEntidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAgrupacionAfiliacionesPorEntidad() {
        return idAgrupacionAfiliacionesPorEntidad;
    }

    /**
     * Define el valor de la propiedad idAgrupacionAfiliacionesPorEntidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAgrupacionAfiliacionesPorEntidad(String value) {
        this.idAgrupacionAfiliacionesPorEntidad = value;
    }

    /**
     * Gets the value of the personasPorAfiliar property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the personasPorAfiliar property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPersonasPorAfiliar().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PersonaNaturalTipo }
     * 
     * 
     */
    public List<PersonaNaturalTipo> getPersonasPorAfiliar() {
        if (personasPorAfiliar == null) {
            personasPorAfiliar = new ArrayList<PersonaNaturalTipo>();
        }
        return this.personasPorAfiliar;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoOficioAfiliaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoOficioAfiliaciones() {
        return idDocumentoOficioAfiliaciones;
    }

    /**
     * Define el valor de la propiedad idDocumentoOficioAfiliaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoOficioAfiliaciones(String value) {
        this.idDocumentoOficioAfiliaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad oficioAfiliacionesParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getOficioAfiliacionesParcial() {
        return oficioAfiliacionesParcial;
    }

    /**
     * Define el valor de la propiedad oficioAfiliacionesParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setOficioAfiliacionesParcial(AprobacionDocumentoTipo value) {
        this.oficioAfiliacionesParcial = value;
    }

}
