
package co.gov.ugpp.schema.liquidacion.liquidaciontipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.liquidacion.agrupacionafiliacionesporentidadtipo.v1.AgrupacionAfiliacionesPorEntidadTipo;


/**
 * <p>Clase Java para LiquidacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LiquidacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="esAmpliadoRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesAmpliacionRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="periodoFiscalizacion" type="{http://www.ugpp.gov.co/esb/schema/RangoFechaTipo/v1}RangoFechaTipo" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoDeclararCorregirAmpliado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requerimientoDeclararAmpliadoParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="esDeterminaDeuda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLiquidadorLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLiquidadorLiquidacionAmpliada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoArchivoParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoAutoArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="liquidacionOficialParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoLiquidacionOficial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esRecibidoRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esConfirmadaDeuda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoConstanciaEjecutoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoEnvioMemorandoCobro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="liquidador" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="subdirectorCobranzas" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="esTodosEmpleadosAfiliados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesAfiliaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="agrupacionAfiliacionesPorEntidad" type="{http://www.ugpp.gov.co/schema/Liquidacion/AgrupacionAfiliacionesPorEntidadTipo/v1}AgrupacionAfiliacionesPorEntidadTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codEstadoLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalCierre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuarioGeneraActo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LiquidacionTipo", propOrder = {
    "idLiquidacion",
    "expediente",
    "aportante",
    "esAmpliadoRequerimiento",
    "descObservacionesAmpliacionRequerimiento",
    "idFiscalizacion",
    "periodoFiscalizacion",
    "idActoRequerimientoDeclararCorregirAmpliado",
    "requerimientoDeclararAmpliadoParcial",
    "esDeterminaDeuda",
    "idLiquidadorLiquidacion",
    "idLiquidadorLiquidacionAmpliada",
    "autoArchivoParcial",
    "idActoAutoArchivo",
    "liquidacionOficialParcial",
    "idActoLiquidacionOficial",
    "esRecibidoRecursoReconsideracion",
    "esConfirmadaDeuda",
    "idDocumentoConstanciaEjecutoria",
    "codEstadoEnvioMemorandoCobro",
    "liquidador",
    "subdirectorCobranzas",
    "esTodosEmpleadosAfiliados",
    "descObservacionesAfiliaciones",
    "agrupacionAfiliacionesPorEntidad",
    "acciones",
    "codEstadoLiquidacion",
    "descEstadoLiquidacion",
    "codCausalCierre",
    "valUsuarioGeneraActo"
})
public class LiquidacionTipo {

    @XmlElement(nillable = true)
    protected String idLiquidacion;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected String esAmpliadoRequerimiento;
    @XmlElement(nillable = true)
    protected String descObservacionesAmpliacionRequerimiento;
    @XmlElement(nillable = true)
    protected String idFiscalizacion;
    protected RangoFechaTipo periodoFiscalizacion;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoDeclararCorregirAmpliado;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo requerimientoDeclararAmpliadoParcial;
    @XmlElement(nillable = true)
    protected String esDeterminaDeuda;
    @XmlElement(nillable = true)
    protected String idLiquidadorLiquidacion;
    @XmlElement(nillable = true)
    protected String idLiquidadorLiquidacionAmpliada;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoArchivoParcial;
    @XmlElement(nillable = true)
    protected String idActoAutoArchivo;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo liquidacionOficialParcial;
    @XmlElement(nillable = true)
    protected String idActoLiquidacionOficial;
    @XmlElement(nillable = true)
    protected String esRecibidoRecursoReconsideracion;
    @XmlElement(nillable = true)
    protected String esConfirmadaDeuda;
    @XmlElement(nillable = true)
    protected String idDocumentoConstanciaEjecutoria;
    @XmlElement(nillable = true)
    protected String codEstadoEnvioMemorandoCobro;
    @XmlElement(nillable = true)
    protected FuncionarioTipo liquidador;
    @XmlElement(nillable = true)
    protected FuncionarioTipo subdirectorCobranzas;
    @XmlElement(nillable = true)
    protected String esTodosEmpleadosAfiliados;
    @XmlElement(nillable = true)
    protected String descObservacionesAfiliaciones;
    @XmlElement(nillable = true)
    protected List<AgrupacionAfiliacionesPorEntidadTipo> agrupacionAfiliacionesPorEntidad;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(nillable = true)
    protected String codEstadoLiquidacion;
    @XmlElement(nillable = true)
    protected String descEstadoLiquidacion;
    @XmlElement(nillable = true)
    protected String codCausalCierre;
    @XmlElement(nillable = true)
    protected String valUsuarioGeneraActo;

    /**
     * Obtiene el valor de la propiedad idLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidacion() {
        return idLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidacion(String value) {
        this.idLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad esAmpliadoRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAmpliadoRequerimiento() {
        return esAmpliadoRequerimiento;
    }

    /**
     * Define el valor de la propiedad esAmpliadoRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAmpliadoRequerimiento(String value) {
        this.esAmpliadoRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesAmpliacionRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesAmpliacionRequerimiento() {
        return descObservacionesAmpliacionRequerimiento;
    }

    /**
     * Define el valor de la propiedad descObservacionesAmpliacionRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesAmpliacionRequerimiento(String value) {
        this.descObservacionesAmpliacionRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad idFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFiscalizacion() {
        return idFiscalizacion;
    }

    /**
     * Define el valor de la propiedad idFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFiscalizacion(String value) {
        this.idFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad periodoFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link RangoFechaTipo }
     *     
     */
    public RangoFechaTipo getPeriodoFiscalizacion() {
        return periodoFiscalizacion;
    }

    /**
     * Define el valor de la propiedad periodoFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoFechaTipo }
     *     
     */
    public void setPeriodoFiscalizacion(RangoFechaTipo value) {
        this.periodoFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoDeclararCorregirAmpliado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoDeclararCorregirAmpliado() {
        return idActoRequerimientoDeclararCorregirAmpliado;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoDeclararCorregirAmpliado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoDeclararCorregirAmpliado(String value) {
        this.idActoRequerimientoDeclararCorregirAmpliado = value;
    }

    /**
     * Obtiene el valor de la propiedad requerimientoDeclararAmpliadoParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getRequerimientoDeclararAmpliadoParcial() {
        return requerimientoDeclararAmpliadoParcial;
    }

    /**
     * Define el valor de la propiedad requerimientoDeclararAmpliadoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setRequerimientoDeclararAmpliadoParcial(AprobacionDocumentoTipo value) {
        this.requerimientoDeclararAmpliadoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad esDeterminaDeuda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDeterminaDeuda() {
        return esDeterminaDeuda;
    }

    /**
     * Define el valor de la propiedad esDeterminaDeuda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDeterminaDeuda(String value) {
        this.esDeterminaDeuda = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidadorLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidadorLiquidacion() {
        return idLiquidadorLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidadorLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidadorLiquidacion(String value) {
        this.idLiquidadorLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidadorLiquidacionAmpliada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidadorLiquidacionAmpliada() {
        return idLiquidadorLiquidacionAmpliada;
    }

    /**
     * Define el valor de la propiedad idLiquidadorLiquidacionAmpliada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidadorLiquidacionAmpliada(String value) {
        this.idLiquidadorLiquidacionAmpliada = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivoParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    /**
     * Define el valor de la propiedad autoArchivoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoArchivoParcial(AprobacionDocumentoTipo value) {
        this.autoArchivoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAutoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAutoArchivo() {
        return idActoAutoArchivo;
    }

    /**
     * Define el valor de la propiedad idActoAutoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAutoArchivo(String value) {
        this.idActoAutoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidacionOficialParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getLiquidacionOficialParcial() {
        return liquidacionOficialParcial;
    }

    /**
     * Define el valor de la propiedad liquidacionOficialParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setLiquidacionOficialParcial(AprobacionDocumentoTipo value) {
        this.liquidacionOficialParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoLiquidacionOficial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoLiquidacionOficial() {
        return idActoLiquidacionOficial;
    }

    /**
     * Define el valor de la propiedad idActoLiquidacionOficial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoLiquidacionOficial(String value) {
        this.idActoLiquidacionOficial = value;
    }

    /**
     * Obtiene el valor de la propiedad esRecibidoRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsRecibidoRecursoReconsideracion() {
        return esRecibidoRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad esRecibidoRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsRecibidoRecursoReconsideracion(String value) {
        this.esRecibidoRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad esConfirmadaDeuda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConfirmadaDeuda() {
        return esConfirmadaDeuda;
    }

    /**
     * Define el valor de la propiedad esConfirmadaDeuda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConfirmadaDeuda(String value) {
        this.esConfirmadaDeuda = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoConstanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoConstanciaEjecutoria() {
        return idDocumentoConstanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad idDocumentoConstanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoConstanciaEjecutoria(String value) {
        this.idDocumentoConstanciaEjecutoria = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoEnvioMemorandoCobro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoEnvioMemorandoCobro() {
        return codEstadoEnvioMemorandoCobro;
    }

    /**
     * Define el valor de la propiedad codEstadoEnvioMemorandoCobro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoEnvioMemorandoCobro(String value) {
        this.codEstadoEnvioMemorandoCobro = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidador.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getLiquidador() {
        return liquidador;
    }

    /**
     * Define el valor de la propiedad liquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setLiquidador(FuncionarioTipo value) {
        this.liquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad subdirectorCobranzas.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getSubdirectorCobranzas() {
        return subdirectorCobranzas;
    }

    /**
     * Define el valor de la propiedad subdirectorCobranzas.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setSubdirectorCobranzas(FuncionarioTipo value) {
        this.subdirectorCobranzas = value;
    }

    /**
     * Obtiene el valor de la propiedad esTodosEmpleadosAfiliados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTodosEmpleadosAfiliados() {
        return esTodosEmpleadosAfiliados;
    }

    /**
     * Define el valor de la propiedad esTodosEmpleadosAfiliados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTodosEmpleadosAfiliados(String value) {
        this.esTodosEmpleadosAfiliados = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesAfiliaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesAfiliaciones() {
        return descObservacionesAfiliaciones;
    }

    /**
     * Define el valor de la propiedad descObservacionesAfiliaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesAfiliaciones(String value) {
        this.descObservacionesAfiliaciones = value;
    }

    /**
     * Gets the value of the agrupacionAfiliacionesPorEntidad property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the agrupacionAfiliacionesPorEntidad property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAgrupacionAfiliacionesPorEntidad().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AgrupacionAfiliacionesPorEntidadTipo }
     * 
     * 
     */
    public List<AgrupacionAfiliacionesPorEntidadTipo> getAgrupacionAfiliacionesPorEntidad() {
        if (agrupacionAfiliacionesPorEntidad == null) {
            agrupacionAfiliacionesPorEntidad = new ArrayList<AgrupacionAfiliacionesPorEntidadTipo>();
        }
        return this.agrupacionAfiliacionesPorEntidad;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoLiquidacion() {
        return codEstadoLiquidacion;
    }

    /**
     * Define el valor de la propiedad codEstadoLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoLiquidacion(String value) {
        this.codEstadoLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoLiquidacion() {
        return descEstadoLiquidacion;
    }

    /**
     * Define el valor de la propiedad descEstadoLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoLiquidacion(String value) {
        this.descEstadoLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalCierre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalCierre() {
        return codCausalCierre;
    }

    /**
     * Define el valor de la propiedad codCausalCierre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalCierre(String value) {
        this.codCausalCierre = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuarioGeneraActo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuarioGeneraActo() {
        return valUsuarioGeneraActo;
    }

    /**
     * Define el valor de la propiedad valUsuarioGeneraActo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuarioGeneraActo(String value) {
        this.valUsuarioGeneraActo = value;
    }

}
