
package co.gov.ugpp.schema.liquidaciones.conceptocontabletipo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ConceptoContableTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConceptoContableTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idConceptoContable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idHojaCalculoLiquidacionDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}codigo" minOccurs="0"/>
 *         &lt;element name="nombre" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre" minOccurs="0"/>
 *         &lt;element name="subsistemas" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}subsistemas" minOccurs="0"/>
 *         &lt;element name="cuenta" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}cuenta" minOccurs="0"/>
 *         &lt;element name="nombre_concepto" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}nombre_concepto" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="estado" type="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}estado" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConceptoContableTipo", propOrder = {
    "idConceptoContable",
    "idHojaCalculoLiquidacionDetalle",
    "codigo",
    "nombre",
    "subsistemas",
    "cuenta",
    "nombreConcepto",
    "valor",
    "estado"
})
public class ConceptoContableTipo {

    @XmlElement(nillable = true)
    protected String idConceptoContable;
    @XmlElement(nillable = true)
    protected String idHojaCalculoLiquidacionDetalle;
    @XmlElement(nillable = true)
    protected String codigo;
    @XmlElement(nillable = true)
    protected String nombre;
    @XmlElement(nillable = true)
    protected String subsistemas;
    @XmlElement(nillable = true)
    protected String cuenta;
    @XmlElement(name = "nombre_concepto", nillable = true)
    protected String nombreConcepto;
    @XmlElement(nillable = true)
    protected BigDecimal valor;
    protected String estado;

    /**
     * Obtiene el valor de la propiedad idConceptoContable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConceptoContable() {
        return idConceptoContable;
    }

    /**
     * Define el valor de la propiedad idConceptoContable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConceptoContable(String value) {
        this.idConceptoContable = value;
    }

    /**
     * Obtiene el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculoLiquidacionDetalle() {
        return idHojaCalculoLiquidacionDetalle;
    }

    /**
     * Define el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculoLiquidacionDetalle(String value) {
        this.idHojaCalculoLiquidacionDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad nombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Define el valor de la propiedad nombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombre(String value) {
        this.nombre = value;
    }

    /**
     * Obtiene el valor de la propiedad subsistemas.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubsistemas() {
        return subsistemas;
    }

    /**
     * Define el valor de la propiedad subsistemas.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubsistemas(String value) {
        this.subsistemas = value;
    }

    /**
     * Obtiene el valor de la propiedad cuenta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * Define el valor de la propiedad cuenta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCuenta(String value) {
        this.cuenta = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreConcepto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreConcepto() {
        return nombreConcepto;
    }

    /**
     * Define el valor de la propiedad nombreConcepto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreConcepto(String value) {
        this.nombreConcepto = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValor(BigDecimal value) {
        this.valor = value;
    }

    /**
     * Obtiene el valor de la propiedad estado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Define el valor de la propiedad estado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstado(String value) {
        this.estado = value;
    }

}
