
package co.gov.ugpp.schema.liquidaciones.detallehojacalculotipo.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.parafiscales.util.adapter.Adapter2;
import co.gov.ugpp.schema.liquidaciones.aportesindependientetipo.v1.AportesIndependienteTipo;
import co.gov.ugpp.schema.liquidaciones.conceptocontabletipo.v1.ConceptoContableTipo;


/**
 * <p>Clase Java para DetalleHojaCalculoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DetalleHojaCalculoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHojaCalculoLiquidacionDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNominaDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPilaDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idConciliacionContableDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="APORTA_id" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="APORTA_Tipo_Identificacion" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="APORTA_Numero_Identificacion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="APORTA_Primer_Nombre" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="APORTA_Clase" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="250"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="APORTA_Aporte_Esap_y_Men" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="APORTA_Excepcion_Ley_1233_2008" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZ_id" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Tipo_Documento" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Numero_Identificacion" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZ_Nombre" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZ_Tipo_Cotizante" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Subtipo_Cotizante" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZ_Extranjero_No_Cotizar" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZ_Colombiano_En_El_Ext" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZ_Actividad_Alto_Riesgo_Pe" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_ano" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_mes" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ing" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_ing_fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_ret" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_ret_fecha" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_sln" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_causal_suspension" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_causal_susp_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_causal_susp_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_suspension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ige" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_ige_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_ige_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_incapacidad_general" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_lma" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_lma_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_lma_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_licencia_maternidad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_vac" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_vac_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_vac_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_vacaciones" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_irp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="COTIZD_irp_fInicial" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="COTIZD_irp_fFinal" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="dias_incapacidad_rp" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_salario_integral" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="dias_laborados" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="COTIZD_ultimo_ibc_sin_novedad" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ing_pila_AT" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ret_pila_AU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="sln_pila_AV" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ige_pila_AU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="lma_pila_AU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="vac_pila_AU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="irp_pila_AU" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="porcentaje_pagos_no_salaria_BB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="excede_limite_pago_no_salar_BC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo_administradora_BD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nombre_corto_administradora_BE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_BF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_BG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_BH" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_BI" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_BJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_BK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_BL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_BM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_BN" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_BO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="codigo_administradora_BP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nombre_corto_administradora_BQ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_BR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_pension_BS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligato_a_pensi_BT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_fsp_BU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_fsp_BV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_pension_adic_ar_BW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obli_pen_adic_ar_BX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_BY" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_BZ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pension_CA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pension_CB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_pension_CC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_pension_CD" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tarifa_pila_fsp_CE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_fsp_CF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_fsp_CG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_fsp_CH" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tarifa_pila_pensi_adicio_ar_CI" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pag_pens_adic_ar_CJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_pension_adicional_ar_CK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_pension_ar_CL" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_CM" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="calculo_actuarial_CN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="codigo_administradora_CO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nombre_corto_administradora_CP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_CQ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_CR" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_CS" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_CT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_CU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_CV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_CW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_CX" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_CY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="nombre_corto_administradora_CZ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_DA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_obligatoria_DC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cotizados_pila_DD" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_DE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotizacion_pagada_pila_DG" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DH" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DI" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_DJ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_DK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_DM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_DO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DP" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DQ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_DR" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_DS" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_DT" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_DU" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_DV" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_DW" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_DX" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_DY" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_DZ" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_EA" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_EB" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EC" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_ED" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_pila_EE" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_EF" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="concepto_ajuste_EG" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_EH" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ibc_core_EI" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_core_EJ" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EK" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_EL" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="aporte_EM" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ajuste_EN" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="dias_cot_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cot_rprof" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cot_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cot_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_coti_pila_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="planilla_pila_cargada" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cotiz_pila_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cotiz_pila_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="dias_cotiz_pila_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_permisos_remunerados" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_susp_permisos" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_vacaciones" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_huelga" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="pago_no_salarial" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="total_remunerado" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="por_pago_no_salarial" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="exc_lim_pago_no_salarial" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pagos_nom_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_calculado_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_salud" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="tarifa_salud_suspension" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_calculada_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_salud" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_pagada_pila_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_salud" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pagos_nom_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_calculado_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pension" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_pension" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cot_pagada_pila_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_pension" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_fsp_subcuen_solidaridad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_fsp_subcuen_subsisten" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_fsp_sub_solidaridad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_fsp_sub_subsistencia" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="cotiz_pag_pila_fsp_sub_solidar" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="cotiz_pag_pila_fsp_sub_subsis" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_fsp_subcuen_subsisten" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_fsp_subcuen_solidaridad" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="calculo_actuarial" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_arl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_arl" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_pagada_pila_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_arl" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_ccf" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_pila_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_ccf" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_pagada_pila_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_ccf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_sena" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_sena" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_sena" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_sena" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_pagada_pila_sena" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_sena" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ibc_icbf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_icbf" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_obl_icbf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="tarifa_pila_icbf" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="cotiz_pagada_pila_icbf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="ajuste_icbf" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/>
 *         &lt;element name="cod_adm_salud" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cod_adm_arl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cod_adm_ccf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_salud" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_pension" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_pension" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_fsp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_fsp" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_arl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_salud" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_arl" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_ccf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_ccf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_sena" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_sena" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_icbf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_icbf" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="concepto_ajuste_EO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="tipo_incumplimiento_EP" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ConceptoContableTipo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/ConceptoContableTipo/v1}ConceptoContableTipo" maxOccurs="unbounded"/>
 *         &lt;element name="AportesIndependienteTipo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/AportesIndependienteTipo/v1}AportesIndependienteTipo" maxOccurs="unbounded"/>
 *         &lt;element name="ContextoRespuestaTipo" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DetalleHojaCalculoTipo", propOrder = {
    "idHojaCalculoLiquidacionDetalle",
    "idNominaDetalle",
    "idPilaDetalle",
    "idConciliacionContableDetalle",
    "aportaId",
    "aportaTipoIdentificacion",
    "aportaNumeroIdentificacion",
    "aportaPrimerNombre",
    "aportaClase",
    "aportaAporteEsapYMen",
    "aportaExcepcionLey12332008",
    "cotizId",
    "cotizTipoDocumento",
    "cotizNumeroIdentificacion",
    "cotizNombre",
    "cotizTipoCotizante",
    "cotizSubtipoCotizante",
    "cotizExtranjeroNoCotizar",
    "cotizColombianoEnElExt",
    "cotizActividadAltoRiesgoPe",
    "cotizdAno",
    "cotizdMes",
    "cotizdIng",
    "cotizdIngFecha",
    "cotizdRet",
    "cotizdRetFecha",
    "cotizdSln",
    "cotizdCausalSuspension",
    "cotizdCausalSuspFInicial",
    "cotizdCausalSuspFFinal",
    "diasSuspension",
    "cotizdIge",
    "cotizdIgeFInicial",
    "cotizdIgeFFinal",
    "diasIncapacidadGeneral",
    "cotizdLma",
    "cotizdLmaFInicial",
    "cotizdLmaFFinal",
    "diasLicenciaMaternidad",
    "cotizdVac",
    "cotizdVacFInicial",
    "cotizdVacFFinal",
    "diasVacaciones",
    "cotizdIrp",
    "cotizdIrpFInicial",
    "cotizdIrpFFinal",
    "diasIncapacidadRp",
    "cotizdSalarioIntegral",
    "diasLaborados",
    "cotizdUltimoIbcSinNovedad",
    "ingPilaAT",
    "retPilaAU",
    "slnPilaAV",
    "igePilaAU",
    "lmaPilaAU",
    "vacPilaAU",
    "irpPilaAU",
    "porcentajePagosNoSalariaBB",
    "excedeLimitePagoNoSalarBC",
    "codigoAdministradoraBD",
    "nombreCortoAdministradoraBE",
    "ibcCoreBF",
    "tarifaCoreBG",
    "cotizacionObligatoriaBH",
    "diasCotizadosPilaBI",
    "ibcPilaBJ",
    "tarifaPilaBK",
    "cotizacionPagadaPilaBL",
    "ajusteBM",
    "conceptoAjusteBN",
    "tipoIncumplimientoBO",
    "codigoAdministradoraBP",
    "nombreCortoAdministradoraBQ",
    "ibcCoreBR",
    "tarifaCorePensionBS",
    "cotizacionObligatoAPensiBT",
    "tarifaCoreFspBU",
    "cotizacionObligatoriaFspBV",
    "tarifaCorePensionAdicArBW",
    "cotizacionObliPenAdicArBX",
    "diasCotizadosPilaBY",
    "ibcPilaBZ",
    "tarifaPensionCA",
    "cotizacionPagadaPensionCB",
    "ajustePensionCC",
    "conceptoAjustePensionCD",
    "tarifaPilaFspCE",
    "cotizacionPagadaFspCF",
    "ajusteFspCG",
    "conceptoAjusteFspCH",
    "tarifaPilaPensiAdicioArCI",
    "cotizacionPagPensAdicArCJ",
    "ajustePensionAdicionalArCK",
    "conceptoAjustePensionArCL",
    "tipoIncumplimientoCM",
    "calculoActuarialCN",
    "codigoAdministradoraCO",
    "nombreCortoAdministradoraCP",
    "ibcCoreCQ",
    "cotizacionObligatoriaCR",
    "diasCotizadosPilaCS",
    "ibcPilaCT",
    "tarifaPilaCU",
    "cotizacionPagadaPilaCV",
    "ajusteCW",
    "conceptoAjusteCX",
    "tipoIncumplimientoCY",
    "nombreCortoAdministradoraCZ",
    "ibcCoreDA",
    "tarifaCoreDB",
    "cotizacionObligatoriaDC",
    "diasCotizadosPilaDD",
    "ibcPilaDE",
    "tarifaPilaDF",
    "cotizacionPagadaPilaDG",
    "ajusteDH",
    "conceptoAjusteDI",
    "tipoIncumplimientoDJ",
    "ibcCoreDK",
    "tarifaCoreDL",
    "aporteDM",
    "tarifaPilaDN",
    "aportePilaDO",
    "ajusteDP",
    "conceptoAjusteDQ",
    "tipoIncumplimientoDR",
    "ibcCoreDS",
    "tarifaCoreDT",
    "aporteDU",
    "tarifaPilaDV",
    "aportePilaDW",
    "ajusteDX",
    "conceptoAjusteDY",
    "tipoIncumplimientoDZ",
    "ibcEA",
    "tarifaCoreEB",
    "aporteEC",
    "tarifaPilaED",
    "aportePilaEE",
    "ajusteEF",
    "conceptoAjusteEG",
    "tipoIncumplimientoEH",
    "ibcCoreEI",
    "tarifaCoreEJ",
    "aporteEK",
    "tarifaPilaEL",
    "aporteEM",
    "ajusteEN",
    "diasCotSalud",
    "diasCotRprof",
    "diasCotCcf",
    "diasCotPension",
    "diasCotiPilaCcf",
    "planillaPilaCargada",
    "diasCotizPilaSalud",
    "diasCotizPilaPension",
    "diasCotizPilaArl",
    "ibcPermisosRemunerados",
    "ibcSuspPermisos",
    "ibcVacaciones",
    "ibcHuelga",
    "pagoNoSalarial",
    "totalRemunerado",
    "porPagoNoSalarial",
    "excLimPagoNoSalarial",
    "ibcPagosNomSalud",
    "ibcCalculadoSalud",
    "tarifaSalud",
    "tarifaSaludSuspension",
    "cotizOblCalculadaSalud",
    "ibcPilaSalud",
    "tarifaPilaSalud",
    "cotizPagadaPilaSalud",
    "ajusteSalud",
    "ibcPagosNomPension",
    "ibcCalculadoPension",
    "tarifaPension",
    "cotizOblPension",
    "ibcPilaPension",
    "tarifaPilaPension",
    "cotPagadaPilaPension",
    "ajustePension",
    "tarifaFspSubcuenSolidaridad",
    "tarifaFspSubcuenSubsisten",
    "cotizOblFspSubSolidaridad",
    "cotizOblFspSubSubsistencia",
    "cotizPagPilaFspSubSolidar",
    "cotizPagPilaFspSubSubsis",
    "ajusteFspSubcuenSubsisten",
    "ajusteFspSubcuenSolidaridad",
    "calculoActuarial",
    "ibcArl",
    "tarifaArl",
    "cotizOblArl",
    "ibcPilaArl",
    "tarifaPilaArl",
    "cotizPagadaPilaArl",
    "ajusteArl",
    "ibcCcf",
    "tarifaCcf",
    "cotizOblCcf",
    "ibcPilaCcf",
    "tarifaPilaCcf",
    "cotizPagadaPilaCcf",
    "ajusteCcf",
    "ibcSena",
    "tarifaSena",
    "cotizOblSena",
    "tarifaPilaSena",
    "cotizPagadaPilaSena",
    "ajusteSena",
    "ibcIcbf",
    "tarifaIcbf",
    "cotizOblIcbf",
    "tarifaPilaIcbf",
    "cotizPagadaPilaIcbf",
    "ajusteIcbf",
    "codAdmSalud",
    "codAdmArl",
    "codAdmCcf",
    "conceptoAjusteSalud",
    "conceptoAjustePension",
    "tipoIncumplimientoPension",
    "conceptoAjusteFsp",
    "tipoIncumplimientoFsp",
    "conceptoAjusteArl",
    "tipoIncumplimientoSalud",
    "tipoIncumplimientoArl",
    "conceptoAjusteCcf",
    "tipoIncumplimientoCcf",
    "conceptoAjusteSena",
    "tipoIncumplimientoSena",
    "tipoIncumplimientoIcbf",
    "conceptoAjusteIcbf",
    "conceptoAjusteEO",
    "tipoIncumplimientoEP",
    "conceptoContableTipo",
    "aportesIndependienteTipo",
    "contextoRespuestaTipo"
})
public class DetalleHojaCalculoTipo {

    protected String idHojaCalculoLiquidacionDetalle;
    protected String idNominaDetalle;
    protected String idPilaDetalle;
    protected String idConciliacionContableDetalle;
    @XmlElement(name = "APORTA_id", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long aportaId;
    @XmlElement(name = "APORTA_Tipo_Identificacion", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long aportaTipoIdentificacion;
    @XmlElement(name = "APORTA_Numero_Identificacion")
    protected String aportaNumeroIdentificacion;
    @XmlElement(name = "APORTA_Primer_Nombre")
    protected String aportaPrimerNombre;
    @XmlElement(name = "APORTA_Clase")
    protected String aportaClase;
    @XmlElement(name = "APORTA_Aporte_Esap_y_Men")
    protected String aportaAporteEsapYMen;
    @XmlElement(name = "APORTA_Excepcion_Ley_1233_2008")
    protected String aportaExcepcionLey12332008;
    @XmlElement(name = "COTIZ_id", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizId;
    @XmlElement(name = "COTIZ_Tipo_Documento", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizTipoDocumento;
    @XmlElement(name = "COTIZ_Numero_Identificacion")
    protected String cotizNumeroIdentificacion;
    @XmlElement(name = "COTIZ_Nombre")
    protected String cotizNombre;
    @XmlElement(name = "COTIZ_Tipo_Cotizante", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizTipoCotizante;
    @XmlElement(name = "COTIZ_Subtipo_Cotizante", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizSubtipoCotizante;
    @XmlElement(name = "COTIZ_Extranjero_No_Cotizar")
    protected String cotizExtranjeroNoCotizar;
    @XmlElement(name = "COTIZ_Colombiano_En_El_Ext")
    protected String cotizColombianoEnElExt;
    @XmlElement(name = "COTIZ_Actividad_Alto_Riesgo_Pe")
    protected String cotizActividadAltoRiesgoPe;
    @XmlElement(name = "COTIZD_ano", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizdAno;
    @XmlElement(name = "COTIZD_mes", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizdMes;
    @XmlElement(name = "COTIZD_ing")
    protected String cotizdIng;
    @XmlElement(name = "COTIZD_ing_fecha", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIngFecha;
    @XmlElement(name = "COTIZD_ret")
    protected String cotizdRet;
    @XmlElement(name = "COTIZD_ret_fecha", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdRetFecha;
    @XmlElement(name = "COTIZD_sln")
    protected String cotizdSln;
    @XmlElement(name = "COTIZD_causal_suspension")
    protected String cotizdCausalSuspension;
    @XmlElement(name = "COTIZD_causal_susp_fInicial", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdCausalSuspFInicial;
    @XmlElement(name = "COTIZD_causal_susp_fFinal", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdCausalSuspFFinal;
    @XmlElement(name = "dias_suspension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasSuspension;
    @XmlElement(name = "COTIZD_ige")
    protected String cotizdIge;
    @XmlElement(name = "COTIZD_ige_fInicial", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIgeFInicial;
    @XmlElement(name = "COTIZD_ige_fFinal", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIgeFFinal;
    @XmlElement(name = "dias_incapacidad_general", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasIncapacidadGeneral;
    @XmlElement(name = "COTIZD_lma")
    protected String cotizdLma;
    @XmlElement(name = "COTIZD_lma_fInicial", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdLmaFInicial;
    @XmlElement(name = "COTIZD_lma_fFinal", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdLmaFFinal;
    @XmlElement(name = "dias_licencia_maternidad", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasLicenciaMaternidad;
    @XmlElement(name = "COTIZD_vac")
    protected String cotizdVac;
    @XmlElement(name = "COTIZD_vac_fInicial", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdVacFInicial;
    @XmlElement(name = "COTIZD_vac_fFinal", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdVacFFinal;
    @XmlElement(name = "dias_vacaciones", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasVacaciones;
    @XmlElement(name = "COTIZD_irp")
    protected String cotizdIrp;
    @XmlElement(name = "COTIZD_irp_fInicial", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIrpFInicial;
    @XmlElement(name = "COTIZD_irp_fFinal", required = true, type = String.class)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar cotizdIrpFFinal;
    @XmlElement(name = "dias_incapacidad_rp", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasIncapacidadRp;
    @XmlElement(name = "COTIZD_salario_integral")
    protected String cotizdSalarioIntegral;
    @XmlElement(name = "dias_laborados", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasLaborados;
    @XmlElement(name = "COTIZD_ultimo_ibc_sin_novedad")
    protected String cotizdUltimoIbcSinNovedad;
    @XmlElement(name = "ing_pila_AT")
    protected String ingPilaAT;
    @XmlElement(name = "ret_pila_AU")
    protected String retPilaAU;
    @XmlElement(name = "sln_pila_AV")
    protected String slnPilaAV;
    @XmlElement(name = "ige_pila_AU")
    protected String igePilaAU;
    @XmlElement(name = "lma_pila_AU")
    protected String lmaPilaAU;
    @XmlElement(name = "vac_pila_AU")
    protected String vacPilaAU;
    @XmlElement(name = "irp_pila_AU")
    protected String irpPilaAU;
    @XmlElement(name = "porcentaje_pagos_no_salaria_BB")
    protected BigDecimal porcentajePagosNoSalariaBB;
    @XmlElement(name = "excede_limite_pago_no_salar_BC")
    protected BigDecimal excedeLimitePagoNoSalarBC;
    @XmlElement(name = "codigo_administradora_BD")
    protected String codigoAdministradoraBD;
    @XmlElement(name = "nombre_corto_administradora_BE")
    protected String nombreCortoAdministradoraBE;
    @XmlElement(name = "ibc_core_BF")
    protected BigDecimal ibcCoreBF;
    @XmlElement(name = "tarifa_core_BG")
    protected BigDecimal tarifaCoreBG;
    @XmlElement(name = "cotizacion_obligatoria_BH")
    protected BigDecimal cotizacionObligatoriaBH;
    @XmlElement(name = "dias_cotizados_pila_BI", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaBI;
    @XmlElement(name = "ibc_pila_BJ")
    protected BigDecimal ibcPilaBJ;
    @XmlElement(name = "tarifa_pila_BK")
    protected BigDecimal tarifaPilaBK;
    @XmlElement(name = "cotizacion_pagada_pila_BL")
    protected BigDecimal cotizacionPagadaPilaBL;
    @XmlElement(name = "ajuste_BM")
    protected BigDecimal ajusteBM;
    @XmlElement(name = "concepto_ajuste_BN")
    protected String conceptoAjusteBN;
    @XmlElement(name = "tipo_incumplimiento_BO")
    protected String tipoIncumplimientoBO;
    @XmlElement(name = "codigo_administradora_BP")
    protected String codigoAdministradoraBP;
    @XmlElement(name = "nombre_corto_administradora_BQ")
    protected String nombreCortoAdministradoraBQ;
    @XmlElement(name = "ibc_core_BR")
    protected BigDecimal ibcCoreBR;
    @XmlElement(name = "tarifa_core_pension_BS")
    protected BigDecimal tarifaCorePensionBS;
    @XmlElement(name = "cotizacion_obligato_a_pensi_BT")
    protected BigDecimal cotizacionObligatoAPensiBT;
    @XmlElement(name = "tarifa_core_fsp_BU")
    protected BigDecimal tarifaCoreFspBU;
    @XmlElement(name = "cotizacion_obligatoria_fsp_BV")
    protected BigDecimal cotizacionObligatoriaFspBV;
    @XmlElement(name = "tarifa_core_pension_adic_ar_BW")
    protected BigDecimal tarifaCorePensionAdicArBW;
    @XmlElement(name = "cotizacion_obli_pen_adic_ar_BX")
    protected BigDecimal cotizacionObliPenAdicArBX;
    @XmlElement(name = "dias_cotizados_pila_BY", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaBY;
    @XmlElement(name = "ibc_pila_BZ")
    protected BigDecimal ibcPilaBZ;
    @XmlElement(name = "tarifa_pension_CA")
    protected BigDecimal tarifaPensionCA;
    @XmlElement(name = "cotizacion_pagada_pension_CB")
    protected BigDecimal cotizacionPagadaPensionCB;
    @XmlElement(name = "ajuste_pension_CC")
    protected BigDecimal ajustePensionCC;
    @XmlElement(name = "concepto_ajuste_pension_CD")
    protected String conceptoAjustePensionCD;
    @XmlElement(name = "tarifa_pila_fsp_CE")
    protected BigDecimal tarifaPilaFspCE;
    @XmlElement(name = "cotizacion_pagada_fsp_CF")
    protected BigDecimal cotizacionPagadaFspCF;
    @XmlElement(name = "ajuste_fsp_CG")
    protected BigDecimal ajusteFspCG;
    @XmlElement(name = "concepto_ajuste_fsp_CH")
    protected String conceptoAjusteFspCH;
    @XmlElement(name = "tarifa_pila_pensi_adicio_ar_CI")
    protected BigDecimal tarifaPilaPensiAdicioArCI;
    @XmlElement(name = "cotizacion_pag_pens_adic_ar_CJ")
    protected BigDecimal cotizacionPagPensAdicArCJ;
    @XmlElement(name = "ajuste_pension_adicional_ar_CK")
    protected BigDecimal ajustePensionAdicionalArCK;
    @XmlElement(name = "concepto_ajuste_pension_ar_CL")
    protected String conceptoAjustePensionArCL;
    @XmlElement(name = "tipo_incumplimiento_CM")
    protected String tipoIncumplimientoCM;
    @XmlElement(name = "calculo_actuarial_CN")
    protected BigDecimal calculoActuarialCN;
    @XmlElement(name = "codigo_administradora_CO")
    protected String codigoAdministradoraCO;
    @XmlElement(name = "nombre_corto_administradora_CP")
    protected String nombreCortoAdministradoraCP;
    @XmlElement(name = "ibc_core_CQ")
    protected BigDecimal ibcCoreCQ;
    @XmlElement(name = "cotizacion_obligatoria_CR")
    protected BigDecimal cotizacionObligatoriaCR;
    @XmlElement(name = "dias_cotizados_pila_CS", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaCS;
    @XmlElement(name = "ibc_pila_CT")
    protected BigDecimal ibcPilaCT;
    @XmlElement(name = "tarifa_pila_CU")
    protected BigDecimal tarifaPilaCU;
    @XmlElement(name = "cotizacion_pagada_pila_CV")
    protected BigDecimal cotizacionPagadaPilaCV;
    @XmlElement(name = "ajuste_CW")
    protected BigDecimal ajusteCW;
    @XmlElement(name = "concepto_ajuste_CX")
    protected String conceptoAjusteCX;
    @XmlElement(name = "tipo_incumplimiento_CY")
    protected String tipoIncumplimientoCY;
    @XmlElement(name = "nombre_corto_administradora_CZ")
    protected String nombreCortoAdministradoraCZ;
    @XmlElement(name = "ibc_core_DA")
    protected BigDecimal ibcCoreDA;
    @XmlElement(name = "tarifa_core_DB")
    protected BigDecimal tarifaCoreDB;
    @XmlElement(name = "cotizacion_obligatoria_DC")
    protected BigDecimal cotizacionObligatoriaDC;
    @XmlElement(name = "dias_cotizados_pila_DD", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizadosPilaDD;
    @XmlElement(name = "ibc_pila_DE")
    protected BigDecimal ibcPilaDE;
    @XmlElement(name = "tarifa_pila_DF")
    protected BigDecimal tarifaPilaDF;
    @XmlElement(name = "cotizacion_pagada_pila_DG")
    protected BigDecimal cotizacionPagadaPilaDG;
    @XmlElement(name = "ajuste_DH")
    protected BigDecimal ajusteDH;
    @XmlElement(name = "concepto_ajuste_DI")
    protected String conceptoAjusteDI;
    @XmlElement(name = "tipo_incumplimiento_DJ")
    protected String tipoIncumplimientoDJ;
    @XmlElement(name = "ibc_core_DK")
    protected BigDecimal ibcCoreDK;
    @XmlElement(name = "tarifa_core_DL")
    protected BigDecimal tarifaCoreDL;
    @XmlElement(name = "aporte_DM")
    protected BigDecimal aporteDM;
    @XmlElement(name = "tarifa_pila_DN")
    protected BigDecimal tarifaPilaDN;
    @XmlElement(name = "aporte_pila_DO")
    protected BigDecimal aportePilaDO;
    @XmlElement(name = "ajuste_DP")
    protected BigDecimal ajusteDP;
    @XmlElement(name = "concepto_ajuste_DQ")
    protected String conceptoAjusteDQ;
    @XmlElement(name = "tipo_incumplimiento_DR")
    protected String tipoIncumplimientoDR;
    @XmlElement(name = "ibc_core_DS")
    protected BigDecimal ibcCoreDS;
    @XmlElement(name = "tarifa_core_DT")
    protected BigDecimal tarifaCoreDT;
    @XmlElement(name = "aporte_DU")
    protected BigDecimal aporteDU;
    @XmlElement(name = "tarifa_pila_DV")
    protected BigDecimal tarifaPilaDV;
    @XmlElement(name = "aporte_pila_DW")
    protected BigDecimal aportePilaDW;
    @XmlElement(name = "ajuste_DX")
    protected BigDecimal ajusteDX;
    @XmlElement(name = "concepto_ajuste_DY")
    protected String conceptoAjusteDY;
    @XmlElement(name = "tipo_incumplimiento_DZ")
    protected String tipoIncumplimientoDZ;
    @XmlElement(name = "ibc_EA")
    protected BigDecimal ibcEA;
    @XmlElement(name = "tarifa_core_EB")
    protected BigDecimal tarifaCoreEB;
    @XmlElement(name = "aporte_EC")
    protected BigDecimal aporteEC;
    @XmlElement(name = "tarifa_pila_ED")
    protected BigDecimal tarifaPilaED;
    @XmlElement(name = "aporte_pila_EE")
    protected BigDecimal aportePilaEE;
    @XmlElement(name = "ajuste_EF")
    protected BigDecimal ajusteEF;
    @XmlElement(name = "concepto_ajuste_EG")
    protected String conceptoAjusteEG;
    @XmlElement(name = "tipo_incumplimiento_EH")
    protected String tipoIncumplimientoEH;
    @XmlElement(name = "ibc_core_EI")
    protected BigDecimal ibcCoreEI;
    @XmlElement(name = "tarifa_core_EJ")
    protected BigDecimal tarifaCoreEJ;
    @XmlElement(name = "aporte_EK")
    protected BigDecimal aporteEK;
    @XmlElement(name = "tarifa_pila_EL")
    protected BigDecimal tarifaPilaEL;
    @XmlElement(name = "aporte_EM")
    protected BigDecimal aporteEM;
    @XmlElement(name = "ajuste_EN")
    protected BigDecimal ajusteEN;
    @XmlElement(name = "dias_cot_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotSalud;
    @XmlElement(name = "dias_cot_rprof", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotRprof;
    @XmlElement(name = "dias_cot_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotCcf;
    @XmlElement(name = "dias_cot_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotPension;
    @XmlElement(name = "dias_coti_pila_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotiPilaCcf;
    @XmlElement(name = "planilla_pila_cargada", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long planillaPilaCargada;
    @XmlElement(name = "dias_cotiz_pila_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizPilaSalud;
    @XmlElement(name = "dias_cotiz_pila_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizPilaPension;
    @XmlElement(name = "dias_cotiz_pila_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long diasCotizPilaArl;
    @XmlElement(name = "ibc_permisos_remunerados", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPermisosRemunerados;
    @XmlElement(name = "ibc_susp_permisos", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcSuspPermisos;
    @XmlElement(name = "ibc_vacaciones", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcVacaciones;
    @XmlElement(name = "ibc_huelga", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcHuelga;
    @XmlElement(name = "pago_no_salarial", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long pagoNoSalarial;
    @XmlElement(name = "total_remunerado", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long totalRemunerado;
    @XmlElement(name = "por_pago_no_salarial", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long porPagoNoSalarial;
    @XmlElement(name = "exc_lim_pago_no_salarial", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long excLimPagoNoSalarial;
    @XmlElement(name = "ibc_pagos_nom_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPagosNomSalud;
    @XmlElement(name = "ibc_calculado_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcCalculadoSalud;
    @XmlElement(name = "tarifa_salud")
    protected BigDecimal tarifaSalud;
    @XmlElement(name = "tarifa_salud_suspension")
    protected BigDecimal tarifaSaludSuspension;
    @XmlElement(name = "cotiz_obl_calculada_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblCalculadaSalud;
    @XmlElement(name = "ibc_pila_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPilaSalud;
    @XmlElement(name = "tarifa_pila_salud")
    protected BigDecimal tarifaPilaSalud;
    @XmlElement(name = "cotiz_pagada_pila_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagadaPilaSalud;
    @XmlElement(name = "ajuste_salud", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteSalud;
    @XmlElement(name = "ibc_pagos_nom_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPagosNomPension;
    @XmlElement(name = "ibc_calculado_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcCalculadoPension;
    @XmlElement(name = "tarifa_pension")
    protected BigDecimal tarifaPension;
    @XmlElement(name = "cotiz_obl_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblPension;
    @XmlElement(name = "ibc_pila_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPilaPension;
    @XmlElement(name = "tarifa_pila_pension")
    protected BigDecimal tarifaPilaPension;
    @XmlElement(name = "cot_pagada_pila_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotPagadaPilaPension;
    @XmlElement(name = "ajuste_pension", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajustePension;
    @XmlElement(name = "tarifa_fsp_subcuen_solidaridad", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long tarifaFspSubcuenSolidaridad;
    @XmlElement(name = "tarifa_fsp_subcuen_subsisten", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long tarifaFspSubcuenSubsisten;
    @XmlElement(name = "cotiz_obl_fsp_sub_solidaridad", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblFspSubSolidaridad;
    @XmlElement(name = "cotiz_obl_fsp_sub_subsistencia", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblFspSubSubsistencia;
    @XmlElement(name = "cotiz_pag_pila_fsp_sub_solidar", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagPilaFspSubSolidar;
    @XmlElement(name = "cotiz_pag_pila_fsp_sub_subsis", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagPilaFspSubSubsis;
    @XmlElement(name = "ajuste_fsp_subcuen_subsisten", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteFspSubcuenSubsisten;
    @XmlElement(name = "ajuste_fsp_subcuen_solidaridad", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteFspSubcuenSolidaridad;
    @XmlElement(name = "calculo_actuarial", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long calculoActuarial;
    @XmlElement(name = "ibc_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcArl;
    @XmlElement(name = "tarifa_arl")
    protected BigDecimal tarifaArl;
    @XmlElement(name = "cotiz_obl_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblArl;
    @XmlElement(name = "ibc_pila_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPilaArl;
    @XmlElement(name = "tarifa_pila_arl")
    protected BigDecimal tarifaPilaArl;
    @XmlElement(name = "cotiz_pagada_pila_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagadaPilaArl;
    @XmlElement(name = "ajuste_arl", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteArl;
    @XmlElement(name = "ibc_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcCcf;
    @XmlElement(name = "tarifa_ccf")
    protected BigDecimal tarifaCcf;
    @XmlElement(name = "cotiz_obl_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblCcf;
    @XmlElement(name = "ibc_pila_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcPilaCcf;
    @XmlElement(name = "tarifa_pila_ccf")
    protected BigDecimal tarifaPilaCcf;
    @XmlElement(name = "cotiz_pagada_pila_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagadaPilaCcf;
    @XmlElement(name = "ajuste_ccf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteCcf;
    @XmlElement(name = "ibc_sena", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcSena;
    @XmlElement(name = "tarifa_sena")
    protected BigDecimal tarifaSena;
    @XmlElement(name = "cotiz_obl_sena", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblSena;
    @XmlElement(name = "tarifa_pila_sena")
    protected BigDecimal tarifaPilaSena;
    @XmlElement(name = "cotiz_pagada_pila_sena", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagadaPilaSena;
    @XmlElement(name = "ajuste_sena", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteSena;
    @XmlElement(name = "ibc_icbf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ibcIcbf;
    @XmlElement(name = "tarifa_icbf")
    protected BigDecimal tarifaIcbf;
    @XmlElement(name = "cotiz_obl_icbf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizOblIcbf;
    @XmlElement(name = "tarifa_pila_icbf")
    protected BigDecimal tarifaPilaIcbf;
    @XmlElement(name = "cotiz_pagada_pila_icbf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long cotizPagadaPilaIcbf;
    @XmlElement(name = "ajuste_icbf", type = String.class)
    @XmlJavaTypeAdapter(Adapter2 .class)
    @XmlSchemaType(name = "integer")
    protected Long ajusteIcbf;
    @XmlElement(name = "cod_adm_salud")
    protected String codAdmSalud;
    @XmlElement(name = "cod_adm_arl")
    protected String codAdmArl;
    @XmlElement(name = "cod_adm_ccf")
    protected String codAdmCcf;
    @XmlElement(name = "concepto_ajuste_salud")
    protected String conceptoAjusteSalud;
    @XmlElement(name = "concepto_ajuste_pension")
    protected String conceptoAjustePension;
    @XmlElement(name = "tipo_incumplimiento_pension")
    protected String tipoIncumplimientoPension;
    @XmlElement(name = "concepto_ajuste_fsp")
    protected String conceptoAjusteFsp;
    @XmlElement(name = "tipo_incumplimiento_fsp")
    protected String tipoIncumplimientoFsp;
    @XmlElement(name = "concepto_ajuste_arl")
    protected String conceptoAjusteArl;
    @XmlElement(name = "tipo_incumplimiento_salud")
    protected String tipoIncumplimientoSalud;
    @XmlElement(name = "tipo_incumplimiento_arl")
    protected String tipoIncumplimientoArl;
    @XmlElement(name = "concepto_ajuste_ccf")
    protected String conceptoAjusteCcf;
    @XmlElement(name = "tipo_incumplimiento_ccf")
    protected String tipoIncumplimientoCcf;
    @XmlElement(name = "concepto_ajuste_sena")
    protected String conceptoAjusteSena;
    @XmlElement(name = "tipo_incumplimiento_sena")
    protected String tipoIncumplimientoSena;
    @XmlElement(name = "tipo_incumplimiento_icbf")
    protected String tipoIncumplimientoIcbf;
    @XmlElement(name = "concepto_ajuste_icbf")
    protected String conceptoAjusteIcbf;
    @XmlElement(name = "concepto_ajuste_EO")
    protected String conceptoAjusteEO;
    @XmlElement(name = "tipo_incumplimiento_EP")
    protected String tipoIncumplimientoEP;
    @XmlElement(name = "ConceptoContableTipo", required = true)
    protected List<ConceptoContableTipo> conceptoContableTipo;
    @XmlElement(name = "AportesIndependienteTipo", required = true)
    protected List<AportesIndependienteTipo> aportesIndependienteTipo;
    @XmlElement(name = "ContextoRespuestaTipo", required = true)
    protected ContextoRespuestaTipo contextoRespuestaTipo;

    /**
     * Obtiene el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculoLiquidacionDetalle() {
        return idHojaCalculoLiquidacionDetalle;
    }

    /**
     * Define el valor de la propiedad idHojaCalculoLiquidacionDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculoLiquidacionDetalle(String value) {
        this.idHojaCalculoLiquidacionDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idNominaDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNominaDetalle() {
        return idNominaDetalle;
    }

    /**
     * Define el valor de la propiedad idNominaDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNominaDetalle(String value) {
        this.idNominaDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idPilaDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPilaDetalle() {
        return idPilaDetalle;
    }

    /**
     * Define el valor de la propiedad idPilaDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPilaDetalle(String value) {
        this.idPilaDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idConciliacionContableDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConciliacionContableDetalle() {
        return idConciliacionContableDetalle;
    }

    /**
     * Define el valor de la propiedad idConciliacionContableDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConciliacionContableDetalle(String value) {
        this.idConciliacionContableDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAPORTAId() {
        return aportaId;
    }

    /**
     * Define el valor de la propiedad aportaId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAId(Long value) {
        this.aportaId = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaTipoIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAPORTATipoIdentificacion() {
        return aportaTipoIdentificacion;
    }

    /**
     * Define el valor de la propiedad aportaTipoIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTATipoIdentificacion(Long value) {
        this.aportaTipoIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaNumeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTANumeroIdentificacion() {
        return aportaNumeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad aportaNumeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTANumeroIdentificacion(String value) {
        this.aportaNumeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaPrimerNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAPrimerNombre() {
        return aportaPrimerNombre;
    }

    /**
     * Define el valor de la propiedad aportaPrimerNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAPrimerNombre(String value) {
        this.aportaPrimerNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaClase.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAClase() {
        return aportaClase;
    }

    /**
     * Define el valor de la propiedad aportaClase.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAClase(String value) {
        this.aportaClase = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaAporteEsapYMen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAAporteEsapYMen() {
        return aportaAporteEsapYMen;
    }

    /**
     * Define el valor de la propiedad aportaAporteEsapYMen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAAporteEsapYMen(String value) {
        this.aportaAporteEsapYMen = value;
    }

    /**
     * Obtiene el valor de la propiedad aportaExcepcionLey12332008.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPORTAExcepcionLey12332008() {
        return aportaExcepcionLey12332008;
    }

    /**
     * Define el valor de la propiedad aportaExcepcionLey12332008.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPORTAExcepcionLey12332008(String value) {
        this.aportaExcepcionLey12332008 = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZId() {
        return cotizId;
    }

    /**
     * Define el valor de la propiedad cotizId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZId(Long value) {
        this.cotizId = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizTipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZTipoDocumento() {
        return cotizTipoDocumento;
    }

    /**
     * Define el valor de la propiedad cotizTipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZTipoDocumento(Long value) {
        this.cotizTipoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizNumeroIdentificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZNumeroIdentificacion() {
        return cotizNumeroIdentificacion;
    }

    /**
     * Define el valor de la propiedad cotizNumeroIdentificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZNumeroIdentificacion(String value) {
        this.cotizNumeroIdentificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizNombre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZNombre() {
        return cotizNombre;
    }

    /**
     * Define el valor de la propiedad cotizNombre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZNombre(String value) {
        this.cotizNombre = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizTipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZTipoCotizante() {
        return cotizTipoCotizante;
    }

    /**
     * Define el valor de la propiedad cotizTipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZTipoCotizante(Long value) {
        this.cotizTipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizSubtipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZSubtipoCotizante() {
        return cotizSubtipoCotizante;
    }

    /**
     * Define el valor de la propiedad cotizSubtipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZSubtipoCotizante(Long value) {
        this.cotizSubtipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizExtranjeroNoCotizar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZExtranjeroNoCotizar() {
        return cotizExtranjeroNoCotizar;
    }

    /**
     * Define el valor de la propiedad cotizExtranjeroNoCotizar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZExtranjeroNoCotizar(String value) {
        this.cotizExtranjeroNoCotizar = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizColombianoEnElExt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZColombianoEnElExt() {
        return cotizColombianoEnElExt;
    }

    /**
     * Define el valor de la propiedad cotizColombianoEnElExt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZColombianoEnElExt(String value) {
        this.cotizColombianoEnElExt = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizActividadAltoRiesgoPe.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZActividadAltoRiesgoPe() {
        return cotizActividadAltoRiesgoPe;
    }

    /**
     * Define el valor de la propiedad cotizActividadAltoRiesgoPe.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZActividadAltoRiesgoPe(String value) {
        this.cotizActividadAltoRiesgoPe = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdAno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZDAno() {
        return cotizdAno;
    }

    /**
     * Define el valor de la propiedad cotizdAno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDAno(Long value) {
        this.cotizdAno = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdMes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCOTIZDMes() {
        return cotizdMes;
    }

    /**
     * Define el valor de la propiedad cotizdMes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDMes(Long value) {
        this.cotizdMes = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIng.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIng() {
        return cotizdIng;
    }

    /**
     * Define el valor de la propiedad cotizdIng.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIng(String value) {
        this.cotizdIng = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIngFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIngFecha() {
        return cotizdIngFecha;
    }

    /**
     * Define el valor de la propiedad cotizdIngFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIngFecha(Calendar value) {
        this.cotizdIngFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdRet.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDRet() {
        return cotizdRet;
    }

    /**
     * Define el valor de la propiedad cotizdRet.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDRet(String value) {
        this.cotizdRet = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdRetFecha.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDRetFecha() {
        return cotizdRetFecha;
    }

    /**
     * Define el valor de la propiedad cotizdRetFecha.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDRetFecha(Calendar value) {
        this.cotizdRetFecha = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdSln.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDSln() {
        return cotizdSln;
    }

    /**
     * Define el valor de la propiedad cotizdSln.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDSln(String value) {
        this.cotizdSln = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDCausalSuspension() {
        return cotizdCausalSuspension;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspension(String value) {
        this.cotizdCausalSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDCausalSuspFInicial() {
        return cotizdCausalSuspFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspFInicial(Calendar value) {
        this.cotizdCausalSuspFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdCausalSuspFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDCausalSuspFFinal() {
        return cotizdCausalSuspFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdCausalSuspFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDCausalSuspFFinal(Calendar value) {
        this.cotizdCausalSuspFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasSuspension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasSuspension() {
        return diasSuspension;
    }

    /**
     * Define el valor de la propiedad diasSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasSuspension(Long value) {
        this.diasSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIge.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIge() {
        return cotizdIge;
    }

    /**
     * Define el valor de la propiedad cotizdIge.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIge(String value) {
        this.cotizdIge = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIgeFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIgeFInicial() {
        return cotizdIgeFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdIgeFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIgeFInicial(Calendar value) {
        this.cotizdIgeFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIgeFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIgeFFinal() {
        return cotizdIgeFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdIgeFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIgeFFinal(Calendar value) {
        this.cotizdIgeFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasIncapacidadGeneral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasIncapacidadGeneral() {
        return diasIncapacidadGeneral;
    }

    /**
     * Define el valor de la propiedad diasIncapacidadGeneral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasIncapacidadGeneral(Long value) {
        this.diasIncapacidadGeneral = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDLma() {
        return cotizdLma;
    }

    /**
     * Define el valor de la propiedad cotizdLma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLma(String value) {
        this.cotizdLma = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLmaFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDLmaFInicial() {
        return cotizdLmaFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdLmaFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLmaFInicial(Calendar value) {
        this.cotizdLmaFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdLmaFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDLmaFFinal() {
        return cotizdLmaFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdLmaFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDLmaFFinal(Calendar value) {
        this.cotizdLmaFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasLicenciaMaternidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasLicenciaMaternidad() {
        return diasLicenciaMaternidad;
    }

    /**
     * Define el valor de la propiedad diasLicenciaMaternidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasLicenciaMaternidad(Long value) {
        this.diasLicenciaMaternidad = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdVac.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDVac() {
        return cotizdVac;
    }

    /**
     * Define el valor de la propiedad cotizdVac.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDVac(String value) {
        this.cotizdVac = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdVacFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDVacFInicial() {
        return cotizdVacFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdVacFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDVacFInicial(Calendar value) {
        this.cotizdVacFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdVacFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDVacFFinal() {
        return cotizdVacFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdVacFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDVacFFinal(Calendar value) {
        this.cotizdVacFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasVacaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasVacaciones() {
        return diasVacaciones;
    }

    /**
     * Define el valor de la propiedad diasVacaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasVacaciones(Long value) {
        this.diasVacaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDIrp() {
        return cotizdIrp;
    }

    /**
     * Define el valor de la propiedad cotizdIrp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrp(String value) {
        this.cotizdIrp = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrpFInicial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIrpFInicial() {
        return cotizdIrpFInicial;
    }

    /**
     * Define el valor de la propiedad cotizdIrpFInicial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrpFInicial(Calendar value) {
        this.cotizdIrpFInicial = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdIrpFFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getCOTIZDIrpFFinal() {
        return cotizdIrpFFinal;
    }

    /**
     * Define el valor de la propiedad cotizdIrpFFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDIrpFFinal(Calendar value) {
        this.cotizdIrpFFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad diasIncapacidadRp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasIncapacidadRp() {
        return diasIncapacidadRp;
    }

    /**
     * Define el valor de la propiedad diasIncapacidadRp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasIncapacidadRp(Long value) {
        this.diasIncapacidadRp = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdSalarioIntegral.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDSalarioIntegral() {
        return cotizdSalarioIntegral;
    }

    /**
     * Define el valor de la propiedad cotizdSalarioIntegral.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDSalarioIntegral(String value) {
        this.cotizdSalarioIntegral = value;
    }

    /**
     * Obtiene el valor de la propiedad diasLaborados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasLaborados() {
        return diasLaborados;
    }

    /**
     * Define el valor de la propiedad diasLaborados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasLaborados(Long value) {
        this.diasLaborados = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizdUltimoIbcSinNovedad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOTIZDUltimoIbcSinNovedad() {
        return cotizdUltimoIbcSinNovedad;
    }

    /**
     * Define el valor de la propiedad cotizdUltimoIbcSinNovedad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOTIZDUltimoIbcSinNovedad(String value) {
        this.cotizdUltimoIbcSinNovedad = value;
    }

    /**
     * Obtiene el valor de la propiedad ingPilaAT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIngPilaAT() {
        return ingPilaAT;
    }

    /**
     * Define el valor de la propiedad ingPilaAT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIngPilaAT(String value) {
        this.ingPilaAT = value;
    }

    /**
     * Obtiene el valor de la propiedad retPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetPilaAU() {
        return retPilaAU;
    }

    /**
     * Define el valor de la propiedad retPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetPilaAU(String value) {
        this.retPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad slnPilaAV.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSlnPilaAV() {
        return slnPilaAV;
    }

    /**
     * Define el valor de la propiedad slnPilaAV.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSlnPilaAV(String value) {
        this.slnPilaAV = value;
    }

    /**
     * Obtiene el valor de la propiedad igePilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIgePilaAU() {
        return igePilaAU;
    }

    /**
     * Define el valor de la propiedad igePilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIgePilaAU(String value) {
        this.igePilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad lmaPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLmaPilaAU() {
        return lmaPilaAU;
    }

    /**
     * Define el valor de la propiedad lmaPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLmaPilaAU(String value) {
        this.lmaPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad vacPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVacPilaAU() {
        return vacPilaAU;
    }

    /**
     * Define el valor de la propiedad vacPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVacPilaAU(String value) {
        this.vacPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad irpPilaAU.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIrpPilaAU() {
        return irpPilaAU;
    }

    /**
     * Define el valor de la propiedad irpPilaAU.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIrpPilaAU(String value) {
        this.irpPilaAU = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajePagosNoSalariaBB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPorcentajePagosNoSalariaBB() {
        return porcentajePagosNoSalariaBB;
    }

    /**
     * Define el valor de la propiedad porcentajePagosNoSalariaBB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPorcentajePagosNoSalariaBB(BigDecimal value) {
        this.porcentajePagosNoSalariaBB = value;
    }

    /**
     * Obtiene el valor de la propiedad excedeLimitePagoNoSalarBC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getExcedeLimitePagoNoSalarBC() {
        return excedeLimitePagoNoSalarBC;
    }

    /**
     * Define el valor de la propiedad excedeLimitePagoNoSalarBC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setExcedeLimitePagoNoSalarBC(BigDecimal value) {
        this.excedeLimitePagoNoSalarBC = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraBD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraBD() {
        return codigoAdministradoraBD;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraBD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraBD(String value) {
        this.codigoAdministradoraBD = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraBE.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraBE() {
        return nombreCortoAdministradoraBE;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraBE.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraBE(String value) {
        this.nombreCortoAdministradoraBE = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreBF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreBF() {
        return ibcCoreBF;
    }

    /**
     * Define el valor de la propiedad ibcCoreBF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreBF(BigDecimal value) {
        this.ibcCoreBF = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreBG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreBG() {
        return tarifaCoreBG;
    }

    /**
     * Define el valor de la propiedad tarifaCoreBG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreBG(BigDecimal value) {
        this.tarifaCoreBG = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaBH.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaBH() {
        return cotizacionObligatoriaBH;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaBH.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaBH(BigDecimal value) {
        this.cotizacionObligatoriaBH = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaBI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaBI() {
        return diasCotizadosPilaBI;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaBI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaBI(Long value) {
        this.diasCotizadosPilaBI = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaBJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaBJ() {
        return ibcPilaBJ;
    }

    /**
     * Define el valor de la propiedad ibcPilaBJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaBJ(BigDecimal value) {
        this.ibcPilaBJ = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaBK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaBK() {
        return tarifaPilaBK;
    }

    /**
     * Define el valor de la propiedad tarifaPilaBK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaBK(BigDecimal value) {
        this.tarifaPilaBK = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaBL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaBL() {
        return cotizacionPagadaPilaBL;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaBL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaBL(BigDecimal value) {
        this.cotizacionPagadaPilaBL = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteBM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteBM() {
        return ajusteBM;
    }

    /**
     * Define el valor de la propiedad ajusteBM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteBM(BigDecimal value) {
        this.ajusteBM = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteBN.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteBN() {
        return conceptoAjusteBN;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteBN.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteBN(String value) {
        this.conceptoAjusteBN = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoBO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoBO() {
        return tipoIncumplimientoBO;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoBO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoBO(String value) {
        this.tipoIncumplimientoBO = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraBP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraBP() {
        return codigoAdministradoraBP;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraBP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraBP(String value) {
        this.codigoAdministradoraBP = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraBQ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraBQ() {
        return nombreCortoAdministradoraBQ;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraBQ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraBQ(String value) {
        this.nombreCortoAdministradoraBQ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreBR.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreBR() {
        return ibcCoreBR;
    }

    /**
     * Define el valor de la propiedad ibcCoreBR.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreBR(BigDecimal value) {
        this.ibcCoreBR = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCorePensionBS.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCorePensionBS() {
        return tarifaCorePensionBS;
    }

    /**
     * Define el valor de la propiedad tarifaCorePensionBS.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCorePensionBS(BigDecimal value) {
        this.tarifaCorePensionBS = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoAPensiBT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoAPensiBT() {
        return cotizacionObligatoAPensiBT;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoAPensiBT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoAPensiBT(BigDecimal value) {
        this.cotizacionObligatoAPensiBT = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreFspBU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreFspBU() {
        return tarifaCoreFspBU;
    }

    /**
     * Define el valor de la propiedad tarifaCoreFspBU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreFspBU(BigDecimal value) {
        this.tarifaCoreFspBU = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaFspBV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaFspBV() {
        return cotizacionObligatoriaFspBV;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaFspBV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaFspBV(BigDecimal value) {
        this.cotizacionObligatoriaFspBV = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCorePensionAdicArBW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCorePensionAdicArBW() {
        return tarifaCorePensionAdicArBW;
    }

    /**
     * Define el valor de la propiedad tarifaCorePensionAdicArBW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCorePensionAdicArBW(BigDecimal value) {
        this.tarifaCorePensionAdicArBW = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObliPenAdicArBX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObliPenAdicArBX() {
        return cotizacionObliPenAdicArBX;
    }

    /**
     * Define el valor de la propiedad cotizacionObliPenAdicArBX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObliPenAdicArBX(BigDecimal value) {
        this.cotizacionObliPenAdicArBX = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaBY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaBY() {
        return diasCotizadosPilaBY;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaBY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaBY(Long value) {
        this.diasCotizadosPilaBY = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaBZ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaBZ() {
        return ibcPilaBZ;
    }

    /**
     * Define el valor de la propiedad ibcPilaBZ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaBZ(BigDecimal value) {
        this.ibcPilaBZ = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPensionCA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPensionCA() {
        return tarifaPensionCA;
    }

    /**
     * Define el valor de la propiedad tarifaPensionCA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPensionCA(BigDecimal value) {
        this.tarifaPensionCA = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPensionCB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPensionCB() {
        return cotizacionPagadaPensionCB;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPensionCB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPensionCB(BigDecimal value) {
        this.cotizacionPagadaPensionCB = value;
    }

    /**
     * Obtiene el valor de la propiedad ajustePensionCC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjustePensionCC() {
        return ajustePensionCC;
    }

    /**
     * Define el valor de la propiedad ajustePensionCC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjustePensionCC(BigDecimal value) {
        this.ajustePensionCC = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjustePensionCD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjustePensionCD() {
        return conceptoAjustePensionCD;
    }

    /**
     * Define el valor de la propiedad conceptoAjustePensionCD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjustePensionCD(String value) {
        this.conceptoAjustePensionCD = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaFspCE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaFspCE() {
        return tarifaPilaFspCE;
    }

    /**
     * Define el valor de la propiedad tarifaPilaFspCE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaFspCE(BigDecimal value) {
        this.tarifaPilaFspCE = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaFspCF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaFspCF() {
        return cotizacionPagadaFspCF;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaFspCF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaFspCF(BigDecimal value) {
        this.cotizacionPagadaFspCF = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteFspCG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteFspCG() {
        return ajusteFspCG;
    }

    /**
     * Define el valor de la propiedad ajusteFspCG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteFspCG(BigDecimal value) {
        this.ajusteFspCG = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteFspCH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteFspCH() {
        return conceptoAjusteFspCH;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteFspCH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteFspCH(String value) {
        this.conceptoAjusteFspCH = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaPensiAdicioArCI.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaPensiAdicioArCI() {
        return tarifaPilaPensiAdicioArCI;
    }

    /**
     * Define el valor de la propiedad tarifaPilaPensiAdicioArCI.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaPensiAdicioArCI(BigDecimal value) {
        this.tarifaPilaPensiAdicioArCI = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagPensAdicArCJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagPensAdicArCJ() {
        return cotizacionPagPensAdicArCJ;
    }

    /**
     * Define el valor de la propiedad cotizacionPagPensAdicArCJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagPensAdicArCJ(BigDecimal value) {
        this.cotizacionPagPensAdicArCJ = value;
    }

    /**
     * Obtiene el valor de la propiedad ajustePensionAdicionalArCK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjustePensionAdicionalArCK() {
        return ajustePensionAdicionalArCK;
    }

    /**
     * Define el valor de la propiedad ajustePensionAdicionalArCK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjustePensionAdicionalArCK(BigDecimal value) {
        this.ajustePensionAdicionalArCK = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjustePensionArCL.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjustePensionArCL() {
        return conceptoAjustePensionArCL;
    }

    /**
     * Define el valor de la propiedad conceptoAjustePensionArCL.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjustePensionArCL(String value) {
        this.conceptoAjustePensionArCL = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoCM.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoCM() {
        return tipoIncumplimientoCM;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoCM.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoCM(String value) {
        this.tipoIncumplimientoCM = value;
    }

    /**
     * Obtiene el valor de la propiedad calculoActuarialCN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCalculoActuarialCN() {
        return calculoActuarialCN;
    }

    /**
     * Define el valor de la propiedad calculoActuarialCN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCalculoActuarialCN(BigDecimal value) {
        this.calculoActuarialCN = value;
    }

    /**
     * Obtiene el valor de la propiedad codigoAdministradoraCO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodigoAdministradoraCO() {
        return codigoAdministradoraCO;
    }

    /**
     * Define el valor de la propiedad codigoAdministradoraCO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodigoAdministradoraCO(String value) {
        this.codigoAdministradoraCO = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraCP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraCP() {
        return nombreCortoAdministradoraCP;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraCP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraCP(String value) {
        this.nombreCortoAdministradoraCP = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreCQ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreCQ() {
        return ibcCoreCQ;
    }

    /**
     * Define el valor de la propiedad ibcCoreCQ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreCQ(BigDecimal value) {
        this.ibcCoreCQ = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaCR.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaCR() {
        return cotizacionObligatoriaCR;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaCR.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaCR(BigDecimal value) {
        this.cotizacionObligatoriaCR = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaCS.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaCS() {
        return diasCotizadosPilaCS;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaCS.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaCS(Long value) {
        this.diasCotizadosPilaCS = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaCT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaCT() {
        return ibcPilaCT;
    }

    /**
     * Define el valor de la propiedad ibcPilaCT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaCT(BigDecimal value) {
        this.ibcPilaCT = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaCU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaCU() {
        return tarifaPilaCU;
    }

    /**
     * Define el valor de la propiedad tarifaPilaCU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaCU(BigDecimal value) {
        this.tarifaPilaCU = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaCV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaCV() {
        return cotizacionPagadaPilaCV;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaCV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaCV(BigDecimal value) {
        this.cotizacionPagadaPilaCV = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteCW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteCW() {
        return ajusteCW;
    }

    /**
     * Define el valor de la propiedad ajusteCW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteCW(BigDecimal value) {
        this.ajusteCW = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteCX.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteCX() {
        return conceptoAjusteCX;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteCX.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteCX(String value) {
        this.conceptoAjusteCX = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoCY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoCY() {
        return tipoIncumplimientoCY;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoCY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoCY(String value) {
        this.tipoIncumplimientoCY = value;
    }

    /**
     * Obtiene el valor de la propiedad nombreCortoAdministradoraCZ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreCortoAdministradoraCZ() {
        return nombreCortoAdministradoraCZ;
    }

    /**
     * Define el valor de la propiedad nombreCortoAdministradoraCZ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreCortoAdministradoraCZ(String value) {
        this.nombreCortoAdministradoraCZ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDA() {
        return ibcCoreDA;
    }

    /**
     * Define el valor de la propiedad ibcCoreDA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDA(BigDecimal value) {
        this.ibcCoreDA = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDB() {
        return tarifaCoreDB;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDB(BigDecimal value) {
        this.tarifaCoreDB = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionObligatoriaDC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionObligatoriaDC() {
        return cotizacionObligatoriaDC;
    }

    /**
     * Define el valor de la propiedad cotizacionObligatoriaDC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionObligatoriaDC(BigDecimal value) {
        this.cotizacionObligatoriaDC = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizadosPilaDD.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizadosPilaDD() {
        return diasCotizadosPilaDD;
    }

    /**
     * Define el valor de la propiedad diasCotizadosPilaDD.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizadosPilaDD(Long value) {
        this.diasCotizadosPilaDD = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaDE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcPilaDE() {
        return ibcPilaDE;
    }

    /**
     * Define el valor de la propiedad ibcPilaDE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcPilaDE(BigDecimal value) {
        this.ibcPilaDE = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDF() {
        return tarifaPilaDF;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDF(BigDecimal value) {
        this.tarifaPilaDF = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizacionPagadaPilaDG.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCotizacionPagadaPilaDG() {
        return cotizacionPagadaPilaDG;
    }

    /**
     * Define el valor de la propiedad cotizacionPagadaPilaDG.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCotizacionPagadaPilaDG(BigDecimal value) {
        this.cotizacionPagadaPilaDG = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDH.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDH() {
        return ajusteDH;
    }

    /**
     * Define el valor de la propiedad ajusteDH.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDH(BigDecimal value) {
        this.ajusteDH = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDI.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDI() {
        return conceptoAjusteDI;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDI.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDI(String value) {
        this.conceptoAjusteDI = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDJ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDJ() {
        return tipoIncumplimientoDJ;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDJ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDJ(String value) {
        this.tipoIncumplimientoDJ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDK() {
        return ibcCoreDK;
    }

    /**
     * Define el valor de la propiedad ibcCoreDK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDK(BigDecimal value) {
        this.ibcCoreDK = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDL() {
        return tarifaCoreDL;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDL(BigDecimal value) {
        this.tarifaCoreDL = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteDM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteDM() {
        return aporteDM;
    }

    /**
     * Define el valor de la propiedad aporteDM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteDM(BigDecimal value) {
        this.aporteDM = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDN() {
        return tarifaPilaDN;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDN(BigDecimal value) {
        this.tarifaPilaDN = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaDO.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaDO() {
        return aportePilaDO;
    }

    /**
     * Define el valor de la propiedad aportePilaDO.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaDO(BigDecimal value) {
        this.aportePilaDO = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDP.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDP() {
        return ajusteDP;
    }

    /**
     * Define el valor de la propiedad ajusteDP.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDP(BigDecimal value) {
        this.ajusteDP = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDQ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDQ() {
        return conceptoAjusteDQ;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDQ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDQ(String value) {
        this.conceptoAjusteDQ = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDR.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDR() {
        return tipoIncumplimientoDR;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDR.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDR(String value) {
        this.tipoIncumplimientoDR = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreDS.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreDS() {
        return ibcCoreDS;
    }

    /**
     * Define el valor de la propiedad ibcCoreDS.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreDS(BigDecimal value) {
        this.ibcCoreDS = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreDT.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreDT() {
        return tarifaCoreDT;
    }

    /**
     * Define el valor de la propiedad tarifaCoreDT.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreDT(BigDecimal value) {
        this.tarifaCoreDT = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteDU.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteDU() {
        return aporteDU;
    }

    /**
     * Define el valor de la propiedad aporteDU.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteDU(BigDecimal value) {
        this.aporteDU = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaDV.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaDV() {
        return tarifaPilaDV;
    }

    /**
     * Define el valor de la propiedad tarifaPilaDV.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaDV(BigDecimal value) {
        this.tarifaPilaDV = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaDW.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaDW() {
        return aportePilaDW;
    }

    /**
     * Define el valor de la propiedad aportePilaDW.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaDW(BigDecimal value) {
        this.aportePilaDW = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteDX.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteDX() {
        return ajusteDX;
    }

    /**
     * Define el valor de la propiedad ajusteDX.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteDX(BigDecimal value) {
        this.ajusteDX = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteDY.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteDY() {
        return conceptoAjusteDY;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteDY.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteDY(String value) {
        this.conceptoAjusteDY = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoDZ.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoDZ() {
        return tipoIncumplimientoDZ;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoDZ.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoDZ(String value) {
        this.tipoIncumplimientoDZ = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcEA.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcEA() {
        return ibcEA;
    }

    /**
     * Define el valor de la propiedad ibcEA.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcEA(BigDecimal value) {
        this.ibcEA = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreEB.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreEB() {
        return tarifaCoreEB;
    }

    /**
     * Define el valor de la propiedad tarifaCoreEB.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreEB(BigDecimal value) {
        this.tarifaCoreEB = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEC.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEC() {
        return aporteEC;
    }

    /**
     * Define el valor de la propiedad aporteEC.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEC(BigDecimal value) {
        this.aporteEC = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaED.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaED() {
        return tarifaPilaED;
    }

    /**
     * Define el valor de la propiedad tarifaPilaED.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaED(BigDecimal value) {
        this.tarifaPilaED = value;
    }

    /**
     * Obtiene el valor de la propiedad aportePilaEE.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAportePilaEE() {
        return aportePilaEE;
    }

    /**
     * Define el valor de la propiedad aportePilaEE.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAportePilaEE(BigDecimal value) {
        this.aportePilaEE = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteEF.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteEF() {
        return ajusteEF;
    }

    /**
     * Define el valor de la propiedad ajusteEF.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteEF(BigDecimal value) {
        this.ajusteEF = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteEG.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteEG() {
        return conceptoAjusteEG;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteEG.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteEG(String value) {
        this.conceptoAjusteEG = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoEH.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoEH() {
        return tipoIncumplimientoEH;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoEH.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoEH(String value) {
        this.tipoIncumplimientoEH = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCoreEI.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIbcCoreEI() {
        return ibcCoreEI;
    }

    /**
     * Define el valor de la propiedad ibcCoreEI.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIbcCoreEI(BigDecimal value) {
        this.ibcCoreEI = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCoreEJ.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCoreEJ() {
        return tarifaCoreEJ;
    }

    /**
     * Define el valor de la propiedad tarifaCoreEJ.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCoreEJ(BigDecimal value) {
        this.tarifaCoreEJ = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEK.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEK() {
        return aporteEK;
    }

    /**
     * Define el valor de la propiedad aporteEK.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEK(BigDecimal value) {
        this.aporteEK = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaEL.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaEL() {
        return tarifaPilaEL;
    }

    /**
     * Define el valor de la propiedad tarifaPilaEL.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaEL(BigDecimal value) {
        this.tarifaPilaEL = value;
    }

    /**
     * Obtiene el valor de la propiedad aporteEM.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAporteEM() {
        return aporteEM;
    }

    /**
     * Define el valor de la propiedad aporteEM.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAporteEM(BigDecimal value) {
        this.aporteEM = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteEN.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAjusteEN() {
        return ajusteEN;
    }

    /**
     * Define el valor de la propiedad ajusteEN.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAjusteEN(BigDecimal value) {
        this.ajusteEN = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotSalud() {
        return diasCotSalud;
    }

    /**
     * Define el valor de la propiedad diasCotSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotSalud(Long value) {
        this.diasCotSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotRprof.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotRprof() {
        return diasCotRprof;
    }

    /**
     * Define el valor de la propiedad diasCotRprof.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotRprof(Long value) {
        this.diasCotRprof = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotCcf() {
        return diasCotCcf;
    }

    /**
     * Define el valor de la propiedad diasCotCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotCcf(Long value) {
        this.diasCotCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotPension() {
        return diasCotPension;
    }

    /**
     * Define el valor de la propiedad diasCotPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotPension(Long value) {
        this.diasCotPension = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotiPilaCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotiPilaCcf() {
        return diasCotiPilaCcf;
    }

    /**
     * Define el valor de la propiedad diasCotiPilaCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotiPilaCcf(Long value) {
        this.diasCotiPilaCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad planillaPilaCargada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getPlanillaPilaCargada() {
        return planillaPilaCargada;
    }

    /**
     * Define el valor de la propiedad planillaPilaCargada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlanillaPilaCargada(Long value) {
        this.planillaPilaCargada = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizPilaSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizPilaSalud() {
        return diasCotizPilaSalud;
    }

    /**
     * Define el valor de la propiedad diasCotizPilaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizPilaSalud(Long value) {
        this.diasCotizPilaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizPilaPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizPilaPension() {
        return diasCotizPilaPension;
    }

    /**
     * Define el valor de la propiedad diasCotizPilaPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizPilaPension(Long value) {
        this.diasCotizPilaPension = value;
    }

    /**
     * Obtiene el valor de la propiedad diasCotizPilaArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getDiasCotizPilaArl() {
        return diasCotizPilaArl;
    }

    /**
     * Define el valor de la propiedad diasCotizPilaArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasCotizPilaArl(Long value) {
        this.diasCotizPilaArl = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPermisosRemunerados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPermisosRemunerados() {
        return ibcPermisosRemunerados;
    }

    /**
     * Define el valor de la propiedad ibcPermisosRemunerados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPermisosRemunerados(Long value) {
        this.ibcPermisosRemunerados = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcSuspPermisos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcSuspPermisos() {
        return ibcSuspPermisos;
    }

    /**
     * Define el valor de la propiedad ibcSuspPermisos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcSuspPermisos(Long value) {
        this.ibcSuspPermisos = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcVacaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcVacaciones() {
        return ibcVacaciones;
    }

    /**
     * Define el valor de la propiedad ibcVacaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcVacaciones(Long value) {
        this.ibcVacaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcHuelga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcHuelga() {
        return ibcHuelga;
    }

    /**
     * Define el valor de la propiedad ibcHuelga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcHuelga(Long value) {
        this.ibcHuelga = value;
    }

    /**
     * Obtiene el valor de la propiedad pagoNoSalarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getPagoNoSalarial() {
        return pagoNoSalarial;
    }

    /**
     * Define el valor de la propiedad pagoNoSalarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPagoNoSalarial(Long value) {
        this.pagoNoSalarial = value;
    }

    /**
     * Obtiene el valor de la propiedad totalRemunerado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getTotalRemunerado() {
        return totalRemunerado;
    }

    /**
     * Define el valor de la propiedad totalRemunerado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalRemunerado(Long value) {
        this.totalRemunerado = value;
    }

    /**
     * Obtiene el valor de la propiedad porPagoNoSalarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getPorPagoNoSalarial() {
        return porPagoNoSalarial;
    }

    /**
     * Define el valor de la propiedad porPagoNoSalarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorPagoNoSalarial(Long value) {
        this.porPagoNoSalarial = value;
    }

    /**
     * Obtiene el valor de la propiedad excLimPagoNoSalarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getExcLimPagoNoSalarial() {
        return excLimPagoNoSalarial;
    }

    /**
     * Define el valor de la propiedad excLimPagoNoSalarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExcLimPagoNoSalarial(Long value) {
        this.excLimPagoNoSalarial = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPagosNomSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPagosNomSalud() {
        return ibcPagosNomSalud;
    }

    /**
     * Define el valor de la propiedad ibcPagosNomSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPagosNomSalud(Long value) {
        this.ibcPagosNomSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCalculadoSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcCalculadoSalud() {
        return ibcCalculadoSalud;
    }

    /**
     * Define el valor de la propiedad ibcCalculadoSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcCalculadoSalud(Long value) {
        this.ibcCalculadoSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaSalud.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaSalud() {
        return tarifaSalud;
    }

    /**
     * Define el valor de la propiedad tarifaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaSalud(BigDecimal value) {
        this.tarifaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaSaludSuspension.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaSaludSuspension() {
        return tarifaSaludSuspension;
    }

    /**
     * Define el valor de la propiedad tarifaSaludSuspension.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaSaludSuspension(BigDecimal value) {
        this.tarifaSaludSuspension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblCalculadaSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblCalculadaSalud() {
        return cotizOblCalculadaSalud;
    }

    /**
     * Define el valor de la propiedad cotizOblCalculadaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblCalculadaSalud(Long value) {
        this.cotizOblCalculadaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPilaSalud() {
        return ibcPilaSalud;
    }

    /**
     * Define el valor de la propiedad ibcPilaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPilaSalud(Long value) {
        this.ibcPilaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaSalud.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaSalud() {
        return tarifaPilaSalud;
    }

    /**
     * Define el valor de la propiedad tarifaPilaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaSalud(BigDecimal value) {
        this.tarifaPilaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagadaPilaSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagadaPilaSalud() {
        return cotizPagadaPilaSalud;
    }

    /**
     * Define el valor de la propiedad cotizPagadaPilaSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagadaPilaSalud(Long value) {
        this.cotizPagadaPilaSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteSalud() {
        return ajusteSalud;
    }

    /**
     * Define el valor de la propiedad ajusteSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteSalud(Long value) {
        this.ajusteSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPagosNomPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPagosNomPension() {
        return ibcPagosNomPension;
    }

    /**
     * Define el valor de la propiedad ibcPagosNomPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPagosNomPension(Long value) {
        this.ibcPagosNomPension = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCalculadoPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcCalculadoPension() {
        return ibcCalculadoPension;
    }

    /**
     * Define el valor de la propiedad ibcCalculadoPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcCalculadoPension(Long value) {
        this.ibcCalculadoPension = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPension.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPension() {
        return tarifaPension;
    }

    /**
     * Define el valor de la propiedad tarifaPension.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPension(BigDecimal value) {
        this.tarifaPension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblPension() {
        return cotizOblPension;
    }

    /**
     * Define el valor de la propiedad cotizOblPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblPension(Long value) {
        this.cotizOblPension = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPilaPension() {
        return ibcPilaPension;
    }

    /**
     * Define el valor de la propiedad ibcPilaPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPilaPension(Long value) {
        this.ibcPilaPension = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaPension.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaPension() {
        return tarifaPilaPension;
    }

    /**
     * Define el valor de la propiedad tarifaPilaPension.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaPension(BigDecimal value) {
        this.tarifaPilaPension = value;
    }

    /**
     * Obtiene el valor de la propiedad cotPagadaPilaPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotPagadaPilaPension() {
        return cotPagadaPilaPension;
    }

    /**
     * Define el valor de la propiedad cotPagadaPilaPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotPagadaPilaPension(Long value) {
        this.cotPagadaPilaPension = value;
    }

    /**
     * Obtiene el valor de la propiedad ajustePension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjustePension() {
        return ajustePension;
    }

    /**
     * Define el valor de la propiedad ajustePension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjustePension(Long value) {
        this.ajustePension = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaFspSubcuenSolidaridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getTarifaFspSubcuenSolidaridad() {
        return tarifaFspSubcuenSolidaridad;
    }

    /**
     * Define el valor de la propiedad tarifaFspSubcuenSolidaridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarifaFspSubcuenSolidaridad(Long value) {
        this.tarifaFspSubcuenSolidaridad = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaFspSubcuenSubsisten.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getTarifaFspSubcuenSubsisten() {
        return tarifaFspSubcuenSubsisten;
    }

    /**
     * Define el valor de la propiedad tarifaFspSubcuenSubsisten.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarifaFspSubcuenSubsisten(Long value) {
        this.tarifaFspSubcuenSubsisten = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblFspSubSolidaridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblFspSubSolidaridad() {
        return cotizOblFspSubSolidaridad;
    }

    /**
     * Define el valor de la propiedad cotizOblFspSubSolidaridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblFspSubSolidaridad(Long value) {
        this.cotizOblFspSubSolidaridad = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblFspSubSubsistencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblFspSubSubsistencia() {
        return cotizOblFspSubSubsistencia;
    }

    /**
     * Define el valor de la propiedad cotizOblFspSubSubsistencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblFspSubSubsistencia(Long value) {
        this.cotizOblFspSubSubsistencia = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagPilaFspSubSolidar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagPilaFspSubSolidar() {
        return cotizPagPilaFspSubSolidar;
    }

    /**
     * Define el valor de la propiedad cotizPagPilaFspSubSolidar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagPilaFspSubSolidar(Long value) {
        this.cotizPagPilaFspSubSolidar = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagPilaFspSubSubsis.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagPilaFspSubSubsis() {
        return cotizPagPilaFspSubSubsis;
    }

    /**
     * Define el valor de la propiedad cotizPagPilaFspSubSubsis.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagPilaFspSubSubsis(Long value) {
        this.cotizPagPilaFspSubSubsis = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteFspSubcuenSubsisten.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteFspSubcuenSubsisten() {
        return ajusteFspSubcuenSubsisten;
    }

    /**
     * Define el valor de la propiedad ajusteFspSubcuenSubsisten.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteFspSubcuenSubsisten(Long value) {
        this.ajusteFspSubcuenSubsisten = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteFspSubcuenSolidaridad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteFspSubcuenSolidaridad() {
        return ajusteFspSubcuenSolidaridad;
    }

    /**
     * Define el valor de la propiedad ajusteFspSubcuenSolidaridad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteFspSubcuenSolidaridad(Long value) {
        this.ajusteFspSubcuenSolidaridad = value;
    }

    /**
     * Obtiene el valor de la propiedad calculoActuarial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCalculoActuarial() {
        return calculoActuarial;
    }

    /**
     * Define el valor de la propiedad calculoActuarial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculoActuarial(Long value) {
        this.calculoActuarial = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcArl() {
        return ibcArl;
    }

    /**
     * Define el valor de la propiedad ibcArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcArl(Long value) {
        this.ibcArl = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaArl.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaArl() {
        return tarifaArl;
    }

    /**
     * Define el valor de la propiedad tarifaArl.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaArl(BigDecimal value) {
        this.tarifaArl = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblArl() {
        return cotizOblArl;
    }

    /**
     * Define el valor de la propiedad cotizOblArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblArl(Long value) {
        this.cotizOblArl = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPilaArl() {
        return ibcPilaArl;
    }

    /**
     * Define el valor de la propiedad ibcPilaArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPilaArl(Long value) {
        this.ibcPilaArl = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaArl.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaArl() {
        return tarifaPilaArl;
    }

    /**
     * Define el valor de la propiedad tarifaPilaArl.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaArl(BigDecimal value) {
        this.tarifaPilaArl = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagadaPilaArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagadaPilaArl() {
        return cotizPagadaPilaArl;
    }

    /**
     * Define el valor de la propiedad cotizPagadaPilaArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagadaPilaArl(Long value) {
        this.cotizPagadaPilaArl = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteArl() {
        return ajusteArl;
    }

    /**
     * Define el valor de la propiedad ajusteArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteArl(Long value) {
        this.ajusteArl = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcCcf() {
        return ibcCcf;
    }

    /**
     * Define el valor de la propiedad ibcCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcCcf(Long value) {
        this.ibcCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaCcf.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaCcf() {
        return tarifaCcf;
    }

    /**
     * Define el valor de la propiedad tarifaCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaCcf(BigDecimal value) {
        this.tarifaCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblCcf() {
        return cotizOblCcf;
    }

    /**
     * Define el valor de la propiedad cotizOblCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblCcf(Long value) {
        this.cotizOblCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcPilaCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcPilaCcf() {
        return ibcPilaCcf;
    }

    /**
     * Define el valor de la propiedad ibcPilaCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcPilaCcf(Long value) {
        this.ibcPilaCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaCcf.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaCcf() {
        return tarifaPilaCcf;
    }

    /**
     * Define el valor de la propiedad tarifaPilaCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaCcf(BigDecimal value) {
        this.tarifaPilaCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagadaPilaCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagadaPilaCcf() {
        return cotizPagadaPilaCcf;
    }

    /**
     * Define el valor de la propiedad cotizPagadaPilaCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagadaPilaCcf(Long value) {
        this.cotizPagadaPilaCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteCcf() {
        return ajusteCcf;
    }

    /**
     * Define el valor de la propiedad ajusteCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteCcf(Long value) {
        this.ajusteCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcSena() {
        return ibcSena;
    }

    /**
     * Define el valor de la propiedad ibcSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcSena(Long value) {
        this.ibcSena = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaSena.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaSena() {
        return tarifaSena;
    }

    /**
     * Define el valor de la propiedad tarifaSena.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaSena(BigDecimal value) {
        this.tarifaSena = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblSena() {
        return cotizOblSena;
    }

    /**
     * Define el valor de la propiedad cotizOblSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblSena(Long value) {
        this.cotizOblSena = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaSena.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaSena() {
        return tarifaPilaSena;
    }

    /**
     * Define el valor de la propiedad tarifaPilaSena.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaSena(BigDecimal value) {
        this.tarifaPilaSena = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagadaPilaSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagadaPilaSena() {
        return cotizPagadaPilaSena;
    }

    /**
     * Define el valor de la propiedad cotizPagadaPilaSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagadaPilaSena(Long value) {
        this.cotizPagadaPilaSena = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteSena() {
        return ajusteSena;
    }

    /**
     * Define el valor de la propiedad ajusteSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteSena(Long value) {
        this.ajusteSena = value;
    }

    /**
     * Obtiene el valor de la propiedad ibcIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getIbcIcbf() {
        return ibcIcbf;
    }

    /**
     * Define el valor de la propiedad ibcIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIbcIcbf(Long value) {
        this.ibcIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaIcbf.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaIcbf() {
        return tarifaIcbf;
    }

    /**
     * Define el valor de la propiedad tarifaIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaIcbf(BigDecimal value) {
        this.tarifaIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizOblIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizOblIcbf() {
        return cotizOblIcbf;
    }

    /**
     * Define el valor de la propiedad cotizOblIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizOblIcbf(Long value) {
        this.cotizOblIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad tarifaPilaIcbf.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTarifaPilaIcbf() {
        return tarifaPilaIcbf;
    }

    /**
     * Define el valor de la propiedad tarifaPilaIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTarifaPilaIcbf(BigDecimal value) {
        this.tarifaPilaIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad cotizPagadaPilaIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getCotizPagadaPilaIcbf() {
        return cotizPagadaPilaIcbf;
    }

    /**
     * Define el valor de la propiedad cotizPagadaPilaIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCotizPagadaPilaIcbf(Long value) {
        this.cotizPagadaPilaIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad ajusteIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Long getAjusteIcbf() {
        return ajusteIcbf;
    }

    /**
     * Define el valor de la propiedad ajusteIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAjusteIcbf(Long value) {
        this.ajusteIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad codAdmSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAdmSalud() {
        return codAdmSalud;
    }

    /**
     * Define el valor de la propiedad codAdmSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAdmSalud(String value) {
        this.codAdmSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad codAdmArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAdmArl() {
        return codAdmArl;
    }

    /**
     * Define el valor de la propiedad codAdmArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAdmArl(String value) {
        this.codAdmArl = value;
    }

    /**
     * Obtiene el valor de la propiedad codAdmCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAdmCcf() {
        return codAdmCcf;
    }

    /**
     * Define el valor de la propiedad codAdmCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAdmCcf(String value) {
        this.codAdmCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteSalud() {
        return conceptoAjusteSalud;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteSalud(String value) {
        this.conceptoAjusteSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjustePension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjustePension() {
        return conceptoAjustePension;
    }

    /**
     * Define el valor de la propiedad conceptoAjustePension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjustePension(String value) {
        this.conceptoAjustePension = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoPension.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoPension() {
        return tipoIncumplimientoPension;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoPension.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoPension(String value) {
        this.tipoIncumplimientoPension = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteFsp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteFsp() {
        return conceptoAjusteFsp;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteFsp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteFsp(String value) {
        this.conceptoAjusteFsp = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoFsp.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoFsp() {
        return tipoIncumplimientoFsp;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoFsp.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoFsp(String value) {
        this.tipoIncumplimientoFsp = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteArl() {
        return conceptoAjusteArl;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteArl(String value) {
        this.conceptoAjusteArl = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoSalud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoSalud() {
        return tipoIncumplimientoSalud;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoSalud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoSalud(String value) {
        this.tipoIncumplimientoSalud = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoArl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoArl() {
        return tipoIncumplimientoArl;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoArl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoArl(String value) {
        this.tipoIncumplimientoArl = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteCcf() {
        return conceptoAjusteCcf;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteCcf(String value) {
        this.conceptoAjusteCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoCcf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoCcf() {
        return tipoIncumplimientoCcf;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoCcf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoCcf(String value) {
        this.tipoIncumplimientoCcf = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteSena() {
        return conceptoAjusteSena;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteSena(String value) {
        this.conceptoAjusteSena = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoSena.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoSena() {
        return tipoIncumplimientoSena;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoSena.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoSena(String value) {
        this.tipoIncumplimientoSena = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoIcbf() {
        return tipoIncumplimientoIcbf;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoIcbf(String value) {
        this.tipoIncumplimientoIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteIcbf.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteIcbf() {
        return conceptoAjusteIcbf;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteIcbf.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteIcbf(String value) {
        this.conceptoAjusteIcbf = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoAjusteEO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoAjusteEO() {
        return conceptoAjusteEO;
    }

    /**
     * Define el valor de la propiedad conceptoAjusteEO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoAjusteEO(String value) {
        this.conceptoAjusteEO = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoIncumplimientoEP.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoIncumplimientoEP() {
        return tipoIncumplimientoEP;
    }

    /**
     * Define el valor de la propiedad tipoIncumplimientoEP.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoIncumplimientoEP(String value) {
        this.tipoIncumplimientoEP = value;
    }

    /**
     * Gets the value of the conceptoContableTipo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the conceptoContableTipo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConceptoContableTipo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConceptoContableTipo }
     * 
     * 
     */
    public List<ConceptoContableTipo> getConceptoContableTipo() {
        if (conceptoContableTipo == null) {
            conceptoContableTipo = new ArrayList<ConceptoContableTipo>();
        }
        return this.conceptoContableTipo;
    }

    /**
     * Gets the value of the aportesIndependienteTipo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the aportesIndependienteTipo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAportesIndependienteTipo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AportesIndependienteTipo }
     * 
     * 
     */
    public List<AportesIndependienteTipo> getAportesIndependienteTipo() {
        if (aportesIndependienteTipo == null) {
            aportesIndependienteTipo = new ArrayList<AportesIndependienteTipo>();
        }
        return this.aportesIndependienteTipo;
    }

    /**
     * Obtiene el valor de la propiedad contextoRespuestaTipo.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuestaTipo() {
        return contextoRespuestaTipo;
    }

    /**
     * Define el valor de la propiedad contextoRespuestaTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuestaTipo(ContextoRespuestaTipo value) {
        this.contextoRespuestaTipo = value;
    }

}
