
package co.gov.ugpp.schema.liquidaciones.liquidaciontipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;


/**
 * <p>Clase Java para LiquidacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LiquidacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="esAmpliacionRequerimientos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esDeterminaDeuda" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="liquidacionOficialParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="liquidacionOficialActo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *         &lt;element name="autoArchivoParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="autoArchivo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *         &lt;element name="constanciaEjecutoria" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="esConfirmacionRecursos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esTodosEmpleadosAfiliados" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esTieneBienes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oficioBienesParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="oficioBienes" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="oficioAfiliacionesParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="oficioAfiliaciones" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idRequerimientoDeclararCorregir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecNotificacionRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esTieneCausalAutoArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalAutoArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LiquidacionTipo", propOrder = {
    "idLiquidacion",
    "expediente",
    "esAmpliacionRequerimientos",
    "esDeterminaDeuda",
    "aportante",
    "liquidacionOficialParcial",
    "liquidacionOficialActo",
    "autoArchivoParcial",
    "autoArchivo",
    "constanciaEjecutoria",
    "esConfirmacionRecursos",
    "esTodosEmpleadosAfiliados",
    "esTieneBienes",
    "oficioBienesParcial",
    "oficioBienes",
    "oficioAfiliacionesParcial",
    "oficioAfiliaciones",
    "idRequerimientoDeclararCorregir",
    "fecNotificacionRequerimiento",
    "esTieneCausalAutoArchivo",
    "codCausalAutoArchivo"
})
public class LiquidacionTipo {

    @XmlElement(nillable = true)
    protected String idLiquidacion;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String esAmpliacionRequerimientos;
    @XmlElement(nillable = true)
    protected String esDeterminaDeuda;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo liquidacionOficialParcial;
    @XmlElement(nillable = true)
    protected ActoAdministrativoTipo liquidacionOficialActo;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoArchivoParcial;
    @XmlElement(nillable = true)
    protected ActoAdministrativoTipo autoArchivo;
    @XmlElement(nillable = true)
    protected DocumentoTipo constanciaEjecutoria;
    @XmlElement(nillable = true)
    protected String esConfirmacionRecursos;
    @XmlElement(nillable = true)
    protected String esTodosEmpleadosAfiliados;
    @XmlElement(nillable = true)
    protected String esTieneBienes;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo oficioBienesParcial;
    @XmlElement(nillable = true)
    protected DocumentoTipo oficioBienes;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo oficioAfiliacionesParcial;
    @XmlElement(nillable = true)
    protected DocumentoTipo oficioAfiliaciones;
    @XmlElement(nillable = true)
    protected String idRequerimientoDeclararCorregir;
    @XmlElement(nillable = true)
    protected String fecNotificacionRequerimiento;
    @XmlElement(nillable = true)
    protected String esTieneCausalAutoArchivo;
    @XmlElement(nillable = true)
    protected String codCausalAutoArchivo;

    /**
     * Obtiene el valor de la propiedad idLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidacion() {
        return idLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidacion(String value) {
        this.idLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad esAmpliacionRequerimientos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAmpliacionRequerimientos() {
        return esAmpliacionRequerimientos;
    }

    /**
     * Define el valor de la propiedad esAmpliacionRequerimientos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAmpliacionRequerimientos(String value) {
        this.esAmpliacionRequerimientos = value;
    }

    /**
     * Obtiene el valor de la propiedad esDeterminaDeuda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDeterminaDeuda() {
        return esDeterminaDeuda;
    }

    /**
     * Define el valor de la propiedad esDeterminaDeuda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDeterminaDeuda(String value) {
        this.esDeterminaDeuda = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidacionOficialParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getLiquidacionOficialParcial() {
        return liquidacionOficialParcial;
    }

    /**
     * Define el valor de la propiedad liquidacionOficialParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setLiquidacionOficialParcial(AprobacionDocumentoTipo value) {
        this.liquidacionOficialParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad liquidacionOficialActo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getLiquidacionOficialActo() {
        return liquidacionOficialActo;
    }

    /**
     * Define el valor de la propiedad liquidacionOficialActo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setLiquidacionOficialActo(ActoAdministrativoTipo value) {
        this.liquidacionOficialActo = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivoParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    /**
     * Define el valor de la propiedad autoArchivoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoArchivoParcial(AprobacionDocumentoTipo value) {
        this.autoArchivoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getAutoArchivo() {
        return autoArchivo;
    }

    /**
     * Define el valor de la propiedad autoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setAutoArchivo(ActoAdministrativoTipo value) {
        this.autoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getConstanciaEjecutoria() {
        return constanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad constanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setConstanciaEjecutoria(DocumentoTipo value) {
        this.constanciaEjecutoria = value;
    }

    /**
     * Obtiene el valor de la propiedad esConfirmacionRecursos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConfirmacionRecursos() {
        return esConfirmacionRecursos;
    }

    /**
     * Define el valor de la propiedad esConfirmacionRecursos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConfirmacionRecursos(String value) {
        this.esConfirmacionRecursos = value;
    }

    /**
     * Obtiene el valor de la propiedad esTodosEmpleadosAfiliados.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTodosEmpleadosAfiliados() {
        return esTodosEmpleadosAfiliados;
    }

    /**
     * Define el valor de la propiedad esTodosEmpleadosAfiliados.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTodosEmpleadosAfiliados(String value) {
        this.esTodosEmpleadosAfiliados = value;
    }

    /**
     * Obtiene el valor de la propiedad esTieneBienes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTieneBienes() {
        return esTieneBienes;
    }

    /**
     * Define el valor de la propiedad esTieneBienes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTieneBienes(String value) {
        this.esTieneBienes = value;
    }

    /**
     * Obtiene el valor de la propiedad oficioBienesParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getOficioBienesParcial() {
        return oficioBienesParcial;
    }

    /**
     * Define el valor de la propiedad oficioBienesParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setOficioBienesParcial(AprobacionDocumentoTipo value) {
        this.oficioBienesParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad oficioBienes.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getOficioBienes() {
        return oficioBienes;
    }

    /**
     * Define el valor de la propiedad oficioBienes.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setOficioBienes(DocumentoTipo value) {
        this.oficioBienes = value;
    }

    /**
     * Obtiene el valor de la propiedad oficioAfiliacionesParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getOficioAfiliacionesParcial() {
        return oficioAfiliacionesParcial;
    }

    /**
     * Define el valor de la propiedad oficioAfiliacionesParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setOficioAfiliacionesParcial(AprobacionDocumentoTipo value) {
        this.oficioAfiliacionesParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad oficioAfiliaciones.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getOficioAfiliaciones() {
        return oficioAfiliaciones;
    }

    /**
     * Define el valor de la propiedad oficioAfiliaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setOficioAfiliaciones(DocumentoTipo value) {
        this.oficioAfiliaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad idRequerimientoDeclararCorregir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRequerimientoDeclararCorregir() {
        return idRequerimientoDeclararCorregir;
    }

    /**
     * Define el valor de la propiedad idRequerimientoDeclararCorregir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRequerimientoDeclararCorregir(String value) {
        this.idRequerimientoDeclararCorregir = value;
    }

    /**
     * Obtiene el valor de la propiedad fecNotificacionRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFecNotificacionRequerimiento() {
        return fecNotificacionRequerimiento;
    }

    /**
     * Define el valor de la propiedad fecNotificacionRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecNotificacionRequerimiento(String value) {
        this.fecNotificacionRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad esTieneCausalAutoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsTieneCausalAutoArchivo() {
        return esTieneCausalAutoArchivo;
    }

    /**
     * Define el valor de la propiedad esTieneCausalAutoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsTieneCausalAutoArchivo(String value) {
        this.esTieneCausalAutoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalAutoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalAutoArchivo() {
        return codCausalAutoArchivo;
    }

    /**
     * Define el valor de la propiedad codCausalAutoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalAutoArchivo(String value) {
        this.codCausalAutoArchivo = value;
    }

}
