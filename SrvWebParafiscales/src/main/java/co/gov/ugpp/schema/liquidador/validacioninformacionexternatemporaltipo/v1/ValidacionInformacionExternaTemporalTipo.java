
package co.gov.ugpp.schema.liquidador.validacioninformacionexternatemporaltipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.errortipo.v1.ErrorTipo;


/**
 * <p>Clase Java para ValidacionInformacionExternaTemporalTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionInformacionExternaTemporalTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idArchivo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idEjecucion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="errorTipo" type="{http://www.ugpp.gov.co/esb/schema/ErrorTipo/v1}ErrorTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="infoNegocio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="estadoValidacion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionInformacionExternaTemporalTipo", propOrder = {
    "idArchivo",
    "idEjecucion",
    "errorTipo",
    "infoNegocio",
    "estadoValidacion"
})
public class ValidacionInformacionExternaTemporalTipo {

    @XmlElement(required = true, nillable = true)
    protected String idArchivo;
    @XmlElement(required = true, nillable = true)
    protected String idEjecucion;
    @XmlElement(nillable = true)
    protected List<ErrorTipo> errorTipo;
    @XmlElement(required = true, nillable = true)
    protected String infoNegocio;
    @XmlElement(required = true, nillable = true)
    protected String estadoValidacion;

    /**
     * Obtiene el valor de la propiedad idArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivo() {
        return idArchivo;
    }

    /**
     * Define el valor de la propiedad idArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivo(String value) {
        this.idArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad idEjecucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEjecucion() {
        return idEjecucion;
    }

    /**
     * Define el valor de la propiedad idEjecucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEjecucion(String value) {
        this.idEjecucion = value;
    }

    /**
     * Gets the value of the errorTipo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorTipo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorTipo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ErrorTipo }
     * 
     * 
     */
    public List<ErrorTipo> getErrorTipo() {
        if (errorTipo == null) {
            errorTipo = new ArrayList<ErrorTipo>();
        }
        return this.errorTipo;
    }

    /**
     * Obtiene el valor de la propiedad infoNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfoNegocio() {
        return infoNegocio;
    }

    /**
     * Define el valor de la propiedad infoNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfoNegocio(String value) {
        this.infoNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad estadoValidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstadoValidacion() {
        return estadoValidacion;
    }

    /**
     * Define el valor de la propiedad estadoValidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstadoValidacion(String value) {
        this.estadoValidacion = value;
    }

}
