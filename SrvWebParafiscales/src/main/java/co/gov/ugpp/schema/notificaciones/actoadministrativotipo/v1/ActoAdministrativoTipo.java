
package co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.notificaciones.validacionactoadministrativotipo.v1.ValidacionActoAdministrativoTipo;


/**
 * <p>Clase Java para ActoAdministrativoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ActoAdministrativoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codTipoActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="fecActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descParteResolutivaActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="valVigencia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="validaciones" type="{http://www.ugpp.gov.co/schema/Notificaciones/ValidacionActoAdministrativoTipo/v1}ValidacionActoAdministrativoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codAreaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descAreaOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ActoAdministrativoTipo", propOrder = {
    "codTipoActoAdministrativo",
    "idActoAdministrativo",
    "fecActoAdministrativo",
    "descParteResolutivaActoAdministrativo",
    "expediente",
    "documento",
    "valVigencia",
    "validaciones",
    "codAreaOrigen",
    "descAreaOrigen"
})
public class ActoAdministrativoTipo {

    @XmlElement(nillable = true)
    protected String codTipoActoAdministrativo;
    @XmlElement(required = true)
    protected String idActoAdministrativo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecActoAdministrativo;
    @XmlElement(nillable = true)
    protected String descParteResolutivaActoAdministrativo;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected DocumentoTipo documento;
    @XmlElement(required = true, nillable = true)
    protected String valVigencia;
    @XmlElement(nillable = true)
    protected List<ValidacionActoAdministrativoTipo> validaciones;
    @XmlElement(nillable = true)
    protected String codAreaOrigen;
    @XmlElement(nillable = true)
    protected String descAreaOrigen;

    /**
     * Obtiene el valor de la propiedad codTipoActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoActoAdministrativo() {
        return codTipoActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad codTipoActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoActoAdministrativo(String value) {
        this.codTipoActoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAdministrativo() {
        return idActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad idActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAdministrativo(String value) {
        this.idActoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecActoAdministrativo() {
        return fecActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad fecActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecActoAdministrativo(Calendar value) {
        this.fecActoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad descParteResolutivaActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescParteResolutivaActoAdministrativo() {
        return descParteResolutivaActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad descParteResolutivaActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescParteResolutivaActoAdministrativo(String value) {
        this.descParteResolutivaActoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocumento(DocumentoTipo value) {
        this.documento = value;
    }

    /**
     * Obtiene el valor de la propiedad valVigencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValVigencia() {
        return valVigencia;
    }

    /**
     * Define el valor de la propiedad valVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValVigencia(String value) {
        this.valVigencia = value;
    }

    /**
     * Gets the value of the validaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidacionActoAdministrativoTipo }
     * 
     * 
     */
    public List<ValidacionActoAdministrativoTipo> getValidaciones() {
        if (validaciones == null) {
            validaciones = new ArrayList<ValidacionActoAdministrativoTipo>();
        }
        return this.validaciones;
    }

    /**
     * Obtiene el valor de la propiedad codAreaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAreaOrigen() {
        return codAreaOrigen;
    }

    /**
     * Define el valor de la propiedad codAreaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAreaOrigen(String value) {
        this.codAreaOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad descAreaOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAreaOrigen() {
        return descAreaOrigen;
    }

    /**
     * Define el valor de la propiedad descAreaOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAreaOrigen(String value) {
        this.descAreaOrigen = value;
    }

}
