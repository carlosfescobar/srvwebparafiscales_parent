
package co.gov.ugpp.schema.notificaciones.controlenviocomunicaciontipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;


/**
 * <p>Clase Java para ControlEnvioComunicacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ControlEnvioComunicacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idControlEnvioComunicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notificacion" type="{http://www.ugpp.gov.co/schema/Notificaciones/NotificacionTipo/v1}NotificacionTipo" minOccurs="0"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesRechazo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesCorreccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRechazo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecCorreccion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idUsuarioRechaza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuarioCorrige" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlEnvioComunicacionTipo", propOrder = {
    "idControlEnvioComunicacion",
    "notificacion",
    "codEstado",
    "descEstado",
    "descObservacionesRechazo",
    "descObservacionesCorreccion",
    "fecRechazo",
    "fecCorreccion",
    "idUsuarioRechaza",
    "idUsuarioCorrige"
})
public class ControlEnvioComunicacionTipo {

    @XmlElement(nillable = true)
    protected String idControlEnvioComunicacion;
    @XmlElement(nillable = true)
    protected NotificacionTipo notificacion;
    @XmlElement(nillable = true)
    protected String codEstado;
    @XmlElement(nillable = true)
    protected String descEstado;
    @XmlElement(nillable = true)
    protected String descObservacionesRechazo;
    @XmlElement(nillable = true)
    protected String descObservacionesCorreccion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRechazo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecCorreccion;
    @XmlElement(nillable = true)
    protected String idUsuarioRechaza;
    @XmlElement(nillable = true)
    protected String idUsuarioCorrige;

    /**
     * Obtiene el valor de la propiedad idControlEnvioComunicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdControlEnvioComunicacion() {
        return idControlEnvioComunicacion;
    }

    /**
     * Define el valor de la propiedad idControlEnvioComunicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdControlEnvioComunicacion(String value) {
        this.idControlEnvioComunicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad notificacion.
     * 
     * @return
     *     possible object is
     *     {@link NotificacionTipo }
     *     
     */
    public NotificacionTipo getNotificacion() {
        return notificacion;
    }

    /**
     * Define el valor de la propiedad notificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link NotificacionTipo }
     *     
     */
    public void setNotificacion(NotificacionTipo value) {
        this.notificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstado() {
        return descEstado;
    }

    /**
     * Define el valor de la propiedad descEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstado(String value) {
        this.descEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesRechazo() {
        return descObservacionesRechazo;
    }

    /**
     * Define el valor de la propiedad descObservacionesRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesRechazo(String value) {
        this.descObservacionesRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesCorreccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesCorreccion() {
        return descObservacionesCorreccion;
    }

    /**
     * Define el valor de la propiedad descObservacionesCorreccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesCorreccion(String value) {
        this.descObservacionesCorreccion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRechazo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRechazo() {
        return fecRechazo;
    }

    /**
     * Define el valor de la propiedad fecRechazo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRechazo(Calendar value) {
        this.fecRechazo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecCorreccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecCorreccion() {
        return fecCorreccion;
    }

    /**
     * Define el valor de la propiedad fecCorreccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecCorreccion(Calendar value) {
        this.fecCorreccion = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuarioRechaza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuarioRechaza() {
        return idUsuarioRechaza;
    }

    /**
     * Define el valor de la propiedad idUsuarioRechaza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuarioRechaza(String value) {
        this.idUsuarioRechaza = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuarioCorrige.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuarioCorrige() {
        return idUsuarioCorrige;
    }

    /**
     * Define el valor de la propiedad idUsuarioCorrige.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuarioCorrige(String value) {
        this.idUsuarioCorrige = value;
    }

}
