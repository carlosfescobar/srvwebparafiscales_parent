
package co.gov.ugpp.schema.notificaciones.mecanismosubsidiariotipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.notificaciones.notificaciontipo.v1.NotificacionTipo;


/**
 * <p>Clase Java para MecanismoSubsidiarioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MecanismoSubsidiarioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="notificaciones" type="{http://www.ugpp.gov.co/schema/Notificaciones/NotificacionTipo/v1}NotificacionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codTipoEdictoAviso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecCreacionMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="esFijado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecFijacionCartelera" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descObservacionesFijado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecDesfijacionCartelera" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descObservacionesDesfijado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPublicadoWeb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecPublicacionWeb" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecDespublicacionWeb" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="archivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codEstadoMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MecanismoSubsidiarioTipo", propOrder = {
    "documento",
    "notificaciones",
    "codTipoEdictoAviso",
    "fecCreacionMecanismoSubsidiario",
    "esFijado",
    "fecFijacionCartelera",
    "descObservacionesFijado",
    "fecDesfijacionCartelera",
    "descObservacionesDesfijado",
    "esPublicadoWeb",
    "fecPublicacionWeb",
    "fecDespublicacionWeb",
    "idMecanismoSubsidiario",
    "archivosTemporales",
    "codEstadoMecanismoSubsidiario",
    "descEstadoMecanismoSubsidiario"
})
public class MecanismoSubsidiarioTipo {

    @XmlElement(nillable = true)
    protected DocumentoTipo documento;
    @XmlElement(nillable = true)
    protected List<NotificacionTipo> notificaciones;
    @XmlElement(nillable = true)
    protected String codTipoEdictoAviso;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecCreacionMecanismoSubsidiario;
    @XmlElement(nillable = true)
    protected String esFijado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecFijacionCartelera;
    @XmlElement(nillable = true)
    protected String descObservacionesFijado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecDesfijacionCartelera;
    @XmlElement(nillable = true)
    protected String descObservacionesDesfijado;
    @XmlElement(nillable = true)
    protected String esPublicadoWeb;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecPublicacionWeb;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecDespublicacionWeb;
    protected Long idMecanismoSubsidiario;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> archivosTemporales;
    @XmlElement(nillable = true)
    protected String codEstadoMecanismoSubsidiario;
    @XmlElement(nillable = true)
    protected String descEstadoMecanismoSubsidiario;

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocumento(DocumentoTipo value) {
        this.documento = value;
    }

    /**
     * Gets the value of the notificaciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notificaciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotificaciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link NotificacionTipo }
     * 
     * 
     */
    public List<NotificacionTipo> getNotificaciones() {
        if (notificaciones == null) {
            notificaciones = new ArrayList<NotificacionTipo>();
        }
        return this.notificaciones;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEdictoAviso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEdictoAviso() {
        return codTipoEdictoAviso;
    }

    /**
     * Define el valor de la propiedad codTipoEdictoAviso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEdictoAviso(String value) {
        this.codTipoEdictoAviso = value;
    }

    /**
     * Obtiene el valor de la propiedad fecCreacionMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecCreacionMecanismoSubsidiario() {
        return fecCreacionMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad fecCreacionMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecCreacionMecanismoSubsidiario(Calendar value) {
        this.fecCreacionMecanismoSubsidiario = value;
    }

    /**
     * Obtiene el valor de la propiedad esFijado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsFijado() {
        return esFijado;
    }

    /**
     * Define el valor de la propiedad esFijado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsFijado(String value) {
        this.esFijado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFijacionCartelera.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFijacionCartelera() {
        return fecFijacionCartelera;
    }

    /**
     * Define el valor de la propiedad fecFijacionCartelera.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFijacionCartelera(Calendar value) {
        this.fecFijacionCartelera = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesFijado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesFijado() {
        return descObservacionesFijado;
    }

    /**
     * Define el valor de la propiedad descObservacionesFijado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesFijado(String value) {
        this.descObservacionesFijado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecDesfijacionCartelera.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecDesfijacionCartelera() {
        return fecDesfijacionCartelera;
    }

    /**
     * Define el valor de la propiedad fecDesfijacionCartelera.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecDesfijacionCartelera(Calendar value) {
        this.fecDesfijacionCartelera = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesDesfijado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesDesfijado() {
        return descObservacionesDesfijado;
    }

    /**
     * Define el valor de la propiedad descObservacionesDesfijado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesDesfijado(String value) {
        this.descObservacionesDesfijado = value;
    }

    /**
     * Obtiene el valor de la propiedad esPublicadoWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPublicadoWeb() {
        return esPublicadoWeb;
    }

    /**
     * Define el valor de la propiedad esPublicadoWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPublicadoWeb(String value) {
        this.esPublicadoWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad fecPublicacionWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecPublicacionWeb() {
        return fecPublicacionWeb;
    }

    /**
     * Define el valor de la propiedad fecPublicacionWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecPublicacionWeb(Calendar value) {
        this.fecPublicacionWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad fecDespublicacionWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecDespublicacionWeb() {
        return fecDespublicacionWeb;
    }

    /**
     * Define el valor de la propiedad fecDespublicacionWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecDespublicacionWeb(Calendar value) {
        this.fecDespublicacionWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad idMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdMecanismoSubsidiario() {
        return idMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad idMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdMecanismoSubsidiario(Long value) {
        this.idMecanismoSubsidiario = value;
    }

    /**
     * Gets the value of the archivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.archivosTemporales;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoMecanismoSubsidiario() {
        return codEstadoMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad codEstadoMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoMecanismoSubsidiario(String value) {
        this.codEstadoMecanismoSubsidiario = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoMecanismoSubsidiario() {
        return descEstadoMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad descEstadoMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoMecanismoSubsidiario(String value) {
        this.descEstadoMecanismoSubsidiario = value;
    }

}
