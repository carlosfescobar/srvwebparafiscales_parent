
package co.gov.ugpp.schema.notificaciones.notificaciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.notificaciones.interesadotipo.v1.InteresadoTipo;


/**
 * <p>Clase Java para NotificacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NotificacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actoAdministrativo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo"/>
 *         &lt;element name="docActaNotificacionPersonal" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *         &lt;element name="interesado" type="{http://www.ugpp.gov.co/schema/Notificaciones/InteresadoTipo/v1}InteresadoTipo" minOccurs="0"/>
 *         &lt;element name="personaNaturalNotificada" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo" minOccurs="0"/>
 *         &lt;element name="codCalidadNotificado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descCalidadNotificado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecNotificacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="esActaNotificacionValidada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descActaNotificacionValidadaObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esDevolucionActoAdministrativoAprobada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descDevolucionActoAdministrativoObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoMecanismoSubsidiario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDocumentosAnexosNotificacionPersonal" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idDocumentosAnexos" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idRadicadoDocumentosAnexosNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoNotificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoNotificacionDefinitiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NotificacionTipo", propOrder = {
    "idNotificacion",
    "actoAdministrativo",
    "docActaNotificacionPersonal",
    "interesado",
    "personaNaturalNotificada",
    "codCalidadNotificado",
    "descCalidadNotificado",
    "codTipoNotificacion",
    "fecNotificacion",
    "esActaNotificacionValidada",
    "descActaNotificacionValidadaObservaciones",
    "esDevolucionActoAdministrativoAprobada",
    "descDevolucionActoAdministrativoObservaciones",
    "esMecanismoSubsidiario",
    "codTipoMecanismoSubsidiario",
    "codDocumentosAnexosNotificacionPersonal",
    "idDocumentosAnexos",
    "idRadicadoDocumentosAnexosNotificacion",
    "codEstadoNotificacion",
    "codTipoNotificacionDefinitiva",
    "archivosTemporales"
})
public class NotificacionTipo {

    @XmlElement(nillable = true)
    protected String idNotificacion;
    @XmlElement(required = true)
    protected ActoAdministrativoTipo actoAdministrativo;
    @XmlElement(nillable = true)
    protected ArchivoTipo docActaNotificacionPersonal;
    @XmlElement(nillable = true)
    protected InteresadoTipo interesado;
    @XmlElement(nillable = true)
    protected PersonaNaturalTipo personaNaturalNotificada;
    @XmlElement(nillable = true)
    protected String codCalidadNotificado;
    @XmlElement(nillable = true)
    protected String descCalidadNotificado;
    @XmlElement(nillable = true)
    protected String codTipoNotificacion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecNotificacion;
    @XmlElement(nillable = true)
    protected String esActaNotificacionValidada;
    @XmlElement(nillable = true)
    protected String descActaNotificacionValidadaObservaciones;
    @XmlElement(nillable = true)
    protected String esDevolucionActoAdministrativoAprobada;
    @XmlElement(nillable = true)
    protected String descDevolucionActoAdministrativoObservaciones;
    @XmlElement(nillable = true)
    protected String esMecanismoSubsidiario;
    @XmlElement(nillable = true)
    protected String codTipoMecanismoSubsidiario;
    @XmlElement(nillable = true)
    protected List<String> codDocumentosAnexosNotificacionPersonal;
    @XmlElement(nillable = true)
    protected List<String> idDocumentosAnexos;
    @XmlElement(nillable = true)
    protected String idRadicadoDocumentosAnexosNotificacion;
    @XmlElement(nillable = true)
    protected String codEstadoNotificacion;
    @XmlElement(nillable = true)
    protected String codTipoNotificacionDefinitiva;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> archivosTemporales;

    /**
     * Obtiene el valor de la propiedad idNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNotificacion() {
        return idNotificacion;
    }

    /**
     * Define el valor de la propiedad idNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNotificacion(String value) {
        this.idNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad actoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getActoAdministrativo() {
        return actoAdministrativo;
    }

    /**
     * Define el valor de la propiedad actoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setActoAdministrativo(ActoAdministrativoTipo value) {
        this.actoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad docActaNotificacionPersonal.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getDocActaNotificacionPersonal() {
        return docActaNotificacionPersonal;
    }

    /**
     * Define el valor de la propiedad docActaNotificacionPersonal.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setDocActaNotificacionPersonal(ArchivoTipo value) {
        this.docActaNotificacionPersonal = value;
    }

    /**
     * Obtiene el valor de la propiedad interesado.
     * 
     * @return
     *     possible object is
     *     {@link InteresadoTipo }
     *     
     */
    public InteresadoTipo getInteresado() {
        return interesado;
    }

    /**
     * Define el valor de la propiedad interesado.
     * 
     * @param value
     *     allowed object is
     *     {@link InteresadoTipo }
     *     
     */
    public void setInteresado(InteresadoTipo value) {
        this.interesado = value;
    }

    /**
     * Obtiene el valor de la propiedad personaNaturalNotificada.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNaturalNotificada() {
        return personaNaturalNotificada;
    }

    /**
     * Define el valor de la propiedad personaNaturalNotificada.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNaturalNotificada(PersonaNaturalTipo value) {
        this.personaNaturalNotificada = value;
    }

    /**
     * Obtiene el valor de la propiedad codCalidadNotificado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCalidadNotificado() {
        return codCalidadNotificado;
    }

    /**
     * Define el valor de la propiedad codCalidadNotificado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCalidadNotificado(String value) {
        this.codCalidadNotificado = value;
    }

    /**
     * Obtiene el valor de la propiedad descCalidadNotificado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCalidadNotificado() {
        return descCalidadNotificado;
    }

    /**
     * Define el valor de la propiedad descCalidadNotificado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCalidadNotificado(String value) {
        this.descCalidadNotificado = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoNotificacion() {
        return codTipoNotificacion;
    }

    /**
     * Define el valor de la propiedad codTipoNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoNotificacion(String value) {
        this.codTipoNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecNotificacion() {
        return fecNotificacion;
    }

    /**
     * Define el valor de la propiedad fecNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecNotificacion(Calendar value) {
        this.fecNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esActaNotificacionValidada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsActaNotificacionValidada() {
        return esActaNotificacionValidada;
    }

    /**
     * Define el valor de la propiedad esActaNotificacionValidada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsActaNotificacionValidada(String value) {
        this.esActaNotificacionValidada = value;
    }

    /**
     * Obtiene el valor de la propiedad descActaNotificacionValidadaObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescActaNotificacionValidadaObservaciones() {
        return descActaNotificacionValidadaObservaciones;
    }

    /**
     * Define el valor de la propiedad descActaNotificacionValidadaObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescActaNotificacionValidadaObservaciones(String value) {
        this.descActaNotificacionValidadaObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad esDevolucionActoAdministrativoAprobada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDevolucionActoAdministrativoAprobada() {
        return esDevolucionActoAdministrativoAprobada;
    }

    /**
     * Define el valor de la propiedad esDevolucionActoAdministrativoAprobada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDevolucionActoAdministrativoAprobada(String value) {
        this.esDevolucionActoAdministrativoAprobada = value;
    }

    /**
     * Obtiene el valor de la propiedad descDevolucionActoAdministrativoObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescDevolucionActoAdministrativoObservaciones() {
        return descDevolucionActoAdministrativoObservaciones;
    }

    /**
     * Define el valor de la propiedad descDevolucionActoAdministrativoObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescDevolucionActoAdministrativoObservaciones(String value) {
        this.descDevolucionActoAdministrativoObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad esMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsMecanismoSubsidiario() {
        return esMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad esMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsMecanismoSubsidiario(String value) {
        this.esMecanismoSubsidiario = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoMecanismoSubsidiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoMecanismoSubsidiario() {
        return codTipoMecanismoSubsidiario;
    }

    /**
     * Define el valor de la propiedad codTipoMecanismoSubsidiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoMecanismoSubsidiario(String value) {
        this.codTipoMecanismoSubsidiario = value;
    }

    /**
     * Gets the value of the codDocumentosAnexosNotificacionPersonal property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codDocumentosAnexosNotificacionPersonal property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodDocumentosAnexosNotificacionPersonal().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodDocumentosAnexosNotificacionPersonal() {
        if (codDocumentosAnexosNotificacionPersonal == null) {
            codDocumentosAnexosNotificacionPersonal = new ArrayList<String>();
        }
        return this.codDocumentosAnexosNotificacionPersonal;
    }

    /**
     * Gets the value of the idDocumentosAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentosAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentosAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentosAnexos() {
        if (idDocumentosAnexos == null) {
            idDocumentosAnexos = new ArrayList<String>();
        }
        return this.idDocumentosAnexos;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoDocumentosAnexosNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoDocumentosAnexosNotificacion() {
        return idRadicadoDocumentosAnexosNotificacion;
    }

    /**
     * Define el valor de la propiedad idRadicadoDocumentosAnexosNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoDocumentosAnexosNotificacion(String value) {
        this.idRadicadoDocumentosAnexosNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoNotificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoNotificacion() {
        return codEstadoNotificacion;
    }

    /**
     * Define el valor de la propiedad codEstadoNotificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoNotificacion(String value) {
        this.codEstadoNotificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoNotificacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoNotificacionDefinitiva() {
        return codTipoNotificacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad codTipoNotificacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoNotificacionDefinitiva(String value) {
        this.codTipoNotificacionDefinitiva = value;
    }

    /**
     * Gets the value of the archivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.archivosTemporales;
    }

}
