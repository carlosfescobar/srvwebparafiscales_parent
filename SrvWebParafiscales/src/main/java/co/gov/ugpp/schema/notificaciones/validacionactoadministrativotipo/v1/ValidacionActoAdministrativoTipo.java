
package co.gov.ugpp.schema.notificaciones.validacionactoadministrativotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ValidacionActoAdministrativoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionActoAdministrativoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codEtapaValidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoAceptacionEtapa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionActoAdministrativoTipo", propOrder = {
    "codEtapaValidacion",
    "codEstadoAceptacionEtapa",
    "desObservacion"
})
public class ValidacionActoAdministrativoTipo {

    @XmlElement(nillable = true)
    protected String codEtapaValidacion;
    @XmlElement(nillable = true)
    protected String codEstadoAceptacionEtapa;
    @XmlElement(nillable = true)
    protected String desObservacion;

    /**
     * Obtiene el valor de la propiedad codEtapaValidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEtapaValidacion() {
        return codEtapaValidacion;
    }

    /**
     * Define el valor de la propiedad codEtapaValidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEtapaValidacion(String value) {
        this.codEtapaValidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoAceptacionEtapa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoAceptacionEtapa() {
        return codEstadoAceptacionEtapa;
    }

    /**
     * Define el valor de la propiedad codEstadoAceptacionEtapa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoAceptacionEtapa(String value) {
        this.codEstadoAceptacionEtapa = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacion() {
        return desObservacion;
    }

    /**
     * Define el valor de la propiedad desObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacion(String value) {
        this.desObservacion = value;
    }

}
