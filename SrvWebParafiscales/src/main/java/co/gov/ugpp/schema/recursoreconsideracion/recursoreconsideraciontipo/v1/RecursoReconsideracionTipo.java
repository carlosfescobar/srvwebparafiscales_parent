
package co.gov.ugpp.schema.recursoreconsideracion.recursoreconsideraciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.recursoreconsideracion.pruebatipo.v1.PruebaTipo;


/**
 * <p>Clase Java para RecursoReconsideracionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RecursoReconsideracionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProcesoEjecutador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="fecEntradaRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntradaRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoDeclararCorregir" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esDigitalizadoExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesDigitalizacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAdmisibleRecurso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSubsanable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDocumentosAnexosRequisitos" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descObservacionesRequisitos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoAdmisorioParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoAutoAdmisorio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoInadmisorioParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoAutoInadmisorio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPresentadoRecursoReposicion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSubsanadaInadmisibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecEntradaRecursoReposicion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntradaRecursoReposicion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codDocumentosAnexosRecursoReposicion" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descObservacionesRecursoReposicion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actoAprobacionInadmisionParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoAprobacionInadmision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actoReponeAdmiteParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoReponeAdmite" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPruebasSuficientes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esNecesarioEjecutarLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pruebas" type="{http://www.ugpp.gov.co/schema/RecursoReconsideracion/PruebaTipo/v1}PruebaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valImporteSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNuevoImporteSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesCalculoSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFalloFinal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFinalActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalesFallo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descObservacionesFallo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actoFalloParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoFallo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fechaPresentacionRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fechaPresentacionEnDebidaForma" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="archivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecursoReconsideracionTipo", propOrder = {
    "idRecursoReconsideracion",
    "codProcesoEjecutador",
    "expediente",
    "aportante",
    "fecEntradaRecursoReconsideracion",
    "idRadicadoEntradaRecursoReconsideracion",
    "idActoRequerimientoDeclararCorregir",
    "esDigitalizadoExpediente",
    "descObservacionesDigitalizacion",
    "esAdmisibleRecurso",
    "esSubsanable",
    "codDocumentosAnexosRequisitos",
    "descObservacionesRequisitos",
    "autoAdmisorioParcial",
    "idActoAutoAdmisorio",
    "autoInadmisorioParcial",
    "idActoAutoInadmisorio",
    "esPresentadoRecursoReposicion",
    "esSubsanadaInadmisibilidad",
    "fecEntradaRecursoReposicion",
    "idRadicadoEntradaRecursoReposicion",
    "codDocumentosAnexosRecursoReposicion",
    "descObservacionesRecursoReposicion",
    "actoAprobacionInadmisionParcial",
    "idActoAprobacionInadmision",
    "actoReponeAdmiteParcial",
    "idActoReponeAdmite",
    "esPruebasSuficientes",
    "esNecesarioEjecutarLiquidador",
    "pruebas",
    "valImporteSancion",
    "valNuevoImporteSancion",
    "descObservacionesCalculoSancion",
    "codFalloFinal",
    "valFinalActoAdministrativo",
    "codCausalesFallo",
    "descObservacionesFallo",
    "actoFalloParcial",
    "idActoFallo",
    "fechaPresentacionRecursoReconsideracion",
    "fechaPresentacionEnDebidaForma",
    "idLiquidacion",
    "idSancion",
    "idLiquidador",
    "codEstado",
    "archivosTemporales"
})
public class RecursoReconsideracionTipo {

    @XmlElement(nillable = true)
    protected String idRecursoReconsideracion;
    @XmlElement(nillable = true)
    protected String codProcesoEjecutador;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEntradaRecursoReconsideracion;
    @XmlElement(nillable = true)
    protected String idRadicadoEntradaRecursoReconsideracion;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoDeclararCorregir;
    @XmlElement(nillable = true)
    protected String esDigitalizadoExpediente;
    @XmlElement(nillable = true)
    protected String descObservacionesDigitalizacion;
    @XmlElement(nillable = true)
    protected String esAdmisibleRecurso;
    @XmlElement(nillable = true)
    protected String esSubsanable;
    @XmlElement(nillable = true)
    protected List<String> codDocumentosAnexosRequisitos;
    @XmlElement(nillable = true)
    protected String descObservacionesRequisitos;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoAdmisorioParcial;
    @XmlElement(nillable = true)
    protected String idActoAutoAdmisorio;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoInadmisorioParcial;
    @XmlElement(nillable = true)
    protected String idActoAutoInadmisorio;
    @XmlElement(nillable = true)
    protected String esPresentadoRecursoReposicion;
    @XmlElement(nillable = true)
    protected String esSubsanadaInadmisibilidad;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEntradaRecursoReposicion;
    @XmlElement(nillable = true)
    protected String idRadicadoEntradaRecursoReposicion;
    @XmlElement(nillable = true)
    protected List<String> codDocumentosAnexosRecursoReposicion;
    @XmlElement(nillable = true)
    protected String descObservacionesRecursoReposicion;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo actoAprobacionInadmisionParcial;
    @XmlElement(nillable = true)
    protected String idActoAprobacionInadmision;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo actoReponeAdmiteParcial;
    @XmlElement(nillable = true)
    protected String idActoReponeAdmite;
    @XmlElement(nillable = true)
    protected String esPruebasSuficientes;
    @XmlElement(nillable = true)
    protected String esNecesarioEjecutarLiquidador;
    @XmlElement(nillable = true)
    protected List<PruebaTipo> pruebas;
    @XmlElement(nillable = true)
    protected String valImporteSancion;
    @XmlElement(nillable = true)
    protected String valNuevoImporteSancion;
    @XmlElement(nillable = true)
    protected String descObservacionesCalculoSancion;
    @XmlElement(nillable = true)
    protected String codFalloFinal;
    @XmlElement(nillable = true)
    protected String valFinalActoAdministrativo;
    @XmlElement(nillable = true)
    protected List<String> codCausalesFallo;
    @XmlElement(nillable = true)
    protected String descObservacionesFallo;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo actoFalloParcial;
    @XmlElement(nillable = true)
    protected String idActoFallo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaPresentacionRecursoReconsideracion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fechaPresentacionEnDebidaForma;
    @XmlElement(nillable = true)
    protected String idLiquidacion;
    @XmlElement(nillable = true)
    protected String idSancion;
    @XmlElement(nillable = true)
    protected String idLiquidador;
    @XmlElement(nillable = true)
    protected String codEstado;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> archivosTemporales;

    /**
     * Obtiene el valor de la propiedad idRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRecursoReconsideracion() {
        return idRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad idRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRecursoReconsideracion(String value) {
        this.idRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad codProcesoEjecutador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProcesoEjecutador() {
        return codProcesoEjecutador;
    }

    /**
     * Define el valor de la propiedad codProcesoEjecutador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProcesoEjecutador(String value) {
        this.codProcesoEjecutador = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEntradaRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEntradaRecursoReconsideracion() {
        return fecEntradaRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad fecEntradaRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEntradaRecursoReconsideracion(Calendar value) {
        this.fecEntradaRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntradaRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntradaRecursoReconsideracion() {
        return idRadicadoEntradaRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntradaRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntradaRecursoReconsideracion(String value) {
        this.idRadicadoEntradaRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoDeclararCorregir.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoDeclararCorregir() {
        return idActoRequerimientoDeclararCorregir;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoDeclararCorregir.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoDeclararCorregir(String value) {
        this.idActoRequerimientoDeclararCorregir = value;
    }

    /**
     * Obtiene el valor de la propiedad esDigitalizadoExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDigitalizadoExpediente() {
        return esDigitalizadoExpediente;
    }

    /**
     * Define el valor de la propiedad esDigitalizadoExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDigitalizadoExpediente(String value) {
        this.esDigitalizadoExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesDigitalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesDigitalizacion() {
        return descObservacionesDigitalizacion;
    }

    /**
     * Define el valor de la propiedad descObservacionesDigitalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesDigitalizacion(String value) {
        this.descObservacionesDigitalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esAdmisibleRecurso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAdmisibleRecurso() {
        return esAdmisibleRecurso;
    }

    /**
     * Define el valor de la propiedad esAdmisibleRecurso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAdmisibleRecurso(String value) {
        this.esAdmisibleRecurso = value;
    }

    /**
     * Obtiene el valor de la propiedad esSubsanable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSubsanable() {
        return esSubsanable;
    }

    /**
     * Define el valor de la propiedad esSubsanable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSubsanable(String value) {
        this.esSubsanable = value;
    }

    /**
     * Gets the value of the codDocumentosAnexosRequisitos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codDocumentosAnexosRequisitos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodDocumentosAnexosRequisitos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodDocumentosAnexosRequisitos() {
        if (codDocumentosAnexosRequisitos == null) {
            codDocumentosAnexosRequisitos = new ArrayList<String>();
        }
        return this.codDocumentosAnexosRequisitos;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesRequisitos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesRequisitos() {
        return descObservacionesRequisitos;
    }

    /**
     * Define el valor de la propiedad descObservacionesRequisitos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesRequisitos(String value) {
        this.descObservacionesRequisitos = value;
    }

    /**
     * Obtiene el valor de la propiedad autoAdmisorioParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoAdmisorioParcial() {
        return autoAdmisorioParcial;
    }

    /**
     * Define el valor de la propiedad autoAdmisorioParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoAdmisorioParcial(AprobacionDocumentoTipo value) {
        this.autoAdmisorioParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAutoAdmisorio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAutoAdmisorio() {
        return idActoAutoAdmisorio;
    }

    /**
     * Define el valor de la propiedad idActoAutoAdmisorio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAutoAdmisorio(String value) {
        this.idActoAutoAdmisorio = value;
    }

    /**
     * Obtiene el valor de la propiedad autoInadmisorioParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoInadmisorioParcial() {
        return autoInadmisorioParcial;
    }

    /**
     * Define el valor de la propiedad autoInadmisorioParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoInadmisorioParcial(AprobacionDocumentoTipo value) {
        this.autoInadmisorioParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAutoInadmisorio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAutoInadmisorio() {
        return idActoAutoInadmisorio;
    }

    /**
     * Define el valor de la propiedad idActoAutoInadmisorio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAutoInadmisorio(String value) {
        this.idActoAutoInadmisorio = value;
    }

    /**
     * Obtiene el valor de la propiedad esPresentadoRecursoReposicion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPresentadoRecursoReposicion() {
        return esPresentadoRecursoReposicion;
    }

    /**
     * Define el valor de la propiedad esPresentadoRecursoReposicion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPresentadoRecursoReposicion(String value) {
        this.esPresentadoRecursoReposicion = value;
    }

    /**
     * Obtiene el valor de la propiedad esSubsanadaInadmisibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSubsanadaInadmisibilidad() {
        return esSubsanadaInadmisibilidad;
    }

    /**
     * Define el valor de la propiedad esSubsanadaInadmisibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSubsanadaInadmisibilidad(String value) {
        this.esSubsanadaInadmisibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEntradaRecursoReposicion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEntradaRecursoReposicion() {
        return fecEntradaRecursoReposicion;
    }

    /**
     * Define el valor de la propiedad fecEntradaRecursoReposicion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEntradaRecursoReposicion(Calendar value) {
        this.fecEntradaRecursoReposicion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntradaRecursoReposicion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntradaRecursoReposicion() {
        return idRadicadoEntradaRecursoReposicion;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntradaRecursoReposicion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntradaRecursoReposicion(String value) {
        this.idRadicadoEntradaRecursoReposicion = value;
    }

    /**
     * Gets the value of the codDocumentosAnexosRecursoReposicion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codDocumentosAnexosRecursoReposicion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodDocumentosAnexosRecursoReposicion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodDocumentosAnexosRecursoReposicion() {
        if (codDocumentosAnexosRecursoReposicion == null) {
            codDocumentosAnexosRecursoReposicion = new ArrayList<String>();
        }
        return this.codDocumentosAnexosRecursoReposicion;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesRecursoReposicion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesRecursoReposicion() {
        return descObservacionesRecursoReposicion;
    }

    /**
     * Define el valor de la propiedad descObservacionesRecursoReposicion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesRecursoReposicion(String value) {
        this.descObservacionesRecursoReposicion = value;
    }

    /**
     * Obtiene el valor de la propiedad actoAprobacionInadmisionParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getActoAprobacionInadmisionParcial() {
        return actoAprobacionInadmisionParcial;
    }

    /**
     * Define el valor de la propiedad actoAprobacionInadmisionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setActoAprobacionInadmisionParcial(AprobacionDocumentoTipo value) {
        this.actoAprobacionInadmisionParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAprobacionInadmision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAprobacionInadmision() {
        return idActoAprobacionInadmision;
    }

    /**
     * Define el valor de la propiedad idActoAprobacionInadmision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAprobacionInadmision(String value) {
        this.idActoAprobacionInadmision = value;
    }

    /**
     * Obtiene el valor de la propiedad actoReponeAdmiteParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getActoReponeAdmiteParcial() {
        return actoReponeAdmiteParcial;
    }

    /**
     * Define el valor de la propiedad actoReponeAdmiteParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setActoReponeAdmiteParcial(AprobacionDocumentoTipo value) {
        this.actoReponeAdmiteParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoReponeAdmite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoReponeAdmite() {
        return idActoReponeAdmite;
    }

    /**
     * Define el valor de la propiedad idActoReponeAdmite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoReponeAdmite(String value) {
        this.idActoReponeAdmite = value;
    }

    /**
     * Obtiene el valor de la propiedad esPruebasSuficientes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPruebasSuficientes() {
        return esPruebasSuficientes;
    }

    /**
     * Define el valor de la propiedad esPruebasSuficientes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPruebasSuficientes(String value) {
        this.esPruebasSuficientes = value;
    }

    /**
     * Obtiene el valor de la propiedad esNecesarioEjecutarLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsNecesarioEjecutarLiquidador() {
        return esNecesarioEjecutarLiquidador;
    }

    /**
     * Define el valor de la propiedad esNecesarioEjecutarLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsNecesarioEjecutarLiquidador(String value) {
        this.esNecesarioEjecutarLiquidador = value;
    }

    /**
     * Gets the value of the pruebas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pruebas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPruebas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PruebaTipo }
     * 
     * 
     */
    public List<PruebaTipo> getPruebas() {
        if (pruebas == null) {
            pruebas = new ArrayList<PruebaTipo>();
        }
        return this.pruebas;
    }

    /**
     * Obtiene el valor de la propiedad valImporteSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValImporteSancion() {
        return valImporteSancion;
    }

    /**
     * Define el valor de la propiedad valImporteSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValImporteSancion(String value) {
        this.valImporteSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad valNuevoImporteSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNuevoImporteSancion() {
        return valNuevoImporteSancion;
    }

    /**
     * Define el valor de la propiedad valNuevoImporteSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNuevoImporteSancion(String value) {
        this.valNuevoImporteSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesCalculoSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesCalculoSancion() {
        return descObservacionesCalculoSancion;
    }

    /**
     * Define el valor de la propiedad descObservacionesCalculoSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesCalculoSancion(String value) {
        this.descObservacionesCalculoSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad codFalloFinal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFalloFinal() {
        return codFalloFinal;
    }

    /**
     * Define el valor de la propiedad codFalloFinal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFalloFinal(String value) {
        this.codFalloFinal = value;
    }

    /**
     * Obtiene el valor de la propiedad valFinalActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFinalActoAdministrativo() {
        return valFinalActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad valFinalActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFinalActoAdministrativo(String value) {
        this.valFinalActoAdministrativo = value;
    }

    /**
     * Gets the value of the codCausalesFallo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codCausalesFallo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodCausalesFallo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodCausalesFallo() {
        if (codCausalesFallo == null) {
            codCausalesFallo = new ArrayList<String>();
        }
        return this.codCausalesFallo;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesFallo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesFallo() {
        return descObservacionesFallo;
    }

    /**
     * Define el valor de la propiedad descObservacionesFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesFallo(String value) {
        this.descObservacionesFallo = value;
    }

    /**
     * Obtiene el valor de la propiedad actoFalloParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getActoFalloParcial() {
        return actoFalloParcial;
    }

    /**
     * Define el valor de la propiedad actoFalloParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setActoFalloParcial(AprobacionDocumentoTipo value) {
        this.actoFalloParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoFallo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoFallo() {
        return idActoFallo;
    }

    /**
     * Define el valor de la propiedad idActoFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoFallo(String value) {
        this.idActoFallo = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaPresentacionRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaPresentacionRecursoReconsideracion() {
        return fechaPresentacionRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad fechaPresentacionRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaPresentacionRecursoReconsideracion(Calendar value) {
        this.fechaPresentacionRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad fechaPresentacionEnDebidaForma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFechaPresentacionEnDebidaForma() {
        return fechaPresentacionEnDebidaForma;
    }

    /**
     * Define el valor de la propiedad fechaPresentacionEnDebidaForma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaPresentacionEnDebidaForma(Calendar value) {
        this.fechaPresentacionEnDebidaForma = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidacion() {
        return idLiquidacion;
    }

    /**
     * Define el valor de la propiedad idLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidacion(String value) {
        this.idLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSancion() {
        return idSancion;
    }

    /**
     * Define el valor de la propiedad idSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSancion(String value) {
        this.idSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidador() {
        return idLiquidador;
    }

    /**
     * Define el valor de la propiedad idLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidador(String value) {
        this.idLiquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

    /**
     * Gets the value of the archivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.archivosTemporales;
    }

}
