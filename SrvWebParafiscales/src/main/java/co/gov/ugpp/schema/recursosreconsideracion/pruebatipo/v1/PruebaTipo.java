
package co.gov.ugpp.schema.recursosreconsideracion.pruebatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;


/**
 * <p>Clase Java para PruebaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PruebaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecSolicitudPrueba" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="valTiempoEsperaPruebaExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descInformacionSolicitada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="esPruebaSolicitada" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="esSolicitadaPruebaInterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esResueltaSolicitudInterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esContinuadoProcesoInterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesSolicitudInterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solicitudPruebaExternaParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentoSolicitudPruebaExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inspeccionTributariaParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoInspeccionTributaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInspeccionTributaria" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="valNombreInspector" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombrePersonaContactadaInspeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCargoPersonaContactadaInspeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoInformeInspeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionesInspeccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PruebaTipo", propOrder = {
    "idPrueba",
    "codTipoPrueba",
    "fecSolicitudPrueba",
    "valTiempoEsperaPruebaExterna",
    "descInformacionSolicitada",
    "entidadExterna",
    "esPruebaSolicitada",
    "esSolicitadaPruebaInterna",
    "esResueltaSolicitudInterna",
    "esContinuadoProcesoInterno",
    "descObservacionesSolicitudInterna",
    "solicitudPruebaExternaParcial",
    "idDocumentoSolicitudPruebaExterna",
    "inspeccionTributariaParcial",
    "idActoInspeccionTributaria",
    "fecInspeccionTributaria",
    "valNombreInspector",
    "valNombrePersonaContactadaInspeccion",
    "valCargoPersonaContactadaInspeccion",
    "idDocumentoInformeInspeccion",
    "descObservacionesInspeccion"
})
public class PruebaTipo {

    @XmlElement(nillable = true)
    protected String idPrueba;
    @XmlElement(nillable = true)
    protected String codTipoPrueba;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecSolicitudPrueba;
    @XmlElement(nillable = true)
    protected String valTiempoEsperaPruebaExterna;
    @XmlElement(nillable = true)
    protected String descInformacionSolicitada;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo esPruebaSolicitada;
    @XmlElement(nillable = true)
    protected String esSolicitadaPruebaInterna;
    @XmlElement(nillable = true)
    protected String esResueltaSolicitudInterna;
    @XmlElement(nillable = true)
    protected String esContinuadoProcesoInterno;
    @XmlElement(nillable = true)
    protected String descObservacionesSolicitudInterna;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo solicitudPruebaExternaParcial;
    @XmlElement(nillable = true)
    protected String idDocumentoSolicitudPruebaExterna;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo inspeccionTributariaParcial;
    @XmlElement(nillable = true)
    protected String idActoInspeccionTributaria;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecInspeccionTributaria;
    @XmlElement(nillable = true)
    protected String valNombreInspector;
    @XmlElement(nillable = true)
    protected String valNombrePersonaContactadaInspeccion;
    @XmlElement(nillable = true)
    protected String valCargoPersonaContactadaInspeccion;
    @XmlElement(nillable = true)
    protected String idDocumentoInformeInspeccion;
    @XmlElement(nillable = true)
    protected String descObservacionesInspeccion;

    /**
     * Obtiene el valor de la propiedad idPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPrueba() {
        return idPrueba;
    }

    /**
     * Define el valor de la propiedad idPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPrueba(String value) {
        this.idPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoPrueba() {
        return codTipoPrueba;
    }

    /**
     * Define el valor de la propiedad codTipoPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoPrueba(String value) {
        this.codTipoPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad fecSolicitudPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecSolicitudPrueba() {
        return fecSolicitudPrueba;
    }

    /**
     * Define el valor de la propiedad fecSolicitudPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecSolicitudPrueba(Calendar value) {
        this.fecSolicitudPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad valTiempoEsperaPruebaExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValTiempoEsperaPruebaExterna() {
        return valTiempoEsperaPruebaExterna;
    }

    /**
     * Define el valor de la propiedad valTiempoEsperaPruebaExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValTiempoEsperaPruebaExterna(String value) {
        this.valTiempoEsperaPruebaExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad descInformacionSolicitada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescInformacionSolicitada() {
        return descInformacionSolicitada;
    }

    /**
     * Define el valor de la propiedad descInformacionSolicitada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescInformacionSolicitada(String value) {
        this.descInformacionSolicitada = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad esPruebaSolicitada.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEsPruebaSolicitada() {
        return esPruebaSolicitada;
    }

    /**
     * Define el valor de la propiedad esPruebaSolicitada.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEsPruebaSolicitada(EntidadExternaTipo value) {
        this.esPruebaSolicitada = value;
    }

    /**
     * Obtiene el valor de la propiedad esSolicitadaPruebaInterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSolicitadaPruebaInterna() {
        return esSolicitadaPruebaInterna;
    }

    /**
     * Define el valor de la propiedad esSolicitadaPruebaInterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSolicitadaPruebaInterna(String value) {
        this.esSolicitadaPruebaInterna = value;
    }

    /**
     * Obtiene el valor de la propiedad esResueltaSolicitudInterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsResueltaSolicitudInterna() {
        return esResueltaSolicitudInterna;
    }

    /**
     * Define el valor de la propiedad esResueltaSolicitudInterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsResueltaSolicitudInterna(String value) {
        this.esResueltaSolicitudInterna = value;
    }

    /**
     * Obtiene el valor de la propiedad esContinuadoProcesoInterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsContinuadoProcesoInterno() {
        return esContinuadoProcesoInterno;
    }

    /**
     * Define el valor de la propiedad esContinuadoProcesoInterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsContinuadoProcesoInterno(String value) {
        this.esContinuadoProcesoInterno = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesSolicitudInterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesSolicitudInterna() {
        return descObservacionesSolicitudInterna;
    }

    /**
     * Define el valor de la propiedad descObservacionesSolicitudInterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesSolicitudInterna(String value) {
        this.descObservacionesSolicitudInterna = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudPruebaExternaParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getSolicitudPruebaExternaParcial() {
        return solicitudPruebaExternaParcial;
    }

    /**
     * Define el valor de la propiedad solicitudPruebaExternaParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setSolicitudPruebaExternaParcial(AprobacionDocumentoTipo value) {
        this.solicitudPruebaExternaParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoSolicitudPruebaExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoSolicitudPruebaExterna() {
        return idDocumentoSolicitudPruebaExterna;
    }

    /**
     * Define el valor de la propiedad idDocumentoSolicitudPruebaExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoSolicitudPruebaExterna(String value) {
        this.idDocumentoSolicitudPruebaExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad inspeccionTributariaParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getInspeccionTributariaParcial() {
        return inspeccionTributariaParcial;
    }

    /**
     * Define el valor de la propiedad inspeccionTributariaParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setInspeccionTributariaParcial(AprobacionDocumentoTipo value) {
        this.inspeccionTributariaParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoInspeccionTributaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoInspeccionTributaria() {
        return idActoInspeccionTributaria;
    }

    /**
     * Define el valor de la propiedad idActoInspeccionTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoInspeccionTributaria(String value) {
        this.idActoInspeccionTributaria = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInspeccionTributaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInspeccionTributaria() {
        return fecInspeccionTributaria;
    }

    /**
     * Define el valor de la propiedad fecInspeccionTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInspeccionTributaria(Calendar value) {
        this.fecInspeccionTributaria = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreInspector.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreInspector() {
        return valNombreInspector;
    }

    /**
     * Define el valor de la propiedad valNombreInspector.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreInspector(String value) {
        this.valNombreInspector = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombrePersonaContactadaInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombrePersonaContactadaInspeccion() {
        return valNombrePersonaContactadaInspeccion;
    }

    /**
     * Define el valor de la propiedad valNombrePersonaContactadaInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombrePersonaContactadaInspeccion(String value) {
        this.valNombrePersonaContactadaInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad valCargoPersonaContactadaInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCargoPersonaContactadaInspeccion() {
        return valCargoPersonaContactadaInspeccion;
    }

    /**
     * Define el valor de la propiedad valCargoPersonaContactadaInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCargoPersonaContactadaInspeccion(String value) {
        this.valCargoPersonaContactadaInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoInformeInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoInformeInspeccion() {
        return idDocumentoInformeInspeccion;
    }

    /**
     * Define el valor de la propiedad idDocumentoInformeInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoInformeInspeccion(String value) {
        this.idDocumentoInformeInspeccion = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionesInspeccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionesInspeccion() {
        return descObservacionesInspeccion;
    }

    /**
     * Define el valor de la propiedad descObservacionesInspeccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionesInspeccion(String value) {
        this.descObservacionesInspeccion = value;
    }

}
