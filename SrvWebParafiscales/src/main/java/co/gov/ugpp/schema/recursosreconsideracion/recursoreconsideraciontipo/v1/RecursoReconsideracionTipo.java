
package co.gov.ugpp.schema.recursosreconsideracion.recursoreconsideraciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;
import co.gov.ugpp.schema.sanciones.informacionpruebatipo.v1.InformacionPruebaTipo;
import co.gov.ugpp.schema.transversales.validaciondocumentotipo.v1.ValidacionDocumentoTipo;


/**
 * <p>Clase Java para RecursoReconsideracionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RecursoReconsideracionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actoAdministrativo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="digitalizacionExpediente" type="{http://www.ugpp.gov.co/schema/RecursosReconsideracion/DigitalizacionExpedienteTipo/v1}DigitalizacionExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="idRadicadoPresentacionRecurso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoPresentacionRecurso" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="esPresentacion10dias" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPresentacionExtemporanea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPresentacionPersonal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAcreditacionCalidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codNivelComplejidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoAdmisibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desTipoAdmisibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSubsanarAdmisibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInformacionCompleta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desSubsanarAdmisibilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inspeccionTributaria" type="{http://www.ugpp.gov.co/schema/RecursosReconsideracion/InspeccionTributariaTipo/v1}InspeccionTributariaTipo" minOccurs="0"/>
 *         &lt;element name="solicitudPruebas" type="{http://www.ugpp.gov.co/schema/Sanciones/InformacionPruebaTipo/v1}InformacionPruebaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="validacionDocumentos" type="{http://www.ugpp.gov.co/schema/Transversales/ValidacionDocumentoTipo/v1}ValidacionDocumentoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="documento" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RecursoReconsideracionTipo", propOrder = {
    "idRecursoReconsideracion",
    "actoAdministrativo",
    "expediente",
    "digitalizacionExpediente",
    "idRadicadoPresentacionRecurso",
    "fecRadicadoPresentacionRecurso",
    "esPresentacion10Dias",
    "esPresentacionExtemporanea",
    "esPresentacionPersonal",
    "esAcreditacionCalidad",
    "codNivelComplejidad",
    "codTipoAdmisibilidad",
    "desTipoAdmisibilidad",
    "esSubsanarAdmisibilidad",
    "esInformacionCompleta",
    "desSubsanarAdmisibilidad",
    "solicitudPruebas",
    "validacionDocumentos",
    "documento"
})
public class RecursoReconsideracionTipo {

    @XmlElement(nillable = true)
    protected String idRecursoReconsideracion;
    @XmlElement(required = true)
    protected ActoAdministrativoTipo actoAdministrativo;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected String idRadicadoPresentacionRecurso;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoPresentacionRecurso;
    @XmlElement(name = "esPresentacion10dias", nillable = true)
    protected String esPresentacion10Dias;
    @XmlElement(nillable = true)
    protected String esPresentacionExtemporanea;
    @XmlElement(nillable = true)
    protected String esPresentacionPersonal;
    @XmlElement(nillable = true)
    protected String esAcreditacionCalidad;
    @XmlElement(nillable = true)
    protected String codNivelComplejidad;
    @XmlElement(nillable = true)
    protected String codTipoAdmisibilidad;
    @XmlElement(nillable = true)
    protected String desTipoAdmisibilidad;
    @XmlElement(nillable = true)
    protected String esSubsanarAdmisibilidad;
    @XmlElement(nillable = true)
    protected String esInformacionCompleta;
    @XmlElement(nillable = true)
    protected String desSubsanarAdmisibilidad;
    @XmlElement(nillable = true)
    protected List<InformacionPruebaTipo> solicitudPruebas;
    @XmlElement(nillable = true)
    protected List<ValidacionDocumentoTipo> validacionDocumentos;
    @XmlElement(nillable = true)
    protected DocumentoTipo documento;

    /**
     * Obtiene el valor de la propiedad idRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRecursoReconsideracion() {
        return idRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad idRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRecursoReconsideracion(String value) {
        this.idRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad actoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getActoAdministrativo() {
        return actoAdministrativo;
    }

    /**
     * Define el valor de la propiedad actoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setActoAdministrativo(ActoAdministrativoTipo value) {
        this.actoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    

    /**
     * Obtiene el valor de la propiedad idRadicadoPresentacionRecurso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoPresentacionRecurso() {
        return idRadicadoPresentacionRecurso;
    }

    /**
     * Define el valor de la propiedad idRadicadoPresentacionRecurso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoPresentacionRecurso(String value) {
        this.idRadicadoPresentacionRecurso = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoPresentacionRecurso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoPresentacionRecurso() {
        return fecRadicadoPresentacionRecurso;
    }

    /**
     * Define el valor de la propiedad fecRadicadoPresentacionRecurso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoPresentacionRecurso(Calendar value) {
        this.fecRadicadoPresentacionRecurso = value;
    }

    /**
     * Obtiene el valor de la propiedad esPresentacion10Dias.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPresentacion10Dias() {
        return esPresentacion10Dias;
    }

    /**
     * Define el valor de la propiedad esPresentacion10Dias.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPresentacion10Dias(String value) {
        this.esPresentacion10Dias = value;
    }

    /**
     * Obtiene el valor de la propiedad esPresentacionExtemporanea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPresentacionExtemporanea() {
        return esPresentacionExtemporanea;
    }

    /**
     * Define el valor de la propiedad esPresentacionExtemporanea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPresentacionExtemporanea(String value) {
        this.esPresentacionExtemporanea = value;
    }

    /**
     * Obtiene el valor de la propiedad esPresentacionPersonal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPresentacionPersonal() {
        return esPresentacionPersonal;
    }

    /**
     * Define el valor de la propiedad esPresentacionPersonal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPresentacionPersonal(String value) {
        this.esPresentacionPersonal = value;
    }

    /**
     * Obtiene el valor de la propiedad esAcreditacionCalidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAcreditacionCalidad() {
        return esAcreditacionCalidad;
    }

    /**
     * Define el valor de la propiedad esAcreditacionCalidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAcreditacionCalidad(String value) {
        this.esAcreditacionCalidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codNivelComplejidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodNivelComplejidad() {
        return codNivelComplejidad;
    }

    /**
     * Define el valor de la propiedad codNivelComplejidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodNivelComplejidad(String value) {
        this.codNivelComplejidad = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoAdmisibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoAdmisibilidad() {
        return codTipoAdmisibilidad;
    }

    /**
     * Define el valor de la propiedad codTipoAdmisibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoAdmisibilidad(String value) {
        this.codTipoAdmisibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad desTipoAdmisibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesTipoAdmisibilidad() {
        return desTipoAdmisibilidad;
    }

    /**
     * Define el valor de la propiedad desTipoAdmisibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesTipoAdmisibilidad(String value) {
        this.desTipoAdmisibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad esSubsanarAdmisibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSubsanarAdmisibilidad() {
        return esSubsanarAdmisibilidad;
    }

    /**
     * Define el valor de la propiedad esSubsanarAdmisibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSubsanarAdmisibilidad(String value) {
        this.esSubsanarAdmisibilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad esInformacionCompleta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInformacionCompleta() {
        return esInformacionCompleta;
    }

    /**
     * Define el valor de la propiedad esInformacionCompleta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInformacionCompleta(String value) {
        this.esInformacionCompleta = value;
    }

    /**
     * Obtiene el valor de la propiedad desSubsanarAdmisibilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesSubsanarAdmisibilidad() {
        return desSubsanarAdmisibilidad;
    }

    /**
     * Define el valor de la propiedad desSubsanarAdmisibilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesSubsanarAdmisibilidad(String value) {
        this.desSubsanarAdmisibilidad = value;
    }

    
    /**
     * Gets the value of the solicitudPruebas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudPruebas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudPruebas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformacionPruebaTipo }
     * 
     * 
     */
    public List<InformacionPruebaTipo> getSolicitudPruebas() {
        if (solicitudPruebas == null) {
            solicitudPruebas = new ArrayList<InformacionPruebaTipo>();
        }
        return this.solicitudPruebas;
    }

    /**
     * Gets the value of the validacionDocumentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validacionDocumentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidacionDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ValidacionDocumentoTipo }
     * 
     * 
     */
    public List<ValidacionDocumentoTipo> getValidacionDocumentos() {
        if (validacionDocumentos == null) {
            validacionDocumentos = new ArrayList<ValidacionDocumentoTipo>();
        }
        return this.validacionDocumentos;
    }

    /**
     * Obtiene el valor de la propiedad documento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocumento() {
        return documento;
    }

    /**
     * Define el valor de la propiedad documento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocumento(DocumentoTipo value) {
        this.documento = value;
    }

}
