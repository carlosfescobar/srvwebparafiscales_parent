
package co.gov.ugpp.schema.sancion.analisissanciontipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;


/**
 * <p>Clase Java para AnalisisSancionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AnalisisSancionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAnalisisSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uvt" type="{http://www.ugpp.gov.co/schema/Sancion/UVTTipo/v1}UVTTipo" minOccurs="0"/>
 *         &lt;element name="fecVencimientoTermino" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecLlegadaExtemporanea" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="diasIncumplimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSancionNumeros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valSancionLetras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionesImporteSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnalisisSancionTipo", propOrder = {
    "idAnalisisSancion",
    "uvt",
    "fecVencimientoTermino",
    "fecLlegadaExtemporanea",
    "diasIncumplimiento",
    "valSancionNumeros",
    "valSancionLetras",
    "desObservacionesImporteSancion"
})
public class AnalisisSancionTipo {

    @XmlElement(nillable = true)
    protected String idAnalisisSancion;
    @XmlElement(nillable = true)
    protected UVTTipo uvt;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecVencimientoTermino;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecLlegadaExtemporanea;
    @XmlElement(nillable = true)
    protected String diasIncumplimiento;
    @XmlElement(nillable = true)
    protected String valSancionNumeros;
    @XmlElement(nillable = true)
    protected String valSancionLetras;
    @XmlElement(nillable = true)
    protected String desObservacionesImporteSancion;

    /**
     * Obtiene el valor de la propiedad idAnalisisSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAnalisisSancion() {
        return idAnalisisSancion;
    }

    /**
     * Define el valor de la propiedad idAnalisisSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAnalisisSancion(String value) {
        this.idAnalisisSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad uvt.
     * 
     * @return
     *     possible object is
     *     {@link UVTTipo }
     *     
     */
    public UVTTipo getUvt() {
        return uvt;
    }

    /**
     * Define el valor de la propiedad uvt.
     * 
     * @param value
     *     allowed object is
     *     {@link UVTTipo }
     *     
     */
    public void setUvt(UVTTipo value) {
        this.uvt = value;
    }

    /**
     * Obtiene el valor de la propiedad fecVencimientoTermino.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecVencimientoTermino() {
        return fecVencimientoTermino;
    }

    /**
     * Define el valor de la propiedad fecVencimientoTermino.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecVencimientoTermino(Calendar value) {
        this.fecVencimientoTermino = value;
    }

    /**
     * Obtiene el valor de la propiedad fecLlegadaExtemporanea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecLlegadaExtemporanea() {
        return fecLlegadaExtemporanea;
    }

    /**
     * Define el valor de la propiedad fecLlegadaExtemporanea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecLlegadaExtemporanea(Calendar value) {
        this.fecLlegadaExtemporanea = value;
    }

    /**
     * Obtiene el valor de la propiedad diasIncumplimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDiasIncumplimiento() {
        return diasIncumplimiento;
    }

    /**
     * Define el valor de la propiedad diasIncumplimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDiasIncumplimiento(String value) {
        this.diasIncumplimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad valSancionNumeros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSancionNumeros() {
        return valSancionNumeros;
    }

    /**
     * Define el valor de la propiedad valSancionNumeros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSancionNumeros(String value) {
        this.valSancionNumeros = value;
    }

    /**
     * Obtiene el valor de la propiedad valSancionLetras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValSancionLetras() {
        return valSancionLetras;
    }

    /**
     * Define el valor de la propiedad valSancionLetras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValSancionLetras(String value) {
        this.valSancionLetras = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionesImporteSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionesImporteSancion() {
        return desObservacionesImporteSancion;
    }

    /**
     * Define el valor de la propiedad desObservacionesImporteSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionesImporteSancion(String value) {
        this.desObservacionesImporteSancion = value;
    }

}
