
package co.gov.ugpp.schema.sancion.informacionpruebatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;


/**
 * <p>Clase Java para InformacionPruebaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InformacionPruebaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esRequiereInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valAreaSeleccionada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desInformacionRequerida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codPersonaSolicitudExterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="funcionarioResuelveInterna" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentosRespuestaInterna" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="desRespuestaInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInformacionExternaCompleta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requerimientoInformacionParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoRequerimientoInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformacionPruebaTipo", propOrder = {
    "idInformacionPrueba",
    "esRequiereInformacion",
    "codTipoRequerimiento",
    "valAreaSeleccionada",
    "desInformacionRequerida",
    "codPersonaSolicitudExterna",
    "entidadExterna",
    "funcionarioResuelveInterna",
    "idDocumentosRespuestaInterna",
    "desRespuestaInformacion",
    "esInformacionExternaCompleta",
    "requerimientoInformacionParcial",
    "idActoRequerimientoInformacion"
})
public class InformacionPruebaTipo {

    @XmlElement(nillable = true)
    protected String idInformacionPrueba;
    @XmlElement(nillable = true)
    protected String esRequiereInformacion;
    @XmlElement(nillable = true)
    protected String codTipoRequerimiento;
    @XmlElement(nillable = true)
    protected String valAreaSeleccionada;
    @XmlElement(nillable = true)
    protected String desInformacionRequerida;
    @XmlElement(nillable = true)
    protected String codPersonaSolicitudExterna;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected FuncionarioTipo funcionarioResuelveInterna;
    @XmlElement(nillable = true)
    protected List<String> idDocumentosRespuestaInterna;
    @XmlElement(nillable = true)
    protected String desRespuestaInformacion;
    @XmlElement(nillable = true)
    protected String esInformacionExternaCompleta;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo requerimientoInformacionParcial;
    @XmlElement(nillable = true)
    protected String idActoRequerimientoInformacion;

    /**
     * Obtiene el valor de la propiedad idInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInformacionPrueba() {
        return idInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad idInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInformacionPrueba(String value) {
        this.idInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad esRequiereInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsRequiereInformacion() {
        return esRequiereInformacion;
    }

    /**
     * Define el valor de la propiedad esRequiereInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsRequiereInformacion(String value) {
        this.esRequiereInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoRequerimiento() {
        return codTipoRequerimiento;
    }

    /**
     * Define el valor de la propiedad codTipoRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoRequerimiento(String value) {
        this.codTipoRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad valAreaSeleccionada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValAreaSeleccionada() {
        return valAreaSeleccionada;
    }

    /**
     * Define el valor de la propiedad valAreaSeleccionada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValAreaSeleccionada(String value) {
        this.valAreaSeleccionada = value;
    }

    /**
     * Obtiene el valor de la propiedad desInformacionRequerida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesInformacionRequerida() {
        return desInformacionRequerida;
    }

    /**
     * Define el valor de la propiedad desInformacionRequerida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesInformacionRequerida(String value) {
        this.desInformacionRequerida = value;
    }

    /**
     * Obtiene el valor de la propiedad codPersonaSolicitudExterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodPersonaSolicitudExterna() {
        return codPersonaSolicitudExterna;
    }

    /**
     * Define el valor de la propiedad codPersonaSolicitudExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodPersonaSolicitudExterna(String value) {
        this.codPersonaSolicitudExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionarioResuelveInterna.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionarioResuelveInterna() {
        return funcionarioResuelveInterna;
    }

    /**
     * Define el valor de la propiedad funcionarioResuelveInterna.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionarioResuelveInterna(FuncionarioTipo value) {
        this.funcionarioResuelveInterna = value;
    }

    /**
     * Gets the value of the idDocumentosRespuestaInterna property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentosRespuestaInterna property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentosRespuestaInterna().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentosRespuestaInterna() {
        if (idDocumentosRespuestaInterna == null) {
            idDocumentosRespuestaInterna = new ArrayList<String>();
        }
        return this.idDocumentosRespuestaInterna;
    }

    /**
     * Obtiene el valor de la propiedad desRespuestaInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesRespuestaInformacion() {
        return desRespuestaInformacion;
    }

    /**
     * Define el valor de la propiedad desRespuestaInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesRespuestaInformacion(String value) {
        this.desRespuestaInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esInformacionExternaCompleta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInformacionExternaCompleta() {
        return esInformacionExternaCompleta;
    }

    /**
     * Define el valor de la propiedad esInformacionExternaCompleta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInformacionExternaCompleta(String value) {
        this.esInformacionExternaCompleta = value;
    }

    /**
     * Obtiene el valor de la propiedad requerimientoInformacionParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getRequerimientoInformacionParcial() {
        return requerimientoInformacionParcial;
    }

    /**
     * Define el valor de la propiedad requerimientoInformacionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setRequerimientoInformacionParcial(AprobacionDocumentoTipo value) {
        this.requerimientoInformacionParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoRequerimientoInformacion() {
        return idActoRequerimientoInformacion;
    }

    /**
     * Define el valor de la propiedad idActoRequerimientoInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoRequerimientoInformacion(String value) {
        this.idActoRequerimientoInformacion = value;
    }

}
