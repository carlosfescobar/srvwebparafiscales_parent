
package co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.sancion.uvttipo.v1.UVTTipo;


/**
 * <p>Clase Java para LiquidacionParcialTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LiquidacionParcialTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idLiquidacionParcial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uvt" type="{http://www.ugpp.gov.co/schema/Sancion/UVTTipo/v1}UVTTipo" minOccurs="0"/>
 *         &lt;element name="codAccionLiquidacionParcial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LiquidacionParcialTipo", propOrder = {
    "idLiquidacionParcial",
    "uvt",
    "codAccionLiquidacionParcial",
    "desObservaciones"
})
public class LiquidacionParcialTipo {

    @XmlElement(nillable = true)
    protected String idLiquidacionParcial;
    @XmlElement(nillable = true)
    protected UVTTipo uvt;
    @XmlElement(nillable = true)
    protected String codAccionLiquidacionParcial;
    @XmlElement(nillable = true)
    protected String desObservaciones;

    /**
     * Obtiene el valor de la propiedad idLiquidacionParcial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidacionParcial() {
        return idLiquidacionParcial;
    }

    /**
     * Define el valor de la propiedad idLiquidacionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidacionParcial(String value) {
        this.idLiquidacionParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad uvt.
     * 
     * @return
     *     possible object is
     *     {@link UVTTipo }
     *     
     */
    public UVTTipo getUvt() {
        return uvt;
    }

    /**
     * Define el valor de la propiedad uvt.
     * 
     * @param value
     *     allowed object is
     *     {@link UVTTipo }
     *     
     */
    public void setUvt(UVTTipo value) {
        this.uvt = value;
    }

    /**
     * Obtiene el valor de la propiedad codAccionLiquidacionParcial.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAccionLiquidacionParcial() {
        return codAccionLiquidacionParcial;
    }

    /**
     * Define el valor de la propiedad codAccionLiquidacionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAccionLiquidacionParcial(String value) {
        this.codAccionLiquidacionParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservaciones() {
        return desObservaciones;
    }

    /**
     * Define el valor de la propiedad desObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservaciones(String value) {
        this.desObservaciones = value;
    }

}
