
package co.gov.ugpp.schema.sancion.sanciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.conceptopersonatipo.v1.ConceptoPersonaTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.comunes.acciontipo.v1.AccionTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.aprobaciondocumentotipo.v1.AprobacionDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.sancion.analisissanciontipo.v1.AnalisisSancionTipo;
import co.gov.ugpp.schema.sancion.informacionpruebatipo.v1.InformacionPruebaTipo;
import co.gov.ugpp.schema.sancion.liquidacionparcialtipo.v1.LiquidacionParcialTipo;


/**
 * <p>Clase Java para SancionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SancionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProcesoPadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProcesoPadre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expedienteCaso" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="expedienteSancion" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="codTipoSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="investigado" type="{http://www.ugpp.gov.co/esb/schema/ConceptoPersonaTipo/v1}ConceptoPersonaTipo" minOccurs="0"/>
 *         &lt;element name="gestorSancion" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="informacionRequerida" type="{http://www.ugpp.gov.co/schema/Sancion/InformacionPruebaTipo/v1}InformacionPruebaTipo" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="pruebaRequerida" type="{http://www.ugpp.gov.co/schema/Sancion/InformacionPruebaTipo/v1}InformacionPruebaTipo" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="esSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionesNoProcedeSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="analisisSancion" type="{http://www.ugpp.gov.co/schema/Sancion/AnalisisSancionTipo/v1}AnalisisSancionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="pliegoCargosParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoPliegoCargos" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resolucionSancionParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoResolucionSancion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="autoArchivoParcial" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AprobacionDocumentoTipo/v1}AprobacionDocumentoTipo" minOccurs="0"/>
 *         &lt;element name="idActoAutoArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRecursoReconsideracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idConstanciaEjecutoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="liquidacionesParciales" type="{http://www.ugpp.gov.co/schema/Sancion/LiquidacionParcialTipo/v1}LiquidacionParcialTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="acciones" type="{http://www.ugpp.gov.co/schema/Comunes/AccionTipo/v1}AccionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fecLlegadaExtemporaneaFiscalizacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codDecisionExtemporaneaLiquidacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desObservacionDecisionExtemporanea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SancionTipo", propOrder = {
    "idSancion",
    "codProcesoPadre",
    "idProcesoPadre",
    "expedienteCaso",
    "expedienteSancion",
    "codTipoSancion",
    "codEstadoSancion",
    "investigado",
    "gestorSancion",
    "informacionRequerida",
    "pruebaRequerida",
    "esSancion",
    "desObservacionesNoProcedeSancion",
    "analisisSancion",
    "pliegoCargosParcial",
    "idActoPliegoCargos",
    "resolucionSancionParcial",
    "idActoResolucionSancion",
    "autoArchivoParcial",
    "idActoAutoArchivo",
    "idRecursoReconsideracion",
    "idConstanciaEjecutoria",
    "liquidacionesParciales",
    "acciones",
    "fecLlegadaExtemporaneaFiscalizacion",
    "codDecisionExtemporaneaLiquidacion",
    "desObservacionDecisionExtemporanea"
})
public class SancionTipo {

    @XmlElement(nillable = true)
    protected String idSancion;
    @XmlElement(nillable = true)
    protected String codProcesoPadre;
    @XmlElement(nillable = true)
    protected String idProcesoPadre;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expedienteCaso;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expedienteSancion;
    @XmlElement(nillable = true)
    protected String codTipoSancion;
    @XmlElement(nillable = true)
    protected String codEstadoSancion;
    @XmlElement(nillable = true)
    protected ConceptoPersonaTipo investigado;
    @XmlElement(nillable = true)
    protected FuncionarioTipo gestorSancion;
    @XmlElement(nillable = true)
    protected List<InformacionPruebaTipo> informacionRequerida;
    @XmlElement(nillable = true)
    protected List<InformacionPruebaTipo> pruebaRequerida;
    @XmlElement(nillable = true)
    protected String esSancion;
    @XmlElement(nillable = true)
    protected String desObservacionesNoProcedeSancion;
    @XmlElement(nillable = true)
    protected List<AnalisisSancionTipo> analisisSancion;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo pliegoCargosParcial;
    @XmlElement(nillable = true)
    protected String idActoPliegoCargos;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo resolucionSancionParcial;
    @XmlElement(nillable = true)
    protected String idActoResolucionSancion;
    @XmlElement(nillable = true)
    protected AprobacionDocumentoTipo autoArchivoParcial;
    @XmlElement(nillable = true)
    protected String idActoAutoArchivo;
    @XmlElement(nillable = true)
    protected String idRecursoReconsideracion;
    @XmlElement(nillable = true)
    protected String idConstanciaEjecutoria;
    @XmlElement(nillable = true)
    protected List<LiquidacionParcialTipo> liquidacionesParciales;
    @XmlElement(nillable = true)
    protected List<AccionTipo> acciones;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecLlegadaExtemporaneaFiscalizacion;
    @XmlElement(nillable = true)
    protected String codDecisionExtemporaneaLiquidacion;
    @XmlElement(nillable = true)
    protected String desObservacionDecisionExtemporanea;

    /**
     * Obtiene el valor de la propiedad idSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSancion() {
        return idSancion;
    }

    /**
     * Define el valor de la propiedad idSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSancion(String value) {
        this.idSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad codProcesoPadre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProcesoPadre() {
        return codProcesoPadre;
    }

    /**
     * Define el valor de la propiedad codProcesoPadre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProcesoPadre(String value) {
        this.codProcesoPadre = value;
    }

    /**
     * Obtiene el valor de la propiedad idProcesoPadre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProcesoPadre() {
        return idProcesoPadre;
    }

    /**
     * Define el valor de la propiedad idProcesoPadre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProcesoPadre(String value) {
        this.idProcesoPadre = value;
    }

    /**
     * Obtiene el valor de la propiedad expedienteCaso.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpedienteCaso() {
        return expedienteCaso;
    }

    /**
     * Define el valor de la propiedad expedienteCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpedienteCaso(ExpedienteTipo value) {
        this.expedienteCaso = value;
    }

    /**
     * Obtiene el valor de la propiedad expedienteSancion.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpedienteSancion() {
        return expedienteSancion;
    }

    /**
     * Define el valor de la propiedad expedienteSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpedienteSancion(ExpedienteTipo value) {
        this.expedienteSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSancion() {
        return codTipoSancion;
    }

    /**
     * Define el valor de la propiedad codTipoSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSancion(String value) {
        this.codTipoSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoSancion() {
        return codEstadoSancion;
    }

    /**
     * Define el valor de la propiedad codEstadoSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoSancion(String value) {
        this.codEstadoSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad investigado.
     * 
     * @return
     *     possible object is
     *     {@link ConceptoPersonaTipo }
     *     
     */
    public ConceptoPersonaTipo getInvestigado() {
        return investigado;
    }

    /**
     * Define el valor de la propiedad investigado.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptoPersonaTipo }
     *     
     */
    public void setInvestigado(ConceptoPersonaTipo value) {
        this.investigado = value;
    }

    /**
     * Obtiene el valor de la propiedad gestorSancion.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getGestorSancion() {
        return gestorSancion;
    }

    /**
     * Define el valor de la propiedad gestorSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setGestorSancion(FuncionarioTipo value) {
        this.gestorSancion = value;
    }

    /**
     * Gets the value of the informacionRequerida property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the informacionRequerida property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInformacionRequerida().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformacionPruebaTipo }
     * 
     * 
     */
    public List<InformacionPruebaTipo> getInformacionRequerida() {
        if (informacionRequerida == null) {
            informacionRequerida = new ArrayList<InformacionPruebaTipo>();
        }
        return this.informacionRequerida;
    }

    /**
     * Gets the value of the pruebaRequerida property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pruebaRequerida property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPruebaRequerida().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InformacionPruebaTipo }
     * 
     * 
     */
    public List<InformacionPruebaTipo> getPruebaRequerida() {
        if (pruebaRequerida == null) {
            pruebaRequerida = new ArrayList<InformacionPruebaTipo>();
        }
        return this.pruebaRequerida;
    }

    /**
     * Obtiene el valor de la propiedad esSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSancion() {
        return esSancion;
    }

    /**
     * Define el valor de la propiedad esSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSancion(String value) {
        this.esSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionesNoProcedeSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionesNoProcedeSancion() {
        return desObservacionesNoProcedeSancion;
    }

    /**
     * Define el valor de la propiedad desObservacionesNoProcedeSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionesNoProcedeSancion(String value) {
        this.desObservacionesNoProcedeSancion = value;
    }

    /**
     * Gets the value of the analisisSancion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the analisisSancion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnalisisSancion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnalisisSancionTipo }
     * 
     * 
     */
    public List<AnalisisSancionTipo> getAnalisisSancion() {
        if (analisisSancion == null) {
            analisisSancion = new ArrayList<AnalisisSancionTipo>();
        }
        return this.analisisSancion;
    }

    /**
     * Obtiene el valor de la propiedad pliegoCargosParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getPliegoCargosParcial() {
        return pliegoCargosParcial;
    }

    /**
     * Define el valor de la propiedad pliegoCargosParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setPliegoCargosParcial(AprobacionDocumentoTipo value) {
        this.pliegoCargosParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoPliegoCargos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoPliegoCargos() {
        return idActoPliegoCargos;
    }

    /**
     * Define el valor de la propiedad idActoPliegoCargos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoPliegoCargos(String value) {
        this.idActoPliegoCargos = value;
    }

    /**
     * Obtiene el valor de la propiedad resolucionSancionParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getResolucionSancionParcial() {
        return resolucionSancionParcial;
    }

    /**
     * Define el valor de la propiedad resolucionSancionParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setResolucionSancionParcial(AprobacionDocumentoTipo value) {
        this.resolucionSancionParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoResolucionSancion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoResolucionSancion() {
        return idActoResolucionSancion;
    }

    /**
     * Define el valor de la propiedad idActoResolucionSancion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoResolucionSancion(String value) {
        this.idActoResolucionSancion = value;
    }

    /**
     * Obtiene el valor de la propiedad autoArchivoParcial.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public AprobacionDocumentoTipo getAutoArchivoParcial() {
        return autoArchivoParcial;
    }

    /**
     * Define el valor de la propiedad autoArchivoParcial.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionDocumentoTipo }
     *     
     */
    public void setAutoArchivoParcial(AprobacionDocumentoTipo value) {
        this.autoArchivoParcial = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAutoArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAutoArchivo() {
        return idActoAutoArchivo;
    }

    /**
     * Define el valor de la propiedad idActoAutoArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAutoArchivo(String value) {
        this.idActoAutoArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad idRecursoReconsideracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRecursoReconsideracion() {
        return idRecursoReconsideracion;
    }

    /**
     * Define el valor de la propiedad idRecursoReconsideracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRecursoReconsideracion(String value) {
        this.idRecursoReconsideracion = value;
    }

    /**
     * Obtiene el valor de la propiedad idConstanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConstanciaEjecutoria() {
        return idConstanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad idConstanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConstanciaEjecutoria(String value) {
        this.idConstanciaEjecutoria = value;
    }

    /**
     * Gets the value of the liquidacionesParciales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the liquidacionesParciales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLiquidacionesParciales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LiquidacionParcialTipo }
     * 
     * 
     */
    public List<LiquidacionParcialTipo> getLiquidacionesParciales() {
        if (liquidacionesParciales == null) {
            liquidacionesParciales = new ArrayList<LiquidacionParcialTipo>();
        }
        return this.liquidacionesParciales;
    }

    /**
     * Gets the value of the acciones property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the acciones property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAcciones().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AccionTipo }
     * 
     * 
     */
    public List<AccionTipo> getAcciones() {
        if (acciones == null) {
            acciones = new ArrayList<AccionTipo>();
        }
        return this.acciones;
    }

    /**
     * Obtiene el valor de la propiedad fecLlegadaExtemporaneaFiscalizacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecLlegadaExtemporaneaFiscalizacion() {
        return fecLlegadaExtemporaneaFiscalizacion;
    }

    /**
     * Define el valor de la propiedad fecLlegadaExtemporaneaFiscalizacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecLlegadaExtemporaneaFiscalizacion(Calendar value) {
        this.fecLlegadaExtemporaneaFiscalizacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codDecisionExtemporaneaLiquidacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodDecisionExtemporaneaLiquidacion() {
        return codDecisionExtemporaneaLiquidacion;
    }

    /**
     * Define el valor de la propiedad codDecisionExtemporaneaLiquidacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodDecisionExtemporaneaLiquidacion(String value) {
        this.codDecisionExtemporaneaLiquidacion = value;
    }

    /**
     * Obtiene el valor de la propiedad desObservacionDecisionExtemporanea.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesObservacionDecisionExtemporanea() {
        return desObservacionDecisionExtemporanea;
    }

    /**
     * Define el valor de la propiedad desObservacionDecisionExtemporanea.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesObservacionDecisionExtemporanea(String value) {
        this.desObservacionDecisionExtemporanea = value;
    }

}
