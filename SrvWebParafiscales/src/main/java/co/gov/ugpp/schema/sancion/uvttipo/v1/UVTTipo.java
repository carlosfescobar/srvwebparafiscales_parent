
package co.gov.ugpp.schema.sancion.uvttipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.rangofechatipo.v1.RangoFechaTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para UVTTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="UVTTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idUVT" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUVTNumeros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUVTLetras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCincoUVTNumeros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCincoUVTLetras" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNumeroResolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecResolucion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="periodoVigencia" type="{http://www.ugpp.gov.co/esb/schema/RangoFechaTipo/v1}RangoFechaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UVTTipo", propOrder = {
    "idUVT",
    "valUVTNumeros",
    "valUVTLetras",
    "valCincoUVTNumeros",
    "valCincoUVTLetras",
    "valNumeroResolucion",
    "fecResolucion",
    "periodoVigencia"
})
public class UVTTipo {

    @XmlElement(nillable = true)
    protected String idUVT;
    @XmlElement(nillable = true)
    protected String valUVTNumeros;
    @XmlElement(nillable = true)
    protected String valUVTLetras;
    @XmlElement(nillable = true)
    protected String valCincoUVTNumeros;
    @XmlElement(nillable = true)
    protected String valCincoUVTLetras;
    @XmlElement(nillable = true)
    protected String valNumeroResolucion;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecResolucion;
    protected RangoFechaTipo periodoVigencia;

    /**
     * Obtiene el valor de la propiedad idUVT.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUVT() {
        return idUVT;
    }

    /**
     * Define el valor de la propiedad idUVT.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUVT(String value) {
        this.idUVT = value;
    }

    /**
     * Obtiene el valor de la propiedad valUVTNumeros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUVTNumeros() {
        return valUVTNumeros;
    }

    /**
     * Define el valor de la propiedad valUVTNumeros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUVTNumeros(String value) {
        this.valUVTNumeros = value;
    }

    /**
     * Obtiene el valor de la propiedad valUVTLetras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUVTLetras() {
        return valUVTLetras;
    }

    /**
     * Define el valor de la propiedad valUVTLetras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUVTLetras(String value) {
        this.valUVTLetras = value;
    }

    /**
     * Obtiene el valor de la propiedad valCincoUVTNumeros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCincoUVTNumeros() {
        return valCincoUVTNumeros;
    }

    /**
     * Define el valor de la propiedad valCincoUVTNumeros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCincoUVTNumeros(String value) {
        this.valCincoUVTNumeros = value;
    }

    /**
     * Obtiene el valor de la propiedad valCincoUVTLetras.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCincoUVTLetras() {
        return valCincoUVTLetras;
    }

    /**
     * Define el valor de la propiedad valCincoUVTLetras.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCincoUVTLetras(String value) {
        this.valCincoUVTLetras = value;
    }

    /**
     * Obtiene el valor de la propiedad valNumeroResolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNumeroResolucion() {
        return valNumeroResolucion;
    }

    /**
     * Define el valor de la propiedad valNumeroResolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNumeroResolucion(String value) {
        this.valNumeroResolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad fecResolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecResolucion() {
        return fecResolucion;
    }

    /**
     * Define el valor de la propiedad fecResolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecResolucion(Calendar value) {
        this.fecResolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad periodoVigencia.
     * 
     * @return
     *     possible object is
     *     {@link RangoFechaTipo }
     *     
     */
    public RangoFechaTipo getPeriodoVigencia() {
        return periodoVigencia;
    }

    /**
     * Define el valor de la propiedad periodoVigencia.
     * 
     * @param value
     *     allowed object is
     *     {@link RangoFechaTipo }
     *     
     */
    public void setPeriodoVigencia(RangoFechaTipo value) {
        this.periodoVigencia = value;
    }

}
