
package co.gov.ugpp.schema.sanciones.informacionpruebatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para InformacionPruebaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InformacionPruebaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esRequiereInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoSolicitudInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="funcionario" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo"/>
 *         &lt;element name="esCompletaInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desCompletaInformacionPrueba" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esInformacionPruebaRecibida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desInformacionPruebaRecibida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="documentos" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="esSolicitaInfoAdicional" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esRequiereInformacionTributaria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esPruebaSuficiente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InformacionPruebaTipo", propOrder = {
    "idInformacionPrueba",
    "codTipoInformacionPrueba",
    "esRequiereInformacion",
    "codTipoSolicitudInformacionPrueba",
    "desInformacionPrueba",
    "funcionario",
    "esCompletaInformacionPrueba",
    "desCompletaInformacionPrueba",
    "esInformacionPruebaRecibida",
    "desInformacionPruebaRecibida",
    "documentos",
    "esSolicitaInfoAdicional",
    "esRequiereInformacionTributaria",
    "esPruebaSuficiente",
    "expediente"
})
public class InformacionPruebaTipo {

    @XmlElement(nillable = true)
    protected String idInformacionPrueba;
    @XmlElement(nillable = true)
    protected String codTipoInformacionPrueba;
    @XmlElement(nillable = true)
    protected String esRequiereInformacion;
    @XmlElement(nillable = true)
    protected String codTipoSolicitudInformacionPrueba;
    @XmlElement(nillable = true)
    protected String desInformacionPrueba;
    @XmlElement(required = true, nillable = true)
    protected FuncionarioTipo funcionario;
    @XmlElement(nillable = true)
    protected String esCompletaInformacionPrueba;
    @XmlElement(nillable = true)
    protected String desCompletaInformacionPrueba;
    @XmlElement(nillable = true)
    protected String esInformacionPruebaRecibida;
    @XmlElement(nillable = true)
    protected String desInformacionPruebaRecibida;
    @XmlElement(nillable = true)
    protected List<DocumentoTipo> documentos;
    @XmlElement(nillable = true)
    protected String esSolicitaInfoAdicional;
    @XmlElement(nillable = true)
    protected String esRequiereInformacionTributaria;
    @XmlElement(nillable = true)
    protected String esPruebaSuficiente;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;

    /**
     * Obtiene el valor de la propiedad idInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInformacionPrueba() {
        return idInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad idInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInformacionPrueba(String value) {
        this.idInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoInformacionPrueba() {
        return codTipoInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad codTipoInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoInformacionPrueba(String value) {
        this.codTipoInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad esRequiereInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsRequiereInformacion() {
        return esRequiereInformacion;
    }

    /**
     * Define el valor de la propiedad esRequiereInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsRequiereInformacion(String value) {
        this.esRequiereInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSolicitudInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSolicitudInformacionPrueba() {
        return codTipoSolicitudInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad codTipoSolicitudInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSolicitudInformacionPrueba(String value) {
        this.codTipoSolicitudInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad desInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesInformacionPrueba() {
        return desInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad desInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesInformacionPrueba(String value) {
        this.desInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionario.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionario() {
        return funcionario;
    }

    /**
     * Define el valor de la propiedad funcionario.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionario(FuncionarioTipo value) {
        this.funcionario = value;
    }

    /**
     * Obtiene el valor de la propiedad esCompletaInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsCompletaInformacionPrueba() {
        return esCompletaInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad esCompletaInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsCompletaInformacionPrueba(String value) {
        this.esCompletaInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad desCompletaInformacionPrueba.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCompletaInformacionPrueba() {
        return desCompletaInformacionPrueba;
    }

    /**
     * Define el valor de la propiedad desCompletaInformacionPrueba.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCompletaInformacionPrueba(String value) {
        this.desCompletaInformacionPrueba = value;
    }

    /**
     * Obtiene el valor de la propiedad esInformacionPruebaRecibida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsInformacionPruebaRecibida() {
        return esInformacionPruebaRecibida;
    }

    /**
     * Define el valor de la propiedad esInformacionPruebaRecibida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsInformacionPruebaRecibida(String value) {
        this.esInformacionPruebaRecibida = value;
    }

    /**
     * Obtiene el valor de la propiedad desInformacionPruebaRecibida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesInformacionPruebaRecibida() {
        return desInformacionPruebaRecibida;
    }

    /**
     * Define el valor de la propiedad desInformacionPruebaRecibida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesInformacionPruebaRecibida(String value) {
        this.desInformacionPruebaRecibida = value;
    }

    /**
     * Gets the value of the documentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoTipo }
     * 
     * 
     */
    public List<DocumentoTipo> getDocumentos() {
        if (documentos == null) {
            documentos = new ArrayList<DocumentoTipo>();
        }
        return this.documentos;
    }

    /**
     * Obtiene el valor de la propiedad esSolicitaInfoAdicional.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSolicitaInfoAdicional() {
        return esSolicitaInfoAdicional;
    }

    /**
     * Define el valor de la propiedad esSolicitaInfoAdicional.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSolicitaInfoAdicional(String value) {
        this.esSolicitaInfoAdicional = value;
    }

    /**
     * Obtiene el valor de la propiedad esRequiereInformacionTributaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsRequiereInformacionTributaria() {
        return esRequiereInformacionTributaria;
    }

    /**
     * Define el valor de la propiedad esRequiereInformacionTributaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsRequiereInformacionTributaria(String value) {
        this.esRequiereInformacionTributaria = value;
    }

    /**
     * Obtiene el valor de la propiedad esPruebaSuficiente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPruebaSuficiente() {
        return esPruebaSuficiente;
    }

    /**
     * Define el valor de la propiedad esPruebaSuficiente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPruebaSuficiente(String value) {
        this.esPruebaSuficiente = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

}
