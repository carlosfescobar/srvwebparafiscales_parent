
package co.gov.ugpp.schema.sanciones.investigadotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personajuridicatipo.v1.PersonaJuridicaTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para InvestigadoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="InvestigadoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="personaJuridica" type="{http://www.ugpp.gov.co/esb/schema/PersonaJuridicaTipo/v1}PersonaJuridicaTipo"/>
 *           &lt;element name="personaNatural" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo"/>
 *         &lt;/choice>
 *         &lt;element name="codTipoInvestigado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvestigadoTipo", propOrder = {
    "personaJuridica",
    "personaNatural",
    "codTipoInvestigado"
})
public class InvestigadoTipo {

    @XmlElement(nillable = true)
    protected PersonaJuridicaTipo personaJuridica;
    @XmlElement(nillable = true)
    protected PersonaNaturalTipo personaNatural;
    @XmlElement(nillable = true)
    protected String codTipoInvestigado;

    /**
     * Obtiene el valor de la propiedad personaJuridica.
     * 
     * @return
     *     possible object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public PersonaJuridicaTipo getPersonaJuridica() {
        return personaJuridica;
    }

    /**
     * Define el valor de la propiedad personaJuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaJuridicaTipo }
     *     
     */
    public void setPersonaJuridica(PersonaJuridicaTipo value) {
        this.personaJuridica = value;
    }

    /**
     * Obtiene el valor de la propiedad personaNatural.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNatural() {
        return personaNatural;
    }

    /**
     * Define el valor de la propiedad personaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNatural(PersonaNaturalTipo value) {
        this.personaNatural = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoInvestigado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoInvestigado() {
        return codTipoInvestigado;
    }

    /**
     * Define el valor de la propiedad codTipoInvestigado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoInvestigado(String value) {
        this.codTipoInvestigado = value;
    }

}
