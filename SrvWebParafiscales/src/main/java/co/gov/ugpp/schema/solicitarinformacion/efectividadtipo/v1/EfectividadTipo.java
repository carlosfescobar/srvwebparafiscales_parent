
package co.gov.ugpp.schema.solicitarinformacion.efectividadtipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para EfectividadTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EfectividadTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cantidadSolicitudes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="porcentajeEfectividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codResultadoEfectividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EfectividadTipo", propOrder = {
    "cantidadSolicitudes",
    "porcentajeEfectividad",
    "codResultadoEfectividad"
})
public class EfectividadTipo {

    @XmlElement(nillable = true)
    protected String cantidadSolicitudes;
    @XmlElement(nillable = true)
    protected String porcentajeEfectividad;
    @XmlElement(nillable = true)
    protected String codResultadoEfectividad;

    /**
     * Obtiene el valor de la propiedad cantidadSolicitudes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadSolicitudes() {
        return cantidadSolicitudes;
    }

    /**
     * Define el valor de la propiedad cantidadSolicitudes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadSolicitudes(String value) {
        this.cantidadSolicitudes = value;
    }

    /**
     * Obtiene el valor de la propiedad porcentajeEfectividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPorcentajeEfectividad() {
        return porcentajeEfectividad;
    }

    /**
     * Define el valor de la propiedad porcentajeEfectividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPorcentajeEfectividad(String value) {
        this.porcentajeEfectividad = value;
    }

    /**
     * Obtiene el valor de la propiedad codResultadoEfectividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodResultadoEfectividad() {
        return codResultadoEfectividad;
    }

    /**
     * Define el valor de la propiedad codResultadoEfectividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodResultadoEfectividad(String value) {
        this.codResultadoEfectividad = value;
    }

}
