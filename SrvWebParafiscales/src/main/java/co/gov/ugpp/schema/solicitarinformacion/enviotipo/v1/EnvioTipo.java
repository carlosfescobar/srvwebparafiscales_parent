
package co.gov.ugpp.schema.solicitarinformacion.enviotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;


/**
 * <p>Clase Java para EnvioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EnvioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="identificacion" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="radicacion" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/RadicacionTipo/v1}RadicacionTipo" minOccurs="0"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="formato" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="archivos" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnvioTipo", propOrder = {
    "identificacion",
    "radicacion",
    "codEstado",
    "codSolicitud",
    "formato",
    "archivos"
})
public class EnvioTipo {

    @XmlElement(nillable = true)
    protected IdentificacionTipo identificacion;
    @XmlElement(nillable = true)
    protected RadicacionTipo radicacion;
    @XmlElement(nillable = true)
    protected String codEstado;
    @XmlElement(nillable = true)
    protected String codSolicitud;
    @XmlElement(nillable = true)
    protected FormatoTipo formato;
    @XmlElement(nillable = true)
    protected List<ArchivoTipo> archivos;

    /**
     * Obtiene el valor de la propiedad identificacion.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdentificacion() {
        return identificacion;
    }

    /**
     * Define el valor de la propiedad identificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdentificacion(IdentificacionTipo value) {
        this.identificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad radicacion.
     * 
     * @return
     *     possible object is
     *     {@link RadicacionTipo }
     *     
     */
    public RadicacionTipo getRadicacion() {
        return radicacion;
    }

    /**
     * Define el valor de la propiedad radicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link RadicacionTipo }
     *     
     */
    public void setRadicacion(RadicacionTipo value) {
        this.radicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

    /**
     * Obtiene el valor de la propiedad codSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodSolicitud() {
        return codSolicitud;
    }

    /**
     * Define el valor de la propiedad codSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodSolicitud(String value) {
        this.codSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad formato.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormato() {
        return formato;
    }

    /**
     * Define el valor de la propiedad formato.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormato(FormatoTipo value) {
        this.formato = value;
    }

    /**
     * Gets the value of the archivos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArchivoTipo }
     * 
     * 
     */
    public List<ArchivoTipo> getArchivos() {
        if (archivos == null) {
            archivos = new ArrayList<ArchivoTipo>();
        }
        return this.archivos;
    }

}
