
package co.gov.ugpp.schema.solicitudinformacion.agrupaciondocumentosolicitudtipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;


/**
 * <p>Clase Java para AgrupacionDocumentoSolicitudTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgrupacionDocumentoSolicitudTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="documentos" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" maxOccurs="unbounded"/>
 *         &lt;element name="solicitudes" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/SolicitudInformacionTipo/v1}SolicitudInformacionTipo" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgrupacionDocumentoSolicitudTipo", propOrder = {
    "documentos",
    "solicitudes"
})
public class AgrupacionDocumentoSolicitudTipo {

    @XmlElement(required = true, nillable = true)
    protected List<DocumentoTipo> documentos;
    @XmlElement(required = true)
    protected List<SolicitudInformacionTipo> solicitudes;

    /**
     * Gets the value of the documentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoTipo }
     * 
     * 
     */
    public List<DocumentoTipo> getDocumentos() {
        if (documentos == null) {
            documentos = new ArrayList<DocumentoTipo>();
        }
        return this.documentos;
    }

    /**
     * Gets the value of the solicitudes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudInformacionTipo }
     * 
     * 
     */
    public List<SolicitudInformacionTipo> getSolicitudes() {
        if (solicitudes == null) {
            solicitudes = new ArrayList<SolicitudInformacionTipo>();
        }
        return this.solicitudes;
    }

}
