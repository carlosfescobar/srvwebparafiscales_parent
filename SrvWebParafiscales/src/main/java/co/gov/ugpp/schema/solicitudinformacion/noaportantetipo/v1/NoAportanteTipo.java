
package co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para NoAportanteTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NoAportanteTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo">
 *       &lt;sequence>
 *         &lt;element name="codTipoNoAportante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NoAportanteTipo", propOrder = {
    "codTipoNoAportante"
})
public class NoAportanteTipo
    extends PersonaNaturalTipo
{

    @XmlElement(nillable = true)
    protected String codTipoNoAportante;

    /**
     * Obtiene el valor de la propiedad codTipoNoAportante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoNoAportante() {
        return codTipoNoAportante;
    }

    /**
     * Define el valor de la propiedad codTipoNoAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoNoAportante(String value) {
        this.codTipoNoAportante = value;
    }

}
