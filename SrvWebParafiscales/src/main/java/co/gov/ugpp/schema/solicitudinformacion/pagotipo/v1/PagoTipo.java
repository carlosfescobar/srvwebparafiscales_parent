
package co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para PagoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PagoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valValor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="periodoPago" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PagoTipo", propOrder = {
    "idPago",
    "valValor",
    "periodoPago"
})
public class PagoTipo {

    @XmlElement(nillable = true)
    protected String idPago;
    @XmlElement(nillable = true)
    protected String valValor;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar periodoPago;

    /**
     * Obtiene el valor de la propiedad idPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPago() {
        return idPago;
    }

    /**
     * Define el valor de la propiedad idPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPago(String value) {
        this.idPago = value;
    }

    /**
     * Obtiene el valor de la propiedad valValor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValValor() {
        return valValor;
    }

    /**
     * Define el valor de la propiedad valValor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValValor(String value) {
        this.valValor = value;
    }

    /**
     * Obtiene el valor de la propiedad periodoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getPeriodoPago() {
        return periodoPago;
    }

    /**
     * Define el valor de la propiedad periodoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodoPago(Calendar value) {
        this.periodoPago = value;
    }

}
