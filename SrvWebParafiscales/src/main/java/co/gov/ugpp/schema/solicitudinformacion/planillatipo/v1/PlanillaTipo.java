
package co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para PlanillaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PlanillaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idNumeroPlanilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codOperador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecPeriodoPago" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estadoPlanilla" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PlanillaTipo", propOrder = {
    "idNumeroPlanilla",
    "codOperador",
    "fecPeriodoPago",
    "descObservaciones",
    "codTipoObservaciones",
    "descTipoObservaciones", 
    "estadoPlanilla" 
})
public class PlanillaTipo {

    @XmlElement(nillable = true)
    protected String idNumeroPlanilla;
    @XmlElement(nillable = true)
    protected String codOperador;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecPeriodoPago;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String codTipoObservaciones;
    @XmlElement(nillable = true)
    protected String descTipoObservaciones;
    @XmlElement(nillable = true)
    protected String estadoPlanilla;

    
    
    public String getEstadoPlanilla() {
        return estadoPlanilla;
    }

    public void setEstadoPlanilla(String estadoPlanilla) {
        this.estadoPlanilla = estadoPlanilla;
    }
    
    
    /**
     * Obtiene el valor de la propiedad idNumeroPlanilla.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumeroPlanilla() {
        return idNumeroPlanilla;
    }

    /**
     * Define el valor de la propiedad idNumeroPlanilla.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumeroPlanilla(String value) {
        this.idNumeroPlanilla = value;
    }

    /**
     * Obtiene el valor de la propiedad codOperador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOperador() {
        return codOperador;
    }

    /**
     * Define el valor de la propiedad codOperador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOperador(String value) {
        this.codOperador = value;
    }

    /**
     * Obtiene el valor de la propiedad fecPeriodoPago.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecPeriodoPago() {
        return fecPeriodoPago;
    }

    /**
     * Define el valor de la propiedad fecPeriodoPago.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecPeriodoPago(Calendar value) {
        this.fecPeriodoPago = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoObservaciones() {
        return codTipoObservaciones;
    }

    /**
     * Define el valor de la propiedad codTipoObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoObservaciones(String value) {
        this.codTipoObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoObservaciones() {
        return descTipoObservaciones;
    }

    /**
     * Define el valor de la propiedad descTipoObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoObservaciones(String value) {
        this.descTipoObservaciones = value;
    }

}
