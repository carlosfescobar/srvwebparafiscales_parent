
package co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;


/**
 * <p>Clase Java para RadicacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RadicacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fecRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="fecRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RadicacionTipo", propOrder = {
    "fecRadicadoEntrada",
    "fecRadicadoSalida",
    "idRadicadoEntrada",
    "idRadicadoSalida"
})
public class RadicacionTipo {

    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecRadicadoEntrada;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecRadicadoSalida;
    @XmlElement(nillable = true)
    protected String idRadicadoEntrada;
    @XmlElement(nillable = true)
    protected String idRadicadoSalida;

    /**
     * Obtiene el valor de la propiedad fecRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoEntrada() {
        return fecRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad fecRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoEntrada(Calendar value) {
        this.fecRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad fecRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoSalida(Calendar value) {
        this.fecRadicadoSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntrada(String value) {
        this.idRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalida(String value) {
        this.idRadicadoSalida = value;
    }

}
