
package co.gov.ugpp.schema.solicitudinformacion.respuestasolicitudtipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;


/**
 * <p>Clase Java para RespuestaSolicitudTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RespuestaSolicitudTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idPrograma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicado" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fecRespuesta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="esArchivoCargado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docRespuesta" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaSolicitudTipo", propOrder = {
    "idPrograma",
    "idRadicado",
    "fecRespuesta",
    "esArchivoCargado",
    "docRespuesta"
})
public class RespuestaSolicitudTipo {

    @XmlElement(nillable = true)
    protected String idPrograma;
    @XmlElement(nillable = true)
    protected List<String> idRadicado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecRespuesta;
    @XmlElement(nillable = true)
    protected String esArchivoCargado;
    @XmlElement(nillable = true)
    protected ArchivoTipo docRespuesta;

    /**
     * Obtiene el valor de la propiedad idPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPrograma() {
        return idPrograma;
    }

    /**
     * Define el valor de la propiedad idPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPrograma(String value) {
        this.idPrograma = value;
    }

    /**
     * Gets the value of the idRadicado property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idRadicado property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdRadicado().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdRadicado() {
        if (idRadicado == null) {
            idRadicado = new ArrayList<String>();
        }
        return this.idRadicado;
    }

    /**
     * Obtiene el valor de la propiedad fecRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRespuesta() {
        return fecRespuesta;
    }

    /**
     * Define el valor de la propiedad fecRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRespuesta(Calendar value) {
        this.fecRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad esArchivoCargado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsArchivoCargado() {
        return esArchivoCargado;
    }

    /**
     * Define el valor de la propiedad esArchivoCargado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsArchivoCargado(String value) {
        this.esArchivoCargado = value;
    }

    /**
     * Obtiene el valor de la propiedad docRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getDocRespuesta() {
        return docRespuesta;
    }

    /**
     * Define el valor de la propiedad docRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setDocRespuesta(ArchivoTipo value) {
        this.docRespuesta = value;
    }

}
