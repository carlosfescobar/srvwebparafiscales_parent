
package co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.schema.denuncias.entidadexternatipo.v1.EntidadExternaTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;
import co.gov.ugpp.schema.solicitudinformacion.pagotipo.v1.PagoTipo;
import co.gov.ugpp.schema.solicitudinformacion.planillatipo.v1.PlanillaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;


/**
 * <p>Clase Java para SolicitudInformacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudInformacionTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/schema/SolicitudInformacion/SolicitudTipo/v1}SolicitudTipo">
 *       &lt;sequence>
 *         &lt;element name="codTipoConsulta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esConsultaNoAportante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noAportante" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/NoAportanteTipo/v1}NoAportanteTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="cotizantes" type="{http://www.ugpp.gov.co/schema/Denuncias/CotizanteTipo/v1}CotizanteTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="planillas" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/PlanillaTipo/v1}PlanillaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="pagos" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/PagoTipo/v1}PagoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="docsSolicitudAportante" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentoAnexo" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="esConsultaAprobada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionAprobada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="entidadExterna" type="{http://www.ugpp.gov.co/schema/Denuncias/EntidadExternaTipo/v1}EntidadExternaTipo" minOccurs="0"/>
 *         &lt;element name="descObservacionNoAportante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudInformacionTipo", propOrder = {
    "codTipoConsulta",
    "esConsultaNoAportante",
    "noAportante",
    "aportante",
    "cotizantes",
    "planillas",
    "pagos",
    "docsSolicitudAportante",
    "idDocumentoAnexo",
    "esConsultaAprobada",
    "descObservacion",
    "descObservacionAprobada",
    "entidadExterna",
    "descObservacionNoAportante"
})
public class SolicitudInformacionTipo
    extends SolicitudTipo
{

    @XmlElement(nillable = true)
    protected String codTipoConsulta;
    @XmlElement(nillable = true)
    protected String esConsultaNoAportante;
    @XmlElement(nillable = true)
    protected NoAportanteTipo noAportante;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected List<CotizanteTipo> cotizantes;
    @XmlElement(nillable = true)
    protected List<PlanillaTipo> planillas;
    @XmlElement(nillable = true)
    protected List<PagoTipo> pagos;
    @XmlElement(nillable = true)
    protected ArchivoTipo docsSolicitudAportante;
    @XmlElement(nillable = true)
    protected List<String> idDocumentoAnexo;
    @XmlElement(nillable = true)
    protected String esConsultaAprobada;
    @XmlElement(nillable = true)
    protected String descObservacion;
    @XmlElement(nillable = true)
    protected String descObservacionAprobada;
    @XmlElement(nillable = true)
    protected EntidadExternaTipo entidadExterna;
    @XmlElement(nillable = true)
    protected String descObservacionNoAportante;

    /**
     * Obtiene el valor de la propiedad codTipoConsulta.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoConsulta() {
        return codTipoConsulta;
    }

    /**
     * Define el valor de la propiedad codTipoConsulta.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoConsulta(String value) {
        this.codTipoConsulta = value;
    }

    /**
     * Obtiene el valor de la propiedad esConsultaNoAportante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConsultaNoAportante() {
        return esConsultaNoAportante;
    }

    /**
     * Define el valor de la propiedad esConsultaNoAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConsultaNoAportante(String value) {
        this.esConsultaNoAportante = value;
    }

    /**
     * Obtiene el valor de la propiedad noAportante.
     * 
     * @return
     *     possible object is
     *     {@link NoAportanteTipo }
     *     
     */
    public NoAportanteTipo getNoAportante() {
        return noAportante;
    }

    /**
     * Define el valor de la propiedad noAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link NoAportanteTipo }
     *     
     */
    public void setNoAportante(NoAportanteTipo value) {
        this.noAportante = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Gets the value of the cotizantes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cotizantes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCotizantes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CotizanteTipo }
     * 
     * 
     */
    public List<CotizanteTipo> getCotizantes() {
        if (cotizantes == null) {
            cotizantes = new ArrayList<CotizanteTipo>();
        }
        return this.cotizantes;
    }

    /**
     * Gets the value of the planillas property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the planillas property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPlanillas().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PlanillaTipo }
     * 
     * 
     */
    public List<PlanillaTipo> getPlanillas() {
        if (planillas == null) {
            planillas = new ArrayList<PlanillaTipo>();
        }
        return this.planillas;
    }

    /**
     * Gets the value of the pagos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pagos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPagos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PagoTipo }
     * 
     * 
     */
    public List<PagoTipo> getPagos() {
        if (pagos == null) {
            pagos = new ArrayList<PagoTipo>();
        }
        return this.pagos;
    }

    /**
     * Obtiene el valor de la propiedad docsSolicitudAportante.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getDocsSolicitudAportante() {
        return docsSolicitudAportante;
    }

    /**
     * Define el valor de la propiedad docsSolicitudAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setDocsSolicitudAportante(ArchivoTipo value) {
        this.docsSolicitudAportante = value;
    }

    /**
     * Gets the value of the idDocumentoAnexo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentoAnexo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentoAnexo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentoAnexo() {
        if (idDocumentoAnexo == null) {
            idDocumentoAnexo = new ArrayList<String>();
        }
        return this.idDocumentoAnexo;
    }

    /**
     * Obtiene el valor de la propiedad esConsultaAprobada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsConsultaAprobada() {
        return esConsultaAprobada;
    }

    /**
     * Define el valor de la propiedad esConsultaAprobada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsConsultaAprobada(String value) {
        this.esConsultaAprobada = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacion() {
        return descObservacion;
    }

    /**
     * Define el valor de la propiedad descObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacion(String value) {
        this.descObservacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionAprobada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionAprobada() {
        return descObservacionAprobada;
    }

    /**
     * Define el valor de la propiedad descObservacionAprobada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionAprobada(String value) {
        this.descObservacionAprobada = value;
    }

    /**
     * Obtiene el valor de la propiedad entidadExterna.
     * 
     * @return
     *     possible object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public EntidadExternaTipo getEntidadExterna() {
        return entidadExterna;
    }

    /**
     * Define el valor de la propiedad entidadExterna.
     * 
     * @param value
     *     allowed object is
     *     {@link EntidadExternaTipo }
     *     
     */
    public void setEntidadExterna(EntidadExternaTipo value) {
        this.entidadExterna = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionNoAportante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionNoAportante() {
        return descObservacionNoAportante;
    }

    /**
     * Define el valor de la propiedad descObservacionNoAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionNoAportante(String value) {
        this.descObservacionNoAportante = value;
    }

}
