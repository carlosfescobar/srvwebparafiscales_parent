
package co.gov.ugpp.schema.solicitudinformacion.solicitudprogramatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1.SolicitudTipo;


/**
 * <p>Clase Java para SolicitudProgramaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudProgramaTipo">
 *   &lt;complexContent>
 *     &lt;extension base="{http://www.ugpp.gov.co/schema/SolicitudInformacion/SolicitudTipo/v1}SolicitudTipo">
 *       &lt;sequence>
 *         &lt;element name="valResumen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFuenteInformacion" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valFiltros" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valCruces" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valResultadoEsperado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valRutaArchivoRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valObservacionRequerimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docAnexos" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valPorcentajeEfectividad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudProgramaTipo", propOrder = {
    "valResumen",
    "codFuenteInformacion",
    "valFiltros",
    "valCruces",
    "valResultadoEsperado",
    "valRutaArchivoRequerimiento",
    "valObservacionRequerimiento",
    "docAnexos",
    "valPorcentajeEfectividad"
})
public class SolicitudProgramaTipo
    extends SolicitudTipo
{

    @XmlElement(nillable = true)
    protected String valResumen;
    @XmlElement(nillable = true)
    protected List<String> codFuenteInformacion;
    @XmlElement(nillable = true)
    protected String valFiltros;
    @XmlElement(nillable = true)
    protected String valCruces;
    @XmlElement(nillable = true)
    protected String valResultadoEsperado;
    @XmlElement(nillable = true)
    protected String valRutaArchivoRequerimiento;
    @XmlElement(nillable = true)
    protected String valObservacionRequerimiento;
    @XmlElement(nillable = true)
    protected List<ArchivoTipo> docAnexos;
    @XmlElement(nillable = true)
    protected String valPorcentajeEfectividad;

    /**
     * Obtiene el valor de la propiedad valResumen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValResumen() {
        return valResumen;
    }

    /**
     * Define el valor de la propiedad valResumen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValResumen(String value) {
        this.valResumen = value;
    }

    /**
     * Gets the value of the codFuenteInformacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codFuenteInformacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodFuenteInformacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodFuenteInformacion() {
        if (codFuenteInformacion == null) {
            codFuenteInformacion = new ArrayList<String>();
        }
        return this.codFuenteInformacion;
    }

    /**
     * Obtiene el valor de la propiedad valFiltros.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFiltros() {
        return valFiltros;
    }

    /**
     * Define el valor de la propiedad valFiltros.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFiltros(String value) {
        this.valFiltros = value;
    }

    /**
     * Obtiene el valor de la propiedad valCruces.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCruces() {
        return valCruces;
    }

    /**
     * Define el valor de la propiedad valCruces.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCruces(String value) {
        this.valCruces = value;
    }

    /**
     * Obtiene el valor de la propiedad valResultadoEsperado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValResultadoEsperado() {
        return valResultadoEsperado;
    }

    /**
     * Define el valor de la propiedad valResultadoEsperado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValResultadoEsperado(String value) {
        this.valResultadoEsperado = value;
    }

    /**
     * Obtiene el valor de la propiedad valRutaArchivoRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValRutaArchivoRequerimiento() {
        return valRutaArchivoRequerimiento;
    }

    /**
     * Define el valor de la propiedad valRutaArchivoRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValRutaArchivoRequerimiento(String value) {
        this.valRutaArchivoRequerimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad valObservacionRequerimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValObservacionRequerimiento() {
        return valObservacionRequerimiento;
    }

    /**
     * Define el valor de la propiedad valObservacionRequerimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValObservacionRequerimiento(String value) {
        this.valObservacionRequerimiento = value;
    }

    /**
     * Gets the value of the docAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArchivoTipo }
     * 
     * 
     */
    public List<ArchivoTipo> getDocAnexos() {
        if (docAnexos == null) {
            docAnexos = new ArrayList<ArchivoTipo>();
        }
        return this.docAnexos;
    }

    /**
     * Obtiene el valor de la propiedad valPorcentajeEfectividad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPorcentajeEfectividad() {
        return valPorcentajeEfectividad;
    }

    /**
     * Define el valor de la propiedad valPorcentajeEfectividad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPorcentajeEfectividad(String value) {
        this.valPorcentajeEfectividad = value;
    }

}
