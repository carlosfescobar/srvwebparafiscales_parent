
package co.gov.ugpp.schema.solicitudinformacion.solicitudtipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.administradoras.seguimientotipo.v1.SeguimientoTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;


/**
 * <p>Clase Java para SolicitudTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SolicitudTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecSolicitud" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="solicitante" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="codTipoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codMedioSolictud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esAprobarSolicitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="docSolicitudInformacion" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="docSolicitudReiteracion" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *         &lt;element name="seguimiento" type="{http://www.ugpp.gov.co/schema/Administradoras/SeguimientoTipo/v1}SeguimientoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SolicitudTipo", propOrder = {
    "idSolicitud",
    "fecSolicitud",
    "solicitante",
    "codTipoSolicitud",
    "codMedioSolictud",
    "codEstadoSolicitud",
    "esAprobarSolicitud",
    "expediente",
    "docSolicitudInformacion",
    "docSolicitudReiteracion",
    "seguimiento",
    "idRadicadoEntrada",
    "idRadicadoSalida"
})
@XmlSeeAlso({
    SolicitudInformacionTipo.class
})
public class SolicitudTipo {

    @XmlElement(nillable = true)
    protected String idSolicitud;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecSolicitud;
    @XmlElement(nillable = true)
    protected FuncionarioTipo solicitante;
    @XmlElement(nillable = true)
    protected String codTipoSolicitud;
    @XmlElement(nillable = true)
    protected String codMedioSolictud;
    @XmlElement(nillable = true)
    protected String codEstadoSolicitud;
    @XmlElement(nillable = true)
    protected String esAprobarSolicitud;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected DocumentoTipo docSolicitudInformacion;
    @XmlElement(nillable = true)
    protected DocumentoTipo docSolicitudReiteracion;
    @XmlElement(nillable = true)
    protected List<SeguimientoTipo> seguimiento;
    @XmlElement(nillable = true)
    protected String idRadicadoEntrada;
    @XmlElement(nillable = true)
    protected String idRadicadoSalida;

    /**
     * Obtiene el valor de la propiedad idSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdSolicitud() {
        return idSolicitud;
    }

    /**
     * Define el valor de la propiedad idSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdSolicitud(String value) {
        this.idSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad fecSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecSolicitud() {
        return fecSolicitud;
    }

    /**
     * Define el valor de la propiedad fecSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecSolicitud(Calendar value) {
        this.fecSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitante.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getSolicitante() {
        return solicitante;
    }

    /**
     * Define el valor de la propiedad solicitante.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setSolicitante(FuncionarioTipo value) {
        this.solicitante = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoSolicitud() {
        return codTipoSolicitud;
    }

    /**
     * Define el valor de la propiedad codTipoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoSolicitud(String value) {
        this.codTipoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad codMedioSolictud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodMedioSolictud() {
        return codMedioSolictud;
    }

    /**
     * Define el valor de la propiedad codMedioSolictud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodMedioSolictud(String value) {
        this.codMedioSolictud = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoSolicitud() {
        return codEstadoSolicitud;
    }

    /**
     * Define el valor de la propiedad codEstadoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoSolicitud(String value) {
        this.codEstadoSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad esAprobarSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsAprobarSolicitud() {
        return esAprobarSolicitud;
    }

    /**
     * Define el valor de la propiedad esAprobarSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsAprobarSolicitud(String value) {
        this.esAprobarSolicitud = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad docSolicitudInformacion.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocSolicitudInformacion() {
        return docSolicitudInformacion;
    }

    /**
     * Define el valor de la propiedad docSolicitudInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocSolicitudInformacion(DocumentoTipo value) {
        this.docSolicitudInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad docSolicitudReiteracion.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocSolicitudReiteracion() {
        return docSolicitudReiteracion;
    }

    /**
     * Define el valor de la propiedad docSolicitudReiteracion.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocSolicitudReiteracion(DocumentoTipo value) {
        this.docSolicitudReiteracion = value;
    }

    /**
     * Gets the value of the seguimiento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the seguimiento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSeguimiento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SeguimientoTipo }
     * 
     * 
     */
    public List<SeguimientoTipo> getSeguimiento() {
        if (seguimiento == null) {
            seguimiento = new ArrayList<SeguimientoTipo>();
        }
        return this.seguimiento;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntrada(String value) {
        this.idRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalida(String value) {
        this.idRadicadoSalida = value;
    }

}
