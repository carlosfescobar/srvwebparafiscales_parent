
package co.gov.ugpp.schema.transversales.agrupacionactoadministrativoanexotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.schema.notificaciones.actoadministrativotipo.v1.ActoAdministrativoTipo;


/**
 * <p>Clase Java para AgrupacionActoAdministrativoAnexoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AgrupacionActoAdministrativoAnexoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actoAdministrativo" type="{http://www.ugpp.gov.co/schema/Notificaciones/ActoAdministrativoTipo/v1}ActoAdministrativoTipo" minOccurs="0"/>
 *         &lt;element name="idDocumentosAnexos" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AgrupacionActoAdministrativoAnexoTipo", propOrder = {
    "actoAdministrativo",
    "idDocumentosAnexos"
})
public class AgrupacionActoAdministrativoAnexoTipo {

    protected ActoAdministrativoTipo actoAdministrativo;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> idDocumentosAnexos;

    /**
     * Obtiene el valor de la propiedad actoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public ActoAdministrativoTipo getActoAdministrativo() {
        return actoAdministrativo;
    }

    /**
     * Define el valor de la propiedad actoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link ActoAdministrativoTipo }
     *     
     */
    public void setActoAdministrativo(ActoAdministrativoTipo value) {
        this.actoAdministrativo = value;
    }

    /**
     * Gets the value of the idDocumentosAnexos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentosAnexos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentosAnexos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getIdDocumentosAnexos() {
        if (idDocumentosAnexos == null) {
            idDocumentosAnexos = new ArrayList<ParametroValoresTipo>();
        }
        return this.idDocumentosAnexos;
    }

}
