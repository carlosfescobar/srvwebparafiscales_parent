
package co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;


/**
 * <p>Clase Java para AprobacionMasivaActoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="AprobacionMasivaActoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idAprobacionMasiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFuncionarioResponsable" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="fecEnvioAprobacion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codEstadoAprobacionMasiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoAprobacionMasiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="actosAdministrativosPorAprobar" type="{http://www.ugpp.gov.co/schema/Transversales/ControlAprobacionActoTipo/v1}ControlAprobacionActoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AprobacionMasivaActoTipo", propOrder = {
    "idAprobacionMasiva",
    "idFuncionarioResponsable",
    "fecEnvioAprobacion",
    "codEstadoAprobacionMasiva",
    "descEstadoAprobacionMasiva",
    "actosAdministrativosPorAprobar"
})
public class AprobacionMasivaActoTipo {

    @XmlElement(nillable = true)
    protected String idAprobacionMasiva;
    @XmlElement(nillable = true)
    protected FuncionarioTipo idFuncionarioResponsable;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEnvioAprobacion;
    @XmlElement(nillable = true)
    protected String codEstadoAprobacionMasiva;
    @XmlElement(nillable = true)
    protected String descEstadoAprobacionMasiva;
    @XmlElement(nillable = true)
    protected List<ControlAprobacionActoTipo> actosAdministrativosPorAprobar;

    /**
     * Obtiene el valor de la propiedad idAprobacionMasiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAprobacionMasiva() {
        return idAprobacionMasiva;
    }

    /**
     * Define el valor de la propiedad idAprobacionMasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAprobacionMasiva(String value) {
        this.idAprobacionMasiva = value;
    }

    /**
     * Obtiene el valor de la propiedad idFuncionarioResponsable.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getIdFuncionarioResponsable() {
        return idFuncionarioResponsable;
    }

    /**
     * Define el valor de la propiedad idFuncionarioResponsable.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setIdFuncionarioResponsable(FuncionarioTipo value) {
        this.idFuncionarioResponsable = value;
    }

    /**
     * Obtiene el valor de la propiedad fecEnvioAprobacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEnvioAprobacion() {
        return fecEnvioAprobacion;
    }

    /**
     * Define el valor de la propiedad fecEnvioAprobacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEnvioAprobacion(Calendar value) {
        this.fecEnvioAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoAprobacionMasiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoAprobacionMasiva() {
        return codEstadoAprobacionMasiva;
    }

    /**
     * Define el valor de la propiedad codEstadoAprobacionMasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoAprobacionMasiva(String value) {
        this.codEstadoAprobacionMasiva = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoAprobacionMasiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoAprobacionMasiva() {
        return descEstadoAprobacionMasiva;
    }

    /**
     * Define el valor de la propiedad descEstadoAprobacionMasiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoAprobacionMasiva(String value) {
        this.descEstadoAprobacionMasiva = value;
    }

    /**
     * Gets the value of the actosAdministrativosPorAprobar property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the actosAdministrativosPorAprobar property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActosAdministrativosPorAprobar().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ControlAprobacionActoTipo }
     * 
     * 
     */
    public List<ControlAprobacionActoTipo> getActosAdministrativosPorAprobar() {
        if (actosAdministrativosPorAprobar == null) {
            actosAdministrativosPorAprobar = new ArrayList<ControlAprobacionActoTipo>();
        }
        return this.actosAdministrativosPorAprobar;
    }

}
