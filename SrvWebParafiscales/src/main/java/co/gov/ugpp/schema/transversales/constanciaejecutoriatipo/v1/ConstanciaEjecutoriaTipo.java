
package co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrotipo.v1.ParametroTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;


/**
 * <p>Clase Java para ConstanciaEjecutoriaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ConstanciaEjecutoriaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idConstanciaEjecutoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRecursos" type="{http://www.ugpp.gov.co/esb/schema/ParametroTipo/v1}ParametroTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valDecisionesRecursos" type="{http://www.ugpp.gov.co/esb/schema/ParametroTipo/v1}ParametroTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codFallo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFallo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoConstanciaEjecutoria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecCreacionDocumentoConstancia" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecConstanciaEjecutoria" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConstanciaEjecutoriaTipo", propOrder = {
    "idConstanciaEjecutoria",
    "fecRecursos",
    "valDecisionesRecursos",
    "codFallo",
    "descFallo",
    "idDocumentoConstanciaEjecutoria",
    "fecCreacionDocumentoConstancia",
    "fecConstanciaEjecutoria"
})
public class ConstanciaEjecutoriaTipo {

    @XmlElement(nillable = true)
    protected String idConstanciaEjecutoria;
    protected List<ParametroTipo> fecRecursos;
    protected List<ParametroTipo> valDecisionesRecursos;
    @XmlElement(nillable = true)
    protected String codFallo;
    @XmlElement(nillable = true)
    protected String descFallo;
    @XmlElement(nillable = true)
    protected String idDocumentoConstanciaEjecutoria;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecCreacionDocumentoConstancia;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecConstanciaEjecutoria;

    /**
     * Obtiene el valor de la propiedad idConstanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConstanciaEjecutoria() {
        return idConstanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad idConstanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConstanciaEjecutoria(String value) {
        this.idConstanciaEjecutoria = value;
    }

    /**
     * Gets the value of the fecRecursos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fecRecursos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFecRecursos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroTipo }
     * 
     * 
     */
    public List<ParametroTipo> getFecRecursos() {
        if (fecRecursos == null) {
            fecRecursos = new ArrayList<ParametroTipo>();
        }
        return this.fecRecursos;
    }

    /**
     * Gets the value of the valDecisionesRecursos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valDecisionesRecursos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValDecisionesRecursos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroTipo }
     * 
     * 
     */
    public List<ParametroTipo> getValDecisionesRecursos() {
        if (valDecisionesRecursos == null) {
            valDecisionesRecursos = new ArrayList<ParametroTipo>();
        }
        return this.valDecisionesRecursos;
    }

    /**
     * Obtiene el valor de la propiedad codFallo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFallo() {
        return codFallo;
    }

    /**
     * Define el valor de la propiedad codFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFallo(String value) {
        this.codFallo = value;
    }

    /**
     * Obtiene el valor de la propiedad descFallo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFallo() {
        return descFallo;
    }

    /**
     * Define el valor de la propiedad descFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFallo(String value) {
        this.descFallo = value;
    }

    /**
     * Obtiene el valor de la propiedad idDocumentoConstanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoConstanciaEjecutoria() {
        return idDocumentoConstanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad idDocumentoConstanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoConstanciaEjecutoria(String value) {
        this.idDocumentoConstanciaEjecutoria = value;
    }

    /**
     * Obtiene el valor de la propiedad fecCreacionDocumentoConstancia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecCreacionDocumentoConstancia() {
        return fecCreacionDocumentoConstancia;
    }

    /**
     * Define el valor de la propiedad fecCreacionDocumentoConstancia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecCreacionDocumentoConstancia(Calendar value) {
        this.fecCreacionDocumentoConstancia = value;
    }

    /**
     * Obtiene el valor de la propiedad fecConstanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecConstanciaEjecutoria() {
        return fecConstanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad fecConstanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecConstanciaEjecutoria(Calendar value) {
        this.fecConstanciaEjecutoria = value;
    }

}
