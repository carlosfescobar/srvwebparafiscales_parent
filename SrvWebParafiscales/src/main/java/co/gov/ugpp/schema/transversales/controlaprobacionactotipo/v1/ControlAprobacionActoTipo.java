
package co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para ControlAprobacionActoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ControlAprobacionActoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idControlAprobacionActo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAprobacionMasivaActo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idArchivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="idFuncionarioApruebaActo" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="idFuncionarioElaboraActo" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="fecAprobacionActo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecTareaAprobacionEnCola" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idActoAdministrativoCreado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoActoAprobacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoActoAprobacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoAprobacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoAprobacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valRolEncargadoAprobar" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlAprobacionActoTipo", propOrder = {
    "idControlAprobacionActo",
    "idAprobacionMasivaActo",
    "idArchivo",
    "expediente",
    "aportante",
    "idFuncionarioApruebaActo",
    "idFuncionarioElaboraActo",
    "fecAprobacionActo",
    "fecTareaAprobacionEnCola",
    "idActoAdministrativoCreado",
    "codTipoActoAprobacion",
    "descTipoActoAprobacion",
    "codEstadoAprobacion",
    "descEstadoAprobacion",
    "valRolEncargadoAprobar"
})
public class ControlAprobacionActoTipo {

    @XmlElement(nillable = true)
    protected String idControlAprobacionActo;
    @XmlElement(nillable = true)
    protected String idAprobacionMasivaActo;
    @XmlElement(nillable = true)
    protected String idArchivo;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected FuncionarioTipo idFuncionarioApruebaActo;
    @XmlElement(nillable = true)
    protected FuncionarioTipo idFuncionarioElaboraActo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecAprobacionActo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecTareaAprobacionEnCola;
    @XmlElement(nillable = true)
    protected String idActoAdministrativoCreado;
    @XmlElement(nillable = true)
    protected String codTipoActoAprobacion;
    @XmlElement(nillable = true)
    protected String descTipoActoAprobacion;
    @XmlElement(nillable = true)
    protected String codEstadoAprobacion;
    @XmlElement(nillable = true)
    protected String descEstadoAprobacion;
    @XmlElement(nillable = true)
    protected String valRolEncargadoAprobar;

    /**
     * Obtiene el valor de la propiedad idControlAprobacionActo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdControlAprobacionActo() {
        return idControlAprobacionActo;
    }

    /**
     * Define el valor de la propiedad idControlAprobacionActo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdControlAprobacionActo(String value) {
        this.idControlAprobacionActo = value;
    }

    /**
     * Obtiene el valor de la propiedad idAprobacionMasivaActo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAprobacionMasivaActo() {
        return idAprobacionMasivaActo;
    }

    /**
     * Define el valor de la propiedad idAprobacionMasivaActo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAprobacionMasivaActo(String value) {
        this.idAprobacionMasivaActo = value;
    }

    /**
     * Obtiene el valor de la propiedad idArchivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdArchivo() {
        return idArchivo;
    }

    /**
     * Define el valor de la propiedad idArchivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdArchivo(String value) {
        this.idArchivo = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad idFuncionarioApruebaActo.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getIdFuncionarioApruebaActo() {
        return idFuncionarioApruebaActo;
    }

    /**
     * Define el valor de la propiedad idFuncionarioApruebaActo.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setIdFuncionarioApruebaActo(FuncionarioTipo value) {
        this.idFuncionarioApruebaActo = value;
    }

    /**
     * Obtiene el valor de la propiedad idFuncionarioElaboraActo.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getIdFuncionarioElaboraActo() {
        return idFuncionarioElaboraActo;
    }

    /**
     * Define el valor de la propiedad idFuncionarioElaboraActo.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setIdFuncionarioElaboraActo(FuncionarioTipo value) {
        this.idFuncionarioElaboraActo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecAprobacionActo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecAprobacionActo() {
        return fecAprobacionActo;
    }

    /**
     * Define el valor de la propiedad fecAprobacionActo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecAprobacionActo(Calendar value) {
        this.fecAprobacionActo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecTareaAprobacionEnCola.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecTareaAprobacionEnCola() {
        return fecTareaAprobacionEnCola;
    }

    /**
     * Define el valor de la propiedad fecTareaAprobacionEnCola.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecTareaAprobacionEnCola(Calendar value) {
        this.fecTareaAprobacionEnCola = value;
    }

    /**
     * Obtiene el valor de la propiedad idActoAdministrativoCreado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAdministrativoCreado() {
        return idActoAdministrativoCreado;
    }

    /**
     * Define el valor de la propiedad idActoAdministrativoCreado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAdministrativoCreado(String value) {
        this.idActoAdministrativoCreado = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoActoAprobacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoActoAprobacion() {
        return codTipoActoAprobacion;
    }

    /**
     * Define el valor de la propiedad codTipoActoAprobacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoActoAprobacion(String value) {
        this.codTipoActoAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoActoAprobacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoActoAprobacion() {
        return descTipoActoAprobacion;
    }

    /**
     * Define el valor de la propiedad descTipoActoAprobacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoActoAprobacion(String value) {
        this.descTipoActoAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoAprobacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoAprobacion() {
        return codEstadoAprobacion;
    }

    /**
     * Define el valor de la propiedad codEstadoAprobacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoAprobacion(String value) {
        this.codEstadoAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoAprobacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoAprobacion() {
        return descEstadoAprobacion;
    }

    /**
     * Define el valor de la propiedad descEstadoAprobacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoAprobacion(String value) {
        this.descEstadoAprobacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valRolEncargadoAprobar.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValRolEncargadoAprobar() {
        return valRolEncargadoAprobar;
    }

    /**
     * Define el valor de la propiedad valRolEncargadoAprobar.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValRolEncargadoAprobar(String value) {
        this.valRolEncargadoAprobar = value;
    }

}
