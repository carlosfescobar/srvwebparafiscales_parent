
package co.gov.ugpp.schema.transversales.controlliquidadortipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.gestiondocumental.formatotipo.v1.FormatoTipo;
import co.gov.ugpp.schema.liquidaciones.hojacalculotipo.v1.HojaCalculoTipo;
import co.gov.ugpp.schema.liquidador.conciliacioncontabletipo.v1.ConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;
import co.gov.ugpp.schema.transversales.hallazgonominatipo.v1.HallazgoNominaTipo;
import co.gov.ugpp.schema.transversales.nominatipo.v1.NominaTipo;


/**
 * <p>Clase Java para ControlLiquidadorTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ControlLiquidadorTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idControlLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProcesoSolicitador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="formatoNomina" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="formatoConciliacion" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoTipo/v1}FormatoTipo" minOccurs="0"/>
 *         &lt;element name="idInformacionExternaNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idInformacionExternaConciliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEnvioNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEnvioConciliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomina" type="{http://www.ugpp.gov.co/schema/Transversales/NominaTipo/v1}NominaTipo" minOccurs="0"/>
 *         &lt;element name="conciliacionContable" type="{http://www.ugpp.gov.co/schema/Liquidador/ConciliacionContableTipo/v1}ConciliacionContableTipo" minOccurs="0"/>
 *         &lt;element name="hojaCalculo" type="{http://www.ugpp.gov.co/schema/Liquidaciones/HojaCalculoTipo/v1}HojaCalculoTipo" minOccurs="0"/>
 *         &lt;element name="codOrigen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codLineaAccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDenuncia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPrograma" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hallazgosNominaPila" type="{http://www.ugpp.gov.co/schema/Transversales/HallazgoNominaTipo/v1}HallazgoNominaTipo" minOccurs="0"/>
 *         &lt;element name="hallazgosConciliacionNomina" type="{http://www.ugpp.gov.co/schema/Transversales/HallazgoConciliacionContableTipo/v1}HallazgoConciliacionContableTipo" minOccurs="0"/>
 *         &lt;element name="archivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="documentosFinales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="descCargueNominaConciliacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esSolicitarAclaraciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPilaDepurada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntrada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoEjecucionLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valLiquidacionAporte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPartidasGlobales" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valLiquidacionSancionOmision" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valLiquidacionSancionInexactitud" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esIncumplimiento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalCierre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlLiquidadorTipo", propOrder = {
    "idControlLiquidador",
    "codProcesoSolicitador",
    "aportante",
    "expediente",
    "formatoNomina",
    "formatoConciliacion",
    "idInformacionExternaNomina",
    "idInformacionExternaConciliacion",
    "idEnvioNomina",
    "idEnvioConciliacion",
    "nomina",
    "conciliacionContable",
    "hojaCalculo",
    "codOrigen",
    "codLineaAccion",
    "codTipoDenuncia",
    "valPrograma",
    "hallazgosNominaPila",
    "hallazgosConciliacionNomina",
    "archivosTemporales",
    "documentosFinales",
    "descCargueNominaConciliacion",
    "esSolicitarAclaraciones",
    "idPilaDepurada",
    "fecRadicadoEntrada",
    "fecRadicadoSalida",
    "idRadicadoEntrada",
    "idRadicadoSalida",
    "codTipoEjecucionLiquidador",
    "valLiquidacionAporte",
    "valPartidasGlobales",
    "valLiquidacionSancionOmision",
    "valLiquidacionSancionInexactitud",
    "esIncumplimiento",
    "codCausalCierre"
})
public class ControlLiquidadorTipo {

    @XmlElement(nillable = true)
    protected String idControlLiquidador;
    @XmlElement(nillable = true)
    protected String codProcesoSolicitador;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(nillable = true)
    protected FormatoTipo formatoNomina;
    @XmlElement(nillable = true)
    protected FormatoTipo formatoConciliacion;
    @XmlElement(nillable = true)
    protected String idInformacionExternaNomina;
    @XmlElement(nillable = true)
    protected String idInformacionExternaConciliacion;
    @XmlElement(nillable = true)
    protected String idEnvioNomina;
    @XmlElement(nillable = true)
    protected String idEnvioConciliacion;
    @XmlElement(nillable = true)
    protected NominaTipo nomina;
    @XmlElement(nillable = true)
    protected ConciliacionContableTipo conciliacionContable;
    @XmlElement(nillable = true)
    protected HojaCalculoTipo hojaCalculo;
    @XmlElement(nillable = true)
    protected String codOrigen;
    @XmlElement(nillable = true)
    protected String codLineaAccion;
    @XmlElement(nillable = true)
    protected String codTipoDenuncia;
    @XmlElement(nillable = true)
    protected String valPrograma;
    @XmlElement(nillable = true)
    protected HallazgoNominaTipo hallazgosNominaPila;
    @XmlElement(nillable = true)
    protected HallazgoConciliacionContableTipo hallazgosConciliacionNomina;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> archivosTemporales;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> documentosFinales;
    @XmlElement(nillable = true)
    protected String descCargueNominaConciliacion;
    @XmlElement(nillable = true)
    protected String esSolicitarAclaraciones;
    @XmlElement(nillable = true)
    protected String idPilaDepurada;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoEntrada;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoSalida;
    @XmlElement(nillable = true)
    protected String idRadicadoEntrada;
    @XmlElement(nillable = true)
    protected String idRadicadoSalida;
    @XmlElement(nillable = true)
    protected String codTipoEjecucionLiquidador;
    @XmlElement(nillable = true)
    protected String valLiquidacionAporte;
    @XmlElement(nillable = true)
    protected String valPartidasGlobales;
    @XmlElement(nillable = true)
    protected String valLiquidacionSancionOmision;
    @XmlElement(nillable = true)
    protected String valLiquidacionSancionInexactitud;
    @XmlElement(nillable = true)
    protected String esIncumplimiento;
    @XmlElement(nillable = true)
    protected String codCausalCierre;

    /**
     * Obtiene el valor de la propiedad idControlLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdControlLiquidador() {
        return idControlLiquidador;
    }

    /**
     * Define el valor de la propiedad idControlLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdControlLiquidador(String value) {
        this.idControlLiquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad codProcesoSolicitador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProcesoSolicitador() {
        return codProcesoSolicitador;
    }

    /**
     * Define el valor de la propiedad codProcesoSolicitador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProcesoSolicitador(String value) {
        this.codProcesoSolicitador = value;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad formatoNomina.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormatoNomina() {
        return formatoNomina;
    }

    /**
     * Define el valor de la propiedad formatoNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormatoNomina(FormatoTipo value) {
        this.formatoNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad formatoConciliacion.
     * 
     * @return
     *     possible object is
     *     {@link FormatoTipo }
     *     
     */
    public FormatoTipo getFormatoConciliacion() {
        return formatoConciliacion;
    }

    /**
     * Define el valor de la propiedad formatoConciliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoTipo }
     *     
     */
    public void setFormatoConciliacion(FormatoTipo value) {
        this.formatoConciliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idInformacionExternaNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInformacionExternaNomina() {
        return idInformacionExternaNomina;
    }

    /**
     * Define el valor de la propiedad idInformacionExternaNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInformacionExternaNomina(String value) {
        this.idInformacionExternaNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad idInformacionExternaConciliacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInformacionExternaConciliacion() {
        return idInformacionExternaConciliacion;
    }

    /**
     * Define el valor de la propiedad idInformacionExternaConciliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInformacionExternaConciliacion(String value) {
        this.idInformacionExternaConciliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idEnvioNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEnvioNomina() {
        return idEnvioNomina;
    }

    /**
     * Define el valor de la propiedad idEnvioNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEnvioNomina(String value) {
        this.idEnvioNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad idEnvioConciliacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEnvioConciliacion() {
        return idEnvioConciliacion;
    }

    /**
     * Define el valor de la propiedad idEnvioConciliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEnvioConciliacion(String value) {
        this.idEnvioConciliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad nomina.
     * 
     * @return
     *     possible object is
     *     {@link NominaTipo }
     *     
     */
    public NominaTipo getNomina() {
        return nomina;
    }

    /**
     * Define el valor de la propiedad nomina.
     * 
     * @param value
     *     allowed object is
     *     {@link NominaTipo }
     *     
     */
    public void setNomina(NominaTipo value) {
        this.nomina = value;
    }

    /**
     * Obtiene el valor de la propiedad conciliacionContable.
     * 
     * @return
     *     possible object is
     *     {@link ConciliacionContableTipo }
     *     
     */
    public ConciliacionContableTipo getConciliacionContable() {
        return conciliacionContable;
    }

    /**
     * Define el valor de la propiedad conciliacionContable.
     * 
     * @param value
     *     allowed object is
     *     {@link ConciliacionContableTipo }
     *     
     */
    public void setConciliacionContable(ConciliacionContableTipo value) {
        this.conciliacionContable = value;
    }

    /**
     * Obtiene el valor de la propiedad hojaCalculo.
     * 
     * @return
     *     possible object is
     *     {@link HojaCalculoTipo }
     *     
     */
    public HojaCalculoTipo getHojaCalculo() {
        return hojaCalculo;
    }

    /**
     * Define el valor de la propiedad hojaCalculo.
     * 
     * @param value
     *     allowed object is
     *     {@link HojaCalculoTipo }
     *     
     */
    public void setHojaCalculo(HojaCalculoTipo value) {
        this.hojaCalculo = value;
    }

    /**
     * Obtiene el valor de la propiedad codOrigen.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodOrigen() {
        return codOrigen;
    }

    /**
     * Define el valor de la propiedad codOrigen.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodOrigen(String value) {
        this.codOrigen = value;
    }

    /**
     * Obtiene el valor de la propiedad codLineaAccion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodLineaAccion() {
        return codLineaAccion;
    }

    /**
     * Define el valor de la propiedad codLineaAccion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodLineaAccion(String value) {
        this.codLineaAccion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDenuncia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDenuncia() {
        return codTipoDenuncia;
    }

    /**
     * Define el valor de la propiedad codTipoDenuncia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDenuncia(String value) {
        this.codTipoDenuncia = value;
    }

    /**
     * Obtiene el valor de la propiedad valPrograma.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPrograma() {
        return valPrograma;
    }

    /**
     * Define el valor de la propiedad valPrograma.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPrograma(String value) {
        this.valPrograma = value;
    }

    /**
     * Obtiene el valor de la propiedad hallazgosNominaPila.
     * 
     * @return
     *     possible object is
     *     {@link HallazgoNominaTipo }
     *     
     */
    public HallazgoNominaTipo getHallazgosNominaPila() {
        return hallazgosNominaPila;
    }

    /**
     * Define el valor de la propiedad hallazgosNominaPila.
     * 
     * @param value
     *     allowed object is
     *     {@link HallazgoNominaTipo }
     *     
     */
    public void setHallazgosNominaPila(HallazgoNominaTipo value) {
        this.hallazgosNominaPila = value;
    }

    /**
     * Obtiene el valor de la propiedad hallazgosConciliacionNomina.
     * 
     * @return
     *     possible object is
     *     {@link HallazgoConciliacionContableTipo }
     *     
     */
    public HallazgoConciliacionContableTipo getHallazgosConciliacionNomina() {
        return hallazgosConciliacionNomina;
    }

    /**
     * Define el valor de la propiedad hallazgosConciliacionNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link HallazgoConciliacionContableTipo }
     *     
     */
    public void setHallazgosConciliacionNomina(HallazgoConciliacionContableTipo value) {
        this.hallazgosConciliacionNomina = value;
    }

    /**
     * Gets the value of the archivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the archivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getArchivosTemporales() {
        if (archivosTemporales == null) {
            archivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.archivosTemporales;
    }

    /**
     * Gets the value of the documentosFinales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentosFinales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentosFinales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getDocumentosFinales() {
        if (documentosFinales == null) {
            documentosFinales = new ArrayList<ParametroValoresTipo>();
        }
        return this.documentosFinales;
    }

    /**
     * Obtiene el valor de la propiedad descCargueNominaConciliacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCargueNominaConciliacion() {
        return descCargueNominaConciliacion;
    }

    /**
     * Define el valor de la propiedad descCargueNominaConciliacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCargueNominaConciliacion(String value) {
        this.descCargueNominaConciliacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esSolicitarAclaraciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsSolicitarAclaraciones() {
        return esSolicitarAclaraciones;
    }

    /**
     * Define el valor de la propiedad esSolicitarAclaraciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsSolicitarAclaraciones(String value) {
        this.esSolicitarAclaraciones = value;
    }

    /**
     * Obtiene el valor de la propiedad idPilaDepurada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPilaDepurada() {
        return idPilaDepurada;
    }

    /**
     * Define el valor de la propiedad idPilaDepurada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPilaDepurada(String value) {
        this.idPilaDepurada = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoEntrada() {
        return fecRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad fecRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoEntrada(Calendar value) {
        this.fecRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoSalida() {
        return fecRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad fecRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoSalida(Calendar value) {
        this.fecRadicadoSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntrada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntrada() {
        return idRadicadoEntrada;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntrada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntrada(String value) {
        this.idRadicadoEntrada = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalida() {
        return idRadicadoSalida;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalida(String value) {
        this.idRadicadoSalida = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoEjecucionLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoEjecucionLiquidador() {
        return codTipoEjecucionLiquidador;
    }

    /**
     * Define el valor de la propiedad codTipoEjecucionLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoEjecucionLiquidador(String value) {
        this.codTipoEjecucionLiquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad valLiquidacionAporte.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLiquidacionAporte() {
        return valLiquidacionAporte;
    }

    /**
     * Define el valor de la propiedad valLiquidacionAporte.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLiquidacionAporte(String value) {
        this.valLiquidacionAporte = value;
    }

    /**
     * Obtiene el valor de la propiedad valPartidasGlobales.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPartidasGlobales() {
        return valPartidasGlobales;
    }

    /**
     * Define el valor de la propiedad valPartidasGlobales.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPartidasGlobales(String value) {
        this.valPartidasGlobales = value;
    }

    /**
     * Obtiene el valor de la propiedad valLiquidacionSancionOmision.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLiquidacionSancionOmision() {
        return valLiquidacionSancionOmision;
    }

    /**
     * Define el valor de la propiedad valLiquidacionSancionOmision.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLiquidacionSancionOmision(String value) {
        this.valLiquidacionSancionOmision = value;
    }

    /**
     * Obtiene el valor de la propiedad valLiquidacionSancionInexactitud.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLiquidacionSancionInexactitud() {
        return valLiquidacionSancionInexactitud;
    }

    /**
     * Define el valor de la propiedad valLiquidacionSancionInexactitud.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLiquidacionSancionInexactitud(String value) {
        this.valLiquidacionSancionInexactitud = value;
    }

    /**
     * Obtiene el valor de la propiedad esIncumplimiento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsIncumplimiento() {
        return esIncumplimiento;
    }

    /**
     * Define el valor de la propiedad esIncumplimiento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsIncumplimiento(String value) {
        this.esIncumplimiento = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalCierre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalCierre() {
        return codCausalCierre;
    }

    /**
     * Define el valor de la propiedad codCausalCierre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalCierre(String value) {
        this.codCausalCierre = value;
    }

}
