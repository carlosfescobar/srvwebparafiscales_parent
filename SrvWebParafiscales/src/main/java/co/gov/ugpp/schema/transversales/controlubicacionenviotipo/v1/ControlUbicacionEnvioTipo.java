
package co.gov.ugpp.schema.transversales.controlubicacionenviotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.schema.transversales.eventoenviotipo.v1.EventoEnvioTipo;


/**
 * <p>Clase Java para ControlUbicacionEnvioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ControlUbicacionEnvioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idControlUbicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="eventoEnvio" type="{http://www.ugpp.gov.co/schema/Transversales/EventoEnvioTipo/v1}EventoEnvioTipo" minOccurs="0"/>
 *         &lt;element name="codEstadoUbicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoUbicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ubicacion" type="{http://www.ugpp.gov.co/esb/schema/UbicacionPersonaTipo/v1}UbicacionPersonaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ControlUbicacionEnvioTipo", propOrder = {
    "idControlUbicacion",
    "eventoEnvio",
    "codEstadoUbicacion",
    "descEstadoUbicacion",
    "ubicacion"
})
public class ControlUbicacionEnvioTipo {

    @XmlElement(nillable = true)
    protected String idControlUbicacion;
    @XmlElement(nillable = true)
    protected EventoEnvioTipo eventoEnvio;
    @XmlElement(nillable = true)
    protected String codEstadoUbicacion;
    @XmlElement(nillable = true)
    protected String descEstadoUbicacion;
    @XmlElement(nillable = true)
    protected UbicacionPersonaTipo ubicacion;

    /**
     * Obtiene el valor de la propiedad idControlUbicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdControlUbicacion() {
        return idControlUbicacion;
    }

    /**
     * Define el valor de la propiedad idControlUbicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdControlUbicacion(String value) {
        this.idControlUbicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad eventoEnvio.
     * 
     * @return
     *     possible object is
     *     {@link EventoEnvioTipo }
     *     
     */
    public EventoEnvioTipo getEventoEnvio() {
        return eventoEnvio;
    }

    /**
     * Define el valor de la propiedad eventoEnvio.
     * 
     * @param value
     *     allowed object is
     *     {@link EventoEnvioTipo }
     *     
     */
    public void setEventoEnvio(EventoEnvioTipo value) {
        this.eventoEnvio = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoUbicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoUbicacion() {
        return codEstadoUbicacion;
    }

    /**
     * Define el valor de la propiedad codEstadoUbicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoUbicacion(String value) {
        this.codEstadoUbicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoUbicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoUbicacion() {
        return descEstadoUbicacion;
    }

    /**
     * Define el valor de la propiedad descEstadoUbicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoUbicacion(String value) {
        this.descEstadoUbicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacion.
     * 
     * @return
     *     possible object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public UbicacionPersonaTipo getUbicacion() {
        return ubicacion;
    }

    /**
     * Define el valor de la propiedad ubicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public void setUbicacion(UbicacionPersonaTipo value) {
        this.ubicacion = value;
    }

}
