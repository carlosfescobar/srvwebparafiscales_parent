
package co.gov.ugpp.schema.transversales.correoelectronicotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para CorreoElectronicoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="CorreoElectronicoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valCorreoElectronico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFuente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFuenteCorreo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CorreoElectronicoTipo", propOrder = {
    "valCorreoElectronico",
    "codFuente",
    "descFuenteCorreo"
})
public class CorreoElectronicoTipo {

    @XmlElement(nillable = true)
    protected String valCorreoElectronico;
    @XmlElement(nillable = true)
    protected String codFuente;
    @XmlElement(nillable = true)
    protected String descFuenteCorreo;

    /**
     * Obtiene el valor de la propiedad valCorreoElectronico.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValCorreoElectronico() {
        return valCorreoElectronico;
    }

    /**
     * Define el valor de la propiedad valCorreoElectronico.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValCorreoElectronico(String value) {
        this.valCorreoElectronico = value;
    }

    /**
     * Obtiene el valor de la propiedad codFuente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFuente() {
        return codFuente;
    }

    /**
     * Define el valor de la propiedad codFuente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFuente(String value) {
        this.codFuente = value;
    }

    /**
     * Obtiene el valor de la propiedad descFuenteCorreo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFuenteCorreo() {
        return descFuenteCorreo;
    }

    /**
     * Define el valor de la propiedad descFuenteCorreo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFuenteCorreo(String value) {
        this.descFuenteCorreo = value;
    }

}
