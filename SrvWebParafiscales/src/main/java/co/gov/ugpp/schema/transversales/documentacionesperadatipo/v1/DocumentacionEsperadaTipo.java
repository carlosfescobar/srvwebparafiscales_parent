
package co.gov.ugpp.schema.transversales.documentacionesperadatipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.denuncias.funcionariotipo.v1.FuncionarioTipo;


/**
 * <p>Clase Java para DocumentacionEsperadaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentacionEsperadaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idInformacionEsperada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idInstanciaProcesoReanudacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalidaCorrespondencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicadoEntradaCorrespondencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentos" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="fecEntradaDocumentos" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNombreModeloProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDescripcion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="funcionario" type="{http://www.ugpp.gov.co/schema/Denuncias/FuncionarioTipo/v1}FuncionarioTipo" minOccurs="0"/>
 *         &lt;element name="codEstado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentacionEsperadaTipo", propOrder = {
    "idInformacionEsperada",
    "idInstanciaProcesoReanudacion",
    "idRadicadoSalidaCorrespondencia",
    "idRadicadoEntradaCorrespondencia",
    "idDocumentos",
    "fecEntradaDocumentos",
    "idExpediente",
    "valNombreModeloProceso",
    "valDescripcion",
    "funcionario",
    "codEstado"
})
public class DocumentacionEsperadaTipo {

    @XmlElement(nillable = true)
    protected String idInformacionEsperada;
    @XmlElement(nillable = true)
    protected String idInstanciaProcesoReanudacion;
    @XmlElement(nillable = true)
    protected String idRadicadoSalidaCorrespondencia;
    @XmlElement(nillable = true)
    protected String idRadicadoEntradaCorrespondencia;
    @XmlElement(nillable = true)
    protected List<String> idDocumentos;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecEntradaDocumentos;
    @XmlElement(nillable = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected String valNombreModeloProceso;
    @XmlElement(nillable = true)
    protected String valDescripcion;
    @XmlElement(nillable = true)
    protected FuncionarioTipo funcionario;
    @XmlElement(nillable = true)
    protected String codEstado;

    /**
     * Obtiene el valor de la propiedad idInformacionEsperada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInformacionEsperada() {
        return idInformacionEsperada;
    }

    /**
     * Define el valor de la propiedad idInformacionEsperada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInformacionEsperada(String value) {
        this.idInformacionEsperada = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaProcesoReanudacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaProcesoReanudacion() {
        return idInstanciaProcesoReanudacion;
    }

    /**
     * Define el valor de la propiedad idInstanciaProcesoReanudacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaProcesoReanudacion(String value) {
        this.idInstanciaProcesoReanudacion = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalidaCorrespondencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalidaCorrespondencia() {
        return idRadicadoSalidaCorrespondencia;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalidaCorrespondencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalidaCorrespondencia(String value) {
        this.idRadicadoSalidaCorrespondencia = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoEntradaCorrespondencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoEntradaCorrespondencia() {
        return idRadicadoEntradaCorrespondencia;
    }

    /**
     * Define el valor de la propiedad idRadicadoEntradaCorrespondencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoEntradaCorrespondencia(String value) {
        this.idRadicadoEntradaCorrespondencia = value;
    }

    /**
     * Gets the value of the idDocumentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentos() {
        if (idDocumentos == null) {
            idDocumentos = new ArrayList<String>();
        }
        return this.idDocumentos;
    }

    /**
     * Obtiene el valor de la propiedad fecEntradaDocumentos.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecEntradaDocumentos() {
        return fecEntradaDocumentos;
    }

    /**
     * Define el valor de la propiedad fecEntradaDocumentos.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecEntradaDocumentos(Calendar value) {
        this.fecEntradaDocumentos = value;
    }

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreModeloProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreModeloProceso() {
        return valNombreModeloProceso;
    }

    /**
     * Define el valor de la propiedad valNombreModeloProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreModeloProceso(String value) {
        this.valNombreModeloProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad valDescripcion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDescripcion() {
        return valDescripcion;
    }

    /**
     * Define el valor de la propiedad valDescripcion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDescripcion(String value) {
        this.valDescripcion = value;
    }

    /**
     * Obtiene el valor de la propiedad funcionario.
     * 
     * @return
     *     possible object is
     *     {@link FuncionarioTipo }
     *     
     */
    public FuncionarioTipo getFuncionario() {
        return funcionario;
    }

    /**
     * Define el valor de la propiedad funcionario.
     * 
     * @param value
     *     allowed object is
     *     {@link FuncionarioTipo }
     *     
     */
    public void setFuncionario(FuncionarioTipo value) {
        this.funcionario = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstado() {
        return codEstado;
    }

    /**
     * Define el valor de la propiedad codEstado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstado(String value) {
        this.codEstado = value;
    }

}
