
package co.gov.ugpp.schema.transversales.documentoplantillatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.datosplantillatipo.v1.DatosPlantillaTipo;


/**
 * <p>Clase Java para DocumentoPlantillaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="DocumentoPlantillaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="valContenidoDocumento" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="codTipoMIMEDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datosPlantilla" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DatosPlantillaTipo/v1}DatosPlantillaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoPlantillaTipo", propOrder = {
    "valContenidoDocumento",
    "codTipoMIMEDocumento",
    "datosPlantilla"
})
public class DocumentoPlantillaTipo {

    @XmlElement(nillable = true)
    protected byte[] valContenidoDocumento;
    @XmlElement(nillable = true)
    protected String codTipoMIMEDocumento;
    @XmlElement(nillable = true)
    protected List<DatosPlantillaTipo> datosPlantilla;

    /**
     * Obtiene el valor de la propiedad valContenidoDocumento.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getValContenidoDocumento() {
        return valContenidoDocumento;
    }

    /**
     * Define el valor de la propiedad valContenidoDocumento.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setValContenidoDocumento(byte[] value) {
        this.valContenidoDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoMIMEDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoMIMEDocumento() {
        return codTipoMIMEDocumento;
    }

    /**
     * Define el valor de la propiedad codTipoMIMEDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoMIMEDocumento(String value) {
        this.codTipoMIMEDocumento = value;
    }

    /**
     * Gets the value of the datosPlantilla property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datosPlantilla property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatosPlantilla().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DatosPlantillaTipo }
     * 
     * 
     */
    public List<DatosPlantillaTipo> getDatosPlantilla() {
        if (datosPlantilla == null) {
            datosPlantilla = new ArrayList<DatosPlantillaTipo>();
        }
        return this.datosPlantilla;
    }

}
