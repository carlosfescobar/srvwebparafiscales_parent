
package co.gov.ugpp.schema.transversales.entidadnegociotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;


/**
 * <p>Clase Java para EntidadNegocioTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="EntidadNegocioTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idEntidadNegocio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idArchivosTemporales" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EntidadNegocioTipo", propOrder = {
    "idEntidadNegocio",
    "codProceso",
    "idArchivosTemporales"
})
public class EntidadNegocioTipo {

    @XmlElement(nillable = true)
    protected String idEntidadNegocio;
    @XmlElement(nillable = true)
    protected String codProceso;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> idArchivosTemporales;

    /**
     * Obtiene el valor de la propiedad idEntidadNegocio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEntidadNegocio() {
        return idEntidadNegocio;
    }

    /**
     * Define el valor de la propiedad idEntidadNegocio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEntidadNegocio(String value) {
        this.idEntidadNegocio = value;
    }

    /**
     * Obtiene el valor de la propiedad codProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProceso() {
        return codProceso;
    }

    /**
     * Define el valor de la propiedad codProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProceso(String value) {
        this.codProceso = value;
    }

    /**
     * Gets the value of the idArchivosTemporales property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idArchivosTemporales property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdArchivosTemporales().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getIdArchivosTemporales() {
        if (idArchivosTemporales == null) {
            idArchivosTemporales = new ArrayList<ParametroValoresTipo>();
        }
        return this.idArchivosTemporales;
    }

}
