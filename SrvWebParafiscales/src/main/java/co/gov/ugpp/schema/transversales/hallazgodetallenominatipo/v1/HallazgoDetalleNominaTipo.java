
package co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;


/**
 * <p>Clase Java para HallazgoDetalleNominaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HallazgoDetalleNominaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHallazgoNominaDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCotizante" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="valNombreCotizante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPeriodoResto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoCotizante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservacionHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuarioCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valUsuarioModificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFechaCreacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valFechaModificacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HallazgoDetalleNominaTipo", propOrder = {
    "idHallazgoNominaDetalle",
    "idCotizante",
    "valNombreCotizante",
    "valPeriodoResto",
    "codTipoCotizante",
    "esHallazgo",
    "descObservacionHallazgo",
    "descHallazgo",
    "valUsuarioCreacion",
    "valUsuarioModificacion",
    "valFechaCreacion",
    "valFechaModificacion"
})
public class HallazgoDetalleNominaTipo {

    protected String idHallazgoNominaDetalle;
    protected IdentificacionTipo idCotizante;
    protected String valNombreCotizante;
    protected String valPeriodoResto;
    protected String codTipoCotizante;
    protected String esHallazgo;
    protected String descObservacionHallazgo;
    protected String descHallazgo;
    protected String valUsuarioCreacion;
    protected String valUsuarioModificacion;
    protected String valFechaCreacion;
    protected String valFechaModificacion;

    /**
     * Obtiene el valor de la propiedad idHallazgoNominaDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHallazgoNominaDetalle() {
        return idHallazgoNominaDetalle;
    }

    /**
     * Define el valor de la propiedad idHallazgoNominaDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHallazgoNominaDetalle(String value) {
        this.idHallazgoNominaDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idCotizante.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdCotizante() {
        return idCotizante;
    }

    /**
     * Define el valor de la propiedad idCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdCotizante(IdentificacionTipo value) {
        this.idCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreCotizante() {
        return valNombreCotizante;
    }

    /**
     * Define el valor de la propiedad valNombreCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreCotizante(String value) {
        this.valNombreCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad valPeriodoResto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPeriodoResto() {
        return valPeriodoResto;
    }

    /**
     * Define el valor de la propiedad valPeriodoResto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPeriodoResto(String value) {
        this.valPeriodoResto = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoCotizante() {
        return codTipoCotizante;
    }

    /**
     * Define el valor de la propiedad codTipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoCotizante(String value) {
        this.codTipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad esHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsHallazgo() {
        return esHallazgo;
    }

    /**
     * Define el valor de la propiedad esHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsHallazgo(String value) {
        this.esHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservacionHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservacionHallazgo() {
        return descObservacionHallazgo;
    }

    /**
     * Define el valor de la propiedad descObservacionHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservacionHallazgo(String value) {
        this.descObservacionHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad descHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescHallazgo() {
        return descHallazgo;
    }

    /**
     * Define el valor de la propiedad descHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescHallazgo(String value) {
        this.descHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuarioCreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuarioCreacion() {
        return valUsuarioCreacion;
    }

    /**
     * Define el valor de la propiedad valUsuarioCreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuarioCreacion(String value) {
        this.valUsuarioCreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valUsuarioModificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValUsuarioModificacion() {
        return valUsuarioModificacion;
    }

    /**
     * Define el valor de la propiedad valUsuarioModificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValUsuarioModificacion(String value) {
        this.valUsuarioModificacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valFechaCreacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFechaCreacion() {
        return valFechaCreacion;
    }

    /**
     * Define el valor de la propiedad valFechaCreacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFechaCreacion(String value) {
        this.valFechaCreacion = value;
    }

    /**
     * Obtiene el valor de la propiedad valFechaModificacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValFechaModificacion() {
        return valFechaModificacion;
    }

    /**
     * Define el valor de la propiedad valFechaModificacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValFechaModificacion(String value) {
        this.valFechaModificacion = value;
    }

}
