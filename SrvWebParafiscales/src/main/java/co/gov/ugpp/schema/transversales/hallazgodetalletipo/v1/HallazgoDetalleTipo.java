
package co.gov.ugpp.schema.transversales.hallazgodetalletipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;


/**
 * <p>Clase Java para HallazgoDetalleTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HallazgoDetalleTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHallazgoDetalle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCotizante" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="valNombreCotizante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valPeriodoResto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoCotizante" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valTipoHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoHallazgo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conceptoContable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valContabilidad" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDiferencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoMedida" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="operador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HallazgoDetalleTipo", propOrder = {
    "idHallazgoDetalle",
    "idCotizante",
    "valNombreCotizante",
    "valPeriodoResto",
    "codTipoCotizante",
    "descHallazgo",
    "valTipoHallazgo",
    "codTipoHallazgo",
    "conceptoContable",
    "valNomina",
    "valContabilidad",
    "valDiferencia",
    "tipoMedida",
    "valor",
    "operador"
})
public class HallazgoDetalleTipo {

    protected String idHallazgoDetalle;
    protected IdentificacionTipo idCotizante;
    protected String valNombreCotizante;
    protected String valPeriodoResto;
    protected String codTipoCotizante;
    protected String descHallazgo;
    protected String valTipoHallazgo;
    protected String codTipoHallazgo;
    protected String conceptoContable;
    protected String valNomina;
    protected String valContabilidad;
    protected String valDiferencia;
    protected String tipoMedida;
    protected String valor;
    protected String operador;

    /**
     * Obtiene el valor de la propiedad idHallazgoDetalle.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHallazgoDetalle() {
        return idHallazgoDetalle;
    }

    /**
     * Define el valor de la propiedad idHallazgoDetalle.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHallazgoDetalle(String value) {
        this.idHallazgoDetalle = value;
    }

    /**
     * Obtiene el valor de la propiedad idCotizante.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdCotizante() {
        return idCotizante;
    }

    /**
     * Define el valor de la propiedad idCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdCotizante(IdentificacionTipo value) {
        this.idCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad valNombreCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNombreCotizante() {
        return valNombreCotizante;
    }

    /**
     * Define el valor de la propiedad valNombreCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNombreCotizante(String value) {
        this.valNombreCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad valPeriodoResto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValPeriodoResto() {
        return valPeriodoResto;
    }

    /**
     * Define el valor de la propiedad valPeriodoResto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValPeriodoResto(String value) {
        this.valPeriodoResto = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoCotizante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoCotizante() {
        return codTipoCotizante;
    }

    /**
     * Define el valor de la propiedad codTipoCotizante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoCotizante(String value) {
        this.codTipoCotizante = value;
    }

    /**
     * Obtiene el valor de la propiedad descHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescHallazgo() {
        return descHallazgo;
    }

    /**
     * Define el valor de la propiedad descHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescHallazgo(String value) {
        this.descHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad valTipoHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValTipoHallazgo() {
        return valTipoHallazgo;
    }

    /**
     * Define el valor de la propiedad valTipoHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValTipoHallazgo(String value) {
        this.valTipoHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoHallazgo() {
        return codTipoHallazgo;
    }

    /**
     * Define el valor de la propiedad codTipoHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoHallazgo(String value) {
        this.codTipoHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad conceptoContable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConceptoContable() {
        return conceptoContable;
    }

    /**
     * Define el valor de la propiedad conceptoContable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConceptoContable(String value) {
        this.conceptoContable = value;
    }

    /**
     * Obtiene el valor de la propiedad valNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValNomina() {
        return valNomina;
    }

    /**
     * Define el valor de la propiedad valNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValNomina(String value) {
        this.valNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad valContabilidad.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValContabilidad() {
        return valContabilidad;
    }

    /**
     * Define el valor de la propiedad valContabilidad.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValContabilidad(String value) {
        this.valContabilidad = value;
    }

    /**
     * Obtiene el valor de la propiedad valDiferencia.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiferencia() {
        return valDiferencia;
    }

    /**
     * Define el valor de la propiedad valDiferencia.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiferencia(String value) {
        this.valDiferencia = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoMedida.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMedida() {
        return tipoMedida;
    }

    /**
     * Define el valor de la propiedad tipoMedida.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMedida(String value) {
        this.tipoMedida = value;
    }

    /**
     * Obtiene el valor de la propiedad valor.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValor() {
        return valor;
    }

    /**
     * Define el valor de la propiedad valor.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValor(String value) {
        this.valor = value;
    }

    /**
     * Obtiene el valor de la propiedad operador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperador() {
        return operador;
    }

    /**
     * Define el valor de la propiedad operador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperador(String value) {
        this.operador = value;
    }

}
