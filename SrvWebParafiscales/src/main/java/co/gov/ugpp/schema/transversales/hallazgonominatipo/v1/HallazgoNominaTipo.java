
package co.gov.ugpp.schema.transversales.hallazgonominatipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;
import co.gov.ugpp.schema.transversales.hallazgodetallenominatipo.v1.HallazgoDetalleNominaTipo;


/**
 * <p>Clase Java para HallazgoNominaTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="HallazgoNominaTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idHallazgo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="idNomina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPila" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idConciliacionContable" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hallazgosDetalle" type="{http://www.ugpp.gov.co/schema/Transversales/HallazgoDetalleNominaTipo/v1}HallazgoDetalleNominaTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HallazgoNominaTipo", propOrder = {
    "idHallazgo",
    "expediente",
    "idNomina",
    "idPila",
    "idConciliacionContable",
    "hallazgosDetalle"
})
public class HallazgoNominaTipo {

    @XmlElement(required = true, nillable = true)
    protected String idHallazgo;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    protected String idNomina;
    protected String idPila;
    protected String idConciliacionContable;
    @XmlElement(nillable = true)
    protected List<HallazgoDetalleNominaTipo> hallazgosDetalle;

    /**
     * Obtiene el valor de la propiedad idHallazgo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHallazgo() {
        return idHallazgo;
    }

    /**
     * Define el valor de la propiedad idHallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHallazgo(String value) {
        this.idHallazgo = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idNomina.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNomina() {
        return idNomina;
    }

    /**
     * Define el valor de la propiedad idNomina.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNomina(String value) {
        this.idNomina = value;
    }

    /**
     * Obtiene el valor de la propiedad idPila.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPila() {
        return idPila;
    }

    /**
     * Define el valor de la propiedad idPila.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPila(String value) {
        this.idPila = value;
    }

    /**
     * Obtiene el valor de la propiedad idConciliacionContable.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdConciliacionContable() {
        return idConciliacionContable;
    }

    /**
     * Define el valor de la propiedad idConciliacionContable.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdConciliacionContable(String value) {
        this.idConciliacionContable = value;
    }

    /**
     * Gets the value of the hallazgosDetalle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hallazgosDetalle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHallazgosDetalle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HallazgoDetalleNominaTipo }
     * 
     * 
     */
    public List<HallazgoDetalleNominaTipo> getHallazgosDetalle() {
        if (hallazgosDetalle == null) {
            hallazgosDetalle = new ArrayList<HallazgoDetalleNominaTipo>();
        }
        return this.hallazgosDetalle;
    }

}
