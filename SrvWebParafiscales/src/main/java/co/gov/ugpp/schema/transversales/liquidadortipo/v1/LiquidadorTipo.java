
package co.gov.ugpp.schema.transversales.liquidadortipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter3;
import co.gov.ugpp.schema.denuncias.aportantetipo.v1.AportanteTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.transversales.hallazgotipo.v1.HallazgoTipo;


/**
 * <p>Clase Java para LiquidadorTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="LiquidadorTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estructuraArchivo" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoEstructuraTipo/v1}FormatoEstructuraTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="hallazgos" type="{http://www.ugpp.gov.co/schema/Transversales/HallazgoTipo/v1}HallazgoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="aportante" type="{http://www.ugpp.gov.co/schema/Denuncias/AportanteTipo/v1}AportanteTipo" minOccurs="0"/>
 *         &lt;element name="idVersion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFormato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRadicado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecInicioPeriodo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="fecFinPeriodo" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="idHojaCalculo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoLiquidador" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valLiquidadorObservacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LiquidadorTipo", propOrder = {
    "idExpediente",
    "idLiquidador",
    "estructuraArchivo",
    "hallazgos",
    "aportante",
    "idVersion",
    "idFormato",
    "idRadicado",
    "fecInicioPeriodo",
    "fecFinPeriodo",
    "idHojaCalculo",
    "codTipoLiquidador",
    "valLiquidadorObservacion"
})
public class LiquidadorTipo {

    @XmlElement(nillable = true)
    protected String idExpediente;
    @XmlElement(nillable = true)
    protected String idLiquidador;
    @XmlElement(nillable = true)
    protected List<FormatoEstructuraTipo> estructuraArchivo;
    @XmlElement(nillable = true)
    protected List<HallazgoTipo> hallazgos;
    @XmlElement(nillable = true)
    protected AportanteTipo aportante;
    @XmlElement(nillable = true)
    protected String idVersion;
    @XmlElement(nillable = true)
    protected String idFormato;
    @XmlElement(nillable = true)
    protected String idRadicado;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecInicioPeriodo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter3 .class)
    @XmlSchemaType(name = "date")
    protected Calendar fecFinPeriodo;
    @XmlElement(nillable = true)
    protected String idHojaCalculo;
    @XmlElement(nillable = true)
    protected String codTipoLiquidador;
    @XmlElement(nillable = true)
    protected String valLiquidadorObservacion;

    /**
     * Obtiene el valor de la propiedad idExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdExpediente() {
        return idExpediente;
    }

    /**
     * Define el valor de la propiedad idExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdExpediente(String value) {
        this.idExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdLiquidador() {
        return idLiquidador;
    }

    /**
     * Define el valor de la propiedad idLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdLiquidador(String value) {
        this.idLiquidador = value;
    }

    /**
     * Gets the value of the estructuraArchivo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estructuraArchivo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstructuraArchivo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormatoEstructuraTipo }
     * 
     * 
     */
    public List<FormatoEstructuraTipo> getEstructuraArchivo() {
        if (estructuraArchivo == null) {
            estructuraArchivo = new ArrayList<FormatoEstructuraTipo>();
        }
        return this.estructuraArchivo;
    }

    /**
     * Gets the value of the hallazgos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hallazgos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHallazgos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HallazgoTipo }
     * 
     * 
     */
    public List<HallazgoTipo> getHallazgos() {
        if (hallazgos == null) {
            hallazgos = new ArrayList<HallazgoTipo>();
        }
        return this.hallazgos;
    }

    /**
     * Obtiene el valor de la propiedad aportante.
     * 
     * @return
     *     possible object is
     *     {@link AportanteTipo }
     *     
     */
    public AportanteTipo getAportante() {
        return aportante;
    }

    /**
     * Define el valor de la propiedad aportante.
     * 
     * @param value
     *     allowed object is
     *     {@link AportanteTipo }
     *     
     */
    public void setAportante(AportanteTipo value) {
        this.aportante = value;
    }

    /**
     * Obtiene el valor de la propiedad idVersion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdVersion() {
        return idVersion;
    }

    /**
     * Define el valor de la propiedad idVersion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdVersion(String value) {
        this.idVersion = value;
    }

    /**
     * Obtiene el valor de la propiedad idFormato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFormato() {
        return idFormato;
    }

    /**
     * Define el valor de la propiedad idFormato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFormato(String value) {
        this.idFormato = value;
    }

    /**
     * Obtiene el valor de la propiedad idRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicado() {
        return idRadicado;
    }

    /**
     * Define el valor de la propiedad idRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicado(String value) {
        this.idRadicado = value;
    }

    /**
     * Obtiene el valor de la propiedad fecInicioPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecInicioPeriodo() {
        return fecInicioPeriodo;
    }

    /**
     * Define el valor de la propiedad fecInicioPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecInicioPeriodo(Calendar value) {
        this.fecInicioPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecFinPeriodo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecFinPeriodo() {
        return fecFinPeriodo;
    }

    /**
     * Define el valor de la propiedad fecFinPeriodo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecFinPeriodo(Calendar value) {
        this.fecFinPeriodo = value;
    }

    /**
     * Obtiene el valor de la propiedad idHojaCalculo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdHojaCalculo() {
        return idHojaCalculo;
    }

    /**
     * Define el valor de la propiedad idHojaCalculo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdHojaCalculo(String value) {
        this.idHojaCalculo = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoLiquidador() {
        return codTipoLiquidador;
    }

    /**
     * Define el valor de la propiedad codTipoLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoLiquidador(String value) {
        this.codTipoLiquidador = value;
    }

    /**
     * Obtiene el valor de la propiedad valLiquidadorObservacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValLiquidadorObservacion() {
        return valLiquidadorObservacion;
    }

    /**
     * Define el valor de la propiedad valLiquidadorObservacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValLiquidadorObservacion(String value) {
        this.valLiquidadorObservacion = value;
    }

}
