
package co.gov.ugpp.schema.transversales.migracioncasotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;


/**
 * <p>Clase Java para MigracionCasoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MigracionCasoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idFila" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idMigracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idNumExpediente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codEstadoCaso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEstadoCaso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idInstanciaProceso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAportante" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="descErrorCaso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigracionCasoTipo", propOrder = {
    "idFila",
    "idMigracion",
    "idNumExpediente",
    "codEstadoCaso",
    "descEstadoCaso",
    "idInstanciaProceso",
    "idAportante",
    "descErrorCaso"
})
public class MigracionCasoTipo {

    @XmlElement(nillable = true)
    protected String idFila;
    @XmlElement(nillable = true)
    protected String idMigracion;
    @XmlElement(nillable = true)
    protected String idNumExpediente;
    @XmlElement(nillable = true)
    protected String codEstadoCaso;
    @XmlElement(nillable = true)
    protected String descEstadoCaso;
    @XmlElement(nillable = true)
    protected String idInstanciaProceso;
    protected IdentificacionTipo idAportante;
    @XmlElement(nillable = true)
    protected String descErrorCaso;

    /**
     * Obtiene el valor de la propiedad idFila.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFila() {
        return idFila;
    }

    /**
     * Define el valor de la propiedad idFila.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFila(String value) {
        this.idFila = value;
    }

    /**
     * Obtiene el valor de la propiedad idMigracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMigracion() {
        return idMigracion;
    }

    /**
     * Define el valor de la propiedad idMigracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMigracion(String value) {
        this.idMigracion = value;
    }

    /**
     * Obtiene el valor de la propiedad idNumExpediente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumExpediente() {
        return idNumExpediente;
    }

    /**
     * Define el valor de la propiedad idNumExpediente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumExpediente(String value) {
        this.idNumExpediente = value;
    }

    /**
     * Obtiene el valor de la propiedad codEstadoCaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEstadoCaso() {
        return codEstadoCaso;
    }

    /**
     * Define el valor de la propiedad codEstadoCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEstadoCaso(String value) {
        this.codEstadoCaso = value;
    }

    /**
     * Obtiene el valor de la propiedad descEstadoCaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEstadoCaso() {
        return descEstadoCaso;
    }

    /**
     * Define el valor de la propiedad descEstadoCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEstadoCaso(String value) {
        this.descEstadoCaso = value;
    }

    /**
     * Obtiene el valor de la propiedad idInstanciaProceso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdInstanciaProceso() {
        return idInstanciaProceso;
    }

    /**
     * Define el valor de la propiedad idInstanciaProceso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdInstanciaProceso(String value) {
        this.idInstanciaProceso = value;
    }

    /**
     * Obtiene el valor de la propiedad idAportante.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdAportante() {
        return idAportante;
    }

    /**
     * Define el valor de la propiedad idAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdAportante(IdentificacionTipo value) {
        this.idAportante = value;
    }

    /**
     * Obtiene el valor de la propiedad descErrorCaso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescErrorCaso() {
        return descErrorCaso;
    }

    /**
     * Define el valor de la propiedad descErrorCaso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescErrorCaso(String value) {
        this.descErrorCaso = value;
    }

}
