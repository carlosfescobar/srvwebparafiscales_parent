
package co.gov.ugpp.schema.transversales.migraciontipo.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.transversales.migracioncasotipo.v1.MigracionCasoTipo;


/**
 * <p>Clase Java para MigracionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="MigracionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idMigracion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUsuario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecMigracion" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="codEscenarioMigrado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descEscenarioMigrado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="casosMigrados" type="{http://www.ugpp.gov.co/schema/Transversales/MigracionCasoTipo/v1}MigracionCasoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MigracionTipo", propOrder = {
    "idMigracion",
    "idUsuario",
    "fecMigracion",
    "codEscenarioMigrado",
    "descEscenarioMigrado",
    "casosMigrados"
})
public class MigracionTipo {

    @XmlElement(nillable = true)
    protected String idMigracion;
    @XmlElement(nillable = true)
    protected String idUsuario;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecMigracion;
    @XmlElement(nillable = true)
    protected String codEscenarioMigrado;
    @XmlElement(nillable = true)
    protected String descEscenarioMigrado;
    protected List<MigracionCasoTipo> casosMigrados;

    /**
     * Obtiene el valor de la propiedad idMigracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMigracion() {
        return idMigracion;
    }

    /**
     * Define el valor de la propiedad idMigracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMigracion(String value) {
        this.idMigracion = value;
    }

    /**
     * Obtiene el valor de la propiedad idUsuario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUsuario() {
        return idUsuario;
    }

    /**
     * Define el valor de la propiedad idUsuario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUsuario(String value) {
        this.idUsuario = value;
    }

    /**
     * Obtiene el valor de la propiedad fecMigracion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecMigracion() {
        return fecMigracion;
    }

    /**
     * Define el valor de la propiedad fecMigracion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecMigracion(Calendar value) {
        this.fecMigracion = value;
    }

    /**
     * Obtiene el valor de la propiedad codEscenarioMigrado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodEscenarioMigrado() {
        return codEscenarioMigrado;
    }

    /**
     * Define el valor de la propiedad codEscenarioMigrado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodEscenarioMigrado(String value) {
        this.codEscenarioMigrado = value;
    }

    /**
     * Obtiene el valor de la propiedad descEscenarioMigrado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescEscenarioMigrado() {
        return descEscenarioMigrado;
    }

    /**
     * Define el valor de la propiedad descEscenarioMigrado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescEscenarioMigrado(String value) {
        this.descEscenarioMigrado = value;
    }

    /**
     * Gets the value of the casosMigrados property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the casosMigrados property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCasosMigrados().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MigracionCasoTipo }
     * 
     * 
     */
    public List<MigracionCasoTipo> getCasosMigrados() {
        if (casosMigrados == null) {
            casosMigrados = new ArrayList<MigracionCasoTipo>();
        }
        return this.casosMigrados;
    }

}
