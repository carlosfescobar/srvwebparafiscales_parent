
package co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para NumeroActoAdministrativoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="NumeroActoAdministrativoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idNumeroActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NumeroActoAdministrativoTipo", propOrder = {
    "idNumeroActoAdministrativo",
    "codTipoActoAdministrativo"
})
public class NumeroActoAdministrativoTipo {

    @XmlElement(nillable = true)
    protected String idNumeroActoAdministrativo;
    @XmlElement(nillable = true)
    protected String codTipoActoAdministrativo;

    /**
     * Obtiene el valor de la propiedad idNumeroActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumeroActoAdministrativo() {
        return idNumeroActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad idNumeroActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumeroActoAdministrativo(String value) {
        this.idNumeroActoAdministrativo = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoActoAdministrativo() {
        return codTipoActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad codTipoActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoActoAdministrativo(String value) {
        this.codTipoActoAdministrativo = value;
    }

}
