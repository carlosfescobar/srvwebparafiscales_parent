
package co.gov.ugpp.schema.transversales.publicacionwebtipo.v1;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.DocumentoTipo;


/**
 * <p>Clase Java para PublicacionWebTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="PublicacionWebTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="esPublicadoWeb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecPublicacionWeb" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="fecDespublicacionWeb" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idPersonaInteresada" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo" minOccurs="0"/>
 *         &lt;element name="docPublicado" type="{http://www.ugpp.gov.co/schema/GestionDocumental/DocumentoTipo/v1}DocumentoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PublicacionWebTipo", propOrder = {
    "esPublicadoWeb",
    "fecPublicacionWeb",
    "fecDespublicacionWeb",
    "idPersonaInteresada",
    "docPublicado"
})
public class PublicacionWebTipo {

    @XmlElement(nillable = true)
    protected String esPublicadoWeb;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecPublicacionWeb;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecDespublicacionWeb;
    @XmlElement(nillable = true)
    protected IdentificacionTipo idPersonaInteresada;
    @XmlElement(nillable = true)
    protected DocumentoTipo docPublicado;

    /**
     * Obtiene el valor de la propiedad esPublicadoWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsPublicadoWeb() {
        return esPublicadoWeb;
    }

    /**
     * Define el valor de la propiedad esPublicadoWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsPublicadoWeb(String value) {
        this.esPublicadoWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad fecPublicacionWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecPublicacionWeb() {
        return fecPublicacionWeb;
    }

    /**
     * Define el valor de la propiedad fecPublicacionWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecPublicacionWeb(Calendar value) {
        this.fecPublicacionWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad fecDespublicacionWeb.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecDespublicacionWeb() {
        return fecDespublicacionWeb;
    }

    /**
     * Define el valor de la propiedad fecDespublicacionWeb.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecDespublicacionWeb(Calendar value) {
        this.fecDespublicacionWeb = value;
    }

    /**
     * Obtiene el valor de la propiedad idPersonaInteresada.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdPersonaInteresada() {
        return idPersonaInteresada;
    }

    /**
     * Define el valor de la propiedad idPersonaInteresada.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdPersonaInteresada(IdentificacionTipo value) {
        this.idPersonaInteresada = value;
    }

    /**
     * Obtiene el valor de la propiedad docPublicado.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoTipo }
     *     
     */
    public DocumentoTipo getDocPublicado() {
        return docPublicado;
    }

    /**
     * Define el valor de la propiedad docPublicado.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoTipo }
     *     
     */
    public void setDocPublicado(DocumentoTipo value) {
        this.docPublicado = value;
    }

}
