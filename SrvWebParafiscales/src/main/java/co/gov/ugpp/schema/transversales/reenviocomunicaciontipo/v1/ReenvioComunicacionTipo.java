
package co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.schema.gestiondocumental.acuseentregadocumentotipo.v1.AcuseEntregaDocumentoTipo;
import co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ExpedienteTipo;


/**
 * <p>Clase Java para ReenvioComunicacionTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ReenvioComunicacionTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idRadicado" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="expediente" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ExpedienteTipo/v1}ExpedienteTipo" minOccurs="0"/>
 *         &lt;element name="idPersonaDestinatario" type="{http://www.ugpp.gov.co/esb/schema/IdentificacionTipo/v1}IdentificacionTipo"/>
 *         &lt;element name="codProcesoEnviaComunicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descProcesoEnviaComunicacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoDireccionEnviada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoDireccionEnviada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valDireccionEnviada" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descObservaciones" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFuenteInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFuenteInformacion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esReenvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trazabilidadRadicadoSalida" type="{http://www.ugpp.gov.co/schema/GestionDocumental/AcuseEntregaDocumentoTipo/v1}AcuseEntregaDocumentoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReenvioComunicacionTipo", propOrder = {
    "idRadicado",
    "expediente",
    "idPersonaDestinatario",
    "codProcesoEnviaComunicacion",
    "descProcesoEnviaComunicacion",
    "codTipoDireccionEnviada",
    "descTipoDireccionEnviada",
    "valDireccionEnviada",
    "descObservaciones",
    "codFuenteInformacion",
    "descFuenteInformacion",
    "esDevolucion",
    "esReenvio",
    "trazabilidadRadicadoSalida"
})
public class ReenvioComunicacionTipo {

    @XmlElement(nillable = true)
    protected String idRadicado;
    @XmlElement(nillable = true)
    protected ExpedienteTipo expediente;
    @XmlElement(required = true)
    protected IdentificacionTipo idPersonaDestinatario;
    @XmlElement(nillable = true)
    protected String codProcesoEnviaComunicacion;
    @XmlElement(nillable = true)
    protected String descProcesoEnviaComunicacion;
    @XmlElement(nillable = true)
    protected String codTipoDireccionEnviada;
    @XmlElement(nillable = true)
    protected String descTipoDireccionEnviada;
    @XmlElement(nillable = true)
    protected String valDireccionEnviada;
    @XmlElement(nillable = true)
    protected String descObservaciones;
    @XmlElement(nillable = true)
    protected String codFuenteInformacion;
    @XmlElement(nillable = true)
    protected String descFuenteInformacion;
    @XmlElement(nillable = true)
    protected String esDevolucion;
    @XmlElement(nillable = true)
    protected String esReenvio;
    protected List<AcuseEntregaDocumentoTipo> trazabilidadRadicadoSalida;

    /**
     * Obtiene el valor de la propiedad idRadicado.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicado() {
        return idRadicado;
    }

    /**
     * Define el valor de la propiedad idRadicado.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicado(String value) {
        this.idRadicado = value;
    }

    /**
     * Obtiene el valor de la propiedad expediente.
     * 
     * @return
     *     possible object is
     *     {@link ExpedienteTipo }
     *     
     */
    public ExpedienteTipo getExpediente() {
        return expediente;
    }

    /**
     * Define el valor de la propiedad expediente.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpedienteTipo }
     *     
     */
    public void setExpediente(ExpedienteTipo value) {
        this.expediente = value;
    }

    /**
     * Obtiene el valor de la propiedad idPersonaDestinatario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificacionTipo }
     *     
     */
    public IdentificacionTipo getIdPersonaDestinatario() {
        return idPersonaDestinatario;
    }

    /**
     * Define el valor de la propiedad idPersonaDestinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificacionTipo }
     *     
     */
    public void setIdPersonaDestinatario(IdentificacionTipo value) {
        this.idPersonaDestinatario = value;
    }

    /**
     * Obtiene el valor de la propiedad codProcesoEnviaComunicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodProcesoEnviaComunicacion() {
        return codProcesoEnviaComunicacion;
    }

    /**
     * Define el valor de la propiedad codProcesoEnviaComunicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodProcesoEnviaComunicacion(String value) {
        this.codProcesoEnviaComunicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descProcesoEnviaComunicacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescProcesoEnviaComunicacion() {
        return descProcesoEnviaComunicacion;
    }

    /**
     * Define el valor de la propiedad descProcesoEnviaComunicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescProcesoEnviaComunicacion(String value) {
        this.descProcesoEnviaComunicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoDireccionEnviada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoDireccionEnviada() {
        return codTipoDireccionEnviada;
    }

    /**
     * Define el valor de la propiedad codTipoDireccionEnviada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoDireccionEnviada(String value) {
        this.codTipoDireccionEnviada = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoDireccionEnviada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoDireccionEnviada() {
        return descTipoDireccionEnviada;
    }

    /**
     * Define el valor de la propiedad descTipoDireccionEnviada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoDireccionEnviada(String value) {
        this.descTipoDireccionEnviada = value;
    }

    /**
     * Obtiene el valor de la propiedad valDireccionEnviada.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDireccionEnviada() {
        return valDireccionEnviada;
    }

    /**
     * Define el valor de la propiedad valDireccionEnviada.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDireccionEnviada(String value) {
        this.valDireccionEnviada = value;
    }

    /**
     * Obtiene el valor de la propiedad descObservaciones.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescObservaciones() {
        return descObservaciones;
    }

    /**
     * Define el valor de la propiedad descObservaciones.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescObservaciones(String value) {
        this.descObservaciones = value;
    }

    /**
     * Obtiene el valor de la propiedad codFuenteInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFuenteInformacion() {
        return codFuenteInformacion;
    }

    /**
     * Define el valor de la propiedad codFuenteInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFuenteInformacion(String value) {
        this.codFuenteInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad descFuenteInformacion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFuenteInformacion() {
        return descFuenteInformacion;
    }

    /**
     * Define el valor de la propiedad descFuenteInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFuenteInformacion(String value) {
        this.descFuenteInformacion = value;
    }

    /**
     * Obtiene el valor de la propiedad esDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDevolucion() {
        return esDevolucion;
    }

    /**
     * Define el valor de la propiedad esDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDevolucion(String value) {
        this.esDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad esReenvio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsReenvio() {
        return esReenvio;
    }

    /**
     * Define el valor de la propiedad esReenvio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsReenvio(String value) {
        this.esReenvio = value;
    }

    /**
     * Gets the value of the trazabilidadRadicadoSalida property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the trazabilidadRadicadoSalida property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTrazabilidadRadicadoSalida().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AcuseEntregaDocumentoTipo }
     * 
     * 
     */
    public List<AcuseEntregaDocumentoTipo> getTrazabilidadRadicadoSalida() {
        if (trazabilidadRadicadoSalida == null) {
            trazabilidadRadicadoSalida = new ArrayList<AcuseEntregaDocumentoTipo>();
        }
        return this.trazabilidadRadicadoSalida;
    }

}
