
package co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.UbicacionPersonaTipo;
import co.gov.ugpp.parafiscales.util.adapter.Adapter1;
import co.gov.ugpp.schema.fiscalizacion.envioactoadministrativotipo.v1.EnvioActoAdministrativoTipo;
import co.gov.ugpp.schema.gestiondocumental.radicadotipo.v1.RadicadoTipo;


/**
 * <p>Clase Java para TrazabilidadRadicacionActoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="TrazabilidadRadicacionActoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idActoAdministrativo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="envioActoAdministrativo" type="{http://www.ugpp.gov.co/schema/Fiscalizacion/EnvioActoAdministrativoTipo/v1}EnvioActoAdministrativoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idRadicadoSalidaDefinitivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecRadicadoSalidaDefinitivo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="radicacionesEntrada" type="{http://www.ugpp.gov.co/schema/GestionDocumental/RadicadoTipo/v1}RadicadoTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valDiasProrroga" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codTipoNotificacionDefinitiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoNotificacionDefinitiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecNotificacionDefinitiva" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="ubicacionDefinitiva" type="{http://www.ugpp.gov.co/esb/schema/UbicacionPersonaTipo/v1}UbicacionPersonaTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrazabilidadRadicacionActoTipo", propOrder = {
    "idActoAdministrativo",
    "envioActoAdministrativo",
    "idRadicadoSalidaDefinitivo",
    "fecRadicadoSalidaDefinitivo",
    "radicacionesEntrada",
    "valDiasProrroga",
    "codTipoNotificacionDefinitiva",
    "descTipoNotificacionDefinitiva",
    "fecNotificacionDefinitiva",
    "ubicacionDefinitiva"
})
public class TrazabilidadRadicacionActoTipo {

    @XmlElement(nillable = true)
    protected String idActoAdministrativo;
    @XmlElement(nillable = true)
    protected List<EnvioActoAdministrativoTipo> envioActoAdministrativo;
    @XmlElement(nillable = true)
    protected String idRadicadoSalidaDefinitivo;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecRadicadoSalidaDefinitivo;
    @XmlElement(nillable = true)
    protected List<RadicadoTipo> radicacionesEntrada;
    @XmlElement(nillable = true)
    protected String valDiasProrroga;
    @XmlElement(nillable = true)
    protected String codTipoNotificacionDefinitiva;
    @XmlElement(nillable = true)
    protected String descTipoNotificacionDefinitiva;
    @XmlElement(type = String.class, nillable = true)
    @XmlJavaTypeAdapter(Adapter1 .class)
    @XmlSchemaType(name = "dateTime")
    protected Calendar fecNotificacionDefinitiva;
    @XmlElement(nillable = true)
    protected UbicacionPersonaTipo ubicacionDefinitiva;

    /**
     * Obtiene el valor de la propiedad idActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdActoAdministrativo() {
        return idActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad idActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdActoAdministrativo(String value) {
        this.idActoAdministrativo = value;
    }

    /**
     * Gets the value of the envioActoAdministrativo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the envioActoAdministrativo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEnvioActoAdministrativo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EnvioActoAdministrativoTipo }
     * 
     * 
     */
    public List<EnvioActoAdministrativoTipo> getEnvioActoAdministrativo() {
        if (envioActoAdministrativo == null) {
            envioActoAdministrativo = new ArrayList<EnvioActoAdministrativoTipo>();
        }
        return this.envioActoAdministrativo;
    }

    /**
     * Obtiene el valor de la propiedad idRadicadoSalidaDefinitivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRadicadoSalidaDefinitivo() {
        return idRadicadoSalidaDefinitivo;
    }

    /**
     * Define el valor de la propiedad idRadicadoSalidaDefinitivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRadicadoSalidaDefinitivo(String value) {
        this.idRadicadoSalidaDefinitivo = value;
    }

    /**
     * Obtiene el valor de la propiedad fecRadicadoSalidaDefinitivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecRadicadoSalidaDefinitivo() {
        return fecRadicadoSalidaDefinitivo;
    }

    /**
     * Define el valor de la propiedad fecRadicadoSalidaDefinitivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecRadicadoSalidaDefinitivo(Calendar value) {
        this.fecRadicadoSalidaDefinitivo = value;
    }

    /**
     * Gets the value of the radicacionesEntrada property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the radicacionesEntrada property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRadicacionesEntrada().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RadicadoTipo }
     * 
     * 
     */
    public List<RadicadoTipo> getRadicacionesEntrada() {
        if (radicacionesEntrada == null) {
            radicacionesEntrada = new ArrayList<RadicadoTipo>();
        }
        return this.radicacionesEntrada;
    }

    /**
     * Obtiene el valor de la propiedad valDiasProrroga.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValDiasProrroga() {
        return valDiasProrroga;
    }

    /**
     * Define el valor de la propiedad valDiasProrroga.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValDiasProrroga(String value) {
        this.valDiasProrroga = value;
    }

    /**
     * Obtiene el valor de la propiedad codTipoNotificacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTipoNotificacionDefinitiva() {
        return codTipoNotificacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad codTipoNotificacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTipoNotificacionDefinitiva(String value) {
        this.codTipoNotificacionDefinitiva = value;
    }

    /**
     * Obtiene el valor de la propiedad descTipoNotificacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoNotificacionDefinitiva() {
        return descTipoNotificacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad descTipoNotificacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoNotificacionDefinitiva(String value) {
        this.descTipoNotificacionDefinitiva = value;
    }

    /**
     * Obtiene el valor de la propiedad fecNotificacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public Calendar getFecNotificacionDefinitiva() {
        return fecNotificacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad fecNotificacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFecNotificacionDefinitiva(Calendar value) {
        this.fecNotificacionDefinitiva = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicacionDefinitiva.
     * 
     * @return
     *     possible object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public UbicacionPersonaTipo getUbicacionDefinitiva() {
        return ubicacionDefinitiva;
    }

    /**
     * Define el valor de la propiedad ubicacionDefinitiva.
     * 
     * @param value
     *     allowed object is
     *     {@link UbicacionPersonaTipo }
     *     
     */
    public void setUbicacionDefinitiva(UbicacionPersonaTipo value) {
        this.ubicacionDefinitiva = value;
    }

}
