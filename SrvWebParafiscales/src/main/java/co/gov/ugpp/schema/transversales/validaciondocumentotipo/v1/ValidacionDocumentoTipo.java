
package co.gov.ugpp.schema.transversales.validaciondocumentotipo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ValidacionDocumentoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionDocumentoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="idDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="esDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="desCausalDevolucion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionDocumentoTipo", propOrder = {
    "idDocumento",
    "esDevolucion",
    "codCausalDevolucion",
    "desCausalDevolucion"
})
public class ValidacionDocumentoTipo {

    @XmlElement(nillable = true)
    protected String idDocumento;
    @XmlElement(nillable = true)
    protected String esDevolucion;
    @XmlElement(nillable = true)
    protected String codCausalDevolucion;
    @XmlElement(nillable = true)
    protected String desCausalDevolucion;

    /**
     * Obtiene el valor de la propiedad idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Define el valor de la propiedad idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Obtiene el valor de la propiedad esDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsDevolucion() {
        return esDevolucion;
    }

    /**
     * Define el valor de la propiedad esDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsDevolucion(String value) {
        this.esDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad codCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodCausalDevolucion() {
        return codCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad codCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodCausalDevolucion(String value) {
        this.codCausalDevolucion = value;
    }

    /**
     * Obtiene el valor de la propiedad desCausalDevolucion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDesCausalDevolucion() {
        return desCausalDevolucion;
    }

    /**
     * Define el valor de la propiedad desCausalDevolucion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDesCausalDevolucion(String value) {
        this.desCausalDevolucion = value;
    }

}
