
package co.gov.ugpp.schema.trasnversales.validacionarchivotipo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ParametroValoresTipo;


/**
 * <p>Clase Java para ValidacionArchivoTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ValidacionArchivoTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="estructuraDocumentos" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="estrcuturaDocumentosMigracion" type="{http://www.ugpp.gov.co/esb/schema/ParametroValoresTipo/v1}ParametroValoresTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ValidacionArchivoTipo", propOrder = {
    "estructuraDocumentos",
    "estrcuturaDocumentosMigracion"
})
public class ValidacionArchivoTipo {

    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> estructuraDocumentos;
    @XmlElement(nillable = true)
    protected List<ParametroValoresTipo> estrcuturaDocumentosMigracion;

    /**
     * Gets the value of the estructuraDocumentos property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estructuraDocumentos property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstructuraDocumentos().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getEstructuraDocumentos() {
        if (estructuraDocumentos == null) {
            estructuraDocumentos = new ArrayList<ParametroValoresTipo>();
        }
        return this.estructuraDocumentos;
    }

    /**
     * Gets the value of the estrcuturaDocumentosMigracion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the estrcuturaDocumentosMigracion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEstrcuturaDocumentosMigracion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametroValoresTipo }
     * 
     * 
     */
    public List<ParametroValoresTipo> getEstrcuturaDocumentosMigracion() {
        if (estrcuturaDocumentosMigracion == null) {
            estrcuturaDocumentosMigracion = new ArrayList<ParametroValoresTipo>();
        }
        return this.estrcuturaDocumentosMigracion;
    }

}
