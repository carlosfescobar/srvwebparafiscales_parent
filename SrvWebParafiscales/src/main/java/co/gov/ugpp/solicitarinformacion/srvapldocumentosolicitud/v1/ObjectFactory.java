
package co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearDocumentoSolicitudsSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpCrearDocumentoSolicitudsSol");
    private final static QName _OpCrearDocumentoSolicitudResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpCrearDocumentoSolicitudResp");
    private final static QName _OpBuscarPorCriteriosDocumentoSolicitudFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpBuscarPorCriteriosDocumentoSolicitudFallo");
    private final static QName _OpCrearDocumentoSolicitudFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpCrearDocumentoSolicitudFallo");
    private final static QName _OpBuscarPorCriteriosDocumentoSolicitudResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpBuscarPorCriteriosDocumentoSolicitudResp");
    private final static QName _OpBuscarPorCriteriosDocumentoSolicitudSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", "OpBuscarPorCriteriosDocumentoSolicitudSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearDocumentoSolicitudSolTipo }
     * 
     */
    public OpCrearDocumentoSolicitudSolTipo createOpCrearDocumentoSolicitudSolTipo() {
        return new OpCrearDocumentoSolicitudSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearDocumentoSolicitudRespTipo }
     * 
     */
    public OpCrearDocumentoSolicitudRespTipo createOpCrearDocumentoSolicitudRespTipo() {
        return new OpCrearDocumentoSolicitudRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDocumentoSolicitudRespTipo }
     * 
     */
    public OpBuscarPorCriteriosDocumentoSolicitudRespTipo createOpBuscarPorCriteriosDocumentoSolicitudRespTipo() {
        return new OpBuscarPorCriteriosDocumentoSolicitudRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDocumentoSolicitudSolTipo }
     * 
     */
    public OpBuscarPorCriteriosDocumentoSolicitudSolTipo createOpBuscarPorCriteriosDocumentoSolicitudSolTipo() {
        return new OpBuscarPorCriteriosDocumentoSolicitudSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoSolicitudSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpCrearDocumentoSolicitudsSol")
    public JAXBElement<OpCrearDocumentoSolicitudSolTipo> createOpCrearDocumentoSolicitudsSol(OpCrearDocumentoSolicitudSolTipo value) {
        return new JAXBElement<OpCrearDocumentoSolicitudSolTipo>(_OpCrearDocumentoSolicitudsSol_QNAME, OpCrearDocumentoSolicitudSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoSolicitudRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpCrearDocumentoSolicitudResp")
    public JAXBElement<OpCrearDocumentoSolicitudRespTipo> createOpCrearDocumentoSolicitudResp(OpCrearDocumentoSolicitudRespTipo value) {
        return new JAXBElement<OpCrearDocumentoSolicitudRespTipo>(_OpCrearDocumentoSolicitudResp_QNAME, OpCrearDocumentoSolicitudRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpBuscarPorCriteriosDocumentoSolicitudFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosDocumentoSolicitudFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosDocumentoSolicitudFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpCrearDocumentoSolicitudFallo")
    public JAXBElement<FalloTipo> createOpCrearDocumentoSolicitudFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearDocumentoSolicitudFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDocumentoSolicitudRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpBuscarPorCriteriosDocumentoSolicitudResp")
    public JAXBElement<OpBuscarPorCriteriosDocumentoSolicitudRespTipo> createOpBuscarPorCriteriosDocumentoSolicitudResp(OpBuscarPorCriteriosDocumentoSolicitudRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDocumentoSolicitudRespTipo>(_OpBuscarPorCriteriosDocumentoSolicitudResp_QNAME, OpBuscarPorCriteriosDocumentoSolicitudRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDocumentoSolicitudSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplDocumentoSolicitud/v1", name = "OpBuscarPorCriteriosDocumentoSolicitudSol")
    public JAXBElement<OpBuscarPorCriteriosDocumentoSolicitudSolTipo> createOpBuscarPorCriteriosDocumentoSolicitudSol(OpBuscarPorCriteriosDocumentoSolicitudSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDocumentoSolicitudSolTipo>(_OpBuscarPorCriteriosDocumentoSolicitudSol_QNAME, OpBuscarPorCriteriosDocumentoSolicitudSolTipo.class, null, value);
    }

}
