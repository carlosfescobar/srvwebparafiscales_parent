
package co.gov.ugpp.solicitarinformacion.srvapldocumentosolicitud.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.solicitudinformacion.agrupaciondocumentosolicitudtipo.v1.AgrupacionDocumentoSolicitudTipo;


/**
 * <p>Clase Java para OpCrearDocumentoSolicitudSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearDocumentoSolicitudSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="agrupacionDocumentoSolicitud" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/AgrupacionDocumentoSolicitudTipo/v1}AgrupacionDocumentoSolicitudTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearDocumentoSolicitudSolTipo", propOrder = {
    "contextoTransaccional",
    "agrupacionDocumentoSolicitud"
})
public class OpCrearDocumentoSolicitudSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AgrupacionDocumentoSolicitudTipo agrupacionDocumentoSolicitud;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad agrupacionDocumentoSolicitud.
     * 
     * @return
     *     possible object is
     *     {@link AgrupacionDocumentoSolicitudTipo }
     *     
     */
    public AgrupacionDocumentoSolicitudTipo getAgrupacionDocumentoSolicitud() {
        return agrupacionDocumentoSolicitud;
    }

    /**
     * Define el valor de la propiedad agrupacionDocumentoSolicitud.
     * 
     * @param value
     *     allowed object is
     *     {@link AgrupacionDocumentoSolicitudTipo }
     *     
     */
    public void setAgrupacionDocumentoSolicitud(AgrupacionDocumentoSolicitudTipo value) {
        this.agrupacionDocumentoSolicitud = value;
    }

}
