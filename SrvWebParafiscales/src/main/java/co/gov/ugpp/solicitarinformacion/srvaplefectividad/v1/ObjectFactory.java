
package co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarEfectividadEntidadExternaResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", "OpConsultarEfectividadEntidadExternaResp");
    private final static QName _OpConsultarEfectividadEntidadExternaSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", "OpConsultarEfectividadEntidadExternaSol");
    private final static QName _OpConsultarEfectividadEntidadExternaFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", "OpConsultarEfectividadEntidadExternaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarEfectividadEntidadExternaRespTipo }
     * 
     */
    public OpConsultarEfectividadEntidadExternaRespTipo createOpConsultarEfectividadEntidadExternaRespTipo() {
        return new OpConsultarEfectividadEntidadExternaRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarEfectividadEntidadExternaSolTipo }
     * 
     */
    public OpConsultarEfectividadEntidadExternaSolTipo createOpConsultarEfectividadEntidadExternaSolTipo() {
        return new OpConsultarEfectividadEntidadExternaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarEfectividadEntidadExternaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", name = "OpConsultarEfectividadEntidadExternaResp")
    public JAXBElement<OpConsultarEfectividadEntidadExternaRespTipo> createOpConsultarEfectividadEntidadExternaResp(OpConsultarEfectividadEntidadExternaRespTipo value) {
        return new JAXBElement<OpConsultarEfectividadEntidadExternaRespTipo>(_OpConsultarEfectividadEntidadExternaResp_QNAME, OpConsultarEfectividadEntidadExternaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarEfectividadEntidadExternaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", name = "OpConsultarEfectividadEntidadExternaSol")
    public JAXBElement<OpConsultarEfectividadEntidadExternaSolTipo> createOpConsultarEfectividadEntidadExternaSol(OpConsultarEfectividadEntidadExternaSolTipo value) {
        return new JAXBElement<OpConsultarEfectividadEntidadExternaSolTipo>(_OpConsultarEfectividadEntidadExternaSol_QNAME, OpConsultarEfectividadEntidadExternaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplEfectividad/v1", name = "OpConsultarEfectividadEntidadExternaFallo")
    public JAXBElement<FalloTipo> createOpConsultarEfectividadEntidadExternaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarEfectividadEntidadExternaFallo_QNAME, FalloTipo.class, null, value);
    }

}
