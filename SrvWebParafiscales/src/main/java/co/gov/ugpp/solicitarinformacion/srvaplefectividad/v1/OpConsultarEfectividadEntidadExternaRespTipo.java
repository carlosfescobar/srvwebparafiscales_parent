
package co.gov.ugpp.solicitarinformacion.srvaplefectividad.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.solicitarinformacion.efectividadtipo.v1.EfectividadTipo;


/**
 * <p>Clase Java para OpConsultarEfectividadEntidadExternaRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpConsultarEfectividadEntidadExternaRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="efectividad" type="{http://www.ugpp.gov.co/schema/SolicitarInformacion/EfectividadTipo/v1}EfectividadTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpConsultarEfectividadEntidadExternaRespTipo", propOrder = {
    "contextoRespuesta",
    "efectividad"
})
public class OpConsultarEfectividadEntidadExternaRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected EfectividadTipo efectividad;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad efectividad.
     * 
     * @return
     *     possible object is
     *     {@link EfectividadTipo }
     *     
     */
    public EfectividadTipo getEfectividad() {
        return efectividad;
    }

    /**
     * Define el valor de la propiedad efectividad.
     * 
     * @param value
     *     allowed object is
     *     {@link EfectividadTipo }
     *     
     */
    public void setEfectividad(EfectividadTipo value) {
        this.efectividad = value;
    }

}
