
package co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarSolicitudKPISol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", "OpConsultarSolicitudKPISol");
    private final static QName _OpConsultarSolicitudKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", "OpConsultarSolicitudKPIFallo");
    private final static QName _OpConsultarSolicitudKPIResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", "OpConsultarSolicitudKPIResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarSolicitudKPIRespTipo }
     * 
     */
    public OpConsultarSolicitudKPIRespTipo createOpConsultarSolicitudKPIRespTipo() {
        return new OpConsultarSolicitudKPIRespTipo();
    }

    /**
     * Create an instance of {@link OpConsultarSolicitudKPISolTipo }
     * 
     */
    public OpConsultarSolicitudKPISolTipo createOpConsultarSolicitudKPISolTipo() {
        return new OpConsultarSolicitudKPISolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarSolicitudKPISolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", name = "OpConsultarSolicitudKPISol")
    public JAXBElement<OpConsultarSolicitudKPISolTipo> createOpConsultarSolicitudKPISol(OpConsultarSolicitudKPISolTipo value) {
        return new JAXBElement<OpConsultarSolicitudKPISolTipo>(_OpConsultarSolicitudKPISol_QNAME, OpConsultarSolicitudKPISolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", name = "OpConsultarSolicitudKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarSolicitudKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarSolicitudKPIFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarSolicitudKPIRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplKPISolicitudInformacion/v1", name = "OpConsultarSolicitudKPIResp")
    public JAXBElement<OpConsultarSolicitudKPIRespTipo> createOpConsultarSolicitudKPIResp(OpConsultarSolicitudKPIRespTipo value) {
        return new JAXBElement<OpConsultarSolicitudKPIRespTipo>(_OpConsultarSolicitudKPIResp_QNAME, OpConsultarSolicitudKPIRespTipo.class, null, value);
    }

}
