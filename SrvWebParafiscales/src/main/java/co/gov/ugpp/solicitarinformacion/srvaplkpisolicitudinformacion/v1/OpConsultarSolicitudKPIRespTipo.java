
package co.gov.ugpp.solicitarinformacion.srvaplkpisolicitudinformacion.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;


/**
 * <p>Clase Java para OpConsultarSolicitudKPIRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpConsultarSolicitudKPIRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="cantidadSolicitudes" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="solicitudInformacion" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/SolicitudInformacionTipo/v1}SolicitudInformacionTipo" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpConsultarSolicitudKPIRespTipo", propOrder = {
    "contextoRespuesta",
    "cantidadSolicitudes",
    "solicitudInformacion"
})
public class OpConsultarSolicitudKPIRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected String cantidadSolicitudes;
    protected List<SolicitudInformacionTipo> solicitudInformacion;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidadSolicitudes.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCantidadSolicitudes() {
        return cantidadSolicitudes;
    }

    /**
     * Define el valor de la propiedad cantidadSolicitudes.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCantidadSolicitudes(String value) {
        this.cantidadSolicitudes = value;
    }

    /**
     * Gets the value of the solicitudInformacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the solicitudInformacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSolicitudInformacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SolicitudInformacionTipo }
     * 
     * 
     */
    public List<SolicitudInformacionTipo> getSolicitudInformacion() {
        if (solicitudInformacion == null) {
            solicitudInformacion = new ArrayList<SolicitudInformacionTipo>();
        }
        return this.solicitudInformacion;
    }

}
