
package co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarNoAportanteResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpActualizarNoAportanteResp");
    private final static QName _OpActualizarNoAportanteFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpActualizarNoAportanteFallo");
    private final static QName _OpBuscarPorIdNoAportanteSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpBuscarPorIdNoAportanteSol");
    private final static QName _OpActualizarNoAportanteSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpActualizarNoAportanteSol");
    private final static QName _OpBuscarPorIdNoAportanteFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpBuscarPorIdNoAportanteFallo");
    private final static QName _OpBuscarPorIdNoAportanteResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", "OpBuscarPorIdNoAportanteResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarNoAportanteSolTipo }
     * 
     */
    public OpActualizarNoAportanteSolTipo createOpActualizarNoAportanteSolTipo() {
        return new OpActualizarNoAportanteSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdNoAportanteSolTipo }
     * 
     */
    public OpBuscarPorIdNoAportanteSolTipo createOpBuscarPorIdNoAportanteSolTipo() {
        return new OpBuscarPorIdNoAportanteSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarNoAportanteRespTipo }
     * 
     */
    public OpActualizarNoAportanteRespTipo createOpActualizarNoAportanteRespTipo() {
        return new OpActualizarNoAportanteRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdNoAportanteRespTipo }
     * 
     */
    public OpBuscarPorIdNoAportanteRespTipo createOpBuscarPorIdNoAportanteRespTipo() {
        return new OpBuscarPorIdNoAportanteRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarNoAportanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpActualizarNoAportanteResp")
    public JAXBElement<OpActualizarNoAportanteRespTipo> createOpActualizarNoAportanteResp(OpActualizarNoAportanteRespTipo value) {
        return new JAXBElement<OpActualizarNoAportanteRespTipo>(_OpActualizarNoAportanteResp_QNAME, OpActualizarNoAportanteRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpActualizarNoAportanteFallo")
    public JAXBElement<FalloTipo> createOpActualizarNoAportanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarNoAportanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdNoAportanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpBuscarPorIdNoAportanteSol")
    public JAXBElement<OpBuscarPorIdNoAportanteSolTipo> createOpBuscarPorIdNoAportanteSol(OpBuscarPorIdNoAportanteSolTipo value) {
        return new JAXBElement<OpBuscarPorIdNoAportanteSolTipo>(_OpBuscarPorIdNoAportanteSol_QNAME, OpBuscarPorIdNoAportanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarNoAportanteSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpActualizarNoAportanteSol")
    public JAXBElement<OpActualizarNoAportanteSolTipo> createOpActualizarNoAportanteSol(OpActualizarNoAportanteSolTipo value) {
        return new JAXBElement<OpActualizarNoAportanteSolTipo>(_OpActualizarNoAportanteSol_QNAME, OpActualizarNoAportanteSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpBuscarPorIdNoAportanteFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdNoAportanteFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdNoAportanteFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdNoAportanteRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplNoAportante/v1", name = "OpBuscarPorIdNoAportanteResp")
    public JAXBElement<OpBuscarPorIdNoAportanteRespTipo> createOpBuscarPorIdNoAportanteResp(OpBuscarPorIdNoAportanteRespTipo value) {
        return new JAXBElement<OpBuscarPorIdNoAportanteRespTipo>(_OpBuscarPorIdNoAportanteResp_QNAME, OpBuscarPorIdNoAportanteRespTipo.class, null, value);
    }

}
