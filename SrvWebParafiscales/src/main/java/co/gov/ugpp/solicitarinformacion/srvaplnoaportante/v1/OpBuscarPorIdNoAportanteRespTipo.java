
package co.gov.ugpp.solicitarinformacion.srvaplnoaportante.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.solicitudinformacion.noaportantetipo.v1.NoAportanteTipo;


/**
 * <p>Clase Java para OpBuscarPorIdNoAportanteRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdNoAportanteRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="noAportante" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/NoAportanteTipo/v1}NoAportanteTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdNoAportanteRespTipo", propOrder = {
    "contextoRespuesta",
    "noAportante"
})
public class OpBuscarPorIdNoAportanteRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected NoAportanteTipo noAportante;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad noAportante.
     * 
     * @return
     *     possible object is
     *     {@link NoAportanteTipo }
     *     
     */
    public NoAportanteTipo getNoAportante() {
        return noAportante;
    }

    /**
     * Define el valor de la propiedad noAportante.
     * 
     * @param value
     *     allowed object is
     *     {@link NoAportanteTipo }
     *     
     */
    public void setNoAportante(NoAportanteTipo value) {
        this.noAportante = value;
    }

}
