
package co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpAlmacenarRespuestaInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpAlmacenarRespuestaInformacionFallo");
    private final static QName _OpValidarRespuestaInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpValidarRespuestaInformacionSol");
    private final static QName _OpValidarRespuestaInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpValidarRespuestaInformacionFallo");
    private final static QName _OpAlmacenarRespuestaInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpAlmacenarRespuestaInformacionResp");
    private final static QName _OpAlmacenarRespuestaInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpAlmacenarRespuestaInformacionSol");
    private final static QName _OpCruzarRespuestaInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpCruzarRespuestaInformacionSol");
    private final static QName _OpCruzarRespuestaInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpCruzarRespuestaInformacionFallo");
    private final static QName _OpCruzarRespuestaInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpCruzarRespuestaInformacionResp");
    private final static QName _OpValidarRespuestaInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "OpValidarRespuestaInformacionResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCruzarRespuestaInformacionRespTipo }
     * 
     */
    public OpCruzarRespuestaInformacionRespTipo createOpCruzarRespuestaInformacionRespTipo() {
        return new OpCruzarRespuestaInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCruzarRespuestaInformacionSolTipo }
     * 
     */
    public OpCruzarRespuestaInformacionSolTipo createOpCruzarRespuestaInformacionSolTipo() {
        return new OpCruzarRespuestaInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpAlmacenarRespuestaInformacionSolTipo }
     * 
     */
    public OpAlmacenarRespuestaInformacionSolTipo createOpAlmacenarRespuestaInformacionSolTipo() {
        return new OpAlmacenarRespuestaInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpValidarRespuestaInformacionRespTipo }
     * 
     */
    public OpValidarRespuestaInformacionRespTipo createOpValidarRespuestaInformacionRespTipo() {
        return new OpValidarRespuestaInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpValidarRespuestaInformacionSolTipo }
     * 
     */
    public OpValidarRespuestaInformacionSolTipo createOpValidarRespuestaInformacionSolTipo() {
        return new OpValidarRespuestaInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpAlmacenarRespuestaInformacionRespTipo }
     * 
     */
    public OpAlmacenarRespuestaInformacionRespTipo createOpAlmacenarRespuestaInformacionRespTipo() {
        return new OpAlmacenarRespuestaInformacionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpAlmacenarRespuestaInformacionFallo")
    public JAXBElement<FalloTipo> createOpAlmacenarRespuestaInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpAlmacenarRespuestaInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarRespuestaInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpValidarRespuestaInformacionSol")
    public JAXBElement<OpValidarRespuestaInformacionSolTipo> createOpValidarRespuestaInformacionSol(OpValidarRespuestaInformacionSolTipo value) {
        return new JAXBElement<OpValidarRespuestaInformacionSolTipo>(_OpValidarRespuestaInformacionSol_QNAME, OpValidarRespuestaInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpValidarRespuestaInformacionFallo")
    public JAXBElement<FalloTipo> createOpValidarRespuestaInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpValidarRespuestaInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAlmacenarRespuestaInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpAlmacenarRespuestaInformacionResp")
    public JAXBElement<OpAlmacenarRespuestaInformacionRespTipo> createOpAlmacenarRespuestaInformacionResp(OpAlmacenarRespuestaInformacionRespTipo value) {
        return new JAXBElement<OpAlmacenarRespuestaInformacionRespTipo>(_OpAlmacenarRespuestaInformacionResp_QNAME, OpAlmacenarRespuestaInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAlmacenarRespuestaInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpAlmacenarRespuestaInformacionSol")
    public JAXBElement<OpAlmacenarRespuestaInformacionSolTipo> createOpAlmacenarRespuestaInformacionSol(OpAlmacenarRespuestaInformacionSolTipo value) {
        return new JAXBElement<OpAlmacenarRespuestaInformacionSolTipo>(_OpAlmacenarRespuestaInformacionSol_QNAME, OpAlmacenarRespuestaInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarRespuestaInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpCruzarRespuestaInformacionSol")
    public JAXBElement<OpCruzarRespuestaInformacionSolTipo> createOpCruzarRespuestaInformacionSol(OpCruzarRespuestaInformacionSolTipo value) {
        return new JAXBElement<OpCruzarRespuestaInformacionSolTipo>(_OpCruzarRespuestaInformacionSol_QNAME, OpCruzarRespuestaInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpCruzarRespuestaInformacionFallo")
    public JAXBElement<FalloTipo> createOpCruzarRespuestaInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCruzarRespuestaInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarRespuestaInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpCruzarRespuestaInformacionResp")
    public JAXBElement<OpCruzarRespuestaInformacionRespTipo> createOpCruzarRespuestaInformacionResp(OpCruzarRespuestaInformacionRespTipo value) {
        return new JAXBElement<OpCruzarRespuestaInformacionRespTipo>(_OpCruzarRespuestaInformacionResp_QNAME, OpCruzarRespuestaInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarRespuestaInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", name = "OpValidarRespuestaInformacionResp")
    public JAXBElement<OpValidarRespuestaInformacionRespTipo> createOpValidarRespuestaInformacionResp(OpValidarRespuestaInformacionRespTipo value) {
        return new JAXBElement<OpValidarRespuestaInformacionRespTipo>(_OpValidarRespuestaInformacionResp_QNAME, OpValidarRespuestaInformacionRespTipo.class, null, value);
    }

}
