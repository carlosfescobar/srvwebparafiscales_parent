
package co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;
import co.gov.ugpp.schema.solicitudinformacion.radicaciontipo.v1.RadicacionTipo;


/**
 * <p>Clase Java para OpCruzarRespuestaInformacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCruzarRespuestaInformacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="radicacion" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/RadicacionTipo/v1}RadicacionTipo" minOccurs="0"/>
 *         &lt;element name="formatoEstructura" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoEstructuraTipo/v1}FormatoEstructuraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCruzarRespuestaInformacionSolTipo", propOrder = {
    "contextoTransaccional",
    "radicacion",
    "formatoEstructura"
})
public class OpCruzarRespuestaInformacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(nillable = true)
    protected RadicacionTipo radicacion;
    @XmlElement(required = true)
    protected FormatoEstructuraTipo formatoEstructura;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad radicacion.
     * 
     * @return
     *     possible object is
     *     {@link RadicacionTipo }
     *     
     */
    public RadicacionTipo getRadicacion() {
        return radicacion;
    }

    /**
     * Define el valor de la propiedad radicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link RadicacionTipo }
     *     
     */
    public void setRadicacion(RadicacionTipo value) {
        this.radicacion = value;
    }

    /**
     * Obtiene el valor de la propiedad formatoEstructura.
     * 
     * @return
     *     possible object is
     *     {@link FormatoEstructuraTipo }
     *     
     */
    public FormatoEstructuraTipo getFormatoEstructura() {
        return formatoEstructura;
    }

    /**
     * Define el valor de la propiedad formatoEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoEstructuraTipo }
     *     
     */
    public void setFormatoEstructura(FormatoEstructuraTipo value) {
        this.formatoEstructura = value;
    }

}
