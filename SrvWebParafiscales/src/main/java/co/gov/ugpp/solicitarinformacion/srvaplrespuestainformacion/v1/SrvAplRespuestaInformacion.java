
package co.gov.ugpp.solicitarinformacion.srvaplrespuestainformacion.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplRespuestaInformacion", targetNamespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplRespuestaInformacion?wsdl")
public class SrvAplRespuestaInformacion
    extends Service
{

    private final static URL SRVAPLRESPUESTAINFORMACION_WSDL_LOCATION;
    private final static WebServiceException SRVAPLRESPUESTAINFORMACION_EXCEPTION;
    private final static QName SRVAPLRESPUESTAINFORMACION_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "SrvAplRespuestaInformacion");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplRespuestaInformacion?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLRESPUESTAINFORMACION_WSDL_LOCATION = url;
        SRVAPLRESPUESTAINFORMACION_EXCEPTION = e;
    }

    public SrvAplRespuestaInformacion() {
        super(__getWsdlLocation(), SRVAPLRESPUESTAINFORMACION_QNAME);
    }

    public SrvAplRespuestaInformacion(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLRESPUESTAINFORMACION_QNAME, features);
    }

    public SrvAplRespuestaInformacion(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLRESPUESTAINFORMACION_QNAME);
    }

    public SrvAplRespuestaInformacion(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLRESPUESTAINFORMACION_QNAME, features);
    }

    public SrvAplRespuestaInformacion(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplRespuestaInformacion(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplRespuestaInformacionSOAP
     */
    @WebEndpoint(name = "portSrvAplRespuestaInformacionSOAP")
    public PortSrvAplRespuestaInformacionSOAP getPortSrvAplRespuestaInformacionSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "portSrvAplRespuestaInformacionSOAP"), PortSrvAplRespuestaInformacionSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplRespuestaInformacionSOAP
     */
    @WebEndpoint(name = "portSrvAplRespuestaInformacionSOAP")
    public PortSrvAplRespuestaInformacionSOAP getPortSrvAplRespuestaInformacionSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplRespuestaInformacion/v1", "portSrvAplRespuestaInformacionSOAP"), PortSrvAplRespuestaInformacionSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLRESPUESTAINFORMACION_EXCEPTION!= null) {
            throw SRVAPLRESPUESTAINFORMACION_EXCEPTION;
        }
        return SRVAPLRESPUESTAINFORMACION_WSDL_LOCATION;
    }

}
