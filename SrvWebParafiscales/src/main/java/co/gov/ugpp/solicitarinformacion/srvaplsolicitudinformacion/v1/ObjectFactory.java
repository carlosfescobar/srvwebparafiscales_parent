
package co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarSolicitudInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpActualizarSolicitudInformacionResp");
    private final static QName _OpBuscarPorIdSolicitudInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorIdSolicitudInformacionSol");
    private final static QName _OpCrearFormatoSolicitudInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearFormatoSolicitudInformacionFallo");
    private final static QName _OpBuscarPorCriteriosSolicitudInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorCriteriosSolicitudInformacionResp");
    private final static QName _OpBuscarPorIdSolicitudInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorIdSolicitudInformacionFallo");
    private final static QName _OpCrearSolicitudInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearSolicitudInformacionResp");
    private final static QName _OpActualizarSolicitudInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpActualizarSolicitudInformacionSol");
    private final static QName _OpActualizarSolicitudInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpActualizarSolicitudInformacionFallo");
    private final static QName _OpBuscarPorCriteriosSolicitudInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorCriteriosSolicitudInformacionFallo");
    private final static QName _OpCrearSolicitudInformacionFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearSolicitudInformacionFallo");
    private final static QName _OpBuscarPorCriteriosSolicitudInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorCriteriosSolicitudInformacionSol");
    private final static QName _OpBuscarPorIdSolicitudInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpBuscarPorIdSolicitudInformacionResp");
    private final static QName _OpCrearSolicitudInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearSolicitudInformacionSol");
    private final static QName _OpCrearFormatoSolicitudInformacionResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearFormatoSolicitudInformacionResp");
    private final static QName _OpCrearFormatoSolicitudInformacionSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", "OpCrearFormatoSolicitudInformacionSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudInformacionRespTipo }
     * 
     */
    public OpBuscarPorIdSolicitudInformacionRespTipo createOpBuscarPorIdSolicitudInformacionRespTipo() {
        return new OpBuscarPorIdSolicitudInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudInformacionSolTipo }
     * 
     */
    public OpCrearSolicitudInformacionSolTipo createOpCrearSolicitudInformacionSolTipo() {
        return new OpCrearSolicitudInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudInformacionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudInformacionSolTipo createOpBuscarPorCriteriosSolicitudInformacionSolTipo() {
        return new OpBuscarPorCriteriosSolicitudInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearFormatoSolicitudInformacionSolTipo }
     * 
     */
    public OpCrearFormatoSolicitudInformacionSolTipo createOpCrearFormatoSolicitudInformacionSolTipo() {
        return new OpCrearFormatoSolicitudInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearFormatoSolicitudInformacionRespTipo }
     * 
     */
    public OpCrearFormatoSolicitudInformacionRespTipo createOpCrearFormatoSolicitudInformacionRespTipo() {
        return new OpCrearFormatoSolicitudInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudInformacionRespTipo }
     * 
     */
    public OpCrearSolicitudInformacionRespTipo createOpCrearSolicitudInformacionRespTipo() {
        return new OpCrearSolicitudInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudInformacionSolTipo }
     * 
     */
    public OpActualizarSolicitudInformacionSolTipo createOpActualizarSolicitudInformacionSolTipo() {
        return new OpActualizarSolicitudInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosSolicitudInformacionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosSolicitudInformacionRespTipo createOpBuscarPorCriteriosSolicitudInformacionRespTipo() {
        return new OpBuscarPorCriteriosSolicitudInformacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudInformacionSolTipo }
     * 
     */
    public OpBuscarPorIdSolicitudInformacionSolTipo createOpBuscarPorIdSolicitudInformacionSolTipo() {
        return new OpBuscarPorIdSolicitudInformacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudInformacionRespTipo }
     * 
     */
    public OpActualizarSolicitudInformacionRespTipo createOpActualizarSolicitudInformacionRespTipo() {
        return new OpActualizarSolicitudInformacionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpActualizarSolicitudInformacionResp")
    public JAXBElement<OpActualizarSolicitudInformacionRespTipo> createOpActualizarSolicitudInformacionResp(OpActualizarSolicitudInformacionRespTipo value) {
        return new JAXBElement<OpActualizarSolicitudInformacionRespTipo>(_OpActualizarSolicitudInformacionResp_QNAME, OpActualizarSolicitudInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorIdSolicitudInformacionSol")
    public JAXBElement<OpBuscarPorIdSolicitudInformacionSolTipo> createOpBuscarPorIdSolicitudInformacionSol(OpBuscarPorIdSolicitudInformacionSolTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudInformacionSolTipo>(_OpBuscarPorIdSolicitudInformacionSol_QNAME, OpBuscarPorIdSolicitudInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearFormatoSolicitudInformacionFallo")
    public JAXBElement<FalloTipo> createOpCrearFormatoSolicitudInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFormatoSolicitudInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorCriteriosSolicitudInformacionResp")
    public JAXBElement<OpBuscarPorCriteriosSolicitudInformacionRespTipo> createOpBuscarPorCriteriosSolicitudInformacionResp(OpBuscarPorCriteriosSolicitudInformacionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudInformacionRespTipo>(_OpBuscarPorCriteriosSolicitudInformacionResp_QNAME, OpBuscarPorCriteriosSolicitudInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorIdSolicitudInformacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdSolicitudInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdSolicitudInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearSolicitudInformacionResp")
    public JAXBElement<OpCrearSolicitudInformacionRespTipo> createOpCrearSolicitudInformacionResp(OpCrearSolicitudInformacionRespTipo value) {
        return new JAXBElement<OpCrearSolicitudInformacionRespTipo>(_OpCrearSolicitudInformacionResp_QNAME, OpCrearSolicitudInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpActualizarSolicitudInformacionSol")
    public JAXBElement<OpActualizarSolicitudInformacionSolTipo> createOpActualizarSolicitudInformacionSol(OpActualizarSolicitudInformacionSolTipo value) {
        return new JAXBElement<OpActualizarSolicitudInformacionSolTipo>(_OpActualizarSolicitudInformacionSol_QNAME, OpActualizarSolicitudInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpActualizarSolicitudInformacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarSolicitudInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSolicitudInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorCriteriosSolicitudInformacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosSolicitudInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosSolicitudInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearSolicitudInformacionFallo")
    public JAXBElement<FalloTipo> createOpCrearSolicitudInformacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSolicitudInformacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosSolicitudInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorCriteriosSolicitudInformacionSol")
    public JAXBElement<OpBuscarPorCriteriosSolicitudInformacionSolTipo> createOpBuscarPorCriteriosSolicitudInformacionSol(OpBuscarPorCriteriosSolicitudInformacionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosSolicitudInformacionSolTipo>(_OpBuscarPorCriteriosSolicitudInformacionSol_QNAME, OpBuscarPorCriteriosSolicitudInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpBuscarPorIdSolicitudInformacionResp")
    public JAXBElement<OpBuscarPorIdSolicitudInformacionRespTipo> createOpBuscarPorIdSolicitudInformacionResp(OpBuscarPorIdSolicitudInformacionRespTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudInformacionRespTipo>(_OpBuscarPorIdSolicitudInformacionResp_QNAME, OpBuscarPorIdSolicitudInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearSolicitudInformacionSol")
    public JAXBElement<OpCrearSolicitudInformacionSolTipo> createOpCrearSolicitudInformacionSol(OpCrearSolicitudInformacionSolTipo value) {
        return new JAXBElement<OpCrearSolicitudInformacionSolTipo>(_OpCrearSolicitudInformacionSol_QNAME, OpCrearSolicitudInformacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoSolicitudInformacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearFormatoSolicitudInformacionResp")
    public JAXBElement<OpCrearFormatoSolicitudInformacionRespTipo> createOpCrearFormatoSolicitudInformacionResp(OpCrearFormatoSolicitudInformacionRespTipo value) {
        return new JAXBElement<OpCrearFormatoSolicitudInformacionRespTipo>(_OpCrearFormatoSolicitudInformacionResp_QNAME, OpCrearFormatoSolicitudInformacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoSolicitudInformacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacion/v1", name = "OpCrearFormatoSolicitudInformacionSol")
    public JAXBElement<OpCrearFormatoSolicitudInformacionSolTipo> createOpCrearFormatoSolicitudInformacionSol(OpCrearFormatoSolicitudInformacionSolTipo value) {
        return new JAXBElement<OpCrearFormatoSolicitudInformacionSolTipo>(_OpCrearFormatoSolicitudInformacionSol_QNAME, OpCrearFormatoSolicitudInformacionSolTipo.class, null, value);
    }

}
