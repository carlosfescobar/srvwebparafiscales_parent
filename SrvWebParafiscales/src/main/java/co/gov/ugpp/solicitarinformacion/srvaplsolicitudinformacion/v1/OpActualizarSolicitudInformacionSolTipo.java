
package co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.solicitudinformacion.solicitudinformaciontipo.v1.SolicitudInformacionTipo;


/**
 * <p>Clase Java para OpActualizarSolicitudInformacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarSolicitudInformacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="solicitudInformacion" type="{http://www.ugpp.gov.co/schema/SolicitudInformacion/SolicitudInformacionTipo/v1}SolicitudInformacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarSolicitudInformacionSolTipo", propOrder = {
    "contextoTransaccional",
    "solicitudInformacion"
})
public class OpActualizarSolicitudInformacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected SolicitudInformacionTipo solicitudInformacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad solicitudInformacion.
     * 
     * @return
     *     possible object is
     *     {@link SolicitudInformacionTipo }
     *     
     */
    public SolicitudInformacionTipo getSolicitudInformacion() {
        return solicitudInformacion;
    }

    /**
     * Define el valor de la propiedad solicitudInformacion.
     * 
     * @param value
     *     allowed object is
     *     {@link SolicitudInformacionTipo }
     *     
     */
    public void setSolicitudInformacion(SolicitudInformacionTipo value) {
        this.solicitudInformacion = value;
    }

}
