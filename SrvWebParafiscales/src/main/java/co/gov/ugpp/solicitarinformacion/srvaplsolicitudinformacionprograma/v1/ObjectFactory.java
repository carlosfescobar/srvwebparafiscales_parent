
package co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdSolicitudInformacionProgramaResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpBuscarPorIdSolicitudInformacionProgramaResp");
    private final static QName _OpActualizarSolicitudInformacionProgramaFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpActualizarSolicitudInformacionProgramaFallo");
    private final static QName _OpBuscarPorIdSolicitudInformacionProgramaSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpBuscarPorIdSolicitudInformacionProgramaSol");
    private final static QName _OpCrearSolicitudInformacionProgramaResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearSolicitudInformacionProgramaResp");
    private final static QName _OpCrearSolicitudInformacionProgramaFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearSolicitudInformacionProgramaFallo");
    private final static QName _OpCrearFormatoSolicitudProgramaFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearFormatoSolicitudProgramaFallo");
    private final static QName _OpCrearFormatoSolicitudProgramaSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearFormatoSolicitudProgramaSol");
    private final static QName _OpCrearFormatoSolicitudProgramaResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearFormatoSolicitudProgramaResp");
    private final static QName _OpBuscarPorIdSolicitudInformacionProgramaFallo_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpBuscarPorIdSolicitudInformacionProgramaFallo");
    private final static QName _OpActualizarSolicitudInformacionProgramaSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpActualizarSolicitudInformacionProgramaSol");
    private final static QName _OpActualizarSolicitudInformacionProgramaResp_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpActualizarSolicitudInformacionProgramaResp");
    private final static QName _OpCrearSolicitudInformacionProgramaSol_QNAME = new QName("http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", "OpCrearSolicitudInformacionProgramaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudInformacionProgramaSolTipo }
     * 
     */
    public OpBuscarPorIdSolicitudInformacionProgramaSolTipo createOpBuscarPorIdSolicitudInformacionProgramaSolTipo() {
        return new OpBuscarPorIdSolicitudInformacionProgramaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudInformacionProgramaRespTipo }
     * 
     */
    public OpCrearSolicitudInformacionProgramaRespTipo createOpCrearSolicitudInformacionProgramaRespTipo() {
        return new OpCrearSolicitudInformacionProgramaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdSolicitudInformacionProgramaRespTipo }
     * 
     */
    public OpBuscarPorIdSolicitudInformacionProgramaRespTipo createOpBuscarPorIdSolicitudInformacionProgramaRespTipo() {
        return new OpBuscarPorIdSolicitudInformacionProgramaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudInformacionProgramaSolTipo }
     * 
     */
    public OpActualizarSolicitudInformacionProgramaSolTipo createOpActualizarSolicitudInformacionProgramaSolTipo() {
        return new OpActualizarSolicitudInformacionProgramaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarSolicitudInformacionProgramaRespTipo }
     * 
     */
    public OpActualizarSolicitudInformacionProgramaRespTipo createOpActualizarSolicitudInformacionProgramaRespTipo() {
        return new OpActualizarSolicitudInformacionProgramaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearSolicitudInformacionProgramaSolTipo }
     * 
     */
    public OpCrearSolicitudInformacionProgramaSolTipo createOpCrearSolicitudInformacionProgramaSolTipo() {
        return new OpCrearSolicitudInformacionProgramaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearFormatoSolicitudProgramaRespTipo }
     * 
     */
    public OpCrearFormatoSolicitudProgramaRespTipo createOpCrearFormatoSolicitudProgramaRespTipo() {
        return new OpCrearFormatoSolicitudProgramaRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearFormatoSolicitudProgramaSolTipo }
     * 
     */
    public OpCrearFormatoSolicitudProgramaSolTipo createOpCrearFormatoSolicitudProgramaSolTipo() {
        return new OpCrearFormatoSolicitudProgramaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudInformacionProgramaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpBuscarPorIdSolicitudInformacionProgramaResp")
    public JAXBElement<OpBuscarPorIdSolicitudInformacionProgramaRespTipo> createOpBuscarPorIdSolicitudInformacionProgramaResp(OpBuscarPorIdSolicitudInformacionProgramaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudInformacionProgramaRespTipo>(_OpBuscarPorIdSolicitudInformacionProgramaResp_QNAME, OpBuscarPorIdSolicitudInformacionProgramaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpActualizarSolicitudInformacionProgramaFallo")
    public JAXBElement<FalloTipo> createOpActualizarSolicitudInformacionProgramaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarSolicitudInformacionProgramaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdSolicitudInformacionProgramaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpBuscarPorIdSolicitudInformacionProgramaSol")
    public JAXBElement<OpBuscarPorIdSolicitudInformacionProgramaSolTipo> createOpBuscarPorIdSolicitudInformacionProgramaSol(OpBuscarPorIdSolicitudInformacionProgramaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdSolicitudInformacionProgramaSolTipo>(_OpBuscarPorIdSolicitudInformacionProgramaSol_QNAME, OpBuscarPorIdSolicitudInformacionProgramaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudInformacionProgramaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearSolicitudInformacionProgramaResp")
    public JAXBElement<OpCrearSolicitudInformacionProgramaRespTipo> createOpCrearSolicitudInformacionProgramaResp(OpCrearSolicitudInformacionProgramaRespTipo value) {
        return new JAXBElement<OpCrearSolicitudInformacionProgramaRespTipo>(_OpCrearSolicitudInformacionProgramaResp_QNAME, OpCrearSolicitudInformacionProgramaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearSolicitudInformacionProgramaFallo")
    public JAXBElement<FalloTipo> createOpCrearSolicitudInformacionProgramaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearSolicitudInformacionProgramaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearFormatoSolicitudProgramaFallo")
    public JAXBElement<FalloTipo> createOpCrearFormatoSolicitudProgramaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearFormatoSolicitudProgramaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoSolicitudProgramaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearFormatoSolicitudProgramaSol")
    public JAXBElement<OpCrearFormatoSolicitudProgramaSolTipo> createOpCrearFormatoSolicitudProgramaSol(OpCrearFormatoSolicitudProgramaSolTipo value) {
        return new JAXBElement<OpCrearFormatoSolicitudProgramaSolTipo>(_OpCrearFormatoSolicitudProgramaSol_QNAME, OpCrearFormatoSolicitudProgramaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearFormatoSolicitudProgramaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearFormatoSolicitudProgramaResp")
    public JAXBElement<OpCrearFormatoSolicitudProgramaRespTipo> createOpCrearFormatoSolicitudProgramaResp(OpCrearFormatoSolicitudProgramaRespTipo value) {
        return new JAXBElement<OpCrearFormatoSolicitudProgramaRespTipo>(_OpCrearFormatoSolicitudProgramaResp_QNAME, OpCrearFormatoSolicitudProgramaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpBuscarPorIdSolicitudInformacionProgramaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdSolicitudInformacionProgramaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdSolicitudInformacionProgramaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudInformacionProgramaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpActualizarSolicitudInformacionProgramaSol")
    public JAXBElement<OpActualizarSolicitudInformacionProgramaSolTipo> createOpActualizarSolicitudInformacionProgramaSol(OpActualizarSolicitudInformacionProgramaSolTipo value) {
        return new JAXBElement<OpActualizarSolicitudInformacionProgramaSolTipo>(_OpActualizarSolicitudInformacionProgramaSol_QNAME, OpActualizarSolicitudInformacionProgramaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarSolicitudInformacionProgramaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpActualizarSolicitudInformacionProgramaResp")
    public JAXBElement<OpActualizarSolicitudInformacionProgramaRespTipo> createOpActualizarSolicitudInformacionProgramaResp(OpActualizarSolicitudInformacionProgramaRespTipo value) {
        return new JAXBElement<OpActualizarSolicitudInformacionProgramaRespTipo>(_OpActualizarSolicitudInformacionProgramaResp_QNAME, OpActualizarSolicitudInformacionProgramaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearSolicitudInformacionProgramaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/SolicitarInformacion/SrvAplSolicitudInformacionPrograma/v1", name = "OpCrearSolicitudInformacionProgramaSol")
    public JAXBElement<OpCrearSolicitudInformacionProgramaSolTipo> createOpCrearSolicitudInformacionProgramaSol(OpCrearSolicitudInformacionProgramaSolTipo value) {
        return new JAXBElement<OpCrearSolicitudInformacionProgramaSolTipo>(_OpCrearSolicitudInformacionProgramaSol_QNAME, OpCrearSolicitudInformacionProgramaSolTipo.class, null, value);
    }

}
