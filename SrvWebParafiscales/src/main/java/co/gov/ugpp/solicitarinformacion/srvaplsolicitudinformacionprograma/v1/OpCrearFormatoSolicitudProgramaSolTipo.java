
package co.gov.ugpp.solicitarinformacion.srvaplsolicitudinformacionprograma.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.formatoestructuratipo.v1.FormatoEstructuraTipo;


/**
 * <p>Clase Java para OpCrearFormatoSolicitudProgramaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearFormatoSolicitudProgramaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="formatoEstructura" type="{http://www.ugpp.gov.co/schema/GestionDocumental/FormatoEstructuraTipo/v1}FormatoEstructuraTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearFormatoSolicitudProgramaSolTipo", propOrder = {
    "contextoTransaccional",
    "formatoEstructura"
})
public class OpCrearFormatoSolicitudProgramaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected FormatoEstructuraTipo formatoEstructura;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad formatoEstructura.
     * 
     * @return
     *     possible object is
     *     {@link FormatoEstructuraTipo }
     *     
     */
    public FormatoEstructuraTipo getFormatoEstructura() {
        return formatoEstructura;
    }

    /**
     * Define el valor de la propiedad formatoEstructura.
     * 
     * @param value
     *     allowed object is
     *     {@link FormatoEstructuraTipo }
     *     
     */
    public void setFormatoEstructura(FormatoEstructuraTipo value) {
        this.formatoEstructura = value;
    }

}
