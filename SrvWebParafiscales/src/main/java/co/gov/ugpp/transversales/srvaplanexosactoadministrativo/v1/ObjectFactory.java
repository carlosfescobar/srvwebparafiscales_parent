
package co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearAnexoActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpCrearAnexoActoAdministrativoSol");
    private final static QName _OpBuscarPorCriteriosAnexoActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpBuscarPorCriteriosAnexoActoAdministrativoFallo");
    private final static QName _OpCrearAnexoActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpCrearAnexoActoAdministrativoResp");
    private final static QName _OpCrearAnexoActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpCrearAnexoActoAdministrativoFallo");
    private final static QName _OpActualizarAnexoActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpActualizarAnexoActoAdministrativoResp");
    private final static QName _OpActualizarAnexoActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpActualizarAnexoActoAdministrativoFallo");
    private final static QName _OpBuscarPorCriteriosAnexoActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpBuscarPorCriteriosAnexoActoAdministrativoSol");
    private final static QName _OpActualizarAnexoActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpActualizarAnexoActoAdministrativoSol");
    private final static QName _OpBuscarPorCriteriosAnexoActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", "OpBuscarPorCriteriosAnexoActoAdministrativoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplanexosactoadministrativo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearAnexoActoAdministrativoRespTipo }
     * 
     */
    public OpCrearAnexoActoAdministrativoRespTipo createOpCrearAnexoActoAdministrativoRespTipo() {
        return new OpCrearAnexoActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAnexoActoAdministrativoSolTipo }
     * 
     */
    public OpCrearAnexoActoAdministrativoSolTipo createOpCrearAnexoActoAdministrativoSolTipo() {
        return new OpCrearAnexoActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAnexoActoAdministrativoRespTipo }
     * 
     */
    public OpActualizarAnexoActoAdministrativoRespTipo createOpActualizarAnexoActoAdministrativoRespTipo() {
        return new OpActualizarAnexoActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAnexoActoAdministrativoSolTipo }
     * 
     */
    public OpActualizarAnexoActoAdministrativoSolTipo createOpActualizarAnexoActoAdministrativoSolTipo() {
        return new OpActualizarAnexoActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo createOpBuscarPorCriteriosAnexoActoAdministrativoSolTipo() {
        return new OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo createOpBuscarPorCriteriosAnexoActoAdministrativoRespTipo() {
        return new OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAnexoActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpCrearAnexoActoAdministrativoSol")
    public JAXBElement<OpCrearAnexoActoAdministrativoSolTipo> createOpCrearAnexoActoAdministrativoSol(OpCrearAnexoActoAdministrativoSolTipo value) {
        return new JAXBElement<OpCrearAnexoActoAdministrativoSolTipo>(_OpCrearAnexoActoAdministrativoSol_QNAME, OpCrearAnexoActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpBuscarPorCriteriosAnexoActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAnexoActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAnexoActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAnexoActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpCrearAnexoActoAdministrativoResp")
    public JAXBElement<OpCrearAnexoActoAdministrativoRespTipo> createOpCrearAnexoActoAdministrativoResp(OpCrearAnexoActoAdministrativoRespTipo value) {
        return new JAXBElement<OpCrearAnexoActoAdministrativoRespTipo>(_OpCrearAnexoActoAdministrativoResp_QNAME, OpCrearAnexoActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpCrearAnexoActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpCrearAnexoActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAnexoActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAnexoActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpActualizarAnexoActoAdministrativoResp")
    public JAXBElement<OpActualizarAnexoActoAdministrativoRespTipo> createOpActualizarAnexoActoAdministrativoResp(OpActualizarAnexoActoAdministrativoRespTipo value) {
        return new JAXBElement<OpActualizarAnexoActoAdministrativoRespTipo>(_OpActualizarAnexoActoAdministrativoResp_QNAME, OpActualizarAnexoActoAdministrativoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpActualizarAnexoActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpActualizarAnexoActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAnexoActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpBuscarPorCriteriosAnexoActoAdministrativoSol")
    public JAXBElement<OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo> createOpBuscarPorCriteriosAnexoActoAdministrativoSol(OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo>(_OpBuscarPorCriteriosAnexoActoAdministrativoSol_QNAME, OpBuscarPorCriteriosAnexoActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAnexoActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpActualizarAnexoActoAdministrativoSol")
    public JAXBElement<OpActualizarAnexoActoAdministrativoSolTipo> createOpActualizarAnexoActoAdministrativoSol(OpActualizarAnexoActoAdministrativoSolTipo value) {
        return new JAXBElement<OpActualizarAnexoActoAdministrativoSolTipo>(_OpActualizarAnexoActoAdministrativoSol_QNAME, OpActualizarAnexoActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAnexosActoAdministrativo/v1", name = "OpBuscarPorCriteriosAnexoActoAdministrativoResp")
    public JAXBElement<OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo> createOpBuscarPorCriteriosAnexoActoAdministrativoResp(OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo>(_OpBuscarPorCriteriosAnexoActoAdministrativoResp_QNAME, OpBuscarPorCriteriosAnexoActoAdministrativoRespTipo.class, null, value);
    }

}
