
package co.gov.ugpp.transversales.srvaplapoderado.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplapoderado.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdApoderadoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", "OpBuscarPorIdApoderadoResp");
    private final static QName _OpBuscarPorIdApoderadoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", "OpBuscarPorIdApoderadoFallo");
    private final static QName _OpBuscarPorIdApoderadoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", "OpBuscarPorIdApoderadoSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplapoderado.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdApoderadoSolTipo }
     * 
     */
    public OpBuscarPorIdApoderadoSolTipo createOpBuscarPorIdApoderadoSolTipo() {
        return new OpBuscarPorIdApoderadoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdApoderadoRespTipo }
     * 
     */
    public OpBuscarPorIdApoderadoRespTipo createOpBuscarPorIdApoderadoRespTipo() {
        return new OpBuscarPorIdApoderadoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdApoderadoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", name = "OpBuscarPorIdApoderadoResp")
    public JAXBElement<OpBuscarPorIdApoderadoRespTipo> createOpBuscarPorIdApoderadoResp(OpBuscarPorIdApoderadoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdApoderadoRespTipo>(_OpBuscarPorIdApoderadoResp_QNAME, OpBuscarPorIdApoderadoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", name = "OpBuscarPorIdApoderadoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdApoderadoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdApoderadoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdApoderadoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplApoderado/v1", name = "OpBuscarPorIdApoderadoSol")
    public JAXBElement<OpBuscarPorIdApoderadoSolTipo> createOpBuscarPorIdApoderadoSol(OpBuscarPorIdApoderadoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdApoderadoSolTipo>(_OpBuscarPorIdApoderadoSol_QNAME, OpBuscarPorIdApoderadoSolTipo.class, null, value);
    }

}
