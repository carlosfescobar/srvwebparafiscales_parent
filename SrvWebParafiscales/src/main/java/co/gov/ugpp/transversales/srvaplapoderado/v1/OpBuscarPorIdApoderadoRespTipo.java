
package co.gov.ugpp.transversales.srvaplapoderado.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.transversales.apoderadotipo.v1.ApoderadoTipo;


/**
 * <p>Clase Java para OpBuscarPorIdApoderadoRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpBuscarPorIdApoderadoRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="apoderado" type="{http://www.ugpp.gov.co/schema/Transversales/ApoderadoTipo/v1}ApoderadoTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpBuscarPorIdApoderadoRespTipo", propOrder = {
    "contextoRespuesta",
    "apoderado"
})
public class OpBuscarPorIdApoderadoRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    protected ApoderadoTipo apoderado;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad apoderado.
     * 
     * @return
     *     possible object is
     *     {@link ApoderadoTipo }
     *     
     */
    public ApoderadoTipo getApoderado() {
        return apoderado;
    }

    /**
     * Define el valor de la propiedad apoderado.
     * 
     * @param value
     *     allowed object is
     *     {@link ApoderadoTipo }
     *     
     */
    public void setApoderado(ApoderadoTipo value) {
        this.apoderado = value;
    }

}
