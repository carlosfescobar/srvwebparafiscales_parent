
package co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearAprobacionMasivaActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpCrearAprobacionMasivaActoSol");
    private final static QName _OpActualizarAprobacionMasivaActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpActualizarAprobacionMasivaActoSol");
    private final static QName _OpActualizarAprobacionMasivaActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpActualizarAprobacionMasivaActoFallo");
    private final static QName _OpBuscarPorCriteriosAprobacionMasivaActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpBuscarPorCriteriosAprobacionMasivaActoFallo");
    private final static QName _OpBuscarPorCriteriosAprobacionMasivaActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpBuscarPorCriteriosAprobacionMasivaActoSol");
    private final static QName _OpCrearAprobacionMasivaActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpCrearAprobacionMasivaActoResp");
    private final static QName _OpActualizarAprobacionMasivaActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpActualizarAprobacionMasivaActoResp");
    private final static QName _OpBuscarPorCriteriosAprobacionMasivaActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpBuscarPorCriteriosAprobacionMasivaActoResp");
    private final static QName _OpCrearAprobacionMasivaActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", "OpCrearAprobacionMasivaActoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarAprobacionMasivaActoSolTipo }
     * 
     */
    public OpActualizarAprobacionMasivaActoSolTipo createOpActualizarAprobacionMasivaActoSolTipo() {
        return new OpActualizarAprobacionMasivaActoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearAprobacionMasivaActoSolTipo }
     * 
     */
    public OpCrearAprobacionMasivaActoSolTipo createOpCrearAprobacionMasivaActoSolTipo() {
        return new OpCrearAprobacionMasivaActoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAprobacionMasivaActoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosAprobacionMasivaActoSolTipo createOpBuscarPorCriteriosAprobacionMasivaActoSolTipo() {
        return new OpBuscarPorCriteriosAprobacionMasivaActoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosAprobacionMasivaActoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosAprobacionMasivaActoRespTipo createOpBuscarPorCriteriosAprobacionMasivaActoRespTipo() {
        return new OpBuscarPorCriteriosAprobacionMasivaActoRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearAprobacionMasivaActoRespTipo }
     * 
     */
    public OpCrearAprobacionMasivaActoRespTipo createOpCrearAprobacionMasivaActoRespTipo() {
        return new OpCrearAprobacionMasivaActoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarAprobacionMasivaActoRespTipo }
     * 
     */
    public OpActualizarAprobacionMasivaActoRespTipo createOpActualizarAprobacionMasivaActoRespTipo() {
        return new OpActualizarAprobacionMasivaActoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAprobacionMasivaActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpCrearAprobacionMasivaActoSol")
    public JAXBElement<OpCrearAprobacionMasivaActoSolTipo> createOpCrearAprobacionMasivaActoSol(OpCrearAprobacionMasivaActoSolTipo value) {
        return new JAXBElement<OpCrearAprobacionMasivaActoSolTipo>(_OpCrearAprobacionMasivaActoSol_QNAME, OpCrearAprobacionMasivaActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAprobacionMasivaActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpActualizarAprobacionMasivaActoSol")
    public JAXBElement<OpActualizarAprobacionMasivaActoSolTipo> createOpActualizarAprobacionMasivaActoSol(OpActualizarAprobacionMasivaActoSolTipo value) {
        return new JAXBElement<OpActualizarAprobacionMasivaActoSolTipo>(_OpActualizarAprobacionMasivaActoSol_QNAME, OpActualizarAprobacionMasivaActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpActualizarAprobacionMasivaActoFallo")
    public JAXBElement<FalloTipo> createOpActualizarAprobacionMasivaActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarAprobacionMasivaActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpBuscarPorCriteriosAprobacionMasivaActoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosAprobacionMasivaActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosAprobacionMasivaActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAprobacionMasivaActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpBuscarPorCriteriosAprobacionMasivaActoSol")
    public JAXBElement<OpBuscarPorCriteriosAprobacionMasivaActoSolTipo> createOpBuscarPorCriteriosAprobacionMasivaActoSol(OpBuscarPorCriteriosAprobacionMasivaActoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAprobacionMasivaActoSolTipo>(_OpBuscarPorCriteriosAprobacionMasivaActoSol_QNAME, OpBuscarPorCriteriosAprobacionMasivaActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearAprobacionMasivaActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpCrearAprobacionMasivaActoResp")
    public JAXBElement<OpCrearAprobacionMasivaActoRespTipo> createOpCrearAprobacionMasivaActoResp(OpCrearAprobacionMasivaActoRespTipo value) {
        return new JAXBElement<OpCrearAprobacionMasivaActoRespTipo>(_OpCrearAprobacionMasivaActoResp_QNAME, OpCrearAprobacionMasivaActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarAprobacionMasivaActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpActualizarAprobacionMasivaActoResp")
    public JAXBElement<OpActualizarAprobacionMasivaActoRespTipo> createOpActualizarAprobacionMasivaActoResp(OpActualizarAprobacionMasivaActoRespTipo value) {
        return new JAXBElement<OpActualizarAprobacionMasivaActoRespTipo>(_OpActualizarAprobacionMasivaActoResp_QNAME, OpActualizarAprobacionMasivaActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosAprobacionMasivaActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpBuscarPorCriteriosAprobacionMasivaActoResp")
    public JAXBElement<OpBuscarPorCriteriosAprobacionMasivaActoRespTipo> createOpBuscarPorCriteriosAprobacionMasivaActoResp(OpBuscarPorCriteriosAprobacionMasivaActoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosAprobacionMasivaActoRespTipo>(_OpBuscarPorCriteriosAprobacionMasivaActoResp_QNAME, OpBuscarPorCriteriosAprobacionMasivaActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplAprobacionMasivaActo/v1", name = "OpCrearAprobacionMasivaActoFallo")
    public JAXBElement<FalloTipo> createOpCrearAprobacionMasivaActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearAprobacionMasivaActoFallo_QNAME, FalloTipo.class, null, value);
    }

}
