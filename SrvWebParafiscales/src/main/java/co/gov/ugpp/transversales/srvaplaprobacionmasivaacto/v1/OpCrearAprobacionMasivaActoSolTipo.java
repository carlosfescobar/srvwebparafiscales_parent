
package co.gov.ugpp.transversales.srvaplaprobacionmasivaacto.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.AprobacionMasivaActoTipo;


/**
 * <p>Clase Java para OpCrearAprobacionMasivaActoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearAprobacionMasivaActoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="aprobacionMasivaActo" type="{http://www.ugpp.gov.co/schema/Transversales/AprobacionMasivaActoTipo/v1}AprobacionMasivaActoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearAprobacionMasivaActoSolTipo", propOrder = {
    "contextoTransaccional",
    "aprobacionMasivaActo"
})
public class OpCrearAprobacionMasivaActoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected AprobacionMasivaActoTipo aprobacionMasivaActo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad aprobacionMasivaActo.
     * 
     * @return
     *     possible object is
     *     {@link AprobacionMasivaActoTipo }
     *     
     */
    public AprobacionMasivaActoTipo getAprobacionMasivaActo() {
        return aprobacionMasivaActo;
    }

    /**
     * Define el valor de la propiedad aprobacionMasivaActo.
     * 
     * @param value
     *     allowed object is
     *     {@link AprobacionMasivaActoTipo }
     *     
     */
    public void setAprobacionMasivaActo(AprobacionMasivaActoTipo value) {
        this.aprobacionMasivaActo = value;
    }

}
