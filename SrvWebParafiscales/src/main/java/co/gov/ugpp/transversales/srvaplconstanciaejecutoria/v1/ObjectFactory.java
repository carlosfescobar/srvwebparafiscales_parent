
package co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearConstanciaEjecutoriaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpCrearConstanciaEjecutoriaSol");
    private final static QName _OpBuscarPorIdConstanciaEjecutoriaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpBuscarPorIdConstanciaEjecutoriaFallo");
    private final static QName _OpActualizarConstanciaEjecutoriaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpActualizarConstanciaEjecutoriaFallo");
    private final static QName _OpCrearConstanciaEjecutoriaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpCrearConstanciaEjecutoriaResp");
    private final static QName _OpActualizarConstanciaEjecutoriaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpActualizarConstanciaEjecutoriaSol");
    private final static QName _OpCrearConstanciaEjecutoriaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpCrearConstanciaEjecutoriaFallo");
    private final static QName _OpActualizarConstanciaEjecutoriaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpActualizarConstanciaEjecutoriaResp");
    private final static QName _OpBuscarPorIdConstanciaEjecutoriaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpBuscarPorIdConstanciaEjecutoriaSol");
    private final static QName _OpBuscarPorIdConstanciaEjecutoriaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", "OpBuscarPorIdConstanciaEjecutoriaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearConstanciaEjecutoriaSolTipo }
     * 
     */
    public OpCrearConstanciaEjecutoriaSolTipo createOpCrearConstanciaEjecutoriaSolTipo() {
        return new OpCrearConstanciaEjecutoriaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarConstanciaEjecutoriaRespTipo }
     * 
     */
    public OpActualizarConstanciaEjecutoriaRespTipo createOpActualizarConstanciaEjecutoriaRespTipo() {
        return new OpActualizarConstanciaEjecutoriaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarConstanciaEjecutoriaSolTipo }
     * 
     */
    public OpActualizarConstanciaEjecutoriaSolTipo createOpActualizarConstanciaEjecutoriaSolTipo() {
        return new OpActualizarConstanciaEjecutoriaSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearConstanciaEjecutoriaRespTipo }
     * 
     */
    public OpCrearConstanciaEjecutoriaRespTipo createOpCrearConstanciaEjecutoriaRespTipo() {
        return new OpCrearConstanciaEjecutoriaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdConstanciaEjecutoriaRespTipo }
     * 
     */
    public OpBuscarPorIdConstanciaEjecutoriaRespTipo createOpBuscarPorIdConstanciaEjecutoriaRespTipo() {
        return new OpBuscarPorIdConstanciaEjecutoriaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdConstanciaEjecutoriaSolTipo }
     * 
     */
    public OpBuscarPorIdConstanciaEjecutoriaSolTipo createOpBuscarPorIdConstanciaEjecutoriaSolTipo() {
        return new OpBuscarPorIdConstanciaEjecutoriaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearConstanciaEjecutoriaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpCrearConstanciaEjecutoriaSol")
    public JAXBElement<OpCrearConstanciaEjecutoriaSolTipo> createOpCrearConstanciaEjecutoriaSol(OpCrearConstanciaEjecutoriaSolTipo value) {
        return new JAXBElement<OpCrearConstanciaEjecutoriaSolTipo>(_OpCrearConstanciaEjecutoriaSol_QNAME, OpCrearConstanciaEjecutoriaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpBuscarPorIdConstanciaEjecutoriaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdConstanciaEjecutoriaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdConstanciaEjecutoriaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpActualizarConstanciaEjecutoriaFallo")
    public JAXBElement<FalloTipo> createOpActualizarConstanciaEjecutoriaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarConstanciaEjecutoriaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearConstanciaEjecutoriaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpCrearConstanciaEjecutoriaResp")
    public JAXBElement<OpCrearConstanciaEjecutoriaRespTipo> createOpCrearConstanciaEjecutoriaResp(OpCrearConstanciaEjecutoriaRespTipo value) {
        return new JAXBElement<OpCrearConstanciaEjecutoriaRespTipo>(_OpCrearConstanciaEjecutoriaResp_QNAME, OpCrearConstanciaEjecutoriaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarConstanciaEjecutoriaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpActualizarConstanciaEjecutoriaSol")
    public JAXBElement<OpActualizarConstanciaEjecutoriaSolTipo> createOpActualizarConstanciaEjecutoriaSol(OpActualizarConstanciaEjecutoriaSolTipo value) {
        return new JAXBElement<OpActualizarConstanciaEjecutoriaSolTipo>(_OpActualizarConstanciaEjecutoriaSol_QNAME, OpActualizarConstanciaEjecutoriaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpCrearConstanciaEjecutoriaFallo")
    public JAXBElement<FalloTipo> createOpCrearConstanciaEjecutoriaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearConstanciaEjecutoriaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarConstanciaEjecutoriaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpActualizarConstanciaEjecutoriaResp")
    public JAXBElement<OpActualizarConstanciaEjecutoriaRespTipo> createOpActualizarConstanciaEjecutoriaResp(OpActualizarConstanciaEjecutoriaRespTipo value) {
        return new JAXBElement<OpActualizarConstanciaEjecutoriaRespTipo>(_OpActualizarConstanciaEjecutoriaResp_QNAME, OpActualizarConstanciaEjecutoriaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdConstanciaEjecutoriaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpBuscarPorIdConstanciaEjecutoriaSol")
    public JAXBElement<OpBuscarPorIdConstanciaEjecutoriaSolTipo> createOpBuscarPorIdConstanciaEjecutoriaSol(OpBuscarPorIdConstanciaEjecutoriaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdConstanciaEjecutoriaSolTipo>(_OpBuscarPorIdConstanciaEjecutoriaSol_QNAME, OpBuscarPorIdConstanciaEjecutoriaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdConstanciaEjecutoriaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplConstanciaEjecutoria/v1", name = "OpBuscarPorIdConstanciaEjecutoriaResp")
    public JAXBElement<OpBuscarPorIdConstanciaEjecutoriaRespTipo> createOpBuscarPorIdConstanciaEjecutoriaResp(OpBuscarPorIdConstanciaEjecutoriaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdConstanciaEjecutoriaRespTipo>(_OpBuscarPorIdConstanciaEjecutoriaResp_QNAME, OpBuscarPorIdConstanciaEjecutoriaRespTipo.class, null, value);
    }

}
