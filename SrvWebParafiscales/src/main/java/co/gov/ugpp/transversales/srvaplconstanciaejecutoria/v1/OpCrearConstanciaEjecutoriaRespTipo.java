
package co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;


/**
 * <p>Clase Java para OpCrearConstanciaEjecutoriaRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearConstanciaEjecutoriaRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="constanciaEjecutoria" type="{http://www.ugpp.gov.co/schema/Transversales/ConstanciaEjecutoriaTipo/v1}ConstanciaEjecutoriaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearConstanciaEjecutoriaRespTipo", propOrder = {
    "contextoRespuesta",
    "constanciaEjecutoria"
})
public class OpCrearConstanciaEjecutoriaRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(required = true)
    protected ConstanciaEjecutoriaTipo constanciaEjecutoria;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link ConstanciaEjecutoriaTipo }
     *     
     */
    public ConstanciaEjecutoriaTipo getConstanciaEjecutoria() {
        return constanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad constanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstanciaEjecutoriaTipo }
     *     
     */
    public void setConstanciaEjecutoria(ConstanciaEjecutoriaTipo value) {
        this.constanciaEjecutoria = value;
    }

}
