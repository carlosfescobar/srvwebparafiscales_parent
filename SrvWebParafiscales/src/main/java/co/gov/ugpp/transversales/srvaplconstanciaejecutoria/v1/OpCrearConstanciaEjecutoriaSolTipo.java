
package co.gov.ugpp.transversales.srvaplconstanciaejecutoria.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.constanciaejecutoriatipo.v1.ConstanciaEjecutoriaTipo;


/**
 * <p>Clase Java para OpCrearConstanciaEjecutoriaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearConstanciaEjecutoriaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="constanciaEjecutoria" type="{http://www.ugpp.gov.co/schema/Transversales/ConstanciaEjecutoriaTipo/v1}ConstanciaEjecutoriaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearConstanciaEjecutoriaSolTipo", propOrder = {
    "contextoTransaccional",
    "constanciaEjecutoria"
})
public class OpCrearConstanciaEjecutoriaSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ConstanciaEjecutoriaTipo constanciaEjecutoria;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad constanciaEjecutoria.
     * 
     * @return
     *     possible object is
     *     {@link ConstanciaEjecutoriaTipo }
     *     
     */
    public ConstanciaEjecutoriaTipo getConstanciaEjecutoria() {
        return constanciaEjecutoria;
    }

    /**
     * Define el valor de la propiedad constanciaEjecutoria.
     * 
     * @param value
     *     allowed object is
     *     {@link ConstanciaEjecutoriaTipo }
     *     
     */
    public void setConstanciaEjecutoria(ConstanciaEjecutoriaTipo value) {
        this.constanciaEjecutoria = value;
    }

}
