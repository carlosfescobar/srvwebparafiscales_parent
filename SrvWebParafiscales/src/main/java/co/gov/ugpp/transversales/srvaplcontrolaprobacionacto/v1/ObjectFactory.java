
package co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarControlAprobacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpActualizarControlAprobacionActoFallo");
    private final static QName _OpCrearControlAprobacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpCrearControlAprobacionActoResp");
    private final static QName _OpCrearControlAprobacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpCrearControlAprobacionActoSol");
    private final static QName _OpCrearControlAprobacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpCrearControlAprobacionActoFallo");
    private final static QName _OpBuscarPorCriteriosControlAprobacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpBuscarPorCriteriosControlAprobacionActoFallo");
    private final static QName _OpBuscarPorCriteriosControlAprobacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpBuscarPorCriteriosControlAprobacionActoResp");
    private final static QName _OpActualizarControlAprobacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpActualizarControlAprobacionActoSol");
    private final static QName _OpActualizarControlAprobacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpActualizarControlAprobacionActoResp");
    private final static QName _OpBuscarPorCriteriosControlAprobacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", "OpBuscarPorCriteriosControlAprobacionActoSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlAprobacionActoSolTipo }
     * 
     */
    public OpBuscarPorCriteriosControlAprobacionActoSolTipo createOpBuscarPorCriteriosControlAprobacionActoSolTipo() {
        return new OpBuscarPorCriteriosControlAprobacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlAprobacionActoRespTipo }
     * 
     */
    public OpActualizarControlAprobacionActoRespTipo createOpActualizarControlAprobacionActoRespTipo() {
        return new OpActualizarControlAprobacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlAprobacionActoSolTipo }
     * 
     */
    public OpActualizarControlAprobacionActoSolTipo createOpActualizarControlAprobacionActoSolTipo() {
        return new OpActualizarControlAprobacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlAprobacionActoSolTipo }
     * 
     */
    public OpCrearControlAprobacionActoSolTipo createOpCrearControlAprobacionActoSolTipo() {
        return new OpCrearControlAprobacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlAprobacionActoRespTipo }
     * 
     */
    public OpCrearControlAprobacionActoRespTipo createOpCrearControlAprobacionActoRespTipo() {
        return new OpCrearControlAprobacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlAprobacionActoRespTipo }
     * 
     */
    public OpBuscarPorCriteriosControlAprobacionActoRespTipo createOpBuscarPorCriteriosControlAprobacionActoRespTipo() {
        return new OpBuscarPorCriteriosControlAprobacionActoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpActualizarControlAprobacionActoFallo")
    public JAXBElement<FalloTipo> createOpActualizarControlAprobacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarControlAprobacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlAprobacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpCrearControlAprobacionActoResp")
    public JAXBElement<OpCrearControlAprobacionActoRespTipo> createOpCrearControlAprobacionActoResp(OpCrearControlAprobacionActoRespTipo value) {
        return new JAXBElement<OpCrearControlAprobacionActoRespTipo>(_OpCrearControlAprobacionActoResp_QNAME, OpCrearControlAprobacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlAprobacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpCrearControlAprobacionActoSol")
    public JAXBElement<OpCrearControlAprobacionActoSolTipo> createOpCrearControlAprobacionActoSol(OpCrearControlAprobacionActoSolTipo value) {
        return new JAXBElement<OpCrearControlAprobacionActoSolTipo>(_OpCrearControlAprobacionActoSol_QNAME, OpCrearControlAprobacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpCrearControlAprobacionActoFallo")
    public JAXBElement<FalloTipo> createOpCrearControlAprobacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearControlAprobacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpBuscarPorCriteriosControlAprobacionActoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosControlAprobacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosControlAprobacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlAprobacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpBuscarPorCriteriosControlAprobacionActoResp")
    public JAXBElement<OpBuscarPorCriteriosControlAprobacionActoRespTipo> createOpBuscarPorCriteriosControlAprobacionActoResp(OpBuscarPorCriteriosControlAprobacionActoRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlAprobacionActoRespTipo>(_OpBuscarPorCriteriosControlAprobacionActoResp_QNAME, OpBuscarPorCriteriosControlAprobacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlAprobacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpActualizarControlAprobacionActoSol")
    public JAXBElement<OpActualizarControlAprobacionActoSolTipo> createOpActualizarControlAprobacionActoSol(OpActualizarControlAprobacionActoSolTipo value) {
        return new JAXBElement<OpActualizarControlAprobacionActoSolTipo>(_OpActualizarControlAprobacionActoSol_QNAME, OpActualizarControlAprobacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlAprobacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpActualizarControlAprobacionActoResp")
    public JAXBElement<OpActualizarControlAprobacionActoRespTipo> createOpActualizarControlAprobacionActoResp(OpActualizarControlAprobacionActoRespTipo value) {
        return new JAXBElement<OpActualizarControlAprobacionActoRespTipo>(_OpActualizarControlAprobacionActoResp_QNAME, OpActualizarControlAprobacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlAprobacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", name = "OpBuscarPorCriteriosControlAprobacionActoSol")
    public JAXBElement<OpBuscarPorCriteriosControlAprobacionActoSolTipo> createOpBuscarPorCriteriosControlAprobacionActoSol(OpBuscarPorCriteriosControlAprobacionActoSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlAprobacionActoSolTipo>(_OpBuscarPorCriteriosControlAprobacionActoSol_QNAME, OpBuscarPorCriteriosControlAprobacionActoSolTipo.class, null, value);
    }

}
