
package co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ControlAprobacionActoTipo;


/**
 * <p>Clase Java para OpCrearControlAprobacionActoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearControlAprobacionActoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="ControlAprobacionActo" type="{http://www.ugpp.gov.co/schema/Transversales/ControlAprobacionActoTipo/v1}ControlAprobacionActoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearControlAprobacionActoSolTipo", propOrder = {
    "contextoTransaccional",
    "controlAprobacionActo"
})
public class OpCrearControlAprobacionActoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(name = "ControlAprobacionActo", required = true)
    protected ControlAprobacionActoTipo controlAprobacionActo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad controlAprobacionActo.
     * 
     * @return
     *     possible object is
     *     {@link ControlAprobacionActoTipo }
     *     
     */
    public ControlAprobacionActoTipo getControlAprobacionActo() {
        return controlAprobacionActo;
    }

    /**
     * Define el valor de la propiedad controlAprobacionActo.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlAprobacionActoTipo }
     *     
     */
    public void setControlAprobacionActo(ControlAprobacionActoTipo value) {
        this.controlAprobacionActo = value;
    }

}
