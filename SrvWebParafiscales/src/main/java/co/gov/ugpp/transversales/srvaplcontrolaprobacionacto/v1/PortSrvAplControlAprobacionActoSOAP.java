
package co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplControlAprobacionActoSOAP", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contactopersonatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.departamentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.identificaciontipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.municipiotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.parametrotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.personajuridicatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.personanaturaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.telefonotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.ubicacionpersonatipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.denuncias.aportantetipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.denuncias.funcionariotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.documentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.expedientetipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.metadatadocumentotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.gestiondocumental.seriedocumentaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.aprobacionmasivaactotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.controlaprobacionactotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.correoelectronicotipo.v1.ObjectFactory.class,
    co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.ObjectFactory.class
})
public interface PortSrvAplControlAprobacionActoSOAP {


    /**
     * 
     * @param msjOpCrearControlAprobacionActoSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpCrearControlAprobacionActoRespTipo
     * @throws MsjOpCrearControlAprobacionActoFallo
     */
    @WebMethod(operationName = "OpCrearControlAprobacionActo", action = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1/OpCrearControlAprobacionActo")
    @WebResult(name = "OpCrearControlAprobacionActoResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpCrearControlAprobacionActoResp")
    public OpCrearControlAprobacionActoRespTipo opCrearControlAprobacionActo(
        @WebParam(name = "OpCrearControlAprobacionActoSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpCrearControlAprobacionActoSol")
        OpCrearControlAprobacionActoSolTipo msjOpCrearControlAprobacionActoSol)
        throws MsjOpCrearControlAprobacionActoFallo
    ;

    /**
     * 
     * @param msjOpActualizarControlAprobacionActoSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpActualizarControlAprobacionActoRespTipo
     * @throws MsjOpActualizarControlAprobacionActoFallo
     */
    @WebMethod(operationName = "OpActualizarControlAprobacionActo", action = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1/OpActualizarControlAprobacionActo")
    @WebResult(name = "OpActualizarControlAprobacionActoResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpActualizarControlAprobacionActoResp")
    public OpActualizarControlAprobacionActoRespTipo opActualizarControlAprobacionActo(
        @WebParam(name = "OpActualizarControlAprobacionActoSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpActualizarControlAprobacionActoSol")
        OpActualizarControlAprobacionActoSolTipo msjOpActualizarControlAprobacionActoSol)
        throws MsjOpActualizarControlAprobacionActoFallo
    ;

    /**
     * 
     * @param msjOpBuscarPorCriteriosControlAprobacionActoSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplcontrolaprobacionacto.v1.OpBuscarPorCriteriosControlAprobacionActoRespTipo
     * @throws MsjOpBuscarPorCriteriosControlAprobacionActoFallo
     */
    @WebMethod(operationName = "OpBuscarPorCriteriosControlAprobacionActo", action = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1/OpBuscarPorCriteriosControlAprobacionActo")
    @WebResult(name = "OpBuscarPorCriteriosControlAprobacionActoResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpBuscarPorCriteriosControlAprobacionActoResp")
    public OpBuscarPorCriteriosControlAprobacionActoRespTipo opBuscarPorCriteriosControlAprobacionActo(
        @WebParam(name = "OpBuscarPorCriteriosControlAprobacionActoSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlAprobacionActo/v1", partName = "msjOpBuscarPorCriteriosControlAprobacionActoSol")
        OpBuscarPorCriteriosControlAprobacionActoSolTipo msjOpBuscarPorCriteriosControlAprobacionActoSol)
        throws MsjOpBuscarPorCriteriosControlAprobacionActoFallo
    ;

}
