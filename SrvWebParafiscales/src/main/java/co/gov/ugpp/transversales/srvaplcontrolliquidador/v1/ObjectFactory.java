
package co.gov.ugpp.transversales.srvaplcontrolliquidador.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplcontrolliquidador.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearControlLiquidadorResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpCrearControlLiquidadorResp");
    private final static QName _OpCrearControlLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpCrearControlLiquidadorFallo");
    private final static QName _OpActualizarControlLiquidadorResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpActualizarControlLiquidadorResp");
    private final static QName _OpBuscarPorIdControlLiquidadorResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorIdControlLiquidadorResp");
    private final static QName _OpActualizarControlLiquidadorSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpActualizarControlLiquidadorSol");
    private final static QName _OpBuscarPorCriteriosControlLiquidadorSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorCriteriosControlLiquidadorSol");
    private final static QName _OpBuscarPorCriteriosControlLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorCriteriosControlLiquidadorFallo");
    private final static QName _OpBuscarPorIdControlLiquidadorSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorIdControlLiquidadorSol");
    private final static QName _OpCrearControlLiquidadorSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpCrearControlLiquidadorSol");
    private final static QName _OpBuscarPorCriteriosControlLiquidadorResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorCriteriosControlLiquidadorResp");
    private final static QName _OpActualizarControlLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpActualizarControlLiquidadorFallo");
    private final static QName _OpBuscarPorIdControlLiquidadorFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "OpBuscarPorIdControlLiquidadorFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplcontrolliquidador.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlLiquidadorSolTipo }
     * 
     */
    public OpBuscarPorIdControlLiquidadorSolTipo createOpBuscarPorIdControlLiquidadorSolTipo() {
        return new OpBuscarPorIdControlLiquidadorSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlLiquidadorSolTipo }
     * 
     */
    public OpBuscarPorCriteriosControlLiquidadorSolTipo createOpBuscarPorCriteriosControlLiquidadorSolTipo() {
        return new OpBuscarPorCriteriosControlLiquidadorSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlLiquidadorSolTipo }
     * 
     */
    public OpActualizarControlLiquidadorSolTipo createOpActualizarControlLiquidadorSolTipo() {
        return new OpActualizarControlLiquidadorSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdControlLiquidadorRespTipo }
     * 
     */
    public OpBuscarPorIdControlLiquidadorRespTipo createOpBuscarPorIdControlLiquidadorRespTipo() {
        return new OpBuscarPorIdControlLiquidadorRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarControlLiquidadorRespTipo }
     * 
     */
    public OpActualizarControlLiquidadorRespTipo createOpActualizarControlLiquidadorRespTipo() {
        return new OpActualizarControlLiquidadorRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlLiquidadorRespTipo }
     * 
     */
    public OpCrearControlLiquidadorRespTipo createOpCrearControlLiquidadorRespTipo() {
        return new OpCrearControlLiquidadorRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosControlLiquidadorRespTipo }
     * 
     */
    public OpBuscarPorCriteriosControlLiquidadorRespTipo createOpBuscarPorCriteriosControlLiquidadorRespTipo() {
        return new OpBuscarPorCriteriosControlLiquidadorRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearControlLiquidadorSolTipo }
     * 
     */
    public OpCrearControlLiquidadorSolTipo createOpCrearControlLiquidadorSolTipo() {
        return new OpCrearControlLiquidadorSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlLiquidadorRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpCrearControlLiquidadorResp")
    public JAXBElement<OpCrearControlLiquidadorRespTipo> createOpCrearControlLiquidadorResp(OpCrearControlLiquidadorRespTipo value) {
        return new JAXBElement<OpCrearControlLiquidadorRespTipo>(_OpCrearControlLiquidadorResp_QNAME, OpCrearControlLiquidadorRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpCrearControlLiquidadorFallo")
    public JAXBElement<FalloTipo> createOpCrearControlLiquidadorFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearControlLiquidadorFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlLiquidadorRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpActualizarControlLiquidadorResp")
    public JAXBElement<OpActualizarControlLiquidadorRespTipo> createOpActualizarControlLiquidadorResp(OpActualizarControlLiquidadorRespTipo value) {
        return new JAXBElement<OpActualizarControlLiquidadorRespTipo>(_OpActualizarControlLiquidadorResp_QNAME, OpActualizarControlLiquidadorRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlLiquidadorRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorIdControlLiquidadorResp")
    public JAXBElement<OpBuscarPorIdControlLiquidadorRespTipo> createOpBuscarPorIdControlLiquidadorResp(OpBuscarPorIdControlLiquidadorRespTipo value) {
        return new JAXBElement<OpBuscarPorIdControlLiquidadorRespTipo>(_OpBuscarPorIdControlLiquidadorResp_QNAME, OpBuscarPorIdControlLiquidadorRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarControlLiquidadorSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpActualizarControlLiquidadorSol")
    public JAXBElement<OpActualizarControlLiquidadorSolTipo> createOpActualizarControlLiquidadorSol(OpActualizarControlLiquidadorSolTipo value) {
        return new JAXBElement<OpActualizarControlLiquidadorSolTipo>(_OpActualizarControlLiquidadorSol_QNAME, OpActualizarControlLiquidadorSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlLiquidadorSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorCriteriosControlLiquidadorSol")
    public JAXBElement<OpBuscarPorCriteriosControlLiquidadorSolTipo> createOpBuscarPorCriteriosControlLiquidadorSol(OpBuscarPorCriteriosControlLiquidadorSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlLiquidadorSolTipo>(_OpBuscarPorCriteriosControlLiquidadorSol_QNAME, OpBuscarPorCriteriosControlLiquidadorSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorCriteriosControlLiquidadorFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosControlLiquidadorFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosControlLiquidadorFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdControlLiquidadorSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorIdControlLiquidadorSol")
    public JAXBElement<OpBuscarPorIdControlLiquidadorSolTipo> createOpBuscarPorIdControlLiquidadorSol(OpBuscarPorIdControlLiquidadorSolTipo value) {
        return new JAXBElement<OpBuscarPorIdControlLiquidadorSolTipo>(_OpBuscarPorIdControlLiquidadorSol_QNAME, OpBuscarPorIdControlLiquidadorSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearControlLiquidadorSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpCrearControlLiquidadorSol")
    public JAXBElement<OpCrearControlLiquidadorSolTipo> createOpCrearControlLiquidadorSol(OpCrearControlLiquidadorSolTipo value) {
        return new JAXBElement<OpCrearControlLiquidadorSolTipo>(_OpCrearControlLiquidadorSol_QNAME, OpCrearControlLiquidadorSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosControlLiquidadorRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorCriteriosControlLiquidadorResp")
    public JAXBElement<OpBuscarPorCriteriosControlLiquidadorRespTipo> createOpBuscarPorCriteriosControlLiquidadorResp(OpBuscarPorCriteriosControlLiquidadorRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosControlLiquidadorRespTipo>(_OpBuscarPorCriteriosControlLiquidadorResp_QNAME, OpBuscarPorCriteriosControlLiquidadorRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpActualizarControlLiquidadorFallo")
    public JAXBElement<FalloTipo> createOpActualizarControlLiquidadorFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarControlLiquidadorFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", name = "OpBuscarPorIdControlLiquidadorFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdControlLiquidadorFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdControlLiquidadorFallo_QNAME, FalloTipo.class, null, value);
    }

}
