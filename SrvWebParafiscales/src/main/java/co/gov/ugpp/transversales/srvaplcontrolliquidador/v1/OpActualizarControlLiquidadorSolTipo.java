
package co.gov.ugpp.transversales.srvaplcontrolliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.controlliquidadortipo.v1.ControlLiquidadorTipo;


/**
 * <p>Clase Java para OpActualizarControlLiquidadorSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarControlLiquidadorSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="controlLiquidador" type="{http://www.ugpp.gov.co/schema/Transversales/ControlLiquidadorTipo/v1}ControlLiquidadorTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarControlLiquidadorSolTipo", propOrder = {
    "contextoTransaccional",
    "controlLiquidador"
})
public class OpActualizarControlLiquidadorSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ControlLiquidadorTipo controlLiquidador;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad controlLiquidador.
     * 
     * @return
     *     possible object is
     *     {@link ControlLiquidadorTipo }
     *     
     */
    public ControlLiquidadorTipo getControlLiquidador() {
        return controlLiquidador;
    }

    /**
     * Define el valor de la propiedad controlLiquidador.
     * 
     * @param value
     *     allowed object is
     *     {@link ControlLiquidadorTipo }
     *     
     */
    public void setControlLiquidador(ControlLiquidadorTipo value) {
        this.controlLiquidador = value;
    }

}
