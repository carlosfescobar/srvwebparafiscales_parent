
package co.gov.ugpp.transversales.srvaplcontrolliquidador.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplControlLiquidador", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplControlLiquidador?wsdl")
public class SrvAplControlLiquidador
    extends Service
{

    private final static URL SRVAPLCONTROLLIQUIDADOR_WSDL_LOCATION;
    private final static WebServiceException SRVAPLCONTROLLIQUIDADOR_EXCEPTION;
    private final static QName SRVAPLCONTROLLIQUIDADOR_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "SrvAplControlLiquidador");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplControlLiquidador?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLCONTROLLIQUIDADOR_WSDL_LOCATION = url;
        SRVAPLCONTROLLIQUIDADOR_EXCEPTION = e;
    }

    public SrvAplControlLiquidador() {
        super(__getWsdlLocation(), SRVAPLCONTROLLIQUIDADOR_QNAME);
    }

    public SrvAplControlLiquidador(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLCONTROLLIQUIDADOR_QNAME, features);
    }

    public SrvAplControlLiquidador(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLCONTROLLIQUIDADOR_QNAME);
    }

    public SrvAplControlLiquidador(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLCONTROLLIQUIDADOR_QNAME, features);
    }

    public SrvAplControlLiquidador(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplControlLiquidador(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplControlLiquidadorSOAP
     */
    @WebEndpoint(name = "portSrvAplControlLiquidadorSOAP")
    public PortSrvAplControlLiquidadorSOAP getPortSrvAplControlLiquidadorSOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "portSrvAplControlLiquidadorSOAP"), PortSrvAplControlLiquidadorSOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplControlLiquidadorSOAP
     */
    @WebEndpoint(name = "portSrvAplControlLiquidadorSOAP")
    public PortSrvAplControlLiquidadorSOAP getPortSrvAplControlLiquidadorSOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Transversales/SrvAplControlLiquidador/v1", "portSrvAplControlLiquidadorSOAP"), PortSrvAplControlLiquidadorSOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLCONTROLLIQUIDADOR_EXCEPTION!= null) {
            throw SRVAPLCONTROLLIQUIDADOR_EXCEPTION;
        }
        return SRVAPLCONTROLLIQUIDADOR_WSDL_LOCATION;
    }

}
