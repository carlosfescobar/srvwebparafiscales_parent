
package co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCruzarConciliacionContableFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpCruzarConciliacionContableFallo");
    private final static QName _OpActualizarHallazgosConciliacionContableNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpActualizarHallazgosConciliacionContableNominaFallo");
    private final static QName _OpCruzarConciliacionContableSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpCruzarConciliacionContableSol");
    private final static QName _OpCruzarConciliacionContableResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpCruzarConciliacionContableResp");
    private final static QName _OpActualizarHallazgosConciliacionContableNominaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpActualizarHallazgosConciliacionContableNominaSol");
    private final static QName _OpActualizarHallazgosConciliacionContableNominaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", "OpActualizarHallazgosConciliacionContableNominaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplcruceconciliacioncontablenomina.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarHallazgosConciliacionContableNominaRespTipo }
     * 
     */
    public OpActualizarHallazgosConciliacionContableNominaRespTipo createOpActualizarHallazgosConciliacionContableNominaRespTipo() {
        return new OpActualizarHallazgosConciliacionContableNominaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarHallazgosConciliacionContableNominaSolTipo }
     * 
     */
    public OpActualizarHallazgosConciliacionContableNominaSolTipo createOpActualizarHallazgosConciliacionContableNominaSolTipo() {
        return new OpActualizarHallazgosConciliacionContableNominaSolTipo();
    }

    /**
     * Create an instance of {@link OpCruzarConciliacionContableRespTipo }
     * 
     */
    public OpCruzarConciliacionContableRespTipo createOpCruzarConciliacionContableRespTipo() {
        return new OpCruzarConciliacionContableRespTipo();
    }

    /**
     * Create an instance of {@link OpCruzarConciliacionContableSolTipo }
     * 
     */
    public OpCruzarConciliacionContableSolTipo createOpCruzarConciliacionContableSolTipo() {
        return new OpCruzarConciliacionContableSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpCruzarConciliacionContableFallo")
    public JAXBElement<FalloTipo> createOpCruzarConciliacionContableFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCruzarConciliacionContableFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpActualizarHallazgosConciliacionContableNominaFallo")
    public JAXBElement<FalloTipo> createOpActualizarHallazgosConciliacionContableNominaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarHallazgosConciliacionContableNominaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarConciliacionContableSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpCruzarConciliacionContableSol")
    public JAXBElement<OpCruzarConciliacionContableSolTipo> createOpCruzarConciliacionContableSol(OpCruzarConciliacionContableSolTipo value) {
        return new JAXBElement<OpCruzarConciliacionContableSolTipo>(_OpCruzarConciliacionContableSol_QNAME, OpCruzarConciliacionContableSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarConciliacionContableRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpCruzarConciliacionContableResp")
    public JAXBElement<OpCruzarConciliacionContableRespTipo> createOpCruzarConciliacionContableResp(OpCruzarConciliacionContableRespTipo value) {
        return new JAXBElement<OpCruzarConciliacionContableRespTipo>(_OpCruzarConciliacionContableResp_QNAME, OpCruzarConciliacionContableRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarHallazgosConciliacionContableNominaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpActualizarHallazgosConciliacionContableNominaSol")
    public JAXBElement<OpActualizarHallazgosConciliacionContableNominaSolTipo> createOpActualizarHallazgosConciliacionContableNominaSol(OpActualizarHallazgosConciliacionContableNominaSolTipo value) {
        return new JAXBElement<OpActualizarHallazgosConciliacionContableNominaSolTipo>(_OpActualizarHallazgosConciliacionContableNominaSol_QNAME, OpActualizarHallazgosConciliacionContableNominaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarHallazgosConciliacionContableNominaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceConciliacionContableNomina/v1", name = "OpActualizarHallazgosConciliacionContableNominaResp")
    public JAXBElement<OpActualizarHallazgosConciliacionContableNominaRespTipo> createOpActualizarHallazgosConciliacionContableNominaResp(OpActualizarHallazgosConciliacionContableNominaRespTipo value) {
        return new JAXBElement<OpActualizarHallazgosConciliacionContableNominaRespTipo>(_OpActualizarHallazgosConciliacionContableNominaResp_QNAME, OpActualizarHallazgosConciliacionContableNominaRespTipo.class, null, value);
    }

}
