
package co.gov.ugpp.transversales.srvaplcrucenominapila.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplcrucenominapila.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarHallazgosNominaPilaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpActualizarHallazgosNominaPilaResp");
    private final static QName _OpActualizarHallazgosNominaPilaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpActualizarHallazgosNominaPilaFallo");
    private final static QName _OpCruzarNominaPilaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpCruzarNominaPilaSol");
    private final static QName _OpCruzarNominaPilaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpCruzarNominaPilaResp");
    private final static QName _OpCruzarNominaPilaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpCruzarNominaPilaFallo");
    private final static QName _OpActualizarHallazgosNominaPilaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", "OpActualizarHallazgosNominaPilaSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplcrucenominapila.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpActualizarHallazgosNominaPilaRespTipo }
     * 
     */
    public OpActualizarHallazgosNominaPilaRespTipo createOpActualizarHallazgosNominaPilaRespTipo() {
        return new OpActualizarHallazgosNominaPilaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarHallazgosNominaPilaSolTipo }
     * 
     */
    public OpActualizarHallazgosNominaPilaSolTipo createOpActualizarHallazgosNominaPilaSolTipo() {
        return new OpActualizarHallazgosNominaPilaSolTipo();
    }

    /**
     * Create an instance of {@link OpCruzarNominaPilaRespTipo }
     * 
     */
    public OpCruzarNominaPilaRespTipo createOpCruzarNominaPilaRespTipo() {
        return new OpCruzarNominaPilaRespTipo();
    }

    /**
     * Create an instance of {@link OpCruzarNominaPilaSolTipo }
     * 
     */
    public OpCruzarNominaPilaSolTipo createOpCruzarNominaPilaSolTipo() {
        return new OpCruzarNominaPilaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarHallazgosNominaPilaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpActualizarHallazgosNominaPilaResp")
    public JAXBElement<OpActualizarHallazgosNominaPilaRespTipo> createOpActualizarHallazgosNominaPilaResp(OpActualizarHallazgosNominaPilaRespTipo value) {
        return new JAXBElement<OpActualizarHallazgosNominaPilaRespTipo>(_OpActualizarHallazgosNominaPilaResp_QNAME, OpActualizarHallazgosNominaPilaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpActualizarHallazgosNominaPilaFallo")
    public JAXBElement<FalloTipo> createOpActualizarHallazgosNominaPilaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarHallazgosNominaPilaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarNominaPilaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpCruzarNominaPilaSol")
    public JAXBElement<OpCruzarNominaPilaSolTipo> createOpCruzarNominaPilaSol(OpCruzarNominaPilaSolTipo value) {
        return new JAXBElement<OpCruzarNominaPilaSolTipo>(_OpCruzarNominaPilaSol_QNAME, OpCruzarNominaPilaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarNominaPilaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpCruzarNominaPilaResp")
    public JAXBElement<OpCruzarNominaPilaRespTipo> createOpCruzarNominaPilaResp(OpCruzarNominaPilaRespTipo value) {
        return new JAXBElement<OpCruzarNominaPilaRespTipo>(_OpCruzarNominaPilaResp_QNAME, OpCruzarNominaPilaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpCruzarNominaPilaFallo")
    public JAXBElement<FalloTipo> createOpCruzarNominaPilaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCruzarNominaPilaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarHallazgosNominaPilaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplCruceNominaPila/v1", name = "OpActualizarHallazgosNominaPilaSol")
    public JAXBElement<OpActualizarHallazgosNominaPilaSolTipo> createOpActualizarHallazgosNominaPilaSol(OpActualizarHallazgosNominaPilaSolTipo value) {
        return new JAXBElement<OpActualizarHallazgosNominaPilaSolTipo>(_OpActualizarHallazgosNominaPilaSol_QNAME, OpActualizarHallazgosNominaPilaSolTipo.class, null, value);
    }

}
