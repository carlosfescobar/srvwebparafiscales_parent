
package co.gov.ugpp.transversales.srvapldocumentocore.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvapldocumentocore.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpAsociarExpedientesCORESol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpAsociarExpedientesCORESol");
    private final static QName _OpCrearDocumentoCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpCrearDocumentoCOREFallo");
    private final static QName _OpAsociarExpedientesCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpAsociarExpedientesCOREFallo");
    private final static QName _OpBuscarPorIdDocumentoCOREResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpBuscarPorIdDocumentoCOREResp");
    private final static QName _OpActualizarDocumentoCORESol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpActualizarDocumentoCORESol");
    private final static QName _OpCrearDocumentoCOREResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpCrearDocumentoCOREResp");
    private final static QName _OpBuscarPorIdDocumentoCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpBuscarPorIdDocumentoCOREFallo");
    private final static QName _OpCrearDocumentoCORESol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpCrearDocumentoCORESol");
    private final static QName _OpBuscarPorIdDocumentoCORESol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpBuscarPorIdDocumentoCORESol");
    private final static QName _OpAsociarExpedientesCOREResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpAsociarExpedientesCOREResp");
    private final static QName _OpActualizarDocumentoCOREFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpActualizarDocumentoCOREFallo");
    private final static QName _OpActualizarDocumentoCOREResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "OpActualizarDocumentoCOREResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvapldocumentocore.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpAsociarExpedientesCORERespTipo }
     * 
     */
    public OpAsociarExpedientesCORERespTipo createOpAsociarExpedientesCORERespTipo() {
        return new OpAsociarExpedientesCORERespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDocumentoCORESolTipo }
     * 
     */
    public OpBuscarPorIdDocumentoCORESolTipo createOpBuscarPorIdDocumentoCORESolTipo() {
        return new OpBuscarPorIdDocumentoCORESolTipo();
    }

    /**
     * Create an instance of {@link OpCrearDocumentoCORESolTipo }
     * 
     */
    public OpCrearDocumentoCORESolTipo createOpCrearDocumentoCORESolTipo() {
        return new OpCrearDocumentoCORESolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDocumentoCORERespTipo }
     * 
     */
    public OpActualizarDocumentoCORERespTipo createOpActualizarDocumentoCORERespTipo() {
        return new OpActualizarDocumentoCORERespTipo();
    }

    /**
     * Create an instance of {@link OpCrearDocumentoCORERespTipo }
     * 
     */
    public OpCrearDocumentoCORERespTipo createOpCrearDocumentoCORERespTipo() {
        return new OpCrearDocumentoCORERespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDocumentoCORESolTipo }
     * 
     */
    public OpActualizarDocumentoCORESolTipo createOpActualizarDocumentoCORESolTipo() {
        return new OpActualizarDocumentoCORESolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdDocumentoCORERespTipo }
     * 
     */
    public OpBuscarPorIdDocumentoCORERespTipo createOpBuscarPorIdDocumentoCORERespTipo() {
        return new OpBuscarPorIdDocumentoCORERespTipo();
    }

    /**
     * Create an instance of {@link OpAsociarExpedientesCORESolTipo }
     * 
     */
    public OpAsociarExpedientesCORESolTipo createOpAsociarExpedientesCORESolTipo() {
        return new OpAsociarExpedientesCORESolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAsociarExpedientesCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpAsociarExpedientesCORESol")
    public JAXBElement<OpAsociarExpedientesCORESolTipo> createOpAsociarExpedientesCORESol(OpAsociarExpedientesCORESolTipo value) {
        return new JAXBElement<OpAsociarExpedientesCORESolTipo>(_OpAsociarExpedientesCORESol_QNAME, OpAsociarExpedientesCORESolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpCrearDocumentoCOREFallo")
    public JAXBElement<FalloTipo> createOpCrearDocumentoCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearDocumentoCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpAsociarExpedientesCOREFallo")
    public JAXBElement<FalloTipo> createOpAsociarExpedientesCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpAsociarExpedientesCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDocumentoCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpBuscarPorIdDocumentoCOREResp")
    public JAXBElement<OpBuscarPorIdDocumentoCORERespTipo> createOpBuscarPorIdDocumentoCOREResp(OpBuscarPorIdDocumentoCORERespTipo value) {
        return new JAXBElement<OpBuscarPorIdDocumentoCORERespTipo>(_OpBuscarPorIdDocumentoCOREResp_QNAME, OpBuscarPorIdDocumentoCORERespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDocumentoCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpActualizarDocumentoCORESol")
    public JAXBElement<OpActualizarDocumentoCORESolTipo> createOpActualizarDocumentoCORESol(OpActualizarDocumentoCORESolTipo value) {
        return new JAXBElement<OpActualizarDocumentoCORESolTipo>(_OpActualizarDocumentoCORESol_QNAME, OpActualizarDocumentoCORESolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpCrearDocumentoCOREResp")
    public JAXBElement<OpCrearDocumentoCORERespTipo> createOpCrearDocumentoCOREResp(OpCrearDocumentoCORERespTipo value) {
        return new JAXBElement<OpCrearDocumentoCORERespTipo>(_OpCrearDocumentoCOREResp_QNAME, OpCrearDocumentoCORERespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpBuscarPorIdDocumentoCOREFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdDocumentoCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdDocumentoCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearDocumentoCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpCrearDocumentoCORESol")
    public JAXBElement<OpCrearDocumentoCORESolTipo> createOpCrearDocumentoCORESol(OpCrearDocumentoCORESolTipo value) {
        return new JAXBElement<OpCrearDocumentoCORESolTipo>(_OpCrearDocumentoCORESol_QNAME, OpCrearDocumentoCORESolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdDocumentoCORESolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpBuscarPorIdDocumentoCORESol")
    public JAXBElement<OpBuscarPorIdDocumentoCORESolTipo> createOpBuscarPorIdDocumentoCORESol(OpBuscarPorIdDocumentoCORESolTipo value) {
        return new JAXBElement<OpBuscarPorIdDocumentoCORESolTipo>(_OpBuscarPorIdDocumentoCORESol_QNAME, OpBuscarPorIdDocumentoCORESolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpAsociarExpedientesCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpAsociarExpedientesCOREResp")
    public JAXBElement<OpAsociarExpedientesCORERespTipo> createOpAsociarExpedientesCOREResp(OpAsociarExpedientesCORERespTipo value) {
        return new JAXBElement<OpAsociarExpedientesCORERespTipo>(_OpAsociarExpedientesCOREResp_QNAME, OpAsociarExpedientesCORERespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpActualizarDocumentoCOREFallo")
    public JAXBElement<FalloTipo> createOpActualizarDocumentoCOREFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarDocumentoCOREFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDocumentoCORERespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", name = "OpActualizarDocumentoCOREResp")
    public JAXBElement<OpActualizarDocumentoCORERespTipo> createOpActualizarDocumentoCOREResp(OpActualizarDocumentoCORERespTipo value) {
        return new JAXBElement<OpActualizarDocumentoCORERespTipo>(_OpActualizarDocumentoCOREResp_QNAME, OpActualizarDocumentoCORERespTipo.class, null, value);
    }

}
