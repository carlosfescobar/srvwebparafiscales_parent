
package co.gov.ugpp.transversales.srvapldocumentocore.v1;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "SrvAplDocumentoCORE", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", wsdlLocation = "http://localhost:8080/SrvWebParafiscales/SrvAplDocumentoCORE?wsdl")
public class SrvAplDocumentoCORE
    extends Service
{

    private final static URL SRVAPLDOCUMENTOCORE_WSDL_LOCATION;
    private final static WebServiceException SRVAPLDOCUMENTOCORE_EXCEPTION;
    private final static QName SRVAPLDOCUMENTOCORE_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "SrvAplDocumentoCORE");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/SrvWebParafiscales/SrvAplDocumentoCORE?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        SRVAPLDOCUMENTOCORE_WSDL_LOCATION = url;
        SRVAPLDOCUMENTOCORE_EXCEPTION = e;
    }

    public SrvAplDocumentoCORE() {
        super(__getWsdlLocation(), SRVAPLDOCUMENTOCORE_QNAME);
    }

    public SrvAplDocumentoCORE(WebServiceFeature... features) {
        super(__getWsdlLocation(), SRVAPLDOCUMENTOCORE_QNAME, features);
    }

    public SrvAplDocumentoCORE(URL wsdlLocation) {
        super(wsdlLocation, SRVAPLDOCUMENTOCORE_QNAME);
    }

    public SrvAplDocumentoCORE(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, SRVAPLDOCUMENTOCORE_QNAME, features);
    }

    public SrvAplDocumentoCORE(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SrvAplDocumentoCORE(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PortSrvAplDocumentoCORESOAP
     */
    @WebEndpoint(name = "portSrvAplDocumentoCORESOAP")
    public PortSrvAplDocumentoCORESOAP getPortSrvAplDocumentoCORESOAP() {
        return super.getPort(new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "portSrvAplDocumentoCORESOAP"), PortSrvAplDocumentoCORESOAP.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PortSrvAplDocumentoCORESOAP
     */
    @WebEndpoint(name = "portSrvAplDocumentoCORESOAP")
    public PortSrvAplDocumentoCORESOAP getPortSrvAplDocumentoCORESOAP(WebServiceFeature... features) {
        return super.getPort(new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoCORE/v1", "portSrvAplDocumentoCORESOAP"), PortSrvAplDocumentoCORESOAP.class, features);
    }

    private static URL __getWsdlLocation() {
        if (SRVAPLDOCUMENTOCORE_EXCEPTION!= null) {
            throw SRVAPLDOCUMENTOCORE_EXCEPTION;
        }
        return SRVAPLDOCUMENTOCORE_WSDL_LOCATION;
    }

}
