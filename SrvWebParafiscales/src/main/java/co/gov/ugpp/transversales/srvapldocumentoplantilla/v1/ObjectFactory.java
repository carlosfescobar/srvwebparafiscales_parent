
package co.gov.ugpp.transversales.srvapldocumentoplantilla.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvapldocumentoplantilla.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpLlenarTokensDocumentoPlantillaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", "OpLlenarTokensDocumentoPlantillaFallo");
    private final static QName _OpLlenarTokensDocumentoPlantillaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", "OpLlenarTokensDocumentoPlantillaSol");
    private final static QName _OpLlenarTokensDocumentoPlantillaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", "OpLlenarTokensDocumentoPlantillaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvapldocumentoplantilla.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpLlenarTokensDocumentoPlantillaSolTipo }
     * 
     */
    public OpLlenarTokensDocumentoPlantillaSolTipo createOpLlenarTokensDocumentoPlantillaSolTipo() {
        return new OpLlenarTokensDocumentoPlantillaSolTipo();
    }

    /**
     * Create an instance of {@link OpLlenarTokensDocumentoPlantillaRespTipo }
     * 
     */
    public OpLlenarTokensDocumentoPlantillaRespTipo createOpLlenarTokensDocumentoPlantillaRespTipo() {
        return new OpLlenarTokensDocumentoPlantillaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", name = "OpLlenarTokensDocumentoPlantillaFallo")
    public JAXBElement<FalloTipo> createOpLlenarTokensDocumentoPlantillaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpLlenarTokensDocumentoPlantillaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLlenarTokensDocumentoPlantillaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", name = "OpLlenarTokensDocumentoPlantillaSol")
    public JAXBElement<OpLlenarTokensDocumentoPlantillaSolTipo> createOpLlenarTokensDocumentoPlantillaSol(OpLlenarTokensDocumentoPlantillaSolTipo value) {
        return new JAXBElement<OpLlenarTokensDocumentoPlantillaSolTipo>(_OpLlenarTokensDocumentoPlantillaSol_QNAME, OpLlenarTokensDocumentoPlantillaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpLlenarTokensDocumentoPlantillaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplDocumentoPlantilla/v1", name = "OpLlenarTokensDocumentoPlantillaResp")
    public JAXBElement<OpLlenarTokensDocumentoPlantillaRespTipo> createOpLlenarTokensDocumentoPlantillaResp(OpLlenarTokensDocumentoPlantillaRespTipo value) {
        return new JAXBElement<OpLlenarTokensDocumentoPlantillaRespTipo>(_OpLlenarTokensDocumentoPlantillaResp_QNAME, OpLlenarTokensDocumentoPlantillaRespTipo.class, null, value);
    }

}
