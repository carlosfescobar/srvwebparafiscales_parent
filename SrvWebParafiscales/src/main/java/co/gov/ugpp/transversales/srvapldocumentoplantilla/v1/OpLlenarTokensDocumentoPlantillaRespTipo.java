
package co.gov.ugpp.transversales.srvapldocumentoplantilla.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.transversales.documentoplantillatipo.v1.DocumentoPlantillaTipo;


/**
 * <p>Clase Java para OpLlenarTokensDocumentoPlantillaRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpLlenarTokensDocumentoPlantillaRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="documentoPlantilla" type="{http://www.ugpp.gov.co/schema/Transversales/DocumentoPlantillaTipo/v1}DocumentoPlantillaTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpLlenarTokensDocumentoPlantillaRespTipo", propOrder = {
    "contextoRespuesta",
    "documentoPlantilla"
})
public class OpLlenarTokensDocumentoPlantillaRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(required = true)
    protected DocumentoPlantillaTipo documentoPlantilla;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad documentoPlantilla.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoPlantillaTipo }
     *     
     */
    public DocumentoPlantillaTipo getDocumentoPlantilla() {
        return documentoPlantilla;
    }

    /**
     * Define el valor de la propiedad documentoPlantilla.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoPlantillaTipo }
     *     
     */
    public void setDocumentoPlantilla(DocumentoPlantillaTipo value) {
        this.documentoPlantilla = value;
    }

}
