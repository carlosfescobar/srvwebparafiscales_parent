
package co.gov.ugpp.transversales.srvaplentidadnegocio.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplentidadnegocio.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarEntidadNegocioResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpActualizarEntidadNegocioResp");
    private final static QName _OpActualizarEntidadNegocioSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpActualizarEntidadNegocioSol");
    private final static QName _OpActualizarEntidadNegocioFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpActualizarEntidadNegocioFallo");
    private final static QName _OpBuscarPorCriteriosEntidadNegocioResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpBuscarPorCriteriosEntidadNegocioResp");
    private final static QName _OpBuscarPorCriteriosEntidadNegocioFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpBuscarPorCriteriosEntidadNegocioFallo");
    private final static QName _OpCrearEntidadNegocioResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpCrearEntidadNegocioResp");
    private final static QName _OpCrearEntidadNegocioFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpCrearEntidadNegocioFallo");
    private final static QName _OpBuscarPorCriteriosEntidadNegocioSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpBuscarPorCriteriosEntidadNegocioSol");
    private final static QName _OpCrearEntidadNegocioSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", "OpCrearEntidadNegocioSol");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplentidadnegocio.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosEntidadNegocioRespTipo }
     * 
     */
    public OpBuscarPorCriteriosEntidadNegocioRespTipo createOpBuscarPorCriteriosEntidadNegocioRespTipo() {
        return new OpBuscarPorCriteriosEntidadNegocioRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarEntidadNegocioSolTipo }
     * 
     */
    public OpActualizarEntidadNegocioSolTipo createOpActualizarEntidadNegocioSolTipo() {
        return new OpActualizarEntidadNegocioSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarEntidadNegocioRespTipo }
     * 
     */
    public OpActualizarEntidadNegocioRespTipo createOpActualizarEntidadNegocioRespTipo() {
        return new OpActualizarEntidadNegocioRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosEntidadNegocioSolTipo }
     * 
     */
    public OpBuscarPorCriteriosEntidadNegocioSolTipo createOpBuscarPorCriteriosEntidadNegocioSolTipo() {
        return new OpBuscarPorCriteriosEntidadNegocioSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearEntidadNegocioSolTipo }
     * 
     */
    public OpCrearEntidadNegocioSolTipo createOpCrearEntidadNegocioSolTipo() {
        return new OpCrearEntidadNegocioSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearEntidadNegocioRespTipo }
     * 
     */
    public OpCrearEntidadNegocioRespTipo createOpCrearEntidadNegocioRespTipo() {
        return new OpCrearEntidadNegocioRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarEntidadNegocioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpActualizarEntidadNegocioResp")
    public JAXBElement<OpActualizarEntidadNegocioRespTipo> createOpActualizarEntidadNegocioResp(OpActualizarEntidadNegocioRespTipo value) {
        return new JAXBElement<OpActualizarEntidadNegocioRespTipo>(_OpActualizarEntidadNegocioResp_QNAME, OpActualizarEntidadNegocioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarEntidadNegocioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpActualizarEntidadNegocioSol")
    public JAXBElement<OpActualizarEntidadNegocioSolTipo> createOpActualizarEntidadNegocioSol(OpActualizarEntidadNegocioSolTipo value) {
        return new JAXBElement<OpActualizarEntidadNegocioSolTipo>(_OpActualizarEntidadNegocioSol_QNAME, OpActualizarEntidadNegocioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpActualizarEntidadNegocioFallo")
    public JAXBElement<FalloTipo> createOpActualizarEntidadNegocioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarEntidadNegocioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosEntidadNegocioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpBuscarPorCriteriosEntidadNegocioResp")
    public JAXBElement<OpBuscarPorCriteriosEntidadNegocioRespTipo> createOpBuscarPorCriteriosEntidadNegocioResp(OpBuscarPorCriteriosEntidadNegocioRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosEntidadNegocioRespTipo>(_OpBuscarPorCriteriosEntidadNegocioResp_QNAME, OpBuscarPorCriteriosEntidadNegocioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpBuscarPorCriteriosEntidadNegocioFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosEntidadNegocioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosEntidadNegocioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearEntidadNegocioRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpCrearEntidadNegocioResp")
    public JAXBElement<OpCrearEntidadNegocioRespTipo> createOpCrearEntidadNegocioResp(OpCrearEntidadNegocioRespTipo value) {
        return new JAXBElement<OpCrearEntidadNegocioRespTipo>(_OpCrearEntidadNegocioResp_QNAME, OpCrearEntidadNegocioRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpCrearEntidadNegocioFallo")
    public JAXBElement<FalloTipo> createOpCrearEntidadNegocioFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearEntidadNegocioFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosEntidadNegocioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpBuscarPorCriteriosEntidadNegocioSol")
    public JAXBElement<OpBuscarPorCriteriosEntidadNegocioSolTipo> createOpBuscarPorCriteriosEntidadNegocioSol(OpBuscarPorCriteriosEntidadNegocioSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosEntidadNegocioSolTipo>(_OpBuscarPorCriteriosEntidadNegocioSol_QNAME, OpBuscarPorCriteriosEntidadNegocioSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearEntidadNegocioSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", name = "OpCrearEntidadNegocioSol")
    public JAXBElement<OpCrearEntidadNegocioSolTipo> createOpCrearEntidadNegocioSol(OpCrearEntidadNegocioSolTipo value) {
        return new JAXBElement<OpCrearEntidadNegocioSolTipo>(_OpCrearEntidadNegocioSol_QNAME, OpCrearEntidadNegocioSolTipo.class, null, value);
    }

}
