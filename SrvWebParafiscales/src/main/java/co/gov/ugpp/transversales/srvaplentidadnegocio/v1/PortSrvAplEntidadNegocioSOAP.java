
package co.gov.ugpp.transversales.srvaplentidadnegocio.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplEntidadNegocioSOAP", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.parametrotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.entidadnegociotipo.v1.ObjectFactory.class,
    co.gov.ugpp.transversales.srvaplentidadnegocio.v1.ObjectFactory.class
})
public interface PortSrvAplEntidadNegocioSOAP {


    /**
     * 
     * @param msjOpCrearEntidadNegocioSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpCrearEntidadNegocioRespTipo
     * @throws MsjOpCrearEntidadNegocioFallo
     */
    @WebMethod(operationName = "OpCrearEntidadNegocio", action = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1/OpCrearEntidadNegocio")
    @WebResult(name = "OpCrearEntidadNegocioResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpCrearEntidadNegocioResp")
    public OpCrearEntidadNegocioRespTipo opCrearEntidadNegocio(
        @WebParam(name = "OpCrearEntidadNegocioSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpCrearEntidadNegocioSol")
        OpCrearEntidadNegocioSolTipo msjOpCrearEntidadNegocioSol)
        throws MsjOpCrearEntidadNegocioFallo
    ;

    /**
     * 
     * @param msjOpActualizarEntidadNegocioSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpActualizarEntidadNegocioRespTipo
     * @throws MsjOpActualizarEntidadNegocioFallo
     */
    @WebMethod(operationName = "OpActualizarEntidadNegocio", action = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1/OpActualizarEntidadNegocio")
    @WebResult(name = "OpActualizarEntidadNegocioResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpActualizarEntidadNegocioResp")
    public OpActualizarEntidadNegocioRespTipo opActualizarEntidadNegocio(
        @WebParam(name = "OpActualizarEntidadNegocioSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpActualizarEntidadNegocioSol")
        OpActualizarEntidadNegocioSolTipo msjOpActualizarEntidadNegocioSol)
        throws MsjOpActualizarEntidadNegocioFallo
    ;

    /**
     * 
     * @param msjOpBuscarPorCriteriosEntidadNegocioSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplentidadnegocio.v1.OpBuscarPorCriteriosEntidadNegocioRespTipo
     * @throws MsjOpBuscarPorCriteriosEntidadNegocioFallo
     */
    @WebMethod(operationName = "OpBuscarPorCriteriosEntidadNegocio", action = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1/OpBuscarPorCriteriosEntidadNegocio")
    @WebResult(name = "OpBuscarPorCriteriosEntidadNegocioResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpBuscarPorCriteriosEntidadNegocioResp")
    public OpBuscarPorCriteriosEntidadNegocioRespTipo opBuscarPorCriteriosEntidadNegocio(
        @WebParam(name = "OpBuscarPorCriteriosEntidadNegocioSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplEntidadNegocio/v1", partName = "msjOpBuscarPorCriteriosEntidadNegocioSol")
        OpBuscarPorCriteriosEntidadNegocioSolTipo msjOpBuscarPorCriteriosEntidadNegocioSol)
        throws MsjOpBuscarPorCriteriosEntidadNegocioFallo
    ;

}
