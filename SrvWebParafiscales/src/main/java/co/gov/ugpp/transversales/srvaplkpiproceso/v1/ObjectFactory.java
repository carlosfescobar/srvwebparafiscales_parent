
package co.gov.ugpp.transversales.srvaplkpiproceso.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplkpiproceso.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpConsultarSolicitudKPIFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", "OpConsultarSolicitudKPIFallo");
    private final static QName _OpConsultarKPIProcesoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", "OpConsultarKPIProcesoSol");
    private final static QName _OpConsultarKPIProcesoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", "OpConsultarKPIProcesoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplkpiproceso.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpConsultarKPIProcesoSolTipo }
     * 
     */
    public OpConsultarKPIProcesoSolTipo createOpConsultarKPIProcesoSolTipo() {
        return new OpConsultarKPIProcesoSolTipo();
    }

    /**
     * Create an instance of {@link OpConsultarKPIProcesoRespTipo }
     * 
     */
    public OpConsultarKPIProcesoRespTipo createOpConsultarKPIProcesoRespTipo() {
        return new OpConsultarKPIProcesoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", name = "OpConsultarSolicitudKPIFallo")
    public JAXBElement<FalloTipo> createOpConsultarSolicitudKPIFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpConsultarSolicitudKPIFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarKPIProcesoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", name = "OpConsultarKPIProcesoSol")
    public JAXBElement<OpConsultarKPIProcesoSolTipo> createOpConsultarKPIProcesoSol(OpConsultarKPIProcesoSolTipo value) {
        return new JAXBElement<OpConsultarKPIProcesoSolTipo>(_OpConsultarKPIProcesoSol_QNAME, OpConsultarKPIProcesoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpConsultarKPIProcesoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", name = "OpConsultarKPIProcesoResp")
    public JAXBElement<OpConsultarKPIProcesoRespTipo> createOpConsultarKPIProcesoResp(OpConsultarKPIProcesoRespTipo value) {
        return new JAXBElement<OpConsultarKPIProcesoRespTipo>(_OpConsultarKPIProcesoResp_QNAME, OpConsultarKPIProcesoRespTipo.class, null, value);
    }

}
