
package co.gov.ugpp.transversales.srvaplkpiproceso.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplKPIProcesoSOAP", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.parametrotipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.parametrovalorestipo.v1.ObjectFactory.class,
    co.gov.ugpp.transversales.srvaplkpiproceso.v1.ObjectFactory.class
})
public interface PortSrvAplKPIProcesoSOAP {


    /**
     * 
     * @param msjOpConsultarKPIProcesoSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplkpiproceso.v1.OpConsultarKPIProcesoRespTipo
     * @throws MsjOpConsultarKPIProcesoFallo
     */
    @WebMethod(operationName = "OpConsultarKPIProceso", action = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1/OpConsultarKPIProceso")
    @WebResult(name = "OpConsultarKPIProcesoResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", partName = "msjOpConsultarKPIProcesoResp")
    public OpConsultarKPIProcesoRespTipo opConsultarKPIProceso(
        @WebParam(name = "OpConsultarKPIProcesoSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplKPIProceso/v1", partName = "msjOpConsultarKPIProcesoSol")
        OpConsultarKPIProcesoSolTipo msjOpConsultarKPIProcesoSol)
        throws MsjOpConsultarKPIProcesoFallo
    ;

}
