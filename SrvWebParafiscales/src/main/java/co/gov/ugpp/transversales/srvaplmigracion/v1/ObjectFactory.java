
package co.gov.ugpp.transversales.srvaplmigracion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplmigracion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorCriteriosCasoMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosCasoMigracionSol");
    private final static QName _OpBuscarPorCriteriosMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosMigracionFallo");
    private final static QName _OpBuscarPorCriteriosMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosMigracionResp");
    private final static QName _OpActualizarMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarMigracionSol");
    private final static QName _OpActualizarMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarMigracionFallo");
    private final static QName _OpBuscarPorCriteriosMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosMigracionSol");
    private final static QName _OpActualizarCasoMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarCasoMigracionSol");
    private final static QName _OpBuscarPorCriteriosCasoMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosCasoMigracionResp");
    private final static QName _OpCrearMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearMigracionSol");
    private final static QName _OpCrearCasoMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearCasoMigracionFallo");
    private final static QName _OpCrearMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearMigracionResp");
    private final static QName _OpActualizarCasoMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarCasoMigracionResp");
    private final static QName _OpActualizarMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarMigracionResp");
    private final static QName _OpCrearCasoMigracionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearCasoMigracionResp");
    private final static QName _OpBuscarPorCriteriosCasoMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpBuscarPorCriteriosCasoMigracionFallo");
    private final static QName _OpActualizarCasoMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpActualizarCasoMigracionFallo");
    private final static QName _OpCrearCasoMigracionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearCasoMigracionSol");
    private final static QName _OpCrearMigracionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", "OpCrearMigracionFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplmigracion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosCasoMigracionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosCasoMigracionRespTipo createOpBuscarPorCriteriosCasoMigracionRespTipo() {
        return new OpBuscarPorCriteriosCasoMigracionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearMigracionSolTipo }
     * 
     */
    public OpCrearMigracionSolTipo createOpCrearMigracionSolTipo() {
        return new OpCrearMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarMigracionSolTipo }
     * 
     */
    public OpActualizarMigracionSolTipo createOpActualizarMigracionSolTipo() {
        return new OpActualizarMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosMigracionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosMigracionSolTipo createOpBuscarPorCriteriosMigracionSolTipo() {
        return new OpBuscarPorCriteriosMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarCasoMigracionSolTipo }
     * 
     */
    public OpActualizarCasoMigracionSolTipo createOpActualizarCasoMigracionSolTipo() {
        return new OpActualizarCasoMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosMigracionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosMigracionRespTipo createOpBuscarPorCriteriosMigracionRespTipo() {
        return new OpBuscarPorCriteriosMigracionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosCasoMigracionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosCasoMigracionSolTipo createOpBuscarPorCriteriosCasoMigracionSolTipo() {
        return new OpBuscarPorCriteriosCasoMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearCasoMigracionSolTipo }
     * 
     */
    public OpCrearCasoMigracionSolTipo createOpCrearCasoMigracionSolTipo() {
        return new OpCrearCasoMigracionSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearCasoMigracionRespTipo }
     * 
     */
    public OpCrearCasoMigracionRespTipo createOpCrearCasoMigracionRespTipo() {
        return new OpCrearCasoMigracionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarMigracionRespTipo }
     * 
     */
    public OpActualizarMigracionRespTipo createOpActualizarMigracionRespTipo() {
        return new OpActualizarMigracionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarCasoMigracionRespTipo }
     * 
     */
    public OpActualizarCasoMigracionRespTipo createOpActualizarCasoMigracionRespTipo() {
        return new OpActualizarCasoMigracionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearMigracionRespTipo }
     * 
     */
    public OpCrearMigracionRespTipo createOpCrearMigracionRespTipo() {
        return new OpCrearMigracionRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosCasoMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosCasoMigracionSol")
    public JAXBElement<OpBuscarPorCriteriosCasoMigracionSolTipo> createOpBuscarPorCriteriosCasoMigracionSol(OpBuscarPorCriteriosCasoMigracionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosCasoMigracionSolTipo>(_OpBuscarPorCriteriosCasoMigracionSol_QNAME, OpBuscarPorCriteriosCasoMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosMigracionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosMigracionResp")
    public JAXBElement<OpBuscarPorCriteriosMigracionRespTipo> createOpBuscarPorCriteriosMigracionResp(OpBuscarPorCriteriosMigracionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosMigracionRespTipo>(_OpBuscarPorCriteriosMigracionResp_QNAME, OpBuscarPorCriteriosMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarMigracionSol")
    public JAXBElement<OpActualizarMigracionSolTipo> createOpActualizarMigracionSol(OpActualizarMigracionSolTipo value) {
        return new JAXBElement<OpActualizarMigracionSolTipo>(_OpActualizarMigracionSol_QNAME, OpActualizarMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarMigracionFallo")
    public JAXBElement<FalloTipo> createOpActualizarMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosMigracionSol")
    public JAXBElement<OpBuscarPorCriteriosMigracionSolTipo> createOpBuscarPorCriteriosMigracionSol(OpBuscarPorCriteriosMigracionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosMigracionSolTipo>(_OpBuscarPorCriteriosMigracionSol_QNAME, OpBuscarPorCriteriosMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarCasoMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarCasoMigracionSol")
    public JAXBElement<OpActualizarCasoMigracionSolTipo> createOpActualizarCasoMigracionSol(OpActualizarCasoMigracionSolTipo value) {
        return new JAXBElement<OpActualizarCasoMigracionSolTipo>(_OpActualizarCasoMigracionSol_QNAME, OpActualizarCasoMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosCasoMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosCasoMigracionResp")
    public JAXBElement<OpBuscarPorCriteriosCasoMigracionRespTipo> createOpBuscarPorCriteriosCasoMigracionResp(OpBuscarPorCriteriosCasoMigracionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosCasoMigracionRespTipo>(_OpBuscarPorCriteriosCasoMigracionResp_QNAME, OpBuscarPorCriteriosCasoMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearMigracionSol")
    public JAXBElement<OpCrearMigracionSolTipo> createOpCrearMigracionSol(OpCrearMigracionSolTipo value) {
        return new JAXBElement<OpCrearMigracionSolTipo>(_OpCrearMigracionSol_QNAME, OpCrearMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearCasoMigracionFallo")
    public JAXBElement<FalloTipo> createOpCrearCasoMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearCasoMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearMigracionResp")
    public JAXBElement<OpCrearMigracionRespTipo> createOpCrearMigracionResp(OpCrearMigracionRespTipo value) {
        return new JAXBElement<OpCrearMigracionRespTipo>(_OpCrearMigracionResp_QNAME, OpCrearMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarCasoMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarCasoMigracionResp")
    public JAXBElement<OpActualizarCasoMigracionRespTipo> createOpActualizarCasoMigracionResp(OpActualizarCasoMigracionRespTipo value) {
        return new JAXBElement<OpActualizarCasoMigracionRespTipo>(_OpActualizarCasoMigracionResp_QNAME, OpActualizarCasoMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarMigracionResp")
    public JAXBElement<OpActualizarMigracionRespTipo> createOpActualizarMigracionResp(OpActualizarMigracionRespTipo value) {
        return new JAXBElement<OpActualizarMigracionRespTipo>(_OpActualizarMigracionResp_QNAME, OpActualizarMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearCasoMigracionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearCasoMigracionResp")
    public JAXBElement<OpCrearCasoMigracionRespTipo> createOpCrearCasoMigracionResp(OpCrearCasoMigracionRespTipo value) {
        return new JAXBElement<OpCrearCasoMigracionRespTipo>(_OpCrearCasoMigracionResp_QNAME, OpCrearCasoMigracionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpBuscarPorCriteriosCasoMigracionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosCasoMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosCasoMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpActualizarCasoMigracionFallo")
    public JAXBElement<FalloTipo> createOpActualizarCasoMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarCasoMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearCasoMigracionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearCasoMigracionSol")
    public JAXBElement<OpCrearCasoMigracionSolTipo> createOpCrearCasoMigracionSol(OpCrearCasoMigracionSolTipo value) {
        return new JAXBElement<OpCrearCasoMigracionSolTipo>(_OpCrearCasoMigracionSol_QNAME, OpCrearCasoMigracionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplMigracion/v1", name = "OpCrearMigracionFallo")
    public JAXBElement<FalloTipo> createOpCrearMigracionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearMigracionFallo_QNAME, FalloTipo.class, null, value);
    }

}
