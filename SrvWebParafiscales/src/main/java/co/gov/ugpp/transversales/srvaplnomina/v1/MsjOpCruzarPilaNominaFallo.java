
package co.gov.ugpp.transversales.srvaplnomina.v1;

import javax.xml.ws.WebFault;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "OpCruzarPilaNominaFallo", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1")
public class MsjOpCruzarPilaNominaFallo
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private FalloTipo faultInfo;

    /**
     * 
     * @param message
     * @param faultInfo
     */
    public MsjOpCruzarPilaNominaFallo(String message, FalloTipo faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param message
     * @param faultInfo
     * @param cause
     */
    public MsjOpCruzarPilaNominaFallo(String message, FalloTipo faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo
     */
    public FalloTipo getFaultInfo() {
        return faultInfo;
    }

}
