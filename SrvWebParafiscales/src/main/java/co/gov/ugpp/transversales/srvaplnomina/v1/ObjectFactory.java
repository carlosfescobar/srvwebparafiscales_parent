
package co.gov.ugpp.transversales.srvaplnomina.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplnomina.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCruzarPilaNominaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", "OpCruzarPilaNominaFallo");
    private final static QName _OpCruzarPilaNominaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", "OpCruzarPilaNominaSol");
    private final static QName _OpCruzarPilaNominaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", "OpCruzarPilaNominaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplnomina.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCruzarPilaNominaRespTipo }
     * 
     */
    public OpCruzarPilaNominaRespTipo createOpCruzarPilaNominaRespTipo() {
        return new OpCruzarPilaNominaRespTipo();
    }

    /**
     * Create an instance of {@link OpCruzarPilaNominaSolTipo }
     * 
     */
    public OpCruzarPilaNominaSolTipo createOpCruzarPilaNominaSolTipo() {
        return new OpCruzarPilaNominaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", name = "OpCruzarPilaNominaFallo")
    public JAXBElement<FalloTipo> createOpCruzarPilaNominaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCruzarPilaNominaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarPilaNominaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", name = "OpCruzarPilaNominaSol")
    public JAXBElement<OpCruzarPilaNominaSolTipo> createOpCruzarPilaNominaSol(OpCruzarPilaNominaSolTipo value) {
        return new JAXBElement<OpCruzarPilaNominaSolTipo>(_OpCruzarPilaNominaSol_QNAME, OpCruzarPilaNominaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCruzarPilaNominaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNomina/v1", name = "OpCruzarPilaNominaResp")
    public JAXBElement<OpCruzarPilaNominaRespTipo> createOpCruzarPilaNominaResp(OpCruzarPilaNominaRespTipo value) {
        return new JAXBElement<OpCruzarPilaNominaRespTipo>(_OpCruzarPilaNominaResp_QNAME, OpCruzarPilaNominaRespTipo.class, null, value);
    }

}
