
package co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpGenerarNumeroActoAdministrativoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", "OpGenerarNumeroActoAdministrativoSol");
    private final static QName _OpGenerarNumeroActoAdministrativoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", "OpGenerarNumeroActoAdministrativoFallo");
    private final static QName _OpGenerarNumeroActoAdministrativoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", "OpGenerarNumeroActoAdministrativoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpGenerarNumeroActoAdministrativoSolTipo }
     * 
     */
    public OpGenerarNumeroActoAdministrativoSolTipo createOpGenerarNumeroActoAdministrativoSolTipo() {
        return new OpGenerarNumeroActoAdministrativoSolTipo();
    }

    /**
     * Create an instance of {@link OpGenerarNumeroActoAdministrativoRespTipo }
     * 
     */
    public OpGenerarNumeroActoAdministrativoRespTipo createOpGenerarNumeroActoAdministrativoRespTipo() {
        return new OpGenerarNumeroActoAdministrativoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpGenerarNumeroActoAdministrativoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", name = "OpGenerarNumeroActoAdministrativoSol")
    public JAXBElement<OpGenerarNumeroActoAdministrativoSolTipo> createOpGenerarNumeroActoAdministrativoSol(OpGenerarNumeroActoAdministrativoSolTipo value) {
        return new JAXBElement<OpGenerarNumeroActoAdministrativoSolTipo>(_OpGenerarNumeroActoAdministrativoSol_QNAME, OpGenerarNumeroActoAdministrativoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", name = "OpGenerarNumeroActoAdministrativoFallo")
    public JAXBElement<FalloTipo> createOpGenerarNumeroActoAdministrativoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpGenerarNumeroActoAdministrativoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpGenerarNumeroActoAdministrativoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", name = "OpGenerarNumeroActoAdministrativoResp")
    public JAXBElement<OpGenerarNumeroActoAdministrativoRespTipo> createOpGenerarNumeroActoAdministrativoResp(OpGenerarNumeroActoAdministrativoRespTipo value) {
        return new JAXBElement<OpGenerarNumeroActoAdministrativoRespTipo>(_OpGenerarNumeroActoAdministrativoResp_QNAME, OpGenerarNumeroActoAdministrativoRespTipo.class, null, value);
    }

}
