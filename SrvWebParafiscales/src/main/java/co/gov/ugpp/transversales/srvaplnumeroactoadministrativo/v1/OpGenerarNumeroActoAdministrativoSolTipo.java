
package co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1.NumeroActoAdministrativoTipo;


/**
 * <p>Clase Java para OpGenerarNumeroActoAdministrativoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpGenerarNumeroActoAdministrativoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="numeroActoAdministrativo" type="{http://www.ugpp.gov.co/schema/Transversales/NumeroActoAdministrativoTipo/v1}NumeroActoAdministrativoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpGenerarNumeroActoAdministrativoSolTipo", propOrder = {
    "contextoTransaccional",
    "numeroActoAdministrativo"
})
public class OpGenerarNumeroActoAdministrativoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected NumeroActoAdministrativoTipo numeroActoAdministrativo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad numeroActoAdministrativo.
     * 
     * @return
     *     possible object is
     *     {@link NumeroActoAdministrativoTipo }
     *     
     */
    public NumeroActoAdministrativoTipo getNumeroActoAdministrativo() {
        return numeroActoAdministrativo;
    }

    /**
     * Define el valor de la propiedad numeroActoAdministrativo.
     * 
     * @param value
     *     allowed object is
     *     {@link NumeroActoAdministrativoTipo }
     *     
     */
    public void setNumeroActoAdministrativo(NumeroActoAdministrativoTipo value) {
        this.numeroActoAdministrativo = value;
    }

}
