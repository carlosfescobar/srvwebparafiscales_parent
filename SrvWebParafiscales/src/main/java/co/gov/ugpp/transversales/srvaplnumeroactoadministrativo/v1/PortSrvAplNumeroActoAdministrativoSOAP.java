
package co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.8
 * Generated source version: 2.2
 * 
 */
@WebService(name = "portSrvAplNumeroActoAdministrativoSOAP", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.criterioordenamientotipo.ObjectFactory.class,
    co.gov.ugpp.esb.schema.errortipo.v1.ObjectFactory.class,
    co.gov.ugpp.esb.schema.fallotipo.v1.ObjectFactory.class,
    co.gov.ugpp.schema.transversales.numeroactoadministrativotipo.v1.ObjectFactory.class,
    co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.ObjectFactory.class
})
public interface PortSrvAplNumeroActoAdministrativoSOAP {


    /**
     * 
     * @param msjOpGenerarNumeroActoAdministrativoSol
     * @return
     *     returns co.gov.ugpp.transversales.srvaplnumeroactoadministrativo.v1.OpGenerarNumeroActoAdministrativoRespTipo
     * @throws MsjOpGenerarNumeroActoAdministrativoFallo
     */
    @WebMethod(operationName = "OpGenerarNumeroActoAdministrativo", action = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1/OpGenerarNumeroActoAdministrativo")
    @WebResult(name = "OpGenerarNumeroActoAdministrativoResp", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", partName = "msjOpGenerarNumeroActoAdministrativoResp")
    public OpGenerarNumeroActoAdministrativoRespTipo opGenerarNumeroActoAdministrativo(
        @WebParam(name = "OpGenerarNumeroActoAdministrativoSol", targetNamespace = "http://www.ugpp.gov.co/Transversales/SrvAplNumeroActoAdministrativo/v1", partName = "msjOpGenerarNumeroActoAdministrativoSol")
        OpGenerarNumeroActoAdministrativoSolTipo msjOpGenerarNumeroActoAdministrativoSol)
        throws MsjOpGenerarNumeroActoAdministrativoFallo
    ;

}
