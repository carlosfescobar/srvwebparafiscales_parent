
package co.gov.ugpp.transversales.srvaplpersonajuridica.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplpersonajuridica.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpBuscarPorIdPersonaJuridicaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", "OpBuscarPorIdPersonaJuridicaSol");
    private final static QName _OpBuscarPorIdPersonaJuridicaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", "OpBuscarPorIdPersonaJuridicaResp");
    private final static QName _OpBuscarPorIdPersonaJuridicaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", "OpBuscarPorIdPersonaJuridicaFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplpersonajuridica.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaJuridicaRespTipo }
     * 
     */
    public OpBuscarPorIdPersonaJuridicaRespTipo createOpBuscarPorIdPersonaJuridicaRespTipo() {
        return new OpBuscarPorIdPersonaJuridicaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaJuridicaSolTipo }
     * 
     */
    public OpBuscarPorIdPersonaJuridicaSolTipo createOpBuscarPorIdPersonaJuridicaSolTipo() {
        return new OpBuscarPorIdPersonaJuridicaSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaJuridicaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", name = "OpBuscarPorIdPersonaJuridicaSol")
    public JAXBElement<OpBuscarPorIdPersonaJuridicaSolTipo> createOpBuscarPorIdPersonaJuridicaSol(OpBuscarPorIdPersonaJuridicaSolTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaJuridicaSolTipo>(_OpBuscarPorIdPersonaJuridicaSol_QNAME, OpBuscarPorIdPersonaJuridicaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaJuridicaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", name = "OpBuscarPorIdPersonaJuridicaResp")
    public JAXBElement<OpBuscarPorIdPersonaJuridicaRespTipo> createOpBuscarPorIdPersonaJuridicaResp(OpBuscarPorIdPersonaJuridicaRespTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaJuridicaRespTipo>(_OpBuscarPorIdPersonaJuridicaResp_QNAME, OpBuscarPorIdPersonaJuridicaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaJuridica/v1", name = "OpBuscarPorIdPersonaJuridicaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdPersonaJuridicaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdPersonaJuridicaFallo_QNAME, FalloTipo.class, null, value);
    }

}
