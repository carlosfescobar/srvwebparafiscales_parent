
package co.gov.ugpp.transversales.srvaplpersonanatural.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplpersonanatural.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearPersonaNaturalSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpCrearPersonaNaturalSol");
    private final static QName _OpCrearPersonaNaturalResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpCrearPersonaNaturalResp");
    private final static QName _OpCrearPersonaNaturalFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpCrearPersonaNaturalFallo");
    private final static QName _OpBuscarPorIdPersonaNaturalSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpBuscarPorIdPersonaNaturalSol");
    private final static QName _OpBuscarPorIdPersonaNaturalFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpBuscarPorIdPersonaNaturalFallo");
    private final static QName _OpBuscarPorIdPersonaNaturalResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", "OpBuscarPorIdPersonaNaturalResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplpersonanatural.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearPersonaNaturalRespTipo }
     * 
     */
    public OpCrearPersonaNaturalRespTipo createOpCrearPersonaNaturalRespTipo() {
        return new OpCrearPersonaNaturalRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearPersonaNaturalSolTipo }
     * 
     */
    public OpCrearPersonaNaturalSolTipo createOpCrearPersonaNaturalSolTipo() {
        return new OpCrearPersonaNaturalSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaNaturalRespTipo }
     * 
     */
    public OpBuscarPorIdPersonaNaturalRespTipo createOpBuscarPorIdPersonaNaturalRespTipo() {
        return new OpBuscarPorIdPersonaNaturalRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdPersonaNaturalSolTipo }
     * 
     */
    public OpBuscarPorIdPersonaNaturalSolTipo createOpBuscarPorIdPersonaNaturalSolTipo() {
        return new OpBuscarPorIdPersonaNaturalSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearPersonaNaturalSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpCrearPersonaNaturalSol")
    public JAXBElement<OpCrearPersonaNaturalSolTipo> createOpCrearPersonaNaturalSol(OpCrearPersonaNaturalSolTipo value) {
        return new JAXBElement<OpCrearPersonaNaturalSolTipo>(_OpCrearPersonaNaturalSol_QNAME, OpCrearPersonaNaturalSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearPersonaNaturalRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpCrearPersonaNaturalResp")
    public JAXBElement<OpCrearPersonaNaturalRespTipo> createOpCrearPersonaNaturalResp(OpCrearPersonaNaturalRespTipo value) {
        return new JAXBElement<OpCrearPersonaNaturalRespTipo>(_OpCrearPersonaNaturalResp_QNAME, OpCrearPersonaNaturalRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpCrearPersonaNaturalFallo")
    public JAXBElement<FalloTipo> createOpCrearPersonaNaturalFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearPersonaNaturalFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaNaturalSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpBuscarPorIdPersonaNaturalSol")
    public JAXBElement<OpBuscarPorIdPersonaNaturalSolTipo> createOpBuscarPorIdPersonaNaturalSol(OpBuscarPorIdPersonaNaturalSolTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaNaturalSolTipo>(_OpBuscarPorIdPersonaNaturalSol_QNAME, OpBuscarPorIdPersonaNaturalSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpBuscarPorIdPersonaNaturalFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdPersonaNaturalFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdPersonaNaturalFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdPersonaNaturalRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplPersonaNatural/v1", name = "OpBuscarPorIdPersonaNaturalResp")
    public JAXBElement<OpBuscarPorIdPersonaNaturalRespTipo> createOpBuscarPorIdPersonaNaturalResp(OpBuscarPorIdPersonaNaturalRespTipo value) {
        return new JAXBElement<OpBuscarPorIdPersonaNaturalRespTipo>(_OpBuscarPorIdPersonaNaturalResp_QNAME, OpBuscarPorIdPersonaNaturalRespTipo.class, null, value);
    }

}
