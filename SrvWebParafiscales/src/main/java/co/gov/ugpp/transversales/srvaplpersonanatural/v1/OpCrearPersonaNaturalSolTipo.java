
package co.gov.ugpp.transversales.srvaplpersonanatural.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;


/**
 * <p>Clase Java para OpCrearPersonaNaturalSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpCrearPersonaNaturalSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="personaNatural" type="{http://www.ugpp.gov.co/esb/schema/PersonaNaturalTipo/v1}PersonaNaturalTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpCrearPersonaNaturalSolTipo", propOrder = {
    "contextoTransaccional",
    "personaNatural"
})
public class OpCrearPersonaNaturalSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected PersonaNaturalTipo personaNatural;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad personaNatural.
     * 
     * @return
     *     possible object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public PersonaNaturalTipo getPersonaNatural() {
        return personaNatural;
    }

    /**
     * Define el valor de la propiedad personaNatural.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonaNaturalTipo }
     *     
     */
    public void setPersonaNatural(PersonaNaturalTipo value) {
        this.personaNatural = value;
    }

}
