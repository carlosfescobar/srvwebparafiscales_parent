
package co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpCrearReenvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpCrearReenvioComunicacionSol");
    private final static QName _OpBuscarPorCriteriosReenvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpBuscarPorCriteriosReenvioComunicacionResp");
    private final static QName _OpActualizarReenvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpActualizarReenvioComunicacionFallo");
    private final static QName _OpBuscarPorCriteriosReenvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpBuscarPorCriteriosReenvioComunicacionFallo");
    private final static QName _OpActualizarReenvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpActualizarReenvioComunicacionSol");
    private final static QName _OpCrearReenvioComunicacionFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpCrearReenvioComunicacionFallo");
    private final static QName _OpActualizarReenvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpActualizarReenvioComunicacionResp");
    private final static QName _OpBuscarPorCriteriosReenvioComunicacionSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpBuscarPorCriteriosReenvioComunicacionSol");
    private final static QName _OpCrearReenvioComunicacionResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", "OpCrearReenvioComunicacionResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearReenvioComunicacionRespTipo }
     * 
     */
    public OpCrearReenvioComunicacionRespTipo createOpCrearReenvioComunicacionRespTipo() {
        return new OpCrearReenvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosReenvioComunicacionSolTipo }
     * 
     */
    public OpBuscarPorCriteriosReenvioComunicacionSolTipo createOpBuscarPorCriteriosReenvioComunicacionSolTipo() {
        return new OpBuscarPorCriteriosReenvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarReenvioComunicacionRespTipo }
     * 
     */
    public OpActualizarReenvioComunicacionRespTipo createOpActualizarReenvioComunicacionRespTipo() {
        return new OpActualizarReenvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarReenvioComunicacionSolTipo }
     * 
     */
    public OpActualizarReenvioComunicacionSolTipo createOpActualizarReenvioComunicacionSolTipo() {
        return new OpActualizarReenvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosReenvioComunicacionRespTipo }
     * 
     */
    public OpBuscarPorCriteriosReenvioComunicacionRespTipo createOpBuscarPorCriteriosReenvioComunicacionRespTipo() {
        return new OpBuscarPorCriteriosReenvioComunicacionRespTipo();
    }

    /**
     * Create an instance of {@link OpCrearReenvioComunicacionSolTipo }
     * 
     */
    public OpCrearReenvioComunicacionSolTipo createOpCrearReenvioComunicacionSolTipo() {
        return new OpCrearReenvioComunicacionSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearReenvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpCrearReenvioComunicacionSol")
    public JAXBElement<OpCrearReenvioComunicacionSolTipo> createOpCrearReenvioComunicacionSol(OpCrearReenvioComunicacionSolTipo value) {
        return new JAXBElement<OpCrearReenvioComunicacionSolTipo>(_OpCrearReenvioComunicacionSol_QNAME, OpCrearReenvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosReenvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpBuscarPorCriteriosReenvioComunicacionResp")
    public JAXBElement<OpBuscarPorCriteriosReenvioComunicacionRespTipo> createOpBuscarPorCriteriosReenvioComunicacionResp(OpBuscarPorCriteriosReenvioComunicacionRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosReenvioComunicacionRespTipo>(_OpBuscarPorCriteriosReenvioComunicacionResp_QNAME, OpBuscarPorCriteriosReenvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpActualizarReenvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpActualizarReenvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarReenvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpBuscarPorCriteriosReenvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosReenvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosReenvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarReenvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpActualizarReenvioComunicacionSol")
    public JAXBElement<OpActualizarReenvioComunicacionSolTipo> createOpActualizarReenvioComunicacionSol(OpActualizarReenvioComunicacionSolTipo value) {
        return new JAXBElement<OpActualizarReenvioComunicacionSolTipo>(_OpActualizarReenvioComunicacionSol_QNAME, OpActualizarReenvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpCrearReenvioComunicacionFallo")
    public JAXBElement<FalloTipo> createOpCrearReenvioComunicacionFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearReenvioComunicacionFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarReenvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpActualizarReenvioComunicacionResp")
    public JAXBElement<OpActualizarReenvioComunicacionRespTipo> createOpActualizarReenvioComunicacionResp(OpActualizarReenvioComunicacionRespTipo value) {
        return new JAXBElement<OpActualizarReenvioComunicacionRespTipo>(_OpActualizarReenvioComunicacionResp_QNAME, OpActualizarReenvioComunicacionRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosReenvioComunicacionSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpBuscarPorCriteriosReenvioComunicacionSol")
    public JAXBElement<OpBuscarPorCriteriosReenvioComunicacionSolTipo> createOpBuscarPorCriteriosReenvioComunicacionSol(OpBuscarPorCriteriosReenvioComunicacionSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosReenvioComunicacionSolTipo>(_OpBuscarPorCriteriosReenvioComunicacionSol_QNAME, OpBuscarPorCriteriosReenvioComunicacionSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearReenvioComunicacionRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplReenvioComunicacion/v1", name = "OpCrearReenvioComunicacionResp")
    public JAXBElement<OpCrearReenvioComunicacionRespTipo> createOpCrearReenvioComunicacionResp(OpCrearReenvioComunicacionRespTipo value) {
        return new JAXBElement<OpCrearReenvioComunicacionRespTipo>(_OpCrearReenvioComunicacionResp_QNAME, OpCrearReenvioComunicacionRespTipo.class, null, value);
    }

}
