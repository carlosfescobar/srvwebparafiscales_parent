
package co.gov.ugpp.transversales.srvaplreenviocomunicacion.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.reenviocomunicaciontipo.v1.ReenvioComunicacionTipo;


/**
 * <p>Clase Java para OpActualizarReenvioComunicacionSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarReenvioComunicacionSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="reenvioComunicacion" type="{http://www.ugpp.gov.co/schema/Transversales/ReenvioComunicacionTipo/v1}ReenvioComunicacionTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarReenvioComunicacionSolTipo", propOrder = {
    "contextoTransaccional",
    "reenvioComunicacion"
})
public class OpActualizarReenvioComunicacionSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ReenvioComunicacionTipo reenvioComunicacion;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad reenvioComunicacion.
     * 
     * @return
     *     possible object is
     *     {@link ReenvioComunicacionTipo }
     *     
     */
    public ReenvioComunicacionTipo getReenvioComunicacion() {
        return reenvioComunicacion;
    }

    /**
     * Define el valor de la propiedad reenvioComunicacion.
     * 
     * @param value
     *     allowed object is
     *     {@link ReenvioComunicacionTipo }
     *     
     */
    public void setReenvioComunicacion(ReenvioComunicacionTipo value) {
        this.reenvioComunicacion = value;
    }

}
