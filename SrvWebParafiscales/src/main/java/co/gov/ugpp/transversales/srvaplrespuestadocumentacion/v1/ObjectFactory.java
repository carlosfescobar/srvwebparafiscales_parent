
package co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpRegistrarDocumentacionEsperadaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpRegistrarDocumentacionEsperadaResp");
    private final static QName _OpBuscarPorCriteriosDocumentacionEsperadaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpBuscarPorCriteriosDocumentacionEsperadaFallo");
    private final static QName _OpActualizarDocumentacionEsperadaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpActualizarDocumentacionEsperadaFallo");
    private final static QName _OpActualizarDocumentacionEsperadaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpActualizarDocumentacionEsperadaSol");
    private final static QName _OpRegistrarDocumentacionEsperadaFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpRegistrarDocumentacionEsperadaFallo");
    private final static QName _OpRegistrarDocumentacionEsperadaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpRegistrarDocumentacionEsperadaSol");
    private final static QName _OpBuscarPorCriteriosDocumentacionEsperadaSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpBuscarPorCriteriosDocumentacionEsperadaSol");
    private final static QName _OpActualizarDocumentacionEsperadaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpActualizarDocumentacionEsperadaResp");
    private final static QName _OpBuscarPorCriteriosDocumentacionEsperadaResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", "OpBuscarPorCriteriosDocumentacionEsperadaResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplrespuestadocumentacion.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDocumentacionEsperadaRespTipo }
     * 
     */
    public OpBuscarPorCriteriosDocumentacionEsperadaRespTipo createOpBuscarPorCriteriosDocumentacionEsperadaRespTipo() {
        return new OpBuscarPorCriteriosDocumentacionEsperadaRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDocumentacionEsperadaRespTipo }
     * 
     */
    public OpActualizarDocumentacionEsperadaRespTipo createOpActualizarDocumentacionEsperadaRespTipo() {
        return new OpActualizarDocumentacionEsperadaRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorCriteriosDocumentacionEsperadaSolTipo }
     * 
     */
    public OpBuscarPorCriteriosDocumentacionEsperadaSolTipo createOpBuscarPorCriteriosDocumentacionEsperadaSolTipo() {
        return new OpBuscarPorCriteriosDocumentacionEsperadaSolTipo();
    }

    /**
     * Create an instance of {@link OpRegistrarDocumentacionEsperadaSolTipo }
     * 
     */
    public OpRegistrarDocumentacionEsperadaSolTipo createOpRegistrarDocumentacionEsperadaSolTipo() {
        return new OpRegistrarDocumentacionEsperadaSolTipo();
    }

    /**
     * Create an instance of {@link OpActualizarDocumentacionEsperadaSolTipo }
     * 
     */
    public OpActualizarDocumentacionEsperadaSolTipo createOpActualizarDocumentacionEsperadaSolTipo() {
        return new OpActualizarDocumentacionEsperadaSolTipo();
    }

    /**
     * Create an instance of {@link OpRegistrarDocumentacionEsperadaRespTipo }
     * 
     */
    public OpRegistrarDocumentacionEsperadaRespTipo createOpRegistrarDocumentacionEsperadaRespTipo() {
        return new OpRegistrarDocumentacionEsperadaRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRegistrarDocumentacionEsperadaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpRegistrarDocumentacionEsperadaResp")
    public JAXBElement<OpRegistrarDocumentacionEsperadaRespTipo> createOpRegistrarDocumentacionEsperadaResp(OpRegistrarDocumentacionEsperadaRespTipo value) {
        return new JAXBElement<OpRegistrarDocumentacionEsperadaRespTipo>(_OpRegistrarDocumentacionEsperadaResp_QNAME, OpRegistrarDocumentacionEsperadaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpBuscarPorCriteriosDocumentacionEsperadaFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorCriteriosDocumentacionEsperadaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorCriteriosDocumentacionEsperadaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpActualizarDocumentacionEsperadaFallo")
    public JAXBElement<FalloTipo> createOpActualizarDocumentacionEsperadaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarDocumentacionEsperadaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDocumentacionEsperadaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpActualizarDocumentacionEsperadaSol")
    public JAXBElement<OpActualizarDocumentacionEsperadaSolTipo> createOpActualizarDocumentacionEsperadaSol(OpActualizarDocumentacionEsperadaSolTipo value) {
        return new JAXBElement<OpActualizarDocumentacionEsperadaSolTipo>(_OpActualizarDocumentacionEsperadaSol_QNAME, OpActualizarDocumentacionEsperadaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpRegistrarDocumentacionEsperadaFallo")
    public JAXBElement<FalloTipo> createOpRegistrarDocumentacionEsperadaFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpRegistrarDocumentacionEsperadaFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpRegistrarDocumentacionEsperadaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpRegistrarDocumentacionEsperadaSol")
    public JAXBElement<OpRegistrarDocumentacionEsperadaSolTipo> createOpRegistrarDocumentacionEsperadaSol(OpRegistrarDocumentacionEsperadaSolTipo value) {
        return new JAXBElement<OpRegistrarDocumentacionEsperadaSolTipo>(_OpRegistrarDocumentacionEsperadaSol_QNAME, OpRegistrarDocumentacionEsperadaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDocumentacionEsperadaSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpBuscarPorCriteriosDocumentacionEsperadaSol")
    public JAXBElement<OpBuscarPorCriteriosDocumentacionEsperadaSolTipo> createOpBuscarPorCriteriosDocumentacionEsperadaSol(OpBuscarPorCriteriosDocumentacionEsperadaSolTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDocumentacionEsperadaSolTipo>(_OpBuscarPorCriteriosDocumentacionEsperadaSol_QNAME, OpBuscarPorCriteriosDocumentacionEsperadaSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarDocumentacionEsperadaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpActualizarDocumentacionEsperadaResp")
    public JAXBElement<OpActualizarDocumentacionEsperadaRespTipo> createOpActualizarDocumentacionEsperadaResp(OpActualizarDocumentacionEsperadaRespTipo value) {
        return new JAXBElement<OpActualizarDocumentacionEsperadaRespTipo>(_OpActualizarDocumentacionEsperadaResp_QNAME, OpActualizarDocumentacionEsperadaRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorCriteriosDocumentacionEsperadaRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplRespuestaDocumentacion/v1", name = "OpBuscarPorCriteriosDocumentacionEsperadaResp")
    public JAXBElement<OpBuscarPorCriteriosDocumentacionEsperadaRespTipo> createOpBuscarPorCriteriosDocumentacionEsperadaResp(OpBuscarPorCriteriosDocumentacionEsperadaRespTipo value) {
        return new JAXBElement<OpBuscarPorCriteriosDocumentacionEsperadaRespTipo>(_OpBuscarPorCriteriosDocumentacionEsperadaResp_QNAME, OpBuscarPorCriteriosDocumentacionEsperadaRespTipo.class, null, value);
    }

}
