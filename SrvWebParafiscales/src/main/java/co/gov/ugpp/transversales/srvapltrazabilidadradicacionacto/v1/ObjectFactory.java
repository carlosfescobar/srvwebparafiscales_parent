
package co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpActualizarTrazabilidadRadicacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpActualizarTrazabilidadRadicacionActoFallo");
    private final static QName _OpCrearTrazabilidadRadicacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpCrearTrazabilidadRadicacionActoResp");
    private final static QName _OpBuscarPorIdTrazabilidadRadicacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpBuscarPorIdTrazabilidadRadicacionActoFallo");
    private final static QName _OpCrearTrazabilidadRadicacionActoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpCrearTrazabilidadRadicacionActoFallo");
    private final static QName _OpBuscarPorIdTrazabilidadRadicacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpBuscarPorIdTrazabilidadRadicacionActoSol");
    private final static QName _OpCrearTrazabilidadRadicacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpCrearTrazabilidadRadicacionActoSol");
    private final static QName _OpBuscarPorIdTrazabilidadRadicacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpBuscarPorIdTrazabilidadRadicacionActoResp");
    private final static QName _OpActualizarTrazabilidadRadicacionActoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpActualizarTrazabilidadRadicacionActoSol");
    private final static QName _OpActualizarTrazabilidadRadicacionActoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", "OpActualizarTrazabilidadRadicacionActoResp");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadRadicacionActoSolTipo }
     * 
     */
    public OpCrearTrazabilidadRadicacionActoSolTipo createOpCrearTrazabilidadRadicacionActoSolTipo() {
        return new OpCrearTrazabilidadRadicacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdTrazabilidadRadicacionActoRespTipo }
     * 
     */
    public OpBuscarPorIdTrazabilidadRadicacionActoRespTipo createOpBuscarPorIdTrazabilidadRadicacionActoRespTipo() {
        return new OpBuscarPorIdTrazabilidadRadicacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadRadicacionActoRespTipo }
     * 
     */
    public OpActualizarTrazabilidadRadicacionActoRespTipo createOpActualizarTrazabilidadRadicacionActoRespTipo() {
        return new OpActualizarTrazabilidadRadicacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpActualizarTrazabilidadRadicacionActoSolTipo }
     * 
     */
    public OpActualizarTrazabilidadRadicacionActoSolTipo createOpActualizarTrazabilidadRadicacionActoSolTipo() {
        return new OpActualizarTrazabilidadRadicacionActoSolTipo();
    }

    /**
     * Create an instance of {@link OpCrearTrazabilidadRadicacionActoRespTipo }
     * 
     */
    public OpCrearTrazabilidadRadicacionActoRespTipo createOpCrearTrazabilidadRadicacionActoRespTipo() {
        return new OpCrearTrazabilidadRadicacionActoRespTipo();
    }

    /**
     * Create an instance of {@link OpBuscarPorIdTrazabilidadRadicacionActoSolTipo }
     * 
     */
    public OpBuscarPorIdTrazabilidadRadicacionActoSolTipo createOpBuscarPorIdTrazabilidadRadicacionActoSolTipo() {
        return new OpBuscarPorIdTrazabilidadRadicacionActoSolTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpActualizarTrazabilidadRadicacionActoFallo")
    public JAXBElement<FalloTipo> createOpActualizarTrazabilidadRadicacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpActualizarTrazabilidadRadicacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadRadicacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpCrearTrazabilidadRadicacionActoResp")
    public JAXBElement<OpCrearTrazabilidadRadicacionActoRespTipo> createOpCrearTrazabilidadRadicacionActoResp(OpCrearTrazabilidadRadicacionActoRespTipo value) {
        return new JAXBElement<OpCrearTrazabilidadRadicacionActoRespTipo>(_OpCrearTrazabilidadRadicacionActoResp_QNAME, OpCrearTrazabilidadRadicacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpBuscarPorIdTrazabilidadRadicacionActoFallo")
    public JAXBElement<FalloTipo> createOpBuscarPorIdTrazabilidadRadicacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpBuscarPorIdTrazabilidadRadicacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpCrearTrazabilidadRadicacionActoFallo")
    public JAXBElement<FalloTipo> createOpCrearTrazabilidadRadicacionActoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpCrearTrazabilidadRadicacionActoFallo_QNAME, FalloTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdTrazabilidadRadicacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpBuscarPorIdTrazabilidadRadicacionActoSol")
    public JAXBElement<OpBuscarPorIdTrazabilidadRadicacionActoSolTipo> createOpBuscarPorIdTrazabilidadRadicacionActoSol(OpBuscarPorIdTrazabilidadRadicacionActoSolTipo value) {
        return new JAXBElement<OpBuscarPorIdTrazabilidadRadicacionActoSolTipo>(_OpBuscarPorIdTrazabilidadRadicacionActoSol_QNAME, OpBuscarPorIdTrazabilidadRadicacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpCrearTrazabilidadRadicacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpCrearTrazabilidadRadicacionActoSol")
    public JAXBElement<OpCrearTrazabilidadRadicacionActoSolTipo> createOpCrearTrazabilidadRadicacionActoSol(OpCrearTrazabilidadRadicacionActoSolTipo value) {
        return new JAXBElement<OpCrearTrazabilidadRadicacionActoSolTipo>(_OpCrearTrazabilidadRadicacionActoSol_QNAME, OpCrearTrazabilidadRadicacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpBuscarPorIdTrazabilidadRadicacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpBuscarPorIdTrazabilidadRadicacionActoResp")
    public JAXBElement<OpBuscarPorIdTrazabilidadRadicacionActoRespTipo> createOpBuscarPorIdTrazabilidadRadicacionActoResp(OpBuscarPorIdTrazabilidadRadicacionActoRespTipo value) {
        return new JAXBElement<OpBuscarPorIdTrazabilidadRadicacionActoRespTipo>(_OpBuscarPorIdTrazabilidadRadicacionActoResp_QNAME, OpBuscarPorIdTrazabilidadRadicacionActoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadRadicacionActoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpActualizarTrazabilidadRadicacionActoSol")
    public JAXBElement<OpActualizarTrazabilidadRadicacionActoSolTipo> createOpActualizarTrazabilidadRadicacionActoSol(OpActualizarTrazabilidadRadicacionActoSolTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadRadicacionActoSolTipo>(_OpActualizarTrazabilidadRadicacionActoSol_QNAME, OpActualizarTrazabilidadRadicacionActoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpActualizarTrazabilidadRadicacionActoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplTrazabilidadRadicacionActo/v1", name = "OpActualizarTrazabilidadRadicacionActoResp")
    public JAXBElement<OpActualizarTrazabilidadRadicacionActoRespTipo> createOpActualizarTrazabilidadRadicacionActoResp(OpActualizarTrazabilidadRadicacionActoRespTipo value) {
        return new JAXBElement<OpActualizarTrazabilidadRadicacionActoRespTipo>(_OpActualizarTrazabilidadRadicacionActoResp_QNAME, OpActualizarTrazabilidadRadicacionActoRespTipo.class, null, value);
    }

}
