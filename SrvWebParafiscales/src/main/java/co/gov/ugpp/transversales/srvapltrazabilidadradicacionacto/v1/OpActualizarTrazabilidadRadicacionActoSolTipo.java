
package co.gov.ugpp.transversales.srvapltrazabilidadradicacionacto.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.trazabilidadradicacionacto.v1.TrazabilidadRadicacionActoTipo;


/**
 * <p>Clase Java para OpActualizarTrazabilidadRadicacionActoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpActualizarTrazabilidadRadicacionActoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="trazabilidadRadicacionActo" type="{http://www.ugpp.gov.co/schema/Transversales/TrazabilidadRadicacionActo/v1}TrazabilidadRadicacionActoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpActualizarTrazabilidadRadicacionActoSolTipo", propOrder = {
    "contextoTransaccional",
    "trazabilidadRadicacionActo"
})
public class OpActualizarTrazabilidadRadicacionActoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected TrazabilidadRadicacionActoTipo trazabilidadRadicacionActo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad trazabilidadRadicacionActo.
     * 
     * @return
     *     possible object is
     *     {@link TrazabilidadRadicacionActoTipo }
     *     
     */
    public TrazabilidadRadicacionActoTipo getTrazabilidadRadicacionActo() {
        return trazabilidadRadicacionActo;
    }

    /**
     * Define el valor de la propiedad trazabilidadRadicacionActo.
     * 
     * @param value
     *     allowed object is
     *     {@link TrazabilidadRadicacionActoTipo }
     *     
     */
    public void setTrazabilidadRadicacionActo(TrazabilidadRadicacionActoTipo value) {
        this.trazabilidadRadicacionActo = value;
    }

}
