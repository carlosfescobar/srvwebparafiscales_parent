
package co.gov.ugpp.transversales.srvaplvalidador.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the co.gov.ugpp.transversales.srvaplvalidador.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _OpValidarFormatoArchivoSol_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", "OpValidarFormatoArchivoSol");
    private final static QName _OpValidarFormatoArchivoResp_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", "OpValidarFormatoArchivoResp");
    private final static QName _OpValidarFormatoArchivoFallo_QNAME = new QName("http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", "OpValidarFormatoArchivoFallo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: co.gov.ugpp.transversales.srvaplvalidador.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpValidarFormatoArchivoSolTipo }
     * 
     */
    public OpValidarFormatoArchivoSolTipo createOpValidarFormatoArchivoSolTipo() {
        return new OpValidarFormatoArchivoSolTipo();
    }

    /**
     * Create an instance of {@link OpValidarFormatoArchivoRespTipo }
     * 
     */
    public OpValidarFormatoArchivoRespTipo createOpValidarFormatoArchivoRespTipo() {
        return new OpValidarFormatoArchivoRespTipo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarFormatoArchivoSolTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", name = "OpValidarFormatoArchivoSol")
    public JAXBElement<OpValidarFormatoArchivoSolTipo> createOpValidarFormatoArchivoSol(OpValidarFormatoArchivoSolTipo value) {
        return new JAXBElement<OpValidarFormatoArchivoSolTipo>(_OpValidarFormatoArchivoSol_QNAME, OpValidarFormatoArchivoSolTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OpValidarFormatoArchivoRespTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", name = "OpValidarFormatoArchivoResp")
    public JAXBElement<OpValidarFormatoArchivoRespTipo> createOpValidarFormatoArchivoResp(OpValidarFormatoArchivoRespTipo value) {
        return new JAXBElement<OpValidarFormatoArchivoRespTipo>(_OpValidarFormatoArchivoResp_QNAME, OpValidarFormatoArchivoRespTipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FalloTipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.ugpp.gov.co/Transversales/SrvAplValidador/v1", name = "OpValidarFormatoArchivoFallo")
    public JAXBElement<FalloTipo> createOpValidarFormatoArchivoFallo(FalloTipo value) {
        return new JAXBElement<FalloTipo>(_OpValidarFormatoArchivoFallo_QNAME, FalloTipo.class, null, value);
    }

}
