
package co.gov.ugpp.transversales.srvaplvalidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextorespuestatipo.v1.ContextoRespuestaTipo;
import co.gov.ugpp.schema.trasnversales.validacionarchivotipo.v1.ValidacionArchivoTipo;


/**
 * <p>Clase Java para OpValidarFormatoArchivoRespTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpValidarFormatoArchivoRespTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoRespuesta" type="{http://www.ugpp.gov.co/esb/schema/ContextoRespuestaTipo/v1}ContextoRespuestaTipo"/>
 *         &lt;element name="validacionArchivoTipo" type="{http://www.ugpp.gov.co/schema/Trasnversales/ValidacionArchivoTipo/v1}ValidacionArchivoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpValidarFormatoArchivoRespTipo", propOrder = {
    "contextoRespuesta",
    "validacionArchivoTipo"
})
public class OpValidarFormatoArchivoRespTipo {

    @XmlElement(required = true)
    protected ContextoRespuestaTipo contextoRespuesta;
    @XmlElement(required = true)
    protected ValidacionArchivoTipo validacionArchivoTipo;

    /**
     * Obtiene el valor de la propiedad contextoRespuesta.
     * 
     * @return
     *     possible object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public ContextoRespuestaTipo getContextoRespuesta() {
        return contextoRespuesta;
    }

    /**
     * Define el valor de la propiedad contextoRespuesta.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoRespuestaTipo }
     *     
     */
    public void setContextoRespuesta(ContextoRespuestaTipo value) {
        this.contextoRespuesta = value;
    }

    /**
     * Obtiene el valor de la propiedad validacionArchivoTipo.
     * 
     * @return
     *     possible object is
     *     {@link ValidacionArchivoTipo }
     *     
     */
    public ValidacionArchivoTipo getValidacionArchivoTipo() {
        return validacionArchivoTipo;
    }

    /**
     * Define el valor de la propiedad validacionArchivoTipo.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidacionArchivoTipo }
     *     
     */
    public void setValidacionArchivoTipo(ValidacionArchivoTipo value) {
        this.validacionArchivoTipo = value;
    }

}
