
package co.gov.ugpp.transversales.srvaplvalidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.gestiondocumental.archivotipo.v1.ArchivoTipo;


/**
 * <p>Clase Java para OpValidarFormatoArchivoSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpValidarFormatoArchivoSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="archivoValidar" type="{http://www.ugpp.gov.co/schema/GestionDocumental/ArchivoTipo/v1}ArchivoTipo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpValidarFormatoArchivoSolTipo", propOrder = {
    "contextoTransaccional",
    "archivoValidar"
})
public class OpValidarFormatoArchivoSolTipo {

    @XmlElement(required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(required = true)
    protected ArchivoTipo archivoValidar;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad archivoValidar.
     * 
     * @return
     *     possible object is
     *     {@link ArchivoTipo }
     *     
     */
    public ArchivoTipo getArchivoValidar() {
        return archivoValidar;
    }

    /**
     * Define el valor de la propiedad archivoValidar.
     * 
     * @param value
     *     allowed object is
     *     {@link ArchivoTipo }
     *     
     */
    public void setArchivoValidar(ArchivoTipo value) {
        this.archivoValidar = value;
    }

}
