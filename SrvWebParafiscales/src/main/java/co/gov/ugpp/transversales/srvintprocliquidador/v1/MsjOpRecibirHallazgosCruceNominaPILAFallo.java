
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * <p>Clase Java para msjOpRecibirHallazgosCruceNominaPILAFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpRecibirHallazgosCruceNominaPILAFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceNominaPILAFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpRecibirHallazgosCruceNominaPILAFallo", propOrder = {
    "opRecibirHallazgosCruceNominaPILAFallo"
})
public class MsjOpRecibirHallazgosCruceNominaPILAFallo {

    @XmlElement(name = "OpRecibirHallazgosCruceNominaPILAFallo", nillable = true)
    protected FalloTipo opRecibirHallazgosCruceNominaPILAFallo;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceNominaPILAFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpRecibirHallazgosCruceNominaPILAFallo() {
        return opRecibirHallazgosCruceNominaPILAFallo;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceNominaPILAFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceNominaPILAFallo(FalloTipo value) {
        this.opRecibirHallazgosCruceNominaPILAFallo = value;
    }

}
