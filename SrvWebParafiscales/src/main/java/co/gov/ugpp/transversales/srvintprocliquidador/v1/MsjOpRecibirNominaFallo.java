
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.fallotipo.v1.FalloTipo;


/**
 * <p>Clase Java para msjOpRecibirNominaFallo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="msjOpRecibirNominaFallo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirNominaFallo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "msjOpRecibirNominaFallo", propOrder = {
    "opRecibirNominaFallo"
})
public class MsjOpRecibirNominaFallo {

    @XmlElement(name = "OpRecibirNominaFallo", nillable = true)
    protected FalloTipo opRecibirNominaFallo;

    /**
     * Obtiene el valor de la propiedad opRecibirNominaFallo.
     * 
     * @return
     *     possible object is
     *     {@link FalloTipo }
     *     
     */
    public FalloTipo getOpRecibirNominaFallo() {
        return opRecibirNominaFallo;
    }

    /**
     * Define el valor de la propiedad opRecibirNominaFallo.
     * 
     * @param value
     *     allowed object is
     *     {@link FalloTipo }
     *     
     */
    public void setOpRecibirNominaFallo(FalloTipo value) {
        this.opRecibirNominaFallo = value;
    }

}
