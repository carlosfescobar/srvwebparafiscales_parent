
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import co.gov.ugpp.esb.schema.contextotransaccionaltipo.v1.ContextoTransaccionalTipo;
import co.gov.ugpp.schema.transversales.hallazgoconciliacioncontabletipo.v1.HallazgoConciliacionContableTipo;


/**
 * <p>Clase Java para OpRecibirHallazgosCruceConciliacionContableNominaSolTipo complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="OpRecibirHallazgosCruceConciliacionContableNominaSolTipo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="contextoTransaccional" type="{http://www.ugpp.gov.co/esb/schema/ContextoTransaccionalTipo/v1}ContextoTransaccionalTipo"/>
 *         &lt;element name="hallazgo" type="{http://www.ugpp.gov.co/schema/Transversales/HallazgoConciliacionContableTipo/v1}HallazgoConciliacionContableTipo" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OpRecibirHallazgosCruceConciliacionContableNominaSolTipo", propOrder = {
    "contextoTransaccional",
    "hallazgo"
})
public class OpRecibirHallazgosCruceConciliacionContableNominaSolTipo {

    @XmlElement(namespace = "", required = true)
    protected ContextoTransaccionalTipo contextoTransaccional;
    @XmlElement(namespace = "")
    protected HallazgoConciliacionContableTipo hallazgo;

    /**
     * Obtiene el valor de la propiedad contextoTransaccional.
     * 
     * @return
     *     possible object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public ContextoTransaccionalTipo getContextoTransaccional() {
        return contextoTransaccional;
    }

    /**
     * Define el valor de la propiedad contextoTransaccional.
     * 
     * @param value
     *     allowed object is
     *     {@link ContextoTransaccionalTipo }
     *     
     */
    public void setContextoTransaccional(ContextoTransaccionalTipo value) {
        this.contextoTransaccional = value;
    }

    /**
     * Obtiene el valor de la propiedad hallazgo.
     * 
     * @return
     *     possible object is
     *     {@link HallazgoConciliacionContableTipo }
     *     
     */
    public HallazgoConciliacionContableTipo getHallazgo() {
        return hallazgo;
    }

    /**
     * Define el valor de la propiedad hallazgo.
     * 
     * @param value
     *     allowed object is
     *     {@link HallazgoConciliacionContableTipo }
     *     
     */
    public void setHallazgo(HallazgoConciliacionContableTipo value) {
        this.hallazgo = value;
    }

}
