
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceConciliacionContableNominaResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceConciliacionContableNomina_Output", propOrder = {
    "opRecibirHallazgosCruceConciliacionContableNominaResp"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceConciliacionContableNominaOutput {

    @XmlElement(name = "OpRecibirHallazgosCruceConciliacionContableNominaResp", nillable = true)
    protected OpRecibirHallazgosCruceConciliacionContableNominaRespTipo opRecibirHallazgosCruceConciliacionContableNominaResp;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaResp.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHallazgosCruceConciliacionContableNominaRespTipo }
     *     
     */
    public OpRecibirHallazgosCruceConciliacionContableNominaRespTipo getOpRecibirHallazgosCruceConciliacionContableNominaResp() {
        return opRecibirHallazgosCruceConciliacionContableNominaResp;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceConciliacionContableNominaResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHallazgosCruceConciliacionContableNominaRespTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceConciliacionContableNominaResp(OpRecibirHallazgosCruceConciliacionContableNominaRespTipo value) {
        this.opRecibirHallazgosCruceConciliacionContableNominaResp = value;
    }

}
