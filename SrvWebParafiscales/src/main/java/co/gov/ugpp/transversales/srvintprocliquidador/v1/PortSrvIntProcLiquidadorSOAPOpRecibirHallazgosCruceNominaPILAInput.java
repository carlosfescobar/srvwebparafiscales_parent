
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Input complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Input">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHallazgosCruceNominaPILASol" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHallazgosCruceNominaPILA_Input", propOrder = {
    "opRecibirHallazgosCruceNominaPILASol"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHallazgosCruceNominaPILAInput {

    @XmlElement(name = "OpRecibirHallazgosCruceNominaPILASol", nillable = true)
    protected OpRecibirHallazgosCruceNominaPILASolTipo opRecibirHallazgosCruceNominaPILASol;

    /**
     * Obtiene el valor de la propiedad opRecibirHallazgosCruceNominaPILASol.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHallazgosCruceNominaPILASolTipo }
     *     
     */
    public OpRecibirHallazgosCruceNominaPILASolTipo getOpRecibirHallazgosCruceNominaPILASol() {
        return opRecibirHallazgosCruceNominaPILASol;
    }

    /**
     * Define el valor de la propiedad opRecibirHallazgosCruceNominaPILASol.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHallazgosCruceNominaPILASolTipo }
     *     
     */
    public void setOpRecibirHallazgosCruceNominaPILASol(OpRecibirHallazgosCruceNominaPILASolTipo value) {
        this.opRecibirHallazgosCruceNominaPILASol = value;
    }

}
