
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirHojaCalculoLiquidadorResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirHojaCalculoLiquidador_Output", propOrder = {
    "opRecibirHojaCalculoLiquidadorResp"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirHojaCalculoLiquidadorOutput {

    @XmlElement(name = "OpRecibirHojaCalculoLiquidadorResp", nillable = true)
    protected OpRecibirHojaCalculoLiquidadorRespTipo opRecibirHojaCalculoLiquidadorResp;

    /**
     * Obtiene el valor de la propiedad opRecibirHojaCalculoLiquidadorResp.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirHojaCalculoLiquidadorRespTipo }
     *     
     */
    public OpRecibirHojaCalculoLiquidadorRespTipo getOpRecibirHojaCalculoLiquidadorResp() {
        return opRecibirHojaCalculoLiquidadorResp;
    }

    /**
     * Define el valor de la propiedad opRecibirHojaCalculoLiquidadorResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirHojaCalculoLiquidadorRespTipo }
     *     
     */
    public void setOpRecibirHojaCalculoLiquidadorResp(OpRecibirHojaCalculoLiquidadorRespTipo value) {
        this.opRecibirHojaCalculoLiquidadorResp = value;
    }

}
