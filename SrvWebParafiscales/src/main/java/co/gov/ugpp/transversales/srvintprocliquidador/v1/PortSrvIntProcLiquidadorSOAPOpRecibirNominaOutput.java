
package co.gov.ugpp.transversales.srvintprocliquidador.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Output complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Output">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.ugpp.gov.co/Transversales/SrvIntProcLiquidador/v1}OpRecibirNominaResp" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "portSrvIntProcLiquidadorSOAP_OpRecibirNomina_Output", propOrder = {
    "opRecibirNominaResp"
})
public class PortSrvIntProcLiquidadorSOAPOpRecibirNominaOutput {

    @XmlElement(name = "OpRecibirNominaResp", nillable = true)
    protected OpRecibirNominaRespTipo opRecibirNominaResp;

    /**
     * Obtiene el valor de la propiedad opRecibirNominaResp.
     * 
     * @return
     *     possible object is
     *     {@link OpRecibirNominaRespTipo }
     *     
     */
    public OpRecibirNominaRespTipo getOpRecibirNominaResp() {
        return opRecibirNominaResp;
    }

    /**
     * Define el valor de la propiedad opRecibirNominaResp.
     * 
     * @param value
     *     allowed object is
     *     {@link OpRecibirNominaRespTipo }
     *     
     */
    public void setOpRecibirNominaResp(OpRecibirNominaRespTipo value) {
        this.opRecibirNominaResp = value;
    }

}
