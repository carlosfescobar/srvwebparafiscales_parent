package co.gov.ugpp.parafiscales.procesos.test;

import co.gov.ugpp.esb.schema.identificaciontipo.v1.IdentificacionTipo;
import co.gov.ugpp.esb.schema.municipiotipo.v1.MunicipioTipo;
import co.gov.ugpp.esb.schema.personanaturaltipo.v1.PersonaNaturalTipo;
import co.gov.ugpp.parafiscales.MapperFacade;
import co.gov.ugpp.schema.denuncias.cotizantetipo.v1.CotizanteTipo;
import co.gov.ugpp.parafiscales.persistence.entity.ContactoPersona;
import co.gov.ugpp.parafiscales.persistence.entity.CorreoElectronico;
import co.gov.ugpp.parafiscales.persistence.entity.Cotizante;
import co.gov.ugpp.parafiscales.persistence.entity.Departamento;
import co.gov.ugpp.parafiscales.persistence.entity.Identificacion;
import co.gov.ugpp.parafiscales.persistence.entity.Municipio;
import co.gov.ugpp.parafiscales.persistence.entity.Persona;
import co.gov.ugpp.parafiscales.persistence.entity.ValorDominio;
import co.gov.ugpp.parafiscales.MapperFacadeImpl;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author rpadilla
 */
public class TestMapper {

    private final MapperFacade mapper = new MapperFacadeImpl();

    @Test
    public void testMapPersonaEntityToPersonaNaturalTipo() throws Exception {
        Persona persona = this.mockPersona();
        PersonaNaturalTipo personaNaturalTipo = mapper.map(persona, PersonaNaturalTipo.class);
        this.asegurarIgualdadPersonaEntityConPersonaNaturalTipo(persona, personaNaturalTipo);
    }

    @Test
    public void testMapCotizanteEntityToCotizanteTipo() throws Exception {
        Cotizante cotizante = this.mockCotizante();
        CotizanteTipo cotizanteTipo = mapper.map(cotizante, CotizanteTipo.class);
        this.asegurarIgualdadPersonaEntityConPersonaNaturalTipo(cotizante.getPersona(), cotizanteTipo);
    }

    @Test
    public void testMapIdentificacionEntityToIdentificacionTipo() throws Exception {

        Identificacion identificacion = this.mockIdentificacion();
        IdentificacionTipo identificacionTipo = mapper.map(identificacion, IdentificacionTipo.class);

        assertEquals(identificacion.getValNumeroIdentificacion(), identificacionTipo.getValNumeroIdentificacion());
        assertEquals(identificacion.getCodTipoIdentificacion(), identificacionTipo.getCodTipoIdentificacion());
        assertEquals(identificacion.getMunicipio().getCodigo().toString(),
                identificacionTipo.getMunicipioExpedicion().getCodMunicipio());
        assertEquals(identificacion.getMunicipio().getValNombre(),
                identificacionTipo.getMunicipioExpedicion().getValNombre());
        assertEquals(identificacion.getMunicipio().getDepartamento().getCodigo().toString(),
                identificacionTipo.getMunicipioExpedicion().getDepartamento().getCodDepartamento());
        assertEquals(identificacion.getMunicipio().getDepartamento().getValNombre(),
                identificacionTipo.getMunicipioExpedicion().getDepartamento().getValNombre());
    }

    @Test
    public void testMapMunicipioEntityToMunicipioTipo() throws Exception {

        Municipio municipio = this.mockMunicipio();

        MunicipioTipo municipioTipo = mapper.map(municipio, MunicipioTipo.class);

        assertEquals(municipio.getCodigo().toString(), municipioTipo.getCodMunicipio());
        assertEquals(municipio.getValNombre(), municipioTipo.getValNombre());
        assertEquals(municipio.getDepartamento().getCodigo().toString(),
                municipioTipo.getDepartamento().getCodDepartamento());
        assertEquals(municipio.getDepartamento().getValNombre(), municipioTipo.getDepartamento().getValNombre());
    }

    private void asegurarIgualdadPersonaEntityConPersonaNaturalTipo(Persona persona, PersonaNaturalTipo personaNaturalTipo) {

        assertNotNull(persona.getNombreCompleto());
        assertEquals(persona.getNombreCompleto(),
                personaNaturalTipo.getValNombreCompleto());
        assertEquals(persona.getCodEstadoCivil().getId(),
                personaNaturalTipo.getCodEstadoCivil());
        assertEquals(persona.getContacto().getEsAutorizaNotificacionElectronica().toString(),
                personaNaturalTipo.getContacto().getEsAutorizaNotificacionElectronica());
        assertEquals(persona.getValNumeroIdentificacion(),
                personaNaturalTipo.getIdPersona().getValNumeroIdentificacion());
        assertEquals(persona.getCodTipoIdentificacion().getId(),
                personaNaturalTipo.getIdPersona().getCodTipoIdentificacion());

        int emailsSize = personaNaturalTipo.getContacto().getCorreosElectronicos().size();

        assertTrue(emailsSize > 0);

        for (int i = 0; i < emailsSize; i++) {
            assertEquals(persona.getContacto().getCorreoElectronicoList().get(i)
                    .getValCorreoElectronico(), personaNaturalTipo.getContacto().getCorreosElectronicos().get(i).getValCorreoElectronico());
        }
    }

    /**
     * **********************************************************************
     *
     * Mocks of objects used for testing the MapperFacade
     * ********************************************************************
     */
    private Cotizante mockCotizante() {
        Cotizante cotizante = new Cotizante();
        cotizante.setPersona(this.mockPersona());
        return cotizante;
    }

    private Persona mockPersona() {
        Persona persona = new Persona();
        persona.setValNumeroIdentificacion("1234567890");
        persona.setCodTipoIdentificacion(this.mockCodTipoIdentificacion());
        persona.setValPrimerApellido("El Primer Apellido");
        persona.setValSegundoApellido("El Segundo Apellido");
        persona.setValPrimerNombre("El Primer Nombre");
        persona.setValSegundoNombre("El Segundo Nombre");
        persona.setCodSexo(this.mockCodSexo());
        persona.setCodEstadoCivil(this.mockCodEstadoCivil());
        persona.setCodNivelEducativo(this.mockCodNivelEducativo());
        persona.setContacto(this.mockContactoPersona());
        return persona;
    }

    private Identificacion mockIdentificacion() {
        Identificacion identificacion = new Identificacion();
        identificacion.setCodTipoIdentificacion("12345L");
        identificacion.setValNumeroIdentificacion("1234567890");
        identificacion.setMunicipio(this.mockMunicipio());
        return identificacion;
    }

    private ContactoPersona mockContactoPersona() {
        ContactoPersona contactoPersona = new ContactoPersona();
        contactoPersona.setEsAutorizaNotificacionElectronica(true);
        CorreoElectronico correoElectronico = new CorreoElectronico();
        correoElectronico.setValCorreoElectronico("test@everis.com");
        contactoPersona.getCorreoElectronicoList().add(correoElectronico);
        return contactoPersona;
    }

    private Municipio mockMunicipio() {
        Municipio municipio = new Municipio(12345L);
        municipio.setCodigo("789");
        municipio.setValNombre("Cartagena");
        municipio.setDepartamento(this.mockDepartamento());
        return municipio;
    }

    public Departamento mockDepartamento() {
        Departamento departamento = new Departamento(12L);
        departamento.setCodigo("123");
        departamento.setValNombre("Bolivar");
        return departamento;
    }

    private ValorDominio mockCodEstadoCivil() {
        ValorDominio cod = new ValorDominio("123");
        cod.setNombre("Soltero");
        return cod;
    }

    private ValorDominio mockCodTipoIdentificacion() {
        ValorDominio cod = new ValorDominio("123456");
        cod.setNombre("Tarjeta de Identidad");
        return cod;
    }

    private ValorDominio mockCodNivelEducativo() {
        ValorDominio cod = new ValorDominio("123");
        cod.setNombre("Doctorado");
        return cod;
    }

    private ValorDominio mockCodSexo() {
        ValorDominio cod = new ValorDominio("1234");
        cod.setNombre("Masculino");
        return cod;
    }

    private ValorDominio mockCodTipoPersona() {
        ValorDominio cod = new ValorDominio("123456");
        cod.setNombre("Persona Natural");
        return cod;
    }

}
