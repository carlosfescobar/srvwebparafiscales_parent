package co.gov.ugpp.parafiscales.dto;

/**
 * DTO que contiene los atributos que se validan de una celda en el servicio de validacion arhivo
 * @author jmunocab
 */
public class CellComponents {
    
    private boolean required;
    private int datatType;
    private boolean date;

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public int getDatatType() {
        return datatType;
    }

    public void setDatatType(int datatType) {
        this.datatType = datatType;
    }   

    public boolean isDate() {
        return date;
    }

    public void setDate(boolean isDate) {
        this.date = isDate;
    }
}
