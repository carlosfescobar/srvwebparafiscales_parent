package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum ClasificacionPersonaEnum {

    APORTANTE("78"),
    DENUNCIANTE("79"),
    NO_APORTANTE("81"),
    COTIZANTE("82"),
    ENTIDAD_EXTERNA("94"),
    TERCERO("83"),
    INVESTIGADO("84"),
    INTERESADO("85"),
    PERSONA_NATURAL("3"),
    PERSONA_JURIDICA("4"),
    REPRESENTANTE_LEGAL("101"),
    APODERADO("102"),
    AUTORIZADO("103");

    private final String code;

    ClasificacionPersonaEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }

}
