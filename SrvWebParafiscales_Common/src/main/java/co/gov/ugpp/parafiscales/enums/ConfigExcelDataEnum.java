package co.gov.ugpp.parafiscales.enums;

/**
 * Identifica los tipos de dato válidos para el procesamiento de los archivos en excel
 * @author jmunocab
 */
public enum ConfigExcelDataEnum {

    ROW_ID(-1);

    private final int index;

    ConfigExcelDataEnum(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
