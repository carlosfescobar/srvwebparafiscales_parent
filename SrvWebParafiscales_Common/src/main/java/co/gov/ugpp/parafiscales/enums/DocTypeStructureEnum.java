package co.gov.ugpp.parafiscales.enums;

/**
 * Enum que representa la configuraciòn de la hoja de estructura de docTypes del
 * servicio de validación de archivo
 *
 * @author jmunocab
 */
public enum DocTypeStructureEnum {

    PATH(0),
    COLUMN_INDEX(1);

    private final int index;

    DocTypeStructureEnum(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
