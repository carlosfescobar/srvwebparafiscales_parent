package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jumnocab
 */
public enum DominioEnum {

    COD_TIPO_DIRECCION("33");

    private final String code;

    DominioEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    
    @Override
    public String toString(){
        return code;
    }
}
