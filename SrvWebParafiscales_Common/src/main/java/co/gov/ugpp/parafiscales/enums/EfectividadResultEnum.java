package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum EfectividadResultEnum {

    EFECTIVO("240001"),
    NO_EFECTIVO("240002");

    private final String code;

    EfectividadResultEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    
    @Override
    public String toString(){
        return code;
    }
}
