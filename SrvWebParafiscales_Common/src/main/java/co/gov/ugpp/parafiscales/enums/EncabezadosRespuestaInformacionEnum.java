package co.gov.ugpp.parafiscales.enums;

/**
 * Enum que representa el Nombre de la columna y su posición en los dierentes
 * formatos del servicio de respuesta información
 *
 * @author jmunocab
 */
public enum EncabezadosRespuestaInformacionEnum {

    //Estructura BANCOS
    FORMATO_BANCOS_CELDA_NOMBRE_BANCO(1, "Nombre Banco", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_TIPO_DOCUMENTO(2, "Tipo de documento", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_NUMERO_DOCUMENTO(3, "Número de documento", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_BANCOS_CELDA_NOMBRES_APELLIDOS(4, "Nombre y apellidos", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_TIPO_CUENTA(5, "Tipo de cuenta", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_NUMERO_CUENTA(6, "Número de cuenta", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_BANCOS_CELDA_DIRECCION_RESIDENCIA(7, "Dirección residencia", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_DIRECCION_TRABAJO(8, "Dirección trabajo", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_TELEFONO(9, "Teléfono 1", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_BANCOS_CELDA_CELULAR(10, "Celular", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_BANCOS_CELDA_OBSERVACIONES(11, "Observaciones", false, TipoDatoExcelEnum.ALFANUMERICO),
    //Estructura TELEFONÍA
    FORMATO_TELEFONIA_CELDA_TIPO_DOCUMENTO(1, "Tipo de documento", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_NUMERO_DOCUMENTO(2, "Número de documento", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_TELEFONIA_CELDA_RAZON_SOCIAL(3, "Razón Social", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_DIRECCION(4, "Dirección 1", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_DEPARTAMENTO(5, "Departamento 1", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_CIUDAD(6, "Ciudad 1", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_DIRECCION_DOS(7, "Dirección 2", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_DEPARTAMENTO_DOS(8, "Departamento 2", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_CIUDAD_DOS(9, "Ciudad 2", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_TELEFONO(10, "Teléfono 1", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_TELEFONO_DOS(11, "Teléfono 2", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_CELULAR(12, "Celular", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_TELEFONIA_CELDA_EMAIL(13, "Email", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_TELEFONIA_CELDA_FAX(14, "Fax", true, TipoDatoExcelEnum.NUMERICO),
    //Estructura PILA
    FORMATO_PILA_CELDA_TIPO_DOCUMENTO(1, "Tipo de documento", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_PILA_CELDA_NUMERO_DOCUMENTO(2, "Número de documento", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_PILA_CELDA_NUMERO_PLANILLA(3, "Número de planilla", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_PILA_CELDA_PERIODO_SALUD(4, "Periodo salud", true, TipoDatoExcelEnum.FECHA),
    FORMATO_PILA_CELDA_FECHA_CARGUE_PILA(5, "Fecha cargue PILA", false, TipoDatoExcelEnum.FECHA),
    FORMATO_PILA_CELDA_NOMBRE_LOTE(6, "Nombre del lote", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_PILA_CELDA_OBSERVACIONES(7, "Observaciones", false, TipoDatoExcelEnum.ALFANUMERICO),
    //Estructura SAYP
    FORMATO_SAYP_CELDA_TIPO_DOCUMENTO(1, "Tipo de documento", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_SAYP_CELDA_NUMERO_DOCUMENTO(2, "Número de documento", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_SAYP_CELDA_RAZON_SOCIAL(3, "Nombre o Razón Social", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_SAYP_CELDA_FECHA_PAGO(4, "Fecha de pago", true, TipoDatoExcelEnum.FECHA),
    FORMATO_SAYP_CELDA_VALOR_PAGADO(5, "Valor pagado", true, TipoDatoExcelEnum.NUMERICO),
    FORMATO_SAYP_CELDA_CONFIRMACION(6, "Confirmación", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_SAYP_CELDA_FUNCIONARIO_VALIDADOR(7, "Funcionario validador", true, TipoDatoExcelEnum.ALFANUMERICO),
    FORMATO_SAYP_CELDA_OBSERVACIONES(8, "Observaciones", false, TipoDatoExcelEnum.ALFANUMERICO);

    private final int index;
    private final String nombreColumna;
    private final boolean requerido;
    private final TipoDatoExcelEnum tipoDato;

    EncabezadosRespuestaInformacionEnum(final int index, final String nombreColumna, final boolean requerido, final TipoDatoExcelEnum tipoDato) {
        this.index = index;
        this.nombreColumna = nombreColumna;
        this.requerido = requerido;
        this.tipoDato = tipoDato;
    }

    @Override
    public String toString() {
        return nombreColumna + ": " + index;
    }

    public int getIndex() {
        return index;
    }

    public String getNombreColumna() {
        return nombreColumna;
    }

    public boolean isRequerido() {
        return requerido;
    }

    public TipoDatoExcelEnum getTipoDato() {
        return tipoDato;
    }
}
