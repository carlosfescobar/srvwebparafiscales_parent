package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author agarciap
 */
public enum EquivalenciasCodigosDominioEnum {

    CEDULA_CIUDADANIA("100001", "CC"),
    TARJETA_IDENTIDAD("100002", "TI"),
    PASAPORTE("100003", "PA"),
    CEDULA_EXTRANJERIA("100004", "CE"),
    NIT("100005", "NI"),
    REGISTRO_CIVIL("100006", "TI");

    private final String codigoDominio;
    private final String equivalencia;

    EquivalenciasCodigosDominioEnum(final String codigoDominio, final String equivalencia) {
        this.codigoDominio = codigoDominio;
        this.equivalencia = equivalencia;
    }

    @Override
    public String toString() {
        return equivalencia + ": " + codigoDominio;
    }

    public String getCodigoDominio() {
        return codigoDominio;
    }

    public String getEquivalencia() {
        return equivalencia;
    }
    
    
    
}

