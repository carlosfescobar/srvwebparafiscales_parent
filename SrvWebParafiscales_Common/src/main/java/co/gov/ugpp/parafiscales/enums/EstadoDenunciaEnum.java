package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum EstadoDenunciaEnum {

    SUSPENDIDO("1270013");


    private final String code;

    
    
    EstadoDenunciaEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }

}
