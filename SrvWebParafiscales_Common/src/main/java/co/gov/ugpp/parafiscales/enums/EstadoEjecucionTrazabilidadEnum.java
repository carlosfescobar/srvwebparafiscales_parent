package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum EstadoEjecucionTrazabilidadEnum {

    PROCESO_TRAZABILIDAD_INICIADO("1660001"),
    PROCESO_TRAZABILIDAD_FALLIDO("1660002"),
    PROCESO_TRAZABILIDAD_COMPLETADO("1660003");

    private final String code;

    EstadoEjecucionTrazabilidadEnum(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }

    public String getCode() {
        return code;
    }
}
