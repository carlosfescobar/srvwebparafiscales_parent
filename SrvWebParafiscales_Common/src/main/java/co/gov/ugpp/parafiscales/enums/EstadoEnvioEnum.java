package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author everis Colombia
 */
public enum EstadoEnvioEnum {

    ENVIADO("1590001");

    private final String estadoEnvio;

    EstadoEnvioEnum(String code) {
        this.estadoEnvio = code;
    }

    public String getCode() {
        return estadoEnvio;
    }

    @Override
    public String toString() {
        return estadoEnvio;
    }
}
