package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum FileStructureEnum {

    // COD_OPERADOR("Código del operador"),
    VAL_NOMBRE_BANCO("Nombre Banco"),
    COD_TIPO_DOCUMENTO("Tipo de documento"),
    VAL_NUMERO_DOCUMENTO("Número de documento"),
    VAL_PRIMER_NOMBRE("Primer nombre"),
    VAL_SEGUNDO_NOMBRE("Segundo nombre"),
    VAL_CONFIRMACION("Confirmación"),
    VAL_PRIMER_APELLIDO("Primer apellido"),
    VAL_SEGUNDO_APELLIDO("Segundo apellido"),
    TIPO_CUENTA("Tipo de cuenta"),
    VAL_NUMERO_CUENTA("Número de cuenta"),
    VAL_DIRECCION_RESIDENCIA("Dirección residencia"),
    VAL_DIRECCION_TRABAJO("Dirección trabajo"),
    VAL_TELEFONO1("Teléfono 1"),
    VAL_CELULAR("Celular"),
    VAL_OBSERVACIONES("Observaciones"),
    FEC_CARGUE_PILA("Fecha cargue PILA"),
    VAL_NOMBRE_LOTE("Nombre del lote"),
    VAL_NOMBRE_RAZON_SOCIAL("Nombre o Razón Social"),
    VAL_RAZON_SOCIAL("Razón Social"),
    VAL_DIRECCION1("Dirección 1"),
    VAL_DEPARTAMENTO1("Departamento 1"),
    VAL_CIUDAD1("Ciudad 1"),
    VAL_DIRECCION2("Dirección 2"),
    VAL_DEPARTAMENTO2("Departamento 2"),
    VAL_CIUDAD2("Ciudad 2"),
    VAL_NUMERO_PLANILLA("Número de planilla"),
    VAL_TELEFONO2("Teléfono 2"),
    VAL_EMAIL("Email"),
    VAL_FAX("Fax"),
    FEC_PAGO("Fecha de pago"),
    VAL_VR_PAGO("Valor pagado"),
    VAL_FUNCIONARIO("Funcionario validador"),
    FEC_PERIODO_SALUD("Periodo salud"),
    NOMBRE_APELLIDO("Nombre y apellidos");

    private final String nombreColumna;

    FileStructureEnum(String nombreColumna) {
        this.nombreColumna = nombreColumna;
    }

    public String getNombreColumna() {
        return nombreColumna;
    }
}
