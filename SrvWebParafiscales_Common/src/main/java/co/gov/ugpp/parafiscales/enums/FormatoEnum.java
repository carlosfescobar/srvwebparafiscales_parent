package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum FormatoEnum {

    FORMATO_ESTRUCTURA_BANCO(1871L, 580L),
    FORMATO_ESTRUCTURA_PILA(1872L, 240L),
    FORMATO_ESTRUCTURA_SAYP(1873L, 321L),
    FORMATO_ESTRUCTURA_TELEFONIA(1874L, 499L);

    private final Long formato;
    private final Long version;

    FormatoEnum(Long formato, Long version) {
        this.formato = formato;
        this.version = version;
    }

    @Override
    public String toString() {
        return formato + ": " + version;
    }

    public Long getFormato() {
        return formato;
    }

    public Long getVersion() {
        return version;
    }

}
