/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jsaenzar
 */
public class LdapEnum {
    
        public enum Parametros
    {
        INITIAL_CONTEXT_FACTORY,
        PROVIDER_URL,
        SECURITY_AUTHENTICATION,
        SECURITY_PRINCIPAL,
        SECURITY_CREDENTIALS,
        SESSION_TIMEOUT,
        SMTP
    }
//    public static String[] ATTR_IDS_LDAP ={"sn","givenname","name","mail","sAMAccountName","telephonenumber"};    
    /**
     * 
     */
    public static String[] ATTR_IDS_LDAP ={"sAMAccountName", "displayName","givenname","sn","telephonenumber","st","l","title","description","department","physicalDeliveryOfficeName","mail","memberOf"}; 
}
