package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum OrigenProcesoEnum {

    DENUNCIA("730001"),
    PROGRAMA("730002");

    private final String code;

    OrigenProcesoEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }

}
