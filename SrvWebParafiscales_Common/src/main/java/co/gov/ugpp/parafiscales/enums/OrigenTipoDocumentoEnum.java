package co.gov.ugpp.parafiscales.enums;

/**
 * Enum que funciona de homologación entre tipos de documento y origen de documento
 * @author jmunocab
 */
public enum OrigenTipoDocumentoEnum {
    
    DOCUMENTO_HOJA_TRABAJO("1460001", "990022"),
    DOCUMENTO_ANEXO_TH("1460002", "990024"),
    DOCUMENTO_NOMINA("1460003", "990020"),
    DOCUMENTO_CONCILIACION_CONTABLE("1460004", "990021"),
    DOCUMENTO_PILA("1460005", "990023"),
    DOCUMENTO_MEMORANDO_CONTABLE("1460006", "990025");
    
    private final String codTipoDocumento;
    private final String codOrigenDocumento;

    OrigenTipoDocumentoEnum(String code, String codOrigenDocumento) {
        this.codTipoDocumento = code;
        this.codOrigenDocumento = codOrigenDocumento;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }

    public String getCodOrigenDocumento() {
        return codOrigenDocumento;
    }
}
