package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmuncab
 */
public enum ProcesoEnum {

    LIQUIDADOR("550001");

    private final String code;

    ProcesoEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    
    @Override
    public String toString(){
        return code;
    }
}
