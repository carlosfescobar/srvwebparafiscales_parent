package co.gov.ugpp.parafiscales.enums;

/**
 * Enum que representa los codigos de error de SQL para indicar que la base de
 * datos no está disponible
 *
 * @author jmunocab
 */
public enum SQLNotAvailableErrorCodesEnum {

    END_OF_FILE_COMMUNICATION_CHANNEL(3113),
    NOT_CONNECTED_ORACLE(3114),
    ORACLE_INITIALIZATION_OR_SHUTDOW_IN_PROGRESS(1033),
    ORACLE_NOT_AVAILABLE(1034),
    IMMEDIATE_SHUT_DOWN_IN_PROGRESS_NOT_OPERATIONS_PERMITED(1089),
    SHUT_DOWN_IN_PROGRESS_CONNECTIONS_IS_NOT_PERMITED(1090),
    IO_EXCEPTION(17002);

    private final int errorCode;

    SQLNotAvailableErrorCodesEnum(final int errorCode) {
        this.errorCode = errorCode;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
