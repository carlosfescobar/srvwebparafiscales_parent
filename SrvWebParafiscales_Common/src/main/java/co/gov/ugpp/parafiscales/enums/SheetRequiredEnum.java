package co.gov.ugpp.parafiscales.enums;

/**
 * Enum que representa la configuraciòn de la hoja de requeridos del servicio de validación de archivo
 *
 * @author jmunocab
 */
public enum SheetRequiredEnum {
    
    COLUMN_INDEX(0),
    DATA_TYPE(1),
    REQUIRED(2),
    IS_DATE(3);

    private final int index;

    SheetRequiredEnum(final int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
