package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum SolicitudEntidadEstadoEnum {

    SOLICITADA("230001"),
    SIN_RESOLVER("230003"),
    RESUELTA("230004"),
    ENVIADA("230002"),
    DEVUELTA("230008");

    private final String code;

    SolicitudEntidadEstadoEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
