package co.gov.ugpp.parafiscales.enums;

import java.util.Arrays;
import java.util.List;

/**
 * Enum que funciona contiene los nombres de las consultas de los KPI's de los
 * procesos del BPM de Parafiscales
 *
 * @author jmunocab
 */
public enum TipoConsultaKPIEnum {

    FISCALIZACION_FISCALIZACIONES_AUTOS_DE_ARCHIVO_DEVOLUCIONES("1690001", Arrays.asList("KPIFiscalizacion.conteoAutosArchivoDevoluciones", "KPIFiscalizacion.informacionAutosArchivoDevoluciones")),
    FISCALIZACION_FISCALIZACIONES_REQUERIMIENTOS_DEVOLUCIONES("1690002", Arrays.asList("KPIFiscalizacion.conteoRequerimientosDevoluciones", "KPIFiscalizacion.infoRequerimientosDevoluciones")),
    FISCALIZACION_FISCALIZACIONES_AUTO_ARCHIVO("1690003", Arrays.asList("KPIFiscalizacion.conteoAutosArchivo", "KPIFiscalizacion.infoAutosArchivo")),
    FISCALIZACION_FISCALIZACIONES_REQUERIMIENTO_DECLARAR_CORREGIR("1690004", Arrays.asList("KPIFiscalizacion.conteoReqDeclararCorregir", "KPIFiscalizacion.infoReqDeclararCorregir")),
    FISCALIZACION_FISCALIZACIONES_POR_ESTADO("1690005", Arrays.asList("KPIFiscalizacion.fiscalizacionesPorEstado")),
    LIQUIDACION_LIQUIDACIONES_POR_ESTADO("1690006", Arrays.asList("KPILiquidacion.liquidacionesPorEstado")),
    LIQUIDACION_EXPEDIENTES_AUTO_ARCHIVO("1690007", Arrays.asList("KPILiquidacion.expedientesAutoArchivo")),
    LIQUIDACION_RECURSOS_CONTRA_LIQUIDACIONES_OFICIALES("1690008", Arrays.asList("KPILiquidacion.recursosLiquidacionesOficiales")),
    RECURSO_RECONSIDERACION_RECURSOS_INADMITIDOS("1690009", Arrays.asList("KPIRecursosReconsideracion.recursosInadmitidos")),
    RECURSO_RECONSIDERACION_RECURSOS_ADMITIDOS("1690010", Arrays.asList("KPIRecursosReconsideracion.recursosAdmitidos")),
    RECURSO_RECONSIDERACION_RECURSOS_INTERPUESTOS("1690011", Arrays.asList("KPIRecursosReconsideracion.recursosInterpuestos")),
    RECURSO_RECONSIDERACION_SOLICITUDES_INVESTIGADO("1690012", Arrays.asList("KPIRecursosReconsideracion.solicitudesInvestigado")),
    RECURSO_RECONSIDERACION_SOLICITUDES_TERCERO("1690013", Arrays.asList("KPIRecursosReconsideracion.solicitudesTercero")),
    RECURSO_RECONSIDERACION_SOLICITUDES_INTERNAS("1690014", Arrays.asList("KPIRecursosReconsideracion.solicitudesInternas")),
    RECURSO_RECONSIDERACION_INSPECCIONES_TRIBUTARIAS("1690015", Arrays.asList("KPIRecursosReconsideracion.inspeccionesTributarias")),
    RECURSO_RECONSIDERACION_RECURSOS_POR_PROCESO_EJECUTADOR("1690016", Arrays.asList("KPIRecursosReconsideracion.porProceso")),
    DENUNCIAS_TRASLADADAS_NO_COMPETENCIA("1690017", Arrays.asList("KPIDenuncias.denunciasTrasladadasNoCompetencia")),
    DENUNCIAS_DENUNCIAS_POR_ESTADO("1690018", Arrays.asList("KPIDenuncias.denunciasPorEstado")),
    DENUNCIAS_DENUNCIAS_DECISION_COMITE("1690019", Arrays.asList("KPIDenuncias.denunciasPorDecisionComite")),
    SOLICITAR_INFORMACION_SOLICITUDES_ESTADO("1690020", Arrays.asList("KPISolicitudInformacion.solicitudesPorEstado")),
    NOTIFICACIONES_ACTOS_ADMINISTRATIVOS_TRAMITE_NOTIFICACION("1690021", Arrays.asList("KPINotificacion.actosAdministrativosTramiteNotificacion")),
    NOTIFICACIONES_ACTOS_ADMINISTRATIVOS_NOTIFICADOS("1690022", Arrays.asList("KPINotificacion.actosAdministrativosNotificados")),
    NOTIFICACIONES_ACTOS_ADMINISTRATIVOS_DEVUELTOS("1690023", Arrays.asList("KPINotificacion.actosAdministrativosDevueltos")),
    NOTIFICACIONES_ACTOS_ADMINISTRATIVOS_PUBLICADOS_WEB("1690024", Arrays.asList("KPINotificacion.actosAdministrativosPublicadosWeb")),
    SOLICITAR_INFORMACION_SOLICITUDES_COBRANZA("1690025", Arrays.asList("KPISolicitudInformacion.solicitudesPorTipo")),
    SOLICITAR_INFORMACION_DEVOLUCIONES_ENTIDAD_EXTERNA("1690026", Arrays.asList("KPISolicitudInformacion.solicitudesDevoluciones")),
    SOLICITAR_INFORMACION_SOLICITUDES_DETERMINACION("1690027", Arrays.asList("KPISolicitudInformacion.solicitudesPorTipo")),
    SANCIONES_EXPEDIENTES_PLIEGO_CARGOS("1690028", Arrays.asList("KPISancion.expedientesPliegoCargos")),
    SANCIONES_REQUERIMIENTOS_INFORMACION_GENERADOS("1690029", Arrays.asList("KPISancion.requerimientosInformacionGenerados")),
    SANCIONES_CASOS_ENVIADOS_NO_ENTREGA_INFORMACION("1690030", Arrays.asList("KPISancion.casosEnviadosSancionNoEntregaInformacion")),
    SANCIONES_SOLICITUDES_REALIZADAS_INVESTIGADO("1690031", Arrays.asList("KPISancion.solicitudesRealizadasInvestigado")),
    SANCIONES_SOLICITUDES_REALIZADAS_TERCERO("1690032", Arrays.asList("KPISancion.solicitudesRealizadasTercero")),
    SANCIONES_SOLICITUDES_INTERNAS("1690033", Arrays.asList("KPISancion.solicitudesInternas")),
    SANCIONES_SOLICITUDES_PRUEBAS_INVESTIGADO("1690034", Arrays.asList("KPISancion.solicitudesPruebasRealizadasInvestigado")),
    SANCIONES_SOLICITUDES_PRUEBAS_TERCERO("1690035", Arrays.asList("KPISancion.solicitudesPruebasRealizadasTercero")),
    SANCIONES_SOLICITUDES_PRUEBAS_INTERNAS("1690036", Arrays.asList("KPISancion.solicitudesPruebasInternasRealizadas")),
    SANCIONES_EXPEDIENTES_AUTOS_ARCHIVO("1690037", Arrays.asList("KPISancion.expedientesAutoArchivo")),
    SANCIONES_RECURSOS_RECONSIDERACION_PRESENTADOS("1690038", Arrays.asList("KPISancion.recursosReconsideracionPresentados")),
    SANCIONES_REQUERIMIENTOS_INFORMACION_DEVUELTOS("1690039", Arrays.asList("KPISancion.requerimientosInformacionDevueltos")),
    SANCIONES_PRUEBAS_DEVUELTAS("1690040", Arrays.asList("KPISancion.pruebasDevueltas")),
    SANCIONES_PLIEGOS_CARGO_DEVUELTOS("1690041", Arrays.asList("KPISancion.pliegosCargoDevueltos")),
    SANCIONES_RESOLUCIONES_SANCION_DEVUELTOS("1690042", Arrays.asList("KPISancion.resolucionesSancionDevueltas"));

    private final String codTipoKPI;
    private final List<String> valNombreConsulta;

    TipoConsultaKPIEnum(String codTipoKPI, List<String> valNombreConsulta) {
        this.codTipoKPI = codTipoKPI;
        this.valNombreConsulta = valNombreConsulta;
    }

    public String getCodTipoKPI() {
        return codTipoKPI;
    }

    public List<String> getValNombreConsulta() {
        return valNombreConsulta;
    }
}
