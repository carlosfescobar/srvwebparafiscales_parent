package co.gov.ugpp.parafiscales.enums;

/**
 * Identifica los tipos de dato válidos para el procesamiento de los archivos en excel
 * @author jmunocab
 */
public enum TipoDatoExcelEnum {

    FECHA("Fecha"),
    NUMERICO("Numérico"),
    ALFANUMERICO("Alfanumérico");

    private final String descripcion;

    TipoDatoExcelEnum(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }

}
