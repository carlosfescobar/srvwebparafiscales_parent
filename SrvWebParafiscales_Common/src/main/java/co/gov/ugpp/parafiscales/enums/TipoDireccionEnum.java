package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jsaenzar
 */
public enum TipoDireccionEnum {

    PROCESAL("330001"),
    RUT("330002"),
    PILA("330003"),
    RUE("330004"),
    RUA("330005"),
    OTRA("330006");

    private final String code;

    TipoDireccionEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code;
    }
}
