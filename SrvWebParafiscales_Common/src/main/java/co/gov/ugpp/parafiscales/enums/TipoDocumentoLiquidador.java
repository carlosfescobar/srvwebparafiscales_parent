package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jmunocab
 */
public enum TipoDocumentoLiquidador {
    
    DOCUMENTO_HOJA_TRABAJO("1460001"),
    DOCUMENTO_ANEXO_TH("1460002"),
    DOCUMENTO_NOMINA("1460003"),
    DOCUMENTO_CONCILIACION_CONTABLE("1460004"),
    DOCUMENTO_PILA("1460005"),
    DOCUMENTO_MEMORANDO_CONTABLE("1460006");
    
    private final String codTipoDocumento;

    TipoDocumentoLiquidador(String code) {
        this.codTipoDocumento = code;
    }

    @Override
    public String toString() {
        return codTipoDocumento;
    }

    public String getCodTipoDocumento() {
        return codTipoDocumento;
    }
}
