package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author everis Colombia
 */
public enum TipoHallazgoEnum {

    CRUCE_PILA_NOMINA("430001"),
    CRUCE_NOMINA_VACACIONES("430002"),
    CRUCE_CONCILIACION_CONTABLE("430003");

    private final String tipoEntrada;

    TipoHallazgoEnum(String code) {
        this.tipoEntrada = code;
    }

    public String getCode() {
        return tipoEntrada;
    }
    
    @Override
    public String toString(){
        return tipoEntrada;
    }
}
