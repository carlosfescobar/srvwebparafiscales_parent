package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author everis Colombia
 */
public enum TipoMedioEnum {

    CORRESPONDENCIA_OFICIO("220001"),
    PROGRAMA("220003");

    private final String tipoMedio;

    TipoMedioEnum(String code) {
        this.tipoMedio = code;
    }

    public String getCode() {
        return tipoMedio;
    }
    
    @Override
    public String toString(){
        return tipoMedio;
    }
}
