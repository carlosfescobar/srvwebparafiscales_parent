package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author jsaenzar
 */
public enum TipoPersonaEnum {

    PERSONA_NATURAL("200001"),
    PERSONA_JURIDICA("200002");

    private final String code;

    TipoPersonaEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    
    @Override
    public String toString(){
        return code;
    }
}
