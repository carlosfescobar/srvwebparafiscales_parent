package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author everis Colombia
 */
public enum TipoRadicadoEnum {

    RADICADO_ENTRADA("1530001"),
    RADICADO_SALIDA("1530002");

    private final String tipoRadicado;

    TipoRadicadoEnum(String code) {
        this.tipoRadicado = code;
    }

    public String getTipoRadicado() {
        return tipoRadicado;
    }
    
    @Override
    public String toString(){
        return tipoRadicado;
    }
}
