package co.gov.ugpp.parafiscales.enums;

/**
 *
 * @author everis Colombia
 */
public enum TipoSolicitudEnum {

    SOLICITUD_INFORMACION("380001"),
    SOLICITUD_PROGRAMA("380002");

    private final String tipoSolicitud;

    TipoSolicitudEnum(String code) {
        this.tipoSolicitud = code;
    }

    public String getCode() {
        return tipoSolicitud;
    }
    
    @Override
    public String toString(){
        return tipoSolicitud;
    }
}
