package co.gov.ugpp.parafiscales.exception;

import javax.ejb.ApplicationException;

/**
 *
 * @author jmunocab
 */
@ApplicationException(rollback = true)
public class TechnicalException extends RuntimeException {

    public TechnicalException(final String message, final Throwable throwable) {
        super(message, throwable);
    }

    public TechnicalException(final String message) {
        this(message, null);
    }
}
