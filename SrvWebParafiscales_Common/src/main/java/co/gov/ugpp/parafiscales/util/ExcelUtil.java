package co.gov.ugpp.parafiscales.util;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 *
 * @author zrodrigu
 */
public class ExcelUtil {

    public static boolean evaluarFila(Row row, int desde, int hasta) {
        for (int i = desde; i <= hasta; i++) {
            if (row.getCell(i) != null && !(row.getCell(i).getCellType() == Cell.CELL_TYPE_BLANK)) {
                return true;
            }
        }
        return false;
    }
}
