package co.gov.ugpp.parafiscales.util;

import co.gov.ugpp.parafiscales.enums.TipoEntradaEnum;

/**
 *
 * @author everis Colombia
 */
public class ParametroBD {

    //El tipo de entada del parámetro IN - OUT
    private TipoEntradaEnum tipoEntrada;
    //El valor del parámetro
    private Object valorEntrada;

    public TipoEntradaEnum getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(TipoEntradaEnum tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    public Object getValorEntrada() {
        return valorEntrada;
    }

    public void setValorEntrada(Object valorEntrada) {
        this.valorEntrada = valorEntrada;
    }
}
