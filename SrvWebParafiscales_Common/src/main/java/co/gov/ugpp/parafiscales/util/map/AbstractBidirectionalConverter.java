package co.gov.ugpp.parafiscales.util.map;

/**
 *
 * @author rpadilla
 * @param <S>
 * @param <D>
 */
public abstract class AbstractBidirectionalConverter<S, D> extends AbstractCustomConverter<Object, Object> {

    public AbstractBidirectionalConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
    }

    public abstract D convertTo(S srcObj);

    public abstract void copyTo(S srcObj, D destObj);

    public abstract S convertFrom(D srcObj);

    public abstract void copyFrom(D srcObj, S destObj);

    @Override
    public Object convert(Object srcObj) {
        if (getAType().isAssignableFrom(srcObj.getClass())) {
            return this.convertTo((S) srcObj);
        }
        return this.convertFrom((D) srcObj);
    }

    @Override
    public Object copy(final Object srcObj, final Object destObj) {
        if (this.getAType().isAssignableFrom(srcObj.getClass())) {
            this.copyTo((S) srcObj, (D) destObj);
        } else {
            this.copyFrom((D) srcObj, (S) destObj);
        }
        return destObj;
    }

    @Override
    public boolean canMap(Class<?> srcType, Class<?> destType) {
        return super.canMap(srcType, destType)
                || this.bType.isAssignableFrom(srcType) && this.aType.equals(destType);
    }

}
