package co.gov.ugpp.parafiscales.util.map;

public abstract class AbstractCustomConverter<S, D> extends AbstractCustomMapper<S, D> implements Converter<S, D> {

    public AbstractCustomConverter(final MapperFactory mapperFactory) {
        super(mapperFactory);
    }

}
