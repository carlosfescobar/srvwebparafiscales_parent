package co.gov.ugpp.parafiscales.util.map;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class AbstractCustomMapper<S, D> implements Converter<S, D> {

    protected final Class<S> aType;
    protected final Class<D> bType;
    protected final MapperFactory mapperFactory;

    public AbstractCustomMapper(final MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
        final Type[] pt = ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments();
        this.aType = (Class<S>) pt[0];
        this.bType = (Class<D>) pt[1];
    }

    @Override
    public boolean canMap(Class<?> srcType, Class<?> destType) {
        return this.aType.isAssignableFrom(srcType) && this.bType.equals(destType);
    }

    @Override
    public Class<S> getAType() {
        return this.aType;
    }

    @Override
    public Class<D> getBType() {
        return this.bType;
    }
    
    @Override
    public MapperFactory getMapperFactory() {
        return mapperFactory;
    }
    
    @Override
    public ConverterFactory getConverterFactory() {
        return mapperFactory.getConverterFactory();
    }

    @Override
    public MapperFacade getMapperFacade() {
        return mapperFactory.getMapperFacade();
    }
    
}
