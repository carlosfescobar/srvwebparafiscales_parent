package co.gov.ugpp.parafiscales.util.map;

import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rpadilla
 */
public class ClassMap {

    private final MapperFactory outer;

    public static enum DIRECTION {

        BOTH, A_TO_B, B_TO_A
    }

    private final Class<?> A;
    private final Class<?> B;
    private final PropertyDescriptor[] aProps;
    private final PropertyDescriptor[] bProps;
    private final String id;
    private boolean byDefault;
    private final Map<String, String> aToBFields;
    private final Map<String, String> bToAFields;
    private final Set<String> aExcludedFields;
    private final Set<String> bExcludedFields;

    protected ClassMap(final MapperFactory outer, Class<?> A, Class<?> B) {
        this.outer = outer;
        this.A = A;
        this.B = B;
        this.id = ClassMap.obtainId(this.A, this.B);
        this.byDefault = Boolean.FALSE;
        this.aToBFields = new LinkedHashMap<String, String>();
        this.bToAFields = new LinkedHashMap<String, String>();
        this.aExcludedFields = new LinkedHashSet<String>();
        this.bExcludedFields = new LinkedHashSet<String>();
        try {
            this.aProps = Introspector.getBeanInfo(A, Object.class).getPropertyDescriptors();
            this.bProps = Introspector.getBeanInfo(B, Object.class).getPropertyDescriptors();
        } catch (IntrospectionException ex) {
            throw new RuntimeException(ex);
        }
    }

    public ClassMap fieldAToB(String nameA, String nameB) {
        this.excludeFromB(nameA);
        return this.field(nameA, nameB, DIRECTION.A_TO_B);
    }

    public ClassMap fieldBToA(String nameA, String nameB) {
        this.excludeFromA(nameB);
        return this.field(nameB, nameA, DIRECTION.B_TO_A);
    }

    public ClassMap field(String name1, String name2) {
        return this.field(name1, name2, DIRECTION.BOTH);
    }

    private ClassMap field(String nameA, String nameB, DIRECTION direction) {
        if (direction == DIRECTION.BOTH || direction == DIRECTION.A_TO_B) {
            if (aToBFields.containsKey(nameA)) {
                throw new IllegalArgumentException("The field " + nameA + " was previously mapped as source from A to B.");
            }
            if (aToBFields.containsValue(nameB)) {
                throw new IllegalArgumentException("The field " + nameB + " was previously mapped as destination from A to B.");
            }
            aToBFields.put(nameA, nameB);
        }
        if (direction == DIRECTION.BOTH || direction == DIRECTION.B_TO_A) {
            if (bToAFields.containsKey(nameB)) {
                throw new IllegalArgumentException("The field " + nameB + " was previously mapped as source from B to A.");
            }
            if (bToAFields.containsValue(nameA)) {
                throw new IllegalArgumentException("The field " + nameA + " was previously mapped as destination from B to A.");
            }
            bToAFields.put(nameB, nameA);
        }
        return this;
    }

    public ClassMap excludeFromA(final String... names) {
        for (final String name : names) {
            this.excludeFromA(name);
        }
        return this;
    }

    public ClassMap excludeFromA(final String name) {
        if (aExcludedFields.contains(name)) {
            throw new IllegalArgumentException("The field " + name + " was previously excluded from A and you're trying to exclude it.");
        }
        aExcludedFields.add(name);
        return this;
    }

    public ClassMap excludeFromB(final String... names) {
        for (final String name : names) {
            this.excludeFromB(name);
        }
        return this;
    }

    public ClassMap excludeFromB(final String name) {
        if (bExcludedFields.contains(name)) {
            throw new IllegalArgumentException("The field " + name + " was previously excluded from B and you're trying to exclude it.");
        }
        bExcludedFields.add(name);
        return this;
    }

    public ClassMap exclude(final String... names) {
        for (final String name : names) {
            this.exclude(name);
        }
        return this;
    }

    public ClassMap exclude(final String name) {
        if (aToBFields.containsKey(name)) {
            throw new IllegalArgumentException("The field " + name + " was previously mapped from A to B and you're trying to exclude it.");
        }
        if (bToAFields.containsKey(name)) {
            throw new IllegalArgumentException("The field " + name + " was previously mapped from B to A and you're trying to exclude it.");
        }
        this.excludeFromA(name);
        this.excludeFromB(name);
        return this;
    }

    public void register() {
        this.outer.registerClassMap(this);
    }

    public static String obtainId(final Class<?> A, final Class<?> B) {
        if (A.getName().compareTo(B.getName()) >= 0) {
            return A.getName() + "_" + B.getName();
        }
        return B.getName() + "_" + A.getName();
    }

    public Class<?> getA() {
        return A;
    }

    public Class<?> getB() {
        return B;
    }

    public String getId() {
        return id;
    }

    public boolean isByDefault() {
        return byDefault;
    }

    public ClassMap byDefault() {
        this.byDefault = Boolean.TRUE;
        return this;
    }

    public Map<String, String> getAtoBFields() {
        return this.aToBFields;
    }

    public Map<String, String> getBtoAFields() {
        return this.bToAFields;
    }

    public Set<String> getAExcludedFields() {
        return this.aExcludedFields;
    }

    public Set<String> getBExcludedFields() {
        return this.bExcludedFields;
    }

    public PropertyDescriptor[] getAProps() {
        return aProps;
    }

    public PropertyDescriptor[] getBProps() {
        return bProps;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClassMap other = (ClassMap) obj;
        return !(this.id == null) ? other.id != null : !this.id.equals(other.id);
    }

    @Override
    public String toString() {
        return this.id;
    }

}
