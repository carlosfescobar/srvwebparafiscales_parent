package co.gov.ugpp.parafiscales.util.map;

public interface Converter<S, D> {

    D convert(S srcObj);

    D copy(S srcObj, D destObj);

    Class<S> getAType();

    Class<D> getBType();

    MapperFactory getMapperFactory();

    ConverterFactory getConverterFactory();

    MapperFacade getMapperFacade();

    boolean canMap(Class<?> srcType, Class<?> destType);

}
