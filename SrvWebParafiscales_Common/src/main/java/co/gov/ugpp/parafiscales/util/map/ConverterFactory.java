package co.gov.ugpp.parafiscales.util.map;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author rpadilla
 */
public class ConverterFactory {

    private final Set<Converter> converters = new HashSet<Converter>();
    private final Map<String, Converter> convertersCache = new HashMap<String, Converter>();

    public <S, D> void registerConverter(final Converter<S, D> converter) {        
        this.converters.add(converter);
    }

    public <S, D> Converter<S, D> getConverter(final Class<S> srcType, final Class<D> destType) {
        final String id = ClassMap.obtainId(srcType, destType);
        final Converter converter = convertersCache.get(id);
        if (converter != null) {
            return converter;
        }
        for (final Converter converterIt : converters) {
            if (converterIt.canMap(srcType, destType)) {
                convertersCache.put(id, converterIt);
                return converterIt;
            }
        }
        throw new IllegalArgumentException("No se ha encontrado un Converter para los tipos: "
                + srcType.getName() + ", " + destType.getName());
    }

}
