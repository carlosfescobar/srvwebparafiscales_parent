package co.gov.ugpp.parafiscales.util.map;

import java.util.List;
import java.util.Set;

/**
 *
 * @author rpadilla
 */
public class MapperFacade {

    private final MapperFactory mapperFactory;

    public MapperFacade(final MapperFactory mapperFactory) {
        this.mapperFactory = mapperFactory;
    }

    public <S, D> D map(final S source, final Class<D> destClass) {
        if (source == null) {
            return null;
        }
        final Converter converter = this.obtainConverter(source.getClass(), destClass);
        return (D) converter.convert(source);
    }

    public <S, D> D map(final S srcObj, final D destObj) {
        if (srcObj == null) {
            return null;
        }
        if (destObj == null) {
            throw new IllegalArgumentException("El objeto de destino no puede ser null");
        }
        final Converter converter = this.obtainConverter(srcObj.getClass(), destObj.getClass());
        return (D) converter.copy(srcObj, destObj);
    }

    public <S, D> List<D> map(List<S> source, Class<S> srcClass, Class<D> destClass) {
        if (source == null) {
            return null;
        }
        final List<D> resp;
        try {
            resp = (List<D>) source.getClass().newInstance();
        } catch (final IllegalAccessException exc) {
            throw new RuntimeException(exc);
        } catch (final InstantiationException exc) {
            throw new RuntimeException(exc);
        }
        for (final S src : source) {
            resp.add(this.map(src, destClass));
        }
        return resp;
    }

    public <S, D> Set<D> map(Set<S> source, Class<S> srcClass, Class<D> destClass) {
        if (source == null) {
            return null;
        }
        final Set<D> resp;
        try {
            resp = (Set<D>) source.getClass().newInstance();
        } catch (final IllegalAccessException exc) {
            throw new RuntimeException(exc);
        } catch (final InstantiationException exc) {
            throw new RuntimeException(exc);
        }
        for (final S src : source) {
            resp.add(this.map(src, destClass));
        }
        return resp;
    }

    private Converter obtainConverter(final Class<?> aClass, final Class<?> bClass) {
        return this.mapperFactory.getConverterFactory().getConverter(aClass, bClass);
    }
}
