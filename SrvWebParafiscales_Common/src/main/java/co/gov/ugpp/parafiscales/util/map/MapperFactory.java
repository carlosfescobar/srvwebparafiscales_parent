package co.gov.ugpp.parafiscales.util.map;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author rpadilla
 */
public class MapperFactory {

    private final Set<ClassMap> classMaps = new HashSet<ClassMap>();

    private final ConverterFactory converterFactory = new ConverterFactory();

    private final MapperFacade mapperFacade = new MapperFacade(this);
    

    public void registerClassMap(ClassMap classMap) {
        boolean added = this.classMaps.add(classMap);
        if (added == Boolean.FALSE) {
            throw new IllegalArgumentException("Mapeo duplicado : " + classMap.getId());
        }
    }

    public ClassMap classMap(Class<?> A, Class<?> B) {
        return new ClassMap(this, A, B);
    }
    
    public Set<ClassMap> getMappings() {
        return classMaps;
    }

    public ConverterFactory getConverterFactory() {
        return this.converterFactory;
    }

    public MapperFacade getMapperFacade() {
        return this.mapperFacade;
    }

}
